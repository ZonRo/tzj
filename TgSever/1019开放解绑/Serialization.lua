--------------------------------------------
--序列化操作
--作者:wym
--完成时间:2014.02.10
--------------------------------------------
local string = require("string")
local io = require("io")
local base = _G
SerialiZation ={}
function Hex2(x)
	if not x then return "nil" end
	return string.format("%x",x)
end

Hex = Hex or Hex2
local n = 0
---序列化的真正处理函数---
local function Serialize(f,o,tab,hex,hex2)
	tab = tab or 1
	local tabs = string.rep("\t",tab)
	if type(o) == "number" then
		if hex then
			f:write("0x"..Hex(o),",\n")
		else
			f:write(o,",\n")
		end
	elseif type(o) == "string" then
		f:write(string.format("%q",o),",\n")
	elseif type(o) == "table" then
		local sortT = {}
		for k,v in pairs(o) do
			if type(k) ~= "number" then
				if type(k) == "string" then
					f:write(tabs,string.format("[%q]",k)," = ")
				else
					error("cannot Serialize a" .. type(k))
				end
				if type(v) == 'table' then
					f:write("{\n")
					--dstring("s1")
					Serialize(f,v,tab+1,hex,hex2)
					f:write(tabs,"},\n")
				else
					--dstring("s2")
					Serialize(f,v,tab+1,hex,hex2)
				end
			else
				sortT[#sortT+1] = k
			end
		end
		table.sort(sortT)
		for i = 1,#sortT do
			if hex2 then
				f:write(tabs,"[0x",Hex(sortT[i]),"]"," = ")
			else
				f:write(tabs,"[",sortT[i],"]"," = ")
			end
			if type(o[sortT[i]]) == 'table' then
				f:write("{\n")
				--dstring("s3")
				Serialize(f,o[sortT[i]],tab+1,hex,hex2)
				f:write(tabs,"},\n")
			else
				--dstring("s4")
				Serialize(f,o[sortT[i]],tab+1,hex,hex2)
			end
		end
		tabs = string.rep("\t",tab-1)
	elseif type(o) == "boolean" then
		if o then f:write("true,\n")
		else f:write("false,\n") end
	else
		error("cannot Serialize a " .. type(o))
	end
end
---保存表到文件---
function SerialiZation.SaveTable(f,t,tname,hex,hex2)
	if type(t) ~= 'table' then print(tname..' not a table');return false end
	tname = tname or "未知"
	f:write(tname," = {\n")
	Serialize(f,t,1,hex,hex2)
	f:write("}\n")
	return true
end

---序列化的真正处理函数---
local function SerializeToStr(o,tab,hex,hex2)
	tab = tab or 1
	local tabs = string.rep("\t",tab)
	local str = ""
	if type(o) == "number" then
		str = str..o..",\n"
	elseif type(o) == "string" then
		str = str..string.format("%q",o)..",\n"
	elseif type(o) == "table" then
		for i = 1,#o do
			str = str..tabs.."["..i.."]".." = "
			if type(o[i]) == 'table' then
				str = str.."{\n"
				str = str..SerializeToStr(o[i],tab+1)
				str = str..tabs.."},\n"
			else
				str = str..SerializeToStr(o[i],tab+1)
			end
		end
		for k,v in pairs(o) do
			if type(k) ~= "number" then
				if type(k) == "string" then
					str = str..tabs..string.format("[%q]",k).." = "
				else
					error("cannot Serialize a" .. type(k))
				end
				if type(v) == 'table' then
					str = str.."{\n"
					str = str..SerializeToStr(v,tab+1)
					str = str..tabs.."},\n"
				else
					str = str..SerializeToStr(v,tab+1)
				end
			end
		end
		tabs = string.rep("\t",tab-1)
	elseif type(o) == "boolean" then
		if o then str = str.."true,\n"
		else str = str.."false,\n" end
	else
		error("cannot Serialize a " .. type(o))
	end
	return str
end
---保存表到字串---
function SerialiZation.SaveTableToStr(t,tname)
	if type(t) ~= 'table' then print(tname..' not a table');return false end
	tname = tname or "未知"
	local str = ""
	str = str..tname.." = {\n"
	str = str..SerializeToStr(t,1)
	str = str.."}\n"
	return str
end
---从文件中匹配账号密码---
function SerialiZation.ReadUserAndKey(fn)
	local t={}
	local index = 1
	local f = io.open(fn)
	if f then
		local str = f:read("*all")
		for user,pwd in string.gmatch(str,"(%w%w%w%w%w%w+@?%w*%.?%w*)%W+(%w+)") do--四个以上的字母
			t[index]= {}
			t[index].user = user
			t[index].pwd = pwd
			index = index+1
		end
		f:close()
	else
		return nil,"无法打开文件"
	end
	return t
end
function SerialiZation.listfile ( dir, ext, recursive, fullpath)
    -- set default
    dir = dir or '.'
    ext = ext or '*.*'                      -- *.* file / directory
    local rec = recursive and '/s' or ''   -- search sub directory

    -- trim quote proc
    dir = string.gsub( dir, "[\\/]", '\\' )
    dir = string.find( dir, '^%b""$') and string.sub( dir, 2, -2 ) or dir
    dir = string.find( dir, '.+\\$') and string.sub( dir, 1, -2 ) or dir

    if string.find( ext, "^[\\/]$" ) then  -- directory flag /ad
        ext = '/ad'
    else                                    -- file flag /a-d and ext *.* or etc.
        ext = '/a-d '..ext
    end

    -- in linux use ls
    local cmd = "pushd %q &dir %s /b %s &popd"
    cmd = string.format( cmd, dir, ext, rec )

    local t = {}
    local list = io.popen( cmd )
    for line in list:lines() do
		if fullpath then
			line = recursive and line or dir..'\\'..line
		end
        t[#t+1] = line
        --print(line)
    end
    list:close()
    return t
end
return SerialiZation
