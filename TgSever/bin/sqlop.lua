module("CSql",package.seeall)
require"luasql.sqlite3"
local env,con
function Open()
	env = assert(luasql.sqlite3())
	con =assert(env:connect("blackhand.db"))
	
end

function SelectTable(tname,tt)
	local cmd = "select * from "..tname.." where"
	local first = true
	for k,v in pairs(tt) do
		if first then first = false
		else cmd = cmd.." and" end
		cmd = cmd.." "..k.." = \'"..v.."\'"
	end
	return Execute(cmd)
end

function Close()
	con:close()
	env:close()
end

function enumSimpleTable(t)
         for k,v in pairs(t) do
                   print(k.." = "..v)
         end
end

function Commit()
	con:commit()
end

function Execute(statement)
	local cur = con:execute(statement)
	print("sql type:"..type(cur).." execute:"..statement)
	if not cur or type(cur) == "number" then return cur end
	local result = {}
	while true do
		local t = {cur:fetch()}
		if #t <= 0 then break end
		result[#result+1] = t
	end
	cur:close()
	return result
end