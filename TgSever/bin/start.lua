--package.path = [[.\?.lua;G:\eproject\验证服务器\lua\?.lua;G:\eproject\验证服务器\lua\?\init.lua;G:\eproject\验证服务器\?.lua;G:\eproject\验证服务器\?\init.lua]]
--package.cpath = [[.\?.dll;.\?51.dll;G:\eproject\验证服务器\?.dll;G:\eproject\验证服务器\?51.dll;G:\eproject\验证服务器\clibs\?.dll;G:\eproject\验证服务器\clibs\?51.dll;G:\eproject\验证服务器\loadall.dll;G:\eproject\验证服务器\clibs\loadall.dll]]
g_NewServer = true
g_UserData =  {}
require 'alien'
local Kernel32 = alien.load 'Kernel32.dll'

Kernel32.OutputDebugStringA:types {"string",abi="stdcall",ret = "void"}
function print(msg)
	Kernel32.OutputDebugStringA("msk_server:"..msg)
end

require "sqlop"
CSql.Open()
local sz = require "Serialization"
local extern = alien.load 'extern.dll'
extern.CreateWindow:types {"int",abi="stdcall",ret = "void"}
extern.EnCrypt:types {"string","string",abi="stdcall",ret = "string"}
extern.DeCrypt:types {"string","string",abi="stdcall",ret = "string"}
extern.SendText:types {"int","string",abi="stdcall",ret = "void"}
extern.AddText:types {"string",abi="stdcall",ret = "void"}
extern.GetMsgKind:types {"int",abi="stdcall",ret = "int"}
extern.GetClientHandle:types {"int",abi="stdcall",ret = "int"}
--extern.SendData:types {"string","string",abi="stdcall",ret = "void"}
extern.CloseClient:types {"string",abi="stdcall",ret = "void"}
extern.Disconnect:types {"int",abi="stdcall",ret = "void"}
extern.GetRequestText:types {"int",abi="stdcall",ret = "string"}
extern.GetMd5:types {"string",abi="stdcall",ret = "string"}
extern.OpenServer:types {"int",abi="stdcall",ret = "void"}

extern.OpenServer(6115)

local GetMd5 = extern.GetMd5
local CreateWindow = extern.CreateWindow
local EnCrypt = extern.EnCrypt
local DeCrypt = extern.DeCrypt
local SendData,CloseClient
local AddText = extern.AddText
if g_NewServer then
	SendData = extern.SendText
	CloseClient = extern.Disconnect
else
	CloseClient = extern.CloseClient
	SendData = extern.SendData
end

local GetMsgKind = extern.GetMsgKind
local GetClientHandle = extern.GetClientHandle
local GetRequestText = extern.GetRequestText
local Clinets = {}
math.randomseed(os.time())
local randomBase = "123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
function Getkey(n)
	local key = ""
	local idx
	for i =1,n do
		idx = math.random(1,#randomBase)
		key= key..randomBase:sub(idx,idx)
	end
	return key
end

local oldSend = SendData
function SendData(client,head,data,key)
	print("SendData to:"..client)
	if key then data = EnCrypt(data,key) end
	if head then data = head.."#"..data end	
	oldSend(client,data)
end

function Send(t,head,msg)
	SendData(t.client,head,msg,t.key)
end

function GetGameData()
	if g_GameData then return g_GameData end
	local f = assert(io.open("signature.lua","r"))
	g_GameData = f:read("*all")
	f:close()
	g_GameData = string.gsub(g_GameData,"SDClient.exe","!")
	g_GameData = string.gsub(g_GameData,"CEGUIBase.dll","@")
	g_GameData = string.gsub(g_GameData,"base","#")
	g_GameData = string.gsub(g_GameData,"call","&")
	g_GameData = string.gsub(g_GameData,"address","|")
	g_GameData = string.gsub(g_GameData,"offset","?")
	g_GameData = string.gsub(g_GameData,"hook",">")	
	return g_GameData
end

function TrueLog(typename,user,dec)
	local f=  io.open("userLog.txt","a+")
	if not f then return end
	dec = dec or ""
	f:write(os.date(),user,typename,dec,"\n")
	f:close()
end

function SetLog(typename,user,dec)
	dec = dec or "无"
	local cmd = "insert into log(createtime,type,extern,user) values("..os.time()..",\'"..typename.."\',\'"..dec.."\',\'"..user.."\')"
	CSql.Execute(cmd)
end

function GetTableNum(t)
	if not t then return 0 end
	local n = 0
	for _,_ in pairs(t) do
		n = n +1
	end
	return n
end

function CheckData(t,data)
	--print("CheckData key:"..t.key)
	--print("dt:"..data)
	data = DeCrypt(data,t.key)
	--print("adt:"..data)
	local cmd
	cmd,data = data:match("(%l+)#(.+)")
	--print("CheckData:"..cmd)
	if cmd == "reg" then
		local user,pwd,macInfo = data:match("(%w+)|(%w+)|(.+)")
		local res = CSql.Execute("select id from users where user = \'"..user.."\'")
		if res and #res > 0 then		
			SendData(t.client,"regFalied","用户已存在",t.key)
			return
		end
		macInfo = DeCrypt(macInfo,"fish>bear?")
		local f,err= loadstring("macT = {"..macInfo.."}")
		if err then SendData(t.client,"regFalied",err,t.key) return end
		f()
		t.macT = macT
		t.user = user
		t.pwd = pwd
		t.regkey = Getkey(5)
		t.regret = Getkey(5)
		Send(t,"code","SetRegister(\'"..t.regkey.."\',\'"..t.regret.."\') local ret = GetRegister(\'"..t.regkey.."\') SendVerifyServer(\"regret\",ret)")
	elseif cmd == "amdin" then
		print("admin log!!")
		if data == "wangyangmoc|wd125849" then
			t.admin = true
		end
	elseif cmd == "wymdt" then
		print("admin update data!!")
		if not t.admin then 
			SendData(t.client,"ERROR","非管理员",t.key)
			CloseClient(t.client)
			Clinets[t.client] = nil
			return
		end			
		local f = assert(io.open("signature.lua","w"))
		f:write(data)
		f:close()
		g_GameData = nil
		SendData(t.client,"SUC","数据上传成功",t.key)
	elseif cmd == "cards" then		
		if not t.admin then 
			SendData(t.client,"ERROR","非管理员",t.key)
			CloseClient(t.client)
			Clinets[t.client] = nil
			return
		end		
		print("暂不支持卡号查询")		
	elseif cmd == "regret" then
		if t.regret ~= data then return Send(t,"regFalied","注册表操作失败") end
		local res = CSql.SelectTable("mac",t.macT)
		if not res or #res == 0 then 
			print("not find recoard")
			cmd = "insert into mac(mc,cpu) values(\'"..t.macT.mc.."\',\'"..t.macT.cpu.."\')"
			res = CSql.Execute(cmd)
			if not res then SendData(t.client,"regFalied","set mac failed",t.key) return end
			res = CSql.Execute("select max(id) from mac")
			if not res then SendData(t.client,"regFalied","get macid failed",t.key) return end
		end
		local macid = res[1][1]
		print("get id:"..macid)
		cmd = "insert into users(user,pwd,time,chec,ret,macid) values(\'"..t.user.."\',\'"..t.pwd.."\',"..os.time()..",\'"..t.regkey.."\',\'"..t.regret.."\',"..macid..")"
		res = CSql.Execute(cmd)
		if not res then SendData(t.client,"regFalied","set user failed",t.key) return end
		CSql.Commit()
		SendData(t.client,"regOk",macid,t.key)
		SetLog("reg",t.user)
	elseif cmd == "log" then		
		print("log:"..data)
		local version,user,pwd,macid,dataKey,macInfo = data:match("(%d+)|(%w+)|(%w+)|(%d+)|([%w,]+)|(.+)")
		version = tonumber(version)--版本
		local res = CSql.Execute("select * from users where user = \'"..user.."\' and pwd = \'"..pwd.."\'")
		if not res or #res == 0 then return Send(t,"logFailed","账号或密码错误") end--账号密码验证
		print("账号OK")
		t.lastkey = res[1][5]
		t.lastret = res[1][6]
		t.time = res[1][4]
		t.curret = Getkey(5)
		t.id = res[1][1]
		t.dataKey = dataKey
		repeat
			t.curkey = Getkey(5)
		until t.curkey ~= t.lastkey
		macInfo = DeCrypt(macInfo,"fish>bear?")
		local f,err= loadstring("macT = {"..macInfo.."}")
		if err then return Send(t,"logFailed",err) end
		f()
		macid = tonumber(macid)
		local curId = tonumber(res[1][7])
		print("cur:"..curId.." tar:"..macid)
		if macid ~= curId then return Send(t,"logFailed","机器不匹配1") end--机器ID和发来的ID不一致		
		macInfo = DeCrypt(macInfo,"fish>bear?")
		res = CSql.SelectTable("mac",macT)		
		if not res or #res == 0 or tonumber(res[1][1]) ~= macid then return Send(t,"logFailed","机器不匹配2") end--账号ID和发来的ID不一致			
		if (t.time-os.time() < 10) then return Send(t,"logFailed","时间不足") end--时间验证			
		Send(t,"code","SetRegister(\'"..t.curkey.."\',\'"..t.curret.."\') local ret1,ret2 = GetRegister(\'"..t.curkey.."\') or \"nil\",GetRegister(\'"..t.lastkey.."\') or \"nil\" SendVerifyServer(\"logret\",ret1..\"|\"..ret2)")--随机代码验证
	elseif cmd == "logret" then
		local ret1,ret2 = data:match("(%w+)|(%w+)")	
		print("1:"..ret1.." 2:"..ret2.." l:"..t.lastret)
		if t.lastret ~= ret2 or t.curret ~= ret1 then return Send(t,"logFailed","机器不匹配3") end		
		local res = CSql.Execute("update users set chec ='"..t.curkey.."',ret = \'"..t.curret.."\'".." where id = "..t.id)
		if not res then return Send(t,"logFailed","账号更新失败") end
		local gameData = GetGameData()
		t.dataKey = DeCrypt(t.dataKey,"evUpdateGameDir")		
		gameData = EnCrypt(gameData,t.dataKey)
		local md5 = GetMd5(gameData)
		gameData = md5.."@"..gameData
		--print("str:"..gameData)
		Send(t,"code","DelRegister(\'"..t.lastkey.."\')")
		SendData(t.client,"logOk",t.time.."|"..gameData)	
		SetLog("log",t.user)	
	elseif cmd == "recharge" then
		local user,pwd,cardnum,macInfo = data:match("(%w+)|(%w+)|(%w+)|(.+)")
		version = tonumber(version)--版本
		local res = CSql.Execute("select id,macid,time from users where user = \'"..user.."\' and pwd = \'"..pwd.."\'")
		if not res or #res == 0 then return Send(t,"rechargeFailed","账号或密码错误") end
		local uid = res[1][1]
		local mid = res[1][2]
		local utm = res[1][3]
		utm = tonumber(utm)
		res = CSql.Execute("select id,usetime,alltime from card where number = \'"..cardnum.."\'")
		if not res or #res == 0 then return Send(t,"rechargeFailed","无法找到卡号") end
		local cid,usetime,alltime = res[1][1],res[1][2],res[1][3]
		print("usetime = "..usetime)
		usetime = tonumber(usetime)
		alltime = tonumber(alltime)				
		if usetime ~= 0 then
			return Send(t,"rechargeFailed","卡号已使用 "..os.date("%Y-%m-%d-%X",usetime))
		end
		local res = CSql.Execute("update card set usetime = "..os.time()..",userid = "..uid..",macid = "..mid.." where id = "..cid)
		if not res then return Send(t,"rechargeFailed","无法更新卡号") end
		if utm < os.time() then utm = os.time() end
		utm = utm + alltime
		local res = CSql.Execute("update users set time = "..utm.." where id = "..uid)
		if not res then
			CSql.Execute("update card set usetime = 0,userid = 0,macid = 0 where id = "..cid)
			return Send(t,"rechargeFailed","无法加上时间")
		end
		Send(t,"rechargeOk","到期时间 "..os.date("%Y-%m-%d-%X",utm))		
		SetLog("recharge",t.user)	
	elseif cmd == "aquiresql" then
		if not t.admin then 
			SendData(t.client,"ERROR","非管理员",t.key)
			CloseClient(t.client)
			Clinets[t.client] = nil
			return
		end	
		local ret = CSql.Execute(data)
		if not ret then Send(t,"aquirefaile","数据库查询失败") end		
		ret =  sz.SaveTableToStr(ret,"sqlret")	
		Send(t,"aquireok",ret)	
	elseif cmd == "sendfile" then
		if not t.admin then 
			SendData(t.client,"ERROR","非管理员",t.key)
			CloseClient(t.client)
			Clinets[t.client] = nil
			return
		end	
		local f= assert(io.open("tmp.data","w"))
		f:write(data)
		f:close()
		SendData(t.client,"SUC","文件上传成功",t.key)
	elseif cmd == "userdata" then
		if not t.admin then 
			SendData(t.client,"ERROR","非管理员",t.key)
			CloseClient(t.client)
			Clinets[t.client] = nil
			return
		end	
		local unum = 0
		local serverNum = {}
		for k,v in pairs(g_UserData) do
			unum = unum+1			
			serverNum[v[1]] = v[2] or 0
			serverNum[v[1]] = serverNum[v[1]] or 0
			serverNum[v[1]] = serverNum[v[1]] + 1			
		end
		local strS = "用户数量:"..unum.."\n"
		for k,v in pairs(serverNum) do
			strS = strS..k.." = "..v.."\n"
		end
		--ret =  sz.SaveTableToStr(g_UserData,"userData")	
		Send(t,"aquireok",strS)			
	else
		print("unknow cmd:"..(cmd or "nil"))
	end
end

function MainProc(event,data)
	print("event "..event)
	if event == "evClientMsg" then
		data = tonumber(data)
		local tp = GetMsgKind(data)
		local client = GetClientHandle(data)
		print("tp:"..tp.." client:"..client)
		if tp == 0 then 		
			MainProc("clientIn",client)
		elseif tp == 2 then 			
			g_CurClient = client
			MainProc("dataIn",data)
		else				
			MainProc("clientOut",client)
		end
	elseif event == "clientIn" then		
		Clinets[data] = {}
		Clinets[data].key = Getkey(16)
		Clinets[data].client = data		
		SendData(data,nil,Clinets[data].key)
	elseif event == "clientOut" then
		Clinets[data] = nil
	elseif event == "dataIn" then
		local client
		if g_NewServer then
			client = GetClientHandle(data)
			data = GetRequestText(data)
		else
			client,data = data:match("([^@]+)@(.+)")
		end
		--print("dataIn:"..data:len())
		--print("dataIn:"..data)		
		local t = Clinets[client]
		if not t then 
			print("not get client")
			CloseClient(client) 
			return 
		end
		CheckData(t,data)
	elseif event == "restart" then
		CSql.Close()
		for k,v in pairs(Clinets) do
			CloseClient(k) 
		end
		g_GameData = nil
	elseif event == "exit" then
		CSql.Close()
		g_GameData = nil
	elseif event == "createcard" then
		local tm,num = data:match("([^,]+),(%d+)")
		tm = tonumber(tm)
		num = tonumber(num)
		tm = tm*24*60*60
		if not tm or tm == 0 or not num or num == 0 then
			print("createcard arg error")
			return
		end
		print("createcard:"..tm.."-"..num)

	elseif event == "loadcard" then
		local tm,num = data:match("([^,]+),(%d+)")
		tm = tonumber(tm)
		num = tonumber(num)
		if not tm or tm == 0 or not num or num == 0 then
--			print("createcard arg error")
			return
		end
		tm = tm*24*60*60
--		print("loadcard:"..tm.."-"..num)
		local ret = CSql.Execute("select max(id) from card")
		local bg = 1
		if ret and ret[1] then bg = ret[1][1]+1 end
--		print("bg = "..bg)
		local tc = {}
		for i=1,num do
			tc[#tc+1] = bg..Getkey(20)
			bg = bg + 1
		end
	--	print("key create ok now insert")
		local cmd
		local snum,fnum = 0,0
		local curtm = os.time()
		for i=#tc,1,-1 do
			--print("now insert "..tc[i])
			cmd = "insert into card(number,createtime,selltime,usetime,userid,macid,alltime) values(\'"..tc[i].."\',"..curtm..","..curtm..",0,0,0,"..tm..")"
			ret = CSql.Execute(cmd)
			if not ret then 				
				table.remove(tc,i) 
				fnum = fnum + 1
--				print("insert error")
			else
				snum = snum + 1
--				print("insert ok")
			end
		end
--		print("now log")
		for i= 1,#tc do
			AddText(tc[i])
		end	
		AddText("成功:"..snum.." 失败:"..fnum)
	elseif event == "logdata" then
		local bindcode,ser,num = data:match("(%d+)|(%d+)|(%d+)")
		if not bindcode then return end
		bindcode,ser,num  = tonumber(bindcode),tonumber(ser),tonumber(num)
		g_UserData[bindcode] = {ser,num}
	elseif event == "adminlog" then
		if data == "wangyangmoc|wd125849" then
			g_AdminLog = true
		else
			g_AdminLog = false
		end
	end
end
print("server now start")
CreateWindow(e_L)