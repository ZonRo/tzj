require"lfs"
lfs.chdir("./aide")
local msk = require "cryptodll"
math.randomseed(os.time())
function copyfile(src,dest)
    local f = io.open(src,"rb")
    if not f then 
        print("copyfile failed file not exit",src)
        return 
    end
    local data = f:read("*all")
    f:close()
    f = io.open(dest,"wb")
    if not f then 
        print("copyfile failed file cant create",dest)
        return 
    end
    f:write(data)
    f:close()
    return
end

function cleardir(path)
     for file in lfs.dir(path) do
        if file ~= "." and file ~= ".." then
            os.remove(path..'/'..file)
        end
    end
end

function copydir(src,dest,wefind)
    for file in lfs.dir(src) do
        if file ~= "." and file ~= ".." then
            local f = src..'\\'..file
            local desf = dest.."\\"..file
            --print ("/t "..f)
            if string.find(f, wefind) ~= nil then
                --print("/t "..f)
                copyfile(f,desf)
            end
            local attr = lfs.attributes (f)
            assert (type(attr) == "table")
            if attr.mode == "directory" and intofolder then
                lfs.mkdir(desf)
                copydir (f,desf, wefind)
            else
                
            end
        end
    end
end

function findindir (path, wefind, r_table, intofolder,shortname)
    for file in lfs.dir(path) do
        if file ~= "." and file ~= ".." then
            local f = path..'\\'..file
            --print ("/t "..f)
            if string.find(f, wefind) ~= nil then
                --print("/t "..f)
                table.insert(r_table, f)
                if shortname then table.insert(shortname, file) end
            end
            local attr = lfs.attributes (f)
            assert (type(attr) == "table")
            if attr.mode == "directory" and intofolder then
                findindir (f, wefind, r_table, intofolder)
            else
                --for name, value in pairs(attr) do
                --    print (name, value)
                --end
            end
        end
    end
end

function compluafile()
    --lfs.chdir([[G:/bitbuket/tg_dll/aide]])
    lfs.mkdir("../scriptc_sd")
    cleardir("../scriptc_sd")
	local currentFolder = [[../script_sd]]
	-------------------------------------
	local input_table = {}
	findindir(currentFolder, "%.lua", input_table, true)--查找txt文件
	i=1
	local newpath,newshort
	local comand
	while input_table[i]~=nil do 
		newpath = string.gsub(input_table[i],"script_sd","scriptc_sd")
		os.execute("comp -o "..newpath.." "..input_table[i])
		print(newpath,"ok")
		i=i+1
	end
end

function PackToMskSpData()
    local currentFolder = [[../scriptc_sd]]
	-------------------------------------
    lfs.mkdir("../ver_release_sd")
    cleardir("../ver_release_sd")
	local input_table = {}
    local shortname_table = {}
	findindir(currentFolder, "%.lua", input_table, true,shortname_table)--查找txt文件
    i=1
	local newpath,newshort
	local comand
    local f,data
    f = assert(io.open([[C:/Windows/explorer.exe]],"rb"))
    data = f:read("*all")
    f:close()
    f = assert(io.open([[../ver_release_sd/mskdata]],"wb"))
    f:write(data)
    f:close()
    f = io.open([[../ver_release_sd/mskdata:gamestart]],"rb")
    if f then 
        print("mskdata not clear")
        f:close()
    else
        print("mskdata clear")
    end
    data = nil
    local rnum
	while input_table[i]~=nil do
        newshort = string.gsub(shortname_table[i],"%.lua","")
        print("now write",newshort)
        f = assert(io.open(input_table[i],"rb"))
		data = f:read("*all")
        f:close()
        print("crypt pre:"..#data)
        rnum = math.random(0,255)
        data = msk.xor(data,rnum)
        --data = msk.crypt(data,"pubKey")
        print("crypt ok:"..#data)
        f = assert(io.open([[../ver_release_sd/mskdata]]..":msk.sp."..newshort,"wb"))
		f:write(data)
        f:close()
        print("rnum:"..rnum)
		i=i+1
	end
end

function CopyNeedFile()
    --key 和 dll 不加密
    --写入私key
    copyfile("../Release/client_sd.exe","../ver_release_sd/msk.exe")
    print("client.exe ok")
    local data,data2
    local f = assert(io.open([[prvKey]],"rb"))
    data = f:read("*all")
    f:close()
    f = assert(io.open([[../ver_release_sd/mskdata:msk.priv]],"wb"))
    f:write(data)
    f:close()
    print("msk.priv ok")
    --写入dll
    f = assert(io.open([[../Release/dll_sd.dll]],"rb"))
    data = f:read("*all")
    f:close()
    f = assert(io.open([[../ver_release_sd/mskdata:msk.dll]],"wb"))
    f:write(data)
    f:close()
    print("msk.dll ok")
    copyfile("../Release/dll_loader.dll","../ver_release_sd/msk.exe:mskl")--msk.exe:
    --[==[
    f = assert(io.open([[../Release/dll_loader.dll]],"rb"))
    data = f:read(1024)
    data2 = f:read("*all")
    f:close()
    f = assert(io.open([[../ver_release_sd/mskl]],"wb"))
    f:write(data2)
    f:write(data)
    f:close()
    --]==]
    print("dll_loader.dll ok")
    lfs.mkdir("../ver_release_sd/luasocket")
    cleardir("../ver_release_sd/luasocket")
    copydir("../lua/luasocket","../ver_release_sd/luasocket","%.lua")
    print("luasocket ok")
end
--msk.key(1024,"prvKey","pubKey")
compluafile()
PackToMskSpData()
CopyNeedFile()

print("ok")