--require 'pack'
------------获取PE头部偏移-------------
function getPeHeader(f)
	if fstore.peHeader then return fstore.peHeader end
	local e_lfanew = 60--PE头RVA偏移
	f:seek("set",e_lfanew)--e_lfanew = 60
	_,fstore.peHeader = f:read(4):unpack('I')--读取偏移的值
	return fstore.peHeader
end

------------是否PE文件-------------
function isPeFile(f)
	f:seek("set")--移回文件起始
	local mz = f:read(2)
	if mz ~= "MZ" then print "MZ" return false end
	local peHeaderOff = getPeHeader(f)
	f:seek("set",peHeaderOff)--移动到PE头部
	local _,signature = f:read(4):unpack('I')--读取Signature
	if signature ~= 0x00004550 then  print "signature" return false end
	return true
end

--------------获取段数量-------------
function getNumberOfSections(f)
	if fstore.numOffSections then return fstore.numOffSections end
	local peFileHeader = getPeHeader(f)+4--文件头部
	f:seek("set",peFileHeader+2)--移动到NumberOfSections
	_,fstore.numOffSections =  f:read(2):unpack('H')
	return fstore.numOffSections
end

--------------根据名字获取段数据-------------
function getSectionDataByName(f,sName)
	local peSectionHeader = getPeHeader(f)+4+20+224
	f:seek("set",peSectionHeader)
	local sectionHeader = 0
	local numberOfSections = getNumberOfSections(f)
	for i = 1,numberOfSections do
		f:seek("set",peSectionHeader+(i-1)*40)--移动到NumberOfSections名字
		local name = f:read(8)
		if name:find(sName) then--获取段 
			sectionHeader = peSectionHeader+(i-1)*40
			break
		end
	end
	if sectionHeader == 0 then print"没有找到.text 段" return end
	f:seek("set",peSectionHeader+12)
	local _,virtualAddress = f:read(4):unpack('I')
	imageBase = imageBase+virtualAddress
	local _,sizeOfRawData = f:read(4):unpack('I')
	local _,pointerToRawData = f:read(4):unpack('I')
	f:seek("set",pointerToRawData)
	local data = f:read(sizeOfRawData)
	return data
end

------------获取文件数据段(.text)-------------
function getPeSectionData(fname,sName)
	fstore = {}--存储已有数据
	local f = assert(io.open(fname, "rb"))
	if not isPeFile(f) then
		print("不是PE文件！")
		f:close()
		return
	end
	local data = getSectionDataByName(f,sName)
	f:close()
	if data then 
		return data
	else
		print("寻找区段失败!")
	end
end
------------特征码字串转换为lua string-------------
local keyWord = {"(",")",".","%","+","-","*","?","[","]","^","$"}
function transSignatureToByte(sig)
	sig = string.gsub(sig,' ','')--去掉空格
	if sig:len()%2 ~= 0 then 
		error('特征码长度必须为2的倍数')
	else
		local str = ""
		for i= 1,sig:len(),2 do
			local hex = tonumber("0x"..sig:sub(i,i+1))
			str = str..string.char(hex)
		end
		local strLen = str:len()
		-- for _,v in ipairs(keyWord) do--替换掉lua的模式关键字 不然lua会按照模式匹配进行查找 这个版本暂不支持模式匹配
			-- local f = string.find(str,"%"..v)
			-- str = string.gsub(str,"%"..v,"%%%"..v)
		-- end
		return str,strLen
	end
end

function findString(lStr,sStr,bg)
	bg = bg or 1
	return string.find(lStr,sStr,bg) or -1
	--[[
	local len = #sStr
	for i=bg,#lStr-#sStr do
		if lStr:sub(i,i+len-1) == sStr then
			return i
		end
	end
	--]]
end

------------搜索特征码-------------
function searchSig1(data,sig)
	local bytes,len = transSignatureToByte(sig[1])
	local address,result
	local num = sig[5] or 1
	local offset = 0
	for i =1,num do
		offset = findString(data,bytes,offset+1)--string.find(data,bytes,offset+1)
	end
	print('get offset:',Hex(offset))
	if not offset then 
		result = -1
		address = -1		
		return address,result
	end
	local dis = sig[3]
	if sig[4] > 0 then dis = sig[3]+len 
	else dis = sig[3] end
	offset = offset+dis*sig[4]
	address =  offset+imageBase-1
	if sig[2] == "base" then
		if sig[4] < 0 then 
			offset = offset-4 
			address = address-4
		end
		_,result = string.sub(data,offset,offset+3):unpack('I')
	elseif sig[2] == "address" then
		result = address
	elseif sig[2] == "call" then
		if sig[4] < 0 then 
			offset = offset-4 
			address = address-4
		end
		local _,off = string.sub(data,offset,offset+3):unpack('I')
		result = address+off+4
	end
	print("address:",Hex(address),"offset:",offset)
	return address,result
end

function searchSig(data,sig)
	local bytes,len = transSignatureToByte(sig[1])
	local address,result
	local num = sig[5] or 1
	local offset = 0
	for i =1,num do
		offset = findString(data,bytes,offset+1)--string.find(data,bytes,offset+1)
	end
	if not offset then 
		result = 0
		address = 0
		return address,result
	end
	local dis = sig[3]
	if sig[4] > 0 then dis = sig[3]+len 
	else dis = sig[3] end
	offset = offset+dis*sig[4]
	address =  offset+imageBase-1
	if sig[2] == "base" then
		if sig[4] < 0 then 
			offset = offset-4 
			address = address-4
		end
		_,result = data:unpack('I',offset)
	elseif sig[2] == "address" then
		result = address
	elseif sig[2] == "call" then
		if sig[4] < 0 then 
			offset = offset-4 
			address = address-4
		end
		local _,off = data:unpack('I',offset)
		result = address+off+4
	end
	return result,address
end
