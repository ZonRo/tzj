local print = print
local type = type
local string = string
local cbArea = g_cbArea
local cbServer = g_cbServer
local cbScript = g_cbScript
local uiConfig = g_uiConfig
local tostring = tostring
local pcall = pcall
local assert = assert
local setmetatable = setmetatable
local tonumber = tonumber
local require = require
local curScriptT = g_ScriptT
CTeam = {}

--local lTeams = {}
local lNewTeam = {}
function CTeam.CreateTeam(t,at,count,sp)
	count = count or 5
	sp = sp or 'normal'
	print("CreateTeam",at.name,count)
	local ret = lNewTeam
	local teamready = false
	lNewTeam[#lNewTeam+1] = at
	CAccountManager.SetExtern(at.index,"teamleader",lNewTeam[1].name)
	CAccountManager.SetExtern(at.index,"specia",sp)
	if #lNewTeam >= count then
		--lTeams[#lTeams+1] = lNewTeam
		setmetatable(lNewTeam,CTeam)
		CTeam.__index = CTeam
		lNewTeam.curstep = 0
		lNewTeam.curat = 1
		lNewTeam.leader = at.name
		lNewTeam.status = sp
		--lNewTeam.steps = uiConfig.scriptt
		lNewTeam = {}
		teamready = true
	end
	return ret,teamready
end

function CTeam.ResetStep(t)
	--print("ResetStep")
	for i=1,#t do
		CAccountManager.SetExtern(t[i].index,"ready",false)
	end
end

function CTeam.IsStepOk(t,oid)
	for i=1,#t do
		if t[i].ready == false then
			--print("IsStepOk failed",i,t[i].user)
			return false
		end
	end
	return true
end

function CTeam.IsXcjReady(t)
	for i=1,#t do
		if t[i].xcjready ~= true then
			print("IsXcjReady failed",i,t[i].user)
			return false
		end
	end
	return true
end

function CTeam.ResetReady(t)
	for i=1,#t do
		CAccountManager.SetExtern(t[i].index,"specia","normal")
		CAccountManager.SetExtern(t[i].index,"teamleader","")
		t[i].team = nil
	end
	local tm
	local st
	for i=1,#t do
		st = g_savedTeam[t[i]]
		if st and #st > 0 then
			tm = st[#st]
			table.remove(st,#st)
			CAccountManager.SetExtern(t[i].index,"specia",tm.status)
			CAccountManager.SetExtern(t[i].index,"teamleader",tm.leader)
			t[i].team = tm
		end
	end
end

function CTeam.RunCurTskStep(t,at)
	if at ~= t[1] then
		NetSend(at.socket,"tleader#"..t[1].name)
	end
	local att = t[t.curat]
	local tid = t.steps[t.curstep]
	NetSend(at.socket,"rstep#"..tid)
	return true
end

function CTeam.RunTskStep(t)
	print("RunXcjStep")
	t.curstep = t.curstep + 1
	if t.curstep == #t.steps+1 then
		t.curstep = 0
		for i=1,#t do
			NetSend(t[i].socket,"rstep#-1")
		end
		return false
	end
	local at = t[t.curat]
	local tid = t.steps[t.curstep]
	for i=1,#t do
		NetSend(t[i].socket,"rstep#"..tid)
	end
	return true
end

function CTeam.RunCurXcjStep(t,at)
	if at ~= t[1] then
		NetSend(at.socket,"tleader#"..t[1].name)
	end
	local att = t[t.curat]
	NetSend(at.socket,"rxcjstep#"..t.curstep.."|"..att.guid.."|"..att.name)
	return true
end

function CTeam.RunXcjStep(t)
	print("RunXcjStep")
	t.curstep = t.curstep + 1
	if t.curstep == 5 then
		t.curstep = 0
		if t.curat == #t  then--全部执行完毕
			t.curat = 1
			for i=1,#t do
				NetSend(t[i].socket,"rxcjstep#5|0|0")
			end
			return false
		end
		t.curat = t.curat+1
	end
	local at = t[t.curat]
	for i=1,#t do
		NetSend(t[i].socket,"rxcjstep#"..t.curstep.."|"..at.guid.."|"..at.name)
	end
	return true
end

function CTeam.RunByStep()
	
end

function CTeam.RunCurTmpTeam(at)
	if nil == at.tmpteam then return false end
	NetSend(at.socket,"tleader#"..t[1].name)
	NetSend(at.socket,"ttstep#"..at.tmpteam.cstep)
	return true
end

function CTeam.RunCurByStep(at)
	if CTeam.RunCurTmpTeam(at) then return end
	NetSend(at.socket,"tleader#"..t[1].name)
	local at = t[t.curat]
	NetSend(at.socket,"rbystep#"..t.curstep)
	return true
end


function CTeam.RunTmpTeam(at)
	if nil == at.tmpteam then return false end
	at.tmpteam.cstep = at.tmpteam.cstep + 1
	at.tmpteam.readystep = at.tmpteam.readystep + 1
	local cat
	if at.tmpteam.readystep  == #at.team then
		at.tmpteam.readystep = 0
		for i=1,#at.team do
			cat = at.tmpteam[i]
			NetSend(cat.socket,"ttstep#"..at.tmpteam.cstep)
		end
	end
	if at.tmpteam.nstep == at.tmpteam.cstep then
		for i=1,#at.team do
			cat = at.tmpteam[i]
			cat.tmpteam = nil
		end
	end
	return true
end

function CTeam.RunStep(t)
	print("RunStep")
	if CTeam.RunTmpTeam(t[1]) then return end
	CTeam.ResetStep(t)
	if not t.teamleaderready then
		CAccountManager.SetExtern(t[1].index,"ready",true)
		for i=2,#t do
			NetSend(t[i].socket,"tleader#"..t[1].name)
		end
		return
	end
	if not t.teamready then
		CAccountManager.SetExtern(t[1].index,"ready",true)
		for i=2,#t do
			NetSend(t[1].socket,"iteam#"..t[i].guid..","..t[i].name)
		end
		return
	end
	if not t.friendready then
		CAccountManager.SetExtern(t[#t].index,"ready",true)--最后一个人不需要加
		local friends
		for i=1,#t-1 do
			friends = ""
			for j=i+1,#t do
				friends = friends..t[j].guid.."|"..t[j].name..","
			end
			NetSend(t[i].socket,"ifriend#"..friends)
		end
		return
	end
	if not t:StepFun() then
		CTeam.ResetReady(t)
	end
end

function CTeam.GetTeamLeader(t)
	return t[1].guid,t[1].name
end

return CTeam
