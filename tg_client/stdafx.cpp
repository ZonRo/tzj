
// stdafx.cpp : 只包括标准包含文件的源文件
// tg_client.pch 将作为预编译头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"
#include <map>
using std::map;

bool g_CreateClient = false;
CEvent g_etCreateGame;
CEvent g_etPlayerSound;
CEvent g_etClientExit;
CString g_strDllPath;
CString g_strGamDir;
CCriticalSection g_csGlobal;
CCriticalSection g_csGamDir;
bool g_bHideWindow;
#ifdef _VER_RELEASE
decltype(CreateFileW)* fCreateFileW;
decltype(ReadFile)* fReadFile;
decltype(WriteFile)* fWriteFile;
decltype(CloseHandle)* fCloseHandle;
decltype(GetFileSize)* fGetFileSize;

decltype(CreateToolhelp32Snapshot)* fCreateToolhelp32Snapshot;
decltype(Process32First)* fProcess32First;
decltype(Process32Next)* fProcess32Next;
#endif
//CTools theTools;



map<string,string> mapGlobal;
const char* GetGlobal(const char*key)
{
	const char *ret = "";
	g_csGlobal.Lock(); 
	auto it = mapGlobal.find(key);
	if (it != mapGlobal.end()){
		ret = it->second.c_str();
	}
	g_csGlobal.Unlock();
	return ret;
}

void SetGlobal(const char* key, const char* value)
{
	g_csGlobal.Lock();
	mapGlobal.erase(string(key));
	mapGlobal.insert(make_pair(string(key), string(value)));
	g_csGlobal.Unlock();
}

void LOG(const char *fmt, ...)
{
	auto fp = fopen("控制台错误.txt", "a");
	if (fp == nullptr) return;
	va_list	ap;
	char	sz[1024] = {0};
	va_start(ap, fmt);
	vsprintf(sz, fmt, ap);
	sz[strlen(sz)] = '\n';
	fwrite(sz, strlen(sz), 1, fp);
	fclose(fp);
	va_end(ap);
}