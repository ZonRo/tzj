#pragma once


// CTgClientDlg 对话框
#include "tg_client.h"
#include "tg_clientDoc.h"
#include "ListenSocket.h"  
#include "afxcmn.h"
#include "MyListCtrl.h"
#include "afxwin.h"
#include "MyComboBox.h"

class CTgClientDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CTgClientDlg)

public:
	CTgClientDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CTgClientDlg();

// 对话框数据
	enum { IDD = IDD_TG_CLIENT };
public:
	CMyListCtrl m_listAccount;
	CMyComboBox m_cbScript;
	CMyComboBox m_cbServer;
	CMyComboBox m_cbArea;
	//CMFCEditBrowseCtrl m_ctGameDir;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	//
	afx_msg void OnBnClickedCreategame();
	
public:
	
private:
	lua_State *& m_l;
public:
	//	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnNMClickListAccount(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListAccount(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCbnSelchangeComboArea();
protected:
	afx_msg LRESULT OnSocket(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedButton1();
	CString m_strCode;
	virtual BOOL OnInitDialog();
	afx_msg void OnNMRClickListAccount(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnShowwindow();
	afx_msg void OnRestart();
	afx_msg void OnShowdebuginfo();
	afx_msg void OnStopdebug();
	afx_msg void OnNMCustomdrawListAccount(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedRunlog();
	afx_msg void OnBnClickedCloseall();
	afx_msg void OnBnClickedHidestr();
};
