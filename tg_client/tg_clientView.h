
// tg_clientView.h : Ctg_clientView 类的接口
//

#pragma once

#include "resource.h"
#include "afxeditbrowsectrl.h"
#include "tg_clientDoc.h"
#include "ListenSocket.h"  
#include "afxcmn.h"
#include "MyListCtrl.h"
#include "afxwin.h"
#include "MyComboBox.h"
#include "CfgDlg.h"
#include "TgClientDlg.h"
class Ctg_clientView : public CFormView
{
protected: // 仅从序列化创建
	Ctg_clientView();
	DECLARE_DYNCREATE(Ctg_clientView)

public:
	enum{ IDD = IDD_TG_CLIENT_FORM };

// 特性
public:
	Ctg_clientDoc* GetDocument() const;

// 操作
public:

// 重写
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual void OnInitialUpdate(); // 构造后第一次调用

// 实现
public:
	virtual ~Ctg_clientView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual void PostNcDestroy();

public:
	CString m_strGamDir;
private:
	SOCKET m_sListen;
	lua_State *& m_l;
protected:
	afx_msg LRESULT OnSocket(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnFileOpen();
	afx_msg void OnBnClickedButton1();
	CString m_strCode;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CTabCtrl m_tbCtrl;
	CCfgDlg m_cfgDlg;
	CTgClientDlg m_tcDlg;
	afx_msg void OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult);
};

#ifndef _DEBUG  // tg_clientView.cpp 中的调试版本
inline Ctg_clientDoc* Ctg_clientView::GetDocument() const
   { return reinterpret_cast<Ctg_clientDoc*>(m_pDocument); }
#endif

