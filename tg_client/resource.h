//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 tg_client.rc 使用
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_TG_CLIENT_FORM              101
#define IDR_MAINFRAME                   128
#define IDR_tg_clientTYPE               130
#define IDD_DIALOG1                     310
#define IDD_DLGVF                       310
#define IDR_DLL2                        320
#define IDR_DLL3                        321
#define IDD_DIALOG_INPUT                322
#define IDD_TG_CLIENT                   325
#define IDD_DLGCFG                      326
#define IDD_CFG                         326
#define IDR_MENU_ACCOUNT                329
#define IDC_EDIT1                       1001
#define IDC_EDIT2                       1002
#define IDC_KEY                         1002
#define IDC_TEAMNUM                     1002
#define BTN_LOG                         1003
#define BTN_CREATEGAME                  1004
#define BTN_REG                         1004
#define IDC_TRADE_PEOPLE                1004
#define BTN_RECHARGE                    1005
#define IDC_TRADE_PLACE                 1005
#define EDB_GAMEDIR                     1006
#define BTN_REBIND                      1006
#define IDC_KEEP_MONEY                  1007
#define IDC_ADDBOUND                    1008
#define IDC_LIST_ACCOUNT                1009
#define IDC_EDIT_INPUT                  1011
#define IDC_COMBO_AREA                  1012
#define IDC_COMBO_SERVER                1013
#define IDC_COMBO_SCRIPT                1014
#define IDC_BUTTON1                     1015
#define IDC_USER                        1017
#define IDC_TAB1                        1018
#define IDC_MULOPEN                     1020
#define IDC_APPLY                       1021
#define IDC_LISTSERVER                  1025
#define IDC_PRO                         1026
#define IDC_MINSIZE                     1027
#define IDC_PLAY_WARN                   1028
#define IDC_CHECK1                      1029
#define IDC_RUNLOG                      1029
#define IDC_TRADE_FIRST                 1029
#define IDC_CLOSEALL                    1030
#define IDC_FIGHT_BACK                  1030
#define IDC_HIDESTR                     1032
#define ID_32772                        32772
#define ID_32773                        32773
#define ID_32774                        32774
#define ID_32775                        32775
#define ID_SHOWWINDOW                   32776
#define ID_RESTART                      32777
#define ID_SHOWDEBUGINFO                32778
#define ID_32779                        32779
#define ID_STOPDEBUG                    32780

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        330
#define _APS_NEXT_COMMAND_VALUE         32781
#define _APS_NEXT_CONTROL_VALUE         1033
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
