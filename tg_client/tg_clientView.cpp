
// tg_clientView.cpp : Ctg_clientView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "tg_client.h"
#endif
#include "../nomodule/MemoryModule.h"
#include "tg_clientDoc.h"
#include "tg_clientView.h"
#include "CfgDlg.h"
#include "TgClientDlg.h"
#include "InputDlg.h"
#include <WinSock2.h>
#include <Psapi.h>
//--自己添加的
#include "Thread.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#define  TM_CHECK 1

// Ctg_clientView

IMPLEMENT_DYNCREATE(Ctg_clientView, CFormView)

BEGIN_MESSAGE_MAP(Ctg_clientView, CFormView)
//	ON_NOTIFY(NM_THEMECHANGED, EDB_GAMEDIR, &Ctg_clientView::OnNMThemeChangedGamedir)
	ON_MESSAGE(WM_SOCKET, &Ctg_clientView::OnSocket)
	ON_COMMAND(ID_FILE_OPEN, &Ctg_clientView::OnFileOpen)
	ON_BN_CLICKED(IDC_BUTTON1, &Ctg_clientView::OnBnClickedButton1)
	ON_WM_TIMER()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, &Ctg_clientView::OnTcnSelchangeTab1)
END_MESSAGE_MAP()

// Ctg_clientView 构造/析构

Ctg_clientView::Ctg_clientView()
	: CFormView(Ctg_clientView::IDD),
	m_l(theApp.m_l)
#ifndef _VER_RELEASE
	, m_strCode(_T("G:\\bitbuket\\tzj\\script_clinet_tg\\test.lua"))
#endif
{
	// TODO:  在此处添加构造代码
}

Ctg_clientView::~Ctg_clientView()
{
}

void Ctg_clientView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_strCode);
	DDX_Control(pDX, IDC_TAB1, m_tbCtrl);
}

BOOL Ctg_clientView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO:  在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式
	return CFormView::PreCreateWindow(cs);
}

#include "MainFrm.h"
void Ctg_clientView::OnInitialUpdate()
{
	try
	{
		CFormView::OnInitialUpdate();
		GetParentFrame()->RecalcLayout();
		ResizeParentToFit();
		m_tbCtrl.InsertItem(0, _T("账号"));
		m_tbCtrl.InsertItem(1, _T("配置"));
		m_cfgDlg.Create(IDD_DLGCFG, &m_tbCtrl);
		m_tcDlg.Create(IDD_TG_CLIENT, &m_tbCtrl);
		m_tbCtrl.SetCurSel(0);
		CRect rs;
		m_tbCtrl.GetClientRect(&rs);
		//调整子对话框在父窗口中的位置，根据实际修改
		rs.top += 25;
		rs.bottom -= 60;
		rs.left += 1;
		rs.right -= 10;

		//设置子对话框尺寸并移动到指定位置
		m_cfgDlg.MoveWindow(&rs);
		m_tcDlg.MoveWindow(&rs);
		//分别设置隐藏和显示
		m_tcDlg.ShowWindow(true);
		m_cfgDlg.ShowWindow(false);
		//*(int *)0 = 0;
		auto sData = lt::get<lt::table>(m_l, "gtServerData");
		auto tString = sData.get<lt::table>("data").get<const char *>("time");
		GetDocument()->SetTitle(CString(tString));
		AfxBeginThread(CreateGameThread, this);
		AfxBeginThread(PlayerSoundThread, this);
		// Create listening socket  
		SOCKADDR_IN   local;
		m_sListen = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		// Bind  
		local.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
		local.sin_family = AF_INET;
		local.sin_port = htons(0);
		ENSURE_WIN32(::bind(m_sListen, (struct sockaddr *)&local, sizeof(local)) == 0);
		// Listen  
		ENSURE_WIN32(listen(m_sListen, 3) == 0);
		// Associate listening socket with FD_ACCEPT event  
		ENSURE_WIN32(0==WSAAsyncSelect(m_sListen, this->GetSafeHwnd(), WM_SOCKET, FD_ACCEPT));
		this->SetTimer(TM_CHECK, 30000, NULL);

		auto clData = lt::table(m_l, "gtClientData");
		SOCKADDR_IN sa;
		memset(&sa, 0, sizeof(sa));
		int len = sizeof(sa);
		auto err = getsockname(m_sListen, (SOCKADDR *)&sa, &len);
		ENSURE_WIN32(err == 0);
		//WSAEFAULT
		//ENSURE_WIN32(0 == getsockname(m_sListen, (SOCKADDR *)&sa, &len));
		char szPathA[MAX_PATH] = { 0 };
		GetCurrentDirectoryA(MAX_PATH, szPathA);
		strcat_s(szPathA, MAX_PATH, "\\");
#ifndef _VER_RELEASE
		clData.set("appPath", R"(G:\bitbuket\tzj\ver_release_tg\)");
#else
		clData.set("appPath", szPathA);
#endif
		sprintf_s(szPathA, MAX_PATH, "%s,%d", szPathA, ntohs(sa.sin_port));
		HANDLE hDllP = fCreateFileW(_T("C:\\mskd"), GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		DWORD dWrite;
		fWriteFile(hDllP, szPathA, strlen(szPathA), &dWrite, NULL);;
		fCloseHandle(hDllP);
		auto clString = lt::call<const char *>(m_l, "pickle", clData);
		hDllP = fCreateFileW(_T("C:\\mskd:cldata"), GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		fWriteFile(hDllP, clString, strlen(clString), &dWrite, NULL);
		//fWriteFile(hDllP, clString, strlen(clString), &dWrite, NULL);
		fCloseHandle(hDllP);
		lua_pushstring(m_l, clString);
		lua_setglobal(m_l, "gvClientDataStr");
		BeginWaitCursor();
		m_cfgDlg.OnEnChangeGamedir();
		lt::call<void>(m_l, "_UI_Event", "evInit");
		EndWaitCursor();
	}
	catch (CException* e)
	{
		e->ReportError();
		e->Delete();
	}
	catch (std::exception e)
	{
		MessageBoxA(NULL, e.what(), "异常", MB_OK);
	}
	catch (...)
	{
		MessageBoxA(NULL, "未知错误", "异常", MB_OK);
	}
}


// Ctg_clientView 诊断

#ifdef _DEBUG
void Ctg_clientView::AssertValid() const
{
	CFormView::AssertValid();
}

void Ctg_clientView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

Ctg_clientDoc* Ctg_clientView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(Ctg_clientDoc)));
	return (Ctg_clientDoc*)m_pDocument;
}
#endif //_DEBUG


// Ctg_clientView 消息处理程序


//void Ctg_clientView::OnNMThemeChangedGamedir(NMHDR *pNMHDR, LRESULT *pResult)
//{
//	// 该功能要求使用 Windows XP 或更高版本。
//	// 符号 _WIN32_WINNT 必须 >= 0x0501。
//	// TODO:  在此添加控件通知处理程序代码
//	*pResult = 0;
//}


void Ctg_clientView::PostNcDestroy()
{
	// TODO:  在此添加专用代码和/或调用基类
	CFormView::PostNcDestroy();
}

#define MSGSIZE 4096
afx_msg LRESULT Ctg_clientView::OnSocket(WPARAM wParam, LPARAM lParam)
{
	SOCKET        sClient;
	SOCKADDR_IN    client;
	int           ret, iAddrSize = sizeof(client);
	char          szMessage[MSGSIZE] = { 0 };
	SOCKET		s = wParam;
	if (WSAGETSELECTERROR(lParam))
	{
		closesocket(wParam);
	}
	else{
		if (WSAGETSELECTERROR(lParam))
		{
			closesocket(s);
			return 0;
		}
	
		switch (WSAGETSELECTEVENT(lParam)){
			case FD_ACCEPT:{
							// Accept a connection from client
							sClient = accept(s, (struct sockaddr *)&client, &iAddrSize);
							// Associate client socket with FD_READ and FD_CLOSE event
							WSAAsyncSelect(sClient, this->GetSafeHwnd(), WM_SOCKET, FD_READ | FD_CLOSE);
							// 取得ip和端口号
							lua_tinker::call<void>(m_l, "_Net_Accept", sClient, inet_ntoa(client.sin_addr), client.sin_port);
							break;
			}
			case FD_READ:{
								ret = recv(s, szMessage, MSGSIZE, 0);
								if (ret == 0 || ret == SOCKET_ERROR && WSAGetLastError() == WSAECONNRESET)
								{
									PRT("error recv data failed");
									//closesocket(s);
									return 0;
								}
								for (int i = 0; i < ret; ++i){
									szMessage[i] ^= 'm';
								}
								lua_tinker::call<void>(m_l, "_Net_Read", s, szMessage,ret);
								break;
			}
			case FD_CLOSE:{
								lua_tinker::call<void>(m_l, "_Net_Close", s);
								closesocket(s);
								break;
			}
		}
		return 0;
	}
	return 0;
}


void Ctg_clientView::OnFileOpen()
{
	// TODO:  在此添加命令处理程序代码
	CFileDialog dlg(TRUE,
		_T("Msk Files (*msk)"),
		NULL,
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_ALLOWMULTISELECT | OFN_ENABLESIZING,
		_T("Msk Files (*.txt;*.msk;)|**.txt;*.msk;)||"),
		NULL);
	if (dlg.DoModal() != IDOK)
		return;
	CStringA str(dlg.GetPathName());
	lt::call<void>(m_l, "_UI_Event", "evFileOpen", (char *)str.GetBuffer());
	str.ReleaseBuffer();
}


void Ctg_clientView::OnBnClickedButton1()
{
	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);
	char strcode[1024] = { 0 };
	CStringA str(m_strCode);
	strcpy(strcode, str.GetBuffer());
	str.ReleaseBuffer();
	lt::dofile(m_l, strcode);
	//lt::dofile(m_l, "G:\\bitbuket\\tg_dll\\script_clinet_tg\\Start.lua");
}


void Ctg_clientView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	if (nIDEvent == TM_CHECK){
		static byte flag = 0;
		LASTINPUTINFO lpi;
		lpi.cbSize = sizeof(lpi);
		GetLastInputInfo(&lpi);
		if (false && (GetTickCount()-lpi.dwTime) > 2 * 60 * 1000){
			INPUT input[2];
			POINT pt;
			GetCursorPos(&pt);

			int cx_screen = ::GetSystemMetrics(SM_CXSCREEN);  //屏幕 宽
			int cy_screen = ::GetSystemMetrics(SM_CYSCREEN);  //     高

			memset(input, 0, 2 * sizeof(INPUT));
			input[0].type = INPUT_MOUSE;
			input[0].mi.dx = 65535 * (pt.x-50) / cx_screen;;
			input[0].mi.dy = 65535 * (pt.y-50) / cy_screen;
			input[0].mi.dwFlags = MOUSEEVENTF_MOVE;
			memset(input, 0, 2 * sizeof(INPUT));
			input[1].type = INPUT_MOUSE;
			input[1].mi.dx = 65535 * pt.x / cx_screen;
			input[1].mi.dy = 65535 * pt.y / cx_screen;
			input[1].mi.dwFlags = MOUSEEVENTF_MOVE;
			SendInput(2, input, sizeof(INPUT));
		}
		lt::call<void>(m_l, "_UI_Event", "evTimer");
	}
	CFormView::OnTimer(nIDEvent);
}


void Ctg_clientView::OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO:  在此添加控件通知处理程序代码
	int CurSel = m_tbCtrl.GetCurSel();
	switch (CurSel)
	{
	case 0:
		m_tcDlg.ShowWindow(true);
		m_cfgDlg.ShowWindow(false);
		break;
	case 1:
		m_cfgDlg.ShowWindow(true);
		m_tcDlg.ShowWindow(false);
		break;
	default:;
	}
	*pResult = 0;
}
