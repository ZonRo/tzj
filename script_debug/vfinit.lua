package.path = [[G:\bitbuket\tzj\ver_release_tg\lua\?.lua;G:\bitbuket\tzj\ver_release_tg\脚本\?.lua;]]
package.cpath = [[G:\bitbuket\tzj\ver_release_tg\clibs\?.dll;]]
require "std"
require "base64"
require "des56"
local http = require "socket.http"
local socket = require("socket")
local gvHost = false and "127.0.0.1" or "23.239.198.7"
local gvPort = 10001
local serverVertions,oldServerVertions,serverTable
function print(...)
	local str = ""
	local t= {...}
	for i=1,#t do
		str = str..tostring(t[i]).." "
	end
	MskPrint(str)
end

local function ServerAssert(v,err,f,...)
	if v then return v end
	if f then f(...) end
	error({result = 'failed',data = err},0)
end

function ParseConfig(data)
	local block,key,value,source
	local t = {}
	local line
	local lines = data:split('\n')
	for i=1, #lines do
		line = lines[i]
		block = line:match'%[(.+)%]'
		if block then
			source = block
			t[block] = {}
		elseif line ~= "" then
			key,value = line:match("(%P+)%s*=%s*(.*)")
			if key then t[source][key] = value end
		end
	end
	return t
end

function GetServerConfig()
	if serverVertions then return end
	serverVertions = {}
	local data,err = http.request('http://launcher.tzj.iwgame.com//ServerList.txt')
	if data then
		local cfg = ParseConfig(data)
		if not cfg.Telecom then return end
		serverTable = {}
		local idx
		local snum = 0
		for k,v in pairs(cfg.Telecom) do
			idx = v:find(',')
			idx = v:sub(1,idx-1)
			idx = tonumber(idx)
			serverTable[idx] = k
			serverVertions[idx] = v:match("%d-,%d-,%d-,.-,(.+),")
			snum = snum + 1
		end
		for i=0,snum-1 do
			AddServer(serverTable[i])
		end
	end
end

local function ConnectServer(sdata)
	GetServerConfig()
	if sdata.serveridx and sdata.serveridx ~= -1 then
		sdata.ver = serverVertions[sdata.serveridx]
		gvServerIdx = sdata.serveridx
		gvServerName = serverTable[gvServerIdx]
		gvGameVer = sdata.ver
	else
		sdata.ver = nil
	end
	ServerAssert(sdata.cmd ~= 'log' or sdata.ver ~= nil,'没有选择大区')
	ServerAssert(nil ~= sdata.user:match("^[^']+$") and #sdata.user > 0,"输入单引号(')以外的任意字符")
	ServerAssert(nil ~= sdata.pwd:match("^[^']+$") and #sdata.pwd > 0,"输入单引号(')以外的任意字符")
	local c = ServerAssert(socket.connect(gvHost, gvPort),'连接服务器失败')
	local curHost,curPort = c:getpeername()
	ServerAssert(curHost==gvHost and curPort==gvPort,'连接服务器失败 '..curHost..' '..gvHost..' '..curPort..' '..gvPort)
	c:settimeout(5)
	sdata = pickle(sdata)
	sdata = base64.encode(sdata)..'\n'
	ServerAssert(c:send(sdata),'尝试登录失败',c.close,c)
	local data = ServerAssert(c:receive(),'无法接受信息',c.close,c)
	c:close()
	data = base64.decode(data)
	local ret = eval(data)
	gtServerData = ret
	if ret.data.sig then
		gvSigData = ret.data.sig
	end
	return ret
end
function AskData(sSata)
	local ret,err = pcall(ConnectServer,sSata)
	if not ret and type(err) == 'string' then
		err = {result = 'failed',data = "脚本错误:"..err}
	end
	--print('ret',ret,err)
	return err
end
require "mskco"
GetServerConfig()