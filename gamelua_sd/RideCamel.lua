----------------------------HG------------------------------

STake_Start("RideCamel_HG_move_004","RideCamel_HG_move_004")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

    Sound(0.302,"Pc_Horse_runnoise_000")
    
STake_Start("RideCamel_HG_wait_000","RideCamel_HG_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()    
    
STake_Start("RideCamel_HG_shangma","RideCamel_HG_wait_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.010,"Pc_Horse_up")
    
    SkeletonMod(0.0, 0.0)
    
----------------------------SG------------------------------

STake_Start("RideCamel_SG_move_004","RideCamel_SG_move_004")
    BlendMode(1)
    BlendTime(0.2)
    Loop()  
    
    
STake_Start("RideCamel_SG_wait_000","RideCamel_SG_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()      
    
STake_Start("RideCamel_SG_shangma","RideCamel_SG_wait_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.010,"Pc_Horse_up")
    
    SkeletonMod(0.0, 0.0)
    