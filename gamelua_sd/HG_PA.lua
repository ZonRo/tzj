GroupBegin("CPB")

--HG_PA--
STake_Start("HG_PA_attack_000","HG_PA_attack_000")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_att")     
    Priority(0,"5")   
  
    MustPlay(0.00,0.296)  
    Soft(0.296,0.8)
    
		
  	AttackStart()
  	Hurt(0.216,"hurt_11")

  	Sound(0.155,"HG_GB_PA_Attack002")  	  	
  	HurtSound(0.216,"PA_Hit_002") 

    DirFx(0.216,"HG_GB_PA_hit_000",25,1)

STake_Start("HG_PA_attack_001","HG_PA_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_att")     
    Priority(0,"5")   
  
    MustPlay(0.00,0.280)  
    Soft(0.280,0.8)
    
		
  	AttackStart()
  	Hurt(0.224,"hurt_11")

  	Sound(0.195,"HG_GB_PA_Attack002")  	  	
  	HurtSound(0.224,"PA_Hit_002") 

    DirFx(0.224,"HG_GB_PA_hit_000",25,1)

STake_Start("HG_PA_attack_002","HG_PA_attack_002")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_att")     
    Priority(0,"5") 
        
    MustPlay(0.00,0.500)  
  		
  	AttackStart()
  	Hurt(0.160,"hurt_21")

  	Sound(0.100,"HG_GB_PA_Attack001")  	  	
  	HurtSound(0.160,"PA_Hit_001") 

    DirFx(0.160,"HG_GB_PA_hit_001",25,1)
    

STake_Start("HG_PA_def_000","HG_PA_def_000")
    BlendMode(0)
    BlendTime(0.2)
    
STake_Start("HG_PA_dodge_000","HG_PA_dodge_000")
    BlendMode(0)
    BlendTime(0.2)
    
STake_Start("HG_PA_hit","HG_PA_hit")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_hurt")
    
STake_Start("HG_PA_move_001","HG_PA_move_001")
    BlendTime(0.2)
    BlendMode(1)
    
    --GroundFx(0.960, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.510, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.960, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.510, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      
    
    GroundSound(0.340,"Sound_step")
    GroundSound(0.900,"Sound_step")
  Loop()               
    
STake_Start("HG_PA_move_003","HG_PA_move_003")
    BlendTime(0.2)
    BlendMode(1)
    
    --GroundFx(0.570, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.930, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.570, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.930, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      
   
    GroundSound(0.400,"Sound_step")
    GroundSound(0.900,"Sound_step")
  Loop()  
    
STake_Start("HG_PA_move_004","HG_PA_move_004")
    BlendTime(0.2)
    BlendMode(1) 
    
    GroundFx(0.710, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.359, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.710, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.359, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      
      
    GroundSound(0.029,"Sound_step")
    GroundSound(0.700,"Sound_step")
  Loop()
  
STake_Start("HG_PA_move_005","HG_PA_move_005")
    BlendTime(0.2)
    BlendMode(1) 
    
    GroundFx(0.710, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.359, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.710, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.359, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      
 
    GroundSound(0.029,"Sound_step")
    GroundSound(0.700,"Sound_step")
  Loop()
    
STake_Start("HG_PA_move_006","HG_PA_move_006")
    BlendTime(0.2)
    BlendMode(1)
        
    GroundFx(0.359, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.710, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.359, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.710, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)          
      
    GroundSound(0.029,"Sound_step")
    GroundSound(0.403,"Sound_step")
  Loop()
    
STake_Start("HG_PA_move_009","HG_PA_move_009")
    BlendTime(0.2)
    BlendMode(0)

  Loop() 
    
STake_Start("HG_PA_move_010","HG_PA_move_010")
    BlendTime(0.2)
    BlendMode(0)
  Loop()
    
STake_Start("HG_PA_wait_000","HG_PA_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("wait") 
    Loop()