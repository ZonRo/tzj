local modname = ...
lout('start loading '.. modname)

module(modname, package.seeall)

local cwindowptr = nil
-------------------------------------------------------------------------------
-- configurable data
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- utility functions
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- global callback
-------------------------------------------------------------------------------
function OnInitWindow(cwndptr)
    local wnd = wndMgr:getWindow('Scenemap_fuben__auto_closebutton__')
    wnd:subscribeEvent('Clicked', 'InstanceMap.OnCloseBtnClicked')

    cwindowptr = tolua.cast(cwndptr, 'CWindow')
end

function OnEnterScene(newMapId)
    -- change chapter img
    lout('InstanceMap.OnEnterScene: '..newMapId)
    local imgName = WorldMap.IMG_CHAPTER_MAP[newMapId]
	lout('InstanceMap.OnEnterScene: Map Image Name ' .. tostring(imgName))
    if imgName ~= nil then
        local wnd = wndMgr:getWindow('Scenemap_fuben/FubenName')
        wnd:setProperty('Image', imgName)
    end
end


function OnCloseBtnClicked(args)
    cwindowptr:Hide()
end

-------------------------------------------------------------------------------
-- init & fini callback
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- entry point
-------------------------------------------------------------------------------
lout('end loading '.. modname)
    