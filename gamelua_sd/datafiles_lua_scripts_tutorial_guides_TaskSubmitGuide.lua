
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame 
	
	self.upIndicator = nil  	
	self.eventConnection = nil
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	-- Listen to Task Submit Event
	self.eventConnection = gameEventMgr:subscribeEvent('EventTaskSubmitted', self.OnEvent, self)
	
	-- Create Indicator
	self.upIndicator = ui.createFrame('UpIndicatorFrame')
	if self.guideCfg.IndicatorText then
		self.upIndicator.pWindow:setText(self.guideCfg.IndicatorText)
	end
		
	-- Attach to SubmitTask Window
	self:TryAttachWindow()
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	if self.eventConnection then
		self.eventConnection:disconnect()
		self.eventConnection = nil
	end
	
	if self.upIndicator then
		ui.destroyFrame(self.upIndicator)
		self.upIndicator = nil
	end
end

function TryAttachWindow(self)
	if self.windowAttached then 
		return
	end
	
	local dialogBtn = InitWindowPtr('Root/SubmitWnd/CompleteQuestButton')
	if not dialogBtn or not dialogBtn:isVisible() then
		return
	end
	
	self.upIndicator:Show()
	UiUtility.AttachWndToWnd(self.upIndicator.pWindow, dialogBtn, UiUtility.PUT_BOTTOM)
		
	-- Add Animation
	local animName = guideMgr.indicator_animations['UP']
	SD.WndAnimManager:Instance():CreateAnim(self.upIndicator.pWindow, animName, false, true, true)
	
	self.windowAttached = true
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	self:TryAttachWindow()
end

function OnEvent(self)
	self.completed = true
end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

