function CastingBar_ProgressMoving(e)
    local we = CEGUI.toWindowEventArgs(e)
    we.window:setProperty("progBar", "set:Progress image:CDBar2_B");
end

function CastingBar_ProgressFinish(e)
    local we = CEGUI.toWindowEventArgs(e)
	lout('finish')
    we.window:setProperty("progBar", "set:Progress image:CDBar2_Y");
end

function CastingBar_ProgressBreak(e)
    local we = CEGUI.toWindowEventArgs(e)
	lout('break')
    we.window:setProperty("progBar", "set:Progress image:CDBar2_R");
end

function PowerAccBar_ProgressMoving(e)
    local we = CEGUI.toWindowEventArgs(e)
    we.window:setProperty("progBar", "set:Progress image:CDBar2_B");
end

function PowerAccBar_ProgressFinish(e)
    local we = CEGUI.toWindowEventArgs(e)
    we.window:setProperty("progBar", "set:Progress image:CDBar2_Y");
end

function PowerAccBar_ProgressBreak(e)
    local we = CEGUI.toWindowEventArgs(e)
    we.window:setProperty("progBar", "set:Progress image:CDBar2_R");
end