local modname = ...
lout('start loading '.. modname)

module(modname, package.seeall)

SelectServer = 
{
	['ServerSelect/ServerFrame/EnterButton'] = 
	{
		Type = "Text",  -- text or image
		Text = "(Enter)",
	}, 
}

ShortcutTable = 
{
	["ServerSelect.layout"] = SelectServer, 
}

SubmitTask = 
{
	['Root/SubmitWnd/Content/ItemRewardGroup1/RewardItem1'] = 
	{
		Type = "Text",  -- text or image
		Text = "F1",
	}, 
	
	['Root/SubmitWnd/Content/ItemRewardGroup1/RewardItem2'] = 
	{
		Type = "Text",  -- text or image
		Text = "F2",
	}, 
	
	['Root/SubmitWnd/Content/ItemRewardGroup1/RewardItem3'] = 
	{
		Type = "Text",  -- text or image
		Text = "F3",
	},
	
	['Root/SubmitWnd/Content/ItemRewardGroup1/RewardItem4'] = 
	{
		Type = "Text",  -- text or image
		Text = "F4",
	}, 
	
	['Root/SubmitWnd/Content/ItemRewardGroup1/RewardItem50'] = 
	{
		Type = "Text",  -- text or image
		Text = "F5",
	}, 
	
	['Root/SubmitWnd/Content/ItemRewardGroup1/RewardItem6'] = 
	{
		Type = "Text",  -- text or image
		Text = "F6",
	}, 
	
	['Root/SubmitWnd/Content/ItemRewardGroup1/RewardItem7'] = 
	{
		Type = "Text",  -- text or image
		Text = "F7",
	},
	
	['Root/SubmitWnd/Content/ItemRewardGroup1/RewardItem8'] = 
	{
		Type = "Text",  -- text or image
		Text = "F8",
	}, 
	
}

ShortcutTable = 
{
	["SubmitTask.layout"] = SubmitTask, 
}

-- SetWidgetText, called only when Frame Window first shown.
-- pWidget:       The widget which needs to set text, type is CEGUI::Window.
-- strWidgetName: Widget name in layout file (without prefix)
-- strLayoutName: Layout file name.
function SetWidgetText(pWidget, strWidgetName, strLayoutName)
	if not pWidget then
		lout('SetWidgetText: pWidget nil')
		return
	end
	if not strWidgetName then 
		lout('SetWidgetText: strWidgetName nil')
		return
	end
	
	if not strLayoutName then 
		lout('SetWidgetText: strLayoutName nil')
		return
	end
	
	local layoutTab = ShortcutTable[strLayoutName]
	if not layoutTab then
		lout('SetWidgetText: not find layout '..strLayoutName)
		return
	end
	
	local widgetTab = layoutTab[strWidgetName]
	if not widgetTab then
		lout('SetWidgetText: not find widget '..strWidgetName ..', '..strLayoutName)
		return
	end
	
	if widgetTab.Type == "Text" then
		pWidget:setText(pWidget:getText():c_str()..widgetTab.Text)
	elseif widgetTab.Type == "Image" then
		
	end
end



lout('end loading '.. modname)