GroupBegin("CPB")

--HG--

STake_Start("HG_DL_DA_wait_000","HG_DL_DA_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("wait") 
    
STake_Start("HG_DL_DA_wait_000_ui","HG_DL_DA_wait_000")
    BlendMode(0)
    BlendTime(0)
    Loop()
    
STake_Start("HG_DL_DA_link_000","HG_DL_DA_link_000")
    BlendMode(2)
    BlendTime(0.2)
    SkeletonMod(0.1, 0.228, "DL_DA_2L")
    SkeletonMod(0.1, 0.228, "DL_DA_2R")

STake_Start("HG_DL_DA_link_001","HG_DL_DA_link_001")
    BlendMode(0)
    BlendTime(0.2)
    SkeletonMod(0.1, 0.728, "DL_DA_1L")
    SkeletonMod(0.1, 0.728, "DL_DA_1R")
    
    
STake_Start("HG_DL_DA_move_001","HG_DL_DA_move_001")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")    
    --Sound(0.601,"PC_step_normal")
  Loop() 
  
    --GroundFx(0.831, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.484, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.831, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.484, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    


STake_Start("HG_DL_DA_move_003","HG_DL_DA_move_003")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")    
    --Sound(0.601,"PC_step_normal")
  Loop() 
  
    --GroundFx(0.831, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.484, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.450, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.831, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    

    

STake_Start("HG_DL_DA_move_004","HG_DL_DA_move_004")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")    
    --Sound(0.601,"PC_step_normal")
  Loop() 
    
    GroundFx(0.432, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.432, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
  

STake_Start("HG_DL_DA_move_005","HG_DL_DA_move_005")
    BlendTime(0.2)
    BlendMode(0)
    BlendPattern("right_left_move")    
    --Sound(0.601,"PC_step_normal")
  Loop() 


    GroundFx(0.432, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.432, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
  

STake_Start("HG_DL_DA_move_006","HG_DL_DA_move_006")
    BlendTime(0.2)
    BlendMode(0)
    BlendPattern("right_left_move")    
    --Sound(0.601,"PC_step_normal")
  Loop() 

    GroundFx(0.432, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.432, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
  

STake_Start("HG_DL_DA_move_009","HG_DL_DA_move_009")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")    
    --Sound(0.601,"PC_step_normal")
  Loop() 

STake_Start("HG_DL_DA_move_010","HG_DL_DA_move_010")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")        
    --Sound(0.601,"PC_step_normal")
  Loop() 
  
  
STake_Start("HG_DL_DA_move_011","HG_DL_DA_move_011")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")    
    --Sound(0.601,"PC_step_normal")
  Loop() 
  
    GroundFx(0.447, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.081, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.447, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.081, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    

    

STake_Start("HG_DL_DA_move_012","HG_DL_DA_move_012")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")    
    --Sound(0.601,"PC_step_normal")
  Loop() 
    
    GroundFx(0.447, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.081, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.447, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.081, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      
  

STake_Start("HG_DL_DA_move_013","HG_DL_DA_move_013")
    BlendTime(0.2)
    BlendMode(0)
    BlendPattern("right_left_move")    
    --Sound(0.601,"PC_step_normal")
  Loop() 


    GroundFx(0.447, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.081, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.447, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.081, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)     
  

STake_Start("HG_DL_DA_move_014","HG_DL_DA_move_014")
    BlendTime(0.2)
    BlendMode(0)
    BlendPattern("right_left_move")    
    --Sound(0.601,"PC_step_normal")
  Loop() 

    GroundFx(0.588, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.441, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(0.85, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.675, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.588, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.441, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(0.85, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.675, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    
STake_Start("HG_DL_DA_dodge_000","HG_DL_DA_dodge_000")
    BlendMode(0)
    BlendTime(0.05)

    --Fx(0.015,"")   

STake_Start("HG_DL_DA_def_000","HG_DL_DA_def_000")
    BlendMode(0)
    BlendTime(0.05)
    --Sound(0.00,"")
    --Fx(0.015,"")   
    
    
STake_Start("HG_DL_DA_hit","HG_DL_DA_hit")
    BlendMode(0)
    BlendTime(0.05)
    --Sound(0.059,"voc_derais_injure_01_short_01")
    BlendPattern("Upper_hurt") 
    
STake_Start("HG_DL_DA_dead","HG_DL_DA_dead")
    BlendMode(0)
    BlendTime(0.2)
    StopChannel(0,2)
    --Sound(0.059,"voc_Male_death")
    
    
    
STake_Start("HG_DL_DA_attack_000","HG_DL_DA_attack_000")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("Upper_att")  

    MustPlay(0.000,0.44)  
    Soft(0.44,1.000)

  	
  	Fx(0.000,"HG_DL_DA_attack_000")
    AttackStart()

  	Hurt(0.30,"hurt_11")

        --Sound(0.000,"")
        --Sound(0.000,"") 
        --HurtSound(0.30,"") 

  	  	
    DirFx(0.30,"HG_DL_DA_attack_hit",0,1)
    


STake_Start("HG_DL_DA_attack_001","HG_DL_DA_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    BlendPattern("Upper_att")  

    MustPlay(0.000,0.423)  
    Soft(0.423,0.900)
 	
  	
  	Fx(0.000,"HG_DL_DA_attack_001")
        
  	AttackStart()

  	Hurt(0.266,"hurt_11")

        --Sound(0.000,"")
        --Sound(0.000,"") 
  	--HurtSound(0.266,"")
  
    DirFx(0.266,"HG_DL_DA_attack_hit",0,1)
    


STake_Start("HG_DL_DA_attack_002","HG_DL_DA_attack_002")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    BlendPattern("Upper_att")      
    
    MustPlay(0.000,0.51)  
  	
  	Fx(0.000,"HG_DL_DA_attack_002")
        
    
  	AttackStart()
	
  	Hurt(0.39,"hurt_21")

        --Sound(0.000,"")
        --Sound(0.000,"") 
        HurtSound(0.39,"")
  	
    DirFx(0.39,"HG_DL_DA_attack_hit",0,1)
    
   

STake_Start("HG_DL_DA_skill_000","HG_DL_DA_skill_000")    --����
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.803)
		--Soft(0.572,0.803)
		
		--AttackStart()
  	--Hurt(0.509,"hurt_11")
  	
  	Charge(0.22,0.05,40,180,11)
  	
  	Fx(0.000,"HG_DL_DA_skill_000_fire")
  	DirFx(0.509,"G_DL_DA_skill_000_hit",25,1)
  	
  	
  	MotionBlur(0.000,0.5,2) 
  	
  	
STake_Start("HG_DL_DA_skill_001","HG_DL_DA_skill_001")    --����
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.6)
		--Soft(0.49,0.67)
		
		AttackStart()
  	Hurt(0.27,"hurt_11")
  	
  	Charge(0.08,0.16,20,0,0)
  	
  	Fx(0.000,"HG_DL_DA_skill_001_fire")
  	DirFx(0.27,"HG_DL_DA_skill_001_hit",25,1)
  	
  	Critical(0.27,0,"HG_DL_DA_skill_009_hit","")
  	
STake_Start("HG_DL_DA_skill_002","HG_DL_DA_skill_002")    --����
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.99)
		--Soft(0.64,0.95)
		
		AttackStart()
  	Hurt(0.3,"hurt_21")
  	
  	Charge(0.27,0.04,20,0,0)
  	
  	Fx(0.000,"HG_DL_DA_skill_002_fire")
  	DirFx(0.3,"HG_DL_DA_skill_002_hit",25,1)
  	
  	Critical(0.3,0,"HG_DL_DA_skill_009_hit","")
  	
  	CameraShake(0.3,"ShakeTimes = 0.05","ShakeMode = 10","ShakeAmplitudeX = 1","ShakeAmplitudeY = 1","ShakeAmplitudeZ = 1")
  	
STake_Start("HG_DL_DA_skill_003","HG_DL_DA_skill_003")    --����
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.79)
		--Soft(0.479,0.904)
		
		AttackStart()
  	Hurt(0.393,"hurt_21")
  	
  	Charge(0.000,0.05,20,0,0)
  	
  	Fx(0.000,"HG_DL_DA_skill_003_fire")
  	DirFx(0.393,"HG_DL_DA_skill_003_hit",15,1,1.1)
  	Critical(0.393,0,"HG_DL_DA_skill_009_hit","")
  	
  	CameraShake(0.393,"ShakeTimes = 0.05","ShakeMode = 10","ShakeAmplitudeX = 1","ShakeAmplitudeY = 1","ShakeAmplitudeZ = 1")
  	
STake_Start("HG_DL_DA_skill_004","HG_DL_DA_skill_004")    --���
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.87)
		--Soft(0.000,0.000)
		
		AttackStart()
  	Hurt(0.331,"hurt_11")
  	
  	Charge(0.303,0.046,20,0,0)
  	
  	Fx(0.000,"HG_DL_DA_skill_004_fire")
  	DirFx(0.331,"HG_DL_DA_skill_004_hit",25,1)
  	
  	Critical(0.331,0,"HG_DL_DA_skill_009_hit","")
  	
  	
STake_Start("HG_DL_DA_skill_005","HG_DL_DA_skill_005")    --ҹ��
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.94)
		--Soft(0.000,0.000)
		
		AttackStart()
  	Hurt(0.37,"hurt_21")
  	
  	Charge(0.20,0.07,20,0,0)
  	
  	Fx(0.000,"HG_DL_DA_skill_005_fire")
  	DirFx(0.37,"HG_DL_DA_skill_005_hit",25,0)
  	
  	Critical(0.37,0,"HG_DL_DA_skill_009_hit","")
  	
  	CameraShake(0.37,"ShakeTimes = 0.05","ShakeMode = 10","ShakeAmplitudeX = 1","ShakeAmplitudeY = 1","ShakeAmplitudeZ = 1")
  	
STake_Start("HG_DL_DA_skill_006","HG_DL_DA_skill_006")    --���ҵ���
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    Loop()
    
    --Hard(0.000,0.000)
		--Soft(0.000,0.000)
		
		AttackStart()
  	Hurt(0.0,"hurt_11")
  	
  	Fx(0.000,"HG_DL_DA_skill_006_fire")
        Fx(0.500,"HG_DL_DA_skill_006_fire")
        Fx(0.2,"HG_DL_DA_skill_006_fire_feidao")
        --Fx(0.8,"HG_DL_DA_skill_006_fire_feidao")

  	DirFx(0.0,"HG_DL_DA_skill_006_hit",25,1)
  	
  	
  	
STake_Start("HG_DL_DA_skill_006_end","HG_DL_DA_wait_000")  --���ҵ�������
		BlendTime(0.2)
    BlendMode(0)     
    Priority(0,"5")  
  	
  	
  	
  	
  	
  	
STake_Start("HG_DL_DA_skill_007","HG_DL_DA_skill_007")    --����ָ·
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,1.28)
		--Soft(0.000,0.000)
		
		AttackStart()
  	Hurt(0.5,"hurt_11")
  	
  	
  	Fx(0.000,"HG_DL_DA_skill_007_fire")
  	DirFx(0.5,"HG_DL_DA_skill_007_hit",25,1)
  	
  	Critical(0.5,0,"HG_DL_DA_skill_009_hit","")
  	
  	
STake_Start("HG_DL_DA_skill_008","HG_DL_DA_skill_008")    --������
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.68)
		--Soft(0.000,0.000)
		
		
  	
  	Fx(0.000,"HG_DL_DA_skill_008")
  	--DirFx(0.000,"",25,1)
  	
  	
STake_Start("HG_DL_DA_skill_009","HG_DL_DA_skill_009")    --������
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    --Hard(0.000,0.000)
		--Soft(0.000,0.000)
		
  	
  	Fx(0.000,"HG_DL_DA_skill_009")
  	DirFx(0.000,"HG_DL_DA_skill_009_hit",25,1)
  	

STake_Start("HG_DL_DA_skill_010","HG_DL_DA_skill_010")    --�黯
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    --Hard(0.000,0.000)
		--Soft(0.000,0.000)
		
		
  	
  	Fx(0.000,"HG_DL_DA_skill_010")
  	--DirFx(0.000,"",25,1)
  	
  	
STake_Start("HG_DL_DA_skill_011","HG_DL_DA_skill_011")    --���絶��
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    Loop()
    
    
    --Hard(0.000,0.000)
		
		
		AttackStart()
  	Hurt(0.0,"hurt_11")
  	
  	
  	Fx(0.000,"HG_DL_DA_skill_011_loop")
    --DragFx(0.00, 1.0, "HG_DL_DA_skill_011_wind", "HG_DL_DA_skill_011_wind_hit", 0, 0, 0, "Reference", "Reference",0,0,0,0,0,1)
  	DirFx(0.0,"HG_DL_DA_skill_011_hit",25,1)
  	
  	Critical(0.0,0,"HG_DL_DA_skill_009_hit","")
  	