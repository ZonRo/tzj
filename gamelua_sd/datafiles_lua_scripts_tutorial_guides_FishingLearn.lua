
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	self.fishingRodItemID = SD.ParseItemID('1_599')
	self.fishingSkillID = 0
	
	self.bRodEquipped = false
	self.bSkillLearned = false
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	if not self.bRodEquipped and SD.GetEquippedItemIDOnPlayer(EQUIP_PART_WEAPON) == self.fishingRodItemID then
		self.bRodEquipped = true
	end
	
	if not self.bSkillLearned and SD.HasLearnedSkill(self.fishingSkillID) then
		self.bSkillLearned = true
	end
	
	if self.bRodEquipped and self.bSkillLearned then
		self.completed = true
	end
end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

