----------------------------------CR female------------------------------------------ 
STake_Start("SG_Arrow_attack_000","SG_Arrow_attack_000")
    BlendMode(0)
    BlendTime(0.0)
    BlendPattern("Arrow_attack")      
    Priority(0,"5")    
    Channel(0, "2") 
    
  	--Hard(0.000,0.916)  	
  	
    AttackStart()
  	Hurt(0.08,"hurt_11")

  	Fx(0.000,"HG_Arrow_attack_000")
        --Fx(0.700,"HG_Arrow_attack_000_b")
  	DragFx(0.08,0.1,"HG_Arrow_attack_tracker","HG_Arrow_attack_000_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0)
  
    SightChange(0.08)

    Sound(0.020,"PC_Gong_Shot")
    Sound(0.530,"PC_LaGong_Duan")
    HurtSound(0.080,"PC_Gong_Hit_002")
    
    
STake_Start("SG_Arrow_attack_000_1","SG_Arrow_attack_000")    --ͨ�þѻ�
    BlendMode(0)
    BlendTime(0.0)
    BlendPattern("Arrow_attack")      
    Priority(0,"5")    
    Channel(0, "2") 
    
  	
    AttackStart()
  	Hurt(0.08,"hurt_11")

  	Fx(0.000,"HG_Arrow_attack_000")
  
    SightChange(0.08)

    Sound(0.020,"PC_Gong_Shot")
    Sound(0.530,"PC_LaGong_Duan")
    HurtSound(0.080,"PC_Gong_Hit_002")    

  	DragFx(0.08,0.1,"HG_Arrow_Juji_tracker","HG_Arrow_attack_000_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0)

    CameraShake(0.08,"ShakeTimes = 0.08","ShakeMode = 3","ShakeSpeed = 200","ShakeAmplitudeX = 0","ShakeAmplitudeY = 25","ShakeAmplitudeZ = 35")	  
    

STake_Start("SG_Arrow_attack_000_2","SG_Arrow_attack_000")    --���ִ�ѻ�
    BlendMode(0)
    BlendTime(0.0)
    BlendPattern("Arrow_attack")      
    Priority(0,"5")    
    Channel(0, "2") 
    
  	
    AttackStart()
  	Hurt(0.08,"hurt_11")

  	Fx(0.000,"HG_Arrow_attack_000")
  
    SightChange(0.08)

    Sound(0.020,"PC_Gong_Shot")
    Sound(0.530,"PC_LaGong_Duan")
    HurtSound(0.080,"PC_Gong_Hit_002")    
    
  	DragFx(0.08,0.1,"HG_Arrow_Juji_tracker","HG_Arrow_attack_000_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0)

    CameraShake(0.08,"ShakeTimes = 0.05","ShakeMode = 3","ShakeSpeed = 200","ShakeAmplitudeX = 0","ShakeAmplitudeY = 25","ShakeAmplitudeZ = 35") 
    
    
STake_Start("SG_Arrow_link_000","SG_Arrow_link_000") --�л���
    BlendMode(2)
    BlendTime(0.05)

    BlendPattern("Arrow_attack")      
    Priority(0,"5")    
    Channel(0, "2") 

STake_Start("SG_Arrow_dead","SG_Arrow_dead")
    BlendMode(0)
    BlendTime(0.2)
    
    --Sound(1.280,"T1_Bow_dead_002")
    GroundSound(0.860,"Sound_fall")
    GroundSound(1.360,"Sound_daodi")
	StopChannel(0,2)

STake_Start("SG_Arrow_dead_001","SG_Arrow_dead_001")
    BlendMode(0)
    BlendTime(0.2)
     
    GroundSound(0.426,"Sound_daodi")
    
STake_Start("SG_Arrow_emo_000","SG_Arrow_emo_000")
    BlendMode(0)
    BlendTime(0.2)    
    
    
STake_Start("SG_Arrow_emo_001","SG_Arrow_emo_001")
    BlendMode(0)
    BlendTime(0.2)    
        

STake_Start("SG_Arrow_hit","SG_Arrow_hit")
    BlendMode(0)
    BlendTime(0.05)

    BlendPattern("Upper_hurt")  
    

STake_Start("SG_Arrow_hit_000","SG_Arrow_hit_000")
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"1")
    Priority(0.300,"5")

    Charge(0.000,0.300,30,0,5)

    GroundSound(0.090,"Sound_step")
    GroundSound(0.250,"Sound_step")
    GroundSound(0.680,"Sound_step")
    GroundSound(0.960,"Sound_step")

STake_Start("SG_Arrow_hit_001","SG_Arrow_hit_001")
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"1")
    Priority(0.300,"5")

    Charge(0.000,0.300,30,0,5)

    GroundSound(0.150,"Sound_step")
    GroundSound(0.320,"Sound_step")
    GroundSound(0.700,"Sound_step")   
    
STake_Start("SG_Arrow_hit_002","SG_Arrow_hit_002")
    BlendMode(0)
    BlendTime(0.05)

    Priority(0,"1")
    Priority(0.300,"5")

    Charge(0.000,0.300,30,0,5)

    GroundSound(0.150,"Sound_step")
    GroundSound(0.320,"Sound_step")
    GroundSound(0.700,"Sound_step")  

STake_Start("SG_Arrow_hit_003","SG_Arrow_hit_003")
    BlendMode(0)
    BlendTime(0.05)

    Priority(0,"1")
    Priority(0.300,"5")

    Charge(0.000,0.300,30,0,5)

    GroundSound(0.150,"Sound_step")
    GroundSound(0.320,"Sound_step")
    GroundSound(0.700,"Sound_step")  

STake_Start("SG_Arrow_hit_005","SG_Arrow_hit_005")
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"1")
    Priority(1.470,"5")

    Charge(0.000,0.470,100,0,5,1.2)

    --Sound(0.183,"HG_Hit_005")
    GroundSound(0.433,"Sound_fall")
    GroundSound(1.190,"Sound_jump")
    GroundSound(1.416,"Sound_step")

STake_Start("SG_Arrow_hit_006","SG_Arrow_hit_006")
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"1")
    Priority(1.470,"5")

    Charge(0.000,0.470,100,0,5,1.2)

    --Sound(0.183,"HG_Hit_005")
    GroundSound(0.433,"Sound_fall")
    GroundSound(1.190,"Sound_jump")
    GroundSound(1.416,"Sound_step")

STake_Start("SG_Arrow_hit_007","SG_Arrow_hit_007")
    BlendMode(0)
    BlendTime(0.05)

    Priority(0,"1")
    Priority(1.470,"5")

    Charge(0.000,0.470,100,0,5,1.2)

    --Sound(0.183,"HG_Hit_005")
    GroundSound(0.433,"Sound_fall")
    GroundSound(1.190,"Sound_jump")
    GroundSound(1.416,"Sound_step")

STake_Start("SG_Arrow_hit_008","SG_Arrow_hit_008")
    BlendMode(0)
    BlendTime(0.05)

    Priority(0,"1")
    Priority(1.470,"5")

    Charge(0.000,0.470,100,0,5,1.2)

    --Sound(0.183,"HG_Hit_005")
    GroundSound(0.433,"Sound_fall")
    GroundSound(1.190,"Sound_jump")
    GroundSound(1.416,"Sound_step")

STake_Start("SG_Arrow_hit_009","SG_Arrow_hit_009(loop6-66)")
    BlendMode(0)
    BlendTime(0.05)
    Loop(0.2,2.2)
    --BlendPattern("Upper_hurt") 
    
    
STake_Start("SG_Arrow_move_001","SG_Arrow_move_001")
    BlendMode(1)
    BlendTime(0.2)
    Loop()
    BlendPattern("Arrow_move")         

    --GroundFx(0.149, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.616, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.155, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.614, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    GroundSound(0.398,"Sound_step")
    GroundSound(0.753,"Sound_step")

STake_Start("SG_Arrow_move_001_L","SG_Arrow_move_001_L")
    BlendMode(1)
    BlendTime(0.2)
    Loop()
    BlendPattern("Arrow_move")         

    --GroundFx(0.149, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.616, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.155, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.614, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    GroundSound(0.398,"Sound_step")
    GroundSound(0.753,"Sound_step")

STake_Start("SG_Arrow_move_001_R","SG_Arrow_move_001_R")
    BlendMode(1)
    BlendTime(0.2)
    Loop()
    BlendPattern("Arrow_move")         

    --GroundFx(0.149, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.616, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.155, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.614, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    GroundSound(0.398,"Sound_step")
    GroundSound(0.753,"Sound_step")

STake_Start("SG_Arrow_move_004","SG_Arrow_move_004")
    BlendMode(1)
    BlendTime(0.2)
    Loop()
    BlendPattern("Arrow_move")     

    GroundFx(0.439, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.439, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)           
    
    GroundSound(0.395,"Sound_step")
    GroundSound(0.718,"Sound_step")    
    
STake_Start("SG_Arrow_move_004_R","SG_Arrow_move_004_R")
    BlendMode(1)
    BlendTime(0.2)
    Loop()
    BlendPattern("Arrow_move")     

    GroundFx(0.439, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.439, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)           

    GroundSound(0.395,"Sound_step")
    GroundSound(0.718,"Sound_step")        

STake_Start("SG_Arrow_move_004_L","SG_Arrow_move_004_L")
    BlendMode(1)
    BlendTime(0.2)
    Loop()
    BlendPattern("Arrow_move")     

    GroundFx(0.439, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.439, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)         
    
    GroundSound(0.395,"Sound_step")
    GroundSound(0.718,"Sound_step")    

STake_Start("SG_Arrow_move_005","SG_Arrow_move_005")
    BlendMode(1)
    BlendTime(0.2)
    Loop()
    BlendPattern("Arrow_move")    

    GroundFx(0.439, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.439, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)           
     
    GroundSound(0.168,"Sound_step")
    GroundSound(0.557,"Sound_step") 
    
STake_Start("SG_Arrow_move_006","SG_Arrow_move_006")
    BlendMode(1)
    BlendTime(0.2)
    Loop()
    BlendPattern("Arrow_move")         
    
    GroundFx(0.439, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.439, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)           

    GroundSound(0.080,"Sound_step")
    GroundSound(0.432,"Sound_step")     

STake_Start("SG_Arrow_move_012","SG_Arrow_move_012")
    BlendMode(1)
    BlendTime(0.2)
    Loop()
    BlendPattern("Arrow_move")    

    GroundSound(0.470,"Sound_step")
    GroundSound(0.970,"Sound_step")  

STake_Start("SG_Arrow_move_013","SG_Arrow_move_013")
    BlendMode(1)
    BlendTime(0.2)
    Loop()
    BlendPattern("Arrow_move")    

    GroundSound(0.470,"Sound_step")
    GroundSound(0.970,"Sound_step")  

STake_Start("SG_Arrow_move_014_loop","SG_Arrow_move_014(loop26-51)")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    FrameTime(0.866,1.7)
    BlendPattern("Arrow_jump")    

   
STake_Start("SG_Arrow_move_014_jump","SG_Arrow_move_014(loop26-51)")
    BlendMode(0)
    BlendTime(0.2)
    Loop(0.866,1.7)
    BlendPattern("Arrow_jump")      

    GroundSound(0.010,"Sound_jump")
    --Sound(0.010,"char_05_cloth_flap_01")   

STake_Start("SG_Arrow_move_014_down","SG_Arrow_move_014(loop26-51)")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.7,2.4)
    BlendPattern("Arrow_jump")   

    GroundSound(2.000,"Sound_fall")

STake_Start("SG_Arrow_move_015_loop","SG_Arrow_move_015")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    FrameTime(0.866,1.7)
    BlendPattern("Arrow_jump") 

    GroundSound(0.010,"Sound_jump")
    --Sound(0.010,"char_05_cloth_flap_01")

STake_Start("SG_Arrow_move_015_jump","SG_Arrow_move_015")
    BlendMode(0)
    BlendTime(0.2)
    Loop(0.866,1.7)
    BlendPattern("Arrow_jump")    


STake_Start("SG_Arrow_move_015_down","SG_Arrow_move_015")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.7,2.4)
    BlendPattern("Arrow_jump")    

    GroundSound(1.800,"Sound_fall")

STake_Start("SG_Arrow_move_016_loop","SG_Arrow_move_016")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    FrameTime(0.866,1.7)
    BlendPattern("Arrow_jump") 

    GroundSound(0.010,"Sound_jump")
    --Sound(0.010,"char_05_cloth_flap_01")    
    
STake_Start("SG_Arrow_move_016_jump","SG_Arrow_move_016")
    BlendMode(0)
    BlendTime(0.2)
    Loop(0.866,1.7)
    BlendPattern("Arrow_jump")    


STake_Start("SG_Arrow_move_016_down","SG_Arrow_move_016")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.7,2.4)
    BlendPattern("Arrow_jump")   

    GroundSound(1.800,"Sound_fall")

STake_Start("SG_Arrow_move_017_loop","SG_Arrow_move_017")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    FrameTime(0.866,1.7)
    BlendPattern("Arrow_jump") 

    GroundSound(0.010,"Sound_jump")
    --Sound(0.010,"char_05_cloth_flap_01") 

STake_Start("SG_Arrow_move_017_jump","SG_Arrow_move_017")
    BlendMode(0)
    BlendTime(0.2)
    Loop(0.866,1.7)
    BlendPattern("Arrow_jump")    


STake_Start("SG_Arrow_move_017_down","SG_Arrow_move_017")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.7,2.4)
    BlendPattern("Arrow_jump")  

    GroundSound(1.800,"Sound_fall")

STake_Start("SG_Arrow_move_018_loop","SG_Arrow_move_018")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    FrameTime(0.866,1.7)
    BlendPattern("Arrow_jump") 

    GroundSound(0.010,"Sound_jump")
    --Sound(0.010,"char_05_cloth_flap_01") 

STake_Start("SG_Arrow_move_018_jump","SG_Arrow_move_018")
    BlendMode(0)
    BlendTime(0.2)
    Loop(0.866,1.7)
    BlendPattern("Arrow_jump")    


STake_Start("SG_Arrow_move_018_down","SG_Arrow_move_018")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.7,2.4)
    BlendPattern("Arrow_jump")  

    GroundSound(1.800,"Sound_fall")
    
STake_Start("SG_Arrow_skill_000","SG_Arrow_skill_000")   --����뷢
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")   
    Priority(0,"5")     
    Channel(0, "0") 
    
  	Hard(0.000,0.736)  	
  	
    AttackStart()
  	Hurt(0.219,"hurt_21")


  	Fx(0.000,"HG_Arrow_skill_000_fire")


  	DragFx(0.219,0.2,"Bow_qiyan_attack_000_tracker","HG_Arrow_skill_000_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1)

    SightChange(0.219)

    --Sound(0.001,"PC_Gong_Skill_000")
    HurtSound(0.219,"BTS_FAN_Skill_005_Hit")

STake_Start("SG_Arrow_skill_004","SG_Arrow_skill_004")  --�����չ�1�� ������
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")   
    Priority(0,"5")     
    Channel(0, "0") 
    
  	Hard(0.000,0.912)  	
  	
    AttackStart()
  	Hurt(0.352,"hurt_21")

  	
  	Fx(0.000,"HG_Arrow_skill_001_fire")
  	--Fx(0.000,"HG_Arrow_skill_004_fire")

    --DragFx(0.512,0.3,"HG_Arrow_skill_004_trail","HG_Arrow_skill_004_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1)
	DragFx(0.212,0.15,"HG_Arrow_skill_007_trail","HG_Arrow_skill_007_hit",0,0,0,"LeftHand","Spine1",0,0,0,0,0,1,0,0)
	
	--(time, flytime, fxname, hitfx, hittime, mode, overrange, bone, hitbone, fadein, fadeout, r, a, b, loop, shift,dirmode,fxscale,hitfxscale,nonefxname,nonfxscale,nonehitfxname,nonehitfxscale,nonehitx,nonehity,nonehitz)

    SightChange(0.432)

    Sound(0.050,"PC_LaGong_Duan")
    Sound(0.448,"BTS_FAN_Skill_004")
    HurtSound(0.512,"BTS_FAN_Hit_003")    

STake_Start("SG_Arrow_skill_001","SG_Arrow_skill_001")  --�����չ�2 ����
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")   
    Priority(0,"5")     
    Channel(0, "0") 
    
    
  	Hard(0.000,0.443)  	
  	
    AttackStart()
  	Hurt(0.230,"hurt_21")
  	Hurt(0.356,"hurt_21")
  	
  	Fx(0.000,"HG_Arrow_skill_001_fire")

  	--DragFx(0.300,0.5,"HG_Arrow_skill_001_tracker","HG_Arrow_skill_001_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1)
  	--DragFx(0.616,0.5,"HG_Arrow_skill_001_tracker","HG_Arrow_skill_001_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1)
  	--DragFx(0.412,0.3,"HG_Arrow_skill_004_trail","HG_Arrow_skill_004_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1) --��
    DragFx(0.112,0.15,"HG_Arrow_skill_007_trail","HG_Arrow_skill_007_hit",0,0,0,"LeftHand","Spine1",0,0,0,0,0,1,0,0,1,2) 
	DragFx(0.252,0.15,"HG_Arrow_skill_007_trail","HG_Arrow_skill_007_hit",0,0,0,"LeftHand","Spine1",0,0,0,0,0,1,0,0,1,2) 
	--ϸ

    SightChange(0.050)
    SightChange(0.316)

    Sound(0.063,"PC_Gong_Shot")
    Sound(0.278,"PC_Gong_Shot")
    HurtSound(0.100,"PC_Gong_hit")
    HurtSound(0.316,"PC_Gong_hit")

STake_Start("SG_Arrow_skill_002","SG_Arrow_skill_002")  --�����չ�3 ��
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")   
    Priority(0,"5")     
    Channel(0, "0") 

    
  	Hard(0.000,1.127)  	
  	
    AttackStart()
  	Hurt(0.610,"hurt_11")

  	--Fx(0.000,"HG_Arrow_skill_001_fire")
  	--Fx(0.000,"HG_Arrow_skill_002_fire")
  	Fx(0.000,"HG_Arrow_skill_006_fire")

  	--DragFx(0.610,0.5,"HG_Arrow_skill_002_tracker","HG_Arrow_skill_002_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1)

    SightChange(0.610)

    Sound(0.050,"HG_SL_ST_Skill_001")
    HurtSound(0.610,"BTS_FAN_Skill_004_Hit")
    GroundSound(1.080,"Sound_fall")
    GroundSound(0.140,"Sound_jump")


STake_Start("SG_Arrow_skill_003","SG_Arrow_skill_003")  --������
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")   
    Priority(0,"5")     
    --Channel(0, "0") 
    
    
  	--Hard(0.000,0.858)  	
  	
    AttackStart()
  	Hurt(0.385,"hurt_41")

  	Fx(0.00,"HG_Arrow_skill_003_fire")

    DirFx(0.385,"HG_Arrow_skill_003_hit",0,1)  
 
    CameraShake(0.385,"ShakeTimes = 0.05","ShakeMode = 10","ShakeAmplitudeX = 1","ShakeAmplitudeY = 1","ShakeAmplitudeZ = 1")

    Sound(0.050,"BTS_FAN_skill_001")
    HurtSound(0.385,"PA_Hit_004")
    GroundSound(0.792,"Sound_fall")
    GroundSound(0.242,"Sound_jump")  	
    
    
STake_Start("SG_Arrow_skill_009","SG_Arrow_skill_006")  --�����չ���2��-����
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")   
    Priority(0,"5")     
    Channel(0, "0") 
  

    
  	Hard(0.000,0.750)  	
  	
    AttackStart()
  	Hurt(0.465,"hurt_31")

  	--Fx(0.000,"HG_Arrow_skill_006_fire")
  
   	--DragFx(0.465,0.5,"Bow_qiyan_attack_000_tracker","HG_Arrow_skill_006_hit",0,0,0,"LeftHand","Spine1",0,0,0,0,0,1)
    DragFx(0.512,0.3,"HG_Arrow_skill_004_trail","HG_Arrow_skill_004_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1) 
    SightChange(0.465)    

    Sound(0.001,"PC_Gong_Skill_000")
    HurtSound(0.219,"BTS_FAN_Skill_005_Hit")        
    
    
 


STake_Start("SG_Arrow_skill_005","SG_Arrow_skill_005")  --�����������¼�
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")   
    Priority(0,"5")     
    Channel(0, "0") 
    
    
  	Hard(0.000,0.633)  	
  	
    AttackStart()
  	Hurt(0.050,"hurt_11")
  	Hurt(0.139,"hurt_11")
  	Hurt(0.278,"hurt_11")
  	Hurt(0.329,"hurt_11")
  	Hurt(0.405,"hurt_11") 
  	Hurt(0.468,"hurt_11") 
  	
  	Fx(0.000,"HG_Arrow_skill_005_fire")

  	DragFx(0.050,0.5,"HG_Arrow_skill_005_tracker","HG_Arrow_skill_005_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 50, -30, 0)
  	DragFx(0.139,0.5,"HG_Arrow_skill_005_tracker","HG_Arrow_skill_005_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 50, 30, 0)
  	DragFx(0.278,0.5,"HG_Arrow_skill_005_tracker","HG_Arrow_skill_005_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 50, -30, 0)
  	DragFx(0.329,0.5,"HG_Arrow_skill_005_tracker","HG_Arrow_skill_005_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 50, 30, 0)
  	DragFx(0.405,0.5,"HG_Arrow_skill_005_tracker","HG_Arrow_skill_005_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 50, -30, 0)
  	DragFx(0.468,0.5,"HG_Arrow_skill_005_tracker","HG_Arrow_skill_005_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 50, 30, 0)


    SightChange(0.050)
    SightChange(0.139)
    SightChange(0.278)
    SightChange(0.329)
    SightChange(0.405)
    SightChange(0.468)    

    --Sound(0.050,"BTS_FAN_skill_008_att01")
    --Sound(0.063,"PC_Gong_Shot")
    --Sound(0.139,"PC_Gong_Shot")
    --Sound(0.202,"PC_Gong_Shot")
    --Sound(0.316,"PC_Gong_Shot")
    --Sound(0.392,"PC_Gong_Shot")
    --Sound(0.468,"PC_Gong_Shot")
    --Sound(0.531,"PC_Gong_Shot")
    HurtSound(0.050,"PC_Gong_hit")
    HurtSound(0.139,"PC_Gong_hit")
    HurtSound(0.278,"PC_Gong_hit")
    HurtSound(0.329,"PC_Gong_hit")
    HurtSound(0.405,"PC_Gong_hit")
    HurtSound(0.468,"PC_Gong_hit")
    

STake_Start("SG_Arrow_skill_006","SG_Arrow_skill_006")  --����������Ӱ��
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")   
    Priority(0,"5")     
    Channel(0, "0") 
  
    
  	Hard(0.000,0.750)  	
  	
    AttackStart()
  	Hurt(0.465,"hurt_31")

  	Fx(0.000,"HG_Arrow_skill_006_fire")
  
   	DragFx(0.465,0.5,"Bow_qiyan_attack_000_tracker","HG_Arrow_skill_006_hit",0,0,0,"LeftHand","Spine1",0,0,0,0,0,1)
 
    SightChange(0.465)    

    --Sound(0.001,"PC_Gong_Skill_000")
    HurtSound(0.219,"BTS_FAN_Skill_005_Hit")      
    
STake_Start("SG_Arrow_skill_007_loop1","SG_Arrow_skill_007")  --����loop1
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")   
    Priority(0,"5")     
    Channel(0, "0") 
    Fx(0,"SG_QZ_SW_skill_012_loop01")
    Fx(0.01,"Trail01_QZ") 
    Fx(0.234,"Trail01_QZ") 
    Fx(0.634,"Trail01_QZ") 
  
    FrameTime(0.00,1.50)  
    Loop(1.00,1.50)
  

   	Fx(0.000,"HG_Arrow_skill_007_fire")
    LoopFx(0.01, 300, 0, "HG_Arrow_skill_007_fire","Spine1",0.8)    
    
 
    --SightChange(0.465)    

STake_Start("SG_Arrow_skill_007_loop2","SG_Arrow_skill_007")  --����loop2
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")   
    Priority(0,"5")     
    Channel(0, "0")
    FrameTime(0.8,1.50)  
    Loop(0.801,1.50)
	
    Fx(0.8,"SG_QZ_SW_skill_012_loop01")
    Fx(0.8,"SG_QZ_SW_skill_012_loophit01")  
    Fx(0.801,"Trail01_QZ") 
    Fx(1.034,"Trail01_QZ") 
   	--Fx(0.000,"HG_Arrow_skill_007_fire")
   	Fx(0.81,"HG_BTS_CR_skill_001_hit01")
    LoopFx(0.80001, 300, 0, "HG_Arrow_skill_007_fire","Spine1",1.2)    
    
 
    --SightChange(0.465)    



STake_Start("SG_Arrow_skill_007_loop3","SG_Arrow_skill_007")  --����loop3
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")   
    Priority(0,"5")     
    Channel(0, "0") 
  
    FrameTime(1,1.50)  
    Loop(1.1,1.50)
  

   --Fx(1,"HG_BTS_CR_skill_001_hit01")
  	Fx(0.000,"HG_Arrow_skill_007_fire")
    LoopFx(0.01, 30.0, 0, "HG_Arrow_skill_007_fire","Spine1",0.8)    
    
 
    --SightChange(0.465)  


STake_Start("SG_Arrow_skill_007_att","SG_Arrow_skill_007")  --����attack
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")   
    Priority(0,"5")     
    Channel(0, "0") 
  
    FrameTime(1.50,2.60)  
    
  	Hard(1.50,1.90)  	
  	
    AttackStart()
  	Hurt(0.00,"hurt_31")

  	--Fx(0.000,"HG_Arrow_skill_007_fire")
  	Fx(0.000,"HG_Arrow_skill_001_fire")
  
   	--DragFx(0.00,0.5,"HG_Arrow_skill_007_trail","HG_Arrow_skill_007_hit",0,0,0,"LeftHand","Spine1",0,0,0,0,0,1) -- ϸ
	DragFx(0.00,0.3,"HG_Arrow_skill_004_trail","HG_Arrow_skill_004_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1) -- ��
	Sound(0.001,"PC_LaGong_Duan")
    Sound(0.001,"BTS_FAN_Skill_004")

    --DragFx(0.000,0.5,"HG_Arrow_skill_001_tracker","HG_Arrow_skill_001_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1)
  	--DragFx(0.100,0.5,"HG_Arrow_skill_001_tracker","HG_Arrow_skill_001_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1)
 
    SightChange(0.00)    

    --Charge(0.000,0.400,60,180,4)    
    Charge(0.200,0.600,60,180,4)    



STake_Start("SG_Arrow_skill_008","SG_Arrow_skill_008")  --��Ӱ
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")   
    Priority(0,"5")     
    --Channel(0, "0") 
  
    
  	--Hard(0.000,0.800)  	
  	
    AttackStart()
  	Hurt(0.200,"hurt_21")

  	Fx(0.000,"HG_Arrow_skill_008_fire")
	Sound(0.050,"BTS_FAN_skill_001")
   	DragFx(0.200,0.5,"HG_Arrow_skill_008_tracker","HG_Arrow_skill_008_hit",0,0,0,"LeftHand","Spine1",0,0,0,0,0,1)
 
    SightChange(0.200)    

    Charge(0.200,0.600,300,180,4)    

STake_Start("SG_Arrow_wait_000","SG_Arrow_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("wait")     
    
STake_Start("SG_Arrow_wait_000_ui","SG_Arrow_wait_000")
    BlendMode(0)
    BlendTime(0)
    Loop()

STake_Start("SG_Arrow_wait_000_up","SG_Arrow_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("Arrow_attack")     
    
    LoopFx(0.0, 4.0, 0, "Bow_qiyan_attack_000_tracker","RightHand",1)    


STake_Start("SG_Arrow_wait_000_down","SG_Arrow_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("Arrow_move")     

STake_Start("SG_Arrow_hit_010","SG_Arrow_hit_000")
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"1")
    Priority(0.300,"5")

    BlendPattern("Upper_hurt")  
    Charge(0.000,0.300,60,0,5)

    GroundSound(0.090,"Sound_step")
    GroundSound(0.250,"Sound_step")
    GroundSound(0.680,"Sound_step")
    GroundSound(0.960,"Sound_step")

STake_Start("SG_Arrow_hit_011","SG_Arrow_hit_001")
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"1")
    Priority(0.300,"5")

    BlendPattern("Upper_hurt") 
    Charge(0.000,0.300,60,0,5)

    GroundSound(0.150,"Sound_step")
    GroundSound(0.320,"Sound_step")
    GroundSound(0.700,"Sound_step")
    
STake_Start("SG_Arrow_hit_012","SG_Arrow_hit_002")
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"1")
    Priority(0.300,"5")

    BlendPattern("Upper_hurt")
    Charge(0.000,0.300,60,0,5)     

    GroundSound(0.150,"Sound_step")
    GroundSound(0.320,"Sound_step")
    GroundSound(0.700,"Sound_step")

STake_Start("SG_Arrow_hit_013","SG_Arrow_hit_003")
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"1")
    Priority(0.300,"5")

    BlendPattern("Upper_hurt") 
    Charge(0.000,0.300,60,0,5)

    GroundSound(0.150,"Sound_step")
    GroundSound(0.320,"Sound_step")
    GroundSound(0.700,"Sound_step")
    
STake_Start("SG_Arrow_hit_0053","SG_Arrow_hit_005")
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"1")
    Priority(1.470,"5")

    Charge(0.000,0.470,200,0,5,1.2)

    --Sound(0.183,"HG_Hit_005")
    GroundSound(0.433,"Sound_fall")
    GroundSound(1.190,"Sound_jump")
    GroundSound(1.416,"Sound_step")

STake_Start("SG_Arrow_hit_0063","SG_Arrow_hit_006")
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"1")
    Priority(1.470,"5")

    Charge(0.000,0.470,200,0,5,1.2)

    --Sound(0.183,"HG_Hit_005")
    GroundSound(0.433,"Sound_fall")
    GroundSound(1.190,"Sound_jump")
    GroundSound(1.416,"Sound_step")

STake_Start("SG_Arrow_hit_0073","SG_Arrow_hit_007")
    BlendMode(0)
    BlendTime(0.05)

    Priority(0,"1")
    Priority(1.470,"5")

    Charge(0.000,0.470,200,0,5,1.2)

    --Sound(0.183,"HG_Hit_005")
    GroundSound(0.433,"Sound_fall")
    GroundSound(1.190,"Sound_jump")
    GroundSound(1.416,"Sound_step")

STake_Start("SG_Arrow_hit_0083","SG_Arrow_hit_008")
    BlendMode(0)
    BlendTime(0.05)

    Priority(0,"1")
    Priority(1.470,"5")

    Charge(0.000,0.470,200,0,5,1.2)

    --Sound(0.183,"HG_Hit_005")
    GroundSound(0.433,"Sound_fall")
    GroundSound(1.190,"Sound_jump")
    GroundSound(1.416,"Sound_step")


 STake_Start("SG_Arrow_skill_001_ju","SG_Arrow_skill_004")  --�����չ���1�� ������ ju 
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")   
    Priority(0,"5")     
    Channel(0, "0") 
    
  	Hard(0.000,0.912)  	
  	
    AttackStart()
  	Hurt(0.352,"hurt_21")

  	
  	Fx(0.000,"HG_Arrow_skill_001_fire")
  	--Fx(0.000,"HG_Arrow_skill_004_fire")
  	--Fx(0.000,"HG_Arrow_skill_002_fire")

  	--DragFx(0.162,0.2,"HG_Arrow_skill_004_trail","HG_Arrow_skill_004_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1)
    --DragFx(0.165,0.2,"Bow_qiyan_attack_000_tracker","HG_Arrow_skill_006_hit",0,0,0,"LeftHand","Spine1",0,0,0,0,0,1)
    DragFx(0.112,0.2,"HG_Arrow_skill_007_trail","HG_Arrow_skill_007_hit",0,0,0,"LeftHand","Spine1",0,0,0,0,0,1)
    --SightChange(0.432)

    Sound(0.050,"PC_LaGong_Duan")
    Sound(0.448,"BTS_FAN_Skill_004")
    HurtSound(0.352,"BTS_FAN_Hit_003")       
    
    
STake_Start("SG_Arrow_skill_002_ju","SG_Arrow_skill_001")  --�����չ�2 ���� ju
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")   
    Priority(0,"5")     
    Channel(0, "0") 
    
    
  	Hard(0.000,0.443)  	
  	
    AttackStart()
  	Hurt(0.100,"hurt_21")
  	Hurt(0.316,"hurt_21")
  	
  	Fx(0.000,"HG_Arrow_skill_001_fire")

  	DragFx(0.300,0.5,"HG_Arrow_skill_001_tracker","HG_Arrow_skill_001_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1)
  	DragFx(0.616,0.5,"HG_Arrow_skill_001_tracker","HG_Arrow_skill_001_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1)

    SightChange(0.050)
    SightChange(0.316)

    --Sound(0.063,"PC_Gong_Shot")
    --Sound(0.278,"PC_Gong_Shot")
    HurtSound(0.100,"PC_Gong_hit")
    HurtSound(0.316,"PC_Gong_hit")    

    
------------------------------------------------------------���м���----------------------------------------------------------------------------
------------------------------------------------------------���м���----------------------------------------------------------------------------
------------------------------------------------------------���м���----------------------------------------------------------------------------
 
  STake_Start("SG_Arrow_skill_001_air","SG_Arrow_skill_004")  --�����չ���1�� ������ air
    BlendMode(0)
    BlendTime(0.2)
    --BlendPattern("no_sync")   
    Priority(0,"5")     
    --Channel(0, "0") 
    BlendPattern("air_attack")  
    Channel(0, "2")   

    
  	
    AttackStart()
  	Hurt(0.512,"hurt_21")

  	
  	Fx(0.000,"HG_Arrow_skill_001_fire")
  	--Fx(0.000,"HG_Arrow_skill_004_fire")

  	DragFx(0.512,0.3,"HG_Arrow_skill_004_trail","HG_Arrow_skill_004_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1)

    SightChange(0.432)

    Sound(0.050,"PC_LaGong_Duan")
    Sound(0.448,"BTS_FAN_Skill_004")
    HurtSound(0.512,"BTS_FAN_Hit_003")          
    
    
STake_Start("SG_Arrow_skill_002_air","SG_Arrow_skill_006")  --�����չ���2�� air
    BlendMode(0)
    BlendTime(0.2)
    --BlendPattern("no_sync")   
    Priority(0,"5")     
    --Channel(0, "0") 
    BlendPattern("air_attack")  
    Channel(0, "2")  
  

    
  	
    AttackStart()
  	Hurt(0.465,"hurt_31")

    Fx(0.000,"HG_Arrow_skill_001_fire")
    --Fx(0.000,"HG_Arrow_skill_002_fire")
    
   	--DragFx(0.465,0.5,"Bow_qiyan_attack_000_tracker","HG_Arrow_skill_006_hit",0,0,0,"LeftHand","Spine1",0,0,0,0,0,1)
   	DragFx(0.512,0.3,"HG_Arrow_skill_004_trail","HG_Arrow_skill_004_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1)
 
    SightChange(0.465)    

    Sound(0.001,"PC_Gong_Skill_000")
    HurtSound(0.219,"BTS_FAN_Skill_005_Hit")        
    
       
 
       
STake_Start("SG_Arrow_skill_003_air","SG_Arrow_skill_002")  --�����չ�3 �� air
    BlendMode(0)
    BlendTime(0.2)
    --BlendPattern("no_sync")   
    Priority(0,"5")     
    --Channel(0, "0") 
    BlendPattern("air_attack")  
    Channel(0, "2")  
    
  	
    AttackStart()
  	Hurt(0.610,"hurt_11")

  	--Fx(0.000,"HG_Arrow_skill_001_fire")
  	--Fx(0.000,"HG_Arrow_skill_002_fire")
    Fx(0.000,"HG_Arrow_skill_006_fire")
  	--DragFx(0.610,0.5,"HG_Arrow_skill_002_tracker","HG_Arrow_skill_002_hit", 0, 0, 0, "LeftHand", "Spine1", 0, 0, 0, 0, 0,1)

    SightChange(0.610)

    Sound(0.050,"HG_SL_ST_Skill_001")
    HurtSound(0.610,"BTS_FAN_Skill_004_Hit")
    GroundSound(1.080,"Sound_fall")
    GroundSound(0.140,"Sound_jump")