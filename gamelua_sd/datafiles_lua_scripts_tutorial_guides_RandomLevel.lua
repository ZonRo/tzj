
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local openRandomLevelUI = indicatorFlow.createIndicatorSpec()
	openRandomLevelUI.frameName     = 'openRandomLevelUI'
	openRandomLevelUI.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	openRandomLevelUI.frameText     = '点击随机副本打开界面'
	openRandomLevelUI.triggerWin    = 'MainMenu_Buttons/ButtonFrame/Template_2'
	openRandomLevelUI.attachWin     = 'MainMenu_Buttons/ButtonFrame/Template_2'
	openRandomLevelUI.attachWinRoot = 'MainMenu_Buttons'
	openRandomLevelUI.attachFunc    = nil
	openRandomLevelUI.attachFuncParam = nil
	openRandomLevelUI.priority      = 1
	
	local selectLevel = indicatorFlow.createIndicatorSpec()
	selectLevel.frameName     = 'selectLevel'
	selectLevel.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	selectLevel.frameText     = '勾选魅影石林副本'
	selectLevel.triggerWin    = 'Npc_RandomLevel/BtnBG/ScrollPane/Container/ProgressFrame/CheckBox1_1'
	selectLevel.attachWin     = 'Npc_RandomLevel/BtnBG/ScrollPane/Container/ProgressFrame/CheckBox1_1'
	selectLevel.attachWinRoot = 'Npc_RandomLevel'
	selectLevel.attachFunc    = nil
	selectLevel.attachFuncParam = nil
	selectLevel.priority      = 2
	
	local joinLevel = indicatorFlow.createIndicatorSpec()
	joinLevel.frameName     = 'joinLevel'
	joinLevel.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	joinLevel.frameText     = '点击加入副本队列'
	joinLevel.triggerWin    = 'Npc_RandomLevel/Enter'
	joinLevel.attachWin     = 'Npc_RandomLevel/Enter'
	joinLevel.attachWinRoot = 'Npc_RandomLevel'
	joinLevel.attachFunc    = nil
	joinLevel.attachFuncParam = nil
	joinLevel.priority      = 3
	
	self.indicatorSpecs = {
		openRandomLevelUI, selectLevel, joinLevel
	}
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

