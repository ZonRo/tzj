-------------------------------------------------------------------------------
-- utility functions
-------------------------------------------------------------------------------
function GetPlayersUnit()
    local playerEnt = Utility.GetPlayerEntity()
    return playerEnt:GetUnit()
end

function GetLevelAttribute( nProf, wLevel, strAttribute )
    local stLevelAttr = _G["GetLevelAttributeStruct"](nProf, wLevel)
    return stLevelAttr["m_d"..strAttribute]
end

-- get a std::vector<string>
function CreateStrSet(...)
    local lst = arg
    local vec = std.set_string_:new_local()
    for i=1,#lst do
        vec:insert(lst[i])
    end
    return vec
end

-- assert with a traceback
function Assert(expr, level)    
    if not expr then
        if level == nil then
            level = 1
        end
        level = level + 1 -- we don't want to see Assert function in callstack
        error(debug.traceback(), level)
    end
end

-- output to script debug window
function out(content)
	SD.DebugOutput(tostring(content))
end

-- output to client logger
function lout(content)
	if tolua.type(content) == 'CEGUI::String' then
		SD.LoggerOutput(content:c_str())
	else
		SD.LoggerOutput(tostring(content))
	end
end

-- output to client logger
function lout2(level, content)
	SD.LoggerOutput(level, tostring(content))
end

-- write specified windows's tooltip to file
function dumpTips(wndName)
	local wnd = SD.ConvertPointerToWnd(wndName)
    local str = wnd:getProperty('Tooltip')
    local fp, err = io.open('dumpTips.txt', 'wb')
    if( err ) then
        lout('failed to dump tips:'..wndName)
    else
        fp:write(str:c_str())
        fp:close()
        lout('dumping tips success:'..wndName)
    end
end

-- write string to file
function fout(content, filename)
    if filename == nil or #filename == 0 then
        filename = "fout.txt"
    end
    local fp, err = io.open(filename, 'wb')
    if( err ) then
        out('failed to open: '..filename)
    else
        fp:write(content:c_str())
        fp:close()
        out('write to file success:'..filename)
    end
end

-- show all member in table
function showtable(tb)
    for k,v in pairs(tb) do 
       out(tostring(k) .. " : " .. tostring(v) .. "\n") 
    end
end

-- show cvector3 value
function showvec3(v)
    out( tostring(v:X()) .. ", "..tostring(v:Y())..", "..tostring(v:Z()))
end

-- set value in global environment
function setfield(f, v)
    local t = _G
    for w, d in string.gfind(f, "([%w_]+)(.?)") do
        if d == "." then
            t[w] = t[w] or {}
            t = t[w]
        else
            t[w] = v
        end
    end
    
    return v
end

-- get value in global environment
function getfield(f)
    local v = _G
    for w in string.gfind(f, "[%w_]+") do
        v = v[w]
    end
    return v
end

-- load script
function LoadModule(name)
    if nil == package.loaded[name] then
        lout('Loading module: '..name)
        require(name)
        local m = getfield(name); Assert(m)
        -- if load function defined, call it
        local onLoad = m.Load
        if onLoad then 
            onLoad()
        end
    end
end

-- unload script
function UnloadModule(name)
    if nil ~= package.loaded[name] then
        lout('Unloading module: '..name)
        local m = getfield(name); Assert(m)
        -- if unload function defined, call it
        local onUnload = m.Unload
        if onUnload then
            onUnload()
        end
        -- remove the entry in package.loaded
        package.loaded[name] = nil
    end
end

-- merge table, warning: the original value in `to' might be overwritten
function MergeTable(to, from)
    for k,v in pairs(from) do        
        to[k] = v
    end
end

-- convert a uint64 to a string
function U64ToStr(data)
    return SD.Uint64ToString(data)
end

-- count the element in table, this is a O(N) operation, so be careful
function GetTableSize(t)
    local cnt = 0
    for k, _ in pairs(t) do
        cnt = cnt + 1
    end
    return cnt
end

--****************************
--* InitState
--* @param modtable the module itself '_M'
--* @param modname the modname
--* @param stateName the defined state shortname 
--* @return 1.stateName, 2.stateFullName, 3. LuaFSMState
--****************************
function InitState(modtable, modname, stateName)    
    local stateFullName = modname.."."..stateName
    local S = SD.LuaFSMState:new(stateFullName)
    modtable[stateName] = S
    local peertable = {}
    setmetatable(peertable, {__index = _G})
    tolua.setpeer(S, peertable)
    setfenv(2, peertable)
    
    return stateName, stateFullName, S
end

--****************************
--* cast given event to specified type, if failed, assert
--* @param pEvt event injected by FSMContainer
--* @param evtname the expected event type name
--* @return 1.casted event
--****************************
function CastEvent(pEvt, evtname)
    local evtWrapper = tolua.cast(pEvt, 'SD::CEEventWrapper')
    local castedEvt = tolua.cast(evtWrapper:GetData(), evtname)
    Assert( nil ~= castedEvt, 2 )
    return castedEvt
end

-------------------------------------------------------------------------------
-- data structure
-------------------------------------------------------------------------------

Bimap={}
-- bimap, bijection
function Bimap:new()
    o = {}
    setmetatable(o, self)
    self.__index = self
    o.l2r = {}
    o.r2l = {}
    return o
end

function Bimap:Add(lhs, rhs)
    Assert( self.l2r[lhs] == nil )
    Assert( self.r2l[rhs] == nil )
    self.l2r[lhs] = rhs
    self.r2l[rhs] = lhs
end

function Bimap:RemoveLHS(lhs)
    Assert( self.l2r[lhs] ~= nil )
    local rhs = self.l2r[lhs]
    self.l2r[lhs] = nil
    self.r2l[rhs] = nil
end

function Bimap:RemoveRHS(rhs)
    Assert( self.r2l[rhs] ~= nil )
    local lhs = self.r2l[rhs]
    self.l2r[lhs] = nil
    self.r2l[rhs] = nil
end

function Bimap:GetLHS(lhs)
    Assert( lhs )
    local rhs = self.l2r[lhs]
    return rhs
end

function Bimap:GetRHS(rhs)
    Assert(rhs)
    local lhs = self.r2l[rhs]
    return lhs
end
-- bimap definition done.

function toInt(x)
	if x <= 0 then
		return math.ceil(x)
	end
	
	if math.ceil(x) == x then
		x = math.ceil(x)
	else
		x = math.ceil(x) - 1
	end
	return x
end

function Clamp(val, minVal, maxVal)
	if val < minVal then return minVal end
	if val > maxVal then return maxVal end
	return val;
end

-- Window Utility functions
function IsWindowPresent(name)
	if not name then return nil end
	return wndMgr:isWindowPresent(name)
end

function InitWindowPtr(name)
	if not name then return nil end
	
	if not IsWindowPresent(name) then
		return nil
	end
	return wndMgr:getWindow(name)
end












