
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local buyBook = indicatorFlow.createIndicatorSpec()
	buyBook.frameName     = 'BuyFishBook'
	buyBook.frameType     = indicatorFlow.INDICATOR_FRAME_LEFT
	buyBook.frameText     = '点击购买技能书'
	buyBook.attachFunc   = nil
	buyBook.attachFuncParam = nil
	buyBook.triggerFunc   = nil
	buyBook.triggerFuncParam = nil
	buyBook.triggerWin    = 'Root/ShopFrame/sell/Goods1/Button__auto_dragcontainer__'
	buyBook.attachWin     = 'Root/ShopFrame/sell/Goods1/Button1'
	buyBook.attachWinRoot = 'Root/NpcShop'
	buyBook.priority      = 1
	

	local learnBook = indicatorFlow.createIndicatorSpec()
	learnBook.frameName     = 'LearnFishBook'
	learnBook.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	learnBook.frameText     = '点击学习钓鱼技能'
	learnBook.attachFunc    = self.OnBuyRodTriggered
	learnBook.attachFuncParam = self
	learnBook.triggerFunc   = nil
	learnBook.triggerFuncParam = nil
	learnBook.attachType    = indicatorFlow.INDICATOR_ATTACH_BAGITEM
	learnBook.attachItemID  = '1_599'
	learnBook.priority      = 2
	

	
	self.indicatorSpecs = {
		buyBook, buyFishRod, learnBook, epuipRod, 
	}
	
	self.indicatorFlows = nil
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnBuyRodTriggered(self, indicator)
	lout('Fishing: OnBuyRodTriggered')
	SD.GenericFunc('HideCWindow', 'NpcShopWindow')
end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

