
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	self.bTaskAttached = false
	
	local tasktraceSpec = indicatorFlow.createIndicatorSpec()
	tasktraceSpec.frameName     = 'tasktraceSpec'
	tasktraceSpec.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	tasktraceSpec.frameText     = '鼠标放到追踪上可以快速在雷达地图上找到任务对应的地点哦'
	tasktraceSpec.triggerWin    = 'TaskTrace'
	tasktraceSpec.attachWin     = 'TaskTrace/Content/Item1920'
	tasktraceSpec.attachWinRoot = 'TaskTrace/Frame'
	tasktraceSpec.attachFunc    = self.OnTaskTraceAttach
	tasktraceSpec.attachFuncParam = self
	tasktraceSpec.priority      = 0
	
	self.tasktraceSpec = tasktraceSpec
	
	self.indicatorSpecs = {
		tasktraceSpec, 
	}
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function OnTaskTraceAttach(self, indicator)
	self.bTaskAttached = true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	if self.bTaskAttached then
		local pTaskTraceWnd = InitWindowPtr('TaskTrace')
		local pHoverWnd = CEGUI.System:getSingleton():getWindowContainingMouse()
		if pTaskTraceWnd and pHoverWnd then
			if pHoverWnd:isAncestor(pTaskTraceWnd) then
				self.completed = true
				return
			end
		end
	end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
	
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

