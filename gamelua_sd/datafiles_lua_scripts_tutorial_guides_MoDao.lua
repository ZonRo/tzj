
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame
	
	local useModao = indicatorFlow.createIndicatorSpec()
	useModao.frameName     = 'useModao'
	useModao.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	useModao.frameWidth    = 200
	useModao.frameText     = '按快捷键 Y 进行磨刀，磨刀可以补足武器锋利度使其发挥最大威力！'
	useModao.attachFunc    = nil
	useModao.attachFuncParam = self
	useModao.triggerFunc   = nil
	useModao.triggerFuncParam = nil
	useModao.triggerWin    = 'DefaultMainMenu/ItemFrame1/Modao__auto_dragcontainer__'
	useModao.attachWin     = 'DefaultMainMenu/ItemFrame1/Modao__auto_dragcontainer__'
	useModao.attachWinRoot = 'MainMenu_SkillRelease'
	useModao.triggerKey    = 'Polish'
	useModao.priority      = 1
	
	self.indicatorSpecs = {
		useModao, 
	}
	
	self.indicatorFlows = nil
	
	-- Server Completion
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	-- Create Indicator
	self.downIndicator = ui.createFrame('DownIndicatorFrame')
	if self.guideCfg.IndicatorText then
		self.downIndicator.pWindow:setText(self.guideCfg.IndicatorText)
	end
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)

	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
	
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function Update(self, fTime)
	if self.completed then
		return
	end
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

