

-- All Feature Definitions
FEATURE_SKILL_TALENT            = 1     -- 经脉
FEATURE_CRAFT_TALENT            = 2     -- 天工树
FEATURE_LIFE_SKILL              = 3     -- 技艺
FEATURE_TRAINING_PLACE          = 4     -- 试炼之地
FEATURE_BATTLE_QUEUE            = 5     -- 战场竞技场排队
FEATURE_RANDOM_DUNGON           = 6     -- 随机副本
FEATURE_GAME_RANK               = 7     -- 排行榜
FEATURE_STAR_DROP               = 8     -- 星尘
FEATURE_HEAD_COUNT              = 9     -- 人头数
FEATURE_ACCOMPLISHMENT_CARD     = 10    -- 记功牌
FEATURE_FATIGUE_DEGREE          = 11    -- 疲劳度
FEATURE_ACHIEVEMENT_SYSTEM      = 12    -- 成就
FEATURE_ATLAS_SYSTEM            = 13    -- 图鉴
FEATURE_CIMELIA_SYSTEM          = 14    -- 宝物
FEATURE_MEDAL_SYSTEM            = 15    -- 勋章
FEATURE_REPUTE_SYSTEM           = 16    -- 声望
FEATURE_RANDOM_TASK             = 17    -- 系统随机任务
FEATURE_EQUIPMENT_ENHANCE       = 18    -- 强化
FEATURE_EQUIPMENT_GEMINLAY      = 19    -- 镶嵌
FEATURE_EQUIPMENT_EXTRACT       = 20    -- 淬炼
FEATURE_EQUIPMENT_BREACH        = 21    -- 突破