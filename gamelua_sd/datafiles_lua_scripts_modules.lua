local modname = ...

module(modname, package.seeall)

local _modules = {}

function load(name)
	unLoad(name)
	
	lout('Loading module: '..name)
	local status, err = pcall(require, name)
	if not status then
		lout('Loading module error: ' .. tostring(err))
		return
	end
	_modules[name] = true
	local m = getfield(name)
	if m then
		-- if load function defined, call it
		local onModuleLoad = m.Load
		if onModuleLoad then 
			onModuleLoad()
		end
	end
end

function unLoad(name)
	if _modules[name] then
		lout('Unloading module: '..name)
		
		local m = getfield(name);
		if m then
			-- if unload function defined, call it
			local onModuleUnload = m.Unload
			if onModuleUnload then
				onModuleUnload()
			end
		end
		
		_modules[name] = false
		package.loaded[name] = nil
		_G[name] = nil
	end
end

function loadList(list)
	for i, v in ipairs(list) do
		load(v)
	end
end

function unLoadAll()
	for k, v in pairs(_modules) do
		if v then
			unLoad(k)
		end
	end
	_modules = {}
end