
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local fishingSpec = indicatorFlow.createIndicatorSpec()
	fishingSpec.frameName     = 'fishingSpec'
	fishingSpec.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	fishingSpec.frameText     = '面向河边点这里就可以开始钓鱼啦'
	fishingSpec.triggerWin    = 'MainMenu_Fishing/Icon__auto_dragcontainer__'
	fishingSpec.attachWin     = 'MainMenu_Fishing/Icon'
	fishingSpec.attachWinRoot = 'MainMenu_Fishing'
	fishingSpec.attachFunc    = nil
	fishingSpec.attachFuncParam = nil
	fishingSpec.priority      = 0
	
	self.fishingSpec = fishingSpec
	
	self.indicatorSpecs = {
		fishingSpec, 
	}
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
	
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
	
end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

