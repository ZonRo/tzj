STake_Start("N1_wait_000","N1_wait_000")  --N1标准待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

-----------------------------------------------------

STake_Start("N1_wait_001","N1_wait_001")  --蹲地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_002","N1_wait_002")  --坐姿待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_003","N1_wait_003")  --盘腿运气养神
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_004","N1_wait_004")  --躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_005","N1_wait_005")  --跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_006","N1_wait_006")  --轻受伤站立待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_007","N1_wait_007")  --重受伤跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008","N1_wait_008")  --受伤躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008_emo_001","N1_wait_008_emo_001")  --受伤躺地上说话
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_009","N1_wait_009")  --坐地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_010","N1_wait_010")  --手背背后跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011","N1_wait_011")  --说书先生说书
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011_emo_001","N1_wait_011_emo_001")  --说书先生喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_011_emo_002","N1_wait_011_emo_002")  --说书先生鞠躬
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012","N1_wait_012")  --屠夫切肉
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_012_emo_001","N1_wait_012_emo_001")  --屠夫切肉把肉归拢一边
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012_emo_002","N1_wait_012_emo_002")  --屠夫吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013","N1_wait_013")  --挑扁担货郎吆喝
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_013_emo_001","N1_wait_013_emo_001")  --挑扁担货郎擦汗
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013_emo_002","N1_wait_013_emo_002")  --挑扁担货郎把扁担放下休息
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_wait_013_move_001","N1_wait_013_move_001")  --挑扁担货郎走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_wait_014","N1_wait_014")  --大户人家护卫
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_014_emo_001","N1_wait_014_emo_001")  --大户人家护卫随机表情
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_emo_002","N1_wait_014_emo_002")  --大户人家护卫拦住去路
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_move_001","N1_wait_014_move_001")  --大户人家护卫走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015","N1_wait_015")  --马飞刷马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_emo_001","N1_wait_015_emo_001")  --马飞喂马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_move_001","N1_wait_015_move_001")  --马飞走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016","N1_wait_016")  --驿官待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016_emo_001","N1_wait_016_emo_001")  --驿官记账
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017","N1_wait_017")  --收钱艺人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_001","N1_wait_017_emo_001")  --收钱艺人敲锣
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_002","N1_wait_017_emo_002")  --收钱艺人坐下休息
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017_move_001","N1_wait_017_move_001")  --收钱艺人敲锣走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018","N1_wait_018")  --卖艺男批砖
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018_emo_001","N1_wait_018_emo_001")  --卖艺男抱拳行礼
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_emo_002","N1_wait_018_emo_002")  --卖艺男大声吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_move_001","N1_wait_018_move_001")  --卖艺男走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019","N1_wait_019")  --厨师切菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019_emo_001","N1_wait_019_emo_001")  --厨师切菜杂耍
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_020","N1_wait_020")  --厨师炒菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_020_emo_001","N1_wait_020_emo_001")  --厨师炒菜盛菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021","N1_wait_021")  --伙计端盘待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_021_emo_001","N1_wait_021_emo_001")  --伙计端盘上菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021_move_001","N1_wait_021_move_001")  --伙计端盘走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022","N1_wait_022")  --坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022_emo_001","N1_wait_022_emo_001")  --坐着等上菜喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_023","N1_wait_023")  --地痞坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_023_emo_001","N1_wait_023_emo_001")  --地痞坐着等上菜拍桌子
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_024","N1_wait_024")  --坐着夹菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_024_emo_001","N1_wait_024_emo_001")  --坐着吃饭
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_025","N1_wait_025")  --坐着喝茶
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_026","N1_wait_026")  --服务生打算盘
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_027","N1_wait_027")  --抱头躲藏
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028","N1_wait_028")  --扎马步waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028_emo_001","N1_wait_028_emo_001")  --扎马步emo
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_029","N1_wait_029")  --撒网捕鱼waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_029_emo_001","N1_wait_029_emo_001")  --撒网
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_030","N1_wait_030")  --坐着waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

------------------------------------------------------------

STake_Start("N1_move_000","N1_move_000")  --普通前走
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_F","N1_move_001_F")  --前跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_B","N1_move_001_B")  --后退
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_L","N1_move_001_L")  --左跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_R","N1_move_001_R")  --右跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_002","N1_move_002")  --举刀前跑冲锋
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_003","N1_move_003")  --左踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_004","N1_move_004")  --右踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    
STake_Start("N1_move_005","N1_move_005-007")  --起跳
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.8333)

STake_Start("N1_move_006","N1_move_006")  --空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_007","N1_move_005-007")  --落地
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.84,1.566)

STake_Start("N1_move_020","N1_move_020")  --惊恐跑步
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_021","N1_move_021")  --受伤走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

-----------------------------------------------------
STake_Start("N1_life_000","N1_life_000")  --站立伸懒腰
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_001","N1_life_001")  --站立左右看
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_002","N1_life_002")  --站立大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_003","N1_life_003")  --站立哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_004","N1_life_004")  --跪地哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_005","N1_life_005")  --打铁
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_006","N1_life_006")  --打招呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_007","N1_life_007")  --招呼客人
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_008","N1_life_008")  --普通说话
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_009","N1_life_009")  --惊恐
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_010","N1_life_010")  --耕地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_011","N1_life_011")  --插秧
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_012","N1_life_012")  --站着擦汗
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_013","N1_life_013")  --伐木
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_014","N1_life_014")  --修理
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_015","N1_life_015")  --死亡自杀
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.832,1.833)

STake_Start("N1_life_016","N1_life_016")  --坐着聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_017","N1_life_017")  --跪地磕头
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_019","N1_life_019")  --钓鱼待机
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_020","N1_life_020")  --钓鱼表情
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_021","N1_life_021")  --躲起来左右看
    BlendMode(0)
    BlendTime(0.2)
    
STake_Start("N1_life_022","N1_life_022")  --手背身后跪地左右张望
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_023","N1_life_023")  --手背身后跪地上哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_024","N1_life_024")  --受伤躺地呻吟
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_025","N1_life_025")  --受伤躺地挣扎
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_026","N1_life_026")  --欢呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_027","N1_life_027")  --坐地喝酒
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_028","N1_life_028")  --坐地聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_029","N1_life_029")  --坐地大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_030","N1_life_030")  --扫地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_031","N1_life_031")  --向前一指
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_032","N1_life_032")  --惊恐害怕
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_033","N1_life_033")  --打坐
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_034","N1_life_034")  --喝醉躺地
    BlendMode(0)
    BlendTime(0.2)
    Loop()


---------------------------------------

STake_Start("N1_bout_001","N1_bout_001")  --出生
    BlendMode(0)
    BlendTime(0)



STake_Start("N1_ZHLB01_wait_000","N1_ZHLB01_wait_000")  --账房老板
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_ZHLB01_emo_000","N1_ZHLB01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_ZHLB01_emo_001","N1_ZHLB01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_YS01_wait_000","N1_YS01_wait_000")  --药商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_YS01_emo_000","N1_YS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_YS01_emo_001","N1_YS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_WQS01_wait_000","N1_WQS01_wait_000")  --武器商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_WQS01_emo_000","N1_WQS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_WQS01_emo_001","N1_WQS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_wait_000","N1_LR01_wait_000")  --
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_LR01_emo_000","N1_LR01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_emo_001","N1_LR01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_wait_000","N1_JHYY01_wait_000")  --江湖游医
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHYY01_emo_000","N1_JHYY01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_emo_001","N1_JHYY01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_JHSS01_wait_000","N1_JHSS01_wait_000")  --江湖术士
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHSS01_emo_000","N1_JHSS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHSS01_emo_001","N1_JHSS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_wait_000","N1_DXE01_wait_000")  --店小二
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_DXE01_emo_000","N1_DXE01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_emo_001","N1_DXE01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM03_life_000","N1_CM03_life_000")  --村民03
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM03_life_001","N1_CM03_life_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM05_life_002","N1_CM05_life_002")  --村民05
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM05_life_003","N1_CM05_life_003")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_wait_000","N1_QZGJDZ01_wait_000")  --全真高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZGJDZ01_emo_001","N1_QZGJDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_emo_002","N1_QZGJDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_wait_000","N1_QZRMDZ01_wait_000")  --全真入门弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZRMDZ01_emo_001","N1_QZRMDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_emo_002","N1_QZRMDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_wait_000","N1_RS_wait_000")  --儒生
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_RS_emo_001","N1_RS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_emo_002","N1_RS_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_wait_000","N1_XinShi_wait_000")  --信使
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XinShi_emo_001","N1_XinShi_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_emo_002","N1_XinShi_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_wait_000","N1_MGN_wait_000")  --蒙古男
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_MGN_emo_001","N1_MGN_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_emo_002","N1_MGN_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSGJDZ_wait_000","N1_BTSGJDZ_wait_000")  --白驼高阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSGJDZ_emo_001","N1_BTSGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSGJDZ_emo_002","N1_BTSGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJJL_wait_000","N1_HAJJL_wait_000")  --红袄军将领
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJJL_emo_001","N1_HAJJL_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJJL_emo_002","N1_HAJJL_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJ01_wait_000","N1_HAJ01_wait_000")  --红袄军伤兵
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJ01_emo_001","N1_HAJ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJ01_emo_002","N1_HAJ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_wait_000","N1_BTSDJDZ_wait_000")  --白驼低阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSDJDZ_emo_001","N1_BTSDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSDJDZ_emo_002","N1_BTSDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_emo_003","N1_BTSDJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_wait_000","N1_BTSZJDZ_wait_000")  --白驼中阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_emo_001","N1_BTSZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSZJDZ_emo_002","N1_BTSZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSZJDZ_emo_003","N1_BTSZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_wait_000","N1_XYSl01_wait_000")  --西域商旅01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_emo_001","N1_XYSl01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl01_emo_002","N1_XYSl01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_XYSl02_wait_000","N1_XYSl02_wait_000")  --西域商旅02
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl02_emo_001","N1_XYSl02_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl02_emo_002","N1_XYSl02_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_SLSJQS_wait_000","N1_SLSJQS_wait_000")  --少林俗家拳师01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_SLSJQS_emo_001","N1_SLSJQS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_SLSJQS_emo_002","N1_SLSJQS_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSanDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮三袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSanDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSanDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSiDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮四袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSiDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSiDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBLDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮六袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBLDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBLDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBBDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮八袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBBDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBBDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDDJDZ_wait_000","N1_THDDJDZ_wait_000")  --桃花岛低阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDDJDZ_emo_001","N1_THDDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDDJDZ_emo_002","N1_THDDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_wait_000","N1_THDZJDZ_wait_000")  --桃花岛中阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDZJDZ_emo_001","N1_THDZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDZJDZ_emo_002","N1_THDZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_003","N1_THDZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_004","N1_THDZJDZ_emo_004")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDGJDZ_wait_000","N1_THDGJDZ_wait_000")  --桃花岛高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDGJDZ_emo_001","N1_THDGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDGJDZ_emo_002","N1_THDGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_MGSB_01_wait_000","N1_MGSB_01_wait_000")  --躺着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_wait_001","N1_MGSB_01_wait_001")  --蹲着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_emo_000","N1_MGSB_01_emo_000")  --躺着一边呻吟一边伸手求助
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGSB_01_emo_001","N1_MGSB_01_emo_001")  --躺着摇着头悲叹自己的命运
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_wait_000","N1_CHDMGNZ01_wait_000")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_CHDMGNZ01_emo_001","N1_CHDMGNZ01_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_emo_002","N1_CHDMGNZ01_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_wait_000","N1_MTQS_wait_000")  --蒙古马头琴手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MTQS_emo_001","N1_MTQS_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_emo_002","N1_MTQS_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

-------------------------------塞外古城--------------------------

STake_Start("N1_wait_swgc_zhongdu_emo_001","N1_wait_008_emo_001")  
    BlendMode(0)
    BlendTime(0.2)
    Direction(2) 
	FrameTime(1.015,1.782)
	Loop(1.015,1.782)
    PlaySpeed(1.015,1.782,0.45)
    Fx(1.015,"FX_buff_poisoning","Head",0.8,1,40,0,0,0,0,0,1,1,1)



---------------------------------------------------------

STake_Start("N1_life_B_move_003","N1_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    
STake_Start("N1_life_B_move_004","YP_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 
    
STake_Start("N1_life_wait_000","N1_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("N1_life_B_wait_000","YP_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()   

STake_Start("N1_life_def_000","YP_def_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.235,3)
    Sound(0.00,"weap_Sword_block")
    Fx(0,"Fx_gedang_wuqi")   

    Sound(0.053,"YP_def_000")

STake_Start("N1_life_dodge_000","YP_dodge_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.352,3)       

    Sound(0.112,"N1_Axe_dodge_000") 
    
STake_Start("N1_life_attack_000","YP_attack_000")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    
  	--Hard(0.000,0.266)  	
  	
    AttackStart()
  	Hurt(0.240,"hurt_11")
    Sound(0.190,"YP_attack_000")
    HurtSound(0.240,"blade_Hit")

    Fx(0.000,"YP_attack_000_fire") 
    DirFx(0.240,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_001","YP_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    
    --Hard(0.000,0.334)  	
  	
    AttackStart()
  	Hurt(0.217,"hurt_11")
    Sound(0.167,"YP_attack_000")
    HurtSound(0.217,"blade_Hit")

    Fx(0.000,"YP_attack_001_fire") 
    DirFx(0.217,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_002","YP_attack_002")
    BlendMode(0)
    BlendTime(0.2)  
    Priority(0,"5") 
    
    --Hard(0.000,0.448) 
    
    AttackStart()
  	Hurt(0.205,"hurt_11")
    Sound(0.155,"YP_attack_000")
    HurtSound(0.205,"blade_Hit")    
    
    Fx(0.000,"YP_attack_002_fire")     
    DirFx(0.205,"YP_attack_hit",0,1)
    

    




STake_Start("N1_wait_000","N1_wait_000")  --N1标准待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

-----------------------------------------------------

STake_Start("N1_wait_001","N1_wait_001")  --蹲地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_002","N1_wait_002")  --坐姿待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_003","N1_wait_003")  --盘腿运气养神
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_004","N1_wait_004")  --躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_005","N1_wait_005")  --跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_006","N1_wait_006")  --轻受伤站立待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_007","N1_wait_007")  --重受伤跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008","N1_wait_008")  --受伤躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008_emo_001","N1_wait_008_emo_001")  --受伤躺地上说话
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_009","N1_wait_009")  --坐地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_010","N1_wait_010")  --手背背后跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011","N1_wait_011")  --说书先生说书
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011_emo_001","N1_wait_011_emo_001")  --说书先生喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_011_emo_002","N1_wait_011_emo_002")  --说书先生鞠躬
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012","N1_wait_012")  --屠夫切肉
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_012_emo_001","N1_wait_012_emo_001")  --屠夫切肉把肉归拢一边
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012_emo_002","N1_wait_012_emo_002")  --屠夫吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013","N1_wait_013")  --挑扁担货郎吆喝
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_013_emo_001","N1_wait_013_emo_001")  --挑扁担货郎擦汗
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013_emo_002","N1_wait_013_emo_002")  --挑扁担货郎把扁担放下休息
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_wait_013_move_001","N1_wait_013_move_001")  --挑扁担货郎走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_wait_014","N1_wait_014")  --大户人家护卫
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_014_emo_001","N1_wait_014_emo_001")  --大户人家护卫随机表情
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_emo_002","N1_wait_014_emo_002")  --大户人家护卫拦住去路
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_move_001","N1_wait_014_move_001")  --大户人家护卫走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015","N1_wait_015")  --马飞刷马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_emo_001","N1_wait_015_emo_001")  --马飞喂马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_move_001","N1_wait_015_move_001")  --马飞走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016","N1_wait_016")  --驿官待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016_emo_001","N1_wait_016_emo_001")  --驿官记账
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017","N1_wait_017")  --收钱艺人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_001","N1_wait_017_emo_001")  --收钱艺人敲锣
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_002","N1_wait_017_emo_002")  --收钱艺人坐下休息
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017_move_001","N1_wait_017_move_001")  --收钱艺人敲锣走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018","N1_wait_018")  --卖艺男批砖
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018_emo_001","N1_wait_018_emo_001")  --卖艺男抱拳行礼
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_emo_002","N1_wait_018_emo_002")  --卖艺男大声吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_move_001","N1_wait_018_move_001")  --卖艺男走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019","N1_wait_019")  --厨师切菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019_emo_001","N1_wait_019_emo_001")  --厨师切菜杂耍
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_020","N1_wait_020")  --厨师炒菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_020_emo_001","N1_wait_020_emo_001")  --厨师炒菜盛菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021","N1_wait_021")  --伙计端盘待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_021_emo_001","N1_wait_021_emo_001")  --伙计端盘上菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021_move_001","N1_wait_021_move_001")  --伙计端盘走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022","N1_wait_022")  --坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022_emo_001","N1_wait_022_emo_001")  --坐着等上菜喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_023","N1_wait_023")  --地痞坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_023_emo_001","N1_wait_023_emo_001")  --地痞坐着等上菜拍桌子
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_024","N1_wait_024")  --坐着夹菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_024_emo_001","N1_wait_024_emo_001")  --坐着吃饭
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_025","N1_wait_025")  --坐着喝茶
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_026","N1_wait_026")  --服务生打算盘
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_027","N1_wait_027")  --抱头躲藏
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028","N1_wait_028")  --扎马步waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028_emo_001","N1_wait_028_emo_001")  --扎马步emo
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_029","N1_wait_029")  --撒网捕鱼waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_029_emo_001","N1_wait_029_emo_001")  --撒网
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_030","N1_wait_030")  --坐着waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

------------------------------------------------------------

STake_Start("N1_move_000","N1_move_000")  --普通前走
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_F","N1_move_001_F")  --前跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_B","N1_move_001_B")  --后退
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_L","N1_move_001_L")  --左跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_R","N1_move_001_R")  --右跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_002","N1_move_002")  --举刀前跑冲锋
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_003","N1_move_003")  --左踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_004","N1_move_004")  --右踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    
STake_Start("N1_move_005","N1_move_005-007")  --起跳
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.8333)

STake_Start("N1_move_006","N1_move_006")  --空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_007","N1_move_005-007")  --落地
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.84,1.566)

STake_Start("N1_move_020","N1_move_020")  --惊恐跑步
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_021","N1_move_021")  --受伤走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

-----------------------------------------------------
STake_Start("N1_life_000","N1_life_000")  --站立伸懒腰
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_001","N1_life_001")  --站立左右看
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_002","N1_life_002")  --站立大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_003","N1_life_003")  --站立哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_004","N1_life_004")  --跪地哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_005","N1_life_005")  --打铁
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_006","N1_life_006")  --打招呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_007","N1_life_007")  --招呼客人
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_008","N1_life_008")  --普通说话
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_009","N1_life_009")  --惊恐
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_010","N1_life_010")  --耕地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_011","N1_life_011")  --插秧
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_012","N1_life_012")  --站着擦汗
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_013","N1_life_013")  --伐木
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_014","N1_life_014")  --修理
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_015","N1_life_015")  --死亡自杀
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.832,1.833)

STake_Start("N1_life_016","N1_life_016")  --坐着聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_017","N1_life_017")  --跪地磕头
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_019","N1_life_019")  --钓鱼待机
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_020","N1_life_020")  --钓鱼表情
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_021","N1_life_021")  --躲起来左右看
    BlendMode(0)
    BlendTime(0.2)
    
STake_Start("N1_life_022","N1_life_022")  --手背身后跪地左右张望
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_023","N1_life_023")  --手背身后跪地上哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_024","N1_life_024")  --受伤躺地呻吟
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_025","N1_life_025")  --受伤躺地挣扎
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_026","N1_life_026")  --欢呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_027","N1_life_027")  --坐地喝酒
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_028","N1_life_028")  --坐地聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_029","N1_life_029")  --坐地大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_030","N1_life_030")  --扫地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_031","N1_life_031")  --向前一指
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_032","N1_life_032")  --惊恐害怕
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_033","N1_life_033")  --打坐
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_034","N1_life_034")  --喝醉躺地
    BlendMode(0)
    BlendTime(0.2)
    Loop()


---------------------------------------

STake_Start("N1_bout_001","N1_bout_001")  --出生
    BlendMode(0)
    BlendTime(0)



STake_Start("N1_ZHLB01_wait_000","N1_ZHLB01_wait_000")  --账房老板
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_ZHLB01_emo_000","N1_ZHLB01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_ZHLB01_emo_001","N1_ZHLB01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_YS01_wait_000","N1_YS01_wait_000")  --药商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_YS01_emo_000","N1_YS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_YS01_emo_001","N1_YS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_WQS01_wait_000","N1_WQS01_wait_000")  --武器商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_WQS01_emo_000","N1_WQS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_WQS01_emo_001","N1_WQS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_wait_000","N1_LR01_wait_000")  --
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_LR01_emo_000","N1_LR01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_emo_001","N1_LR01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_wait_000","N1_JHYY01_wait_000")  --江湖游医
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHYY01_emo_000","N1_JHYY01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_emo_001","N1_JHYY01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_JHSS01_wait_000","N1_JHSS01_wait_000")  --江湖术士
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHSS01_emo_000","N1_JHSS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHSS01_emo_001","N1_JHSS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_wait_000","N1_DXE01_wait_000")  --店小二
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_DXE01_emo_000","N1_DXE01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_emo_001","N1_DXE01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM03_life_000","N1_CM03_life_000")  --村民03
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM03_life_001","N1_CM03_life_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM05_life_002","N1_CM05_life_002")  --村民05
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM05_life_003","N1_CM05_life_003")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_wait_000","N1_QZGJDZ01_wait_000")  --全真高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZGJDZ01_emo_001","N1_QZGJDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_emo_002","N1_QZGJDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_wait_000","N1_QZRMDZ01_wait_000")  --全真入门弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZRMDZ01_emo_001","N1_QZRMDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_emo_002","N1_QZRMDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_wait_000","N1_RS_wait_000")  --儒生
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_RS_emo_001","N1_RS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_emo_002","N1_RS_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_wait_000","N1_XinShi_wait_000")  --信使
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XinShi_emo_001","N1_XinShi_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_emo_002","N1_XinShi_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_wait_000","N1_MGN_wait_000")  --蒙古男
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_MGN_emo_001","N1_MGN_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_emo_002","N1_MGN_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSGJDZ_wait_000","N1_BTSGJDZ_wait_000")  --白驼高阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSGJDZ_emo_001","N1_BTSGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSGJDZ_emo_002","N1_BTSGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJJL_wait_000","N1_HAJJL_wait_000")  --红袄军将领
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJJL_emo_001","N1_HAJJL_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJJL_emo_002","N1_HAJJL_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJ01_wait_000","N1_HAJ01_wait_000")  --红袄军伤兵
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJ01_emo_001","N1_HAJ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJ01_emo_002","N1_HAJ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_wait_000","N1_BTSDJDZ_wait_000")  --白驼低阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSDJDZ_emo_001","N1_BTSDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSDJDZ_emo_002","N1_BTSDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_emo_003","N1_BTSDJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_wait_000","N1_BTSZJDZ_wait_000")  --白驼中阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_emo_001","N1_BTSZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSZJDZ_emo_002","N1_BTSZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSZJDZ_emo_003","N1_BTSZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_wait_000","N1_XYSl01_wait_000")  --西域商旅01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_emo_001","N1_XYSl01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl01_emo_002","N1_XYSl01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_XYSl02_wait_000","N1_XYSl02_wait_000")  --西域商旅02
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl02_emo_001","N1_XYSl02_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl02_emo_002","N1_XYSl02_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_SLSJQS_wait_000","N1_SLSJQS_wait_000")  --少林俗家拳师01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_SLSJQS_emo_001","N1_SLSJQS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_SLSJQS_emo_002","N1_SLSJQS_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSanDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮三袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSanDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSanDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSiDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮四袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSiDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSiDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBLDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮六袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBLDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBLDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBBDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮八袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBBDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBBDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDDJDZ_wait_000","N1_THDDJDZ_wait_000")  --桃花岛低阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDDJDZ_emo_001","N1_THDDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDDJDZ_emo_002","N1_THDDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_wait_000","N1_THDZJDZ_wait_000")  --桃花岛中阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDZJDZ_emo_001","N1_THDZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDZJDZ_emo_002","N1_THDZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_003","N1_THDZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_004","N1_THDZJDZ_emo_004")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDGJDZ_wait_000","N1_THDGJDZ_wait_000")  --桃花岛高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDGJDZ_emo_001","N1_THDGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDGJDZ_emo_002","N1_THDGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_MGSB_01_wait_000","N1_MGSB_01_wait_000")  --躺着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_wait_001","N1_MGSB_01_wait_001")  --蹲着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_emo_000","N1_MGSB_01_emo_000")  --躺着一边呻吟一边伸手求助
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGSB_01_emo_001","N1_MGSB_01_emo_001")  --躺着摇着头悲叹自己的命运
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_wait_000","N1_CHDMGNZ01_wait_000")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_CHDMGNZ01_emo_001","N1_CHDMGNZ01_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_emo_002","N1_CHDMGNZ01_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_wait_000","N1_MTQS_wait_000")  --蒙古马头琴手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MTQS_emo_001","N1_MTQS_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_emo_002","N1_MTQS_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

-------------------------------塞外古城--------------------------

STake_Start("N1_wait_swgc_zhongdu_emo_001","N1_wait_008_emo_001")  
    BlendMode(0)
    BlendTime(0.2)
    Direction(2) 
	FrameTime(1.015,1.782)
	Loop(1.015,1.782)
    PlaySpeed(1.015,1.782,0.45)
    Fx(1.015,"FX_buff_poisoning","Head",0.8,1,40,0,0,0,0,0,1,1,1)



---------------------------------------------------------

STake_Start("N1_life_B_move_003","N1_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    
STake_Start("N1_life_B_move_004","YP_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 
    
STake_Start("N1_life_wait_000","N1_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("N1_life_B_wait_000","YP_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()   

STake_Start("N1_life_def_000","YP_def_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.235,3)
    Sound(0.00,"weap_Sword_block")
    Fx(0,"Fx_gedang_wuqi")   

    Sound(0.053,"YP_def_000")

STake_Start("N1_life_dodge_000","YP_dodge_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.352,3)       

    Sound(0.112,"N1_Axe_dodge_000") 
    
STake_Start("N1_life_attack_000","YP_attack_000")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    
  	--Hard(0.000,0.266)  	
  	
    AttackStart()
  	Hurt(0.240,"hurt_11")
    Sound(0.190,"YP_attack_000")
    HurtSound(0.240,"blade_Hit")

    Fx(0.000,"YP_attack_000_fire") 
    DirFx(0.240,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_001","YP_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    
    --Hard(0.000,0.334)  	
  	
    AttackStart()
  	Hurt(0.217,"hurt_11")
    Sound(0.167,"YP_attack_000")
    HurtSound(0.217,"blade_Hit")

    Fx(0.000,"YP_attack_001_fire") 
    DirFx(0.217,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_002","YP_attack_002")
    BlendMode(0)
    BlendTime(0.2)  
    Priority(0,"5") 
    
    --Hard(0.000,0.448) 
    
    AttackStart()
  	Hurt(0.205,"hurt_11")
    Sound(0.155,"YP_attack_000")
    HurtSound(0.205,"blade_Hit")    
    
    Fx(0.000,"YP_attack_002_fire")     
    DirFx(0.205,"YP_attack_hit",0,1)
    

    




STake_Start("N1_wait_000","N1_wait_000")  --N1标准待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

-----------------------------------------------------

STake_Start("N1_wait_001","N1_wait_001")  --蹲地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_002","N1_wait_002")  --坐姿待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_003","N1_wait_003")  --盘腿运气养神
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_004","N1_wait_004")  --躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_005","N1_wait_005")  --跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_006","N1_wait_006")  --轻受伤站立待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_007","N1_wait_007")  --重受伤跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008","N1_wait_008")  --受伤躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008_emo_001","N1_wait_008_emo_001")  --受伤躺地上说话
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_009","N1_wait_009")  --坐地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_010","N1_wait_010")  --手背背后跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011","N1_wait_011")  --说书先生说书
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011_emo_001","N1_wait_011_emo_001")  --说书先生喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_011_emo_002","N1_wait_011_emo_002")  --说书先生鞠躬
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012","N1_wait_012")  --屠夫切肉
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_012_emo_001","N1_wait_012_emo_001")  --屠夫切肉把肉归拢一边
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012_emo_002","N1_wait_012_emo_002")  --屠夫吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013","N1_wait_013")  --挑扁担货郎吆喝
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_013_emo_001","N1_wait_013_emo_001")  --挑扁担货郎擦汗
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013_emo_002","N1_wait_013_emo_002")  --挑扁担货郎把扁担放下休息
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_wait_013_move_001","N1_wait_013_move_001")  --挑扁担货郎走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_wait_014","N1_wait_014")  --大户人家护卫
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_014_emo_001","N1_wait_014_emo_001")  --大户人家护卫随机表情
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_emo_002","N1_wait_014_emo_002")  --大户人家护卫拦住去路
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_move_001","N1_wait_014_move_001")  --大户人家护卫走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015","N1_wait_015")  --马飞刷马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_emo_001","N1_wait_015_emo_001")  --马飞喂马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_move_001","N1_wait_015_move_001")  --马飞走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016","N1_wait_016")  --驿官待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016_emo_001","N1_wait_016_emo_001")  --驿官记账
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017","N1_wait_017")  --收钱艺人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_001","N1_wait_017_emo_001")  --收钱艺人敲锣
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_002","N1_wait_017_emo_002")  --收钱艺人坐下休息
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017_move_001","N1_wait_017_move_001")  --收钱艺人敲锣走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018","N1_wait_018")  --卖艺男批砖
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018_emo_001","N1_wait_018_emo_001")  --卖艺男抱拳行礼
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_emo_002","N1_wait_018_emo_002")  --卖艺男大声吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_move_001","N1_wait_018_move_001")  --卖艺男走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019","N1_wait_019")  --厨师切菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019_emo_001","N1_wait_019_emo_001")  --厨师切菜杂耍
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_020","N1_wait_020")  --厨师炒菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_020_emo_001","N1_wait_020_emo_001")  --厨师炒菜盛菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021","N1_wait_021")  --伙计端盘待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_021_emo_001","N1_wait_021_emo_001")  --伙计端盘上菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021_move_001","N1_wait_021_move_001")  --伙计端盘走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022","N1_wait_022")  --坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022_emo_001","N1_wait_022_emo_001")  --坐着等上菜喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_023","N1_wait_023")  --地痞坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_023_emo_001","N1_wait_023_emo_001")  --地痞坐着等上菜拍桌子
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_024","N1_wait_024")  --坐着夹菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_024_emo_001","N1_wait_024_emo_001")  --坐着吃饭
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_025","N1_wait_025")  --坐着喝茶
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_026","N1_wait_026")  --服务生打算盘
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_027","N1_wait_027")  --抱头躲藏
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028","N1_wait_028")  --扎马步waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028_emo_001","N1_wait_028_emo_001")  --扎马步emo
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_029","N1_wait_029")  --撒网捕鱼waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_029_emo_001","N1_wait_029_emo_001")  --撒网
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_030","N1_wait_030")  --坐着waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

------------------------------------------------------------

STake_Start("N1_move_000","N1_move_000")  --普通前走
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_F","N1_move_001_F")  --前跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_B","N1_move_001_B")  --后退
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_L","N1_move_001_L")  --左跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_R","N1_move_001_R")  --右跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_002","N1_move_002")  --举刀前跑冲锋
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_003","N1_move_003")  --左踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_004","N1_move_004")  --右踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    
STake_Start("N1_move_005","N1_move_005-007")  --起跳
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.8333)

STake_Start("N1_move_006","N1_move_006")  --空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_007","N1_move_005-007")  --落地
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.84,1.566)

STake_Start("N1_move_020","N1_move_020")  --惊恐跑步
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_021","N1_move_021")  --受伤走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

-----------------------------------------------------
STake_Start("N1_life_000","N1_life_000")  --站立伸懒腰
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_001","N1_life_001")  --站立左右看
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_002","N1_life_002")  --站立大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_003","N1_life_003")  --站立哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_004","N1_life_004")  --跪地哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_005","N1_life_005")  --打铁
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_006","N1_life_006")  --打招呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_007","N1_life_007")  --招呼客人
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_008","N1_life_008")  --普通说话
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_009","N1_life_009")  --惊恐
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_010","N1_life_010")  --耕地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_011","N1_life_011")  --插秧
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_012","N1_life_012")  --站着擦汗
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_013","N1_life_013")  --伐木
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_014","N1_life_014")  --修理
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_015","N1_life_015")  --死亡自杀
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.832,1.833)

STake_Start("N1_life_016","N1_life_016")  --坐着聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_017","N1_life_017")  --跪地磕头
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_019","N1_life_019")  --钓鱼待机
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_020","N1_life_020")  --钓鱼表情
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_021","N1_life_021")  --躲起来左右看
    BlendMode(0)
    BlendTime(0.2)
    
STake_Start("N1_life_022","N1_life_022")  --手背身后跪地左右张望
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_023","N1_life_023")  --手背身后跪地上哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_024","N1_life_024")  --受伤躺地呻吟
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_025","N1_life_025")  --受伤躺地挣扎
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_026","N1_life_026")  --欢呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_027","N1_life_027")  --坐地喝酒
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_028","N1_life_028")  --坐地聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_029","N1_life_029")  --坐地大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_030","N1_life_030")  --扫地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_031","N1_life_031")  --向前一指
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_032","N1_life_032")  --惊恐害怕
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_033","N1_life_033")  --打坐
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_034","N1_life_034")  --喝醉躺地
    BlendMode(0)
    BlendTime(0.2)
    Loop()


---------------------------------------

STake_Start("N1_bout_001","N1_bout_001")  --出生
    BlendMode(0)
    BlendTime(0)



STake_Start("N1_ZHLB01_wait_000","N1_ZHLB01_wait_000")  --账房老板
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_ZHLB01_emo_000","N1_ZHLB01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_ZHLB01_emo_001","N1_ZHLB01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_YS01_wait_000","N1_YS01_wait_000")  --药商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_YS01_emo_000","N1_YS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_YS01_emo_001","N1_YS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_WQS01_wait_000","N1_WQS01_wait_000")  --武器商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_WQS01_emo_000","N1_WQS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_WQS01_emo_001","N1_WQS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_wait_000","N1_LR01_wait_000")  --
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_LR01_emo_000","N1_LR01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_emo_001","N1_LR01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_wait_000","N1_JHYY01_wait_000")  --江湖游医
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHYY01_emo_000","N1_JHYY01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_emo_001","N1_JHYY01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_JHSS01_wait_000","N1_JHSS01_wait_000")  --江湖术士
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHSS01_emo_000","N1_JHSS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHSS01_emo_001","N1_JHSS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_wait_000","N1_DXE01_wait_000")  --店小二
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_DXE01_emo_000","N1_DXE01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_emo_001","N1_DXE01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM03_life_000","N1_CM03_life_000")  --村民03
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM03_life_001","N1_CM03_life_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM05_life_002","N1_CM05_life_002")  --村民05
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM05_life_003","N1_CM05_life_003")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_wait_000","N1_QZGJDZ01_wait_000")  --全真高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZGJDZ01_emo_001","N1_QZGJDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_emo_002","N1_QZGJDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_wait_000","N1_QZRMDZ01_wait_000")  --全真入门弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZRMDZ01_emo_001","N1_QZRMDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_emo_002","N1_QZRMDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_wait_000","N1_RS_wait_000")  --儒生
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_RS_emo_001","N1_RS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_emo_002","N1_RS_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_wait_000","N1_XinShi_wait_000")  --信使
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XinShi_emo_001","N1_XinShi_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_emo_002","N1_XinShi_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_wait_000","N1_MGN_wait_000")  --蒙古男
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_MGN_emo_001","N1_MGN_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_emo_002","N1_MGN_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSGJDZ_wait_000","N1_BTSGJDZ_wait_000")  --白驼高阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSGJDZ_emo_001","N1_BTSGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSGJDZ_emo_002","N1_BTSGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJJL_wait_000","N1_HAJJL_wait_000")  --红袄军将领
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJJL_emo_001","N1_HAJJL_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJJL_emo_002","N1_HAJJL_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJ01_wait_000","N1_HAJ01_wait_000")  --红袄军伤兵
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJ01_emo_001","N1_HAJ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJ01_emo_002","N1_HAJ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_wait_000","N1_BTSDJDZ_wait_000")  --白驼低阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSDJDZ_emo_001","N1_BTSDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSDJDZ_emo_002","N1_BTSDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_emo_003","N1_BTSDJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_wait_000","N1_BTSZJDZ_wait_000")  --白驼中阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_emo_001","N1_BTSZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSZJDZ_emo_002","N1_BTSZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSZJDZ_emo_003","N1_BTSZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_wait_000","N1_XYSl01_wait_000")  --西域商旅01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_emo_001","N1_XYSl01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl01_emo_002","N1_XYSl01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_XYSl02_wait_000","N1_XYSl02_wait_000")  --西域商旅02
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl02_emo_001","N1_XYSl02_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl02_emo_002","N1_XYSl02_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_SLSJQS_wait_000","N1_SLSJQS_wait_000")  --少林俗家拳师01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_SLSJQS_emo_001","N1_SLSJQS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_SLSJQS_emo_002","N1_SLSJQS_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSanDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮三袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSanDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSanDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSiDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮四袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSiDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSiDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBLDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮六袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBLDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBLDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBBDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮八袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBBDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBBDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDDJDZ_wait_000","N1_THDDJDZ_wait_000")  --桃花岛低阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDDJDZ_emo_001","N1_THDDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDDJDZ_emo_002","N1_THDDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_wait_000","N1_THDZJDZ_wait_000")  --桃花岛中阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDZJDZ_emo_001","N1_THDZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDZJDZ_emo_002","N1_THDZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_003","N1_THDZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_004","N1_THDZJDZ_emo_004")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDGJDZ_wait_000","N1_THDGJDZ_wait_000")  --桃花岛高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDGJDZ_emo_001","N1_THDGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDGJDZ_emo_002","N1_THDGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_MGSB_01_wait_000","N1_MGSB_01_wait_000")  --躺着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_wait_001","N1_MGSB_01_wait_001")  --蹲着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_emo_000","N1_MGSB_01_emo_000")  --躺着一边呻吟一边伸手求助
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGSB_01_emo_001","N1_MGSB_01_emo_001")  --躺着摇着头悲叹自己的命运
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_wait_000","N1_CHDMGNZ01_wait_000")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_CHDMGNZ01_emo_001","N1_CHDMGNZ01_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_emo_002","N1_CHDMGNZ01_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_wait_000","N1_MTQS_wait_000")  --蒙古马头琴手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MTQS_emo_001","N1_MTQS_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_emo_002","N1_MTQS_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

-------------------------------塞外古城--------------------------

STake_Start("N1_wait_swgc_zhongdu_emo_001","N1_wait_008_emo_001")  
    BlendMode(0)
    BlendTime(0.2)
    Direction(2) 
	FrameTime(1.015,1.782)
	Loop(1.015,1.782)
    PlaySpeed(1.015,1.782,0.45)
    Fx(1.015,"FX_buff_poisoning","Head",0.8,1,40,0,0,0,0,0,1,1,1)



---------------------------------------------------------

STake_Start("N1_life_B_move_003","N1_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    
STake_Start("N1_life_B_move_004","YP_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 
    
STake_Start("N1_life_wait_000","N1_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("N1_life_B_wait_000","YP_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()   

STake_Start("N1_life_def_000","YP_def_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.235,3)
    Sound(0.00,"weap_Sword_block")
    Fx(0,"Fx_gedang_wuqi")   

    Sound(0.053,"YP_def_000")

STake_Start("N1_life_dodge_000","YP_dodge_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.352,3)       

    Sound(0.112,"N1_Axe_dodge_000") 
    
STake_Start("N1_life_attack_000","YP_attack_000")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    
  	--Hard(0.000,0.266)  	
  	
    AttackStart()
  	Hurt(0.240,"hurt_11")
    Sound(0.190,"YP_attack_000")
    HurtSound(0.240,"blade_Hit")

    Fx(0.000,"YP_attack_000_fire") 
    DirFx(0.240,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_001","YP_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    
    --Hard(0.000,0.334)  	
  	
    AttackStart()
  	Hurt(0.217,"hurt_11")
    Sound(0.167,"YP_attack_000")
    HurtSound(0.217,"blade_Hit")

    Fx(0.000,"YP_attack_001_fire") 
    DirFx(0.217,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_002","YP_attack_002")
    BlendMode(0)
    BlendTime(0.2)  
    Priority(0,"5") 
    
    --Hard(0.000,0.448) 
    
    AttackStart()
  	Hurt(0.205,"hurt_11")
    Sound(0.155,"YP_attack_000")
    HurtSound(0.205,"blade_Hit")    
    
    Fx(0.000,"YP_attack_002_fire")     
    DirFx(0.205,"YP_attack_hit",0,1)
    

    




STake_Start("N1_wait_000","N1_wait_000")  --N1标准待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

-----------------------------------------------------

STake_Start("N1_wait_001","N1_wait_001")  --蹲地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_002","N1_wait_002")  --坐姿待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_003","N1_wait_003")  --盘腿运气养神
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_004","N1_wait_004")  --躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_005","N1_wait_005")  --跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_006","N1_wait_006")  --轻受伤站立待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_007","N1_wait_007")  --重受伤跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008","N1_wait_008")  --受伤躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008_emo_001","N1_wait_008_emo_001")  --受伤躺地上说话
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_009","N1_wait_009")  --坐地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_010","N1_wait_010")  --手背背后跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011","N1_wait_011")  --说书先生说书
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011_emo_001","N1_wait_011_emo_001")  --说书先生喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_011_emo_002","N1_wait_011_emo_002")  --说书先生鞠躬
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012","N1_wait_012")  --屠夫切肉
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_012_emo_001","N1_wait_012_emo_001")  --屠夫切肉把肉归拢一边
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012_emo_002","N1_wait_012_emo_002")  --屠夫吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013","N1_wait_013")  --挑扁担货郎吆喝
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_013_emo_001","N1_wait_013_emo_001")  --挑扁担货郎擦汗
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013_emo_002","N1_wait_013_emo_002")  --挑扁担货郎把扁担放下休息
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_wait_013_move_001","N1_wait_013_move_001")  --挑扁担货郎走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_wait_014","N1_wait_014")  --大户人家护卫
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_014_emo_001","N1_wait_014_emo_001")  --大户人家护卫随机表情
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_emo_002","N1_wait_014_emo_002")  --大户人家护卫拦住去路
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_move_001","N1_wait_014_move_001")  --大户人家护卫走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015","N1_wait_015")  --马飞刷马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_emo_001","N1_wait_015_emo_001")  --马飞喂马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_move_001","N1_wait_015_move_001")  --马飞走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016","N1_wait_016")  --驿官待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016_emo_001","N1_wait_016_emo_001")  --驿官记账
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017","N1_wait_017")  --收钱艺人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_001","N1_wait_017_emo_001")  --收钱艺人敲锣
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_002","N1_wait_017_emo_002")  --收钱艺人坐下休息
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017_move_001","N1_wait_017_move_001")  --收钱艺人敲锣走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018","N1_wait_018")  --卖艺男批砖
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018_emo_001","N1_wait_018_emo_001")  --卖艺男抱拳行礼
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_emo_002","N1_wait_018_emo_002")  --卖艺男大声吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_move_001","N1_wait_018_move_001")  --卖艺男走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019","N1_wait_019")  --厨师切菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019_emo_001","N1_wait_019_emo_001")  --厨师切菜杂耍
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_020","N1_wait_020")  --厨师炒菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_020_emo_001","N1_wait_020_emo_001")  --厨师炒菜盛菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021","N1_wait_021")  --伙计端盘待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_021_emo_001","N1_wait_021_emo_001")  --伙计端盘上菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021_move_001","N1_wait_021_move_001")  --伙计端盘走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022","N1_wait_022")  --坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022_emo_001","N1_wait_022_emo_001")  --坐着等上菜喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_023","N1_wait_023")  --地痞坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_023_emo_001","N1_wait_023_emo_001")  --地痞坐着等上菜拍桌子
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_024","N1_wait_024")  --坐着夹菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_024_emo_001","N1_wait_024_emo_001")  --坐着吃饭
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_025","N1_wait_025")  --坐着喝茶
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_026","N1_wait_026")  --服务生打算盘
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_027","N1_wait_027")  --抱头躲藏
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028","N1_wait_028")  --扎马步waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028_emo_001","N1_wait_028_emo_001")  --扎马步emo
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_029","N1_wait_029")  --撒网捕鱼waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_029_emo_001","N1_wait_029_emo_001")  --撒网
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_030","N1_wait_030")  --坐着waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

------------------------------------------------------------

STake_Start("N1_move_000","N1_move_000")  --普通前走
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_F","N1_move_001_F")  --前跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_B","N1_move_001_B")  --后退
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_L","N1_move_001_L")  --左跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_R","N1_move_001_R")  --右跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_002","N1_move_002")  --举刀前跑冲锋
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_003","N1_move_003")  --左踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_004","N1_move_004")  --右踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    
STake_Start("N1_move_005","N1_move_005-007")  --起跳
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.8333)

STake_Start("N1_move_006","N1_move_006")  --空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_007","N1_move_005-007")  --落地
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.84,1.566)

STake_Start("N1_move_020","N1_move_020")  --惊恐跑步
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_021","N1_move_021")  --受伤走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

-----------------------------------------------------
STake_Start("N1_life_000","N1_life_000")  --站立伸懒腰
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_001","N1_life_001")  --站立左右看
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_002","N1_life_002")  --站立大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_003","N1_life_003")  --站立哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_004","N1_life_004")  --跪地哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_005","N1_life_005")  --打铁
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_006","N1_life_006")  --打招呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_007","N1_life_007")  --招呼客人
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_008","N1_life_008")  --普通说话
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_009","N1_life_009")  --惊恐
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_010","N1_life_010")  --耕地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_011","N1_life_011")  --插秧
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_012","N1_life_012")  --站着擦汗
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_013","N1_life_013")  --伐木
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_014","N1_life_014")  --修理
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_015","N1_life_015")  --死亡自杀
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.832,1.833)

STake_Start("N1_life_016","N1_life_016")  --坐着聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_017","N1_life_017")  --跪地磕头
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_019","N1_life_019")  --钓鱼待机
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_020","N1_life_020")  --钓鱼表情
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_021","N1_life_021")  --躲起来左右看
    BlendMode(0)
    BlendTime(0.2)
    
STake_Start("N1_life_022","N1_life_022")  --手背身后跪地左右张望
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_023","N1_life_023")  --手背身后跪地上哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_024","N1_life_024")  --受伤躺地呻吟
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_025","N1_life_025")  --受伤躺地挣扎
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_026","N1_life_026")  --欢呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_027","N1_life_027")  --坐地喝酒
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_028","N1_life_028")  --坐地聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_029","N1_life_029")  --坐地大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_030","N1_life_030")  --扫地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_031","N1_life_031")  --向前一指
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_032","N1_life_032")  --惊恐害怕
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_033","N1_life_033")  --打坐
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_034","N1_life_034")  --喝醉躺地
    BlendMode(0)
    BlendTime(0.2)
    Loop()


---------------------------------------

STake_Start("N1_bout_001","N1_bout_001")  --出生
    BlendMode(0)
    BlendTime(0)



STake_Start("N1_ZHLB01_wait_000","N1_ZHLB01_wait_000")  --账房老板
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_ZHLB01_emo_000","N1_ZHLB01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_ZHLB01_emo_001","N1_ZHLB01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_YS01_wait_000","N1_YS01_wait_000")  --药商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_YS01_emo_000","N1_YS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_YS01_emo_001","N1_YS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_WQS01_wait_000","N1_WQS01_wait_000")  --武器商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_WQS01_emo_000","N1_WQS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_WQS01_emo_001","N1_WQS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_wait_000","N1_LR01_wait_000")  --
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_LR01_emo_000","N1_LR01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_emo_001","N1_LR01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_wait_000","N1_JHYY01_wait_000")  --江湖游医
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHYY01_emo_000","N1_JHYY01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_emo_001","N1_JHYY01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_JHSS01_wait_000","N1_JHSS01_wait_000")  --江湖术士
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHSS01_emo_000","N1_JHSS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHSS01_emo_001","N1_JHSS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_wait_000","N1_DXE01_wait_000")  --店小二
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_DXE01_emo_000","N1_DXE01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_emo_001","N1_DXE01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM03_life_000","N1_CM03_life_000")  --村民03
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM03_life_001","N1_CM03_life_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM05_life_002","N1_CM05_life_002")  --村民05
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM05_life_003","N1_CM05_life_003")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_wait_000","N1_QZGJDZ01_wait_000")  --全真高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZGJDZ01_emo_001","N1_QZGJDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_emo_002","N1_QZGJDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_wait_000","N1_QZRMDZ01_wait_000")  --全真入门弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZRMDZ01_emo_001","N1_QZRMDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_emo_002","N1_QZRMDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_wait_000","N1_RS_wait_000")  --儒生
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_RS_emo_001","N1_RS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_emo_002","N1_RS_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_wait_000","N1_XinShi_wait_000")  --信使
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XinShi_emo_001","N1_XinShi_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_emo_002","N1_XinShi_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_wait_000","N1_MGN_wait_000")  --蒙古男
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_MGN_emo_001","N1_MGN_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_emo_002","N1_MGN_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSGJDZ_wait_000","N1_BTSGJDZ_wait_000")  --白驼高阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSGJDZ_emo_001","N1_BTSGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSGJDZ_emo_002","N1_BTSGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJJL_wait_000","N1_HAJJL_wait_000")  --红袄军将领
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJJL_emo_001","N1_HAJJL_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJJL_emo_002","N1_HAJJL_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJ01_wait_000","N1_HAJ01_wait_000")  --红袄军伤兵
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJ01_emo_001","N1_HAJ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJ01_emo_002","N1_HAJ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_wait_000","N1_BTSDJDZ_wait_000")  --白驼低阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSDJDZ_emo_001","N1_BTSDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSDJDZ_emo_002","N1_BTSDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_emo_003","N1_BTSDJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_wait_000","N1_BTSZJDZ_wait_000")  --白驼中阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_emo_001","N1_BTSZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSZJDZ_emo_002","N1_BTSZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSZJDZ_emo_003","N1_BTSZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_wait_000","N1_XYSl01_wait_000")  --西域商旅01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_emo_001","N1_XYSl01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl01_emo_002","N1_XYSl01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_XYSl02_wait_000","N1_XYSl02_wait_000")  --西域商旅02
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl02_emo_001","N1_XYSl02_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl02_emo_002","N1_XYSl02_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_SLSJQS_wait_000","N1_SLSJQS_wait_000")  --少林俗家拳师01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_SLSJQS_emo_001","N1_SLSJQS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_SLSJQS_emo_002","N1_SLSJQS_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSanDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮三袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSanDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSanDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSiDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮四袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSiDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSiDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBLDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮六袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBLDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBLDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBBDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮八袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBBDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBBDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDDJDZ_wait_000","N1_THDDJDZ_wait_000")  --桃花岛低阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDDJDZ_emo_001","N1_THDDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDDJDZ_emo_002","N1_THDDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_wait_000","N1_THDZJDZ_wait_000")  --桃花岛中阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDZJDZ_emo_001","N1_THDZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDZJDZ_emo_002","N1_THDZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_003","N1_THDZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_004","N1_THDZJDZ_emo_004")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDGJDZ_wait_000","N1_THDGJDZ_wait_000")  --桃花岛高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDGJDZ_emo_001","N1_THDGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDGJDZ_emo_002","N1_THDGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_MGSB_01_wait_000","N1_MGSB_01_wait_000")  --躺着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_wait_001","N1_MGSB_01_wait_001")  --蹲着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_emo_000","N1_MGSB_01_emo_000")  --躺着一边呻吟一边伸手求助
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGSB_01_emo_001","N1_MGSB_01_emo_001")  --躺着摇着头悲叹自己的命运
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_wait_000","N1_CHDMGNZ01_wait_000")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_CHDMGNZ01_emo_001","N1_CHDMGNZ01_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_emo_002","N1_CHDMGNZ01_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_wait_000","N1_MTQS_wait_000")  --蒙古马头琴手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MTQS_emo_001","N1_MTQS_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_emo_002","N1_MTQS_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

-------------------------------塞外古城--------------------------

STake_Start("N1_wait_swgc_zhongdu_emo_001","N1_wait_008_emo_001")  
    BlendMode(0)
    BlendTime(0.2)
    Direction(2) 
	FrameTime(1.015,1.782)
	Loop(1.015,1.782)
    PlaySpeed(1.015,1.782,0.45)
    Fx(1.015,"FX_buff_poisoning","Head",0.8,1,40,0,0,0,0,0,1,1,1)



---------------------------------------------------------

STake_Start("N1_life_B_move_003","N1_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    
STake_Start("N1_life_B_move_004","YP_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 
    
STake_Start("N1_life_wait_000","N1_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("N1_life_B_wait_000","YP_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()   

STake_Start("N1_life_def_000","YP_def_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.235,3)
    Sound(0.00,"weap_Sword_block")
    Fx(0,"Fx_gedang_wuqi")   

    Sound(0.053,"YP_def_000")

STake_Start("N1_life_dodge_000","YP_dodge_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.352,3)       

    Sound(0.112,"N1_Axe_dodge_000") 
    
STake_Start("N1_life_attack_000","YP_attack_000")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    
  	--Hard(0.000,0.266)  	
  	
    AttackStart()
  	Hurt(0.240,"hurt_11")
    Sound(0.190,"YP_attack_000")
    HurtSound(0.240,"blade_Hit")

    Fx(0.000,"YP_attack_000_fire") 
    DirFx(0.240,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_001","YP_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    
    --Hard(0.000,0.334)  	
  	
    AttackStart()
  	Hurt(0.217,"hurt_11")
    Sound(0.167,"YP_attack_000")
    HurtSound(0.217,"blade_Hit")

    Fx(0.000,"YP_attack_001_fire") 
    DirFx(0.217,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_002","YP_attack_002")
    BlendMode(0)
    BlendTime(0.2)  
    Priority(0,"5") 
    
    --Hard(0.000,0.448) 
    
    AttackStart()
  	Hurt(0.205,"hurt_11")
    Sound(0.155,"YP_attack_000")
    HurtSound(0.205,"blade_Hit")    
    
    Fx(0.000,"YP_attack_002_fire")     
    DirFx(0.205,"YP_attack_hit",0,1)
    

    




STake_Start("N1_wait_000","N1_wait_000")  --N1标准待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

-----------------------------------------------------

STake_Start("N1_wait_001","N1_wait_001")  --蹲地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_002","N1_wait_002")  --坐姿待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_003","N1_wait_003")  --盘腿运气养神
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_004","N1_wait_004")  --躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_005","N1_wait_005")  --跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_006","N1_wait_006")  --轻受伤站立待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_007","N1_wait_007")  --重受伤跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008","N1_wait_008")  --受伤躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008_emo_001","N1_wait_008_emo_001")  --受伤躺地上说话
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_009","N1_wait_009")  --坐地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_010","N1_wait_010")  --手背背后跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011","N1_wait_011")  --说书先生说书
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011_emo_001","N1_wait_011_emo_001")  --说书先生喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_011_emo_002","N1_wait_011_emo_002")  --说书先生鞠躬
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012","N1_wait_012")  --屠夫切肉
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_012_emo_001","N1_wait_012_emo_001")  --屠夫切肉把肉归拢一边
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012_emo_002","N1_wait_012_emo_002")  --屠夫吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013","N1_wait_013")  --挑扁担货郎吆喝
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_013_emo_001","N1_wait_013_emo_001")  --挑扁担货郎擦汗
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013_emo_002","N1_wait_013_emo_002")  --挑扁担货郎把扁担放下休息
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_wait_013_move_001","N1_wait_013_move_001")  --挑扁担货郎走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_wait_014","N1_wait_014")  --大户人家护卫
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_014_emo_001","N1_wait_014_emo_001")  --大户人家护卫随机表情
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_emo_002","N1_wait_014_emo_002")  --大户人家护卫拦住去路
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_move_001","N1_wait_014_move_001")  --大户人家护卫走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015","N1_wait_015")  --马飞刷马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_emo_001","N1_wait_015_emo_001")  --马飞喂马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_move_001","N1_wait_015_move_001")  --马飞走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016","N1_wait_016")  --驿官待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016_emo_001","N1_wait_016_emo_001")  --驿官记账
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017","N1_wait_017")  --收钱艺人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_001","N1_wait_017_emo_001")  --收钱艺人敲锣
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_002","N1_wait_017_emo_002")  --收钱艺人坐下休息
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017_move_001","N1_wait_017_move_001")  --收钱艺人敲锣走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018","N1_wait_018")  --卖艺男批砖
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018_emo_001","N1_wait_018_emo_001")  --卖艺男抱拳行礼
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_emo_002","N1_wait_018_emo_002")  --卖艺男大声吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_move_001","N1_wait_018_move_001")  --卖艺男走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019","N1_wait_019")  --厨师切菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019_emo_001","N1_wait_019_emo_001")  --厨师切菜杂耍
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_020","N1_wait_020")  --厨师炒菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_020_emo_001","N1_wait_020_emo_001")  --厨师炒菜盛菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021","N1_wait_021")  --伙计端盘待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_021_emo_001","N1_wait_021_emo_001")  --伙计端盘上菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021_move_001","N1_wait_021_move_001")  --伙计端盘走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022","N1_wait_022")  --坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022_emo_001","N1_wait_022_emo_001")  --坐着等上菜喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_023","N1_wait_023")  --地痞坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_023_emo_001","N1_wait_023_emo_001")  --地痞坐着等上菜拍桌子
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_024","N1_wait_024")  --坐着夹菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_024_emo_001","N1_wait_024_emo_001")  --坐着吃饭
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_025","N1_wait_025")  --坐着喝茶
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_026","N1_wait_026")  --服务生打算盘
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_027","N1_wait_027")  --抱头躲藏
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028","N1_wait_028")  --扎马步waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028_emo_001","N1_wait_028_emo_001")  --扎马步emo
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_029","N1_wait_029")  --撒网捕鱼waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_029_emo_001","N1_wait_029_emo_001")  --撒网
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_030","N1_wait_030")  --坐着waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

------------------------------------------------------------

STake_Start("N1_move_000","N1_move_000")  --普通前走
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_F","N1_move_001_F")  --前跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_B","N1_move_001_B")  --后退
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_L","N1_move_001_L")  --左跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_R","N1_move_001_R")  --右跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_002","N1_move_002")  --举刀前跑冲锋
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_003","N1_move_003")  --左踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_004","N1_move_004")  --右踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    
STake_Start("N1_move_005","N1_move_005-007")  --起跳
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.8333)

STake_Start("N1_move_006","N1_move_006")  --空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_007","N1_move_005-007")  --落地
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.84,1.566)

STake_Start("N1_move_020","N1_move_020")  --惊恐跑步
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_021","N1_move_021")  --受伤走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

-----------------------------------------------------
STake_Start("N1_life_000","N1_life_000")  --站立伸懒腰
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_001","N1_life_001")  --站立左右看
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_002","N1_life_002")  --站立大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_003","N1_life_003")  --站立哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_004","N1_life_004")  --跪地哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_005","N1_life_005")  --打铁
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_006","N1_life_006")  --打招呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_007","N1_life_007")  --招呼客人
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_008","N1_life_008")  --普通说话
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_009","N1_life_009")  --惊恐
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_010","N1_life_010")  --耕地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_011","N1_life_011")  --插秧
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_012","N1_life_012")  --站着擦汗
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_013","N1_life_013")  --伐木
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_014","N1_life_014")  --修理
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_015","N1_life_015")  --死亡自杀
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.832,1.833)

STake_Start("N1_life_016","N1_life_016")  --坐着聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_017","N1_life_017")  --跪地磕头
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_019","N1_life_019")  --钓鱼待机
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_020","N1_life_020")  --钓鱼表情
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_021","N1_life_021")  --躲起来左右看
    BlendMode(0)
    BlendTime(0.2)
    
STake_Start("N1_life_022","N1_life_022")  --手背身后跪地左右张望
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_023","N1_life_023")  --手背身后跪地上哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_024","N1_life_024")  --受伤躺地呻吟
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_025","N1_life_025")  --受伤躺地挣扎
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_026","N1_life_026")  --欢呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_027","N1_life_027")  --坐地喝酒
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_028","N1_life_028")  --坐地聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_029","N1_life_029")  --坐地大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_030","N1_life_030")  --扫地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_031","N1_life_031")  --向前一指
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_032","N1_life_032")  --惊恐害怕
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_033","N1_life_033")  --打坐
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_034","N1_life_034")  --喝醉躺地
    BlendMode(0)
    BlendTime(0.2)
    Loop()


---------------------------------------

STake_Start("N1_bout_001","N1_bout_001")  --出生
    BlendMode(0)
    BlendTime(0)



STake_Start("N1_ZHLB01_wait_000","N1_ZHLB01_wait_000")  --账房老板
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_ZHLB01_emo_000","N1_ZHLB01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_ZHLB01_emo_001","N1_ZHLB01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_YS01_wait_000","N1_YS01_wait_000")  --药商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_YS01_emo_000","N1_YS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_YS01_emo_001","N1_YS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_WQS01_wait_000","N1_WQS01_wait_000")  --武器商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_WQS01_emo_000","N1_WQS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_WQS01_emo_001","N1_WQS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_wait_000","N1_LR01_wait_000")  --
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_LR01_emo_000","N1_LR01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_emo_001","N1_LR01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_wait_000","N1_JHYY01_wait_000")  --江湖游医
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHYY01_emo_000","N1_JHYY01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_emo_001","N1_JHYY01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_JHSS01_wait_000","N1_JHSS01_wait_000")  --江湖术士
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHSS01_emo_000","N1_JHSS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHSS01_emo_001","N1_JHSS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_wait_000","N1_DXE01_wait_000")  --店小二
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_DXE01_emo_000","N1_DXE01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_emo_001","N1_DXE01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM03_life_000","N1_CM03_life_000")  --村民03
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM03_life_001","N1_CM03_life_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM05_life_002","N1_CM05_life_002")  --村民05
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM05_life_003","N1_CM05_life_003")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_wait_000","N1_QZGJDZ01_wait_000")  --全真高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZGJDZ01_emo_001","N1_QZGJDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_emo_002","N1_QZGJDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_wait_000","N1_QZRMDZ01_wait_000")  --全真入门弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZRMDZ01_emo_001","N1_QZRMDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_emo_002","N1_QZRMDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_wait_000","N1_RS_wait_000")  --儒生
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_RS_emo_001","N1_RS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_emo_002","N1_RS_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_wait_000","N1_XinShi_wait_000")  --信使
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XinShi_emo_001","N1_XinShi_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_emo_002","N1_XinShi_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_wait_000","N1_MGN_wait_000")  --蒙古男
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_MGN_emo_001","N1_MGN_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_emo_002","N1_MGN_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSGJDZ_wait_000","N1_BTSGJDZ_wait_000")  --白驼高阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSGJDZ_emo_001","N1_BTSGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSGJDZ_emo_002","N1_BTSGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJJL_wait_000","N1_HAJJL_wait_000")  --红袄军将领
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJJL_emo_001","N1_HAJJL_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJJL_emo_002","N1_HAJJL_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJ01_wait_000","N1_HAJ01_wait_000")  --红袄军伤兵
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJ01_emo_001","N1_HAJ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJ01_emo_002","N1_HAJ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_wait_000","N1_BTSDJDZ_wait_000")  --白驼低阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSDJDZ_emo_001","N1_BTSDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSDJDZ_emo_002","N1_BTSDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_emo_003","N1_BTSDJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_wait_000","N1_BTSZJDZ_wait_000")  --白驼中阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_emo_001","N1_BTSZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSZJDZ_emo_002","N1_BTSZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSZJDZ_emo_003","N1_BTSZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_wait_000","N1_XYSl01_wait_000")  --西域商旅01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_emo_001","N1_XYSl01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl01_emo_002","N1_XYSl01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_XYSl02_wait_000","N1_XYSl02_wait_000")  --西域商旅02
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl02_emo_001","N1_XYSl02_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl02_emo_002","N1_XYSl02_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_SLSJQS_wait_000","N1_SLSJQS_wait_000")  --少林俗家拳师01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_SLSJQS_emo_001","N1_SLSJQS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_SLSJQS_emo_002","N1_SLSJQS_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSanDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮三袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSanDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSanDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSiDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮四袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSiDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSiDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBLDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮六袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBLDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBLDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBBDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮八袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBBDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBBDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDDJDZ_wait_000","N1_THDDJDZ_wait_000")  --桃花岛低阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDDJDZ_emo_001","N1_THDDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDDJDZ_emo_002","N1_THDDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_wait_000","N1_THDZJDZ_wait_000")  --桃花岛中阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDZJDZ_emo_001","N1_THDZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDZJDZ_emo_002","N1_THDZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_003","N1_THDZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_004","N1_THDZJDZ_emo_004")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDGJDZ_wait_000","N1_THDGJDZ_wait_000")  --桃花岛高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDGJDZ_emo_001","N1_THDGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDGJDZ_emo_002","N1_THDGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_MGSB_01_wait_000","N1_MGSB_01_wait_000")  --躺着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_wait_001","N1_MGSB_01_wait_001")  --蹲着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_emo_000","N1_MGSB_01_emo_000")  --躺着一边呻吟一边伸手求助
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGSB_01_emo_001","N1_MGSB_01_emo_001")  --躺着摇着头悲叹自己的命运
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_wait_000","N1_CHDMGNZ01_wait_000")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_CHDMGNZ01_emo_001","N1_CHDMGNZ01_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_emo_002","N1_CHDMGNZ01_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_wait_000","N1_MTQS_wait_000")  --蒙古马头琴手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MTQS_emo_001","N1_MTQS_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_emo_002","N1_MTQS_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

-------------------------------塞外古城--------------------------

STake_Start("N1_wait_swgc_zhongdu_emo_001","N1_wait_008_emo_001")  
    BlendMode(0)
    BlendTime(0.2)
    Direction(2) 
	FrameTime(1.015,1.782)
	Loop(1.015,1.782)
    PlaySpeed(1.015,1.782,0.45)
    Fx(1.015,"FX_buff_poisoning","Head",0.8,1,40,0,0,0,0,0,1,1,1)



---------------------------------------------------------

STake_Start("N1_life_B_move_003","N1_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    
STake_Start("N1_life_B_move_004","YP_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 
    
STake_Start("N1_life_wait_000","N1_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("N1_life_B_wait_000","YP_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()   

STake_Start("N1_life_def_000","YP_def_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.235,3)
    Sound(0.00,"weap_Sword_block")
    Fx(0,"Fx_gedang_wuqi")   

    Sound(0.053,"YP_def_000")

STake_Start("N1_life_dodge_000","YP_dodge_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.352,3)       

    Sound(0.112,"N1_Axe_dodge_000") 
    
STake_Start("N1_life_attack_000","YP_attack_000")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    
  	--Hard(0.000,0.266)  	
  	
    AttackStart()
  	Hurt(0.240,"hurt_11")
    Sound(0.190,"YP_attack_000")
    HurtSound(0.240,"blade_Hit")

    Fx(0.000,"YP_attack_000_fire") 
    DirFx(0.240,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_001","YP_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    
    --Hard(0.000,0.334)  	
  	
    AttackStart()
  	Hurt(0.217,"hurt_11")
    Sound(0.167,"YP_attack_000")
    HurtSound(0.217,"blade_Hit")

    Fx(0.000,"YP_attack_001_fire") 
    DirFx(0.217,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_002","YP_attack_002")
    BlendMode(0)
    BlendTime(0.2)  
    Priority(0,"5") 
    
    --Hard(0.000,0.448) 
    
    AttackStart()
  	Hurt(0.205,"hurt_11")
    Sound(0.155,"YP_attack_000")
    HurtSound(0.205,"blade_Hit")    
    
    Fx(0.000,"YP_attack_002_fire")     
    DirFx(0.205,"YP_attack_hit",0,1)
    

    




STake_Start("N1_wait_000","N1_wait_000")  --N1标准待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

-----------------------------------------------------

STake_Start("N1_wait_001","N1_wait_001")  --蹲地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_002","N1_wait_002")  --坐姿待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_003","N1_wait_003")  --盘腿运气养神
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_004","N1_wait_004")  --躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_005","N1_wait_005")  --跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_006","N1_wait_006")  --轻受伤站立待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_007","N1_wait_007")  --重受伤跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008","N1_wait_008")  --受伤躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008_emo_001","N1_wait_008_emo_001")  --受伤躺地上说话
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_009","N1_wait_009")  --坐地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_010","N1_wait_010")  --手背背后跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011","N1_wait_011")  --说书先生说书
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011_emo_001","N1_wait_011_emo_001")  --说书先生喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_011_emo_002","N1_wait_011_emo_002")  --说书先生鞠躬
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012","N1_wait_012")  --屠夫切肉
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_012_emo_001","N1_wait_012_emo_001")  --屠夫切肉把肉归拢一边
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012_emo_002","N1_wait_012_emo_002")  --屠夫吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013","N1_wait_013")  --挑扁担货郎吆喝
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_013_emo_001","N1_wait_013_emo_001")  --挑扁担货郎擦汗
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013_emo_002","N1_wait_013_emo_002")  --挑扁担货郎把扁担放下休息
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_wait_013_move_001","N1_wait_013_move_001")  --挑扁担货郎走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_wait_014","N1_wait_014")  --大户人家护卫
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_014_emo_001","N1_wait_014_emo_001")  --大户人家护卫随机表情
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_emo_002","N1_wait_014_emo_002")  --大户人家护卫拦住去路
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_move_001","N1_wait_014_move_001")  --大户人家护卫走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015","N1_wait_015")  --马飞刷马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_emo_001","N1_wait_015_emo_001")  --马飞喂马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_move_001","N1_wait_015_move_001")  --马飞走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016","N1_wait_016")  --驿官待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016_emo_001","N1_wait_016_emo_001")  --驿官记账
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017","N1_wait_017")  --收钱艺人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_001","N1_wait_017_emo_001")  --收钱艺人敲锣
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_002","N1_wait_017_emo_002")  --收钱艺人坐下休息
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017_move_001","N1_wait_017_move_001")  --收钱艺人敲锣走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018","N1_wait_018")  --卖艺男批砖
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018_emo_001","N1_wait_018_emo_001")  --卖艺男抱拳行礼
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_emo_002","N1_wait_018_emo_002")  --卖艺男大声吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_move_001","N1_wait_018_move_001")  --卖艺男走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019","N1_wait_019")  --厨师切菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019_emo_001","N1_wait_019_emo_001")  --厨师切菜杂耍
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_020","N1_wait_020")  --厨师炒菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_020_emo_001","N1_wait_020_emo_001")  --厨师炒菜盛菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021","N1_wait_021")  --伙计端盘待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_021_emo_001","N1_wait_021_emo_001")  --伙计端盘上菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021_move_001","N1_wait_021_move_001")  --伙计端盘走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022","N1_wait_022")  --坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022_emo_001","N1_wait_022_emo_001")  --坐着等上菜喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_023","N1_wait_023")  --地痞坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_023_emo_001","N1_wait_023_emo_001")  --地痞坐着等上菜拍桌子
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_024","N1_wait_024")  --坐着夹菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_024_emo_001","N1_wait_024_emo_001")  --坐着吃饭
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_025","N1_wait_025")  --坐着喝茶
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_026","N1_wait_026")  --服务生打算盘
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_027","N1_wait_027")  --抱头躲藏
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028","N1_wait_028")  --扎马步waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028_emo_001","N1_wait_028_emo_001")  --扎马步emo
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_029","N1_wait_029")  --撒网捕鱼waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_029_emo_001","N1_wait_029_emo_001")  --撒网
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_030","N1_wait_030")  --坐着waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

------------------------------------------------------------

STake_Start("N1_move_000","N1_move_000")  --普通前走
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_F","N1_move_001_F")  --前跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_B","N1_move_001_B")  --后退
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_L","N1_move_001_L")  --左跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_R","N1_move_001_R")  --右跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_002","N1_move_002")  --举刀前跑冲锋
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_003","N1_move_003")  --左踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_004","N1_move_004")  --右踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    
STake_Start("N1_move_005","N1_move_005-007")  --起跳
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.8333)

STake_Start("N1_move_006","N1_move_006")  --空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_007","N1_move_005-007")  --落地
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.84,1.566)

STake_Start("N1_move_020","N1_move_020")  --惊恐跑步
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_021","N1_move_021")  --受伤走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

-----------------------------------------------------
STake_Start("N1_life_000","N1_life_000")  --站立伸懒腰
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_001","N1_life_001")  --站立左右看
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_002","N1_life_002")  --站立大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_003","N1_life_003")  --站立哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_004","N1_life_004")  --跪地哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_005","N1_life_005")  --打铁
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_006","N1_life_006")  --打招呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_007","N1_life_007")  --招呼客人
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_008","N1_life_008")  --普通说话
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_009","N1_life_009")  --惊恐
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_010","N1_life_010")  --耕地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_011","N1_life_011")  --插秧
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_012","N1_life_012")  --站着擦汗
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_013","N1_life_013")  --伐木
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_014","N1_life_014")  --修理
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_015","N1_life_015")  --死亡自杀
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.832,1.833)

STake_Start("N1_life_016","N1_life_016")  --坐着聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_017","N1_life_017")  --跪地磕头
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_019","N1_life_019")  --钓鱼待机
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_020","N1_life_020")  --钓鱼表情
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_021","N1_life_021")  --躲起来左右看
    BlendMode(0)
    BlendTime(0.2)
    
STake_Start("N1_life_022","N1_life_022")  --手背身后跪地左右张望
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_023","N1_life_023")  --手背身后跪地上哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_024","N1_life_024")  --受伤躺地呻吟
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_025","N1_life_025")  --受伤躺地挣扎
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_026","N1_life_026")  --欢呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_027","N1_life_027")  --坐地喝酒
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_028","N1_life_028")  --坐地聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_029","N1_life_029")  --坐地大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_030","N1_life_030")  --扫地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_031","N1_life_031")  --向前一指
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_032","N1_life_032")  --惊恐害怕
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_033","N1_life_033")  --打坐
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_034","N1_life_034")  --喝醉躺地
    BlendMode(0)
    BlendTime(0.2)
    Loop()


---------------------------------------

STake_Start("N1_bout_001","N1_bout_001")  --出生
    BlendMode(0)
    BlendTime(0)



STake_Start("N1_ZHLB01_wait_000","N1_ZHLB01_wait_000")  --账房老板
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_ZHLB01_emo_000","N1_ZHLB01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_ZHLB01_emo_001","N1_ZHLB01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_YS01_wait_000","N1_YS01_wait_000")  --药商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_YS01_emo_000","N1_YS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_YS01_emo_001","N1_YS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_WQS01_wait_000","N1_WQS01_wait_000")  --武器商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_WQS01_emo_000","N1_WQS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_WQS01_emo_001","N1_WQS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_wait_000","N1_LR01_wait_000")  --
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_LR01_emo_000","N1_LR01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_emo_001","N1_LR01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_wait_000","N1_JHYY01_wait_000")  --江湖游医
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHYY01_emo_000","N1_JHYY01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_emo_001","N1_JHYY01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_JHSS01_wait_000","N1_JHSS01_wait_000")  --江湖术士
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHSS01_emo_000","N1_JHSS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHSS01_emo_001","N1_JHSS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_wait_000","N1_DXE01_wait_000")  --店小二
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_DXE01_emo_000","N1_DXE01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_emo_001","N1_DXE01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM03_life_000","N1_CM03_life_000")  --村民03
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM03_life_001","N1_CM03_life_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM05_life_002","N1_CM05_life_002")  --村民05
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM05_life_003","N1_CM05_life_003")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_wait_000","N1_QZGJDZ01_wait_000")  --全真高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZGJDZ01_emo_001","N1_QZGJDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_emo_002","N1_QZGJDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_wait_000","N1_QZRMDZ01_wait_000")  --全真入门弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZRMDZ01_emo_001","N1_QZRMDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_emo_002","N1_QZRMDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_wait_000","N1_RS_wait_000")  --儒生
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_RS_emo_001","N1_RS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_emo_002","N1_RS_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_wait_000","N1_XinShi_wait_000")  --信使
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XinShi_emo_001","N1_XinShi_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_emo_002","N1_XinShi_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_wait_000","N1_MGN_wait_000")  --蒙古男
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_MGN_emo_001","N1_MGN_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_emo_002","N1_MGN_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSGJDZ_wait_000","N1_BTSGJDZ_wait_000")  --白驼高阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSGJDZ_emo_001","N1_BTSGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSGJDZ_emo_002","N1_BTSGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJJL_wait_000","N1_HAJJL_wait_000")  --红袄军将领
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJJL_emo_001","N1_HAJJL_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJJL_emo_002","N1_HAJJL_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJ01_wait_000","N1_HAJ01_wait_000")  --红袄军伤兵
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJ01_emo_001","N1_HAJ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJ01_emo_002","N1_HAJ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_wait_000","N1_BTSDJDZ_wait_000")  --白驼低阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSDJDZ_emo_001","N1_BTSDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSDJDZ_emo_002","N1_BTSDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_emo_003","N1_BTSDJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_wait_000","N1_BTSZJDZ_wait_000")  --白驼中阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_emo_001","N1_BTSZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSZJDZ_emo_002","N1_BTSZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSZJDZ_emo_003","N1_BTSZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_wait_000","N1_XYSl01_wait_000")  --西域商旅01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_emo_001","N1_XYSl01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl01_emo_002","N1_XYSl01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_XYSl02_wait_000","N1_XYSl02_wait_000")  --西域商旅02
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl02_emo_001","N1_XYSl02_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl02_emo_002","N1_XYSl02_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_SLSJQS_wait_000","N1_SLSJQS_wait_000")  --少林俗家拳师01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_SLSJQS_emo_001","N1_SLSJQS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_SLSJQS_emo_002","N1_SLSJQS_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSanDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮三袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSanDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSanDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSiDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮四袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSiDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSiDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBLDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮六袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBLDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBLDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBBDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮八袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBBDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBBDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDDJDZ_wait_000","N1_THDDJDZ_wait_000")  --桃花岛低阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDDJDZ_emo_001","N1_THDDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDDJDZ_emo_002","N1_THDDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_wait_000","N1_THDZJDZ_wait_000")  --桃花岛中阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDZJDZ_emo_001","N1_THDZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDZJDZ_emo_002","N1_THDZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_003","N1_THDZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_004","N1_THDZJDZ_emo_004")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDGJDZ_wait_000","N1_THDGJDZ_wait_000")  --桃花岛高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDGJDZ_emo_001","N1_THDGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDGJDZ_emo_002","N1_THDGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_MGSB_01_wait_000","N1_MGSB_01_wait_000")  --躺着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_wait_001","N1_MGSB_01_wait_001")  --蹲着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_emo_000","N1_MGSB_01_emo_000")  --躺着一边呻吟一边伸手求助
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGSB_01_emo_001","N1_MGSB_01_emo_001")  --躺着摇着头悲叹自己的命运
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_wait_000","N1_CHDMGNZ01_wait_000")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_CHDMGNZ01_emo_001","N1_CHDMGNZ01_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_emo_002","N1_CHDMGNZ01_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_wait_000","N1_MTQS_wait_000")  --蒙古马头琴手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MTQS_emo_001","N1_MTQS_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_emo_002","N1_MTQS_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

-------------------------------塞外古城--------------------------

STake_Start("N1_wait_swgc_zhongdu_emo_001","N1_wait_008_emo_001")  
    BlendMode(0)
    BlendTime(0.2)
    Direction(2) 
	FrameTime(1.015,1.782)
	Loop(1.015,1.782)
    PlaySpeed(1.015,1.782,0.45)
    Fx(1.015,"FX_buff_poisoning","Head",0.8,1,40,0,0,0,0,0,1,1,1)



---------------------------------------------------------

STake_Start("N1_life_B_move_003","N1_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    
STake_Start("N1_life_B_move_004","YP_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 
    
STake_Start("N1_life_wait_000","N1_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("N1_life_B_wait_000","YP_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()   

STake_Start("N1_life_def_000","YP_def_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.235,3)
    Sound(0.00,"weap_Sword_block")
    Fx(0,"Fx_gedang_wuqi")   

    Sound(0.053,"YP_def_000")

STake_Start("N1_life_dodge_000","YP_dodge_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.352,3)       

    Sound(0.112,"N1_Axe_dodge_000") 
    
STake_Start("N1_life_attack_000","YP_attack_000")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    
  	--Hard(0.000,0.266)  	
  	
    AttackStart()
  	Hurt(0.240,"hurt_11")
    Sound(0.190,"YP_attack_000")
    HurtSound(0.240,"blade_Hit")

    Fx(0.000,"YP_attack_000_fire") 
    DirFx(0.240,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_001","YP_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    
    --Hard(0.000,0.334)  	
  	
    AttackStart()
  	Hurt(0.217,"hurt_11")
    Sound(0.167,"YP_attack_000")
    HurtSound(0.217,"blade_Hit")

    Fx(0.000,"YP_attack_001_fire") 
    DirFx(0.217,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_002","YP_attack_002")
    BlendMode(0)
    BlendTime(0.2)  
    Priority(0,"5") 
    
    --Hard(0.000,0.448) 
    
    AttackStart()
  	Hurt(0.205,"hurt_11")
    Sound(0.155,"YP_attack_000")
    HurtSound(0.205,"blade_Hit")    
    
    Fx(0.000,"YP_attack_002_fire")     
    DirFx(0.205,"YP_attack_hit",0,1)
    

    




STake_Start("N1_wait_000","N1_wait_000")  --N1标准待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

-----------------------------------------------------

STake_Start("N1_wait_001","N1_wait_001")  --蹲地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_002","N1_wait_002")  --坐姿待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_003","N1_wait_003")  --盘腿运气养神
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_004","N1_wait_004")  --躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_005","N1_wait_005")  --跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_006","N1_wait_006")  --轻受伤站立待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_007","N1_wait_007")  --重受伤跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008","N1_wait_008")  --受伤躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008_emo_001","N1_wait_008_emo_001")  --受伤躺地上说话
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_009","N1_wait_009")  --坐地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_010","N1_wait_010")  --手背背后跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011","N1_wait_011")  --说书先生说书
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011_emo_001","N1_wait_011_emo_001")  --说书先生喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_011_emo_002","N1_wait_011_emo_002")  --说书先生鞠躬
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012","N1_wait_012")  --屠夫切肉
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_012_emo_001","N1_wait_012_emo_001")  --屠夫切肉把肉归拢一边
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012_emo_002","N1_wait_012_emo_002")  --屠夫吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013","N1_wait_013")  --挑扁担货郎吆喝
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_013_emo_001","N1_wait_013_emo_001")  --挑扁担货郎擦汗
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013_emo_002","N1_wait_013_emo_002")  --挑扁担货郎把扁担放下休息
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_wait_013_move_001","N1_wait_013_move_001")  --挑扁担货郎走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_wait_014","N1_wait_014")  --大户人家护卫
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_014_emo_001","N1_wait_014_emo_001")  --大户人家护卫随机表情
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_emo_002","N1_wait_014_emo_002")  --大户人家护卫拦住去路
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_move_001","N1_wait_014_move_001")  --大户人家护卫走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015","N1_wait_015")  --马飞刷马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_emo_001","N1_wait_015_emo_001")  --马飞喂马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_move_001","N1_wait_015_move_001")  --马飞走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016","N1_wait_016")  --驿官待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016_emo_001","N1_wait_016_emo_001")  --驿官记账
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017","N1_wait_017")  --收钱艺人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_001","N1_wait_017_emo_001")  --收钱艺人敲锣
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_002","N1_wait_017_emo_002")  --收钱艺人坐下休息
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017_move_001","N1_wait_017_move_001")  --收钱艺人敲锣走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018","N1_wait_018")  --卖艺男批砖
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018_emo_001","N1_wait_018_emo_001")  --卖艺男抱拳行礼
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_emo_002","N1_wait_018_emo_002")  --卖艺男大声吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_move_001","N1_wait_018_move_001")  --卖艺男走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019","N1_wait_019")  --厨师切菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019_emo_001","N1_wait_019_emo_001")  --厨师切菜杂耍
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_020","N1_wait_020")  --厨师炒菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_020_emo_001","N1_wait_020_emo_001")  --厨师炒菜盛菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021","N1_wait_021")  --伙计端盘待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_021_emo_001","N1_wait_021_emo_001")  --伙计端盘上菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021_move_001","N1_wait_021_move_001")  --伙计端盘走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022","N1_wait_022")  --坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022_emo_001","N1_wait_022_emo_001")  --坐着等上菜喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_023","N1_wait_023")  --地痞坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_023_emo_001","N1_wait_023_emo_001")  --地痞坐着等上菜拍桌子
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_024","N1_wait_024")  --坐着夹菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_024_emo_001","N1_wait_024_emo_001")  --坐着吃饭
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_025","N1_wait_025")  --坐着喝茶
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_026","N1_wait_026")  --服务生打算盘
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_027","N1_wait_027")  --抱头躲藏
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028","N1_wait_028")  --扎马步waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028_emo_001","N1_wait_028_emo_001")  --扎马步emo
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_029","N1_wait_029")  --撒网捕鱼waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_029_emo_001","N1_wait_029_emo_001")  --撒网
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_030","N1_wait_030")  --坐着waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

------------------------------------------------------------

STake_Start("N1_move_000","N1_move_000")  --普通前走
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_F","N1_move_001_F")  --前跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_B","N1_move_001_B")  --后退
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_L","N1_move_001_L")  --左跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_R","N1_move_001_R")  --右跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_002","N1_move_002")  --举刀前跑冲锋
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_003","N1_move_003")  --左踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_004","N1_move_004")  --右踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    
STake_Start("N1_move_005","N1_move_005-007")  --起跳
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.8333)

STake_Start("N1_move_006","N1_move_006")  --空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_007","N1_move_005-007")  --落地
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.84,1.566)

STake_Start("N1_move_020","N1_move_020")  --惊恐跑步
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_021","N1_move_021")  --受伤走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

-----------------------------------------------------
STake_Start("N1_life_000","N1_life_000")  --站立伸懒腰
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_001","N1_life_001")  --站立左右看
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_002","N1_life_002")  --站立大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_003","N1_life_003")  --站立哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_004","N1_life_004")  --跪地哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_005","N1_life_005")  --打铁
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_006","N1_life_006")  --打招呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_007","N1_life_007")  --招呼客人
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_008","N1_life_008")  --普通说话
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_009","N1_life_009")  --惊恐
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_010","N1_life_010")  --耕地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_011","N1_life_011")  --插秧
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_012","N1_life_012")  --站着擦汗
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_013","N1_life_013")  --伐木
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_014","N1_life_014")  --修理
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_015","N1_life_015")  --死亡自杀
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.832,1.833)

STake_Start("N1_life_016","N1_life_016")  --坐着聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_017","N1_life_017")  --跪地磕头
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_019","N1_life_019")  --钓鱼待机
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_020","N1_life_020")  --钓鱼表情
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_021","N1_life_021")  --躲起来左右看
    BlendMode(0)
    BlendTime(0.2)
    
STake_Start("N1_life_022","N1_life_022")  --手背身后跪地左右张望
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_023","N1_life_023")  --手背身后跪地上哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_024","N1_life_024")  --受伤躺地呻吟
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_025","N1_life_025")  --受伤躺地挣扎
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_026","N1_life_026")  --欢呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_027","N1_life_027")  --坐地喝酒
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_028","N1_life_028")  --坐地聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_029","N1_life_029")  --坐地大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_030","N1_life_030")  --扫地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_031","N1_life_031")  --向前一指
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_032","N1_life_032")  --惊恐害怕
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_033","N1_life_033")  --打坐
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_034","N1_life_034")  --喝醉躺地
    BlendMode(0)
    BlendTime(0.2)
    Loop()


---------------------------------------

STake_Start("N1_bout_001","N1_bout_001")  --出生
    BlendMode(0)
    BlendTime(0)



STake_Start("N1_ZHLB01_wait_000","N1_ZHLB01_wait_000")  --账房老板
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_ZHLB01_emo_000","N1_ZHLB01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_ZHLB01_emo_001","N1_ZHLB01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_YS01_wait_000","N1_YS01_wait_000")  --药商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_YS01_emo_000","N1_YS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_YS01_emo_001","N1_YS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_WQS01_wait_000","N1_WQS01_wait_000")  --武器商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_WQS01_emo_000","N1_WQS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_WQS01_emo_001","N1_WQS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_wait_000","N1_LR01_wait_000")  --
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_LR01_emo_000","N1_LR01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_emo_001","N1_LR01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_wait_000","N1_JHYY01_wait_000")  --江湖游医
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHYY01_emo_000","N1_JHYY01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_emo_001","N1_JHYY01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_JHSS01_wait_000","N1_JHSS01_wait_000")  --江湖术士
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHSS01_emo_000","N1_JHSS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHSS01_emo_001","N1_JHSS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_wait_000","N1_DXE01_wait_000")  --店小二
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_DXE01_emo_000","N1_DXE01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_emo_001","N1_DXE01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM03_life_000","N1_CM03_life_000")  --村民03
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM03_life_001","N1_CM03_life_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM05_life_002","N1_CM05_life_002")  --村民05
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM05_life_003","N1_CM05_life_003")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_wait_000","N1_QZGJDZ01_wait_000")  --全真高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZGJDZ01_emo_001","N1_QZGJDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_emo_002","N1_QZGJDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_wait_000","N1_QZRMDZ01_wait_000")  --全真入门弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZRMDZ01_emo_001","N1_QZRMDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_emo_002","N1_QZRMDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_wait_000","N1_RS_wait_000")  --儒生
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_RS_emo_001","N1_RS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_emo_002","N1_RS_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_wait_000","N1_XinShi_wait_000")  --信使
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XinShi_emo_001","N1_XinShi_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_emo_002","N1_XinShi_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_wait_000","N1_MGN_wait_000")  --蒙古男
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_MGN_emo_001","N1_MGN_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_emo_002","N1_MGN_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSGJDZ_wait_000","N1_BTSGJDZ_wait_000")  --白驼高阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSGJDZ_emo_001","N1_BTSGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSGJDZ_emo_002","N1_BTSGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJJL_wait_000","N1_HAJJL_wait_000")  --红袄军将领
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJJL_emo_001","N1_HAJJL_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJJL_emo_002","N1_HAJJL_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJ01_wait_000","N1_HAJ01_wait_000")  --红袄军伤兵
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJ01_emo_001","N1_HAJ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJ01_emo_002","N1_HAJ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_wait_000","N1_BTSDJDZ_wait_000")  --白驼低阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSDJDZ_emo_001","N1_BTSDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSDJDZ_emo_002","N1_BTSDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_emo_003","N1_BTSDJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_wait_000","N1_BTSZJDZ_wait_000")  --白驼中阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_emo_001","N1_BTSZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSZJDZ_emo_002","N1_BTSZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSZJDZ_emo_003","N1_BTSZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_wait_000","N1_XYSl01_wait_000")  --西域商旅01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_emo_001","N1_XYSl01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl01_emo_002","N1_XYSl01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_XYSl02_wait_000","N1_XYSl02_wait_000")  --西域商旅02
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl02_emo_001","N1_XYSl02_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl02_emo_002","N1_XYSl02_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_SLSJQS_wait_000","N1_SLSJQS_wait_000")  --少林俗家拳师01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_SLSJQS_emo_001","N1_SLSJQS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_SLSJQS_emo_002","N1_SLSJQS_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSanDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮三袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSanDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSanDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSiDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮四袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSiDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSiDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBLDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮六袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBLDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBLDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBBDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮八袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBBDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBBDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDDJDZ_wait_000","N1_THDDJDZ_wait_000")  --桃花岛低阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDDJDZ_emo_001","N1_THDDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDDJDZ_emo_002","N1_THDDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_wait_000","N1_THDZJDZ_wait_000")  --桃花岛中阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDZJDZ_emo_001","N1_THDZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDZJDZ_emo_002","N1_THDZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_003","N1_THDZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_004","N1_THDZJDZ_emo_004")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDGJDZ_wait_000","N1_THDGJDZ_wait_000")  --桃花岛高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDGJDZ_emo_001","N1_THDGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDGJDZ_emo_002","N1_THDGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_MGSB_01_wait_000","N1_MGSB_01_wait_000")  --躺着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_wait_001","N1_MGSB_01_wait_001")  --蹲着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_emo_000","N1_MGSB_01_emo_000")  --躺着一边呻吟一边伸手求助
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGSB_01_emo_001","N1_MGSB_01_emo_001")  --躺着摇着头悲叹自己的命运
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_wait_000","N1_CHDMGNZ01_wait_000")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_CHDMGNZ01_emo_001","N1_CHDMGNZ01_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_emo_002","N1_CHDMGNZ01_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_wait_000","N1_MTQS_wait_000")  --蒙古马头琴手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MTQS_emo_001","N1_MTQS_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_emo_002","N1_MTQS_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

-------------------------------塞外古城--------------------------

STake_Start("N1_wait_swgc_zhongdu_emo_001","N1_wait_008_emo_001")  
    BlendMode(0)
    BlendTime(0.2)
    Direction(2) 
	FrameTime(1.015,1.782)
	Loop(1.015,1.782)
    PlaySpeed(1.015,1.782,0.45)
    Fx(1.015,"FX_buff_poisoning","Head",0.8,1,40,0,0,0,0,0,1,1,1)



---------------------------------------------------------

STake_Start("N1_life_B_move_003","N1_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    
STake_Start("N1_life_B_move_004","YP_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 
    
STake_Start("N1_life_wait_000","N1_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("N1_life_B_wait_000","YP_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()   

STake_Start("N1_life_def_000","YP_def_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.235,3)
    Sound(0.00,"weap_Sword_block")
    Fx(0,"Fx_gedang_wuqi")   

    Sound(0.053,"YP_def_000")

STake_Start("N1_life_dodge_000","YP_dodge_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.352,3)       

    Sound(0.112,"N1_Axe_dodge_000") 
    
STake_Start("N1_life_attack_000","YP_attack_000")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    
  	--Hard(0.000,0.266)  	
  	
    AttackStart()
  	Hurt(0.240,"hurt_11")
    Sound(0.190,"YP_attack_000")
    HurtSound(0.240,"blade_Hit")

    Fx(0.000,"YP_attack_000_fire") 
    DirFx(0.240,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_001","YP_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    
    --Hard(0.000,0.334)  	
  	
    AttackStart()
  	Hurt(0.217,"hurt_11")
    Sound(0.167,"YP_attack_000")
    HurtSound(0.217,"blade_Hit")

    Fx(0.000,"YP_attack_001_fire") 
    DirFx(0.217,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_002","YP_attack_002")
    BlendMode(0)
    BlendTime(0.2)  
    Priority(0,"5") 
    
    --Hard(0.000,0.448) 
    
    AttackStart()
  	Hurt(0.205,"hurt_11")
    Sound(0.155,"YP_attack_000")
    HurtSound(0.205,"blade_Hit")    
    
    Fx(0.000,"YP_attack_002_fire")     
    DirFx(0.205,"YP_attack_hit",0,1)
    

    




STake_Start("N1_wait_000","N1_wait_000")  --N1标准待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

-----------------------------------------------------

STake_Start("N1_wait_001","N1_wait_001")  --蹲地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_002","N1_wait_002")  --坐姿待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_003","N1_wait_003")  --盘腿运气养神
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_004","N1_wait_004")  --躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_005","N1_wait_005")  --跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_006","N1_wait_006")  --轻受伤站立待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_007","N1_wait_007")  --重受伤跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008","N1_wait_008")  --受伤躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008_emo_001","N1_wait_008_emo_001")  --受伤躺地上说话
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_009","N1_wait_009")  --坐地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_010","N1_wait_010")  --手背背后跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011","N1_wait_011")  --说书先生说书
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011_emo_001","N1_wait_011_emo_001")  --说书先生喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_011_emo_002","N1_wait_011_emo_002")  --说书先生鞠躬
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012","N1_wait_012")  --屠夫切肉
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_012_emo_001","N1_wait_012_emo_001")  --屠夫切肉把肉归拢一边
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012_emo_002","N1_wait_012_emo_002")  --屠夫吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013","N1_wait_013")  --挑扁担货郎吆喝
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_013_emo_001","N1_wait_013_emo_001")  --挑扁担货郎擦汗
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013_emo_002","N1_wait_013_emo_002")  --挑扁担货郎把扁担放下休息
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_wait_013_move_001","N1_wait_013_move_001")  --挑扁担货郎走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_wait_014","N1_wait_014")  --大户人家护卫
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_014_emo_001","N1_wait_014_emo_001")  --大户人家护卫随机表情
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_emo_002","N1_wait_014_emo_002")  --大户人家护卫拦住去路
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_move_001","N1_wait_014_move_001")  --大户人家护卫走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015","N1_wait_015")  --马飞刷马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_emo_001","N1_wait_015_emo_001")  --马飞喂马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_move_001","N1_wait_015_move_001")  --马飞走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016","N1_wait_016")  --驿官待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016_emo_001","N1_wait_016_emo_001")  --驿官记账
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017","N1_wait_017")  --收钱艺人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_001","N1_wait_017_emo_001")  --收钱艺人敲锣
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_002","N1_wait_017_emo_002")  --收钱艺人坐下休息
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017_move_001","N1_wait_017_move_001")  --收钱艺人敲锣走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018","N1_wait_018")  --卖艺男批砖
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018_emo_001","N1_wait_018_emo_001")  --卖艺男抱拳行礼
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_emo_002","N1_wait_018_emo_002")  --卖艺男大声吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_move_001","N1_wait_018_move_001")  --卖艺男走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019","N1_wait_019")  --厨师切菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019_emo_001","N1_wait_019_emo_001")  --厨师切菜杂耍
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_020","N1_wait_020")  --厨师炒菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_020_emo_001","N1_wait_020_emo_001")  --厨师炒菜盛菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021","N1_wait_021")  --伙计端盘待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_021_emo_001","N1_wait_021_emo_001")  --伙计端盘上菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021_move_001","N1_wait_021_move_001")  --伙计端盘走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022","N1_wait_022")  --坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022_emo_001","N1_wait_022_emo_001")  --坐着等上菜喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_023","N1_wait_023")  --地痞坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_023_emo_001","N1_wait_023_emo_001")  --地痞坐着等上菜拍桌子
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_024","N1_wait_024")  --坐着夹菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_024_emo_001","N1_wait_024_emo_001")  --坐着吃饭
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_025","N1_wait_025")  --坐着喝茶
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_026","N1_wait_026")  --服务生打算盘
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_027","N1_wait_027")  --抱头躲藏
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028","N1_wait_028")  --扎马步waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028_emo_001","N1_wait_028_emo_001")  --扎马步emo
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_029","N1_wait_029")  --撒网捕鱼waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_029_emo_001","N1_wait_029_emo_001")  --撒网
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_030","N1_wait_030")  --坐着waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

------------------------------------------------------------

STake_Start("N1_move_000","N1_move_000")  --普通前走
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_F","N1_move_001_F")  --前跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_B","N1_move_001_B")  --后退
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_L","N1_move_001_L")  --左跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_R","N1_move_001_R")  --右跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_002","N1_move_002")  --举刀前跑冲锋
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_003","N1_move_003")  --左踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_004","N1_move_004")  --右踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    
STake_Start("N1_move_005","N1_move_005-007")  --起跳
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.8333)

STake_Start("N1_move_006","N1_move_006")  --空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_007","N1_move_005-007")  --落地
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.84,1.566)

STake_Start("N1_move_020","N1_move_020")  --惊恐跑步
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_021","N1_move_021")  --受伤走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

-----------------------------------------------------
STake_Start("N1_life_000","N1_life_000")  --站立伸懒腰
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_001","N1_life_001")  --站立左右看
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_002","N1_life_002")  --站立大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_003","N1_life_003")  --站立哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_004","N1_life_004")  --跪地哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_005","N1_life_005")  --打铁
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_006","N1_life_006")  --打招呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_007","N1_life_007")  --招呼客人
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_008","N1_life_008")  --普通说话
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_009","N1_life_009")  --惊恐
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_010","N1_life_010")  --耕地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_011","N1_life_011")  --插秧
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_012","N1_life_012")  --站着擦汗
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_013","N1_life_013")  --伐木
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_014","N1_life_014")  --修理
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_015","N1_life_015")  --死亡自杀
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.832,1.833)

STake_Start("N1_life_016","N1_life_016")  --坐着聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_017","N1_life_017")  --跪地磕头
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_019","N1_life_019")  --钓鱼待机
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_020","N1_life_020")  --钓鱼表情
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_021","N1_life_021")  --躲起来左右看
    BlendMode(0)
    BlendTime(0.2)
    
STake_Start("N1_life_022","N1_life_022")  --手背身后跪地左右张望
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_023","N1_life_023")  --手背身后跪地上哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_024","N1_life_024")  --受伤躺地呻吟
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_025","N1_life_025")  --受伤躺地挣扎
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_026","N1_life_026")  --欢呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_027","N1_life_027")  --坐地喝酒
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_028","N1_life_028")  --坐地聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_029","N1_life_029")  --坐地大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_030","N1_life_030")  --扫地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_031","N1_life_031")  --向前一指
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_032","N1_life_032")  --惊恐害怕
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_033","N1_life_033")  --打坐
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_034","N1_life_034")  --喝醉躺地
    BlendMode(0)
    BlendTime(0.2)
    Loop()


---------------------------------------

STake_Start("N1_bout_001","N1_bout_001")  --出生
    BlendMode(0)
    BlendTime(0)



STake_Start("N1_ZHLB01_wait_000","N1_ZHLB01_wait_000")  --账房老板
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_ZHLB01_emo_000","N1_ZHLB01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_ZHLB01_emo_001","N1_ZHLB01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_YS01_wait_000","N1_YS01_wait_000")  --药商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_YS01_emo_000","N1_YS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_YS01_emo_001","N1_YS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_WQS01_wait_000","N1_WQS01_wait_000")  --武器商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_WQS01_emo_000","N1_WQS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_WQS01_emo_001","N1_WQS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_wait_000","N1_LR01_wait_000")  --
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_LR01_emo_000","N1_LR01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_emo_001","N1_LR01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_wait_000","N1_JHYY01_wait_000")  --江湖游医
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHYY01_emo_000","N1_JHYY01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_emo_001","N1_JHYY01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_JHSS01_wait_000","N1_JHSS01_wait_000")  --江湖术士
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHSS01_emo_000","N1_JHSS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHSS01_emo_001","N1_JHSS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_wait_000","N1_DXE01_wait_000")  --店小二
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_DXE01_emo_000","N1_DXE01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_emo_001","N1_DXE01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM03_life_000","N1_CM03_life_000")  --村民03
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM03_life_001","N1_CM03_life_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM05_life_002","N1_CM05_life_002")  --村民05
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM05_life_003","N1_CM05_life_003")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_wait_000","N1_QZGJDZ01_wait_000")  --全真高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZGJDZ01_emo_001","N1_QZGJDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_emo_002","N1_QZGJDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_wait_000","N1_QZRMDZ01_wait_000")  --全真入门弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZRMDZ01_emo_001","N1_QZRMDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_emo_002","N1_QZRMDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_wait_000","N1_RS_wait_000")  --儒生
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_RS_emo_001","N1_RS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_emo_002","N1_RS_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_wait_000","N1_XinShi_wait_000")  --信使
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XinShi_emo_001","N1_XinShi_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_emo_002","N1_XinShi_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_wait_000","N1_MGN_wait_000")  --蒙古男
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_MGN_emo_001","N1_MGN_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_emo_002","N1_MGN_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSGJDZ_wait_000","N1_BTSGJDZ_wait_000")  --白驼高阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSGJDZ_emo_001","N1_BTSGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSGJDZ_emo_002","N1_BTSGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJJL_wait_000","N1_HAJJL_wait_000")  --红袄军将领
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJJL_emo_001","N1_HAJJL_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJJL_emo_002","N1_HAJJL_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJ01_wait_000","N1_HAJ01_wait_000")  --红袄军伤兵
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJ01_emo_001","N1_HAJ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJ01_emo_002","N1_HAJ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_wait_000","N1_BTSDJDZ_wait_000")  --白驼低阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSDJDZ_emo_001","N1_BTSDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSDJDZ_emo_002","N1_BTSDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_emo_003","N1_BTSDJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_wait_000","N1_BTSZJDZ_wait_000")  --白驼中阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_emo_001","N1_BTSZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSZJDZ_emo_002","N1_BTSZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSZJDZ_emo_003","N1_BTSZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_wait_000","N1_XYSl01_wait_000")  --西域商旅01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_emo_001","N1_XYSl01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl01_emo_002","N1_XYSl01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_XYSl02_wait_000","N1_XYSl02_wait_000")  --西域商旅02
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl02_emo_001","N1_XYSl02_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl02_emo_002","N1_XYSl02_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_SLSJQS_wait_000","N1_SLSJQS_wait_000")  --少林俗家拳师01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_SLSJQS_emo_001","N1_SLSJQS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_SLSJQS_emo_002","N1_SLSJQS_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSanDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮三袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSanDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSanDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSiDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮四袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSiDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSiDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBLDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮六袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBLDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBLDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBBDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮八袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBBDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBBDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDDJDZ_wait_000","N1_THDDJDZ_wait_000")  --桃花岛低阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDDJDZ_emo_001","N1_THDDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDDJDZ_emo_002","N1_THDDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_wait_000","N1_THDZJDZ_wait_000")  --桃花岛中阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDZJDZ_emo_001","N1_THDZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDZJDZ_emo_002","N1_THDZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_003","N1_THDZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_004","N1_THDZJDZ_emo_004")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDGJDZ_wait_000","N1_THDGJDZ_wait_000")  --桃花岛高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDGJDZ_emo_001","N1_THDGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDGJDZ_emo_002","N1_THDGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_MGSB_01_wait_000","N1_MGSB_01_wait_000")  --躺着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_wait_001","N1_MGSB_01_wait_001")  --蹲着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_emo_000","N1_MGSB_01_emo_000")  --躺着一边呻吟一边伸手求助
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGSB_01_emo_001","N1_MGSB_01_emo_001")  --躺着摇着头悲叹自己的命运
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_wait_000","N1_CHDMGNZ01_wait_000")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_CHDMGNZ01_emo_001","N1_CHDMGNZ01_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_emo_002","N1_CHDMGNZ01_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_wait_000","N1_MTQS_wait_000")  --蒙古马头琴手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MTQS_emo_001","N1_MTQS_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_emo_002","N1_MTQS_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

-------------------------------塞外古城--------------------------

STake_Start("N1_wait_swgc_zhongdu_emo_001","N1_wait_008_emo_001")  
    BlendMode(0)
    BlendTime(0.2)
    Direction(2) 
	FrameTime(1.015,1.782)
	Loop(1.015,1.782)
    PlaySpeed(1.015,1.782,0.45)
    Fx(1.015,"FX_buff_poisoning","Head",0.8,1,40,0,0,0,0,0,1,1,1)



---------------------------------------------------------

STake_Start("N1_life_B_move_003","N1_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    
STake_Start("N1_life_B_move_004","YP_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 
    
STake_Start("N1_life_wait_000","N1_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("N1_life_B_wait_000","YP_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()   

STake_Start("N1_life_def_000","YP_def_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.235,3)
    Sound(0.00,"weap_Sword_block")
    Fx(0,"Fx_gedang_wuqi")   

    Sound(0.053,"YP_def_000")

STake_Start("N1_life_dodge_000","YP_dodge_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.352,3)       

    Sound(0.112,"N1_Axe_dodge_000") 
    
STake_Start("N1_life_attack_000","YP_attack_000")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    
  	--Hard(0.000,0.266)  	
  	
    AttackStart()
  	Hurt(0.240,"hurt_11")
    Sound(0.190,"YP_attack_000")
    HurtSound(0.240,"blade_Hit")

    Fx(0.000,"YP_attack_000_fire") 
    DirFx(0.240,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_001","YP_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    
    --Hard(0.000,0.334)  	
  	
    AttackStart()
  	Hurt(0.217,"hurt_11")
    Sound(0.167,"YP_attack_000")
    HurtSound(0.217,"blade_Hit")

    Fx(0.000,"YP_attack_001_fire") 
    DirFx(0.217,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_002","YP_attack_002")
    BlendMode(0)
    BlendTime(0.2)  
    Priority(0,"5") 
    
    --Hard(0.000,0.448) 
    
    AttackStart()
  	Hurt(0.205,"hurt_11")
    Sound(0.155,"YP_attack_000")
    HurtSound(0.205,"blade_Hit")    
    
    Fx(0.000,"YP_attack_002_fire")     
    DirFx(0.205,"YP_attack_hit",0,1)
    

    




STake_Start("N1_wait_000","N1_wait_000")  --N1标准待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

-----------------------------------------------------

STake_Start("N1_wait_001","N1_wait_001")  --蹲地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_002","N1_wait_002")  --坐姿待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_003","N1_wait_003")  --盘腿运气养神
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_004","N1_wait_004")  --躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_005","N1_wait_005")  --跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_006","N1_wait_006")  --轻受伤站立待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_007","N1_wait_007")  --重受伤跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008","N1_wait_008")  --受伤躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008_emo_001","N1_wait_008_emo_001")  --受伤躺地上说话
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_009","N1_wait_009")  --坐地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_010","N1_wait_010")  --手背背后跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011","N1_wait_011")  --说书先生说书
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011_emo_001","N1_wait_011_emo_001")  --说书先生喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_011_emo_002","N1_wait_011_emo_002")  --说书先生鞠躬
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012","N1_wait_012")  --屠夫切肉
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_012_emo_001","N1_wait_012_emo_001")  --屠夫切肉把肉归拢一边
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012_emo_002","N1_wait_012_emo_002")  --屠夫吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013","N1_wait_013")  --挑扁担货郎吆喝
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_013_emo_001","N1_wait_013_emo_001")  --挑扁担货郎擦汗
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013_emo_002","N1_wait_013_emo_002")  --挑扁担货郎把扁担放下休息
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_wait_013_move_001","N1_wait_013_move_001")  --挑扁担货郎走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_wait_014","N1_wait_014")  --大户人家护卫
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_014_emo_001","N1_wait_014_emo_001")  --大户人家护卫随机表情
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_emo_002","N1_wait_014_emo_002")  --大户人家护卫拦住去路
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_move_001","N1_wait_014_move_001")  --大户人家护卫走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015","N1_wait_015")  --马飞刷马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_emo_001","N1_wait_015_emo_001")  --马飞喂马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_move_001","N1_wait_015_move_001")  --马飞走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016","N1_wait_016")  --驿官待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016_emo_001","N1_wait_016_emo_001")  --驿官记账
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017","N1_wait_017")  --收钱艺人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_001","N1_wait_017_emo_001")  --收钱艺人敲锣
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_002","N1_wait_017_emo_002")  --收钱艺人坐下休息
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017_move_001","N1_wait_017_move_001")  --收钱艺人敲锣走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018","N1_wait_018")  --卖艺男批砖
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018_emo_001","N1_wait_018_emo_001")  --卖艺男抱拳行礼
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_emo_002","N1_wait_018_emo_002")  --卖艺男大声吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_move_001","N1_wait_018_move_001")  --卖艺男走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019","N1_wait_019")  --厨师切菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019_emo_001","N1_wait_019_emo_001")  --厨师切菜杂耍
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_020","N1_wait_020")  --厨师炒菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_020_emo_001","N1_wait_020_emo_001")  --厨师炒菜盛菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021","N1_wait_021")  --伙计端盘待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_021_emo_001","N1_wait_021_emo_001")  --伙计端盘上菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021_move_001","N1_wait_021_move_001")  --伙计端盘走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022","N1_wait_022")  --坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022_emo_001","N1_wait_022_emo_001")  --坐着等上菜喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_023","N1_wait_023")  --地痞坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_023_emo_001","N1_wait_023_emo_001")  --地痞坐着等上菜拍桌子
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_024","N1_wait_024")  --坐着夹菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_024_emo_001","N1_wait_024_emo_001")  --坐着吃饭
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_025","N1_wait_025")  --坐着喝茶
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_026","N1_wait_026")  --服务生打算盘
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_027","N1_wait_027")  --抱头躲藏
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028","N1_wait_028")  --扎马步waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028_emo_001","N1_wait_028_emo_001")  --扎马步emo
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_029","N1_wait_029")  --撒网捕鱼waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_029_emo_001","N1_wait_029_emo_001")  --撒网
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_030","N1_wait_030")  --坐着waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

------------------------------------------------------------

STake_Start("N1_move_000","N1_move_000")  --普通前走
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_F","N1_move_001_F")  --前跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_B","N1_move_001_B")  --后退
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_L","N1_move_001_L")  --左跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_R","N1_move_001_R")  --右跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_002","N1_move_002")  --举刀前跑冲锋
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_003","N1_move_003")  --左踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_004","N1_move_004")  --右踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    
STake_Start("N1_move_005","N1_move_005-007")  --起跳
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.8333)

STake_Start("N1_move_006","N1_move_006")  --空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_007","N1_move_005-007")  --落地
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.84,1.566)

STake_Start("N1_move_020","N1_move_020")  --惊恐跑步
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_021","N1_move_021")  --受伤走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

-----------------------------------------------------
STake_Start("N1_life_000","N1_life_000")  --站立伸懒腰
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_001","N1_life_001")  --站立左右看
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_002","N1_life_002")  --站立大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_003","N1_life_003")  --站立哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_004","N1_life_004")  --跪地哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_005","N1_life_005")  --打铁
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_006","N1_life_006")  --打招呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_007","N1_life_007")  --招呼客人
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_008","N1_life_008")  --普通说话
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_009","N1_life_009")  --惊恐
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_010","N1_life_010")  --耕地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_011","N1_life_011")  --插秧
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_012","N1_life_012")  --站着擦汗
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_013","N1_life_013")  --伐木
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_014","N1_life_014")  --修理
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_015","N1_life_015")  --死亡自杀
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.832,1.833)

STake_Start("N1_life_016","N1_life_016")  --坐着聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_017","N1_life_017")  --跪地磕头
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_019","N1_life_019")  --钓鱼待机
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_020","N1_life_020")  --钓鱼表情
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_021","N1_life_021")  --躲起来左右看
    BlendMode(0)
    BlendTime(0.2)
    
STake_Start("N1_life_022","N1_life_022")  --手背身后跪地左右张望
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_023","N1_life_023")  --手背身后跪地上哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_024","N1_life_024")  --受伤躺地呻吟
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_025","N1_life_025")  --受伤躺地挣扎
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_026","N1_life_026")  --欢呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_027","N1_life_027")  --坐地喝酒
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_028","N1_life_028")  --坐地聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_029","N1_life_029")  --坐地大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_030","N1_life_030")  --扫地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_031","N1_life_031")  --向前一指
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_032","N1_life_032")  --惊恐害怕
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_033","N1_life_033")  --打坐
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_034","N1_life_034")  --喝醉躺地
    BlendMode(0)
    BlendTime(0.2)
    Loop()


---------------------------------------

STake_Start("N1_bout_001","N1_bout_001")  --出生
    BlendMode(0)
    BlendTime(0)



STake_Start("N1_ZHLB01_wait_000","N1_ZHLB01_wait_000")  --账房老板
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_ZHLB01_emo_000","N1_ZHLB01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_ZHLB01_emo_001","N1_ZHLB01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_YS01_wait_000","N1_YS01_wait_000")  --药商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_YS01_emo_000","N1_YS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_YS01_emo_001","N1_YS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_WQS01_wait_000","N1_WQS01_wait_000")  --武器商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_WQS01_emo_000","N1_WQS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_WQS01_emo_001","N1_WQS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_wait_000","N1_LR01_wait_000")  --
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_LR01_emo_000","N1_LR01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_emo_001","N1_LR01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_wait_000","N1_JHYY01_wait_000")  --江湖游医
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHYY01_emo_000","N1_JHYY01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_emo_001","N1_JHYY01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_JHSS01_wait_000","N1_JHSS01_wait_000")  --江湖术士
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHSS01_emo_000","N1_JHSS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHSS01_emo_001","N1_JHSS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_wait_000","N1_DXE01_wait_000")  --店小二
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_DXE01_emo_000","N1_DXE01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_emo_001","N1_DXE01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM03_life_000","N1_CM03_life_000")  --村民03
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM03_life_001","N1_CM03_life_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM05_life_002","N1_CM05_life_002")  --村民05
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM05_life_003","N1_CM05_life_003")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_wait_000","N1_QZGJDZ01_wait_000")  --全真高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZGJDZ01_emo_001","N1_QZGJDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_emo_002","N1_QZGJDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_wait_000","N1_QZRMDZ01_wait_000")  --全真入门弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZRMDZ01_emo_001","N1_QZRMDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_emo_002","N1_QZRMDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_wait_000","N1_RS_wait_000")  --儒生
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_RS_emo_001","N1_RS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_emo_002","N1_RS_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_wait_000","N1_XinShi_wait_000")  --信使
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XinShi_emo_001","N1_XinShi_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_emo_002","N1_XinShi_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_wait_000","N1_MGN_wait_000")  --蒙古男
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_MGN_emo_001","N1_MGN_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_emo_002","N1_MGN_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSGJDZ_wait_000","N1_BTSGJDZ_wait_000")  --白驼高阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSGJDZ_emo_001","N1_BTSGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSGJDZ_emo_002","N1_BTSGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJJL_wait_000","N1_HAJJL_wait_000")  --红袄军将领
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJJL_emo_001","N1_HAJJL_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJJL_emo_002","N1_HAJJL_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJ01_wait_000","N1_HAJ01_wait_000")  --红袄军伤兵
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJ01_emo_001","N1_HAJ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJ01_emo_002","N1_HAJ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_wait_000","N1_BTSDJDZ_wait_000")  --白驼低阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSDJDZ_emo_001","N1_BTSDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSDJDZ_emo_002","N1_BTSDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_emo_003","N1_BTSDJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_wait_000","N1_BTSZJDZ_wait_000")  --白驼中阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_emo_001","N1_BTSZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSZJDZ_emo_002","N1_BTSZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSZJDZ_emo_003","N1_BTSZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_wait_000","N1_XYSl01_wait_000")  --西域商旅01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_emo_001","N1_XYSl01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl01_emo_002","N1_XYSl01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_XYSl02_wait_000","N1_XYSl02_wait_000")  --西域商旅02
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl02_emo_001","N1_XYSl02_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl02_emo_002","N1_XYSl02_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_SLSJQS_wait_000","N1_SLSJQS_wait_000")  --少林俗家拳师01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_SLSJQS_emo_001","N1_SLSJQS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_SLSJQS_emo_002","N1_SLSJQS_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSanDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮三袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSanDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSanDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSiDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮四袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSiDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSiDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBLDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮六袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBLDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBLDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBBDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮八袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBBDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBBDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDDJDZ_wait_000","N1_THDDJDZ_wait_000")  --桃花岛低阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDDJDZ_emo_001","N1_THDDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDDJDZ_emo_002","N1_THDDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_wait_000","N1_THDZJDZ_wait_000")  --桃花岛中阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDZJDZ_emo_001","N1_THDZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDZJDZ_emo_002","N1_THDZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_003","N1_THDZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_004","N1_THDZJDZ_emo_004")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDGJDZ_wait_000","N1_THDGJDZ_wait_000")  --桃花岛高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDGJDZ_emo_001","N1_THDGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDGJDZ_emo_002","N1_THDGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_MGSB_01_wait_000","N1_MGSB_01_wait_000")  --躺着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_wait_001","N1_MGSB_01_wait_001")  --蹲着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_emo_000","N1_MGSB_01_emo_000")  --躺着一边呻吟一边伸手求助
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGSB_01_emo_001","N1_MGSB_01_emo_001")  --躺着摇着头悲叹自己的命运
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_wait_000","N1_CHDMGNZ01_wait_000")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_CHDMGNZ01_emo_001","N1_CHDMGNZ01_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_emo_002","N1_CHDMGNZ01_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_wait_000","N1_MTQS_wait_000")  --蒙古马头琴手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MTQS_emo_001","N1_MTQS_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_emo_002","N1_MTQS_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

-------------------------------塞外古城--------------------------

STake_Start("N1_wait_swgc_zhongdu_emo_001","N1_wait_008_emo_001")  
    BlendMode(0)
    BlendTime(0.2)
    Direction(2) 
	FrameTime(1.015,1.782)
	Loop(1.015,1.782)
    PlaySpeed(1.015,1.782,0.45)
    Fx(1.015,"FX_buff_poisoning","Head",0.8,1,40,0,0,0,0,0,1,1,1)



---------------------------------------------------------

STake_Start("N1_life_B_move_003","N1_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    
STake_Start("N1_life_B_move_004","YP_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 
    
STake_Start("N1_life_wait_000","N1_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("N1_life_B_wait_000","YP_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()   

STake_Start("N1_life_def_000","YP_def_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.235,3)
    Sound(0.00,"weap_Sword_block")
    Fx(0,"Fx_gedang_wuqi")   

    Sound(0.053,"YP_def_000")

STake_Start("N1_life_dodge_000","YP_dodge_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.352,3)       

    Sound(0.112,"N1_Axe_dodge_000") 
    
STake_Start("N1_life_attack_000","YP_attack_000")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    
  	--Hard(0.000,0.266)  	
  	
    AttackStart()
  	Hurt(0.240,"hurt_11")
    Sound(0.190,"YP_attack_000")
    HurtSound(0.240,"blade_Hit")

    Fx(0.000,"YP_attack_000_fire") 
    DirFx(0.240,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_001","YP_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    
    --Hard(0.000,0.334)  	
  	
    AttackStart()
  	Hurt(0.217,"hurt_11")
    Sound(0.167,"YP_attack_000")
    HurtSound(0.217,"blade_Hit")

    Fx(0.000,"YP_attack_001_fire") 
    DirFx(0.217,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_002","YP_attack_002")
    BlendMode(0)
    BlendTime(0.2)  
    Priority(0,"5") 
    
    --Hard(0.000,0.448) 
    
    AttackStart()
  	Hurt(0.205,"hurt_11")
    Sound(0.155,"YP_attack_000")
    HurtSound(0.205,"blade_Hit")    
    
    Fx(0.000,"YP_attack_002_fire")     
    DirFx(0.205,"YP_attack_hit",0,1)
    

    




STake_Start("N1_wait_000","N1_wait_000")  --N1标准待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

-----------------------------------------------------

STake_Start("N1_wait_001","N1_wait_001")  --蹲地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_002","N1_wait_002")  --坐姿待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_003","N1_wait_003")  --盘腿运气养神
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_004","N1_wait_004")  --躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_005","N1_wait_005")  --跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_006","N1_wait_006")  --轻受伤站立待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_007","N1_wait_007")  --重受伤跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008","N1_wait_008")  --受伤躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_008_emo_001","N1_wait_008_emo_001")  --受伤躺地上说话
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_009","N1_wait_009")  --坐地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_010","N1_wait_010")  --手背背后跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011","N1_wait_011")  --说书先生说书
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_011_emo_001","N1_wait_011_emo_001")  --说书先生喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_011_emo_002","N1_wait_011_emo_002")  --说书先生鞠躬
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012","N1_wait_012")  --屠夫切肉
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_012_emo_001","N1_wait_012_emo_001")  --屠夫切肉把肉归拢一边
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_012_emo_002","N1_wait_012_emo_002")  --屠夫吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013","N1_wait_013")  --挑扁担货郎吆喝
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_013_emo_001","N1_wait_013_emo_001")  --挑扁担货郎擦汗
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_013_emo_002","N1_wait_013_emo_002")  --挑扁担货郎把扁担放下休息
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_wait_013_move_001","N1_wait_013_move_001")  --挑扁担货郎走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_wait_014","N1_wait_014")  --大户人家护卫
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_014_emo_001","N1_wait_014_emo_001")  --大户人家护卫随机表情
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_emo_002","N1_wait_014_emo_002")  --大户人家护卫拦住去路
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_014_move_001","N1_wait_014_move_001")  --大户人家护卫走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015","N1_wait_015")  --马飞刷马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_emo_001","N1_wait_015_emo_001")  --马飞喂马
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_015_move_001","N1_wait_015_move_001")  --马飞走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016","N1_wait_016")  --驿官待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_016_emo_001","N1_wait_016_emo_001")  --驿官记账
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017","N1_wait_017")  --收钱艺人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_001","N1_wait_017_emo_001")  --收钱艺人敲锣
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_017_emo_002","N1_wait_017_emo_002")  --收钱艺人坐下休息
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_017_move_001","N1_wait_017_move_001")  --收钱艺人敲锣走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018","N1_wait_018")  --卖艺男批砖
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_018_emo_001","N1_wait_018_emo_001")  --卖艺男抱拳行礼
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_emo_002","N1_wait_018_emo_002")  --卖艺男大声吆喝
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_018_move_001","N1_wait_018_move_001")  --卖艺男走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019","N1_wait_019")  --厨师切菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_019_emo_001","N1_wait_019_emo_001")  --厨师切菜杂耍
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_020","N1_wait_020")  --厨师炒菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_020_emo_001","N1_wait_020_emo_001")  --厨师炒菜盛菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021","N1_wait_021")  --伙计端盘待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_021_emo_001","N1_wait_021_emo_001")  --伙计端盘上菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_021_move_001","N1_wait_021_move_001")  --伙计端盘走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022","N1_wait_022")  --坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_022_emo_001","N1_wait_022_emo_001")  --坐着等上菜喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_023","N1_wait_023")  --地痞坐着等上菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_023_emo_001","N1_wait_023_emo_001")  --地痞坐着等上菜拍桌子
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_024","N1_wait_024")  --坐着夹菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_024_emo_001","N1_wait_024_emo_001")  --坐着吃饭
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_025","N1_wait_025")  --坐着喝茶
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_026","N1_wait_026")  --服务生打算盘
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_027","N1_wait_027")  --抱头躲藏
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028","N1_wait_028")  --扎马步waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_028_emo_001","N1_wait_028_emo_001")  --扎马步emo
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_029","N1_wait_029")  --撒网捕鱼waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_wait_029_emo_001","N1_wait_029_emo_001")  --撒网
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_wait_030","N1_wait_030")  --坐着waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

------------------------------------------------------------

STake_Start("N1_move_000","N1_move_000")  --普通前走
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_F","N1_move_001_F")  --前跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_B","N1_move_001_B")  --后退
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_L","N1_move_001_L")  --左跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_001_R","N1_move_001_R")  --右跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_002","N1_move_002")  --举刀前跑冲锋
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_003","N1_move_003")  --左踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_move_004","N1_move_004")  --右踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    
STake_Start("N1_move_005","N1_move_005-007")  --起跳
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.8333)

STake_Start("N1_move_006","N1_move_006")  --空中loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_007","N1_move_005-007")  --落地
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.84,1.566)

STake_Start("N1_move_020","N1_move_020")  --惊恐跑步
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_move_021","N1_move_021")  --受伤走路
    BlendMode(0)
    BlendTime(0.2)
    Loop()

-----------------------------------------------------
STake_Start("N1_life_000","N1_life_000")  --站立伸懒腰
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_001","N1_life_001")  --站立左右看
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_002","N1_life_002")  --站立大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_003","N1_life_003")  --站立哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_004","N1_life_004")  --跪地哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_005","N1_life_005")  --打铁
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_006","N1_life_006")  --打招呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_007","N1_life_007")  --招呼客人
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_008","N1_life_008")  --普通说话
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_009","N1_life_009")  --惊恐
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_010","N1_life_010")  --耕地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_011","N1_life_011")  --插秧
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_012","N1_life_012")  --站着擦汗
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_013","N1_life_013")  --伐木
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_014","N1_life_014")  --修理
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_015","N1_life_015")  --死亡自杀
    BlendMode(0)
    BlendTime(0.2)
    Loop(1.832,1.833)

STake_Start("N1_life_016","N1_life_016")  --坐着聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_017","N1_life_017")  --跪地磕头
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_019","N1_life_019")  --钓鱼待机
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_020","N1_life_020")  --钓鱼表情
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_021","N1_life_021")  --躲起来左右看
    BlendMode(0)
    BlendTime(0.2)
    
STake_Start("N1_life_022","N1_life_022")  --手背身后跪地左右张望
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_023","N1_life_023")  --手背身后跪地上哭泣
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_024","N1_life_024")  --受伤躺地呻吟
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_025","N1_life_025")  --受伤躺地挣扎
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_026","N1_life_026")  --欢呼
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_027","N1_life_027")  --坐地喝酒
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_028","N1_life_028")  --坐地聊天
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_029","N1_life_029")  --坐地大笑
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_030","N1_life_030")  --扫地
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_031","N1_life_031")  --向前一指
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_032","N1_life_032")  --惊恐害怕
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_life_033","N1_life_033")  --打坐
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N1_life_034","N1_life_034")  --喝醉躺地
    BlendMode(0)
    BlendTime(0.2)
    Loop()


---------------------------------------

STake_Start("N1_bout_001","N1_bout_001")  --出生
    BlendMode(0)
    BlendTime(0)



STake_Start("N1_ZHLB01_wait_000","N1_ZHLB01_wait_000")  --账房老板
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_ZHLB01_emo_000","N1_ZHLB01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_ZHLB01_emo_001","N1_ZHLB01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_YS01_wait_000","N1_YS01_wait_000")  --药商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_YS01_emo_000","N1_YS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_YS01_emo_001","N1_YS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_WQS01_wait_000","N1_WQS01_wait_000")  --武器商
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_WQS01_emo_000","N1_WQS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_WQS01_emo_001","N1_WQS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_wait_000","N1_LR01_wait_000")  --
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_LR01_emo_000","N1_LR01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_LR01_emo_001","N1_LR01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_wait_000","N1_JHYY01_wait_000")  --江湖游医
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHYY01_emo_000","N1_JHYY01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHYY01_emo_001","N1_JHYY01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N1_JHSS01_wait_000","N1_JHSS01_wait_000")  --江湖术士
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_JHSS01_emo_000","N1_JHSS01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_JHSS01_emo_001","N1_JHSS01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_wait_000","N1_DXE01_wait_000")  --店小二
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_DXE01_emo_000","N1_DXE01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_DXE01_emo_001","N1_DXE01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM03_life_000","N1_CM03_life_000")  --村民03
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM03_life_001","N1_CM03_life_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CM05_life_002","N1_CM05_life_002")  --村民05
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_CM05_life_003","N1_CM05_life_003")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_wait_000","N1_QZGJDZ01_wait_000")  --全真高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZGJDZ01_emo_001","N1_QZGJDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZGJDZ01_emo_002","N1_QZGJDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_wait_000","N1_QZRMDZ01_wait_000")  --全真入门弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_QZRMDZ01_emo_001","N1_QZRMDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_QZRMDZ01_emo_002","N1_QZRMDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_wait_000","N1_RS_wait_000")  --儒生
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_RS_emo_001","N1_RS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_RS_emo_002","N1_RS_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_wait_000","N1_XinShi_wait_000")  --信使
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XinShi_emo_001","N1_XinShi_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XinShi_emo_002","N1_XinShi_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_wait_000","N1_MGN_wait_000")  --蒙古男
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_MGN_emo_001","N1_MGN_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGN_emo_002","N1_MGN_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSGJDZ_wait_000","N1_BTSGJDZ_wait_000")  --白驼高阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSGJDZ_emo_001","N1_BTSGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSGJDZ_emo_002","N1_BTSGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJJL_wait_000","N1_HAJJL_wait_000")  --红袄军将领
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJJL_emo_001","N1_HAJJL_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJJL_emo_002","N1_HAJJL_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_HAJ01_wait_000","N1_HAJ01_wait_000")  --红袄军伤兵
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_HAJ01_emo_001","N1_HAJ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_HAJ01_emo_002","N1_HAJ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_wait_000","N1_BTSDJDZ_wait_000")  --白驼低阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSDJDZ_emo_001","N1_BTSDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSDJDZ_emo_002","N1_BTSDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSDJDZ_emo_003","N1_BTSDJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_wait_000","N1_BTSZJDZ_wait_000")  --白驼中阶男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_BTSZJDZ_emo_001","N1_BTSZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_BTSZJDZ_emo_002","N1_BTSZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_BTSZJDZ_emo_003","N1_BTSZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_wait_000","N1_XYSl01_wait_000")  --西域商旅01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl01_emo_001","N1_XYSl01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl01_emo_002","N1_XYSl01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_XYSl02_wait_000","N1_XYSl02_wait_000")  --西域商旅02
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_XYSl02_emo_001","N1_XYSl02_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_XYSl02_emo_002","N1_XYSl02_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_SLSJQS_wait_000","N1_SLSJQS_wait_000")  --少林俗家拳师01
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_SLSJQS_emo_001","N1_SLSJQS_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_SLSJQS_emo_002","N1_SLSJQS_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSanDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮三袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSanDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSanDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBSiDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮四袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBSiDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBSiDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBLDDZ01_wait_000","N1_GBSanDDZ01_wait_000")  --丐帮六袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBLDDZ01_emo_001","N1_GBSanDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBLDDZ01_emo_002","N1_GBSanDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_GBBDDZ01_wait_000","N1_GBSiDDZ01_wait_000")  --丐帮八袋男弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_GBBDDZ01_emo_001","N1_GBSiDDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_GBBDDZ01_emo_002","N1_GBSiDDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDDJDZ_wait_000","N1_THDDJDZ_wait_000")  --桃花岛低阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDDJDZ_emo_001","N1_THDDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDDJDZ_emo_002","N1_THDDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_wait_000","N1_THDZJDZ_wait_000")  --桃花岛中阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDZJDZ_emo_001","N1_THDZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDZJDZ_emo_002","N1_THDZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_003","N1_THDZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDZJDZ_emo_004","N1_THDZJDZ_emo_004")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_THDGJDZ_wait_000","N1_THDGJDZ_wait_000")  --桃花岛高阶弟子
    BlendMode(0)
    BlendTime(0.2)
    Loop() 

STake_Start("N1_THDGJDZ_emo_001","N1_THDGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_THDGJDZ_emo_002","N1_THDGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N1_MGSB_01_wait_000","N1_MGSB_01_wait_000")  --躺着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_wait_001","N1_MGSB_01_wait_001")  --蹲着受伤的蒙古兵
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MGSB_01_emo_000","N1_MGSB_01_emo_000")  --躺着一边呻吟一边伸手求助
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MGSB_01_emo_001","N1_MGSB_01_emo_001")  --躺着摇着头悲叹自己的命运
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_wait_000","N1_CHDMGNZ01_wait_000")  --吃喝的蒙古男子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_CHDMGNZ01_emo_001","N1_CHDMGNZ01_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_CHDMGNZ01_emo_002","N1_CHDMGNZ01_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_wait_000","N1_MTQS_wait_000")  --蒙古马头琴手
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N1_MTQS_emo_001","N1_MTQS_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N1_MTQS_emo_002","N1_MTQS_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 

-------------------------------塞外古城--------------------------

STake_Start("N1_wait_swgc_zhongdu_emo_001","N1_wait_008_emo_001")  
    BlendMode(0)
    BlendTime(0.2)
    Direction(2) 
	FrameTime(1.015,1.782)
	Loop(1.015,1.782)
    PlaySpeed(1.015,1.782,0.45)
    Fx(1.015,"FX_buff_poisoning","Head",0.8,1,40,0,0,0,0,0,1,1,1)



---------------------------------------------------------

STake_Start("N1_life_B_move_003","N1_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

    
STake_Start("N1_life_B_move_004","YP_move_004")
    BlendMode(0)
    BlendTime(0.2)
    Loop() 
    
STake_Start("N1_life_wait_000","N1_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("N1_life_B_wait_000","YP_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()   

STake_Start("N1_life_def_000","YP_def_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.235,3)
    Sound(0.00,"weap_Sword_block")
    Fx(0,"Fx_gedang_wuqi")   

    Sound(0.053,"YP_def_000")

STake_Start("N1_life_dodge_000","YP_dodge_000")
    BlendMode(0)
    BlendTime(0.1)
    MotionBlur(0.000,0.352,3)       

    Sound(0.112,"N1_Axe_dodge_000") 
    
STake_Start("N1_life_attack_000","YP_attack_000")
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    
  	--Hard(0.000,0.266)  	
  	
    AttackStart()
  	Hurt(0.240,"hurt_11")
    Sound(0.190,"YP_attack_000")
    HurtSound(0.240,"blade_Hit")

    Fx(0.000,"YP_attack_000_fire") 
    DirFx(0.240,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_001","YP_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    
    --Hard(0.000,0.334)  	
  	
    AttackStart()
  	Hurt(0.217,"hurt_11")
    Sound(0.167,"YP_attack_000")
    HurtSound(0.217,"blade_Hit")

    Fx(0.000,"YP_attack_001_fire") 
    DirFx(0.217,"YP_attack_hit",0,1)

STake_Start("N1_life_attack_002","YP_attack_002")
    BlendMode(0)
    BlendTime(0.2)  
    Priority(0,"5") 
    
    --Hard(0.000,0.448) 
    
    AttackStart()
  	Hurt(0.205,"hurt_11")
    Sound(0.155,"YP_attack_000")
    HurtSound(0.205,"blade_Hit")    
    
    Fx(0.000,"YP_attack_002_fire")     
    DirFx(0.205,"YP_attack_hit",0,1)
    

    




