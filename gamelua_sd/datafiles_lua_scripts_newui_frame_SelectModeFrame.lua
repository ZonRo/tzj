-- SelectModeFrame

local layoutName = 'Tutorial_ModeSelect.layout'

ui.defineFrame(..., layoutName)
function Init(self)
	self.btnMode1 = self:GetChildWindow('Course_AVShow/ModBtn1') -- ZeroMode
	self.btnMode2 = self:GetChildWindow('Course_AVShow/ModBtn2') -- UsualMode
	self.btnMode3 = self:GetChildWindow('Course_AVShow/ModBtn3') -- ClassicMode
	
	local curModeText = nil
	local curMode = SD.GetOperationMode()
	if curMode ==  SD.HS_OPERATION_ZERO then
		curModeText = 'Course_AVShow/C1/BG/ChooseText'
	elseif curMode ==  SD.HS_OPERATION_USUAL then
		curModeText = 'Course_AVShow/C2/BG/ChooseText'
	elseif curMode ==  SD.HS_OPERATION_CLASSIC then
		curModeText = 'Course_AVShow/C3/BG/ChooseText'
	end
	
	if curModeText then
		local pModeText = self:GetChildWindow(curModeText)
		if pModeText then pModeText:show() end
	end
	
	self.isIn = false
	self.completed = false
end

function UnInit(self)
	lout('SelectModeFrame:UnInit()')
	self:UnsubscribeGameEvents()
end


function Subscribe(self)
	self:SubscribeUIEvents()
end

function SubscribeUIEvents(self)
	lout('SelectModeFrame:SubscribeEvents()')
	self.btnMode1:subscribeEvent('Clicked', self.OnClickModBtn1, self)
	self.btnMode2:subscribeEvent('Clicked', self.OnClickModBtn2, self)
	self.btnMode3:subscribeEvent('Clicked', self.OnClickModBtn3, self)
end

function SubscribeGameEvents(self)
	lout('SelectModeFrame:SubscribeGameEvents()')
	if gameEventMgr then
		self.opModeConn = gameEventMgr:subscribeEvent('EventOpMode', self.OnModeChanged, self)
	end
end 

function UnsubscribeGameEvents(self)
	lout('SelectModeFrame:UnsubscribeGameEvents()')
	if self.opModeConn then
		self.opModeConn:disconnect()
		self.opModeConn = nil
	end
end 

function Enter(self)
	if not self.isIn then
		self.isIn = true
		self:SubscribeGameEvents()
		self:Show()
		--hide selectmode small icon
        local smallicon = wndMgr:getWindow('DefaultMainMenu/Mod')
        smallicon:hide()
	end
end

function Leave(self)
	if self.isIn then
		self:UnsubscribeGameEvents()
		self:Hide()
		 -- show selectmode small icon
        local smallicon = wndMgr:getWindow('DefaultMainMenu/Mod')
        smallicon:show()
		self.isIn = false
	end
end

function HasCompleted(self)
	return self.completed
end

function OnModeChanged(self, evtArgs)
	lout('SelectModeFrame:OnModeChanged()')
	self.completed = true
end

function OnClickModBtn1(self, evtArgs)
	lout('SelectModeFrame:OnClickModBtn1()  - Select ZeroMode')
	self:DisableAll()
	
	guideMgr.ReportTutorialFirstSelectOpMode(SD.HS_OPERATION_ZERO)
	if SD.GetOperationMode() ~= SD.HS_OPERATION_ZERO then      
        SD.ChangeOperationMode(SD.HS_OPERATION_ZERO)  -- transit to Normal mode, wait for server response      
    end
	
	self.completed = true
end

function OnClickModBtn2(self, evtArgs)
	lout('SelectModeFrame:OnClickModBtn1()  - Select UsualMode')
	self:DisableAll()
	
	guideMgr.ReportTutorialFirstSelectOpMode(SD.HS_OPERATION_USUAL)
	if SD.GetOperationMode() ~= SD.HS_OPERATION_USUAL then        
        SD.ChangeOperationMode(SD.HS_OPERATION_USUAL) -- transit to usual mode, wait for server response        
    end
	self.completed = true
end

function OnClickModBtn3(self, evtArgs)
	lout('SelectModeFrame:OnClickModBtn1()  - Select ClassicMode')
	self:DisableAll()
	
	guideMgr.ReportTutorialFirstSelectOpMode(SD.HS_OPERATION_CLASSIC)
	if SD.GetOperationMode() ~= SD.HS_OPERATION_CLASSIC then
        SD.ChangeOperationMode(SD.HS_OPERATION_CLASSIC) -- transit to classic mode, wait for server response
    end
	self.completed = true
end

function DisableAll(self)
	self.btnMode1:disable()
	self.btnMode2:disable()
	self.btnMode3:disable()
end

