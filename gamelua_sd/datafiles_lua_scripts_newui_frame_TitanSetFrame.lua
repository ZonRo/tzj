-- TitanSetFrame

local layoutName = 'Titan_Set.layout'

ui.defineFrame(..., layoutName)
function Init(self)
	self.pWindow = self:GetWindow()
	self.globalMouseDownConn = nil
	self.delayHide = false
	
	self.updateTimeout = 0
	self.youqingjianzhengID = {'', '', ''}
	
	self.options = {
		'Money', 'RMB',  'BindingRMB',   'PvPMoney', 'KillPlayer',
		'JJC',   'BattleMoney', 'Package', 'SocialPoint', 
		'Xingchen', 'YouQingJianZheng', 'Time',   'Delay', 'Fps',
	}
	self.optionsInUse = {}
	self.defaultChecked = { 'Money', 'RMB', 'BindingRMB', 'Package', 'Xingchen', 'Fps', 'YouQingJianZheng', }
	self.needActivateOptions = {
		['PvPMoney']   = { Key = FEATURE_ACCOMPLISHMENT_CARD, Activated = false },
		['KillPlayer'] = { Key = FEATURE_HEAD_COUNT, Activated = false },
		['JJC']        = { Key = FEATURE_BATTLE_QUEUE, Activated = false },
		['BattleMoney'] = { Key = FEATURE_BATTLE_QUEUE, Activated = false },
		['Xingchen']   = { Key = FEATURE_STAR_DROP,  Activated = false },
	}
	
	self.pXinchenNumWin = self:GetChildWindow('Titan_Set/InfoGroup/Xingchen/Num')
	if self.pXinchenNumWin then
		self.pXinchenNumWin:show()
	end
	
	self.optionGroup = {}
	
	self:_InitOptions()
	
	self:_FilterInuseOptions()
	
	self:_RelayoutInuseOptions()
	
	self:_LoadDefaultOptions()
	
	self:_ApplyDefaultOptions()
	
end

function UnInit(self)
	lout('TitanSetFrame:UnInit()')
	self:_SaveDefaultOptions()
	
	if self.globalMouseDownConn then
		self.globalMouseDownConn:disconnect()
		self.globalMouseDownConn = nil
	end
	if self.featureOpenConn then
		self.featureOpenConn:disconnect()
		self.featureOpenConn = nil
	end
	if self.featureSyncConn then
		self.featureSyncConn:disconnect()
		self.featureSyncConn = nil
	end
end

function Subscribe(self)
	self:SubscribeUIEvents()
	self:SubscribeGameEvents()
end

function SubscribeUIEvents(self)
	lout('TitanSetFrame:SubscribeEvents()')
	
	self.pWindow:subscribeEvent('Shown', self._OnAfterShow, self)
	self.pWindow:subscribeEvent('Hidden', self._OnAfterHide, self)
	self.pWindow:subscribeEvent('WindowUpdate', self._OnUpdate, self)
	
	local pBtnClose = self:GetChildWindow('Titan_Set/CloseBtn')
	pBtnClose:subscribeEvent('Clicked', self._OnClickClose, self)
	
	local pBtnClose2 = self:GetChildWindow('Titan_Set/CloseBtn2')
	pBtnClose2:subscribeEvent('Clicked', self._OnClickClose, self)
	
	local pBtnHide = self:GetChildWindow('Titan_Set/HideBtn')
	pBtnHide:subscribeEvent('Clicked', self._OnClickHideTitan, self)
	
	for k, v in pairs(self.optionGroup) do
		v.pCheck:subscribeEvent('CheckStateChanged', self._OnOptionCheckChange, self)
		v.pCheck:setUserString('target', k)
	end
end

function SubscribeGameEvents(self)
	lout('TitanSetFrame:SubscribeGameEvents()')
	self.featureOpenConn = gameEventMgr:subscribeEvent(GameEvents.FeatureOpened,  self.HandleFeatureSync, self)
	self.featureSyncConn = gameEventMgr:subscribeEvent(GameEvents.FeatureSync,    self.HandleFeatureSync, self)
end 

function HandleFeatureSync(self, args)
	lout('TitanSetFrame: HandleFeatureSync')

	self:_FilterInuseOptions()
	self:_RelayoutInuseOptions()
		-- notify TitanFrame
	if ui.TitanFrame then
		ui.TitanFrame:HandleSettingChanged()
	end
end

function GetOptionList(self)

	return self.options
end

function GetCheckedOptionList(self)
	local checked_list = {}
	
	for	i, v in ipairs(self.optionsInUse) do
		local option = self.optionGroup[v]
		
		if option.bChecked then
			table.insert(checked_list, v)
		end
	end
	
	return checked_list
end

function _LoadDefaultOptions(self)
	if not CTitanWindowHelper.LoadSettings then
		return
	end
	local default_options = CTitanWindowHelper:LoadSettings();
	if not default_options:empty() then
		local temp_func = loadstring(default_options:c_str())
		if temp_func then
			local setting = temp_func()
			if setting and type(setting) == 'table' then
				self.defaultChecked = setting
			end
		end
	end
end

function _SaveDefaultOptions(self)
	if not CTitanWindowHelper.SaveSettings then
		return
	end
	local option_string = 'return {'
	local checkedOptions = self:GetCheckedOptionList()
	
	for _, v in ipairs(checkedOptions) do
		option_string = option_string .. '"' .. v .. '", '
	end
	option_string = option_string .. '}'
	
	local option_cestring = CEGUI.String:new_local(option_string)
	CTitanWindowHelper:SaveSettings(option_cestring)
end

function _ApplyDefaultOptions(self)
	for _, v in pairs(self.defaultChecked) do
		local option = self.optionGroup[v]
		if option then
			--lout(option.Name)
			option.bChecked = true
			local pCheck = tolua.cast(option.pCheck, 'CEGUI::Checkbox')
			pCheck:setSelected(true)
		end
	end
end

function _OnAfterShow(self, evtArgs)
	local globalEventSet = CEGUI.GlobalEventSet:getSingleton()
	self.globalMouseDownConn = 
			globalEventSet:subscribeEvent('Window/MouseButtonDown', self._OnGlobalMouseDown, self)

	-- local pBtnHide = self:GetChildWindow('Titan_Set/HideBtn')
	-- if ui.TitanFrame then
		-- if ui.TitanFrame:IsVisible() then
			-- pBtnHide:setText('隐藏信息栏')
		-- else
			-- pBtnHide:setText('显示信息栏')
		-- end
	-- end
end

function _OnAfterHide(self, evtArgs)
	if self.globalMouseDownConn then
		self.globalMouseDownConn:disconnect()
		self.globalMouseDownConn = nil
	end
end

function _OnGlobalMouseDown(self, evtArgs)
	local e = tolua.cast(evtArgs, 'CEGUI::MouseEventArgs')
	if e.button ~= CEGUI.LeftButton then return end
	
	if e.window:isAncestor(self.pWindow) or self.pWindow == e.window then
		return
	end
	self.delayHide = true
end

function _OnOptionCheckChange(self, evtArgs)
	local e = tolua.cast(evtArgs, 'CEGUI::WindowEventArgs')
    local pCheck = tolua.cast(e.window, 'CEGUI::Checkbox')
    local optionName = pCheck:getUserString('target'):c_str()
	
	local option = self.optionGroup[optionName]
	if not option then
		lout('TitanSetFrame: _OnOptionCheckChange, get unexpected option: ' .. optionName)
		return
	end
	
	option.bChecked = pCheck:isSelected()
	
	-- notify TitanFrame
	if ui.TitanFrame then
		ui.TitanFrame:HandleSettingChanged()
	end
	
end

local UPDATE_TIME_INTERVAL = 2.0 -- seconds
function _OnUpdate(self, evtArgs)
	local e = tolua.cast(evtArgs, 'CEGUI::UpdateEventArgs')
	local fTime = e.lastFrameTime
	
	if self.delayHide then
		self:Hide()
		self.delayHide = false
		return
	end
	
	self.updateTimeout = self.updateTimeout + fTime
	if self.updateTimeout < UPDATE_TIME_INTERVAL then
		return
	end
	
	self.updateTimeout = 0
	
	-- Refresh option group infos
	for k,v in pairs(self.optionGroup) do
		--lout('TitanSetFrame:_OnUpdate, refresh option '.. k)
		local refreshFunc = v.funcRefresh
		
		refreshFunc(v)
	end
end

function _OnClickClose(self, evtArgs)
	
	self:Hide()
end

function _OnClickHideTitan(self, evtArgs)
	local pBtnHide = self:GetChildWindow('Titan_Set/HideBtn')
	if ui.TitanFrame then
		local bVisible = ui.TitanFrame:ToggleContainerLocked()
		local strText = bVisible and '隐藏信息栏' or '显示信息栏'
		pBtnHide:setText(strText)
	end
end

function _InitOptions(self)
	local refreshFuncs = self:_InitOptionRefreshFuncs()
	
	for i, v in ipairs(self.options) do
		local obj = {}
		obj.Name   = v
		obj.pWin   = self:GetChildWindow('Titan_Set/InfoGroup/' .. v)
		obj.pCheck = self:GetChildWindow('Titan_Set/InfoGroup/' .. v .. '/Check')
		obj.pInfo  = self:GetChildWindow('Titan_Set/InfoGroup/' .. v .. '/Num')
		obj.funcRefresh = refreshFuncs[v]
		obj.bChecked = false
		
		obj.pWin:hide()
		self.optionGroup[v] = obj
	end
	
end

function _FilterInuseOptions(self)
	self.optionsInUse = {}
		
	for k, v in pairs(self.needActivateOptions) do 
		v.Activated = SD.IsSystemFeatureOpened(v.Key)
	end
	
	for i, v in ipairs(self.options) do
		local activateState = self.needActivateOptions[v]
		if not activateState or activateState.Activated then
			table.insert(self.optionsInUse, v)
		end
	end
end

function _RelayoutInuseOptions(self)
	local height_offset = 0
	local margin = 2
	for i, v in ipairs(self.optionsInUse) do 
		local obj = self.optionGroup[v]
		local positionY = height_offset
		local optionHeight = obj.pWin:getPixelSize().height
		obj.pWin:setYPosition(CEGUI.UDim(0, positionY))
		obj.pWin:show()
		height_offset = height_offset + optionHeight + margin
	end
	self.pWindow:setHeight(CEGUI.UDim(0, 90+ height_offset))
	
end

function _InitOptionRefreshFuncs(self)
	local refreshFuncs = {}
	
	-- Money
	function s_ShowMoney(option)
		local strTxt = ui.TitanFrame.pHelper:GetMoneyString()
		option.pInfo:setText(strTxt:c_str())
	
	end
	refreshFuncs['Money'] = s_ShowMoney
	
	-- RMB
	function s_ShowRMB(option)
		local strTxt = ui.TitanFrame.pHelper:GetYuanbaoString()
		option.pInfo:setText(strTxt:c_str())
	end
	refreshFuncs['RMB'] = s_ShowRMB
	
	-- BindingRMB
	function s_ShowBindingRMB(option)
		local strTxt = ui.TitanFrame.pHelper:GetBindYuanbaoString()
		option.pInfo:setText(strTxt:c_str())
	end
	refreshFuncs['BindingRMB'] = s_ShowBindingRMB
	
	-- PVP Money
	function s_ShowPvPMoney(option)
		local strTxt = ui.TitanFrame.pHelper:GetPVPMoneyString()
		option.pInfo:setText(strTxt:c_str())
	end
	refreshFuncs['PvPMoney'] = s_ShowPvPMoney
	
	-- PVP Kill Count
	function s_ShowPvPKillCount(option)
		local strTxt = ui.TitanFrame.pHelper:GetPVPKillCountString()
		option.pInfo:setText(strTxt:c_str())
	end
	refreshFuncs['KillPlayer'] = s_ShowPvPKillCount

	-- Fatigue
	function s_ShowFatigue(option)
		local strTxt = ui.TitanFrame.pHelper:GetFatigueString()
		option.pInfo:setText(strTxt:c_str())
	end
	refreshFuncs['Fatigue'] = s_ShowFatigue

	-- JJC
	function s_ShowJJC(option)
		local strTxt = ui.TitanFrame.pHelper:GetJJCPointString()
		option.pInfo:setText(strTxt:c_str())
	end
	refreshFuncs['JJC'] = s_ShowJJC
	
	-- BattleMoney
	function s_ShowBattleMoney(option)
		local strTxt = ui.TitanFrame.pHelper:GetBattleFieldRewardMoneyString()
		option.pInfo:setText(strTxt:c_str())
	end
	refreshFuncs['BattleMoney'] = s_ShowBattleMoney

	-- Xingchen
	function s_ShowXingchen(option)
		--local strTxt = ui.TitanFrame.pHelper:GetStarDustString()
		--option.pInfo:setText(strTxt:c_str())
	end
	refreshFuncs['Xingchen'] = s_ShowXingchen
	
	-- YouQingJianZheng
	function s_ShowYouQingJianZheng(option)
		local strText = ''
		local strTipText = ''
		for i, name in ipairs(ItemHelper.YQJZ_List) do 
			local yqCfg = ItemHelper.YQJZ_Cfg[name]
			if yqCfg then
				local count = ItemHelper.GetMoneyTokenAmount(yqCfg.ItemID)
				strText = strText .. yqCfg.IconImage..tostring(count)
				strTipText = strTipText ..'\n[colour=\'FFFFFFFF\']' .. yqCfg.ItemName .. '：' .. '[colour="FF12C744"]' .. tostring(count)-- .. '今日可获取：'.. tostring(yqCfg.LimitPerDay)
				
			end
		end
		option.pInfo:setText(strText)
		Utility.SetWindowTooltip(option.pInfo, strTipText)
	end
	refreshFuncs['YouQingJianZheng'] = s_ShowYouQingJianZheng
	
	-- SocialPoint
	function s_ShowSocialPoint(option)
		local strTxt = ui.TitanFrame.pHelper:GetSocialPointString()
		option.pInfo:setText(strTxt:c_str())
	end
	refreshFuncs['SocialPoint'] = s_ShowSocialPoint

	-- LovePoint
	function s_ShowLovePoint(option)
		local strTxt = ui.TitanFrame.pHelper:GetLovePointString()
		option.pInfo:setText(strTxt:c_str())
	end
	refreshFuncs['LovePoint'] = s_ShowLovePoint
	
	-- Package
	function s_ShowPackage(option)
		local strTxt = ui.TitanFrame.pHelper:GetPackageString()
		option.pInfo:setText(strTxt:c_str())
	end
	refreshFuncs['Package'] = s_ShowPackage
	
	-- Time
	function s_ShowTime(option)
		local strTxt = ui.TitanFrame.pHelper:GetTimeString('%H:%M:%S')
		option.pInfo:setText(strTxt:c_str())
	end
	refreshFuncs['Time'] = s_ShowTime

	-- Delay
	function s_ShowDelay(option)
		local strTxt = ui.TitanFrame.pHelper:GetPingString()
		option.pInfo:setText(strTxt:c_str() .. '毫秒')
	end
	refreshFuncs['Delay'] = s_ShowDelay
	
	-- Fps
	function s_ShowFps(option)
		local strTxt = ui.TitanFrame.pHelper:GetFpsString()
		option.pInfo:setText(strTxt:c_str())
	end
	refreshFuncs['Fps'] = s_ShowFps
	
	return refreshFuncs
end


