-- FullScreenVideoFrame

local layoutName = 'FullScreen.layout'

ui.defineFrame(..., layoutName)
function Init(self)
	self.pWindow = self:GetWindow()
	self.videoName = '../video/Huangrong_CG.wmv'
	self.fTotalTime = 29 -- seconds
	self.fPlayTime = 0.0
	self.bVideoPlaying = false
	
	self.stopActions = 'BREAK_SKILL'
end

function UnInit(self)
	lout('FullScreenVideoFrame:UnInit()')
	
end

function Subscribe(self)
	self:SubscribeUIEvents()
end

function SubscribeUIEvents(self)
	lout('FullScreenVideoFrame:SubscribeUIEvents()')
	
	self.pWindow:subscribeEvent('WindowUpdate', self._OnUpdate, self)
end

function _OnUpdate(self, evtArgs)
	if not self.bVideoPlaying then 
		return 
	end
	
	if gameInput:IsActionTriggered(self.stopActions) then
		self.fPlayTime = self.fTotalTime + 1
	end
	
	local e = tolua.cast(evtArgs, 'CEGUI::UpdateEventArgs')
	self.fPlayTime = self.fPlayTime + e.lastFrameTime
	if self.fPlayTime >= self.fTotalTime then
		lout('FullScreenVideoFrame._OnUpdate: Time up, end video')
		self:HideVideo()
		DelayJobWorker.PostJob(DELAY_BY_FRAME, 1, 
			function () 
				ui.destroyFrame(ui.FullScreenVideoFrame)
			end)
	end
end

function ShowVideo(self)
	-- Play Video
	local ops = CreateStrSet(self.stopActions)
	gameInput:DisableAllActionBut(ops, SD.HS_OPERATION_MAX)
	self:PlayVideo()
	self:Show()
	
	if gameEventMgr then
		gameEventMgr:fireEvent(GameEvents.VideoBegin, SD.LuaNoArgEvent())
	end
end


function HideVideo(self)
	self:CloseVideo()
	self:Hide()
	local ops = CreateStrSet()
	gameInput:EnableAllActionBut(ops, SD.HS_OPERATION_MAX)
	
	if gameEventMgr then
		gameEventMgr:fireEvent(GameEvents.VideoEnd, SD.LuaNoArgEvent())
	end
end

function PlayVideo(self)
	if self.bVideoPlaying then
		self:CloseVideo()
	end
	
	lout('FullScreenVideoFrame.PlayVideo: ' .. self.videoName)
	local videoName = self.videoName
	SD.PlayVideoOnWnd(self.pWindow:getName():c_str(), videoName, true)
	self.bVideoPlaying = true
	self.fPlayTime = 0
end

function CloseVideo(self)
	if self.bVideoPlaying then
		SD.DeleteVideoOnWnd(self.pWindow:getName():c_str())
	end
	self.bVideoPlaying = false
	self.fPlayTime = 0
end


