
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame
	

end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)

end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	self.completed = true
	
	if self.frame and self.frame.Update then
		self.frame:Update(fTime)
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

