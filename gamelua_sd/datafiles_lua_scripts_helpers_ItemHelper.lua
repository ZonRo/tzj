local modname = ...
lout('start loading '.. modname)

module(modname, package.seeall)

COMPOSITE_BAG_RANGE  = { first = PACKAGE_POS1, last = PACKAGE_POS4,  }
EQUIPMENT_BAG_RANGE  = { first = PACKAGE_POS5, last = PACKAGE_POS8,  }
MALL_BAG_RANGE       = { first = MALL_POS1,    last = MALL_POS1,     }
TOKEN_BAG_RANGE      = { first = TOKEN_POS,    last = TOKEN_POS,     }
MATERIAL_BAG_RANGE   = { first = PACKAGE_POS9, last = PACKAGE_POS12, }

YQJZ_Cfg =  {
	['DaQingShan'] = {
		ItemID      = '2_61',
		ItemName    = '友情之证·大青山',
		LimitPerDay =  9999,
		IconImage   = '[image=\'set:Icon_Normal1 image:YouQing_Daqingshan\']'
	},
	['SaiWai'] = {
		ItemID      = '2_62',
		ItemName    = '友情之证·塞外',
		LimitPerDay =  9999,
		IconImage   = '[image=\'set:Icon_Normal1 image:YouQing_Saiwai\']'
	},
	['ZhongDu'] = {
		ItemID      = '2_63',
		ItemName    = '友情之证·中都',
		LimitPerDay =  9999,
		IconImage   = '[image=\'set:Icon_Normal1 image:YouQing_Zhongdu\']'
	},
}
YQJZ_List = { 'DaQingShan', 'SaiWai', 'ZhongDu'}

function GetItemPackage()
	local player = Utility.GetPlayerEntity()
	if not player then return nil end
	
	local itemPackage = player:GetUnit():GetItemPackage()
	return itemPackage
end

function GetMoneyTokenAmount(szItemID)
	if not szItemID then return 0 end
	if not Utility.GetTokenAmount then return 0 end
	return Utility.GetTokenAmount(szItemID)
end 

function ContainsItem(oBagPosRange, strItemID)
	if not oBagPosRange or not oBagPosRange.first or not oBagPosRange.last then
		lout('ItemHelper.ContainsItem: Invalid BagPosRange ' .. tostring(oBagPosRange))
		return false
	end
	
	local dwItemID = SD.ParseItemID(strItemID)
	if dwItemID == 0 then
		lout('ItemHelper.ContainsItem: ParseItemID failed ' .. tostring(strItemID))
		return false
	end
	local itemPackage = GetItemPackage()
	if not itemPackage then
		return false
	end
	
	local itemPos = ItemIndex:new_local()
	itemPos.m_nBagPos = -1
	
	itemPackage:GetFirstBagItem(oBagPosRange.first, oBagPosRange.last, dwItemID, itemPos)
	if itemPos.m_nBagPos == -1 then
		return false
	end
	return true
	
end