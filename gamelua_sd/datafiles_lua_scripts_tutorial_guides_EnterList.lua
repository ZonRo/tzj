
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local enterList = indicatorFlow.createIndicatorSpec()
	enterList.frameName     = 'enterList'
	enterList.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	enterList.frameText     = '可以点击进入队列开始排队，等待自动组队进入副本'
	enterList.frameWidth    = 200
	enterList.attachFunc    = nil
	enterList.attachFuncParam = self
	enterList.triggerFunc   = nil
	enterList.triggerFuncParam = nil
	enterList.triggerWin    = 'Level_Enter/Oder_Frame/BtnFrame/EnterList'
	enterList.attachWin     = 'Level_Enter/Oder_Frame/BtnFrame/EnterList'
	enterList.attachWinRoot = 'Level_Enter'
	enterList.triggerKey    = nil
	enterList.priority      = 1
	
	self.indicatorSpecs = {
		enterList, 
	}
	
	self.indicatorFlows = nil
	
	-- Server Completion
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	-- Create Indicator
	self.downIndicator = ui.createFrame('RightIndicatorFrame')
	if self.guideCfg.IndicatorText then
		self.downIndicator.pWindow:setText(self.guideCfg.IndicatorText)
	end
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
	
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end


function Update(self, fTime)
	if self.completed then
		return
	end
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end
