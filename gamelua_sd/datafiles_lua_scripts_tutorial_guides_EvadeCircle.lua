
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame
	self.counter = 0

end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	ui.TutorialVideoFrame:ShowVideo('DodgeCircle')
	
	self.counter = 0
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	ui.TutorialVideoFrame:HideVideo()
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	if SD.IsPlayerDashing() then
		lout('Scanned Dashing...')
		self.counter = self.counter + 1
		if self.counter >= 3 then
			self.completed = true
		end
	end
	
	if self.frame and self.frame.Update then
		self.frame:Update(fTime)
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

