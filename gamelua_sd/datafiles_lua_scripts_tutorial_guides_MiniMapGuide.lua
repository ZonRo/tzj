
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	self.rightIndicator = nil
	self.windowAttached = false
	
	self.doodadCount = 0
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	-- Create Indicator
	self.rightIndicator = ui.createFrame('RightIndicatorFrame')
	if self.guideCfg.IndicatorText then
		self.rightIndicator.pWindow:setText(self.guideCfg.IndicatorText)
	end
	
	-- Attach Callback
	self.displayConn = gameEventMgr:subscribeEvent('EventTaskFinished', self.OnEvent, self)
	
	-- Attach to MainMenu_TempEquip Window
	self:TryAttachWindow()
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	if self.rightIndicator then
		ui.destroyFrame(self.rightIndicator)
		self.rightIndicator = nil
	end
	
	if self.displayConn then
		self.displayConn:disconnect()
		self.displayConn = nil
	end
	
	self.windowAttached = false
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function TryAttachWindow(self)
	if self.windowAttached then 
		return
	end
	
	local dragBtn = InitWindowPtr('Map_LD/Frame/Map')
	if not dragBtn or not dragBtn:isVisible() then
		return
	end
	local vSize = self.rightIndicator.pWindow:getPixelSize()
	local uvPos = CEGUI.UVector2(CEGUI.UDim(0, -vSize.width*0.2), CEGUI.UDim(0, vSize.height*0.6))
		--local uvPos = CEGUI.UVector2(CEGUI.UDim(0.5, -vSize.width/2), CEGUI.UDim(0, -vSize.height))
	local vPos  = CEGUI.CoordConverter:windowToScreen(dragBtn, uvPos)
	local uvNewPos = CEGUI.UVector2(CEGUI.UDim(0, vPos.x), CEGUI.UDim(0, vPos.y))
	
	self.rightIndicator.pWindow:setPosition(uvNewPos)
	self.rightIndicator:Show()
	--UiUtility.AttachWndToWnd(self.rightIndicator.pWindow, dragBtn, UiUtility.PUT_TOP)
	-- Add Animation
	local animName = guideMgr.indicator_animations['RIGHT']
	SD.WndAnimManager:Instance():CreateAnim(self.rightIndicator.pWindow, animName, false, true, true)
	
	self.windowAttached = true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	self:TryAttachWindow()
end

function OnEvent(self, args)
	if not self.completed then
		local wrappedArgs = tolua.cast(args, 'SD::LuaWrappedEvtArg')
		local realArgs = tolua.cast(wrappedArgs.realArgs, 'SD::EventTaskFinished')
	
		if realArgs.m_dwTaskID == 853 then
			lout('MiniMapGuide: OnEvent, Complete Guide On Task ' .. realArgs.m_dwTaskID ..' Finished ')
			self.completed = true
		end
	end
end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

