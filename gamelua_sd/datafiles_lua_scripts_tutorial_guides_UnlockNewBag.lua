
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame
	
	local openBag = indicatorFlow.createIndicatorSpec()
	openBag.frameName     = 'openBag'
	openBag.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	openBag.frameText     = '点击打开包裹'
	openBag.attachFunc    = self.OnOpenBagAttached
	openBag.attachFuncParam = self
	openBag.triggerFunc   = nil
	openBag.triggerFuncParam = nil
	openBag.triggerWin    = 'Root/MainMenu/PackageBtn'
	openBag.attachWin     = 'Root/MainMenu/PackageBtn'
	openBag.attachWinRoot = 'MainMenu_FunctionAndEXP'
	openBag.triggerKey    = 'PackageUI'
	openBag.priority      = 1
	
	local unlockBagPos = indicatorFlow.createIndicatorSpec()
	unlockBagPos.frameName     = 'unlockBagPos'
	unlockBagPos.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	unlockBagPos.frameText     = '解锁新的包裹栏位'
	unlockBagPos.attachFunc    = nil
	unlockBagPos.attachFuncParam = nil
	unlockBagPos.triggerFunc   = nil
	unlockBagPos.triggerFuncParam = nil
	unlockBagPos.triggerWin    = nil
	unlockBagPos.attachWin     = nil
	unlockBagPos.attachWinRoot = 'Root/PackageFrame'
	unlockBagPos.priority      = 2
	
	local confirmUnlockBag = indicatorFlow.createIndicatorSpec()
	confirmUnlockBag.frameName     = 'confirmUnlockBag'
	confirmUnlockBag.frameType     = indicatorFlow.INDICATOR_FRAME_UP
	confirmUnlockBag.frameText     = '点击确定解锁'
	confirmUnlockBag.attachFunc    = self.OnConfirmUnlockAttached
	confirmUnlockBag.attachFuncParam = self
	confirmUnlockBag.triggerFunc   = self.OnConfirmUnlockTriggered
	confirmUnlockBag.triggerFuncParam = self
	confirmUnlockBag.triggerWin    = 'Package_UnlockGrid/Confirm'
	confirmUnlockBag.attachWin     = 'Package_UnlockGrid/Confirm'
	confirmUnlockBag.attachWinRoot = 'Package_UnlockGrid'
	confirmUnlockBag.priority      = 3
	
	
	local openBagGeneralPage = indicatorFlow.createIndicatorSpec()
	openBagGeneralPage.frameName     = 'openBagGeneralPage'
	openBagGeneralPage.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	openBagGeneralPage.frameText     = '点击切换至综合包裹'
	openBagGeneralPage.attachFunc    = self.OnOpenBagGeneralPageAttached
	openBagGeneralPage.attachFuncParam = self
	openBagGeneralPage.triggerFunc   = nil
	openBagGeneralPage.triggerFuncParam = nil
	openBagGeneralPage.triggerWin    = 'Root/PackageFrame/TagContainer1/Btn1_0_0'
	openBagGeneralPage.attachWin     = 'Root/PackageFrame/TagContainer1/Btn1_0_0'
	openBagGeneralPage.attachWinRoot = 'Root/PackageFrame'
	openBagGeneralPage.priority      = 4
	
	local placeBagItemToPos = indicatorFlow.createIndicatorSpec()
	placeBagItemToPos.frameName     = 'placeBagItemToPos'
	placeBagItemToPos.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	placeBagItemToPos.frameText     = '右键点击综合小包将其放至栏位 '
	placeBagItemToPos.attachFunc    = nil
	placeBagItemToPos.attachFuncParam = nil
	placeBagItemToPos.triggerFunc   = nil
	placeBagItemToPos.triggerFuncParam = nil
	placeBagItemToPos.attachType    = indicatorFlow.INDICATOR_ATTACH_BAGITEM
	placeBagItemToPos.attachItemID  = nil
	placeBagItemToPos.priority      = 5
	
	self.indicatorSpecs = {
		openBag, unlockBagPos, confirmUnlockBag,
		openBagGeneralPage, placeBagItemToPos
	}

	self.unlockBagPosSpec = unlockBagPos
	self.placeBagItemToPosSpec = placeBagItemToPos
	
	self.indicatorFlows = nil
end
function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	local unit = GetPlayersUnit()
	if not unit then
		lout('UnlockNewBag:Enter - GetProfession failed')
		return
	end
	
	-- Check if player has unlocked any bag pos by himself, if so, it is no need to guide him.
	local pItemBag = unit:GetItemPackage()
	if pItemBag and pItemBag.IsBagPosOpened then
		local itemBagPoses = {
			PACKAGE_POS2, PACKAGE_POS3, PACKAGE_POS4,
			PACKAGE_POS6, PACKAGE_POS7, PACKAGE_POS8, 
			PACKAGE_POS10, PACKAGE_POS11, PACKAGE_POS12,
		}
		local bAnyOpened = false
		for _, v in ipairs(itemBagPoses) do
			bAnyOpened = pItemBag:IsBagPosOpened(v)
			if bAnyOpened then break end
		end
		if bAnyOpened then
			self.completed = true
			return
		end
	end
	-----------
	
	local targetItemID = '8_7'
	local targetBagType = BAG_ITEM_GENERAL
	-- local bagItemTypes = { BAG_ITEM_MATERIAL, BAG_ITEM_EQUIPMENT, BAG_ITEM_GENERAL}
	-- for i, v in ipairs(bagItemTypes) do
		-- local strItemID = SD.GetNthBagItemID(ITEM_TABLE_BAG, v, 0)
		-- if strItemID == targetItemID then
			-- targetItemID = strItemID
			-- targetBagType = v
			-- break
		-- end
	-- end
	lout('UnlockNewBag: New BagItem ID: ' .. targetItemID..'Type: '..tostring(targetBagType))
	if targetItemID ~= '' and targetBagType then
		lout('UnlockNewBag: New BagItem ID: ' .. targetItemID)
		self.placeBagItemToPosSpec.attachItemID = targetItemID
		
		if targetBagType == BAG_ITEM_MATERIAL then
			self.unlockBagPosSpec.attachWin = 'Root/PackageFrame/Icon10__auto_dragcontainer__'
			self.unlockBagPosSpec.triggerWin = 'Root/PackageFrame/Icon10__auto_dragcontainer__'
			
		elseif targetBagType == BAG_ITEM_EQUIPMENT then
			self.unlockBagPosSpec.attachWin = 'Root/PackageFrame/Icon6__auto_dragcontainer__'
			self.unlockBagPosSpec.triggerWin = 'Root/PackageFrame/Icon6__auto_dragcontainer__'
			
		elseif targetBagType == BAG_ITEM_GENERAL then
			self.unlockBagPosSpec.attachWin = 'Root/PackageFrame/Icon2__auto_dragcontainer__'
			self.unlockBagPosSpec.triggerWin = 'Root/PackageFrame/Icon2__auto_dragcontainer__'
		end
	end
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	if self.indicatorFlows then
		indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
	end
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnOpenBagAttached(self, indicator)
	local pWin = InitWindowPtr('Root/PackageFrame')
	if pWin and pWin:isVisible() then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function OnOpenBagGeneralPageAttached(self, indicator)
	local pBagItemWin = SD.GetFirstBagItemButtonContainer(self.placeBagItemToPosSpec.attachItemID)
	if pBagItemWin then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function OnConfirmUnlockAttached(self, indicator)
	local pRootWin = InitWindowPtr(indicator.specInst.attachWinRoot)
	pRootWin:setProperty('AutoRenderingSurface', 'False')
end

function OnConfirmUnlockTriggered(self, indicator)
	--local pRootWin = InitWindowPtr(indicator.specInst.attachWinRoot)
	--pRootWin:setProperty('AutoRenderingSurface', 'True')
end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

