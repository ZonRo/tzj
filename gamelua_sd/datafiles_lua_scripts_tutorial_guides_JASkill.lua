
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame
	self.skillBookName = 'SkillBook'
	

	self.downAnimName = nil
	self.downIndicator = nil
	
	self.rightAnimName = nil
	self.rightIndicator = nil
	
	self.bSkillBtnAttached = false
	self.bJaBtnAttached  = false
	
	self.bSkillBtnClicked = false
	self.eventSkillBtn = nil
	
	self.fJaAttachTime = 0.0
	
	self.nStartFrame = 0
	
	self.JaPositions = 
	{
	[WM_PROF_XIANGLONG] = {x = 335, y = 92},
	[WM_PROF_BAIZHANG] = {x = 335, y = 92},
	[WM_PROF_YAOGUANG] = {x = 395, y = 92},
	[WM_PROF_JIYING] = {x = 335, y = 92},
	}
	
end

function UnInit(self)

end

function CreateIndicators(self)
	-- Create Indicator
	self.downIndicator = ui.createFrame('DownIndicatorFrame')
	self.downIndicator.pWindow:setText('点击下方的技能图标选中技能或点 + 按钮升级技能')
	self.downAnimName = guideMgr.indicator_animations['DOWN']
	
	self.rightIndicator = ui.createFrame('RightIndicatorFrame')
	self.rightIndicator.pWindow:setText('点击右边技能图标上的 + 按钮学习新的技能变招')
	self.rightAnimName = guideMgr.indicator_animations['RIGHT']
end

function DestroyIndicators(self)
	if self.downIndicator then
		ui.destroyFrame(self.downIndicator)
		self.downIndicator = nil
	end
	
	if self.rightIndicator then
		ui.destroyFrame(self.rightIndicator)
		self.rightIndicator = nil
	end
end

function DisconnectEvents(self)
	if self.eventSkillBtn then 
		self.eventSkillBtn:disconnect() 
		self.eventSkillBtn = nil
	end
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	self:CreateIndicators()
	self.fJaAttachTime = 0.0
	
	-- Attach to MainMenu_TempEquip Window
	--self:TryAttachWindow()
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	
	self:DestroyIndicators()
	self:DisconnectEvents()
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end


function TryAttachWindow(self)
	if self.bSkillBtnAttached and self.bJaBtnAttached then
		return
	end
	
	-- First should click the map button
	if not self.bSkillBtnAttached then
		local unit = GetPlayersUnit()
		if not unit or not unit.GetProfession then
			lout('JaSkill:TryAttachWindow - GetProfession failed')
			return
		end
		
		local prefix = 'SkillBook/KongfuBG/SchoolSkillBG/AutoHighBG/tempelate/AutoHighBG'
		local windowName = nil
		local eProf = unit:GetProfession()
		if eProf == WM_PROF_XIANGLONG or eProf == WM_PROF_BAIZHANG or eProf == WM_PROF_JIYING then
			-- For ShaoLin/GaiBang(左键Ja)
			windowName = prefix .. '/template1/skill__auto_dragcontainer__'
		elseif eProf == WM_PROF_YAOGUANG then
			-- For QuanZhen(右键Ja)
			windowName = prefix .. '/template4/skill__auto_dragcontainer__'
		else 
			lout('JaSkill:TryAttachWindow - GetProfession invalid ' .. tostring(eProf))
			return
		end
		
		local dragBtn = InitWindowPtr(windowName)
		if  not dragBtn or not dragBtn:isVisible() then
			return
		end
		
		self.eventSkillBtn = dragBtn:subscribeEvent('MouseClick', self.OnSkillBtnClicked, self)
		
		local vSize = self.downIndicator.pWindow:getPixelSize()
		local uvPos = CEGUI.UVector2(CEGUI.UDim(0.5, -vSize.width/2), CEGUI.UDim(0, -vSize.height))
		local vPos  = CEGUI.CoordConverter:windowToScreen(dragBtn, uvPos)
		local uvNewPos = CEGUI.UVector2(CEGUI.UDim(0, vPos.x), CEGUI.UDim(0, vPos.y))
		
		self.downIndicator.pWindow:setPosition(uvNewPos)
		self.downIndicator:Show()
		--UiUtility.AttachWndToWnd(self.downIndicator.pWindow, dragBtn, UiUtility.PUT_TOP)
		-- Add Animation
		SD.WndAnimManager:Instance():PlayAnim(
				self.downIndicator.pWindow, self.downAnimName, true)
	
		self.bSkillBtnAttached = true
	end
	
	-- Then should click the new button
	if self.bSkillBtnAttached and self.bSkillBtnClicked and not self.bJaBtnAttached then
		local dragBtn = InitWindowPtr('SkillBook/Improve/Zhaoshi')
		if  not dragBtn or not dragBtn:isVisible() then
			return
		end
		
		self.rightIndicator.pWindow:setProperty('AlwaysOnTop', 'True')
		
		local unit = GetPlayersUnit()
		if not unit or not unit.GetProfession then
			lout('JaSkill:TryAttachWindow - GetProfession failed')
			return
		end
		local eProf = unit:GetProfession()
		local vPos = self.JaPositions[eProf]
		local vSize = self.downIndicator.pWindow:getPixelSize()
		local uvNewPos = CEGUI.UVector2(CEGUI.UDim(0, vPos.x - vSize.width), CEGUI.UDim(0, vPos.y - vSize.height/2 -10))
		if eProf == WM_PROF_YAOGUANG then --全真
				uvNewPos = CEGUI.UVector2(CEGUI.UDim(0, vPos.x - vSize.width), CEGUI.UDim(0, vPos.y - vSize.height/2 -10))
		end
		if not uvNewPos then
			lout('JaSkill:TryAttachWindow - Get JaPositions failed')
			return
		end
		self.rightIndicator:Show()
		dragBtn:addChildWindow(self.rightIndicator.pWindow)
		self.rightIndicator.pWindow:setPosition(uvNewPos)
		
		
		-- Add Animation
		SD.WndAnimManager:Instance():PlayAnim(
				self.rightIndicator.pWindow, self.rightAnimName, true)
		
		self.bJaBtnAttached = true
	end
end

function Update(self, fTime)
	if self.completed then
		return
	end

	self.nStartFrame = self.nStartFrame + 1
	if self.nStartFrame < 5 then
		if self.nStartFrame == 1 then
			SD.GenericFunc('ShowCWindow', self.skillBookName)
		end
		return
	end
	
	if self.bSkillBtnAttached or self.bJaBtnAttached then
		local pWin = InitWindowPtr(self.skillBookName)
		if pWin and not pWin:isVisible() then
			lout('completed1')
			self.completed = true
		end
	end
	
	self:TryAttachWindow()
	
	-- if self.bSkillBtnClicked and self.bJaBtnAttached then
		-- self.fJaAttachTime = self.fJaAttachTime + fTime
		-- if self.fJaAttachTime >= 5 then
			-- lout('completed2')
			-- self.completed = true
		-- end
	-- end
end

function Reset(self)
	self.bSkillBtnAttached = false
	self.bJaBtnAttached  = false
	
	self.bSkillBtnClicked = false
	self.fJaAttachTime = 0.0
	
	self:DestroyIndicators()
	self:DisconnectEvents()
	
	self:CreateIndicators()
end

function OnSkillBtnClicked(self, evtArgs)
	self.bSkillBtnClicked = true
	self.downIndicator:Hide()
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

