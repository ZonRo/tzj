local modname = ...
lout('start loading '.. modname)

module(modname, package.seeall)

-------------------------------------------------------------------------------
-- configurable data
-------------------------------------------------------------------------------
local MAPINFO = {
   [3] = {'序章', '牛家村', '适合玩家等级(1-5)级', '？？'}
,  [13]= {'序章', '试剑崖', '适合玩家等级(6-7)级',}
,  [14]= {'序章', '试剑崖', '适合玩家等级(6-7)级',}
,  [15]= {'序章', '试剑崖', '适合玩家等级(6-7)级',}
,  [16]= {'序章', '试剑崖', '适合玩家等级(6-7)级',}
,  [10]= {'序章', '子午谷', '适合玩家等级(8-11)级', '？？'} 
,  [4] = {'第一章', '大青山下', '适合玩家等级(10-30)级\n一级钓鱼区',}
,  [20]= {'第一章', '苏木石林', '适合玩家等级(13-21)级\n二级钓鱼区',}
,  [21]= {'第一章', '乞颜禁地', '适合玩家等级(16-23)级\n二级钓鱼区',}
,  [22]= {'第一章', '废土之墟', '适合玩家等级(26-30)级\n二级钓鱼区（副本）',}
,  [12]= {'第一章', '塞外', 		'适合玩家等级(30-45)级\n三级钓鱼区',}
,  [29]= {'第一章', '山之岩', '适合玩家等级(31-38)级\n四级钓鱼区（副本）',}
,  [23]= {'第一章', '楼兰故地', '适合玩家等级(34-40)级\n三级钓鱼区',}
,  [24]= {'第一章', '金沙瀑布', '适合玩家等级(38-45)级',}
,  [84]= {'第一章', '中都', '适合玩家等级(45-60)级\n四级钓鱼区',}
,  [30]= {'第一章', '秋水岭', '适合玩家等级(45-50)级\n四级钓鱼区',}
,  [31]= {'第一章', '小隐溪', '适合玩家等级(48-53)级\n四级钓鱼区',}
,  [32]= {'第一章', '雪兰湖', '适合玩家等级(53-58)级\n五级钓鱼区',}
,  [33]= {'第一章', '沙门岛', '适合玩家等级(58-65)级\n五级钓鱼区',}
,  [11]= {'第二章', '嵩山', '适合玩家等级(？？)级', '嵩云岭    ？？级', '嵩山大佛  ？？级', '钜子之门  ？？级'}
,  [6] = {'第二章', '鲁南丘陵', '适合玩家等级(？？)级', '难老泉  ？？级', '水龙窟  ？？级'}
,  [7] = {'第二章', '姜庙镇', '适合玩家等级(？？)级', '竹谷一线天  ？？级', '？？'}
,  [5] = {'第二章', '太湖', '适合玩家等级(？？)级', '？？', '？？'}
,  [81]= {'门派', 	'全真', '适合玩家等级(？？级', '？？'}
,  [86]= {'门派', 	'桃花岛', '适合玩家等级(？？级\n二级钓鱼区', '？？'}
,  [87]= {'门派', 	'丐帮', '适合玩家等级(？？)级\n二级钓鱼区', '？？'}
,  [88]= {'门派', 	'少林', '适合玩家等级(？？)级\n二级钓鱼区', '？？'}
}

local PLAYER_POS_ON_WORLDMAP = {
   [3] = {x = 414, y = 199}
,  [13]= {x = 433, y = 329}
,  [14]= {x = 433, y = 329}
,  [15]= {x = 433, y = 329}
,  [16]= {x = 433, y = 329}
,  [10]= {x = 383, y = 260}  
,  [4] = {x = 216, y = 81}
,  [20] = {x = 210, y = 162}
,  [21] = {x = 314, y = 172}
,  [22] = {x = 347, y = 99}
,  [12]= {x = 460, y = 115}
,  [23]= {x = 434, y = 56}
,  [24]= {x = 500, y = 67}
,  [29]= {x = 568, y = 50}
,  [84]= {x = 645, y = 124}
,  [30]= {x = 752, y = 72}
,  [31]= {x = 683, y = 25}
,  [32]= {x = 803, y = 65}
,  [33]= {x = 861, y = 165}
,  [11]= {x = 518, y = 241}
,  [6] = {x = 656, y = 237}
,  [7] = {x = 694, y = 369}
,  [5] = {x = 751, y = 471}
,  [81]= {x = 430, y = 365}
,  [86]= {x = 880, y = 466}
,  [87]= {x = 556, y = 448}
,  [88]= {x = 490, y = 322}
}

local YEWAI_INFO = {
 	[10] = {'子午谷   8-10级',},
  [13] = {'试炼场   6-7级',},
  [14] = {'试炼场   6-7级',},
  [15] = {'试炼场   6-7级',},
	[16] = {'试炼场   6-7级',},
	[4]  = {'塔拉草海 10-25级',},
	[20] = {'魅影石林 14-24级',},
	[21] = {'雁丘河谷 17-23级',},
	[22] = {'起源之洞 26-29级',},	
	[12] = {'黑风山   30-44级',},
	[29] = {'迷雾森林 31-44级','炎狱火山 31-44级',},
	[23] = {'死亡绿洲 31-41级',},
	[24] = {'落日沙漠 31-41级',},			
	[30] = {'叶舞秋山 45-51级',},
	[31] = {'温泉山麓 48-53级',},
	[32] = {'冰风谷   53-58级',},
	[33] = {'蓬莱仙境 58-65级',},
	[84] = {'蟠龙府邸 54-59级',},	
	[81] = {'风雪惊变 27-28级',},	
	[86] = {'桃花映血 27-28级',},	
	[87] = {'君山之劫 27-28级',},	
	[88] = {'禅林风波 27-28级',},								
}

local WUREN_INFO = {
	[21] = { '起源之地 24-45级'},
	[29] = { '狼居胥山 31-45级'},
	[24] = { '塞外古城 43-45级'},	
	[32] = { '长白山   48-55级'},
	[33] = { '吉永号   55-65级'},		
	[84] = { '赵王府   60-65级'},
}
-- used by sceneMap, instance map
IMG_CHAPTER_MAP = {
   [3] = 'set:Total_Map2 image:xiaoyuancun'
,  [13]= 'set:Total_Map2 image:shijianya'
,  [14]= 'set:Total_Map2 image:shijianya'
,  [15]= 'set:Total_Map2 image:shijianya'
,  [16]= 'set:Total_Map2 image:shijianya'
,  [47]= 'set:Total_Map2 image:langshan'
,  [48]= 'set:Total_Map2 image:saiwaigucheng'
,  [49]= 'set:Total_Map2 image:changbaishan'
,  [11]= 'set:Total_Map2 image:songshan'
,  [4]= 'set:Total_Map2 image:menggu'
,  [12]= 'set:Total_Map2 image:saiwai'
,  [84]= 'set:Total_Map2 image:zhongdu'
}

local TEAM_MEMBER_MAP_TIPS_USER_STRING = 'team_member_role_id'
-------------------------------------------------------------------------------
-- utility functions
-------------------------------------------------------------------------------
local function getDesc(newMapId)
	local fmt =
[[[font='MSYH-11-BOLD'][colour='FFf0d8aa']%s   %s
[font='MSYH-10-BOLD'][colour='FF85fc28']%s
[font='MSYH-11-BOLD'][colour='FFf0d8aa']小副本群
[font='MSYH-10-BOLD'][colour='FFf9cd00']%s[font='MSYH-11-BOLD'][colour='FFf0d8aa']核心副本
[font='MSYH-10-BOLD'][colour='FFf9cd00']%s
]]
	local infos = MAPINFO[newMapId]
	local yewaiInfo = YEWAI_INFO[newMapId]
	local wurenInfo = WUREN_INFO[newMapId]
	local cnt = #infos
	Assert(cnt > 2)
	
	-- 地图名
	local mapName = infos[1]
	local chapter = infos[2]
	local fitLevel = infos[3]
		
	-- 小副本群
	local yewaiDesc = ''
	if yewaiInfo then
		for i, v in ipairs(yewaiInfo) do
			yewaiDesc = yewaiDesc .. v ..'\n'
		end
	else
		yewaiDesc =  '？？\n'
	end
	
	-- 核心副本
	local wurenDesc = ''
	if wurenInfo then
		for i, v in ipairs(wurenInfo) do
			wurenDesc = wurenDesc .. v ..'\n'
		end
	else
		wurenDesc =  '？？'
	end
	return string.format(fmt, mapName, chapter, fitLevel, yewaiDesc, wurenDesc)
end


-------------------------------------------------------------------------------
-- global callback
-------------------------------------------------------------------------------
function OnEnterScene()
	local iconMe = wndMgr:getWindow('WorldMap/IconMe')
	if not iconMe then return end
	
	local sceneMgr = SD.CSceneInfoMgr:Instance()

	-- show me
	local myMapId = sceneMgr:GetCurrentMapID()

	if SD.GenericFunc('IsInInstance', myMapId) == 'false' then
		local x, y
		local icon_pos = PLAYER_POS_ON_WORLDMAP[myMapId]
		if icon_pos and icon_pos ~= 0 and icon_pos.y ~= 0 then
			x = icon_pos.x
			y = icon_pos.y
			lout(type(x) .. tostring(x))
		else
			local info = sceneMgr:GetSceneBasicInfoEx(myMapId)
			x = info.m_leftInWorldMap + info.m_widthInWorldMap * 0.5
			y = info.m_topInWorldMap + info.m_heightInWorldMap * 0.5
		end
		
		iconMe:setPosition(CEGUI.UVector2(CEGUI.UDim(0, x), CEGUI.UDim(0, y)))
		iconMe:setVisible(true)
	else
		iconMe:setVisible(false)
	end


end

function OnInit()
	OnEnterScene()
end

function OnUpdate(arg)
	local members = Utility.GetAllTeammateInfo()
	if not members then return end
	
	local cnt = members:size()
	local sceneMgr = SD.CSceneInfoMgr:Instance()

	-- show me
	-- local myMapId = sceneMgr:GetCurrentMapID()
	-- if SD.GenericFunc('IsInInstance', myMapId) == 'false' then
		-- local info = sceneMgr:GetSceneBasicInfoEx(myMapId)
		-- local x = info.m_leftInWorldMap + info.m_widthInWorldMap * 0.5
		-- local y = info.m_topInWorldMap + info.m_heightInWorldMap * 0.5
		-- iconMe:setPosition(CEGUI.UVector2(CEGUI.UDim(0, x), CEGUI.UDim(0, y)))
		-- iconMe:setVisible(true)
	-- else
		-- iconMe:setVisible(false)
	-- end

	-- show member
	local icons = {}
	icons[i] = wndMgr:getWindow('WorldMap/IconMember'..i)
	local dwCurChannelID = Utility.GetCurrentChannelID()
	for i = 0, 4 do
		local ic = icons[i]

		if i < cnt then
			local memberData = members[i]
			local teamMateMapId = memberData.m_dwMapID
			local isOnline = memberData.m_bIsOnline
			local isSameChannel = memberData.m_oLine.m_dwChannelID ==  dwCurChannelID
			--lout('teammate'..i..':'..teamMateMapId)
			if isOnline and isSameChannel and 
				  memberData.m_qwRoleID ~= Utility.GetPlayerEntity():GetID() and
				  SD.GenericFunc('IsInInstance', teamMateMapId) == 'false'   then
				ic:setVisible(true)
				local info = sceneMgr:GetSceneBasicInfoEx(teamMateMapId)
				Assert(info ~= nil)
				local x = info.m_leftInWorldMap + info.m_widthInWorldMap * 0.5
				local y = info.m_topInWorldMap + info.m_heightInWorldMap * 0.5
				ic:setPosition(CEGUI.UVector2(CEGUI.UDim(0, x), CEGUI.UDim(0, y)))
				ic:setUserString(TEAM_MEMBER_MAP_TIPS_USER_STRING, U64ToStr(memberData.m_qwRoleID))
			else
				ic:setVisible(false)
			end
		else
			ic:setVisible(false)
		end
	end

end


function OnHoverMap(oldMapId, newMapId)
	local descWnd = wndMgr:getWindow('WorldMap/MapFrame/TextLayer/Desc')
	if newMapId == 0 then
		descWnd:hide()
	else
		-- tag window colour change
		if oldMapId > 0 then
			local oldTag = wndMgr:getWindow('Desc'..oldMapId)
			if oldTag ~= nil then
				oldTag:setProperty('TextColour', 'FFFFFFFF')
			end
		end

		local newTag = wndMgr:getWindow('Desc'..newMapId)
		if newTag ~= nil then
			newTag:setProperty('TextColour', 'FF0000FF')
		end

		-- description text
		local infos = MAPINFO[newMapId]
		if infos == nil then
			lout('unexpected newMapId '..newMapId)
		else
			descWnd:setText(getDesc(newMapId))
			descWnd:show()
		end

	end
end

-------------------------------------------------------------------------------
-- init & fini callback
-------------------------------------------------------------------------------
--function Load()
--    lout(modname..'.Load()')
--end
--
--function Unload()
--    lout(modname..'.Unload()')
--end

-------------------------------------------------------------------------------
-- entry point
-------------------------------------------------------------------------------
lout('end loading '.. modname)

