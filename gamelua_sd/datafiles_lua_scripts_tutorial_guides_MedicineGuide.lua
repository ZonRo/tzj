
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	self.downIndicator = nil  	
	self.windowAttached = false
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	-- Create Indicator
	self.downIndicator = ui.createFrame('DownIndicatorFrame')
	if self.guideCfg.IndicatorText then
		self.downIndicator.pWindow:setText(self.guideCfg.IndicatorText)
	end
	
	-- Attach to MainMenu_TempEquip Window
	self:TryAttachWindow()
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	if self.downIndicator then
		ui.destroyFrame(self.downIndicator)
		self.downIndicator = nil
	end
	
	self.windowAttached = false
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function TryAttachWindow(self)
	if self.windowAttached then 
		return
	end
	
	local dragBtn = InitWindowPtr('DefaultMainMenu/ItemFrame1/Item5')
	if not dragBtn or not dragBtn:isVisible() then
		return
	end
	local vSize = self.downIndicator.pWindow:getPixelSize()
	local uvPos = CEGUI.UVector2(CEGUI.UDim(0.5, vSize.width/20), CEGUI.UDim(0, -vSize.height*1.1))
		--local uvPos = CEGUI.UVector2(CEGUI.UDim(0.5, -vSize.width/2), CEGUI.UDim(0, -vSize.height))
	local vPos  = CEGUI.CoordConverter:windowToScreen(dragBtn, uvPos)
	local uvNewPos = CEGUI.UVector2(CEGUI.UDim(0, vPos.x), CEGUI.UDim(0, vPos.y))
	
	self.downIndicator.pWindow:setPosition(uvNewPos)
	self.downIndicator:Show()
	--UiUtility.AttachWndToWnd(self.downIndicator.pWindow, dragBtn, UiUtility.PUT_TOP)
	-- Add Animation
	local animName = guideMgr.indicator_animations['DOWN']
	SD.WndAnimManager:Instance():CreateAnim(self.downIndicator.pWindow, animName, false, true, true)
	
	self.windowAttached = true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	self:TryAttachWindow()
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

