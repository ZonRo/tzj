--Zero Mode
OperationMapping(
0,	--Operation Mode
3,	--Camera Mode
1,	--Anchor Mode
1,	--MouseOp Mode
0 	--ActionMapping Mode
)

--Usual Mode
OperationMapping(
1,	--Operation Mode
5,	--Camera Mode
1,	--Anchor Mode
1,	--MouseOp Mode
5 	--ActionMapping Mode
)

--Classic Mode
OperationMapping(
2,	--Operation Mode
4,	--Camera Mode
1,	--Anchor Mode
1,	--MouseOp Mode
1 	--ActionMapping Mode
)

--Family Mode
OperationMapping(
3,	--Operation Mode
2,	--Camera Mode
3,	--Anchor Mode
1,	--MouseOp Mode
4 	--ActionMapping Mode
)

--FPS Mode
OperationMapping(
4,	--Operation Mode
0,	--Camera Mode
2,	--Anchor Mode
1,	--MouseOp Mode
2 	--ActionMapping Mode
)

--By ZhangChao for Free Operation Mode
OperationMapping(
5,	--Operation Mode
1,	--Camera Mode
0,	--Anchor Mode
0,	--MouseOp Mode
3 	--ActionMapping Mode
)

--Parameter for Free Camera
FreeCameraPara(
1,	--distance from camera to lookat point
12, 	--move speed
1,	--rotate buffer enabled? "0" Yes "1" No
5,	--rotate buffer speed
2,	--locked target rotate speed
50,	--focus hor offset
50, --focus ver offset
0.5	--focus adjust speed
)

--------------------------------------------

--By ZhangChao for FPSCamera
OpCameraPara(
0,	--Camera Mode
0.07,	--X_RotateSpeed
0.07, --Z_RotateSpeed
55.0,	--Max_ElevationAngle
-55.0,	--Min_ElevationAngle
0.0,	--Default_ElevationAngle
5.0,	--MouseWheelSpeed
180.0,	--Max_Distance
30.0,	--Min_Distance
105.0,	--DefaultDistance
0.3,	--Buffered Time
0.3,	--Buffered Percent
1.0,	--Stoppage Time Buffer
8.0,	--Collision Radius
100.0,	--Camera Turning Speed
20,	--Camera Collision Soft Speed
6,	--Camera Mode Exchange Speed
4.0,	--Camera Rotating Speed for Key Dragging
55,	--Camera Fov
5,	--Camera Distance Degree for adjusting distance by keyboard
30, --Camera Opacity Distance
4,   --"1": Adjust_Hor_Only	"2":Adjust_When_Moving "3":Adjust_Whenever "4":Non
1,   --"1": Adjust_Hor_Only	"2":Adjust_When_Moving "3":Adjust_Whenever "4":Non
135,		--Follow Speed
0,			--Intelligent Camera : 1 Yes, 0 No.
0,		--Sync X Z Rotate Speed
0			--Mouse Wheel Flip
)

--By ZhangChao for FreeCamera
OpCameraPara(
1,	--Camera Mode
0.07,	--X_RotateSpeed
0.07,	--Z_RotateSpeed
80.0,	--Max_ElevationAngle
-80.0,	--Min_ElevationAngle
-25.0,	--Default_ElevationAngle
10.0,	--MouseWheelSpeed
180.0,	--Max_Distance
10.0,	--Min_Distance
130.0,	--DefaultDistance
0.3,	--Buffered Time
0.3,	--Buffered Percent
1.0,	--Stoppage Time Buffer
8.0,	--Collision Radius
100.0,	--Camera Turning Speed
10,	--Camera Collision Soft Speed
6,	--Camera Mode Exchange Speed
4.0,	--Camera Rotating Speed for Key Dragging
46,	--Camera Fov
5,	--Camera Distance Degree for adjusting distance by keyboard
30, --Camera Opacity Distance
4,   --"1": Adjust_Hor_Only	"2":Adjust_When_Moving "3":Adjust_Whenever "4":Non
4,   --"1": Adjust_Hor_Only	"2":Adjust_When_Moving "3":Adjust_Whenever "4":Non
135,		--Follow Speed
0,			--Intelligent Camera : 1 Yes, 0 No.
0,		--Sync X Z Rotate Speed
0			--Mouse Wheel Flip
)


--By ZhangChao for Family Mode
OpCameraPara(
2,	--Camera Mode
0.0382,	--X_RotateSpeed
0.0382,	--Z_RotateSpeed
25.0,	--Max_ElevationAngle
-20.0,	--Min_ElevationAngle
-20.0,	--Default_ElevationAngle
10.0,	--MouseWheelSpeed
200.0,	--Max_Distance
200.0,	--Min_Distance
200.0,	--DefaultDistance
2.0,	--Buffered Time
0.3,	--Buffered Percent
1.0,	--Stoppage Time Buffer
8.0,	--Collision Radius
45.0,	--Camera Turning Speed
10,	--Camera Collision Soft Speed
6,	--Camera Mode Exchange Speed
2.0,	--Camera Rotating Speed for Key Dragging
46,	--Camera Fov
5,	--Camera Distance Degree for adjusting distance by keyboard
30, --Camera Opacity Distance
4, 	--"1": Adjust_Hor_Only	"2":Adjust_When_Moving "3":Adjust_Whenever "4":Non
4, 	--"1": Adjust_Hor_Only	"2":Adjust_When_Moving "3":Adjust_Whenever "4":Non
135,		--Follow Speed
0,			--Intelligent Camera : 1 Yes, 0 No.
0,		--Sync X Z Rotate Speed
0			--Mouse Wheel Flip
)

--By ZhangChao for Zero Mode 
OpCameraPara(
3,	--Camera Mode
0.055,	--X_RotateSpeed
0.075,	--Z_RotateSpeed
70.0,	--Max_ElevationAngle
-70.0,	--Min_ElevationAngle
-15.0,	--Default_ElevationAngle
10.0,	--MouseWheelSpeed
180.0,	--Max_Distance
0.0,	--Min_Distance
155.0,	--DefaultDistance
2.0,	--Buffered Time
0.3,	--Buffered Percent
1.0,	--Stoppage Time Buffer
8.0,	--Collision Radius
45.0,	--Camera Turning Speed
10,	--Camera Collision Soft Speed
6,	--Camera Mode Exchange Speed
2.0,	--Camera Rotating Speed for Key Dragging
55,	--Camera Fov
5,	--Camera Distance Degree for adjusting distance by keyboard
25, --Camera Opacity Distance
4,   --"1": Adjust_Hor_Only	"2":Adjust_When_Moving "3":Adjust_Whenever "4":Non	(Mouse)
4,    --"1": Adjust_Hor_Only	"2":Adjust_When_Moving "3":Adjust_Whenever "4":Non (KeyBoard)
135,		--Follow Speed
0,			--Intelligent Camera : 1 Yes, 0 No.
0,		--Sync X Z Rotate Speed
0			--Mouse Wheel Flip
)

--By ZhangChao for Usual Mode 
OpCameraPara(
5,	--Camera Mode
0.055,	--X_RotateSpeed
0.075,	--Z_RotateSpeed
70.0,	--Max_ElevationAngle
-70.0,	--Min_ElevationAngle
-15.0,	--Default_ElevationAngle
10.0,	--MouseWheelSpeed
180.0,	--Max_Distance
0.0,	--Min_Distance
155.0,	--DefaultDistance
2.0,	--Buffered Time
0.3,	--Buffered Percent
1.0,	--Stoppage Time Buffer
8.0,	--Collision Radius
45.0,	--Camera Turning Speed
10,	--Camera Collision Soft Speed
6,	--Camera Mode Exchange Speed
2.0,	--Camera Rotating Speed for Key Dragging
55,	--Camera Fov
5,	--Camera Distance Degree for adjusting distance by keyboard
25, --Camera Opacity Distance
4,   --"1": Adjust_Hor_Only	"2":Adjust_When_Moving "3":Adjust_Whenever "4":Non	(Mouse)
4,    --"1": Adjust_Hor_Only	"2":Adjust_When_Moving "3":Adjust_Whenever "4":Non (KeyBoard)
135,		--Follow Speed
0,			--Intelligent Camera : 1 Yes, 0 No.
0,		--Sync X Z Rotate Speed
0			--Mouse Wheel Flip
)

--By ZhangChao for Classic Mode 
OpCameraPara(
4,	--Camera Mode
0.055,	--X_RotateSpeed
0.075,	--Z_RotateSpeed
70.0,	--Max_ElevationAngle
-70.0,	--Min_ElevationAngle
-15.0,	--Default_ElevationAngle
10.0,	--MouseWheelSpeed
180.0,	--Max_Distance
0.0,	--Min_Distance
155.0,	--DefaultDistance
2.0,	--Buffered Time
0.3,	--Buffered Percent
1.0,	--Stoppage Time Buffer
8.0,	--Collision Radius
45.0,	--Camera Turning Speed
10,	--Camera Collision Soft Speed
6,	--Camera Mode Exchange Speed
2.0,	--Camera Rotating Speed for Key Dragging
55,	--Camera Fov
5,	--Camera Distance Degree for adjusting distance by keyboard
25, --Camera Opacity Distance
4,   --"1": Adjust_Hor_Only	"2":Adjust_When_Moving "3":Adjust_Whenever "4":Non	(Mouse)
4,    --"1": Adjust_Hor_Only	"2":Adjust_When_Moving "3":Adjust_Whenever "4":Non (KeyBoard)
135,		--Follow Speed
0,			--Intelligent Camera : 1 Yes, 0 No.
0,		--Sync X Z Rotate Speed
0			--Mouse Wheel Flip
)
--------------------------------------------
OpAnchorPara(
0,			  --Anchor Mode
30.0,			--Default Height
10.0,			--Anchor Lowest Value
1.0,			--Height Factor
0,			  --Zero Angle (Deg)
0,			  --Horizantal Diff
30,				--Angle Shift Before Turnning(Deg)
0,			--Height when Dist Max(TPS only)
0 			--Height when Dist Min(TPS only)
)

--Normal Mode
OpAnchorPara(
1,			  --Anchor Mode
36.0,			--Default Height
31.0,			--Anchor Lowest Value 
2.0,			--Height Factor 
-3,			--Zero Angle (Deg) 
0,			  --Horizantal Diff
5,				--Angle Shift Before Turnning(Deg) 
0,			--Height when Dist Max(TPS only)
0 			--Height when Dist Min(TPS only)
)

--TPS MODE
OpAnchorPara(
2,			  --Anchor Mode
35.0,			--Default Height
43.0,			--Anchor Lowest Value
1,			--Height Factor
-3,			--Zero Angle (Deg)
0,			  --Horizantal Diff
5,				--Angle Shift Before Turnning(Deg)
46,			--Height when Dist Max(TPS only)
35 			--Height when Dist Min(TPS only)
)

OpAnchorPara(
3,			  --Anchor Mode
60.0,			--Default Height
60.0,			--Anchor Lowest Value
2.0,			--Height Factor
-30,			--Zero Angle (Deg)
15,			  --Horizantal Diff
5,				--Angle Shift Before Turnning(Deg)
0,			--Height when Dist Max(TPS only)
0 			--Height when Dist Min(TPS only)
)

--------------------------------------------
OpMousePara(
0,	-- Mouse Operation Mode
0.2,	-- Click time(seconds)
50,		-- Minimal click disctance
0.0,	-- Minimal distance for drag
0.5,		-- Click time for Key-board(seconds)
0,			-- Mouse Stick Ratio(only for FPS mode)
0,		-- Sniper Mouse Stick Ratio(only for FPS mode)
80,		-- Ground Fx distance
8			-- Ground Fx fade out speed
)

--Normal Mode
OpMousePara(
1,	-- Mouse Operation Mode
1,	-- Click time(seconds)
500,		-- Minimal click disctance
0.0,	-- Minimal distance for drag
0.5,		-- Click time for Key-board(seconds)
0.7,			-- Mouse Stick Ratio(only for FPS mode)
0.2,		-- Sniper Mouse Stick Ratio(only for FPS mode)
80,		-- Ground Fx distance
8			-- Ground Fx fade out speed
)

--By Issac
OpMousePara(
2,	-- Mouse Operation Mode
1,	-- Click time(seconds)
1000,		-- Minimal click disctance
0.0,	-- Minimal distance for drag
0.5,		-- Click time for Key-board(seconds)
0,			-- Mouse Stick Ratio(only for FPS mode)
0,		-- Sniper Mouse Stick Ratio(only for FPS mode)
80,		-- Ground Fx distance
8			-- Ground Fx fade out speed
)

-------------------------------------------------------------------------

--For Free Camera Mode
OpActionMapping(
		"3",						-- Input Mode
		
		"CAMERA_ROTATE_X_LEFT 	= MOUSE_RB_DRAG_X_LEFT",
		"CAMERA_ROTATE_X_RIGHT  = MOUSE_RB_DRAG_X_RIGHT",
		"CAMERA_ROTATE_Y_UP 	  = MOUSE_RB_DRAG_Y_UP",
		"CAMERA_ROTATE_Y_DOWN 	= MOUSE_RB_DRAG_Y_DOWN",

		"DRAWTEXT_ON_DROPITEM_TOGGLE = Control + B",
		"CameraZoom = MOUSE_WHEEL",
		
		----------------------------
		"FreeCameraMoveForward = W",
		"FreeCameraMoveBackward = S",
		"FreeCameraMoveLeft = A",
		"FreeCameraMoveRight = D",
		"FreeCameraMoveUp = Q",
		"FreeCameraMoveDown = E",
		"FreeCameraSpeedIns = Equals",
		"FreeCameraSpeedDes = Minus",
		"FreeCameraSpeedIns = Num-Lock_Add",
		"FreeCameraSpeedDes = Num-Lock_Subtract",		
		"FreeCameraRotateSpeedIns = Control + Equals",
		"FreeCameraRotateSpeedDes = Control + Minus",
		"FreeCameraBufferredRotate = Space",
		"FreeLoseTarget = Control + Q",
		"FreeRightCycle = D",
		"FreeLeftCycle = A",
		"FreeUpCycle = W",
		"FreeDownCycle =S",
		"FreeCameraLockedTargetRotateSpeedIns = Alt + Equals",
		"FreeCameraLockedTargetRotateSpeedDes = Alt + Minus",
		"FreeCameraCollisionToggleOnOff = Control + B",
		"HideAllUI = Alt + F10",
		
		--交互操作部分,这个模式不给交互按键显示会BUG
		"Interact_With_Target = MOUSE_RB_CLICK",
		"InteractTarget = F",
		"InteractTarget = JoystickAction_Interactive",
		
		--技能/道具操作部分--
		"OneKey_QINGGONG = NoKey",
		"LeftKey_Skill = 1",
		"RightKey_Skill = 2",
		"LeftKey_Skill = JoystickAction_LB",
		"RightKey_Skill = JoystickAction_RB",
		"LeftKey_Skill = MOUSE_LB_DOUBLE_CLICK",
		"RightKey_Skill = MOUSE_RB_DOUBLE_CLICK",
		
		"FRAME1_BUTTON_1 = 1",
		"FRAME1_BUTTON_2 = 2",
		"FRAME1_BUTTON_3 = 3",
		"FRAME1_BUTTON_4 = 4",
		"SHIFT_FUNCTION = G", 
		"SHIFT_FUNCTION = JoystickAction_Start",
		"SPECIAL_DAZUO = Comma",
		"FunctionSkill = Shift",--作为格挡用
		"FunctionSkill = JoystickAction_FuncSkill",
		
		"QTE_FUNCTION_Q = Q",
		"QTE_FUNCTION_E = E",
		"QTE_WAVE = Tab",
		"QTE_FUNCTION_Q = JoystickActionQTE_Q",
		"QTE_FUNCTION_E = JoystickActionQTE_E",
		"QTE_WAVE = JoystickAction_UltralSkill",
		
		
		"FRAME1_BUTTON_1 = JoystickAction_Skill_1",
		"FRAME1_BUTTON_2 = JoystickAction_Skill_2",
		"FRAME1_BUTTON_3 = JoystickAction_Skill_3",
		"FRAME1_BUTTON_4 = JoystickAction_Skill_4",
		
		"BREAK_SKILL = Escape",
		"BREAK_SKILL = JoystickAction_Back",
		
		"FRAME1_BUTTON_5 = JoystickAction_ShortCut_1",--道具部分
		"FRAME1_BUTTON_6 = JoystickAction_ShortCut_2",
		"FRAME1_BUTTON_7 = JoystickAction_ShortCut_3",
		"FRAME1_BUTTON_8 = JoystickAction_ShortCut_4",
		--"FRAME1_BUTTON_9 = JoystickAction_ShortCut_5",
		--"FRAME1_BUTTON_10 = JoystickAction_ShortCut_6",
		"FRAME1_BUTTON_5 = F1",
		"FRAME1_BUTTON_6 = F2",
		"FRAME1_BUTTON_7 = F3",
		"FRAME1_BUTTON_8 = F4",
		"MOUNT = T", 
		
		--UI操作部分--
		"PlayerAvaterUI = P",
		"ExpandWndUI = Control + P",
		"PackageUI = B",
		"ChatActiveEvent = Return",
		"ChatActiveEvent_Slash = Slash",
		
		"Fx_Logic_Show = Control + L",
		--系统功能--
		"SheathWeapon = Period",
		"FreeCameraMode = F9",--在这个模式下是退出的意思，不要注释
		"ActiveJoystick = F8",
		"ScreenShot = Snapshot",
		"HideOtherPlayers = Control + Z",
		"NORMAL_UICONTROL_HOLD = Alt",
		--"NORMAL_UICONTROL_HOLD = JoystickAction_Mouse",
		"WindowMinimize = Alt + Tilde",
		"ExitGame = Control + F4",
		"ExitGame = Alt + F4",
		"GENERIC_ATTACK = NoKey",--这个删了会放不出左右键
		"RBSKILL_ATTACK = NoKey",--这个删了会放不出左右键
		"COLLECT_DROPITEM = NoKey"
)

--For Family Mode Action Mapping 家园你懂的
-------------------------------------------------------------------------
OpActionMapping(
		"4",						-- Input Mode
		
		"MOVE_UP = W",
		"MOVE_DOWN = S",
		"MOVE_LEFT = A",
		"MOVE_RIGHT = D",

		"JUMP = Space",
		
		"CAMERA_ROTATE_X_LEFT  = MOUSE_RB_DRAG_X_LEFT",
		"CAMERA_ROTATE_X_RIGHT = MOUSE_RB_DRAG_X_RIGHT",
		"CAMERA_ROTATE_Y_UP    = MOUSE_RB_DRAG_Y_UP",
		"CAMERA_ROTATE_Y_DOWN  = MOUSE_RB_DRAG_Y_DOWN",		
						
		"BREAK_SKILL = Escape",
    
		--"ScriptDebugUI = Control + F12",
		"DataWatcher = Control + F9", 

		"CameraZoom = MOUSE_WHEEL",
		"CameraZoomIn = Home",
		"CameraZoomOut = End",
		
		"ChatActiveEvent = Return",
		"ChatActiveEvent_Slash = Slash",
		
		"HideAllUI = Alt + F10",
		"Fx_Logic_Show = Control + L",
		
		"ScreenShot = Snapshot",

		"ExitGame = Control + F4",
		"ExitGame = Alt + F4",
-----------------------------------------
--Furniture Move Actions
		"FURN_TRY_PLACE     = MOUSE_LB_CLICK",
		"FURN_MOVE_ANYWHERE = Control",
		"FURN_MOVE_FORWARD  = ArrowUp",
		"FURN_MOVE_BACKWARD = ArrowDown",
		"FURN_MOVE_LEFT     = ArrowLeft",
		"FURN_MOVE_RIGHT    = ArrowRight",
		"FURN_MOVE_UP       = Alt + ArrowUp",
		"FURN_MOVE_DOWN     = Alt + ArrowDown",
		"FURN_ROTATE_LEFT   = Alt + MOUSE_RB_DRAG_X_LEFT",
		"FURN_ROTATE_RIGHT  = Alt + MOUSE_RB_DRAG_X_RIGHT",
		"KEY_FURN_ROTATE_LEFT   = Alt + ArrowLeft",
		"KEY_FURN_ROTATE_RIGHT  = Alt + ArrowRight",
		"NULL_ACTION = Alt + MOUSE_RB_DRAG_Y_UP",
		"NULL_ACTION = Alt + MOUSE_RB_DRAG_Y_DOWN"
)

--For old  FPS Action Mapping
OpActionMapping(
		"2"						-- Input Mode
)
-------------------------------------------三大模式
-- For Zero Mode Action Mapping
-------------------------------------------------------------------------
OpActionMapping(
		"0",						-- Input Mode
		
		--人物操作部分--
		"MOVE = JoystickAction_Move",
		"MOVE_UP = W",
		"MOVE_DOWN = S",
		"MOVE_LEFT = A",
		"MOVE_RIGHT = D",
		"JUMP = Space",
		"JUMP = JoystickAction_Jump",
		"AutoRun = NumLock",
		"SheathWeapon = Period",
		"Polish = Y",

		--交互操作部分--
		"Interact_With_Target = MOUSE_RB_CLICK",
		"InteractTarget = F",
		"InteractTarget = JoystickAction_Interactive",
		
		--摄像机操作部分--
		"CAMERA_FACE_ROTATE_X_LEFT 		= MOUSE_RB_DRAG_X_LEFT",
		"CAMERA_FACE_ROTATE_X_RIGHT  	= MOUSE_RB_DRAG_X_RIGHT",
		"CAMERA_FACE_ROTATE_Y_UP     	= MOUSE_RB_DRAG_Y_UP",
		"CAMERA_FACE_ROTATE_Y_DOWN 		= MOUSE_RB_DRAG_Y_DOWN",
		
		"CAMERA_ROTATE_X_LEFT 	= MOUSE_LB_DRAG_X_LEFT",
		"CAMERA_ROTATE_X_RIGHT  = MOUSE_LB_DRAG_X_RIGHT",
		"CAMERA_ROTATE_Y_UP     = MOUSE_LB_DRAG_Y_UP",
		"CAMERA_ROTATE_Y_DOWN 	= MOUSE_LB_DRAG_Y_DOWN",
		
		"CameraZoom = MOUSE_WHEEL",
		"CameraZoom = JoystickAction_Fn",
		"CameraZoomIn = Home",
		"CameraZoomOut = End",
		
		"CameraRestoral = Backspace",--摄像机复位
		"CameraRestoral = JoystickAction_CameraBack",
		
		--技能/道具操作部分--
		"OneKey_QINGGONG = NoKey",
		"FISHING = NoKey",
		"LeftKey_Skill = MouseLButton",
		"RightKey_Skill = MouseRButton",
		"LeftKey_Skill = JoystickAction_LB",
		"RightKey_Skill = JoystickAction_RB",
		"LeftKey_Skill = MOUSE_LB_DOUBLE_CLICK",
		"RightKey_Skill = MOUSE_RB_DOUBLE_CLICK",
		
		"FRAME1_BUTTON_1 = 1",
		"FRAME1_BUTTON_2 = 2",
		"FRAME1_BUTTON_3 = 3",
		"FRAME1_BUTTON_4 = 4",
		"SHIFT_FUNCTION = G", 
		"SHIFT_FUNCTION = JoystickAction_Start",
		"SPECIAL_DAZUO = Comma",
		"FunctionSkill = Shift",--作为格挡用
		"FunctionSkill = JoystickAction_FuncSkill",
		
		"QTE_FUNCTION_Q = Q",
		"QTE_FUNCTION_E = E",
		"QTE_WAVE = Tab",
		"QTE_FUNCTION_F = NoKey",
		"QTE_FUNCTION_Q = JoystickActionQTE_Q",
		"QTE_FUNCTION_E = JoystickActionQTE_E",
		"QTE_WAVE = JoystickAction_UltralSkill",
		
		"FRAME1_BUTTON_1 = JoystickAction_Skill_1",
		"FRAME1_BUTTON_2 = JoystickAction_Skill_2",
		"FRAME1_BUTTON_3 = JoystickAction_Skill_3",
		"FRAME1_BUTTON_4 = JoystickAction_Skill_4",
		
		"BREAK_SKILL = Escape",
		"BREAK_SKILL = JoystickAction_Back",
		
		"FRAME1_BUTTON_5 = JoystickAction_ShortCut_1",--道具部分
		"FRAME1_BUTTON_6 = JoystickAction_ShortCut_2",
		"FRAME1_BUTTON_7 = JoystickAction_ShortCut_3",
		"FRAME1_BUTTON_8 = JoystickAction_ShortCut_4",
		--"FRAME1_BUTTON_9 = JoystickAction_ShortCut_5",
		--"FRAME1_BUTTON_10 = JoystickAction_ShortCut_6",
		"FRAME1_BUTTON_5 = F1",
		"FRAME1_BUTTON_6 = F2",
		"FRAME1_BUTTON_7 = F3",
		"FRAME1_BUTTON_8 = F4",
		"ROGUELIKEITEM = 6",
		"MOUNT = T", 

		--UI操作部分--
		"NewFriend = O",
		"FamilyUI = H",
		"KongfuUI = N",
		"JourneyUI = J",
		"CraftUI = I",
		"PlayerAvaterUI = P",
		"ExpandWndUI = Control + P",
		"PackageUI = B",
		"TaskUI = L",
		"WorldMapUI = M",
		"SkillUI = K",
		"WardrobeUI = U",
		"MallUI = F5",
		"SkillSettingUI = Control + K",
		"RightBottomChatbox = Control + C",
		
		"MiniMap_ZoomIn  = Num-Lock_Add",
		"MiniMap_ZoomOut  = Num-Lock_Subtract",
		
		"ChatActiveEvent = Return",
		"ChatActiveEvent_Slash = Slash",
		"RollAssign = R",
		
		"Fx_Logic_Show = Control + L",
		"UIEdit = Shift + F11",
		--系统功能--
		--"LockTarget = 8",
		"ChangeTarget = F7",
		"FreeCameraMode = F9",
		"ZeroMode1 = F10",
		"ZeroMode2 = F11",
		"ClassicMode = F12",
		"ActiveJoystick = F8",
		"ScreenShot = Snapshot",
		"HideOtherPlayers = Control + Z",

		"NORMAL_UICONTROL_HOLD = Alt",
		--"NORMAL_UICONTROL_HOLD = JoystickAction_Mouse",
		"HideAllUI = Alt + F10",
		"UIEdit = Alt + F9",
		"WhisperWindow = Control + Return",
		"WindowMinimize = Alt + Tilde",
		"ExitGame = Control + F4",
		"ExitGame = Alt + F4",
		"GENERIC_ATTACK = NoKey",--这个删了会放不出左右键
		"RBSKILL_ATTACK = NoKey",--这个删了会放不出左右键
		--上线需屏蔽部分
		
		--"ScriptDebugUI = Control + F12",
		"OpenGM_Panel = Control + G",
		--黑历史部分
		--"HEADUP_DISPLAY_TOGGLE = Control + V",--隐藏公告板,废了
		--"FPS_MODE_TOGGLE = JoystickAction_Start",--一键切换操作模式废了
		--"NORMAL_UICONTROL = Control", --ctrl只作为组合键
		"COLLECT_DROPITEM = NoKey"
)

--For Classic Mode Action Mapping
-------------------------------------------------------------------------
OpActionMapping(
		"1",						-- Input Mode

		--人物操作部分--
		"MOVE = JoystickAction_Move",
		"MOVE_UP = W",
		"MOVE_DOWN = S",
		"MOVE_LEFT = A",
		"MOVE_RIGHT = D",
		"JUMP = Space",
		"JUMP = JoystickAction_Jump",
		"AutoRun = NumLock",
		"SheathWeapon = Period",
		"Polish = Y",
		
		--古典模式专用
		"MOVE_UP = MouseLButton + MouseRButton",
		--交互操作部分--1
		"Interact_With_Target = MOUSE_RB_CLICK",
		"InteractTarget = F",
		"InteractTarget = JoystickAction_Interactive",
		
		--摄像机操作部分--
		"CAMERA_FACE_ROTATE_X_LEFT 		= MOUSE_RB_DRAG_X_LEFT",
		"CAMERA_FACE_ROTATE_X_RIGHT  	= MOUSE_RB_DRAG_X_RIGHT",
		"CAMERA_FACE_ROTATE_Y_UP     	= MOUSE_RB_DRAG_Y_UP",
		"CAMERA_FACE_ROTATE_Y_DOWN 		= MOUSE_RB_DRAG_Y_DOWN",
		
		"CAMERA_ROTATE_X_LEFT 	= MOUSE_LB_DRAG_X_LEFT",
		"CAMERA_ROTATE_X_RIGHT  = MOUSE_LB_DRAG_X_RIGHT",
		"CAMERA_ROTATE_Y_UP     = MOUSE_LB_DRAG_Y_UP",
		"CAMERA_ROTATE_Y_DOWN 	= MOUSE_LB_DRAG_Y_DOWN",
		
		"CameraZoom = MOUSE_WHEEL",
		"CameraZoom = JoystickAction_Fn",
		"CameraZoomIn = Home",
		"CameraZoomOut = End",
		
		"CameraRestoral = Backspace",--摄像机复位
		"CameraRestoral = JoystickAction_CameraBack",
		
		--技能/道具操作部分--
		"OneKey_QINGGONG = NoKey",
		"FISHING = NoKey",
		"LeftKey_Skill = 1",
		"RightKey_Skill = 2",
		"LeftKey_Skill = JoystickAction_LB",
		"RightKey_Skill = JoystickAction_RB",
		--"LeftKey_Skill = MOUSE_LB_DOUBLE_CLICK", --古典模式专用
		--"RightKey_Skill = MOUSE_RB_DOUBLE_CLICK",
		
		"FRAME1_BUTTON_1 = 3",
		"FRAME1_BUTTON_2 = 4",
		"FRAME1_BUTTON_3 = 5",
		"FRAME1_BUTTON_4 = 6",
		"SHIFT_FUNCTION = G", 
		"SHIFT_FUNCTION = JoystickAction_Start",
		"SPECIAL_DAZUO = Comma",
		"FunctionSkill = Shift",--作为格挡用
		"FunctionSkill = JoystickAction_FuncSkill",

		"QTE_FUNCTION_Q = Q",
		"QTE_FUNCTION_E = E",
		"QTE_WAVE = Tab",
		"QTE_FUNCTION_F = NoKey",
		"QTE_FUNCTION_Q = JoystickActionQTE_Q",
		"QTE_FUNCTION_E = JoystickActionQTE_E",
		"QTE_WAVE = JoystickAction_UltralSkill",

		"FRAME1_BUTTON_1 = JoystickAction_Skill_1",
		"FRAME1_BUTTON_2 = JoystickAction_Skill_2",
		"FRAME1_BUTTON_3 = JoystickAction_Skill_3",
		"FRAME1_BUTTON_4 = JoystickAction_Skill_4",

		"BREAK_SKILL = Escape",
		"BREAK_SKILL = JoystickAction_Back",

		"FRAME1_BUTTON_5 = JoystickAction_ShortCut_1",--道具部分
		"FRAME1_BUTTON_6 = JoystickAction_ShortCut_2",
		"FRAME1_BUTTON_7 = JoystickAction_ShortCut_3",
		"FRAME1_BUTTON_8 = JoystickAction_ShortCut_4",
		--"FRAME1_BUTTON_9 = JoystickAction_ShortCut_5",
		--"FRAME1_BUTTON_10 = JoystickAction_ShortCut_6",
		"FRAME1_BUTTON_5 = F1",
		"FRAME1_BUTTON_6 = F2",
		"FRAME1_BUTTON_7 = F3",
		"FRAME1_BUTTON_8 = F4",
		"ROGUELIKEITEM = 7",
		"MOUNT = T", 

		--UI操作部分--
		"NewFriend = O",
		"FamilyUI = H",
		"KongfuUI = N",
		"JourneyUI = J",
		"CraftUI = I",
		"PlayerAvaterUI = P",
		"ExpandWndUI = Control + P",
		"PackageUI = B",
		"TaskUI = L",
		"WorldMapUI = M",
		"SkillUI = K",
		"WardrobeUI = U",
		"MallUI = F5",
		"SkillSettingUI = Control + K",
		"RightBottomChatbox = Control + C",
		
		"MiniMap_ZoomIn  = Num-Lock_Add",
		"MiniMap_ZoomOut  = Num-Lock_Subtract",
		
		"ChatActiveEvent = Return",
		"ChatActiveEvent_Slash = Slash",
		"RollAssign = R",

		"Fx_Logic_Show = Control + L",
		--系统功能--
		--"LockTarget = 8",
		"FreeCameraMode = F9",
		"ZeroMode1 = F10",
		"ZeroMode2 = F11",
		"ClassicMode = F12",
		"ActiveJoystick = F8",
		"ScreenShot = Snapshot",
		"HideOtherPlayers = Control + Z",

		"NORMAL_UICONTROL_HOLD = Alt",
		--"NORMAL_UICONTROL_HOLD = JoystickAction_Mouse",
		"HideAllUI = Alt + F10",
		"UIEdit = Alt + F9",
		"WhisperWindow = Control + Return",
		"WindowMinimize = Alt + Tilde",
		"ExitGame = Control + F4",
		"ExitGame = Alt + F4",
		"GENERIC_ATTACK = NoKey",--这个删了会放不出左右键
		"RBSKILL_ATTACK = NoKey",--这个删了会放不出左右键
		--上线需屏蔽部分

		--"ScriptDebugUI = Control + F12",
		"OpenGM_Panel = Control + G",
		--黑历史部分
		--"HEADUP_DISPLAY_TOGGLE = Control + V",--隐藏公告板,废了
		--"FPS_MODE_TOGGLE = JoystickAction_Start",--一键切换操作模式废了
		--"NORMAL_UICONTROL = Control", --ctrl只作为组合键
		"COLLECT_DROPITEM = NoKey"
)

--By ZhangChao for Usual Mode Action Mapping
-------------------------------------------------------------------------
OpActionMapping(
		"5",						-- Input Mode

		
		--人物操作部分--
		"MOVE = JoystickAction_Move",
		"MOVE_UP = W",
		"MOVE_DOWN = S",
		"MOVE_LEFT = A",
		"MOVE_RIGHT = D",
		"JUMP = Space",
		"JUMP = JoystickAction_Jump",
		"AutoRun = NumLock",
		"SheathWeapon = Period",
		"Polish = Y",
		--交互操作部分--
		"Interact_With_Target = MOUSE_RB_CLICK",
		"InteractTarget = F",
		"InteractTarget = JoystickAction_Interactive",
		
		--摄像机操作部分--
		"CAMERA_FACE_ROTATE_X_LEFT 		= MOUSE_RB_DRAG_X_LEFT",
		"CAMERA_FACE_ROTATE_X_RIGHT  	= MOUSE_RB_DRAG_X_RIGHT",
		"CAMERA_FACE_ROTATE_Y_UP     	= MOUSE_RB_DRAG_Y_UP",
		"CAMERA_FACE_ROTATE_Y_DOWN 		= MOUSE_RB_DRAG_Y_DOWN",
		
		"CAMERA_ROTATE_X_LEFT 	= MOUSE_LB_DRAG_X_LEFT",
		"CAMERA_ROTATE_X_RIGHT  = MOUSE_LB_DRAG_X_RIGHT",
		"CAMERA_ROTATE_Y_UP     = MOUSE_LB_DRAG_Y_UP",
		"CAMERA_ROTATE_Y_DOWN 	= MOUSE_LB_DRAG_Y_DOWN",
		
		"CameraZoom = MOUSE_WHEEL",
		"CameraZoom = JoystickAction_Fn",
		"CameraZoomIn = Home",
		"CameraZoomOut = End",
		
		"CameraRestoral = Backspace",--摄像机复位
		"CameraRestoral = JoystickAction_CameraBack",
		
		--技能/道具操作部分--
		"OneKey_QINGGONG = NoKey",
		"FISHING = NoKey",
		"LeftKey_Skill = MouseLButton",
		"RightKey_Skill = MouseRButton",
		"LeftKey_Skill = JoystickAction_LB",
		"RightKey_Skill = JoystickAction_RB",
		"LeftKey_Skill = MOUSE_LB_DOUBLE_CLICK",
		"RightKey_Skill = MOUSE_RB_DOUBLE_CLICK",
		
		"FRAME1_BUTTON_1 = 1",
		"FRAME1_BUTTON_2 = 2",
		"FRAME1_BUTTON_3 = 3",
		"FRAME1_BUTTON_4 = 4",
		"SHIFT_FUNCTION = G", 
		"SHIFT_FUNCTION = JoystickAction_Start",
		"SPECIAL_DAZUO = Comma",
		"FunctionSkill = Shift",--作为格挡用
		"FunctionSkill = JoystickAction_FuncSkill",
		
		"QTE_FUNCTION_Q = Q",
		"QTE_FUNCTION_E = E",
		"QTE_WAVE = Tab",
		"QTE_FUNCTION_F = NoKey",
		"QTE_FUNCTION_Q = JoystickActionQTE_Q",
		"QTE_FUNCTION_E = JoystickActionQTE_E",
		"QTE_WAVE = JoystickAction_UltralSkill",
		
		"FRAME1_BUTTON_1 = JoystickAction_Skill_1",
		"FRAME1_BUTTON_2 = JoystickAction_Skill_2",
		"FRAME1_BUTTON_3 = JoystickAction_Skill_3",
		"FRAME1_BUTTON_4 = JoystickAction_Skill_4",
		
		"BREAK_SKILL = Escape",
		"BREAK_SKILL = JoystickAction_Back",
		
		"FRAME1_BUTTON_5 = JoystickAction_ShortCut_1",--道具部分
		"FRAME1_BUTTON_6 = JoystickAction_ShortCut_2",
		"FRAME1_BUTTON_7 = JoystickAction_ShortCut_3",
		"FRAME1_BUTTON_8 = JoystickAction_ShortCut_4",
		--"FRAME1_BUTTON_9 = JoystickAction_ShortCut_5",
		--"FRAME1_BUTTON_10 = JoystickAction_ShortCut_6",
		"FRAME1_BUTTON_5 = F1",
		"FRAME1_BUTTON_6 = F2",
		"FRAME1_BUTTON_7 = F3",
		"FRAME1_BUTTON_8 = F4",
		"ROGUELIKEITEM = 6",
		"MOUNT = T", 

		--UI操作部分--
		"NewFriend = O",
		"FamilyUI = H",
		"KongfuUI = N",
		"JourneyUI = J",
		"CraftUI = I",
		"PlayerAvaterUI = P",
		"ExpandWndUI = Control + P",
		"PackageUI = B",
		"TaskUI = L",
		"WorldMapUI = M",
		"SkillUI = K",
		"WardrobeUI = U",
		"MallUI = F5",
		"SkillSettingUI = Control + K",
		"RightBottomChatbox = Control + C",
		
		"MiniMap_ZoomIn  = Num-Lock_Add",
		"MiniMap_ZoomOut  = Num-Lock_Subtract",

		"ChatActiveEvent = Return",
		"ChatActiveEvent_Slash = Slash",
		"RollAssign = R",
		"Fx_Logic_Show = Control + L",

		--系统功能--
		--"LockTarget = 8",
		"FreeCameraMode = F9",
		"ZeroMode1 = F10",
		"ZeroMode2 = F11",
		"ClassicMode = F12",
		"ActiveJoystick = F8",
		"ScreenShot = Snapshot",
		"HideOtherPlayers = Control + Z",

		"NORMAL_UICONTROL_HOLD = Alt",
		--"NORMAL_UICONTROL_HOLD = JoystickAction_Mouse",
		"HideAllUI = Alt + F10",
		"UIEdit = Alt + F9",
		"WhisperWindow = Control + Return",
		"WindowMinimize = Alt + Tilde",
		"ExitGame = Control + F4",
		"ExitGame = Alt + F4",
		"GENERIC_ATTACK = NoKey",--这个删了会放不出左右键
		"RBSKILL_ATTACK = NoKey",--这个删了会放不出左右键
		--上线需屏蔽部分
		--"ScriptDebugUI = Control + F12",
		"OpenGM_Panel = Control + G",
		--黑历史部分
		--"HEADUP_DISPLAY_TOGGLE = Control + V",--隐藏公告板,废了
		--"FPS_MODE_TOGGLE = JoystickAction_Start",--一键切换操作模式废了
		--"NORMAL_UICONTROL = Control", --ctrl只作为组合键
		"COLLECT_DROPITEM = NoKey"
)

--For DOTA BattleFiled Mode Action Mapping
-------------------------------------------------------------------------
OpActionMapping(
		"6",						-- Input Mode
		--PVP特殊按键
		"DOTA_ITEM_1 = F1",
		"DOTA_ITEM_2 = F2",
		"DOTA_ITEM_3 = F3",
		"DOTA_ITEM_4 = F4",

		"BattleScore = F6",
		"DOTA_BATTLEFIELD_SHOP =F7",
		"DOTA_SKILL_1 = Z",
		"DOTA_SKILL_2 = X",
		"DOTA_SKILL_3 = C",
		"DOTA_SKILL_4 = V",

		"DOTA_ADD_SKILL_1 = Alt + Z",
		"DOTA_ADD_SKILL_2 = Alt + X",
		"DOTA_ADD_SKILL_3 = Alt + C",
		"DOTA_ADD_SKILL_4 = Alt + V"
)