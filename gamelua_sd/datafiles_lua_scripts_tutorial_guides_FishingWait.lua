
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	self.bFishingStart = false
	self.fishingStartConn = nil
	self.fishingDoneConn  = nil
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	if gameEventMgr then
		--self.fishingStartConn = gameEventMgr:subscribeEvent('Tutorial_FishingStart',    self.OnFishingStart, self)
		--self.fishingDoneConn  = gameEventMgr:subscribeEvent('Tutorial_FishingDropItem', self.OnFishingDone,  self)
	end
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	--if not self.bFishingStart then
	--	self.displayTime = 0  -- 确保进入钓鱼状态后才开始计时
	--end
	
end

function OnFishingStart(self, evtArgs)
	self.bFishingStart = true
end

function OnFishingDone(self, evtArgs)
	self.completed = true
end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

