
-- Game Event Names COPIED from LuaEventManager.h/LuaGameEvents

GameEvents = 
{
	-- Debugging
	ShowClientTable     = 'GameEvent/ShowClientTable',
	ConsoleCommand      = 'ClientCmd',
	VideoBegin          = 'GameEvent/VideoBegin',
	VideoEnd            = 'GameEvent/VideoEnd',
	
	-- Misc
	EscapeJamWaitEnded  = 'GameEvent/EscapeJamWaitEnded',
	NewEmailNotify      = 'GameEvent/NewMailNotify',
	ChannelInfoUpdate   = 'GameEvent/ChannelInfoUpdate',
	FeatureSync         = 'GameEvent/SyncOpenFeature',
	FeatureOpened       = 'GameEvent/NotifyOpenFeature',
	
	-- Task
	TaskAccepted        = 'EventTaskAccepted',
	TaskSubmitted       = 'EventTaskSubmitted',
	TaskFinished        = 'EventTaskFinished',
	TaskCancelled       = 'EventTaskCancelled',
	TaskTraceChanged    = 'EventTaskObjCountChanged',
	
	-- Operation Mode
	OperationModeChange = 'ChangeOpMode',
	
	-- Enity
	EntityCreated       = 'EventEntityCreation',
	EntityDeleted       = 'EventEntityDeletion',
	EntityDead          = 'EventEntityDead',
}