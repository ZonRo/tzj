-- TutorialVideoFrame

local layoutName = 'Tutorial_Video.layout'

ui.defineFrame(..., layoutName)
function Init(self)
	self.pWindow = self:GetWindow()
	self.pVideo  = self:GetChildWindow('Tutorial_Video/Video')
	self.pSkillKeyImg = self:GetChildWindow('Tutorial_Video/Skill/Key')
	self.pSkillIcon  = self:GetChildWindow('Tutorial_Video/Skill/IconBk/SkillIcon')
	
	self.VideoTypes = { 'Attack', 'Dodge', 'Skill', 'DodgeCircle','Guard','EmergencyEvade','FinalAttack','QinggongAttack','Stardust','LearnNewSkill'}
	self.VideoCfg   = nil
	self.SkillCfg   = nil
	self.SkillKeyCfg = nil
	
	self.bVideoPlaying = false
	self.bDelayCloseTriggered = false
	self.fDelayTime = 0.0
	
	self:InitVideoCfg()
	self:InitSkillCfg()
	self:InitSkillKeyWindows()
end

function UnInit(self)
	lout('TutorialVideoFrame:UnInit()')
	self:CloseVideo()
	self:Hide()
end

function Subscribe(self)
	self:SubscribeUIEvents()
end

function SubscribeUIEvents(self)
	lout('TutorialVideoFrame:SubscribeUIEvents()')
	local pClose = self:GetChildWindow('Tutorial_Video/CloseBtn')
	pClose:subscribeEvent('Clicked', self.OnClose, self)
	
	self.pWindow:subscribeEvent('WindowUpdate', self._OnUpdate, self)
end

function _OnUpdate(self, evtArgs)
	local e = tolua.cast(evtArgs, 'CEGUI::UpdateEventArgs')
	if self.bDelayCloseTriggered then
		self.fDelayTime = self.fDelayTime + e.lastFrameTime
		if self.fDelayTime >= 2.0 then
			self:CloseVideo()
			self:Hide()
		end
	end
end

function ShowVideo(self, videoType)
	local pWin = self.pInfoWins[videoType]
	if not pWin then
		lout('TutorialVideoFrame:ShowVideo Invalid Video Type: ' .. tostring(videoType))
	end
	for k,v in pairs(self.pInfoWins) do 
		if pWin == v then v:show() else v:hide() end
	end
	
	-- Play Video
	self:PlayVideo(videoType)
	
	-- Show Skill Icons
	self:ShowSkillKeyAndIcons(videoType)
	
	-- animation: Video_Position
	SD.WndAnimManager:Instance():CreateAnim(self.pWindow, CEGUI.String('Video_Position'), true, true, true)
	self:Show()
end


function HideVideo(self)
	self.bDelayCloseTriggered = true
	self.fDelayTime = 0.0
end

function PlayVideo(self, videoType)
	if self.bVideoPlaying then
		self:CloseVideo()
	end
	
	local videoName = self:GetVideoName(videoType)
	SD.PlayVideoOnWnd(self.pVideo:getName():c_str(), videoName, true)
	self.bVideoPlaying = true
end

function CloseVideo(self)
	if self.bVideoPlaying then
		SD.DeleteVideoOnWnd(self.pVideo:getName():c_str())
	end
	self.bVideoPlaying = false
	
	self.bDelayCloseTriggered = false
	self.fDelayTime = 0.0
end

function GetVideoName(self, videoType)
	local videoCfg = self.VideoCfg[videoType]
	local unit = GetPlayersUnit()
	if not videoCfg then
		lout('TutorialVideoFrame:GetVideoName - VideoCfg invalid for type '..tostring(videoType))
		return  '../video/intro.wmv'
	end
	if not unit or not unit.GetProfession then
		lout('TutorialVideoFrame:GetVideoName - GetProfession failed')
		return  '../video/intro.wmv'
	end
	
	local eProf = unit:GetProfession()
	if eProf == WM_PROF_XIANGLONG then
		return videoCfg.GaiBang
	elseif eProf == WM_PROF_YAOGUANG then
		return videoCfg.QuanZhen
	elseif eProf == WM_PROF_BAIZHANG then
		return videoCfg.ShaoLin
	else
		return videoCfg.Taohua
	end
end

function ShowSkillKeyAndIcons(self, videoType) 
	local unit = GetPlayersUnit()
	if not unit then return end
	
	local eProf = unit:GetProfession()
	local opMode = SD.GetOperationMode()
	
	local skillCfg = self.SkillCfg[videoType]
	if skillCfg then
		-- Image
		local imageName = nil
		
		if eProf == WM_PROF_XIANGLONG then
			imageName = skillCfg.GaiBang
		elseif eProf == WM_PROF_YAOGUANG then
			imageName = skillCfg.QuanZhen
		elseif eProf == WM_PROF_BAIZHANG then
			imageName = skillCfg.ShaoLin
		else 
			imageName = skillCfg.Taohua
		end
		
		self.pSkillIcon:setProperty('Image', imageName)
	end
	
	local keyImageName = nil
	local skillkeyCfg = self.SkillKeyCfg[videoType]
	if skillkeyCfg then
		if skillkeyCfg.bDiffWithOpMode and not skillkeyCfg.bDiffWithProf then
			keyImageName = skillkeyCfg[opMode]
		end
		
		if not skillkeyCfg.bDiffWithOpMode and skillkeyCfg.bDiffWithProf then
			if eProf == WM_PROF_XIANGLONG then
				keyImageName = skillkeyCfg.GaiBang
			elseif eProf == WM_PROF_YAOGUANG then
				keyImageName = skillkeyCfg.QuanZhen
			elseif eProf == WM_PROF_BAIZHANG then
				keyImageName = skillkeyCfg.ShaoLin
			else 
				keyImageName = skillkeyCfg.Taohua
			end
		end
		
		if skillkeyCfg.bDiffWithOpMode and skillkeyCfg.bDiffWithProf then
			if eProf == WM_PROF_XIANGLONG then
				keyImageName = skillkeyCfg.GaiBang[opMode]
			elseif eProf == WM_PROF_YAOGUANG then
				keyImageName = skillkeyCfg.QuanZhen[opMode]
			elseif eProf == WM_PROF_BAIZHANG then
				keyImageName = skillkeyCfg.ShaoLin[opMode]
			else 
				keyImageName = skillkeyCfg.Taohua[opMode]
			end
		end
		-- Text
		self.pSkillKeyImg:setProperty('Image', keyImageName)  
	end
end

function InitVideoCfg(self)

	self.VideoCfg = {
		Attack = {
			GaiBang  =  '../video/Attack_GaiBang.wmv',
			ShaoLin  =  '../video/Attack_ShaoLin.wmv',
			QuanZhen =  '../video/Attack_QuanZhen.wmv',
			Taohua =  '../video/Attack_TaoHua.wmv',
		},
		Dodge = {
			GaiBang  =  '../video/Dodge_GaiBang.wmv',
			ShaoLin  =  '../video/Dodge_ShaoLin.wmv',
			QuanZhen =  '../video/Dodge_QuanZhen.wmv',
			Taohua =  '../video/Dodge_TaoHua.wmv',
		},
		Skill = {
			GaiBang  =  '../video/Skill_GaiBang.wmv',
			ShaoLin  =  '../video/Skill_ShaoLin.wmv',
			QuanZhen =  '../video/Skill_QuanZhen.wmv',
			Taohua =  '../video/Skill_TaoHua.wmv',
		},
		DodgeCircle = {
			GaiBang  =  '../video/DodgeCircle_GaiBang.wmv',
			ShaoLin  =  '../video/DodgeCircle_ShaoLin.wmv',
			QuanZhen =  '../video/DodgeCircle_QuanZhen.wmv',
			Taohua =  '../video/DodgeCircle_TaoHua.wmv',
		},
		Guard = {
			GaiBang  =  '../video/Guard_GaiBang.wmv',
			ShaoLin  =  '../video/Guard_ShaoLin.wmv',
			QuanZhen =  '../video/Guard_QuanZhen.wmv',
			Taohua =  '../video/Guard_TaoHua.wmv',
		},
		EmergencyEvade = {
			GaiBang  =  '../video/EmergencyEvade_GaiBang.wmv',
			ShaoLin  =  '../video/EmergencyEvade_ShaoLin.wmv',
			QuanZhen =  '../video/EmergencyEvade_QuanZhen.wmv',
			Taohua =  '../video/EmergencyEvade_TaoHua.wmv',
		},
		FinalAttack = {
			GaiBang  =  '../video/FinalAttack_GaiBang.wmv',
			ShaoLin  =  '../video/FinalAttack_ShaoLin.wmv',
			QuanZhen =  '../video/FinalAttack_QuanZhen.wmv',
			Taohua =  '../video/FinalAttack_TaoHua.wmv',
		},
		QinggongAttack = {
			GaiBang  =  '../video/QinggongAttack_GaiBang.wmv',
			ShaoLin  =  '../video/QinggongAttack_ShaoLin.wmv',
			QuanZhen =  '../video/QinggongAttack_QuanZhen.wmv',
			Taohua =  '../video/QinggongAttack_TaoHua.wmv',
		},
		Stardust = {
			GaiBang  =  '../video/Stardust_GaiBang.wmv',
			ShaoLin  =  '../video/Stardust_ShaoLin.wmv',
			QuanZhen =  '../video/Stardust_QuanZhen.wmv',
			Taohua =  '../video/Stardust_TaoHua.wmv',
		},
		LearnNewSkill = {
			GaiBang  =  '../video/QE_GaiBang.wmv',
			ShaoLin  =  '../video/QE_ShaoLin.wmv',
			QuanZhen =  '../video/QE_QuanZhen.wmv',
			Taohua =  '../video/QE_TaoHua.wmv',
		},
		JA = {
			GaiBang  =  '../video/JA_GaiBang.wmv',
			ShaoLin  =  '../video/JA_ShaoLin.wmv',
			QuanZhen =  '../video/JA_QuanZhen.wmv',
			Taohua =  '../video/JA_TaoHua.wmv',
		},
	}
end

function InitSkillCfg(self)
	self.SkillCfg = {
		Skill = {
			GaiBang  =  'set:skill_swgc1 image:3',
			ShaoLin  =  'set:skill_sl_1 image:31',
			QuanZhen =  'set:skill_qz_1 image:33',
			Taohua   =  'set:skill_talent2 image:2',
		},
	}
	
	self.SkillKeyCfg = {
		Skill = {
			bDiffWithOpMode = true,
			bDiffWithProf   = false,
			[SD.HS_OPERATION_CLASSIC] = 'set:Total_Tutorial image:3',
			[SD.HS_OPERATION_USUAL] = 'set:Total_Tutorial image:1',
			[SD.HS_OPERATION_ZERO] = 'set:Total_Tutorial image:1',
		},
		-- Skill = {
			-- bDiffWithOpMode = false,
			-- bDiffWithProf   = true,
			-- GaiBang = 'set:Total_Tutorial image:3_2',
			-- ShaoLin = 'set:Total_Tutorial image:1_2',
			-- QuanZhen = 'set:Total_Tutorial image:1_2',
		-- },
		-- Skill = {
			-- bDiffWithOpMode = true,
			-- bDiffWithProf   = true,
			-- GaiBang = {
				-- [SD.HS_OPERATION_CLASSIC] = 'set:Total_Tutorial image:3_2',
				-- [SD.HS_OPERATION_USUAL] = 'set:Total_Tutorial image:1_2',
				-- [SD.HS_OPERATION_ZERO] = 'set:Total_Tutorial image:1_2',
			-- },
		-- },
	}
end

function InitSkillKeyWindows(self)
	local pAttack = self:GetChildWindow('Tutorial_Video/Attack')
	local pDodge  = self:GetChildWindow('Tutorial_Video/Dodge')
	local pSkill  = self:GetChildWindow('Tutorial_Video/Skill')
	local pDodge2 = self:GetChildWindow('Tutorial_Video/Dodge2')
	local pGuard = self:GetChildWindow('Tutorial_Video/Guard')
	local pEmergencyEvade  = self:GetChildWindow('Tutorial_Video/EmergencyEvade')
	local pFinalAttack  = self:GetChildWindow('Tutorial_Video/FinalAttack')
	local pQinggongAttack = self:GetChildWindow('Tutorial_Video/QinggongAttack')
	local pStardust = self:GetChildWindow('Tutorial_Video/Stardust')
	local pLearnNewSkill = self:GetChildWindow('Tutorial_Video/LearnNewSkill')
	local pJA = self:GetChildWindow('Tutorial_Video/JA')
	self.pInfoWins   = {
		Attack = pAttack,   
		Dodge  = pDodge,
		Skill  = pSkill,
		DodgeCircle = pDodge2,
		Guard = pGuard,
		EmergencyEvade = pEmergencyEvade,
		FinalAttack = pFinalAttack,
		QinggongAttack = pQinggongAttack,
		Stardust = pStardust,
		LearnNewSkill = pLearnNewSkill,
		JA = pJA,
	}
end

function OnClose(self, evtArgs)
	lout('TutorialVideoFrame:OnClose()')
	self:Hide()
end
