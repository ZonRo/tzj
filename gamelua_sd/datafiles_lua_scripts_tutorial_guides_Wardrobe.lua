
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame
	
	local openBag = indicatorFlow.createIndicatorSpec()
	openBag.frameName     = 'openBag'
	openBag.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	openBag.frameText     = '获得了一件新时装哦！但现在还没穿上，请先打开背包'
	openBag.attachFunc    = self.OnOpenBagAttached
	openBag.attachFuncParam = self
	openBag.triggerFunc   = nil
	openBag.triggerFuncParam = nil
	openBag.triggerWin    = 'Root/MainMenu/PackageBtn'
	openBag.attachWin     = 'Root/MainMenu/PackageBtn'
	openBag.attachWinRoot = 'MainMenu_FunctionAndEXP'
	openBag.triggerKey    = 'PackageUI'
	openBag.priority      = 1

	local openBagEquipPage = indicatorFlow.createIndicatorSpec()
	openBagEquipPage.frameName     = 'openBagEquipPage'
	openBagEquipPage.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	openBagEquipPage.frameText     = '在装备包裹里哦'
	openBagEquipPage.attachFunc    = self.OnOpenBagPageEquipAttached
	openBagEquipPage.attachFuncParam = self
	openBagEquipPage.triggerFunc   = nil
	openBagEquipPage.triggerFuncParam = nil
	openBagEquipPage.triggerWin    = 'Root/PackageFrame/TagContainer1/Btn2_1_0'
	openBagEquipPage.attachWin     = 'Root/PackageFrame/TagContainer1/Btn2_1_0'
	openBagEquipPage.attachWinRoot = 'Root/PackageFrame'
	openBagEquipPage.priority      = 2
	
	local placeBagItemToPos = indicatorFlow.createIndicatorSpec()
	placeBagItemToPos.frameName     = 'placeBagItemToPos'
	placeBagItemToPos.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	placeBagItemToPos.frameText     = '右键点击，可将其放入衣柜（时装只有先放入衣柜才能穿哦） '
	placeBagItemToPos.attachFunc    = nil
	placeBagItemToPos.attachFuncParam = nil
	placeBagItemToPos.triggerFunc   = nil
	placeBagItemToPos.triggerFuncParam = nil
	placeBagItemToPos.attachType    = indicatorFlow.INDICATOR_ATTACH_BAGITEM
	placeBagItemToPos.attachItemID  = '9_13008'
	placeBagItemToPos.priority      = 3

	local openWardrobe = indicatorFlow.createIndicatorSpec()
	openWardrobe.frameName     = 'openWardrobe'
	openWardrobe.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	openWardrobe.frameText     = '好啦，现在点击这里进入衣柜'
	openWardrobe.attachFunc    = self.OnOpenWardrobeAttached
	openWardrobe.attachFuncParam = self
	openWardrobe.triggerFunc   = nil
	openWardrobe.triggerFuncParam = nil
	openWardrobe.triggerWin    = 'MainMenu_FunctionAndEXP/ShopBtnFrame/Wardrobe'
	openWardrobe.attachWin     = 'MainMenu_FunctionAndEXP/ShopBtnFrame/Wardrobe'
	openWardrobe.attachWinRoot = 'MainMenu_FunctionAndEXP'
	openWardrobe.triggerKey    = 'WardrobeUI'
	openWardrobe.priority      = 4
	
	local openWardrobeThirdPage = indicatorFlow.createIndicatorSpec()
	openWardrobeThirdPage.frameName     = 'openWardrobeThirdPage'
	openWardrobeThirdPage.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	openWardrobeThirdPage.frameText     = '新获得的时装属于披挂分类，请点击切换分类'
	openWardrobeThirdPage.attachFunc    = self.OnOpenWardrobeThirdPageAttached
	openWardrobeThirdPage.attachFuncParam = self
	openWardrobeThirdPage.triggerFunc   = nil
	openWardrobeThirdPage.triggerFuncParam = nil
	openWardrobeThirdPage.triggerWin    = 'Wardrobe/Frame2/BtnFrame2/RadioBtn3'
	openWardrobeThirdPage.attachWin     = 'Wardrobe/Frame2/BtnFrame2/RadioBtn3'
	openWardrobeThirdPage.attachWinRoot = 'Wardrobe'
	openWardrobeThirdPage.priority      = 5

	local openWardrobeBackPage = indicatorFlow.createIndicatorSpec()
	openWardrobeBackPage.frameName     = 'openWardrobeBackPage'
	openWardrobeBackPage.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	openWardrobeBackPage.frameText     = '点击背饰分类'
	openWardrobeBackPage.attachFunc    = self.OnOpenWardrobeBackPageAttached
	openWardrobeBackPage.attachFuncParam = self
	openWardrobeBackPage.triggerFunc   = nil
	openWardrobeBackPage.triggerFuncParam = nil
	openWardrobeBackPage.triggerWin    = 'Wardrobe/Frame2/GridFrame/BtnFrame/RadioBtn1'
	openWardrobeBackPage.attachWin     = 'Wardrobe/Frame2/GridFrame/BtnFrame/RadioBtn1'
	openWardrobeBackPage.attachWinRoot = 'Wardrobe'
	openWardrobeBackPage.priority      = 6

	local placeWardrobeItemToPos = indicatorFlow.createIndicatorSpec()
	placeWardrobeItemToPos.frameName     = 'placeWardrobeItemToPos'
	placeWardrobeItemToPos.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	placeWardrobeItemToPos.frameText     = '点击穿上新时装！'
	placeWardrobeItemToPos.attachFunc    = nil
	placeWardrobeItemToPos.attachFuncParam = nil
	placeWardrobeItemToPos.triggerFunc   = nil
	placeWardrobeItemToPos.triggerFuncParam = nil
	placeWardrobeItemToPos.attachWinRoot = 'Wardrobe'
	placeWardrobeItemToPos.priority      = 7
	
	local saveLook = indicatorFlow.createIndicatorSpec()
	saveLook.frameName     = 'saveLook'
	saveLook.frameType     = indicatorFlow.INDICATOR_FRAME_LEFT
	saveLook.frameText     = '最后一定别忘了保存形象哟~^ ^'
	saveLook.attachFunc    = self.OnSaveLookAttached
	saveLook.attachFuncParam = self
	saveLook.triggerFunc   = nil
	saveLook.triggerFuncParam = nil
	saveLook.triggerWin    = 'Wardrobe/Frame1/Confirm'
	saveLook.attachWin     = 'Wardrobe/Frame1/Confirm'
	saveLook.attachWinRoot = 'Wardrobe'
	saveLook.priority      = 8
	
	self.indicatorSpecs = {
		openBag, openBagEquipPage, placeBagItemToPos, openWardrobe, openWardrobeThirdPage, openWardrobeBackPage, saveLook
	}
	
	self.placeBagItemToPosSpec = placeBagItemToPos
	self.placeWardrobeItemToPosSpec = placeWardrobeItemToPos
	self.indicatorFlows = nil
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
	self.indicatorFlows = nil
end

function LocateWardrobeItemWindow(self)
	local dwModelID = nil
	local sex = Player.GetSex()
	if sex == WM_SEX_MALE then
		dwModelID = 12015
	elseif sex == WM_SEX_FEMALE then
		dwModelID = 12016
	end
	if not dwModelID then
		lout('Wardrobe: cannot find Model ID.')
		return nil
	end
	
	local pWin = SD.GetDragButtonForModelIDFromWardrobe(dwModelID)
	if not pWin then
		lout('Wardrobe: cannot find DragButton for Model ID ' .. dwModelID)
		return nil
	end
	
	return pWin:getName():c_str()
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end


function OnOpenBagAttached(self, indicator)
	local pWin = InitWindowPtr('Root/PackageFrame')
	if pWin and pWin:isVisible() then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function OnOpenBagPageEquipAttached(self, indicator)
	local pBagItemWin = SD.GetFirstBagItemButtonContainer(self.placeBagItemToPosSpec.attachItemID)
	if not pBagItemWin then 
		lout('not find pBagItemWin')
	end
	if pBagItemWin then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function OnOpenWardrobeAttached(self, indicator)
	local pWin = InitWindowPtr('Wardrobe')
	if pWin and pWin:isVisible() then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function OnOpenWardrobeThirdPageAttached(self, indicator)
	local pWin = InitWindowPtr('Wardrobe/Frame2/BtnFrame2/RadioBtn3')
	local pRadio = tolua.cast(pWin, 'CEGUI::RadioButton')
	if pRadio and pRadio:isSelected() then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function OnOpenWardrobeBackPageAttached(self, indicator)
	local pWin = InitWindowPtr('Wardrobe/Frame2/GridFrame/BtnFrame/RadioBtn1')
	local pRadio = tolua.cast(pWin, 'CEGUI::RadioButton')
	if pRadio and pRadio:isSelected() then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
		
		local strWinName = self:LocateWardrobeItemWindow()
		if not strWinName then
			self.completed = true
		else
			self.placeWardrobeItemToPosSpec.attachWin  = strWinName
			self.placeWardrobeItemToPosSpec.triggerWin = strWinName
		end
	end
end


function Update(self, fTime)
	if self.completed then
		return
	end
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

