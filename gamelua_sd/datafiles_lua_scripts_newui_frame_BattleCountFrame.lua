-- BattleCountFrame

local layoutName = 'BattleCount.layout'

ui.defineFrame(..., layoutName)
function Init(self)
	lout('BattleCountFrame:Init()')
	self.pWindow    = self:GetWindow()
	
	-- For Dragging
	self.btnDrag = self:GetChildWindow('BattleCount/AtkCountTitle')
	self.btnDrag:setProperty("MouseCursorImage", "set:SDFunction image:zhizhen");
	self.dragCtx    = {}
	self.dragCtx.isDragging = false
	self.dragCtx.dragPoint  = nil
	self.dragCtx.oldCursorArea = nil
	
	self.PanelSpecs = {
		SpecType = {"Harm", "Hate", "Heal", },
		Harm = {
			Frame     = self:GetChildWindow("BattleCount/AtkCountFrame"),
			Container = self:GetChildWindow("BattleCount/AtkCountFrame/Container"),
			Template  = self:GetChildWindow("BattleCount/AtkCountFrame/Container/Progress"),
			ToggleBtn = self:GetChildWindow("BattleCount/AtkCountTitle/CheckBtn"),
			ClearBtn  = self:GetChildWindow("BattleCount/AtkCountTitle/ClearBtn"),
			BackBtn  = self:GetChildWindow("BattleCount/AtkCountTitle/BackBtn"),
			FreeList  = {},
			MaxEntryNum = 20,
		},
		Hate   = {
			TopFrame  = self:GetChildWindow("BattleCount/HateCountFrame1"),
			Frame     = self:GetChildWindow("BattleCount/HateCountFrame2"),
			Container = self:GetChildWindow("BattleCount/HateCountFrame2/Container"),
			TopOne  = self:GetChildWindow("BattleCount/HateCountFrame1/Progress1"),
			TopTwo  = self:GetChildWindow("BattleCount/HateCountFrame1/Progress2"),
			Template  = self:GetChildWindow("BattleCount/HateCountFrame2/Container/Progress"),
			ToggleBtn = self:GetChildWindow("BattleCount/HateCountTitle/CheckBtn"),
			ClearBtn  = self:GetChildWindow("BattleCount/HateCountTitle/ClearBtn"),
			FreeList  = {},
			MaxEntryNum = 20,
		},
		Heal = {
			Frame     = self:GetChildWindow("BattleCount/TreatCountFrame"),
			Container = self:GetChildWindow("BattleCount/TreatCountFrame/Container"),
			Template  = self:GetChildWindow("BattleCount/TreatCountFrame/Container/Progress"),
			ToggleBtn = self:GetChildWindow("BattleCount/TreatCountTitle/CheckBtn"),
			ClearBtn = self:GetChildWindow("BattleCount/TreatCountTitle/ClearBtn"),
			BackBtn  = self:GetChildWindow("BattleCount/TreatCountTitle/BackBtn"),
			FreeList  = {},
			MaxEntryNum = 20,
		},
	}
	
	self:_SetPanelProperties()
	self:_PrepreFreeList()
	
	self.maxSize = {width = 800, height = 600}
	
	self.gameEvents = {}
	self.data_table = nil

	-- self.samples_string = [[ return {
		-- Harm = {
			-- {Name='美国偶像', Prof=WM_PROF_XIANGLONG, Value=10000},
			-- {Name='哎呀呀',   Prof=WM_PROF_YAOGUANG,  Value=30000},
			-- {Name='艾呀呀',   Prof=WM_PROF_XIANGLONG, Value=60000},
			-- {Name='美像', Prof=WM_PROF_XIANGLONG, Value=10000},
			-- {Name='呀呀',   Prof=WM_PROF_YAOGUANG,  Value=30000},
			-- {Name='艾呀',   Prof=WM_PROF_XIANGLONG, Value=60000},
			-- {Name='偶像', Prof=WM_PROF_XIANGLONG, Value=10000},
			-- {Name='哎呀',   Prof=WM_PROF_YAOGUANG,  Value=30000},
			-- {Name='caonima',   Prof=WM_PROF_XIANGLONG, Value=60000},
		-- },
		-- Hate = {
			-- {Name='美国偶像', Prof=WM_PROF_XIANGLONG, Target='AYY', Value=40000, IsTarget=1},
			-- {Name='哎呀呀',   Prof=WM_PROF_YAOGUANG,  Target='AYY', Value=10000, IsTarget=0},
			-- {Name='艾呀呀',   Prof=WM_PROF_XIANGLONG, Target='AYY', Value=50000, IsTarget=0},
		-- },
		-- Heal = {
			-- {Name='美国偶像', Prof=WM_PROF_XIANGLONG, Value=1000},
			-- {Name='哎呀呀',   Prof=WM_PROF_YAOGUANG,  Value=5000},
			-- {Name='艾呀呀',   Prof=WM_PROF_XIANGLONG, Value=1000},
		-- },
	-- }]]
	-- self.data_table = self:ParseDataTable(self.samples_string)
	-- self:UpdatePanelData()
	-- self:Show()
end

function UnInit(self)
	lout('BattleCountFrame:UnInit()')
	for i, specTypeName in ipairs(self.PanelSpecs.SpecType) do
		local panel = self.PanelSpecs[specTypeName]
		if not panel then
			lout("BattleCountFrame:UnInit, Invalid Spec Type Name "..specTypeName)
		end
		panel.Container:addChildWindow(panel.Template)
		
		panel.Frame:setMouseInputPropagationEnabled(true)
		panel.Frame:setMousePassThroughEnabled(true)
		panel.Container:setMouseInputPropagationEnabled(true)
		panel.Container:setMousePassThroughEnabled(true)
		local pAutoContainer = InitWindowPtr(panel.Frame:getName():c_str()..'__auto_container__')
		pAutoContainer:setMouseInputPropagationEnabled(true)
		pAutoContainer:setMousePassThroughEnabled(true)
		for _, pRow in ipairs(panel.FreeList) do 
			pRow:setMouseInputPropagationEnabled(true)
			pRow:setMousePassThroughEnabled(true)
			panel.Container:addChildWindow(pRow)
		end
		
	end
	
	for i, v in ipairs(self.gameEvents) do
		v:disconnect()
	end
	self.gameEvents = {}
end

function Subscribe(self)
	self:SubscribeUIEvents()
	self:SubscribeGameEvents()
end

function SubscribeUIEvents(self)
	--BattleCount/CloseBtn
	for i, specTypeName in ipairs(self.PanelSpecs.SpecType) do
		local panel = self.PanelSpecs[specTypeName]
		if not panel then
			lout("BattleCountFrame:_PrepreFreeList, Invalid Spec Type Name "..specTypeName)
		end
		panel.ClearBtn:setID(i)
		panel.ToggleBtn:setID(i)
		panel.ClearBtn:hide()
		if panel.BackBtn then panel.BackBtn:hide() end
		--panel.ClearBtn:subscribeEvent('Clicked', self._OnClickClear, self)
		panel.ToggleBtn:subscribeEvent('CheckStateChanged', self._OnClickToggle, self)
	end
	
	local pHateCheckBox = tolua.cast(self.PanelSpecs.Hate.ToggleBtn, 'CEGUI::Checkbox')
	pHateCheckBox:setSelected(not pHateCheckBox:isSelected())
	
	local pHealCheckBox = tolua.cast(self.PanelSpecs.Heal.ToggleBtn, 'CEGUI::Checkbox')
	pHealCheckBox:setSelected(not pHealCheckBox:isSelected())
	
	local btnDrag = self.btnDrag
	btnDrag:subscribeEvent('MouseButtonDown', self._OnDragBegin,  self)
	btnDrag:subscribeEvent('MouseMove',       self._OnDragging,   self)
	btnDrag:subscribeEvent('MouseButtonUp',   self._OnDragEnding, self)
	btnDrag:subscribeEvent('CaptureLost',     self._OnDragEnd,    self)
end

function SubscribeGameEvents(self)
	if not gameEventMgr then 
		return
	end
	
	local conn = nil
	conn = gameEventMgr:subscribeEvent(GameEvents.ShowClientTable, self._OnShowClientTable, self)
	table.insert(self.gameEvents, conn)

end

function _SetPanelProperties(self)
	for i, specTypeName in ipairs(self.PanelSpecs.SpecType) do
		local panel = self.PanelSpecs[specTypeName]
		if not panel then
			lout("BattleCountFrame:_SetPanelProperties, Invalid Spec Type Name "..specTypeName)
		end
		
		panel.Frame:setMouseInputPropagationEnabled(true)
		panel.Container:setMouseInputPropagationEnabled(true)
		panel.Template:setMouseInputPropagationEnabled(true)
		local pAutoContainer = InitWindowPtr(panel.Frame:getName():c_str()..'__auto_container__')
		pAutoContainer:setMouseInputPropagationEnabled(true)
	end
end

function _PrepreFreeList(self)
	lout('_PrepreFreeList')
	for i, specTypeName in ipairs(self.PanelSpecs.SpecType) do
		local panel = self.PanelSpecs[specTypeName]
		if not panel then
			lout("BattleCountFrame:_PrepreFreeList, Invalid Spec Type Name "..specTypeName)
		end
		
		panel.Container:removeChildWindow(panel.Template)
		for rowIndex = 1, panel.MaxEntryNum do 
			local newRowName = string.format('%s_%d', panel.Template:getName():c_str(), rowIndex)
			local pNewRow = panel.Template:clone(newRowName)
			table.insert(panel.FreeList, pNewRow)
			lout('Add to FreeList ' .. specTypeName .. ': '..newRowName)
		end
	end
end

function _OnClickClear(self, evtArgs)
	local we_arg = tolua.cast(evtArgs, 'CEGUI::WindowEventArgs')
	local w = we_arg.window
	
	local panelIndex = w:getID()
	local panelSpecType = self.PanelSpecs.SpecType[panelIndex]
	local panel = self.PanelSpecs[panelSpecType]
	
	lout('click clear')
	if not panel then return end
	lout('click clear2')
	self:ClearPanel(panel)
end

function _OnClickToggle(self, evtArgs)
	local we_arg = tolua.cast(evtArgs, 'CEGUI::WindowEventArgs')
	local w = we_arg.window
	
	local panelIndex = w:getID()
	local panelSpecType = self.PanelSpecs.SpecType[panelIndex]
	local panel = self.PanelSpecs[panelSpecType]
	
	lout('_OnClickToggle 1')
	if not panel then return end
	lout('_OnClickToggle 2')
	local bVisible = panel.Frame:isVisible(true)
	panel.Frame:setVisible(not bVisible)
	if panel.TopFrame then
		local bVisible = panel.TopFrame:isVisible(true)
		panel.TopFrame:setVisible(not bVisible)
	end
end

function _OnShowClientTable(self, evtArgs)
	if not self:IsVisible() then
		return
	end
	
	lout('ScriptDataFrame: _OnShowClientTable')
	local e = tolua.cast(evtArgs, 'SD::LuaCEGUIStringArgEvent')
	
	
	
	local strText = 'return ' .. e.strText:c_str()
	lout(strText)
	self.data_table = self:ParseDataTable(strText)
	self:UpdatePanelData()
end

function OnEnterScene(self, oldSceneID, newSceneID)
	if newSceneID == 3 or newSceneID == 60 or newSceneID == 62  then
		return
	end
	lout('OnEnterscene ' .. newSceneID)
	if SD.GenericFunc('IsInInstance', newSceneID) == 'true' then
		self:Show()
	else
		self:Hide()
	end
	--self:Show()
	--self.pWindow:setPosition(CEGUI.UVector2(CEGUI.UDim(0, 0), CEGUI.UDim(0, 200)))
end

function OnLeaveScene(self, oldSceneID)
	self:Hide()
end

function ParseDataTable(self, table_stream)
	local dataTable = (function ()
		local temp_func = loadstring(table_stream)
		if temp_func then return temp_func() end
		return nil
	end)()
	
	return dataTable
end

function ClearPanels(self)
	for i, specTypeName in ipairs(self.PanelSpecs.SpecType) do
		local panel = self.PanelSpecs[specTypeName]
		if not panel then
			lout("BattleCountFrame:_PrepreFreeList, Invalid Spec Type Name "..specTypeName)
		end
		self:ClearPanel(panel)
	end
end

function ClearPanel(self, panel)
	local pContainer = panel.Container
	while pContainer:getChildCount() > 0 do
		local nLastIndex = pContainer:getChildCount() - 1
		local pRow = pContainer:getChildAtIdx(nLastIndex)
		pContainer:removeChildWindow(pRow)
		table.insert(panel.FreeList, pRow)
	end
end

function HarmAndHealSorter(x, y)
	return x.Value > y.Value
end

function HateSorter(x, y)
	if x.IsTarget ~= y.IsTarget then
		return x.IsTarget > y.IsTarget
	else
		return x.Value > y.Value
	end
end

function UpdatePanelData(self)
	self:ClearPanels()
	
	local dataTable = self.data_table
	if not dataTable then 
		lout('dataTable is nil')
		return 
	end
	
	local harmDataTable = self.data_table["Harm"]
	local hateDataTable = self.data_table["Hate"]
	local healDataTable = self.data_table["Heal"]
	
	table.sort(harmDataTable, HarmAndHealSorter)
	table.sort(healDataTable, HarmAndHealSorter)
	table.sort(hateDataTable, HateSorter)
	
	self:UpdateHarmPanel(harmDataTable)
	self:UpdateHatePanel(hateDataTable)
	self:UpdateHealPanel(healDataTable)
end

function UpdateHarmPanel(self, harmDataTable)
	local harmPanel = self.PanelSpecs.Harm
	local nFreeNum = table.getn(harmPanel.FreeList)
	local nDataRowNum = table.getn(harmDataTable)
	
	local nTotalValue = 0.1
	for i, v in ipairs(harmDataTable) do
		nTotalValue = nTotalValue + v.Value
	end

	local nMinRowNum = math.min(nFreeNum, nDataRowNum)
	nMinRowNum = math.max(nMinRowNum, 5)
	for i = 1, nMinRowNum do 
		local rowData = harmDataTable[i]
		local nFreeListSize = table.getn(harmPanel.FreeList)
		local pRow = harmPanel.FreeList[nFreeListSize - i + 1]
		table.remove(harmPanel.FreeList, nFreeListSize - i + 1)
		harmPanel.Container:addChildWindow(pRow)
		--lout('Get Harm FreeList at ' .. tostring(nFreeListSize - i + 1))
		pRow:show()
		
		local rowName = pRow:getName():c_str()
		local pProfIcon = InitWindowPtr(rowName .. '/JobIcon')
		local pRankNum  = InitWindowPtr(rowName .. '/RankingNum')
		local pName     = InitWindowPtr(rowName .. '/Name')
		local pValue    = InitWindowPtr(rowName .. '/Num')
		local pPercent  = InitWindowPtr(rowName .. '/PercentNum')
		
		if rowData then
			local fRatio = rowData.Value * 1.0 / nTotalValue
			local fPercent = fRatio * 100
		
			pProfIcon:setProperty('Image', self:_GetProfIcon(rowData.Prof))
			pRankNum:setText(tostring(i) .. '.')
			pName:setText(rowData.Name)
			pValue:setText(string.format("%.1f", rowData.Value))
			pPercent:setText(string.format("%.0f%%", fPercent))
			pRow:setProperty('CurrentProgress', string.format("%.1f", fRatio))
		else 
			pProfIcon:setProperty('Image', '')
			pRankNum:setText('')
			pName:setText('')
			pValue:setText('')
			pPercent:setText('')
			pRow:setProperty('CurrentProgress', '0')
		end
	end

end

function UpdateHateLine(self, nIndex, rowData, pRow)
	pRow:show()
	
	local rowName = pRow:getName():c_str()
	local pProfIcon = InitWindowPtr(rowName .. '/JobIcon')
	local pRankNum  = InitWindowPtr(rowName .. '/RankingNum')
	local pName     = InitWindowPtr(rowName .. '/Name')
	local pValue    = InitWindowPtr(rowName .. '/Num')
	local pPercent  = InitWindowPtr(rowName .. '/PercentNum')
	
	if rowData then
		local fRatio = rowData.Percent
		local fPercent = fRatio * 100
	
		pProfIcon:setProperty('Image', self:_GetProfIcon(rowData.Prof))
		pRankNum:setText(tostring(nIndex) .. '.')
		pName:setText(rowData.Name)
		pValue:setText(string.format("%.1f", rowData.Value))
		pPercent:setText(string.format("%.0f%%", fPercent))
		pRow:setProperty('CurrentProgress', string.format("%.1f", fRatio))
	else 
		pProfIcon:setProperty('Image', '')
		pRankNum:setText('')
		pName:setText('')
		pValue:setText('')
		pPercent:setText('')
		pRow:setProperty('CurrentProgress', '0')
	end
end

function UpdateHatePanel(self, hateDataTable)
	local hatePanel = self.PanelSpecs.Hate
	local nFreeNum = table.getn(hatePanel.FreeList)
	local nDataRowNum = table.getn(hateDataTable)
	
	local nTopHateValue = 1
	local pTargetName = self:GetChildWindow('BattleCount/HateCountFrame1/BossName/Text')
	local pOtInfo =  self:GetChildWindow('BattleCount/HateCountFrame1/OtTips')
	if nDataRowNum >= 1 then
		local topRow = hateDataTable[1]
		local pRow = hatePanel.TopOne
		nTopHateValue = topRow.Value + 1
		pTargetName:setText(topRow.Target)
		
		topRow.Percent = 1 -- 100%
		self:UpdateHateLine(1, topRow, pRow)
	else
		local pRow = hatePanel.TopOne
		self:UpdateHateLine(1, nil, pRow)
	end
	
	if nDataRowNum >= 2 then
		local rowData = hateDataTable[2]
		local pRow = hatePanel.TopTwo
		
		rowData.Percent = rowData.Value / nTopHateValue
		pOtInfo:setVisible((rowData.Percent >= 1))
		self:UpdateHateLine(2, rowData, pRow)
	else
		local pRow = hatePanel.TopTwo
		self:UpdateHateLine(2, nil, pRow)
		pOtInfo:setVisible(false)
	end
	
	local nMinRowNum = math.min(nFreeNum, nDataRowNum)
	nMinRowNum = math.max(nMinRowNum, 3)
	for i = 1, nMinRowNum do 
		local rowData = hateDataTable[i + 2]
		local nFreeListSize = table.getn(hatePanel.FreeList)
		local pRow = hatePanel.FreeList[nFreeListSize - i + 1]
		table.remove(hatePanel.FreeList, nFreeListSize - i + 1)
		hatePanel.Container:addChildWindow(pRow)
		--lout('Get Hate FreeList at ' .. tostring(nMinRowNum - i + 1))
		
		if rowData then
			rowData.Percent  = rowData.Value / nTopHateValue
		end
		self:UpdateHateLine(i + 2, rowData, pRow)
	end

end

function UpdateHealPanel(self, harmDataTable)
	local harmPanel = self.PanelSpecs.Heal
	local nFreeNum = table.getn(harmPanel.FreeList)
	local nDataRowNum = table.getn(harmDataTable)
	
	local nTotalValue = 0.1
	for i, v in ipairs(harmDataTable) do
		nTotalValue = nTotalValue + v.Value
	end
	
	local nMinRowNum = math.min(nFreeNum, nDataRowNum)
	nMinRowNum = math.max(nMinRowNum, 5)
	for i = 1, nMinRowNum do 
		local rowData = harmDataTable[i]
		local nFreeListSize = table.getn(harmPanel.FreeList)
		local pRow = harmPanel.FreeList[nFreeListSize - i + 1]
		table.remove(harmPanel.FreeList, nFreeListSize - i + 1)
		harmPanel.Container:addChildWindow(pRow)
		--lout('Get Harm FreeList at ' .. tostring(nMinRowNum - i + 1))
		pRow:show()
		
		local rowName = pRow:getName():c_str()
		local pProfIcon = InitWindowPtr(rowName .. '/JobIcon')
		local pRankNum  = InitWindowPtr(rowName .. '/RankingNum')
		local pName     = InitWindowPtr(rowName .. '/Name')
		local pValue    = InitWindowPtr(rowName .. '/Num')
		local pPercent  = InitWindowPtr(rowName .. '/PercentNum')
		
		if rowData then
			local fRatio = rowData.Value * 1.0 / nTotalValue
			local fPercent = fRatio * 100
		
			pProfIcon:setProperty('Image', self:_GetProfIcon(rowData.Prof))
			pRankNum:setText(tostring(i) .. '.')
			pName:setText(rowData.Name)
			pValue:setText(string.format("%.1f", rowData.Value))
			pPercent:setText(string.format("%.0f%%", fPercent))
			pRow:setProperty('CurrentProgress', string.format("%.1f", fRatio))
		else 
			pProfIcon:setProperty('Image', '')
			pRankNum:setText('')
			pName:setText('')
			pValue:setText('')
			pPercent:setText('')
			pRow:setProperty('CurrentProgress', '0')
		end
	end

end

function _GetProfIcon(self, nProfType)
	if nProfType == WM_PROF_XIANGLONG then
		return 'set:BG_Normal4 image:TongjiJob_Xianglong'
	elseif nProfType == WM_PROF_BAIZHANG then
		return 'set:BG_Normal4 image:TongjiJob_Baizhang'
	elseif nProfType == WM_PROF_YAOGUANG then 
		return 'set:BG_Normal4 image:TongjiJob_Yaoguang'
	elseif nProfType == WM_PROF_JIYING then
		return 'set:BG_Normal4 image:TongjiJob_Jiying'
	end
	return ''
end

function _OnDragBegin(self, evtArgs)
	lout('BattleCountFrame:_OnDragBegin()')
	
	local e = tolua.cast(evtArgs, 'CEGUI::MouseEventArgs')
	if e.button == CEGUI.LeftButton then
		if self.btnDrag:captureInput() then
			lout('BattleCountFrame:captureInput()')
			self.dragCtx.isDragging = true
			self.dragCtx.dragPoint = CEGUI.CoordConverter:screenToWindow(self.btnDrag, e.position)
			self.dragCtx.oldCursorArea = guiMouse:getConstraintArea()
			
			local constrainArea = 
				self.pWindow:getParent():getInnerRectClipper():getIntersection(self.dragCtx.oldCursorArea)
				
			guiMouse:setConstraintArea(constrainArea)
		end
	end
	return true
end

function _OnDragEnding(self, evtArgs)
	lout('BattleCountFrame:_OnDragEnding()')
	
	local e = tolua.cast(evtArgs, 'CEGUI::MouseEventArgs')
	if e.button == CEGUI.LeftButton then
		self.btnDrag:releaseInput()
	end
	return true
end

function _OnDragging(self, evtArgs)
	local e = tolua.cast(evtArgs, 'CEGUI::MouseEventArgs')
	if self.dragCtx.isDragging then
		--lout('BattleCountFrame:_OnDragging')
		local delta = CEGUI.CoordConverter:screenToWindow(self.btnDrag, e.position)
		delta = delta - self.dragCtx.dragPoint
		
		local udimX = CEGUI.UDim:new_local(0, delta.x)
		local udimY = CEGUI.UDim:new_local(0, delta.y)
		local offset = CEGUI.UVector2:new_local(udimX, udimY)
		
		self.pWindow:setPosition(self.pWindow:getPosition() + offset)
	end
	return true
end

function _OnDragEnd(self, evtArgs)
	lout('BattleCountFrame:_OnDragEnd()')
	
	self.dragCtx.isDragging = false
	guiMouse:setConstraintArea(self.dragCtx.oldCursorArea)
	
	collectgarbage()
	
	return true
end















