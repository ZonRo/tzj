function GroupBegin(name)
end
function GroupEnd()
end

GroupBegin("Function")
-- fx offset mode
root = 0
chara = 1

function Fx(time, name, bone, scale, mode, x, y, z, rz, rx, ry, rate,autodelete,syncchar,isnotalways)
    -- CHARACTER_EVENT_FX
	Event(1, time, name, bone, scale, mode, x, y, z, rz, rx, ry, rate,autodelete,syncchar,isnotalways)
end

function Sound(time, name)
    -- CHARACTER_EVENT_SOUND
    Event(2, time, name)
end

function BodyFx(time, name, bone, scale, mode, x, y, z, rz, rx, ry)
    -- CHARACTER_EVENT_BODY_FX
	Event(3, time, name, bone, scale, mode, x, y, z, rz, rx, ry)
end

function BodySound(time, name)
    -- CHARACTER_EVENT_BODY_SOUND
    Event(4, time, name)
end

function BodyGroundSound(time, name)
    -- CHARACTER_EVENT_BODY_GROUND_SOUND
    Event(5, time, name)
end

function StyleSound(time, name)
    -- CHARACTER_EVENT_STYLE_SOUND
    Event(6, time, name)
end

function TrailFx(beginTime, endTime, name, life, fade, dummy1, dymmy2)
    -- CHARACTER_EVENT_TRAIL_FX
    Event(7, beginTime, endTime - beginTime, name, life, fade, dummy1, dymmy2)
end

function GroundFx(time, name, bone, scale, mode, x, y, z, rz, rx, ry)
    -- CHARACTER_EVENT_GROUND_FX
	Event(8, time, name, bone, scale, mode, x, y, z, rz, rx, ry)
end


function GroundSound(time, name)
    -- CHARACTER_EVENT_GROUND_SOUND
    Event(9, time, name)
end

function AttackFx(time, name, attackInfo, bone, scale, mode, x, y, z, rz, rx, ry)
    -- CHARACTER_EVENT_ATTACK_FX
	Event(10, time, name, attackInfo, bone, scale, mode, x, y, z, rz, rx, ry)
end

function AttackTargetGroundFx(time, name, attackInfo, front)
    -- CHARACTER_EVENT_ATTACK_TARGET_GROUND_FX
	Event(11, time, name, attackInfo, front)
end

function CameraShake(time, ShakeMode, ShakeAspect, ShakeTarget, ShakeRange, ShakeTime, ShakeSpeed, ShakeSpeedRand, ShakeAmplitudeX, ShakeAmplitudeY, ShakeAmplitudeZ, ShakeAmplitudeRand, ShakeShiftX, ShakeShiftY, ShakeShiftZ, ShakeSpeedFadeIn, ShakeSpeedFadeOut, ShakeAmplitudeFadeIn, ShakeAmplitudeFadeOut, ShakeCollisionTime, ShakeFirstDir, ShakeSingleSoft)
    -- CHARACTER_EVENT_CAMERA_Shake
	Event(12, time, ShakeMode, ShakeAspect, ShakeTarget, ShakeRange, ShakeTime, ShakeSpeed, ShakeSpeedRand, ShakeAmplitudeX, ShakeAmplitudeY, ShakeAmplitudeZ, ShakeAmplitudeRand, ShakeShiftX, ShakeShiftY, ShakeShiftZ, ShakeSpeedFadeIn, ShakeSpeedFadeOut, ShakeAmplitudeFadeIn, ShakeAmplitudeFadeOut, ShakeCollisionTime, ShakeFirstDir, ShakeSingleSoft)
end

function RangeCameraShake(time, ShakeDistance, ShakeMode, ShakeAspect, ShakeTarget, ShakeRange, ShakeTime, ShakeSpeed, ShakeSpeedRand, ShakeAmplitudeX, ShakeAmplitudeY, ShakeAmplitudeZ, ShakeAmplitudeRand, ShakeShiftX, ShakeShiftY, ShakeShiftZ, ShakeSpeedFadeIn, ShakeSpeedFadeOut, ShakeAmplitudeFadeIn, ShakeAmplitudeFadeOut, ShakeCollisionTime, ShakeFirstDir, ShakeSingleSoft)
    --C
	Event(56, time, ShakeDistance, ShakeMode, ShakeAspect, ShakeTarget, ShakeRange, ShakeTime, ShakeSpeed, ShakeSpeedRand, ShakeAmplitudeX, ShakeAmplitudeY, ShakeAmplitudeZ, ShakeAmplitudeRand, ShakeShiftX, ShakeShiftY, ShakeShiftZ, ShakeSpeedFadeIn, ShakeSpeedFadeOut, ShakeAmplitudeFadeIn, ShakeAmplitudeFadeOut, ShakeCollisionTime, ShakeFirstDir, ShakeSingleSoft)
end

function HurtCameraShake(time, ShaketargetMode,ShakeDistance, Mode, ShakeAspect, ShakeTarget, ShakeRange, ShakeTime, ShakeSpeed, ShakeSpeedRand, ShakeAmplitudeX, ShakeAmplitudeY, ShakeAmplitudeZ, ShakeAmplitudeRand, ShakeShiftX, ShakeShiftY, ShakeShiftZ, ShakeSpeedFadeIn, ShakeSpeedFadeOut, ShakeAmplitudeFadeIn, ShakeAmplitudeFadeOut, ShakeCollisionTime, ShakeFirstDir, ShakeSingleSoft)
	Event(57, time,  ShaketargetMode, ShakeDistance, Mode, ShakeAspect, ShakeTarget, ShakeRange, ShakeTime, ShakeSpeed, ShakeSpeedRand, ShakeAmplitudeX, ShakeAmplitudeY, ShakeAmplitudeZ, ShakeAmplitudeRand, ShakeShiftX, ShakeShiftY, ShakeShiftZ, ShakeSpeedFadeIn, ShakeSpeedFadeOut, ShakeAmplitudeFadeIn, ShakeAmplitudeFadeOut, ShakeCollisionTime, ShakeFirstDir, ShakeSingleSoft)
end

function Skill(time, name)
    -- CHARACTER_EVENT_SKILL
	Event(13, time, name)
end

function AttackHurtFx(time, name, bone, scale, mode, x, y, z, rz, rx, ry)
    -- CHARACTER_EVENT_ATTACK_HURT_FX
	Event(14, time, name, bone, scale, mode, x, y, z, rz, rx, ry)
end

function DirFx(time, name, height, offset, scale, rz, rx, ry)
    -- CHARACTER_EVENT_DIR_FX
    -- offset is the persentage of the radius
	Event(15, time, name, height, offset, scale, rz, rx, ry)
end

function Hurt(...)
		-- CHARACTER_EVENT_HURT
	Event(16,...)
end

function HurtSound(time,name)
	--CHARACTER_EVENT_HURT_SOUND
	Event(17, time, name)
end

function Charge(time,costtime,dist,angle,mode,hardtime,floattime,height)
	--CHARACTER_EVENT_CHARGE
	Event(18, time, costtime, dist, angle, mode, hardtime,floattime,height)
end

function Critical(time,fxmode,fxname,soundname)
	--CHARACTER_EVENT_CRITICAL
	Event(19, time, fxmode, fxname, soundname)
end

function Soft(time1,time2)
	--CHARACTER_EVENT_SOFT
	Event(21, time1, time2)
end

function All(time1,time2)
	--CHARACTER_EVENT_ALL
	Event(22, time1, time2)
end

function RangeAttackFx(time,name,hitfx)
	--CHARACTER_EVENT_RANGE_ATTACK_FX
	Event(23, time, name, hitfx)
end

function MustPlay(time1,time2)
	--CHARACTER_EVENT_MUST_PLAY
	Event(24, time1, time2)
end

function Priority(time,level)
	--CHARACTER_EVENT_PRIORITY
	Event(25, time, level)
end

function LoopFx(time, endtime, mode1, name, bone, scale, mode2, x, y, z, rz, rx, ry, channel)
    -- CHARACTER_EVENT_LOOP_FX
	Event(26, time, endtime, mode1, name, bone, scale, mode2, x, y, z, rz, rx, ry, channel)
end

function SkillAreaFx(time, name, loop)
    -- CHARACTER_EVENT_SKILL_AREA_FX
	Event(27, time, name, loop)
end

function DragFx(time, flytime, fxname, hitfx, hittime, mode, overrange, bone, hitbone, fadein, fadeout, r, a, b, loop, shift,dirmode,fxscale,hitfxscale,nonefxname,nonefxscale,nonehitfxname,nonehitfxscale,nonehitx,nonehity,nonehitz)
    -- CHARACTER_EVENT_DRAG_FX
	Event(28, time, flytime, fxname, hitfx, hittime, mode, overrange, bone, hitbone, fadein, fadeout, r, a, b, loop, shift,dirmode,fxscale,hitfxscale,nonefxname,nonefxscale,nonehitfxname,nonehitfxscale,nonehitx,nonehity,nonehitz)
end

function DrawDragFx(time, index, flytime, fxname, hitfx, hittime, mode, overrange, bone, fadein, fadeout, r, a, b, loop, shift,dirmode,fxscale,hitfxscale,x,y,z,useZ,isTurn,isRandom)
	Event(58, time, index, flytime, fxname, hitfx, hittime, mode, overrange, bone, fadein, fadeout, r, a, b, loop, shift,dirmode,fxscale,hitfxscale,x,y,z,useZ,isTurn,isRandom)
end

function LinkFx(starttime, fxname, effecttim, startbone, targetbone, mode, delay)
    -- CHARACTER_EVENT_LINK_FX
	Event(31, starttime, fxname, effecttim, startbone, targetbone, mode, delay)
end

function Unsheath(time)
	Event(29, time)
end

function Sheath(time)
	Event(30, time)
end

function Channel(time, channelID)
	Event(32, time, channelID)
end

function SkeletonMod(time, realTime, modID)
	Event(33, time, realTime, modID)
end

function HitGroundFx(time, fxname, scale, x, y, z, rz, rx, ry)
	Event(34, time, fxname,  scale, x, y, z, rz, rx, ry)
end

function FadeIn(time, begintime, durationtime)
	Event(35, time, begintime, durationtime)
end

function FadeOut(time, begintime, durationtime)
	Event(36, time, begintime, durationtime)
end

function SightChange(time)
	Event(37, time)
end

function Pause(time,cost,mode,ratio)  --蘇�珋閨胉hannel 0 衄虴
	Event(38, time, cost, mode,ratio)
end

function PlayMode(time,mode1,mode2)
	Event(39, time,mode1,mode2)
end

function EffectStart(time, name, f1, f2, f3, f4, f5)
    -- CHARACTER_EVENT_START_VIEWEFFCT
    Event(40, time, name, f1, f2, f3, f4, f5)
end

function EffectEnd(time, name )
    -- CHARACTER_EVENT_END_VIEWEFFCT
    Event(41, time, name )
end

function StartJA(time, f1 )
    -- CHARACTER_EVENT_START_JA
    Event(42, time, f1 )
end

function PrivateFx(time, name, bone, scale, mode, x, y, z, rz, rx, ry)
    -- CHARACTER_EVENT_PRIVATE_FX
    Event(43, time,name, bone, scale, mode, x, y, z, rz, rx, ry)
end

function JAHitFx(time, name, bone, scale, mode, x, y, z, rz, rx, ry)
    -- CHARACTER_EVENT_JA_HIT_FX
    Event(44, time,name, bone, scale, mode, x, y, z, rz, rx, ry)
end

function PrivateJAFx(time, name, bone, scale, mode, x, y, z, rz, rx, ry)
    -- CHARACTER_EVENT_PRIVATE_JA_FX
    Event(45, time,name, bone, scale, mode, x, y, z, rz, rx, ry)
end

function HitFx(time, private, name, bone, scale, mode, x, y, z, rz, rx, ry)
    -- CHARACTER_EVENT_HIT_FX
    Event(46, time, private, name, bone, scale, mode, x, y, z, rz, rx, ry)
end

function Hard1(time1,time2)
	Event(47, time1, time2)
end

function Hard2(time1,time2)
	Event(48, time1, time2)
end

function HurtHard(time1,time2)
	Event(49, time1, time2)
end


function Equip(time,part)
	Event(50, time, part)
end

function UnEquip(time,part)
	Event(51, time, part)
end

function StopChannel(time,channel,mode)
	Event(52, time, channel,mode)
end

function LineFx(time,name,index,startbone, endbone,scale,fxlength,x,y,z,rz,rx,ry,rate,autodelete,isRandom)
	Event(53, time,name,index,startbone, endbone,scale,fxlength,x,y,z,rz,rx,ry,rate,autodelete,isRandom)
end

function PlugIn(time)
	Event(54, time)
end

function PlugOut(time)
	Event(55, time)
end

function CutScene(time,name)
	Event(59, time,name)
end

function DarkNight(time,costtime,degree)
	Event(60, time,costtime,degree)
end

function ChangeMaterial(time,costtime,materialname)
	Event(61, time,costtime,materialname)
end

function ContralShake(time,costtime,mode1,degree,mode2,range)
	Event(62, time,costtime,mode1,degree,mode2,range)
end

function PrivateSound(time,soundname)
	Event(63, time,soundname)
end

function HurtBoneFx(time,name,bone,scale,x,y,z,rz, rx, ry, mode, speed)
	Event(64, time,name,bone,scale,x,y,z,rz, rx, ry, mode, speed)
end

function HideWeapon(time)
    -- CHARACTER_EVENT_SOUND
    Event(65, time)
end

function CameraRestoral(time, yaw, pitch, dist, speed)
	Event(66, time, yaw, pitch, dist, speed)
end

function CampFx(time, nameA, nameB, nameC, bone, scale, mode, x, y, z, rz, rx, ry, rate,autodelete,syncchar,isnotalways)
    -- CHARACTER_EVENT_FX
	Event(67, time, nameA, nameB, nameC, bone, scale, mode, x, y, z, rz, rx, ry, rate,autodelete,syncchar,isnotalways)
end

function Next(name)
    -- CHARACTER_EVENT_NEXT_STAKE
    TerminalEvent(20, name)
    -- CHARACTER_STAKE_SPARAM_NEXT
    SpecificString(1, name)
end

function GroundNext(name)
    -- CHARACTER_STAKE_SPARAM_GROUND_NEXT
    SpecificString(0, name)
end

function NoFaceTarget()
    -- CHARACTER_STAKE_FPARAM_FACE_TARGET
    SpecificFloat(1, 0)
end

function NoHurt()
    -- CHARACTER_STAKE_FPARAM_HURT
    SpecificFloat(2, 0)
end

function AttackLeftHand()
    AttackTarget(1)
end

function AttackRightHand()
    AttackTarget(2)
end

function AttackLeftLeg()
    AttackTarget(3)
end

function AttackRightLeg()
    AttackTarget(4)
end

function AttackLeftArm()
    AttackTarget(1)
    AttackTarget(5)
end

function AttackRightArm()
    AttackTarget(2)
    AttackTarget(6)
end

function AttackHead()
    AttackTarget(7)
end

function AttackTail()
    AttackTarget(8)
end

function AttackBody()
    AttackTarget(0)
    AttackTarget(1)
    AttackTarget(2)
    AttackTarget(3)
    AttackTarget(4)
    AttackTarget(5)
    AttackTarget(6)
    AttackTarget(7)
    AttackTarget(8)
end

function AttackLeftWeapon()
    AttackDynaTarget(20,21)
    SpecificFloat(0, 1)
end

function AttackRightWeapon()
    AttackDynaTarget(20,20)
    SpecificFloat(0, 1)
end

function AttackWeapon()
    AttackDynaTarget(20,20)
    AttackDynaTarget(20,21) -- part 20, group 20
    -- CHARACTER_STAKE_FPARAM_WEAPON_ATTACK
    SpecificFloat(0, 1)
end

--腳貼地

function StepBoth()
    StepDown("LeftFoot",0)
    StepDown("RightFoot",0)
end

function StepPetAll()
    StepDown("LeftFoot",0)
    StepDown("RightFoot",0)
    StepDown("RightForeArmRoll",0)
    StepDown("LeftForeArmRoll",0)
end

--blend mode
normal = 0
sync = 1
fromhead = 2


--戰場動作共用--
function BT()
    HipShift(1,1,1.06,1)
    HipShift(1,1,1,2)
    HipShift(1,1,0.99,3)
    HipShift(1,1,0.91,4)
    HipShift(1,1,1.03,6)
end

--武器動作共用--
function WP()
    HipShift(1,1,1.06,1)
    HipShift(1,1,1,2)
    HipShift(1,1,0.95,3)
    HipShift(1,1,0.88,4)
    HipShift(1,1,1,6)
end

function LowHip()
    HipShift(1,1,1.2,1)
    HipShift(1,1,1.2,2)
    HipShift(1,1,1.2,3)
    HipShift(1,1,1.2,4)
    HipShift(1,1,1.2,6)
end


--倒地動作共用--
function DownHip()
    HipShift(1,1,1.5,1)
    HipShift(1,1,1.5,2)
    HipShift(1,1,1.5,3)
    HipShift(1,1,1.5,4)
    HipShift(1,1,1,6)
end



GroupEnd()