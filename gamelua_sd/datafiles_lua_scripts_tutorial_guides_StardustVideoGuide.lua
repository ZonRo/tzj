
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame
	
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	ui.TutorialVideoFrame:ShowVideo('Stardust')
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	ui.TutorialVideoFrame:HideVideo()
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)

end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

