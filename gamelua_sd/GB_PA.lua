GroupBegin("CPB")

-----------------------------------------------------------------------ؤ��ȭͷ�ٿ�------------------------------------------------------------------

STake_Start("HG_GB_PA_PY_move_004","HG_GB_PA_PY_move_004")         --ǰ����
    BlendMode(0.05)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5")   
    PlaySpeed(0,0.23,0.77)
    --ChangeMaterial(0,0.4,"/PC/Material/B")

  	Fx(0,"HG_GB_PA_PY_move_004","Reference")   
	
    Sound(0.02,"char_05_cloth_flap_02")
    Sound(0.02,"HG_SoundVOC_Jump_1")
    Sound(0.26,"N2_BYJQ01_dodge_001")
    GroundFx(0.2, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.36, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.2, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.36, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
    GroundSound(0.2,"Sound_step")
    GroundSound(0.36,"Sound_step") 
    EffectStart(0.0,"MB",4)

STake_Start("HG_GB_PA_PY_move_004_air","HG_GB_PA_PY_move_004_air")         --����ǰ����
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5")   

  	Fx(0,"HG_GB_PA_PY_move_004","Reference")     
	
    Sound(0.010,"char_05_cloth_flap_02")
    Sound(0.02,"HG_SoundVOC_Jump_1")
    Sound(0.010,"N2_BYJQ01_dodge_001")
    EffectStart(0.0,"MB",4)	
	
	
	
STake_Start("HG_GB_PA_SU_move_004","HG_GB_PA_SU_move_004")  --ǰ������ 
    BlendTime(0.2)
    BlendMode(1) 
    Loop()
    --EffectStart(0, "FOV", 15)
    GroundSound(0.05,"Sound_step")
    GroundSound(0.3,"Sound_step")
    GroundFx(0.05, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.3, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(0.05, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.3, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
	
--------------------------------------------------------------------------

STake_Start("HG_GB_PA_QG_move_004_start","HG_GB_PA_QG_move_004_start")    --�Ṧǰ��������
    BlendTime(0.05)
    BlendMode(0)
  	Fx(0,"HG_GB_PA_PY_move_004","Reference")   
  	Fx(0,"FX_move_QG_fire","Reference") 
    GroundFx(0.155, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.08, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.155, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.08, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      


    EffectStart(0, "MB",0)
 
	Sound(0.08,"char_05_cloth_flap_01")
	Sound(0.08,"HG_GB_PA_Skill_000_Combo1")
    GroundSound(0.165,"Sound_step")
    GroundSound(0.04,"Sound_step")


STake_Start("HG_GB_PA_QG_move_004_end","HG_GB_PA_QG_move_004_end")    --�Ṧǰ������ɲ��
    BlendTime(0.02)
    BlendMode(0)

  	--Fx(0,"HG_GB_PA_PY_move_004_QG","Reference")   
    GroundFx(0.21, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.06, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.21, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.06, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    GroundSound(0.090,"Sound_brake")
    GroundSound(0.21,"Sound_step")
    GroundSound(0.06,"Sound_step")

    EffectStart(0, "FOV", 0)
	
STake_Start("HG_GB_PA_QG_move_004","HG_GB_PA_QG_move_004+")    --�Ṧǰ������
    BlendTime(0.2)
    BlendMode(1)

    GroundFx(0.27, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.04, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.27, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.04, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    Loop()    
    
    Fx(0.01,"FX_move_QG_common")


    EffectStart(0, "FOV", 5)
    EffectStart(0, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    GroundSound(0.04,"Sound_step")
    GroundSound(0.27,"Sound_step")

STake_Start("HG_GB_PA_QG_move_014_up","HG_GB_PA_QG_move_014(0,22)(23,(40,60))1")  --�Ṧǰ����1��
    BlendTime(0.1)
    BlendMode(0)
    FrameTime(0,0.733)
  	--Fx(0,"HG_GB_PA_PY_move_004_QG","Reference")     
    Fx(0,"FX_move_QG_01") 
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)
    GroundSound(0.140,"Sound_jump")
    Sound(0.140,"char_05_cloth_flap_01") 
    Sound(0.140,"HG_SoundVOC_Jump_1")       

STake_Start("HG_GB_PA_QG_move_014_loop","HG_GB_PA_QG_move_014(0,22)(23,(40,60))1")  --�Ṧǰ������1��
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.733,1.333) 
    Loop()
  	--Fx(0.733,"HG_GB_PA_PY_move_004_QG","Reference") 
    EffectStart(0.766, "FOV", 15)
    EffectStart(0.766, "MB",0)
    Sound(0.900,"char_05_cloth_flap_02")

STake_Start("HG_GB_PA_QG_move_014_up_2","HG_GB_PA_QG_move_014(0,22)(23,(40,60))2")  --�Ṧǰ����2��
    BlendTime(0.1)
    BlendMode(0)
    FrameTime(0,0.733)
  	--Fx(0,"HG_GB_PA_PY_move_004_QG","Reference")     
    Fx(0,"FX_move_QG_02")
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)
    GroundSound(0.140,"Sound_jump")
    Sound(0.140,"char_05_cloth_flap_01") 
    Sound(0.140,"HG_SoundVOC_Jump_1")    

STake_Start("HG_GB_PA_QG_move_014_loop_2","HG_GB_PA_QG_move_014(0,22)(23,(40,60))2")  --�Ṧǰ������2��
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.733,1.333) 
    Loop()
    EffectStart(0.766, "FOV", 15)
    EffectStart(0.766, "MB",0)
    Sound(0.900,"char_05_cloth_flap_02")	
  	--Fx(0.733,"HG_GB_PA_PY_move_004_QG","Reference") 
	
STake_Start("HG_GB_PA_QG_move_014_up_3","HG_GB_PA_QG_move_014(0,22)(23,(40,60))3")  --�Ṧǰ����3��
    BlendTime(0.1)
    BlendMode(0)
    FrameTime(0,0.733)
  	--Fx(0,"HG_GB_PA_PY_move_004_QG","Reference")     
    Fx(0,"FX_move_QG_03") 
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)
    GroundSound(0.140,"Sound_jump")
    Sound(0.140,"char_05_cloth_flap_01")
    Sound(0.140,"HG_SoundVOC_Jump_1")     

STake_Start("HG_GB_PA_QG_move_014_loop_3","HG_GB_PA_QG_move_014(0,22)(23,(40,60))3")  --�Ṧǰ������3��
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.733,1.333) 
    Loop()
    EffectStart(0.766, "FOV", 15)
    EffectStart(0.766, "MB",0)
    Sound(0.900,"char_05_cloth_flap_02")	
  	--Fx(0.733,"HG_GB_PA_PY_move_004_QG","Reference") 	
	
----------------------------------------------------------------------------- 	
STake_Start("HG_GB_PA_jf","HG_GB_PA_jf")    --���� ǰ
    BlendTime(0.2)
    BlendMode(0)
    HurtHard(0,0.5)
    Priority(0.5,"5")

    GroundSound(0.15,"Sound_step")
    GroundSound(0.18,"Sound_step")
    Sound(0.140,"HG_SoundVOC_Hurt2_1")
	
STake_Start("HG_GB_PA_jf_B","HG_GB_PA_jf_B")    --���� ��
    BlendTime(0.2)
    BlendMode(0)
    HurtHard(0,0.5)
    Priority(0.5,"5")

    GroundSound(0.15,"Sound_step")
    GroundSound(0.18,"Sound_step")
    Sound(0.140,"HG_SoundVOC_Hurt2_1")

STake_Start("HG_GB_PA_jf_L","HG_GB_PA_jf_L")    --���� ��
    BlendTime(0.2)
    BlendMode(0)
    HurtHard(0,0.5)
    Priority(0.5,"5")

    GroundSound(0.15,"Sound_step")
    GroundSound(0.18,"Sound_step")
    Sound(0.140,"HG_SoundVOC_Hurt2_1")

STake_Start("HG_GB_PA_jf_R","HG_GB_PA_jf_R")    --���� ��
    BlendTime(0.2)
    BlendMode(0)
    HurtHard(0,0.5)
    Priority(0.5,"5")

    GroundSound(0.15,"Sound_step")
    GroundSound(0.18,"Sound_step")
    Sound(0.140,"HG_SoundVOC_Hurt2_1")	
	
STake_Start("HG_GB_PA_ss","HG_GB_PA_ss")    --����     
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"1")     
	Priority(0.38,"5")
    BlendPattern("steady") 
    Sound(0.140,"HG_SoundVOC_Jump_1")
    Sound(0.100,"char_05_cloth_flap_02")	

  	Fx(0,"HG_GB_PA_ss_a","Reference",1,0,0,0,0,0,0,0,1) 
	Fx(0,"HG_ALL_ss_b","Reference",1,0,0,0,0,0,0,0,1) 

	
STake_Start("HG_GB_PA_ss_air","HG_GB_PA_ss_air")    --��������     
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"1")     
	Priority(0.38,"5")
    BlendPattern("steady") 	
    Sound(0.140,"HG_SoundVOC_Jump_1")
    Sound(0.100,"char_05_cloth_flap_02")

  	Fx(0,"HG_GB_PA_ss_a","Reference",1,0,0,0,0,0,0,0,1) 
	

STake_Start("HG_GB_PA_def_000","HG_GB_PA_def_000")
    BlendMode(0)
    BlendTime(0.2)
    --BlendPattern("Upper_hurt")
    
    MotionBlur(0.000,0.204,3)  
    Fx(0,"Fx_gedang_quan")      
    


STake_Start("HG_GB_PA_dead","HG_GB_PA_dead")
    BlendMode(0)
    BlendTime(0.2)
    Sound(0.05,"HG_SoundVOC_Dead_1")
    GroundSound(0.89,"Sound_daodi") 
    StopChannel(0,2)		
	
	
STake_Start("HG_GB_PA_hit","HG_GB_PA_hit")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_hurt")
    Sound(0.020,"HG_SoundVOC_Hurt2_1")

STake_Start("HG_GB_PA_move_001","HG_GB_PA_move_001")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")
  Loop() 
  
    GroundFx(0.831, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.484, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
    GroundFx(0.831, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.484, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    

    GroundSound(0.329,"Sound_step")
    GroundSound(0.822,"Sound_step")

STake_Start("HG_GB_PA_move_003","HG_GB_PA_move_003")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")    
    --Sound(0.601,"PC_step_normal")
  Loop() 
  
    GroundFx(0.450, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.831, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.450, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.831, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    

    GroundSound(0.389,"Sound_step")
    GroundSound(0.822,"Sound_step")    

STake_Start("HG_GB_PA_move_004","HG_GB_PA_move_004")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")    
    --Sound(0.601,"PC_step_normal")
  Loop() 
    
    GroundFx(0.34, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.07, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.34, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.07, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    

    GroundSound(0.07,"Sound_step")
    GroundSound(0.34,"Sound_step")

STake_Start("HG_GB_PA_move_005","HG_GB_PA_move_005")
    BlendTime(0.2)
    BlendMode(0)
    BlendPattern("right_left_move")    
    --Sound(0.601,"PC_step_normal")
  Loop() 


    GroundFx(0.432, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.432, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    

    GroundSound(0.021,"Sound_step")
    GroundSound(0.373,"Sound_step")
  

STake_Start("HG_GB_PA_move_006","HG_GB_PA_move_006")
    BlendTime(0.2)
    BlendMode(0)
    BlendPattern("right_left_move")    
    --Sound(0.601,"PC_step_normal")
  Loop() 

    GroundFx(0.432, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.432, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
  
    GroundSound(0.021,"Sound_step")
    GroundSound(0.373,"Sound_step")

STake_Start("HG_GB_PA_move_009","HG_GB_PA_move_009")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")    
    --Sound(0.601,"PC_step_normal")

    GroundSound(0.160,"Sound_step")
    GroundSound(0.520,"Sound_step")

  Loop() 

STake_Start("HG_GB_PA_move_010","HG_GB_PA_move_010")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")        
    GroundSound(0.210,"Sound_step")
    GroundSound(0.610,"Sound_step")
  Loop() 

STake_Start("HG_GB_PA_wait_000","HG_GB_PA_wait_001_new")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("steady")        
    Loop()
    
STake_Start("HG_GB_PA_wait_000_ui","HG_GB_PA_wait_001_new")
    BlendMode(0)
    BlendTime(0)    
    Loop()
    BlendPattern("wait_none")   


STake_Start("HG_GB_PA_SYJwait_000","HG_GB_PA_SYJwait_000")  --���¼��������1
    BlendMode(0)
    BlendTime(0.2)       
    Loop()
	
STake_Start("HG_GB_PA_SYJwait_001","HG_GB_PA_SYJwait_001")  --���¼��������2
    BlendMode(0)
    BlendTime(0.2)    
    Loop()

STake_Start("HG_GB_PA_SYJwait_002","HG_GB_PA_SYJwait_002")  --���¼��������3
    BlendMode(0)
    BlendTime(0.2)     
    Loop()	


------------------------------------------------------------ؤ����ȭ----------------------------------------------------------------------
------------------------------------------------------------ؤ����ȭ----------------------------------------------------------------------
------------------------------------------------------------  ���------------------------------------------------------------------------

--[[STake_Start("HG_GB_PA_attack_A_0000_start1","HG_GB_PA_wait_000")  --����start1 
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("steady")    
    Loop()
  	Fx(0.1,"HG_GB_PA_skill_014_start","",1.1,0,0,0,0,0,0,0,1,0,0)
    Sound(0.1,"HG_GB_PA_Skill_000_Combo1")	]]--

	
STake_Start("HG_GB_PA_attack_A_001","HG_GB_PA_attack_A_001")   -- �չ�1    17001  �����0.2   0.15~0.33   0.4
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"5")     
    BlendPattern("steady") 

    Charge(0,0.05,20,15,12) --0.15
    Sound(0.050,"GB_New2_attack_000")
    Sound(0.050,"HG_SoundVOC_attack1_1")
  	--Fx(0.18,"HG_GB_PA_skill_000_hit","Reference",0.6,0,0,-43,42,180,-5,0,1)    
  	Fx(0.2,"HG_GB_PA_attack_A_001","Reference",1,0,0,-20,5,0,0,0,1,0,0,1) 	
   
	
  	AttackStart()
    Hurt(0.2,"hurt_62",0,1) 	
    HurtSound(0.2,"PA_Hit_002") 
    AttackHurtFx(0.2,"HG_GB_PA_attack_A_hit","Spine1",1)	
    HurtBoneFx(0.4,"FX_Violent_blood_saxue1","Spine1",1.2,0,0,0,180,0,0,1)	
	
    CameraShake(0.2,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")
	--ContralShake(0.2,0.25,0,0.7)	
	
STake_Start("HG_GB_PA_attack_A_002","HG_GB_PA_attack_A_002")   -- �չ�2    17002  �����0.2  0.15~0.33   0.4
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"5")     
    BlendPattern("steady") 	

    Charge(0,0.06,20,15,12) --0.14

  	Fx(0.2,"HG_GB_PA_attack_A_002","Reference",1,0,0,-20,5,0,0,0,1,0,0,1) 
	
  	AttackStart()
    Hurt(0.2,"hurt_62",0,1)	

    Sound(0.050,"GB_New2_attack_000")
    Sound(0.050,"HG_SoundVOC_attack1_1")
    HurtSound(0.2,"PA_Hit_002") 	
    AttackHurtFx(0.2,"HG_GB_PA_attack_A_hit","Spine1",1)	
    HurtBoneFx(0.4,"FX_Violent_blood_saxue1","Spine1",1.2,0,0,0,180,0,0,1) 
	
    CameraShake(0.2,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")
	--ContralShake(0.2,0.25,0,0.7)	
	ContralShake(0.53,0.3,0,0.9)

	
--[[STake_Start("HG_GB_PA_attack_A_003","HG_GB_PA_attack_A_003")   -- �չ�3    17003  �����0.78   0.5~0.67     0.7
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"5")     
    BlendPattern("steady")

	PlaySpeed(0,0.3,0.6)
	PlaySpeed(1,1.1,0.2)
	
	Charge(0,0.05,20,15,12)  --0.93   0.73
	
  	Fx(0,"HG_GB_PA_attack_A_004a","Reference",1,0,0,0,0,0,0,0,1.2,1) 	
  	Fx(0.73,"HG_GB_PA_attack_A_004b","Reference",1,0,0,-33,2,0,0,0,1,0,0,1)  	
	
  	AttackStart()
    Hurt(0.68,"hurt_62",0,1) 
    AttackHurtFx(0.68,"HG_GB_PA_attack_A_hit","Spine1",1)	
    HurtBoneFx(0.68,"FX_Violent_blood_saxue1","Spine1",1.2,0,-20,0,180,0,0,1) 
	
    Sound(0.010,"GB_New2_attack_004")
    Sound(0.734,"GB_New2_attack_000")
    Sound(0.010,"HG_SoundVOC_attack3_1")
    Sound(0.734,"HG_SoundVOC_attack2_1")
    HurtSound(0.68,"PA_Hit_004") 
	
    CameraShake(0.78,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")
	ContralShake(0.78,0.3,0,0.8)]]--
	
STake_Start("HG_GB_PA_attack_A_004","HG_GB_PA_attack_A_004")   -- �չ�4    17004  �����0.3    0.25~0.43   0.5
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"5")     
    BlendPattern("steady") 	
	
    Charge(0,0.06,20,15,12)	--0.24

  	Fx(0.3,"HG_GB_PA_attack_A_003","Reference",1,0,0,-25,0,0,0,0,1,0,0,1) 
	
  	AttackStart()
    Hurt(0.3,"hurt_62",0,1) 

    Sound(0.010,"HG_emo_000_BTSFAN_2")
    Sound(0.180,"GB_New2_attack_000")
    Sound(0.180,"HG_SoundVOC_attack1_1")
    HurtSound(0.3,"PA_Hit_002") 

    AttackHurtFx(0.3,"HG_GB_PA_attack_A_hit","Spine1",1)
    HurtBoneFx(0.3,"FX_Violent_blood_saxue1","Spine1",1.2,0,-40,0,180,0,0,1)
	
    CameraShake(0.3,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")
	--ContralShake(0.3,0.25,0,0.7)	


	
STake_Start("HG_GB_PA_attack_A_005","HG_GB_PA_attack_A_005")   -- �չ�5    17057  �����0.8    
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
	
  	Fx(0.0,"HG_GB_PA_skill_015","Reference",1,0,0,0,0,0,0,0,1.8,0,0,1) 
    PlaySpeed(0,1,2)		
	
    HitGroundFx(0.9,"SFX_hitground")   		 		
    HitGroundFx(0.9,"SFX_hitground2")	
	
	Charge(0,0.4,60,15,12) 

  	AttackStart()
    Hurt(0.4,"hurt_62",0,1) 	

    Sound(0.010,"GB_New2_attack_000")
    Sound(0.783,"GB_New2_attack_003")
    Sound(0.010,"HG_SoundVOC_attack2_1")
    Sound(0.783,"HG_SoundVOC_attack3_1")
    HurtSound(0.4,"PA_Hit_004") 

    AttackHurtFx(0.4,"HG_GB_PA_attack_A_hit","Spine1",1)	
	HurtBoneFx(0.4,"FX_Violent_blood_Blunt_R2","Spine1",0.2,0,0,0,0,0,0)
	
    --Pause(0.41,0.1,1,0.09) 	
	CameraShake(0.9,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 6")
	ContralShake(0.9,0.55,0,0.9)


	
STake_Start("HG_GB_PA_attack_A_JA_001","HG_GB_PA_attack_A_JA_001")   -- JA1    17006  �����0.18|0.355   
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	

    PlaySpeed(0,0.64,1.8)
	
	Charge(0,0.075,20,20,12)  --0.105	
	
  	--Fx(0,"HG_GB_PA_attack_A_JA_001","Reference",1,0,0,0,0,0,0,0,1) 
  	Fx(0,"HG_GB_PA_attack_A_JA_001_a",1,0,0,0,0,0,0,0,1)
  	Fx(0.34,"HG_GB_PA_attack_A_JA_001_b",1,0,0,0,0,0,0,0,1)
  	Fx(0.64,"HG_GB_PA_attack_A_JA_001_c",1,0,0,0,0,0,0,0,1)
    Sound(0.010,"GB_New2_attack_004")
    Sound(0.283,"GB_New2_attack_005")
    Sound(0.579,"GB_New2_attack_003")
    Sound(0.200,"HG_SoundVOC_attack2_1")
    Sound(0.530,"HG_SoundVOC_attack3_1")
	
  	AttackStart()
    Hurt(0.18,"hurt_62",0,1)
    Hurt(0.355,"hurt_62",0,1)
  	HurtSound(0.18,"BTS_CR_HIT_new_2_1") 
  	HurtSound(0.355,"BTS_CR_HIT_new_2_1") 
    --DirFx(0.1,"HG_GB_PA_hit",35,1)		
    --DirFx(0.4,"HG_GB_PA_hit",35,1)	
    AttackHurtFx(0.18,"HG_GB_PA_attack_A_hit","Head",1) 	
    AttackHurtFx(0.355,"HG_GB_PA_attack_A_hit","Spine1",1) 	
	HurtBoneFx(0.18,"","Spine1",1,0,0,5,180,0,0)	
	HurtBoneFx(0.6,"FX_blood_quan_b","Spine1",1.2,0,0,5,180,0,0)
	
    --Pause(0.65,0.15,1,0.15) 	
	CameraShake(0.64,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")
	--ContralShake(0.33,0.2,0,0.7)
	--ContralShake(0.64,0.25,0,0.8)
	
STake_Start("HG_GB_PA_attack_A_JA_002","HG_GB_PA_skill_009")  --JA2  17009   0.29|0.51|0.7|0.9|1.31   soft����ʱ��1.6
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("steady")       		

	PlaySpeed(1.31,1.4,0.5)

  	--Fx(0,"HG_GB_PA_attack_A_JA_002",1,0,0,0,0,0,0,0,1)    
  	Fx(0.24,"HG_GB_PA_skill_009_fire_a",1,0,0,0,0,0,0,0,1)
  	Fx(0.48,"HG_GB_PA_skill_009_fire_b",1,0,0,0,0,0,0,0,1)
  	Fx(0.7,"HG_GB_PA_skill_009_fire_c",1,0,0,0,0,0,0,0,1)
  	Fx(0.9,"HG_GB_PA_skill_009_fire_d",1,0,0,0,0,0,0,0,1)
  	Fx(1.25,"HG_GB_PA_skill_009_fire_e",1,0,0,0,0,0,0,0,0.7)
	
    Sound(0.010,"HG_GB_PA_Skill_014")  		
    Sound(0.20,"GB_New2_attack_004")
    Sound(0.43,"GB_New2_attack_005")
    Sound(1.29,"GB_New2_attack_003")
    Sound(0.212,"HG_SoundVOC_attack4_1")
    Sound(0.729,"HG_SoundVOC_attack2_1")
    Sound(0.900,"HG_SoundVOC_attack1_1")
    Sound(1.313,"HG_SoundVOC_attack3_1")    
    HitGroundFx(1.251 ,"SFX_hitground")
  	HitGroundFx(1.251,"SFX_hitground2")  
  
    Charge(0,0.06,50,18,12)  --0.18~0.24
  
    AttackStart()
    Pause(1.31,0.07,0,0.05) 	
    Hurt(0.29,"hurt_62",0,1)
    Hurt(0.51,"hurt_21",10,1)
    Hurt(0.7,"hurt_62",0,1)
    Hurt(0.9,"hurt_21",10,1)
    Hurt(1.31,"hurt_81",0,1) 
    AttackHurtFx(0.29,"HG_GB_PA_attack_A_hit","Spine1",1) 	--DirFx(0.29,"HG_GB_PA_hit",25,0) 
    AttackHurtFx(0.51,"HG_GB_PA_attack_A_hit","Spine1",1) 	--DirFx(0.51,"HG_GB_PA_hit",25,0)  
    AttackHurtFx(0.7,"HG_GB_PA_attack_A_hit","Spine1",1) 	--DirFx(0.7,"HG_GB_PA_hit",25,0)  
    AttackHurtFx(0.9,"HG_GB_PA_attack_A_hit","Spine1",1) 	--DirFx(0.9,"HG_GB_PA_hit",25,0)  
    AttackHurtFx(1.31,"HG_GB_PA_attack_A_hit","Spine1",1) 	--DirFx(1.31,"HG_GB_PA_hit",25,1)  
    HurtBoneFx(0.29,"","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.51,"","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.7,"","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.9,"","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1.31,"FX_Violent_blood_Sharp_R4","Spine1",0.7,0,0,0,180,0,0,1)    
    HurtSound(0.29,"PA_Hit_002")
    HurtSound(0.51,"PA_Hit_002")
    HurtSound(0.7,"PA_Hit_002")
    HurtSound(0.9,"PA_Hit_002")
    HurtSound(1.31,"PA_Hit_004")

    --CameraShake(1.28,"ShakeTimes = 0.3","ShakeMode = 3","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
    CameraShake(1.28,"ShakeMode = 3","ShakeTimes = 0.25","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")    	
	ContralShake(0.29,0.2,0,0.7)
	ContralShake(0.51,0.2,0,0.7)
	ContralShake(0.7,0.25,0,0.8)
	ContralShake(0.9,0.25,0,0.8)	
	ContralShake(1.4,0.3,0,0.9) 	
	
--[[STake_Start("HG_GB_PA_attack_A_JA_003","HG_GB_PA_skill_016")   -- JA��    17007  �����0.2|0.3|0.4|0.72     0.1~1.5    1.6
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	
	Charge(0,0.2,40,20,12)  --0.2	
	
  	Fx(0.2,"","Reference",0.6,0,10,-50,45,180,-5,0,1) 	
  	Fx(0.3,"","Reference",0.6,0,-13,-50,35,180,-5,0,1) 
  	Fx(0.4,"","Reference",0.6,0,15,-50,50,180,-5,0,1) 	
  	Fx(0.72,"","Reference",0.8,0,0,-40,53,180,-5,0,1) 	
	
  	AttackStart()
    Hurt(0.2,"hurt_62",0,1)
    Hurt(0.3,"hurt_21",0,1)	
    Hurt(0.4,"hurt_21",0,1)	
    Hurt(0.72,"hurt_81",0,1)	
  	HurtSound(0.2,"PA_Hit_001") 
  	HurtSound(0.3,"PA_Hit_001") 
  	HurtSound(0.4,"PA_Hit_001") 
  	HurtSound(0.72,"PA_Hit_001") 	
    DirFx(0.2,"HG_GB_PA_attack_A_hit",35,1.5)		
    DirFx(0.3,"HG_GB_PA_attack_A_hit",35,1.5)	
    DirFx(0.4,"HG_GB_PA_attack_A_hit",35,1.5)	
    DirFx(0.72,"HG_GB_PA_attack_A_hit",35,1.5)		
	
    CameraShake(0.72,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 3")	]]--
	
--------------------------------------------------------------�Ҽ�------------------------------------------------------------------------


--[[STake_Start("HG_GB_PA_attack_B_000_start1","HG_GB_PA_skill_022")  --����start1 
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("steady")  
    PlaySpeed(0.9,0.98,0.7)	
    Loop(0.99,1)
    Fx(0.16,"HG_GB_PA_skill_008_start_loop2","Spine1",1.3,0,0,0,0,0,0,0,1,1)  
    Fx(0.66,"HG_GB_PA_skill_008_start_loop2","Spine1",1.3,0,0,0,0,0,0,0,1,1)  
    Fx(0.99,"HG_GB_PA_skill_008_start_loop2","Spine1",1.3,0,0,0,0,0,0,0,1,1)  
	
    Sound(0.010,"NB_TLKH_skill002start")
    
STake_Start("HG_GB_PA_attack_B_000_start2","HG_GB_PA_skill_022")  --����start2 
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("steady")    
    FrameTime(0.99,1)
    Loop()	
    PlaySpeed(0.9,0.98,0.7)		
    Fx(0.99,"HG_GB_PA_skill_008_start_loop2","Spine1",1.3,0,0,0,0,0,0,0,1,1)   	
    Sound(0.010,"NB_TLKH_skill002start")]]--

STake_Start("HG_GB_PA_attack_B_000","HG_GB_PA_skill_008")   -- �չ�0    17055  �����0.93   0.18~0.83    1.6
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
	
  	Fx(0.1,"HG_GB_PA_skill_008_start","Reference",1,0,0,0,0,0,0,0,1,0)	
  	Fx(0.94,"HG_GB_PA_skill_008_fire","Reference",1,0,0,-15,0,0,0,0,1.5,0,0,1) 
    HitGroundFx(0.93,"SFX_hitground",1.1,0,0,0)   		 		
    HitGroundFx(0.93,"SFX_hitground2",1.1,0,0,0)
    PlaySpeed(0,0.9,3)	
	Charge(0,0.3,170,0,12) 

  	AttackStart()
    Hurt(0.33,"hurt_71",0,1) 	
    Sound(0.010,"GB_New2_attack_003")
    Sound(0.927,"QQJ_Body_Impact")
    Sound(0.010,"HG_SoundVOC_Jump_1")
    Sound(0.889,"HG_SoundVOC_attack2_1")
    HurtSound(0.33,"QZ_PA_Skill_000_hit") 
    DirFx(0.33,"HG_GB_PA_attack_B_001_hit",25,1)	
    HurtBoneFx(0.33,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)	
	
	CameraShake(0.93,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	--ContralShake(0.93,0.55,0,0.9)		
	

STake_Start("HG_GB_PA_attack_B_001","HG_GB_PA_attack_B_001")   -- �չ�1    17051  �����0.8   0.7~1.05  1.1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
	
  	Fx(0,"HG_GB_PA_attack_B_001_start","Reference",1,0,0,0,0,0,0,0,1,0)	
  	Fx(0.78,"HG_GB_PA_attack_B_001_fire","",1.3,0,-40,-5,0,-30,0,0,1.2,0,0,1) 
  	Fx(0,"HG_GB_PA_attack_B_Shan_1","T_R",1,0,0,0,0,0,0,0,0.7)
    --MotionBlur(0.78,0.825,0.5)
	
    PlaySpeed(0,0.6,2)		
    Pause(0.81,0.1,1,0.09) 	--0.1
	Charge(0,0.15,30,20,12) --0.6
	
  	AttackStart()
    Hurt(0.5,"hurt_62",0,1) 	
    Sound(0.010,"GB_New2_attack_000")
    Sound(0.780,"GB_New2_attack_003")
    Sound(0.010,"HG_SoundVOC_attack4_1")
    Sound(0.780,"HG_SoundVOC_attack1_1")
    HurtSound(0.5,"BTS_CR_HIT_new_2_1") 
    DirFx(0.5,"HG_GB_PA_attack_B_001_hit",25,1)
    HurtBoneFx(0.5,"FX_Violent_blood_saxue1","Spine1",1.5,0,-30,0,90,0,0,1)	
	
	CameraShake(0.84,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")
	--ContralShake(0.84,0.25,0,0.75)	

STake_Start("HG_GB_PA_attack_B_002","HG_GB_PA_attack_B_002")   -- �չ�2    17052  �����0.8   0.7~1.05  1.1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
	
  	Fx(0.78,"HG_GB_PA_attack_B_002_fire","",1.3,0,55,-15,10,55,0,0,1.2,0,0,1) 
  	Fx(0,"HG_GB_PA_attack_B_002_start","Reference",1,0,0,0,0,0,0,0,1,0)		
  	Fx(0,"HG_GB_PA_attack_B_Shan_1","T_L",1,0,0,0,0,0,0,0,0.7)
	
    PlaySpeed(0,0.6,2)	
    Pause(0.81,0.1,1,0.09) 		
	Charge(0,0.15,30,20,12) --0.6
	
  	AttackStart()
    Hurt(0.5,"hurt_62",0,1) 	

    Sound(0.010,"GB_New2_attack_000")
    Sound(0.780,"GB_New2_attack_003")
    Sound(0.010,"HG_SoundVOC_attack4_1")
    Sound(0.880,"HG_SoundVOC_attack1_1")
    HurtSound(0.5,"BTS_CR_HIT_new_2_1")
    DirFx(0.5,"HG_GB_PA_attack_B_001_hit",25,1)
    HurtBoneFx(0.5,"FX_Violent_blood_saxue1","Spine1",1.5,0,-30,0,-90,0,0,1)	

    CameraShake(0.84,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")
	--ContralShake(0.84,0.25,0,0.75)	
	ContralShake(1.3,0.3,0,0.9)
	
STake_Start("HG_GB_PA_attack_B_003","HG_GB_PA_skill_005")   -- �չ�3    17053  �����0.93   0.7~1.05  1.1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
	
    PlaySpeed(0,0.3,2)
    --PlaySpeed(0.46,0.5,0.5)		
    PlaySpeed(0.5,0.9,2)		
	Fx(0,"HG_GB_PA_attack_B_003","",1.5,0,0,10,-6,0,0,0,1.3,0,0,1) 
	
  	Fx(0.25,"HG_GB_PA_attack_B_Shan_1","T_L",1,0,0,0,0,0,0,0,1)
  	Fx(0.25,"HG_GB_PA_attack_B_Shan_1","T_R",1,0,0,0,0,0,0,0,1)
    Sound(0.010,"GB_New2_attack_004")
    Sound(0.151,"HG_GB_PA_Skill_012")
    Sound(0.010,"HG_SoundVOC_attack4_1")
    Sound(0.8,"HG_SoundVOC_attack3_1")
    HitGroundFx(0.93,"SFX_hitground",1,0,-40,0)   		 		
    HitGroundFx(0.93,"SFX_hitground2",1,0,-40,0)
	
	Charge(0,0.03,30,20,12) --0.9  

  	AttackStart()
    Hurt(0.58,"hurt_62",0,1) 	
    HurtSound(0.58,"QZ_PA_Skill_000_hit") 
    DirFx(0.58,"HG_GB_PA_attack_B_001_hit",25,1)	
	HurtBoneFx(0.58,"FX_Violent_blood_Sharp_R3","Spine1",0.3,0,0,0,180,0,0)

    CameraShake(0.93,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")	
	--ContralShake(0.93,0.3,0,0.8)		

--[[STake_Start("HG_GB_PA_attack_B_004","HG_GB_PA_attack_B_003")   -- �չ�4    17054  �����0.84   0.7~1.05  1.1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
    --PlaySpeed(0.3,0.4,0.3)	
    PlaySpeed(0.5,0.6,0.8)	
    PlaySpeed(0.6,0.84,1.5)		
	Fx(0.15,"HG_GB_PA_attack_B_004","",1.6,0,0,0,-6,0,0,0,1)
	Fx(0.56,"HG_GB_PA_attack_B_004_long","",1.2,0,0,15,0,0,0,0,1.2,0,1) 	
  	--Fx(0.15,"HG_GB_PA_attack_B_Shan_1","T_R",1,0,0,0,0,0,0,0,0.7)	
  	--Fx(0.15,"HG_GB_PA_attack_B_Shan_1","T_L",1,0,0,0,0,0,0,0,0.7)
  	Fx(0.25,"HG_GB_PA_attack_B_Shan_1","T_R",1.5,0,0,0,0,0,0,0,0.55)	
  	Fx(0.25,"HG_GB_PA_attack_B_Shan_1","T_L",1.5,0,0,0,0,0,0,0,0.55)
    Sound(0.010,"GB_New2_attack_004")
    Sound(0.010,"NB_TLKH_skill002start")
    Sound(0.750,"GB_New2_skill_002")
    Sound(0.010,"HG_SoundVOC_attack3_1")
    Sound(0.879,"HG_SoundVOC_attack3_1")
	
	Charge(0,0.16,30,20,12) --0.6   

  	AttackStart()
    Hurt(0.84,"hurt_62",0,1) 	
    HurtSound(0.84,"QZ_PA_Skill_000_hit") 
    DirFx(0.84,"HG_GB_PA_attack_B_001_hit",25,1)	
    HurtBoneFx(0.84,"FX_Violent_blood_Sharp_R4","Spine1",0.7,0,0,0,180,0,0)
	
    CameraShake(0.84,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	ContralShake(0.76,0.65,0,0.9)]]--
	
	
STake_Start("HG_GB_PA_attack_B_004_1","HG_GB_PA_attack_B_003_new")   -- �չ�4��    17673
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")

	Fx(0,"HG_GB_PA_attack_B_003_new","Reference",1,0,0,0,0,0,0,0,1,1)
	Fx(0.3,"HG_GB_PA_attack_B_004_long_a","Reference",1,0,0,0,0,0,5,0,1.2)
	Fx(0.5,"HG_GB_PA_attack_B_004_long_b","Reference",1,0,0,0,0,0,5,0,1.2)
    Sound(0.23,"GB_New2_attack_000")
    Sound(0.23,"HG_GB_PA_skill_017")
    Sound(0.551,"GB_New2_attack_003")
    Sound(0.010,"HG_SoundVOC_attack4_1")
	
  	AttackStart()
    Hurt(0.5,"hurt_62",0,1) 	
    Hurt(0.7,"hurt_62",0,1) 	
    HurtSound(0.5,"QZ_PA_Skill_000_hit") 
    HurtSound(0.7,"QZ_PA_Skill_000_hit") 	
    DirFx(0.5,"HG_GB_PA_attack_B_001_hit",25,1)
    DirFx(0.7,"HG_GB_PA_attack_B_001_hit",25,1)	
    HurtBoneFx(0.5,"FX_Violent_blood_PT","Spine1",0.8,0,0,0,0,0,0)
    HurtBoneFx(0.7,"FX_Violent_blood_PT","Spine1",0.8,0,0,0,0,0,0)	
	CameraShake(0.5,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")
	CameraShake(0.7,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	ContralShake(0.5,0.25,0,0.9)		
	ContralShake(0.7,0.3,0,0.9)	
	
STake_Start("HG_GB_PA_attack_B_JA_001","HG_GB_PA_skill_003_combo3")  --JA1   17056  �����0.575   0.6~1     1.1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("steady")       		

    --PlaySpeed(0.28,0.31,0.2)	
	
	Charge(0,0.06,30,20,12)	--0.51  
	
  	Fx(0.575,"HG_GB_PA_skill_003_combo3_b","Reference",1.2,0,0,12,0,0,0,0,1,0,0,1) 
	
  	Sound(0.090,"HG_GB_PA_Skill_015")  	
  	Sound(0.011,"HG_SoundVOC_attack3_1")
	Sound(0.3,"HG_GB_ST_Skill_015_3")
    GroundSound(0.555,"Sound_fall")	
    HitGroundFx(0.575,"SFX_hitground")
  	HitGroundFx(0.575,"SFX_hitground2") 	
	
  	AttackStart()
  	Hurt(0.575,"hurt_71")
  	HurtSound(0.575,"PA_Hit_004")  	
    DirFx(0.575,"HG_GB_PA_attack_B_001_hit",25,1)
	HurtBoneFx(0.575,"FX_Violent_blood_PT","Spine1",0.8,0,0,0,0,0,0)

    CameraShake(0.57,"ShakeMode = 3","ShakeTimes = 0.33","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	--ContralShake(0.57,0.3,0,0.8)	
	
STake_Start("HG_GB_PA_attack_B_JA_002","HG_GB_PA_skill_007")  --JA2   17058  0.6|0.7|0.8|0.9|1     
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("steady")       		
    
  	Fx(0.000,"HG_GB_PA_skill_007_fire","",1.1,1,0,-12,5,0,0,0,1,0)  	
  	Fx(0.000,"HG_GB_PA_skill_007_fire","",1,1,0,-5,-15,0,0,0,1,0,0) 	
  	Fx(0.55,"HG_GB_PA_skill_007_long","Reference",1.2,0,0,-18,0,0,0,0,1.2)  
	Sound(0.010,"HG_SoundVOC_attack5_1")  		
  	Sound(0.005,"HG_GB_PA_Skill_012") 
    Sound(0.463,"HG_SoundVOC_attack3_1")
  	Sound(0.010,"HG_GB_PA_Skill_003") 
    GroundSound(1.643,"Sound_fall")    	  	  	
    
    --HitGroundFx( 0.56 ,"SFX_hitground")
  	--HitGroundFx(0.56,"SFX_hitground2")
    PlaySpeed(1.5,1.64,1.8)
	
    Charge(0,0.2,30,20,12) --0.56  
    Pause(0.85,0.1,1,0.2) 
    Pause(0.89,0.1,1,0.2) 
	
    AttackStart()
    Hurt(0.6,"hurt_21",0,1)
    Hurt(0.7,"hurt_21",0,1)
    Hurt(0.8,"hurt_81",0,1)
    Hurt(0.94,"hurt_81",0,1)    
    Hurt(1,"hurt_81",0,1)    
    DirFx(0.6,"HG_GB_PA_attack_B_001_hit",25,1)  
    AttackHurtFx(0.7,"HG_GB_PA_attack_B_001_hit","Hips",1)   
    AttackHurtFx(0.8,"HG_GB_PA_attack_B_001_hit","Hips",1)   
    AttackHurtFx(0.94,"HG_GB_PA_attack_B_001_hit","Hips",1)      
    AttackHurtFx(1,"HG_GB_PA_attack_B_001_hit","Hips",1)   
    HurtBoneFx(0.7,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.8,"","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.94,"","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(1,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)	
   	HurtSound(0.6,"PA_Hit_002")    
  	HurtSound(0.7,"PA_Hit_002")    
  	HurtSound(0.8,"PA_Hit_002")    
  	HurtSound(0.94,"PA_Hit_002")    
  	HurtSound(1,"PA_Hit_002")  

    CameraShake(0.56,"ShakeMode = 3","ShakeTimes = 0.5","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")	
    --CameraShake(0.56,"ShakeMode = 8","ShakeSpeed = 400","ShakeAmplitudeX = 4","ShakeAmplitudeY = 4","ShakeAmplitudeZ = 10","ShakeTimes = 0.1")	
	ContralShake(0.6,0.3,0,0.9)
	ContralShake(0.85,0.3,0,0.9)
	ContralShake(0.89,0.65,0,0.9)
	
	
--[[STake_Start("HG_GB_PA_attack_B_JA_001_C","HG_GB_PA_attack_B_JA_001")  --�Ҽ�JA��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("steady")  	
	
  	Fx(0.75,"HG_GB_PA_attack_B_JA_001","Reference",1,0,0,0,0,0,0,0,0.75,0,0)  
	
    Charge(0,0.1,30,20,12) --0.63~0.73  
	
    AttackStart()
    Hurt(0.75,"hurt_71",0,1)
    Hurt(0.85,"hurt_71",0,1)
    Hurt(0.95,"hurt_71",0,1)
    DirFx(0.75,"HG_GB_PA_attack_B_001_hit",25,1)  
    DirFx(0.85,"HG_GB_PA_attack_B_001_hit",25,1) 
    DirFx(0.95,"HG_GB_PA_attack_B_001_hit",25,1)  	
    HurtBoneFx(0.75,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.85,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.95,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
   	HurtSound(0.75,"PA_Hit_002")    
  	HurtSound(0.85,"PA_Hit_002")    
  	HurtSound(0.95,"PA_Hit_002")    

    CameraShake(0.75,"ShakeMode = 3","ShakeTimes = 0.33","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
    CameraShake(0.85,"ShakeMode = 3","ShakeTimes = 0.33","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")	
    CameraShake(0.95,"ShakeMode = 3","ShakeTimes = 0.5","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")]]--




--------------------------------------------------------------����------------------------------------------------------------------------

STake_Start("HG_GB_PA_skill_040","HG_GB_PA_skill_040_002")  --��Ӱȭ����δ��   17653
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("rush_attack") 	
    Channel(0, "2")    
    StopChannel(0,0,0)	
    Loop()
	Fx(0.01,"HG_GB_PA_skill_040_002_a","Reference",1,1,0,0,0,0,0,0,1,1)
	Fx(1,"HG_GB_PA_skill_040_002_a","Reference",1,1,0,0,0,0,0,0,1,1)
	Fx(2,"HG_GB_PA_skill_040_002_a","Reference",1,1,0,0,0,0,0,0,1,1)	

STake_Start("HG_GB_PA_skill_040_1","HG_GB_PA_skill_040_002")  --��Ӱȭ������   17653
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("rush_attack") 	
    Channel(0, "2")    
    StopChannel(0,0,0)	
    Loop(0.1,3)
	Fx(0,"HG_GB_PA_skill_040_002_fire","Reference",1.2,1,0,0,0,0,0,0,1,1)	
	Fx(0.1,"HG_GB_PA_skill_040_002_b","Reference",1,1,0,0,0,0,0,0,1,1)		
	Fx(1,"HG_GB_PA_skill_040_002_b","Reference",1,1,0,0,0,0,0,0,1,1)	
	Fx(2,"HG_GB_PA_skill_040_002_b","Reference",1,1,0,0,0,0,0,0,1,1)		
	
STake_Start("HG_GB_PA_skill_040_001","HG_GB_PA_skill_040_001")   -- ��ץ����С   17654    ������0.18����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
    StopChannel(0,2,0)		
	Sound(0.1,"HG_SoundVOC_attack1_1")
	Sound(0.12,"GB_New2_attack_004")
    HitGroundFx(0.22,"SFX_hitground",0.8,0,-40,0)   		 		
    HitGroundFx(0.22,"SFX_hitground2",0.8,0,-40,0)
    HitGroundFx(0.22,"SFX_hitground",0.8,0,-70,0)   		 		
    HitGroundFx(0.22,"SFX_hitground2",0.8,0,-70,0)
	
    Fx(0.1,"HG_GB_PA_skill_040_001","Reference",1,1,0,0,0,0,0,0,1,1)

	Charge(0.1,0.1,30,15,12)  --0.14~0.17

    Pause(0.2,0.15,1,0.005) 	
	
  	AttackStart()
    Hurt(0.22,"hurt_21",0,1) 
  	HurtSound(0.22,"PA_Hit_004") 
    DirFx(0.22,"HG_GB_PA_attack_B_001_hit",25,1)
	HurtBoneFx(0.22,"FX_Violent_blood_Sharp_R3","Spine1",0.4,0,0,0,180,0,0)
	
	CameraShake(0.2,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")


STake_Start("HG_GB_PA_skill_040_0011","HG_GB_PA_skill_040_001")   -- ��ץ���δ�       ������0.18����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
    StopChannel(0,2,0)		
	Sound(0.1,"GB_New2_attack_003")
	Sound(0.1,"HG_SoundVOC_attack3_1")
    HitGroundFx(0.22,"SFX_hitground",0.8,0,-40,0)   		 		
    HitGroundFx(0.22,"SFX_hitground2",0.8,0,-40,0)
    HitGroundFx(0.22,"SFX_hitground",0.8,0,-70,0)   		 		
    HitGroundFx(0.22,"SFX_hitground2",0.8,0,-70,0)
	
    Fx(0.1,"HG_GB_PA_skill_040_001_a","Reference",1,1,0,0,0,0,0,0,1,1)
    Fx(0.1,"HG_GB_PA_skill_040_001_a","Reference",1,1,0,0,0,20,0,0,1,1)
    Fx(0.1,"HG_GB_PA_skill_040_001_a","Reference",1,1,0,0,0,-20,0,0,1,1)	
    Fx(0.1,"HG_GB_PA_skill_040_001_b","Reference",1,1,0,0,0,0,0,0,1,1)	

	Charge(0.1,0.1,30,15,12)  --0.14~0.17

    Pause(0.2,0.15,1,0.005) 	
	
    --LinkFx(0,"NB_TY_skill_005_fire_loop",0.5,"T_R","Spine1",1,0.1)	
  	AttackStart()
    Hurt(0.22,"hurt_21",0,1) 
  	HurtSound(0.22,"PA_Hit_004") 
    DirFx(0.22,"HG_GB_PA_attack_B_001_hit",25,1)
	HurtBoneFx(0.22,"FX_Violent_blood_Sharp_R3","Spine1",0.4,0,0,0,180,0,0)
	
	CameraShake(0.2,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")


STake_Start("HG_GB_PA_skill_036","HG_GB_PA_skill_036")     -------��ץ�� 17655
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
	
	Sound(0.03,"HG_SoundVOC_attack4_1")
	Sound(0.1,"HG_GB_PA_skill_005")
	Sound(0.08,"HG_GB_PA_Skill_000_Combo1")
	Sound(0.46,"GB_New2_attack_000")

	PlaySpeed(0,0.4,2)
    Fx(0,"HG_GB_PA_skill_040_001_b","Reference",0.7,1,0,-5,6,0,0,0,1,1)	
    Fx(0,"HG_GB_PA_skill_036","Reference",1.5,1,0,0,-15,0,0,0,1.2,1)

    Charge(0,0.15,20,0,21,0,0) --0.2

    AttackStart()
    Hurt(0.2,"hurt_21",0,2)
	DirFx(0.2,"HG_SL_ST_skill_016_hit",25,1) 
	HurtBoneFx(0.2,"FX_blood_siyao_c","Spine1",1,0,0,0,0,0,0)
    HurtSound(0.2,"BTS_CR_HIT_new_s1")
	
    CameraShake(0.65,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")
	
--[[STake_Start("HG_GB_PA_skill_023","HG_GB_PA_skill_023")   -- ��˪����������1��   17500    ������0.18����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	Sound(0.1,"GB_New2_attack_000")
    HitGroundFx(0.22,"SFX_hitground",0.8,0,-40,0)   		 		
    HitGroundFx(0.22,"SFX_hitground2",0.8,0,-40,0)
    HitGroundFx(0.22,"SFX_hitground",0.8,0,-70,0)   		 		
    HitGroundFx(0.22,"SFX_hitground2",0.8,0,-70,0)
	
	Fx(0,"HG_GB_PA_skill_023","Reference",1,0,0,0,0,0,0,0,1.2,1) 	
	Charge(0,0.03,20,15,12)  --0.14~0.17

	PlaySpeed(0.17,0.2,0.6)
    Pause(0.2,0.15,1,0.005) 	
	
    --LinkFx(0,"NB_TY_skill_005_fire_loop",0.5,"T_R","Spine1",1,0.1)	
  	AttackStart()
    Hurt(0.22,"hurt_21",0,1) 
  	HurtSound(0.22,"PA_Hit_004") 
    DirFx(0.22,"HG_GB_PA_attack_B_001_hit",25,1)
	HurtBoneFx(0.22,"FX_Violent_blood_Sharp_R3","Spine1",0.4,0,0,0,180,0,0)
	
	CameraShake(0.2,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")

STake_Start("HG_GB_PA_skill_023_Q1","HG_GB_PA_skill_023_Q1")   -- ����ȭ1��Q1��   17113
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"5")     
    BlendPattern("steady")	

  	Fx(0,"HG_GB_PA_skill_014_start","",1.5,0,0,0,0,0,0,0,1,1)	
  	Fx(0,"HG_GB_PA_skill_023_Q1","",0.9,0,0,0,0,0,0,0,1,1) 
	
    Sound(0.010,"HG_GB_PA_Skill_000_Combo1")
    Sound(0.059,"GB_HG_New2_skillVOC_000_1")
    Sound(0.174,"HG_SL_ST_attack_003")
    Sound(0.374,"HG_SL_ST_attack_003")
    Sound(0.403,"HG_SL_ST_attack_003")
    Sound(0.516,"HG_SL_ST_attack_003")
    Sound(0.544,"HG_SL_ST_attack_003")
    Sound(0.573,"HG_SL_ST_attack_003")
    Sound(0.601,"HG_SL_ST_attack_003")
    Sound(0.658,"HG_SL_ST_attack_003")
    Sound(0.714,"HG_SL_ST_attack_003")
    Sound(0.852,"HG_SL_ST_attack_003")
    Sound(0.913,"HG_SL_ST_attack_003")
    Sound(0.998,"HG_SL_ST_attack_003")
    Sound(1.224,"HG_SL_ST_attack_003")
    Sound(1.281,"HG_SL_ST_attack_003")
    Sound(1.330,"GB_New2_attack_005")
	
	--0.32|0.357|0.4|0.44|0.462|0.504|0.546|0.63|0.672|0.693|0.735|0.777|0.798|0.84|0.945|0.966|1|1.029|1.07|1.155|1.197|1.239|1.26|1.3|1.323|1.386
	Charge(0,0.105,20,15,12)  --0.546~0.651
	
  	AttackStart()
    Hurt(0.32,"hurt_81",0,1) 
    Hurt(0.462,"hurt_81",0,1) 
    Hurt(0.546,"hurt_81",0,1)
	
    Hurt(0.63,"hurt_21",0,1)
    Hurt(0.735,"hurt_21",0,1)
    Hurt(0.84,"hurt_21",0,1) 
	
    Hurt(0.945,"hurt_21",0,1)
    Hurt(1.029,"hurt_21",0,1) 
    Hurt(1.155,"hurt_21",0,1)
	
    Hurt(1.197,"hurt_21",0,1)
    Hurt(1.26,"hurt_21",0,1)
    Hurt(1.323,"hurt_21",0,1)
  	HurtSound(0.32,"HG_GB_ST_Skill_005_Hit") 
  	HurtSound(0.63,"HG_GB_ST_Skill_005_Hit") 
  	HurtSound(0.945,"HG_GB_ST_Skill_005_Hit") 
  	HurtSound(1.197,"HG_GB_ST_Skill_005_Hit") 
    DirFx(0.32,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(0.462,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(0.546,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(0.63,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(0.735,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(0.945,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(1.029,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(1.155,"HG_GB_PA_attack_B_001_hit",25,1)	  
    DirFx(1.197,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(1.26,"HG_GB_PA_attack_B_001_hit",25,1)		
    DirFx(1.323,"HG_GB_PA_attack_B_001_hit",25,1)	
	HurtBoneFx(0.32,"FX_Violent_blood_PT","Spine1",1,0,0,0,180,0,0)
	HurtBoneFx(0.63,"FX_Violent_blood_PT","Spine1",1,0,0,0,180,0,0)
	HurtBoneFx(0.945,"FX_Violent_blood_PT","Spine1",1,0,0,0,180,0,0)
	HurtBoneFx(1.197,"FX_Violent_blood_PT","Spine1",1,0,0,0,180,0,0)
	
	ContralShake(0.32,0.25,0,0.9)
	ContralShake(0.63,0.25,0,0.9)
	ContralShake(0.945,0.25,0,0.9)
	ContralShake(1.197,0.25,0,0.9)	
	
STake_Start("HG_GB_PA_skill_023_Q2","HG_GB_PA_skill_023_Q2")   -- ����ȭ2��Q2��   17505
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"5")     
    BlendPattern("steady")	

  	Fx(0,"HG_GB_PA_skill_023_Q2","",1,0,0,0,0,0,0,0,1,1) 	
	Sound(0.3,"GB_New2_attack_003")
	
	Charge(0,0.063,20,15,12)  --0.27~0.333
	
  	AttackStart()	
    Hurt(0.35,"hurt_81",0,1)	
	DirFx(0.35,"HG_GB_PA_attack_B_001_hit",25,1)		
	HurtSound(0.35,"BTS_CR_HIT_new_2_1") 
	HurtBoneFx(0.35,"FX_Violent_blood_PT","Spine1",1,0,0,0,180,-60,0,1)
	
	CameraShake(0.35,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	ContralShake(0.35,0.4,0,0.9)

STake_Start("HG_GB_PA_skill_023_E","HG_GB_PA_skill_023_E")   -- ����ȭ��E��   17510   0.64
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"5")     
    BlendPattern("steady")	
  	Fx(0.1,"HG_GB_PA_skill_023_E+","",1,0,0,-20,0,0,0,0,1.2,1,0,1) 
	Sound(0.2,"HG_GB_PA_skill_008")
	Sound(0.5,"HG_SoundVOC_attack3_1")
    HitGroundFx(0.64,"SFX_hitground",0.6,0,-40,0)   		 		
    HitGroundFx(0.64,"SFX_hitground2",0.6,0,-40,0)
    HitGroundFx(0.64,"SFX_hitground",0.6,0,-60,0)   		 		
    HitGroundFx(0.64,"SFX_hitground2",0.6,0,-60,0)
	
	Charge(0,0.48,40,15,12)  --0.26~0.64
	
  	AttackStart()	
    Hurt(0.64,"hurt_71",0,1)	
	DirFx(0.64,"HG_GB_PA_attack_B_001_hit",25,1)		
	HurtSound(0.64,"BTS_CR_HIT_new_2_1") 
	HurtBoneFx(0.64,"FX_Violent_blood_Sharp_R3","Spine1",0.3,0,0,0,180,15,0)
	
	CameraShake(0.64,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	ContralShake(0.64,0.4,0,0.9)]]--

--------------------------------------------------------------


STake_Start("HG_GB_PA_skill_024","HG_GB_PA_skill_024")   -- ��Ծ��Ԩ ������2��  17515   ������0.23|0.754
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	Fx(0,"HG_GB_PA_skill_024","Reference",1.2,0,0,0,0,0,0,0,1,1)
	Sound(0.1,"HG_GB_PA_skill_008")
	Sound(0.03,"HG_SoundVOC_attack5_1")
	
	Charge(0,0.45,160,15,12)  --0~0.2
	
    Pause(0.24,0.16,0,0.08)
  	AttackStart()	
    Hurt(0.24,"hurt_21",0,4)	
    Hurt(0.754,"hurt_71",0,4)	
	DirFx(0.24,"HG_GB_PA_attack_B_001_hit",25,1)		
	DirFx(0.754,"HG_GB_PA_attack_B_001_hit",25,1)		
	HurtSound(0.24,"BTS_CR_HIT_new_2_1") 
	HurtSound(0.754,"BTS_CR_HIT_new_2_1") 
    HurtBoneFx(0.252,"FX_blood_quan_b","Head",1,0,0,0,-90,0,0)
	
	CameraShake(0.594,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")

STake_Start("HG_GB_PA_skill_024_Q1","HG_GB_PA_skill_024_Q1")   -- �ȵذ�1��Q1)   17520   0.6
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	Fx(0,"HG_GB_PA_skill_024_Q1","Reference",1.1,0,0,12,0,0,0,0,1,1)
	Sound(0.2,"HG_GB_ST_Skill_002")
	Sound(0.5,"HG_SoundVOC_attack3_1")
    HitGroundFx(0.6,"SFX_hitground",1.2,0,0,0)   		 		
    HitGroundFx(0.6,"SFX_hitground2",1.2,0,0,0)
	
  	AttackStart()	
    Hurt(0.6,"hurt_81",0,2)	
	DirFx(0.6,"HG_GB_PA_attack_B_001_hit",25,1)		
	HurtSound(0.6,"BTS_CR_HIT_new_2_1") 
    HurtBoneFx(0.6,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
	
	CameraShake(0.6,"ShakeMode = 3","ShakeTimes = 0.33","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	ContralShake(0.6,0.3,0,0.9)
	
STake_Start("HG_GB_PA_skill_024_Q2","HG_GB_PA_skill_024_Q2")   -- �ȵذ�2��Q2)   17525   0.83
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	Sound(0.1,"HG_GB_ST_Skill_014")
	Sound(0.561,"HG_GB_ST_Skill_015_3")
	Sound(0.77,"HG_SoundVOC_attack3_1")
    HitGroundFx(0.83,"SFX_hitground",1.2,0,0,0)   		 		
    HitGroundFx(0.83,"SFX_hitground2",1.2,0,0,0)
	
    Charge(0,0.2,40,15,12)
	
	Fx(0,"HG_GB_PA_skill_024_Q2","Reference",1.3,0,0,0,0,0,0,0,1,1)	
	
  	AttackStart()	
    Hurt(0.83,"hurt_81",0,2)	
	DirFx(0.83,"HG_GB_PA_attack_B_001_hit",25,1)		
	HurtSound(0.83,"BTS_CR_HIT_new_2_1") 
    HurtBoneFx(0.83,"FX_Violent_blood_Blunt_R2","Spine1",0.4,0,0,0,0,0,0)
	
	CameraShake(0.83,"ShakeMode = 3","ShakeTimes = 0.5","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	ContralShake(0.83,0.4,0,0.9)
	
--------------------------------------------------------------	
	
STake_Start("HG_GB_PA_skill_014_001","HG_GB_PA_skill_014_001")   -- ������    17660      
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"5")     
    BlendPattern("steady") 
    PlaySpeed(0,0.3,3)
    	
    --Charge(0,0.2,100,10,8) --0.1~0.4
    Charge(0,0.2,80,0,16)

	Sound(0.1,"HG_GB_PA_Skill_000_Combo1")
	Sound(0.1,"GB_New2_attack_000")
	Sound(0.17,"HG_SoundVOC_attack4_1")
	
  	Fx(0,"HG_GB_PA_skill_014_001_a","Reference",1,0,0,0,0,0,0,0,1,1)   
  	Fx(0,"HG_GB_PA_skill_014_001_a","Reference",1,0,0,0,0,0,0,0,2,1)   	
  	Fx(0.6,"HG_GB_PA_skill_014_001_b","Reference",1,0,0,0,0,0,0,0,1,1)   
	
	AttackStart()
    Hurt(0.1,"hurt_62",0,1) 
    Hurt(0.2,"hurt_62",0,1) 	
    Hurt(0.3,"hurt_81",0,1) 	
    HurtSound(0.1,"PA_Hit_002") 
    HurtSound(0.2,"PA_Hit_002") 
    HurtSound(0.3,"PA_Hit_002") 	
    AttackHurtFx(0.1,"HG_GB_PA_attack_A_hit","Spine1",1)
    AttackHurtFx(0.2,"HG_GB_PA_attack_A_hit","Spine1",1)
    AttackHurtFx(0.3,"HG_GB_PA_attack_A_hit","Spine1",1)	
    HurtBoneFx(0.1,"FX_Violent_blood_saxue1","Spine1",1,0,0,0,0,0,0)	 
    HurtBoneFx(0.2,"FX_Violent_blood_saxue1","Spine1",1,0,0,0,0,0,0)	 
    HurtBoneFx(0.3,"FX_Violent_blood_saxue1","Spine1",1,0,0,0,0,0,0)	 	
    	
    CameraShake(0.45,"ShakeMode = 3","ShakeTimes = 0.25","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")

	
STake_Start("HG_GB_PA_skill_024_E","HG_GB_PA_skill_024_E")   -- ������β��E��   17530   ������0.3|0.48
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	PlaySpeed(0,0.3,3)
	Sound(0.01,"HG_SoundVOC_attack3_1")
	Sound(0.01,"HG_GB_ST_Skill_015_1")
	Sound(0.2,"GB_New2_attack_000")
	Sound(0.35,"GB_New2_attack_000")
	Sound(0.4,"GB_New2_attack_003")
	
    CameraShake(0.315,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")	
    CameraShake(0.48,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")
	ContralShake(0.315,0.3,0,0.9)
	ContralShake(0.48,0.3,0,0.9)
	
--------------------------------------------------------------

	
--[[STake_Start("HG_GB_PA_skill_025B_start","HG_GB_PA_skill_025B")   -- ���򴥷�������3������   17535   
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")		
	FrameTime(0,0.3)
	
	
STake_Start("HG_GB_PA_skill_025A","HG_GB_PA_skill_025A")   -- ���򴥷�������3��fire1   17536   �����������0.15��   Softʱ��0.47��
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"5")     
    BlendPattern("steady")	
	Sound(0.1,"GB_New2_attack_003")
	
	Fx(0.35,"HG_GB_PA_skill_025A_a","Reference",1,0,0,-8,0,0,0,0,1,1)
	
	PlaySpeed(0,0.4,2)
	Charge(0,0.05,20,15,12)  --0.15~0.2
	Pause(0.4,0.16,1,0.005) 

	AttackStart()	
    Hurt(0.2,"hurt_21",0,1)	
	DirFx(0.2,"HG_GB_PA_attack_B_001_hit",25,1)		
	HurtSound(0.2,"BTS_CR_HIT_new_2_1") 
    HurtBoneFx(0.2,"FX_Violent_blood_saxue1","Spine1",1.2,0,0,0,180,-15,0)
	
	CameraShake(0.4,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")]]--
	
	
STake_Start("HG_GB_PA_skill_025A_Q1","HG_GB_PA_skill_025A_Q1")   -- ��ͷˤ1��Q1��   17540   
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")		
	PlugIn(0.21)
    PlugIn(0.21)
    PlugIn(0.21)	
	
	Charge(0,0.21,60,15,12)
    Fx(0,"HG_GB_PA_skill_019_fire1","Reference",1,0,0,3,2,0,0,0,2,1)
    --Fx(0,"HG_GB_PA_skill_025A_Q1_a","Reference",0.8,0,0,-15,8,0,0,0,1,1)
    Fx(0.4,"HG_GB_PA_skill_025A_Q1_b","Reference",1.2,0,0,20,0,0,0,0,1.5,1)	
	Sound(0.1,"HG_GB_PA_skill_005")
	Sound(0.17,"HG_SoundVOC_Jump_1")
	Sound(0.77,"HG_SoundVOC_attack2_1")
    Sound(0.72,"QQJ_Body_Impact")
    HitGroundFx(0.72,"SFX_hitground",1,0,-30,0)   		 		
    HitGroundFx(0.72,"SFX_hitground2",1,0,-30,0)
	
	Pause(0.21,0.1,0,0.09)
	
    AttackStart()
	Hurt(0,"hurt_81",0,2)	
    --Hurt(0.72,"hurt_81",0,2)	
    AttackHurtFx(0,"","Spine1",1) 		
    --AttackHurtFx(0.72,"HG_GB_PA_attack_B_001_hit","Spine1",1) 	
	HurtBoneFx(0,"","Spine1",1,0,0,0,0,0,0)	
	--HurtBoneFx(0.72,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
   	HurtSound(0.02,"BTS_CR_HIT_new_2_1") 	
   	--HurtSound(0.72,"BTS_CR_HIT_new_2_1") 	
    CameraShake(0.72,"ShakeMode = 3","ShakeTimes = 0.45","ShakeFrequency = 0.033","ShakeAmplitudeX = 6","ShakeAmplitudeY = 6","ShakeAmplitudeZ = 8")
	ContralShake(0.72,0.3,0,0.9)

STake_Start("HG_GB_PA_skill_025A_Q2","HG_GB_PA_skill_025A_Q2")   -- ��ͷˤ2��Q2��   17543   
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	
	Pause(0.05,0.1,0,0.09)	
    Fx(0,"HG_GB_PA_skill_025A_Q1_a","Reference",1,0,0,10,2,0,0,0,1.5,1)	
    Fx(0.12,"HG_GB_PA_skill_025A_Q2_a","Reference",1.2,0,0,-10,0,0,0,0,1,1)
	Fx(1,"HG_GB_PA_skill_025A_Q2_c","Reference",1,0,0,0,0,0,0,0,1,1)	
	Fx(1.1,"HG_GB_PA_skill_025A_Q2_b","Reference",1.3,0,0,15,0,0,0,0,1,1)
	--Fx(0.12,"HG_GB_PA_skill_019_fire2","Reference",1,0,0,0,0,0,0,0,1,1)	
	--Fx(1.1,"HG_GB_PA_skill_019_fire4","Reference",1,0,0,0,0,0,0,0,1,1)	
    HitGroundFx(0.39,"SFX_hitground",0.8,0,40,0)   		 		
    HitGroundFx(0.39,"SFX_hitground2",0.8,0,40,0)
    HitGroundFx(1.11,"SFX_hitground",1.2,0,-60,0)   		 		
    HitGroundFx(1.11,"SFX_hitground2",1.2,0,-60,0)	
	PlugIn(0.05)
    PlugIn(0.05)
    PlugIn(0.05)	
	--[[PlugOut(1.1)	
	PlugOut(1.1)	
	PlugOut(1.1)	
	PlugOut(1.1)]]--	
	
	
    AttackStart()
	Hurt(0,"hurt_21",0,2)
    Hurt(0.39,"hurt_81",0,2)
    --Hurt(1.11,"hurt_81",0,2)	
    AttackHurtFx(0,"","Spine1",1) 	
    AttackHurtFx(0.39,"HG_GB_PA_attack_B_001_hit","Spine1",1) 
    --AttackHurtFx(1.11,"HG_GB_PA_attack_B_001_hit","Spine1",1) 	
	HurtBoneFx(0,"","Spine1",1,0,0,0,0,0,0)	
	HurtBoneFx(0.39,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
	--HurtBoneFx(1.11,"FX_Violent_blood_Blunt_R2","Spine1",0.6,0,0,0,0,0,0)
   	HurtSound(0.02,"BTS_CR_HIT_new_2_1") 	
   	HurtSound(0.39,"BTS_CR_HIT_new_2_1") 
   	--HurtSound(1.11,"BTS_CR_HIT_new_2_1") 	

    Sound(0.2,"HG_SoundVOC_attack4_1")
    Sound(0.9,"HG_SoundVOC_attack3_1")
	Sound(0.02,"HG_GB_ST_Skill_015_3")
    Sound(1.11,"QQJ_Body_Impact")

    Sound(0.39,"GB_New2_attack_003")
    Sound(1.11,"GB_New2_attack_004")


	
    CameraShake(0.39,"ShakeMode = 3","ShakeTimes = 0.45","ShakeFrequency = 0.033","ShakeAmplitudeX = 6","ShakeAmplitudeY = 6","ShakeAmplitudeZ = 8")	
    CameraShake(1.11,"ShakeMode = 3","ShakeTimes = 0.6","ShakeFrequency = 0.033","ShakeAmplitudeX = 8","ShakeAmplitudeY = 8","ShakeAmplitudeZ = 8")
	
	ContralShake(0.39,0.3,0,0.9)
	ContralShake(1.11,0.65,0,0.9)		
	
	
	
--[[STake_Start("HG_GB_PA_skill_025A_E","HG_GB_PA_skill_025A_E")   -- ��ʯͷ��E��   17545   
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	


    PlugIn(0.21)	
	PlugOut(0)	
	Pause(0.21,0.1,0,0.09)	
	Charge(0,0.21,60,15,12)	
    Fx(0,"HG_GB_PA_skill_019_fire1","Reference",1,0,0,3,2,0,0,0,2,1)
    Fx(0,"HG_GB_PA_skill_025A_E_fire","Reference",1,0,0,0,0,0,0,0,1)
	Sound(0.01,"GB_New2_attack_005")
	Sound(0.57,"GB_New2_skill_002")
	Sound(0.2,"HG_GB_ST_Skill_015_3")
    Sound(0.55,"HG_SoundVOC_attack3_1")
	
    AttackStart()
    Hurt(0.6,"hurt_81",0,1)	
    AttackHurtFx(0.6,"HG_GB_PA_attack_B_001_hit","Spine1",1) 	
	HurtBoneFx(0.6,"FX_Violent_blood_Blunt_R2","Spine1",0.4,0,0,0,0,0,0)	
   	HurtSound(0.6,"BTS_CR_HIT_new_2_1") 
	
	CameraShake(0.94,"ShakeMode = 3","ShakeTimes = 0.6","ShakeFrequency = 0.033","ShakeAmplitudeX = 6","ShakeAmplitudeY = 6","ShakeAmplitudeZ = 8")
	ContralShake(0.94,0.65,0,0.9)	
	
STake_Start("HG_GB_PA_skill_025B_fire","HG_GB_PA_skill_025B")   -- ���򴥷�������3��fire2   17537   �����������0.18��
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"5")     
    BlendPattern("steady")	
	FrameTime(0.3,1.233)
	Sound(0.4,"GB_New2_attack_005")
	Sound(0.49,"HG_SoundVOC_attack3_1")
	
	Fx(0.5,"HG_GB_PA_skill_025B_a","Reference",1,0,0,-8,4,0,0,0,1,1)
	
	Charge(0,0.05,20,15,12)  --0.18~0.23
    Pause(0.567,0.16,1,0.005) 
	
	AttackStart()	
    Hurt(0.23,"hurt_81",0,1)	
	DirFx(0.23,"HG_GB_PA_attack_B_001_hit",25,1)		
	HurtSound(0.23,"BTS_CR_HIT_new_2_1") 
    HurtBoneFx(0.23,"FX_Violent_blood_saxue1","Spine1",1.2,0,0,0,180,-15,0)
	
	CameraShake(0.53,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")

	
STake_Start("HG_GB_PA_skill_025B_Q","HG_GB_PA_skill_025B_Q")  -- �����䣨Q��   17550
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")		
	
    Fx(0.1,"HG_GB_PA_skill_025B_Q_a","",1,0,0,0,0,0,0,0,1.8)
    --Fx(0.43,"HG_GB_PA_skill_025B_Q_b","",1,0,0,0,0,0,0,0,1)
    PlaySpeed(0,0.25,0.5)
	
	PlugIn(0.05)
	PlugIn(0.05)
	PlugIn(0.05)	
	Charge(0,0.05,40,0,14,0,0.42,70)	
	Charge(0.01,0.2,0,0,14,0,0,-70)	
	Charge(0.011,0.45,40,180,16) 
	
    Sound(0.1,"GB_New2_skill_005")
    Sound(0.4,"GB_New2_attack_003")
    Sound(0.43,"QQJ_Body_Impact")
    Sound(0.2,"HG_SoundVOC_attack3_1")
    GroundSound(0.43,"Sound_fall") 
    HitGroundFx(0.43,"SFX_hitground",1.2,0,0,0)   		 		
    HitGroundFx(0.43,"SFX_hitground2",1.2,0,0,0)	
    CameraShake(0.43,"ShakeMode = 3","ShakeTimes = 0.6","ShakeFrequency = 0.033","ShakeAmplitudeX = 8","ShakeAmplitudeY = 8","ShakeAmplitudeZ = 8")
	ContralShake(0.43,0.65,0,0.9)	


STake_Start("HG_GB_PA_skill_025B_E","HG_GB_PA_skill_025B_E")  -- ��Ƥ��E��   17555
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
    Fx(0,"HG_GB_PA_skill_025B_E_fire","Reference",1,0,0,0,0,0,0,0,1)
	Sound(0.01,"HG_GB_ST_Skill_015_3")
    Sound(0.55,"HG_SoundVOC_attack3_1")
	Sound(0.01,"HG_GB_PA_Skill_002_Combo2")
	
	PlugIn(0.05)
	PlugOut(0.32)	
	PlugOut(0.32)	
	PlugOut(0.32)	
	PlugOut(0.32)		

	Charge(0,0.05,20,0,14,0,0.34,70)
	
	AttackStart()
    Hurt(0.32,"hurt_81",0,1)
    AttackHurtFx(0.32,"HG_GB_PA_attack_B_001_hit","Spine1",1) 
	HurtBoneFx(0.32,"FX_Violent_blood_Blunt_R2","Spine1",0.4,0,0,0,0,0,0)
    HurtSound(0.32,"BTS_CR_HIT_new_2_1") 	
	
    CameraShake(0.62,"ShakeMode = 3","ShakeTimes = 0.6","ShakeFrequency = 0.033","ShakeAmplitudeX = 6","ShakeAmplitudeY = 6","ShakeAmplitudeZ = 8")
	ContralShake(0.62,0.65,0,0.9)	]]--	

	
--------------------------------------------------------------

	
STake_Start("HG_GB_PA_skill_004","HG_GB_PA_skill_move_att3")   -- ͻ������������4��    17560      
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"5")     
    BlendPattern("steady") 
    PlaySpeed(0,0.2,2)
    Charge(0,0.1,200,20,12) 
    --ChangeMaterial(0.03,0.4,"/PC/Material/B")

  	Fx(0,"HG_GB_PA_skill_move_att3","Reference",1,0,0,0,0,0,0,0,1,1)    
    Sound(0.050,"GB_New2_attack_000")
    Sound(0.050,"HG_SoundVOC_attack1_1")
	
	AttackStart()
    Hurt(0.12,"hurt_21",0,1) 	
    HurtSound(0.12,"PA_Hit_002") 
    AttackHurtFx(0.12,"HG_GB_PA_attack_A_hit","Spine1",1)
    HurtBoneFx(0.12,"FX_Violent_blood_saxue1","Spine1",1,0,0,0,0,0,0)	 
    	
    CameraShake(0.12,"ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 4")		
	
	
STake_Start("HG_GB_PA_skill_017","HG_GB_PA_skill_017")   -- ������Q��    17565      
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady") 	
  	Fx(0,"HG_GB_PA_skill_017_fire","Reference",1.6,0,0,0,-12,0,0,0,1,1) 	
	Sound(0.1,"HG_SoundVOC_attack5_1")
	Sound(0.1,"HG_GB_PA_skill_017")
	
	CameraShake(0.21,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	ContralShake(0.21,0.3,0,0.9)		
	
STake_Start("HG_GB_PA_skill_026","HG_GB_PA_skill_026")   -- ���أ�E��    17570      
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady") 	
  	--Fx(0,"HG_GB_PA_skill_026_back","Reference",1,0,0,0,0,0,0,0,1.7,1)	
  	--Fx(0,"HG_GB_PA_skill_026_fire","Reference",0.4,0,0,0,20,0,0,0,1,1) 		
  	Fx(0,"HG_GB_PA_skill_026","Reference",1,0,0,0,0,0,0,0,2,1)	
	PlaySpeed(0,0.8,2)
	Sound(0.1,"HG_GB_PA_Skill_000_Combo3")
	Sound(0.02,"GB_New2_attack_000")
	Sound(0.1,"HG_SoundVOC_Jump_1")
	
	CameraShake(0.81,"ShakeMode = 3","ShakeTimes = 0.5","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	ContralShake(0.81,0.4,0,0.9)
	
	Charge(0,0.375,120,180,16)
	
	
STake_Start("HG_GB_PA_skill_037","HG_GB_PA_skill_037")   -- ��������   17652   
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	Sound(0.1,"HG_GB_PA_skill_005")
	Sound(0.1,"HG_SoundVOC_Jump_1")
	
  	Fx(0.1,"HG_GB_PA_skill_037","Reference",1.2,0,0,0,0,0,0,0,1.3,1)	
    HitGroundFx(0.2,"SFX_hitground",1.2,0,0,0)   		 		
    HitGroundFx(0.2,"SFX_hitground2",1.2,0,0,0)
    Charge(0,0.1,5,0,21,0,0) 
	
  	AttackStart()	
    Hurt(0.2,"hurt_21",0,2)	
	DirFx(0.2,"HG_GB_PA_attack_B_001_hit",25,1)		
	HurtSound(0.2,"BTS_CR_HIT_new_2_1") 
	
    CameraShake(0.2,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")
	ContralShake(0.2,0.3,0,0.9)	
	
	
--------------------------------------------------------------

	
--[[STake_Start("HG_GB_PA_skill_029+","HG_GB_PA_skill_029")   -- �������ڣ�Q��    17580      
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady") 	
	Sound(0.1,"GB_New2_skill_000")
	Sound(0.15,"HG_SoundVOC_attack3_1")
    Fx(0,"HG_GB_PA_skill_027_Q_fire","Reference",1,0,0,0,0,0,0,0,1,1)]]--	

STake_Start("HG_GB_PA_skill_028","HG_GB_PA_skill_028")   -- ���ִ�糵    17585      
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"4")     
    Priority(1.75,"5")   	
    BlendPattern("steady") 
	Sound(0.01,"HG_SoundVOC_attack4_1")
	Sound(0.01,"HG_GB_PA_Skill_000_Combo1")
	Sound(0.4,"HG_GB_PA_skill_017")
	Sound(0.2,"GB_New2_skill_005")
	Sound(1.5,"HG_SoundVOC_attack3_1")
	Sound(1.5,"HG_GB_PA_skill_005")
	
    Fx(0,"HG_GB_PA_skill_028","Reference",1.2,0,0,0,0,0,0,0,1,2)
    Fx(0,"HG_GB_PA_skill_028","Reference",1.2,0,0,0,0,0,0,0,1,2)	
    Fx(1.1,"HG_GB_PA_skill_028_fire2","Reference",1.2,0,0,0,0,0,0,0,1,2)	   
    Fx(1.1,"HG_GB_PA_skill_028_fire2","Reference",1.2,0,0,0,0,0,0,0,1,2)	

	PlugIn(0)
	PlugIn(0)
	PlugIn(0)	
	PlugIn(0)
	PlugIn(0)	
	PlugOut(0)	
	PlugOut(0)
	PlugOut(0)	
	PlugOut(0)
	PlugOut(0)		
	Charge(0,0.21,60,15,12)	
	--Pause(0.21,0.1,0,0.09)
	
	AttackStart()	
    Hurt(0,"hurt_81",0,1)	
	DirFx(0,"HG_GB_PA_attack_B_001_hit",25,1)		
	HurtSound(0.02,"BTS_CR_HIT_new_2_1") 
    HurtBoneFx(0,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)

	CameraShake(0.455,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")
	CameraShake(0.7,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")
	CameraShake(0.945,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")
	CameraShake(1.19,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")
	CameraShake(1.435,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")	
	CameraShake(1.68,"ShakeMode = 3","ShakeTimes = 0.5","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")    	
	ContralShake(1.68,0.65,0,0.9)	
	
STake_Start("HG_GB_PA_skill_028_1","HG_GB_PA_skill_028")   -- ���ִ�糵����    17587      
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"4")     
    BlendPattern("steady") 
	Sound(0.01,"HG_SoundVOC_attack5_1")
	Sound(0.01,"HG_GB_PA_Skill_000_Combo1")
	Sound(0.4,"HG_GB_PA_skill_017")
	Sound(0.2,"GB_New2_skill_005")
	Sound(1.5,"HG_SoundVOC_attack3_1")
	Sound(1.5,"HG_GB_PA_skill_005")
	
    Fx(0,"HG_GB_PA_skill_028_fire","Reference",1.2,0,0,0,0,0,0,0,1,2)
    Fx(0,"HG_GB_PA_skill_028_fire","Reference",1.2,0,0,0,0,0,0,0,1,2)	
    Fx(0.2,"HG_GB_PA_skill_028_fire3","Reference",1.2,0,0,0,0,0,0,0,1,2)	   
    Fx(0.2,"HG_GB_PA_skill_028_fire3","Reference",1.2,0,0,0,0,0,0,0,1,2)	
    Fx(0.9,"HG_GB_PA_skill_028_fire3","Reference",1.2,0,0,0,0,0,0,0,1,2)	   
    Fx(0.9,"HG_GB_PA_skill_028_fire3","Reference",1.2,0,0,0,0,0,0,0,1,2)	
	
	PlugIn(0)
	PlugIn(0)
	PlugIn(0)	
	PlugIn(0)
	PlugIn(0)	
	PlugOut(0)	
	PlugOut(0)
	PlugOut(0)	
	PlugOut(0)
	PlugOut(0)		
	Charge(0,0.21,60,15,12)	
	--Pause(0.21,0.1,0,0.09)
	
	AttackStart()	
    Hurt(0,"hurt_81",0,1)	
	DirFx(0,"HG_GB_PA_attack_B_001_hit",25,1)		
	HurtSound(0.02,"BTS_CR_HIT_new_2_1") 
    HurtBoneFx(0,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)

	CameraShake(0.455,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")
	CameraShake(0.7,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")
	CameraShake(0.945,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")
	CameraShake(1.19,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")
	CameraShake(1.435,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 6")	
	CameraShake(1.68,"ShakeMode = 3","ShakeTimes = 0.5","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")    	
	ContralShake(1.68,0.65,0,0.9)		
	
	
--------------------------------------------------------------

	
STake_Start("HG_GB_PA_skill_021","HG_GB_PA_skill_021")   -- ʱ��С��������6��   17590     soft��ʼ0.76
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	Sound(0.1,"GB_Skill_Drink_small")

    Fx(0,"HG_GB_PA_skill_021_fire","Reference",1.3,0,0,0,-8,0,0,0,1)		

STake_Start("HG_GB_PA_skill_031","HG_GB_PA_skill_031")   -- ����С��   17595     
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	Sound(0.5,"HG_SoundVOC_attack1_1")
	Sound(0.01,"GB_New2_skill_002")
	Sound(0.1,"GB_New2_skill_001")
	Sound(0.55,"HG_GB_PA_Skill_000_Combo2")
  	Fx(0,"HG_GB_PA_skill_014_start","",1.1,0,0,0,0,0,0,0,1,0,0)
  	Fx(0.2,"HG_GB_PA_skill_014_start","",1.1,0,0,0,0,0,0,0,1,0,0)		
	
	Fx(0,"HG_GB_PA_skill_031_d","Reference",0.8,0,0,0,0,0,0,0,1.1,1)
    Fx(0,"HG_GB_PA_skill_031_a","Reference",0.8,0,0,0,0,0,0,0,1.1,1)
	Fx(0,"HG_GB_PA_skill_031_b","Reference",0.8,0,0,0,0,0,0,0,1.1,1)
	Fx(0,"HG_GB_PA_skill_031_c","Reference",0.8,0,0,0,0,0,0,0,1.1,1)
    HitGroundFx(0.6,"SFX_hitground",1.1,0,0,0)   		 		
    HitGroundFx(0.6,"SFX_hitground2",1.1,0,0,0)
	
	AttackStart()	
    Hurt(0.1,"hurt_81",0,1)		
    Hurt(0.3,"hurt_81",0,1)		
    Hurt(0.6,"hurt_81",0,1)	
	DirFx(0.1,"HG_GB_PA_attack_B_001_hit",25,1)		
	DirFx(0.3,"HG_GB_PA_attack_B_001_hit",25,1)		
	DirFx(0.6,"HG_GB_PA_attack_B_001_hit",25,1)	
	HurtSound(0.1,"BTS_CR_HIT_new_2_1") 	
	HurtSound(0.3,"BTS_CR_HIT_new_2_1") 	
	HurtSound(0.6,"BTS_CR_HIT_new_2_1") 
	
    CameraShake(0.1,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")	
    CameraShake(0.3,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")	
	CameraShake(0.6,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	ContralShake(0.6,0.3,0,0.9)
	
STake_Start("HG_GB_PA_skill_029","HG_GB_PA_skill_029")   -- ��ƣ�E1��   17600     0.27��  soft��ʼ0.49
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	Sound(0.1,"GB_Skill_Drink_Spray")
	
	Fx(0,"HG_GB_PA_skill_029_fire","Reference",1,0,0,0,7,0,0,0,1,1,0,1)  
	
	AttackStart()	
    Hurt(0.4,"hurt_21",0,1)	
	HurtSound(0.4,"BTS_CR_HIT_new_2_1") 
	DirFx(0.4,"HG_GB_PA_skill_029_hit",25,1,1)		

STake_Start("HG_GB_PA_skill_030","HG_GB_PA_skill_030")   -- ���E2��   17605     0.29��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	Sound(0.1,"GB_Skill_Drink_Fire")

	Fx(0,"HG_GB_PA_skill_030_fire","Reference",1,0,0,0,0,0,0,0,1.3,1,0,1) 
	
	AttackStart()	
    Hurt(0.4,"hurt_21",0,1)	
	DirFx(0.4,"HG_GB_PA_skill_030_hit_a",25,1,1)		
	HurtSound(0.4,"BTS_CR_HIT_new_2_1") 

	
STake_Start("HG_GB_PA_skill_030_1","HG_GB_PA_skill_030")   -- �������   17606
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	Sound(0.1,"GB_Skill_Drink_Fire")

	Fx(0,"HG_GB_PA_skill_030_fire2","Reference",1,0,0,0,0,0,0,0,1.3,1,0,1) 
	
	AttackStart()	
    Hurt(0.4,"hurt_21",0,1)	
	DirFx(0.4,"HG_GB_PA_skill_030_hit_a",25,1,1)		
	HurtSound(0.4,"BTS_CR_HIT_new_2_1") 
	
	
STake_Start("HG_GB_PA_skill_035_001","HG_GB_PA_skill_035_001")   -- �ȾƱ���   17671     
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	Sound(0.1,"GB_Skill_Drink_Spray")
	Sound(1.1,"HG_GB_PA_Skill_010")
	Sound(1.9,"GlassBroken_1")
	Sound(1.7,"HG_SoundVOC_attack2_1")
    PlaySpeed(0,0.7,2)
    PlaySpeed(1.7,1.9,2)
	
    Fx(0,"HG_GB_PA_skill_035","Reference",1,0,0,0,0,0,0,0,1.3,1)
    HitGroundFx(1.9,"SFX_hitground",1.2,0,0,0)   		 		
    HitGroundFx(1.9,"SFX_hitground2",1.2,0,0,0)
	
	AttackStart()	
    Hurt(1.45,"hurt_81",0,1)	
	DirFx(1.45,"HG_GB_PA_skill_030_hit_a",25,1,1)		
	HurtSound(1.45,"BTS_CR_HIT_new_2_1") 
	
    CameraShake(1.9,"ShakeMode = 3","ShakeTimes = 0.5","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")	
	ContralShake(1.9,0.3,0,0.9)	
	
	
STake_Start("HG_GB_PA_skill_055","HG_GB_PA_skill_041")   -- ׷����   17672     
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
    HitGroundFx(0.33,"SFX_hitground",1.2,0,0,0)   		 		
    HitGroundFx(0.33,"SFX_hitground2",1.2,0,0,0)
    PlaySpeed(0,0.44,1.147)	
    Charge(0,0.3,80,10,12)
    Fx(0,"HG_GB_PA_skill_041","Reference",1,0,0,0,0,0,0,0,1.33,1)
	Sound(0.03,"HG_GB_PA_Skill_015")
	Sound(0.31,"GB_New2_attack_003")
	
	AttackStart()	
    Hurt(0.3,"hurt_81",0,1)	
	DirFx(0.3,"HG_GB_PA_skill_030_hit_a",25,1,1)		
	HurtSound(0.3,"BTS_CR_HIT_new_2_1") 
	
	CameraShake(0.33,"ShakeMode = 3","ShakeTimes = 0.33","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")	
	ContralShake(0.33,0.3,0,0.9)	
	
STake_Start("HG_GB_PA_skill_060","HG_GB_PA_skill_017")   -- ��غ�   17683     
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")

    Fx(0.05,"HG_GB_PA_skill_017_fire_A","Reference",1,0,0,0,0,0,0,0,1,1) 	
	
	Sound(0.1,"HG_SoundVOC_attack5_1")
	Sound(0.1,"HG_GB_PA_skill_017")
	
	AttackStart()	
    Hurt(0.18,"hurt_21",0,1)	
	DirFx(0.18,"HG_GB_PA_skill_030_hit_a",25,1,1)		
	HurtSound(0.18,"BTS_CR_HIT_new_2_1") 
	
	CameraShake(0.21,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	ContralShake(0.21,0.3,0,0.9)	
	
--------------------------------------------------------------	


STake_Start("HG_GB_PA_skill_038","HG_GB_PA_skill_038")   -- Ⱥ���Ƹ�    17575      
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady") 	
	Sound(0.1,"HG_GB_PA_skill_005")
	Sound(0.1,"HG_SoundVOC_Jump_1")
    --Fx(0.46,"","Reference",1,0,0,0,0,0,0,0,1,1)
	
	
STake_Start("HG_GB_PA_skill_039","HG_GB_PA_skill_039")   -- ��Ѫ����    17651     
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady") 	
    Loop(0.433,1.1)	
	Sound(0.2,"HG_GB_PA_skill_005")
	Sound(0.2,"HG_SoundVOC_Jump_1")
	Sound(0.4,"Life_Recover_Loop_1")

    Fx(0.433,"HG_GB_PA_skill_039","Reference",1,0,0,0,0,0,0,0,1,1)


	
--------------------------------------------------------------	
STake_Start("HG_GB_PA_skill_018_fire1","HG_GB_PA_skill_018_B")   -- ����Ͷ��ʧ��   17201
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	

	Charge(0,0.19,80,15,12)	
    Pause(0.2,0.1,0,0.09) 	
	
    AttackStart()
    Hurt(0.2,"hurt_21",0,1)
    AttackHurtFx(0.2,"HG_GB_PA_attack_B_001_hit","Spine1",1) 
   	HurtSound(0.2,"BTS_CR_Hit_001") 
	Sound(0.160,"HG_SoundVOC_attack1_1")  		
  	Sound(0.050,"GB_New2_attack_004") 
	
--[[STake_Start("HG_GB_PA_skill_018_fire2","HG_GB_PA_skill_018+")  -- ����Ͷ���ɹ�   17202
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")		
    FrameTime(0.2,2.033)
	
	--CutScene(0.3,"HG_GB_PA_skill_018_fire2")
    Fx(0.2,"HG_GB_PA_skill_018_a","",1,0,0,0,0,0,0,0,1.1)
    --Fx(1.31,"HG_GB_PA_skill_018_b","",1,0,0,0,0,0,0,0,1)	
    --DrawDragFx(1.31, -1, 0, "HG_GB_PA_skill_018_b", "HG_GB_PA_skill_018_b", 0, 0, 0, "", 0, 0, 0, 0,0, 0, 0,0,1,1,0,0,0,0,1)    
    --Fx(0.2,"HG_QZ_PA_skill_007_tracker","R",1,0,0,0,0,0,0,0,1)	
    --Fx(0.6,"HG_QZ_PA_skill_007_tracker","R",1,0,0,0,0,0,0,0,1)	
    --Fx(1,"HG_QZ_PA_skill_007_tracker","R",1,0,0,0,0,0,0,0,1)		

	PlaySpeed(0.2,0.65,1.5)  --0.2~0.5	
	PlaySpeed(0.65,0.8,0.7)  --0.5~0.714
	PlaySpeed(0.8,0.9,0.5)	--0.714~0.914
	PlaySpeed(0.9,1.05,0.3)	--0.914~1.414
	PlaySpeed(1.26,1.3,0.2)	--1.624~1.824	
	--PlaySpeed(1.6,1.8,0.4) 
	
	PlugIn(0)
	PlugOut(0)	
	Charge(0,0.3,0,0,14,0,1.094,120)	--0.2-0.5
	Charge(0.01,0.2,0,0,14,0,0,0,-120)	--1.624-1.824 
	Charge(0.011,0.4,40,180,16) --1.824
	
    Sound(0.218,"GB_New2_skill_005")
    Sound(1.116,"GB_New2_attack_003")
    Sound(1.644,"QQJ_Body_Impact")
    Sound(0.603,"HG_SoundVOC_Jump_1")
    Sound(1.044,"HG_SoundVOC_attack3_1")
    GroundSound(1.813,"Sound_fall") 
	
	AttackStart()
    Hurt(1.624,"hurt_81",0,1)
    AttackHurtFx(1.624,"HG_GB_PA_attack_B_001_hit","Spine1",1) 
	HurtBoneFx(1.624,"FX_Violent_blood_PT","Spine1",1,0,0,10,0,0,0)	
    HurtSound(1.624,"BTS_CR_HIT_new_2_1") 	
	
    CameraShake(1.3,"ShakeMode = 3","ShakeTimes = 0.6","ShakeFrequency = 0.033","ShakeAmplitudeX = 8","ShakeAmplitudeY = 8","ShakeAmplitudeZ = 8")
	ContralShake(1.3,0.65,0,0.9)	]]--

STake_Start("HG_GB_PA_skill_043","HG_GB_PA_skill_043")  -- ����Ͷ���ɹ�   17202     1.36�����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"4")     
    Priority(1.65,"5") 	
    BlendPattern("steady")		
	
	--CutScene(0.3,"HG_GB_PA_skill_018_fire2")
    Fx(0,"HG_GB_PA_skill_018_a","Reference",1,0,0,0,0,0,0,0,1)
    Fx(0,"Fx_common_dian_B","Reference",1,0,0,0,0,0,0,0,1,3)	
    CampFx(0, "Fx_common_dian_B", "Fx_common_dian_B", "Fx_common_dian_R","Reference",1,1,0,0,0,0,0,0,1,3)
	
	Sound(1.36,"XLJ_Down_Hurt3")
    Fx(1.36,"HG_GB_PA_skill_025B_Q_b","",1.3,0,0,0,0,0,0,0,1)
	
	PlaySpeed(0.2,0.65,1.5)  --0.2~0.5	
	PlaySpeed(0.65,0.8,0.7)  --0.5~0.714
	PlaySpeed(0.8,0.9,0.5)	--0.714~0.914
	PlaySpeed(0.9,1.05,0.3)	--0.914~1.414
    EffectStart(0, "FOV", 25)
    EffectStart(1.1, "FOV", -5)
	
	PlugIn(0)
	PlugOut(0)	
	
    Sound(0.218,"GB_New2_skill_005")
    Sound(1.116,"GB_New2_attack_003")
    Sound(1.644,"QQJ_Body_Impact")
    Sound(0.603,"HG_SoundVOC_Jump_1")
    Sound(1.044,"HG_SoundVOC_attack3_1")
    GroundSound(1.813,"Sound_fall") 
	
	AttackStart()
    Hurt(1.724,"hurt_81",0,1)
    AttackHurtFx(1.724,"HG_GB_PA_attack_B_001_hit","Spine1",1) 
	HurtBoneFx(1.724,"FX_Violent_blood_PT","Spine1",1,0,0,10,0,0,0)	
    HurtSound(1.724,"BTS_CR_HIT_new_2_1") 	
	
    CameraShake(1.3,"ShakeMode = 3","ShakeTimes = 0.6","ShakeFrequency = 0.033","ShakeAmplitudeX = 8","ShakeAmplitudeY = 8","ShakeAmplitudeZ = 8")
	ContralShake(1.3,0.65,0,0.9)	
	
--[[STake_Start("HG_GB_PA_skill_019_fire1","HG_GB_PA_skill_018_B")   -- ץˤʧ��   17206
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	
    Fx(0,"HG_GB_PA_skill_019_fire1","Reference",1,0,0,3,2,0,0,0,1)	
	
	Charge(0,0.12,20,15,12)	
    Pause(0.234,0.1,0,0.09) 	
	
    AttackStart()
    Hurt(0.234,"hurt_21",0,1)
    AttackHurtFx(0.234,"HG_GB_PA_attack_B_001_hit","Spine1",1) 
   	HurtSound(0.234,"BTS_CR_Hit_001") 
	Sound(0.160,"HG_SoundVOC_attack3_1")  		
  	Sound(0.050,"GB_New2_attack_004") 	
	
STake_Start("HG_GB_PA_skill_019_fire2","HG_GB_PA_skill_019")  -- ץˤ�ɹ�   17207  0.5|1.12|1.87
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")		
    FrameTime(0.234,2.6)	

	PlugIn(0)
	PlugOut(0)	
	
	PlaySpeed(1.6,1.76,0.4)  	--1.6~2
	PlaySpeed(1.76,1.86,2)   --2~2.05
	
    Fx(0,"HG_GB_PA_skill_019_fire6","Reference",1,0,0,0,0,0,0,0,0.8)		
    Fx(0.25,"HG_GB_PA_skill_019_fire2","Reference",1,0,0,0,0,0,0,0,0.9)		
    Fx(0.9,"HG_GB_PA_skill_019_fire3","Reference",1,0,0,0,0,0,0,0,1.5)
    Fx(1.4,"HG_GB_PA_skill_019_fire5","Reference",1,0,0,10,0,0,0,0,1)		
    Fx(1.8,"HG_GB_PA_skill_019_fire4","Reference",1,0,0,0,0,0,0,0,0.8)
	
    --Fx(0.234,"HG_QZ_PA_skill_007_tracker","R",1,0,-22,11,14,0,0,0,1)		
    --Fx(0.5,"HG_QZ_PA_skill_007_tracker","R",1,0,-22,11,14,0,0,0,1)		
    --Fx(1,"HG_QZ_PA_skill_007_tracker","R",1,0,-22,11,14,0,0,0,1)	
    --Fx(1.4,"HG_QZ_PA_skill_007_tracker","R",1,0,-22,11,14,0,0,0,1)	
    --Fx(1.8,"HG_QZ_PA_skill_007_tracker","R",1,0,-22,11,14,0,0,0,1)	
    --Fx(0.5,"HG_QZ_PA_skill_007_tracker","Reference",1.5,1,6,35,0,0,0,0,1.5)
    --Fx(1.12,"HG_QZ_PA_skill_007_tracker","Reference",1.5,1,0,-35,0,0,0,0,1.5)
    --Fx(1.87,"HG_QZ_PA_skill_007_tracker","Reference",1.5,1,-3,-40,0,0,0,0,1.5)
	
    AttackStart()
    Hurt(0.266,"hurt_81",0,1)
    Hurt(0.886,"hurt_81",0,1)
    Hurt(1.826,"hurt_81",0,1)	
    AttackHurtFx(0.266,"HG_GB_PA_attack_B_001_hit","Spine1",1) 
    AttackHurtFx(0.886,"HG_GB_PA_attack_B_001_hit","Spine1",1) 
    AttackHurtFx(1.826,"HG_GB_PA_attack_B_001_hit","Spine1",1) 	
	HurtBoneFx(0.266,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
	HurtBoneFx(0.886,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
	HurtBoneFx(1.826,"FX_Violent_blood_Blunt_R2","Spine1",1,0,0,0,0,0,0)
   	HurtSound(0.266,"BTS_CR_HIT_new_2_1") 
   	HurtSound(0.886,"BTS_CR_HIT_new_2_1") 
   	HurtSound(1.826,"BTS_CR_HIT_new_2_1") 	

    Sound(0.375,"HG_SoundVOC_attack3_1")
    Sound(1.653,"HG_SoundVOC_attack3_1")
    Sound(0.423,"QQJ_Body_Impact")
    Sound(1.038,"QQJ_Body_Impact")
    Sound(1.819,"QQJ_Body_Impact")

    Sound(0.240,"GB_New2_attack_003")
    Sound(0.991,"GB_New2_attack_003")
    Sound(1.346,"GB_New2_attack_004")
    Sound(1.724,"GB_New2_attack_003")
	
    CameraShake(0.5,"ShakeMode = 3","ShakeTimes = 0.45","ShakeFrequency = 0.033","ShakeAmplitudeX = 6","ShakeAmplitudeY = 6","ShakeAmplitudeZ = 8")	
    CameraShake(1.12,"ShakeMode = 3","ShakeTimes = 0.45","ShakeFrequency = 0.033","ShakeAmplitudeX = 6","ShakeAmplitudeY = 6","ShakeAmplitudeZ = 8")	
    CameraShake(1.87,"ShakeMode = 3","ShakeTimes = 0.6","ShakeFrequency = 0.033","ShakeAmplitudeX = 8","ShakeAmplitudeY = 8","ShakeAmplitudeZ = 8")
	
	ContralShake(0.5,0.3,0,0.9)
	ContralShake(1.12,0.3,0,0.9)	
	ContralShake(1.87,0.65,0,0.9)	



STake_Start("HG_GB_PA_skill_020","HG_GB_PA_skill_020")   -- ��������   17114
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	
  	Fx(0,"HG_GB_PA_skill_014_start","",1.1,0,0,0,0,0,0,0,1,0,0)
  	Fx(0.3,"HG_GB_PA_skill_014_start","",1.1,0,0,0,0,0,0,0,1,0,0)		
  	Fx(0.6,"HG_GB_PA_skill_014_start","",1.1,0,0,0,0,0,0,0,1,0,0)	
  	Fx(0.8,"HG_GB_PA_skill_014_start","",1.1,0,0,0,0,0,0,0,1,0,0)	
	
    Fx(0,"HG_GB_PA_skill_020_all_a","Reference",1,1,0,0,0,0,0,0,1)
    Fx(0,"HG_GB_PA_skill_020_all_b","Reference",1,1,0,0,0,0,0,0,1)

    --Fx(1.19,"HG_GB_PA_skill_020_all_c","Reference",1,0,-5,-70,0,0,0,0,0.8)
    --Fx(1.28,"HG_GB_PA_skill_020_all_c","Reference",1,0,87,-45,0,0,0,0,0.8)
    --Fx(1.35,"HG_GB_PA_skill_020_all_c","Reference",1,0,-55,37.5,0,0,0,0,0.8)
    --Fx(1.55,"HG_GB_PA_skill_020_all_c","Reference",1,0,-60,-30,0,0,0,0,0.8)
    --Fx(1.62,"HG_GB_PA_skill_020_all_c","Reference",1,0,57,37.5,0,0,0,0,0.8)
	
    AttackStart()
    Hurt(1.19,"hurt_81",0,1)
    Hurt(1.28,"hurt_81",0,1)
    Hurt(1.35,"hurt_81",0,1)	
    Hurt(1.55,"hurt_81",0,1)
    Hurt(1.62,"hurt_81",0,1)
    DirFx(1.19,"HG_GB_PA_attack_B_001_hit",25,1)
    DirFx(1.28,"HG_GB_PA_attack_B_001_hit",25,1)
    DirFx(1.35,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(1.55,"HG_GB_PA_attack_B_001_hit",25,1)
    DirFx(1.62,"HG_GB_PA_attack_B_001_hit",25,1)

 
	    Sound(0.230,"NB_TLKH_skill002start")
    Sound(0.010,"GB_New2_attack_004")
    Sound(0.816,"HG_GB_PA_Skill_002")
    Sound(0.816,"GB_New2_attack_004")

    Sound(0.010,"HG_SoundVOC_attack3_1")
    Sound(1.073,"HG_SoundVOC_attack3_1")

   	HurtSound(1.19,"QZ_PA_Skill_000_hit") 
   	HurtSound(1.28,"QZ_PA_Skill_000_hit") 
   	HurtSound(1.35,"QZ_PA_Skill_000_hit") 
   	HurtSound(1.55,"QZ_PA_Skill_000_hit") 
   	HurtSound(1.62,"QZ_PA_Skill_000_hit")
    CameraShake(1.19,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
    CameraShake(1.28,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")	
	CameraShake(1.35,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
    CameraShake(1.55,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")	
    CameraShake(1.62,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
    CameraShake(1.8,"ShakeMode = 3","ShakeTimes = 0.6","ShakeFrequency = 0.033","ShakeAmplitudeX = 5","ShakeAmplitudeY = 5","ShakeAmplitudeZ = 8")
	ContralShake(1.19,0.3,0,0.9)	
	ContralShake(1.28,0.3,0,0.9)	
	ContralShake(1.35,0.3,0,0.9)	
	ContralShake(1.55,0.3,0,0.9)	
	ContralShake(1.62,0.65,0,0.9)	
 
]]--
		
STake_Start("HG_GB_PA_skill_014","HG_GB_PA_skill_014") -- ����ȭ    17113 soft����ʱ��2   23�������

-- �����0.14|0.168|0.196|0.238|0.266|0.4|0.434|0.462|0.5|0.532|0.6|0.672|0.7|0.728|0.77|0.8|0.938|0.966|0.996|1.03|1.06|1.1 |1.6 
	
    BlendMode(0)
    BlendTime(0.05)
    Priority(0,"5")     
    BlendPattern("steady")

    PlaySpeed(0,0.5,5)
    PlaySpeed(1.5,1.9,2)	
  	Fx(0,"HG_GB_PA_skill_014_start","",1.5,0,0,0,0,0,0,0,1,0,0)	
  	Fx(0.4,"HG_GB_PA_skill_014","",1,0,0,0,0,0,0,0,1,0,0) 
  	Fx(0.54,"HG_GB_PA_skill_014_fire1","",0.8,0,0,0,0,0,0,0,1,0,0) 
  	Fx(0.8,"HG_GB_PA_skill_014_fire2","",0.8,0,0,0,0,0,0,0,1,0,0) 
  	Fx(1.072,"HG_GB_PA_skill_014_fire1","",0.8,0,0,0,0,0,0,0,1,0,0) 
  	Fx(1.338,"HG_GB_PA_skill_014_fire2","",0.8,0,0,0,0,0,0,0,1,0,0) 	
  	Fx(1.4,"HG_GB_PA_skill_014_fire3","",1,0,0,0,0,0,0,0,1,0,0) 	
    Sound(0.010,"HG_GB_PA_Skill_000_Combo1")
    Sound(0.069,"HG_SoundVOC_attack4_1")
    Sound(0.394,"HG_SL_ST_attack_003")
    Sound(0.594,"HG_SL_ST_attack_003")
    Sound(0.623,"HG_SL_ST_attack_003")
    Sound(0.736,"HG_SL_ST_attack_003")
    Sound(0.764,"HG_SL_ST_attack_003")
    Sound(0.793,"HG_SL_ST_attack_003")
    Sound(0.821,"HG_SL_ST_attack_003")
    Sound(0.969,"HG_SoundVOC_attack5_1")
    Sound(0.878,"HG_SL_ST_attack_003")
    Sound(0.934,"HG_SL_ST_attack_003")
    Sound(1.076,"HG_SL_ST_attack_003")
    Sound(1.133,"HG_SL_ST_attack_003")
    Sound(1.218,"HG_SL_ST_attack_003")
    Sound(1.444,"HG_SL_ST_attack_003")
    Sound(1.501,"HG_SL_ST_attack_003")
    Sound(1.610,"GB_New2_attack_005")
    Sound(1.898,"GB_New2_attack_003")	
    Sound(1.869,"HG_SoundVOC_attack3_1")
	Charge(0,0.134,15,20,12)  --0.266~0.4
	
  	AttackStart()
    Hurt(0.14,"hurt_81",0,1) 
    Hurt(0.196,"hurt_81",0,1) 
    Hurt(0.266,"hurt_81",0,1) 
	
    Hurt(0.4,"hurt_21",0,1)
    Hurt(0.462,"hurt_21",0,1)
    Hurt(0.532,"hurt_21",0,1)
	
    Hurt(0.672,"hurt_81",0,1)
    Hurt(0.728,"hurt_21",0,1)
    Hurt(0.8,"hurt_21",0,1) 
	
    Hurt(0.938,"hurt_81",0,1)
    Hurt(0.996,"hurt_21",0,1) 
    Hurt(1.06,"hurt_21",0,1)
    Hurt(1.3,"hurt_81",0,1) 	
  	HurtSound(0.14,"HG_GB_ST_Skill_005_Hit") 
  	HurtSound(0.4,"HG_GB_ST_Skill_005_Hit") 
  	HurtSound(0.728,"HG_GB_ST_Skill_005_Hit") 
  	HurtSound(1.06,"HG_GB_ST_Skill_005_Hit") 
  	HurtSound(1.3,"BTS_CR_HIT_new_2_1") 	
    DirFx(0.14,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(0.196,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(0.266,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(0.4,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(0.462,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(0.532,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(0.672,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(0.728,"HG_GB_PA_attack_B_001_hit",25,1)	  
    DirFx(0.8,"HG_GB_PA_attack_B_001_hit",25,1)	
    DirFx(0.938,"HG_GB_PA_attack_B_001_hit",25,1)		
    DirFx(0.996,"HG_GB_PA_attack_B_001_hit",25,1)	
	DirFx(1.06,"HG_GB_PA_attack_B_001_hit",25,1)	
	DirFx(1.3,"HG_GB_PA_attack_B_001_hit",25,1)		
	HurtBoneFx(0.14,"FX_Violent_blood_PT","Spine1",1,0,0,0,180,0,0)
	HurtBoneFx(0.4,"FX_Violent_blood_PT","Spine1",1,0,0,0,180,0,0)
	HurtBoneFx(0.728,"FX_Violent_blood_PT","Spine1",1,0,0,0,180,0,0)
	HurtBoneFx(1.06,"FX_Violent_blood_PT","Spine1",1,0,0,0,180,0,0)
	HurtBoneFx(1.3,"FX_Violent_blood_PT","Spine1",1,0,0,0,180,0,0,1)
	
    CameraShake(1.96,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")	
	ContralShake(0.54,0.25,0,0.9)
	ContralShake(0.8,0.25,0,0.9)
	ContralShake(1.072,0.25,0,0.9)
	ContralShake(1.338,0.25,0,0.9)	
	ContralShake(1.96,0.4,0,0.9)		
	

--------------------------------------------------------------�������չ�------------------------------------------------------------------------


STake_Start("HG_GB_PA_attack_A_001_air","HG_GB_PA_attack_A_001_air")   -- �չ�1    17101  �����0.18   
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
    Priority(0.35,"6")	
	
  	Fx(0.18,"HG_GB_PA_attack_B_001_fire","",1,0,-50,-20,15,-30,0,-30,1.2,1)	
	
    Sound(0.010,"GB_New2_attack_003")
    Sound(0.010,"HG_SoundVOC_attack1_1")

  	AttackStart()
    Hurt(0.18,"hurt_21",0,1) 	
  	HurtSound(0.18,"BTS_CR_HIT_new_2_1") 
    DirFx(0.18,"HG_GB_PA_attack_B_001_hit",25,1)	
    HurtBoneFx(0.18,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)	
	
	CameraShake(0.18,"ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 4")


STake_Start("HG_GB_PA_attack_A_002_air","HG_GB_PA_attack_A_002_air")   -- �չ�2   17102    �����0.2   
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
    Priority(0.35,"6")	
	
  	Fx(0.2,"HG_GB_PA_attack_B_002_fire","",1,0,50,-20,55,50,-30,0,1.2,1)	
    Sound(0.010,"GB_New2_attack_003")
    Sound(0.010,"HG_SoundVOC_attack2_1")

  	AttackStart()
    Hurt(0.2,"hurt_21",0,1) 	
  	HurtSound(0.2,"BTS_CR_HIT_new_2_1") 
    DirFx(0.2,"HG_GB_PA_attack_B_001_hit",25,1)	
	HurtBoneFx(0.2,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
	
	CameraShake(0.2,"ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 4")


STake_Start("HG_GB_PA_attack_A_003_air","HG_GB_PA_attack_A_003_air")   -- �չ�3   17103  �����0.49  
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
    Priority(0.6,"6")	
	
	Fx(0,"HG_GB_PA_attack_B_003","",1.2,0,0,15,20,0,-40,0,1.4,1)	
    Sound(0.02,"HG_GB_PA_Skill_012")
    Sound(0.02,"HG_SoundVOC_attack1_1")
    Sound(0.4,"HG_SoundVOC_attack3_1")

	
  	AttackStart()
    Hurt(0.49,"hurt_81",0,1) 	
  	HurtSound(0.49,"QZ_PA_Skill_000_hit") 
    DirFx(0.49,"HG_GB_PA_attack_B_001_hit",25,1)
    HurtBoneFx(0.49,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)	
	
	CameraShake(0.49,"ShakeMode = 3","ShakeTimes = 0.1","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 4")

	
STake_Start("HG_GB_PA_attack_B_001_air","HG_GB_PA_attack_B_001_air")   -- �Ҽ��չ�   17106
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")	
	Fx(0,"HG_GB_PA_attack_B_001_air","",1,0,0,0,0,0,0,0,1.1,1)
	Sound(0.03,"GB_New2_attack_000")
	Sound(0.43,"GB_New2_skill_002")
	Sound(0.4,"HG_SoundVOC_attack2_1")
    PlaySpeed(0.44,0.8,0.4)
	
	Charge(0,0.055,0,0,14,0,0,0,-200)	--0.385
    HitGroundFx(0.44,"SFX_hitground",1.1,0,0,0)   		 		
    HitGroundFx(0.44,"SFX_hitground2",1.1,0,0,0)  
	
  	AttackStart()
    Hurt(0.44,"hurt_81",0,2) 	
  	HurtSound(0.44,"BTS_CR_HIT_new_2_1") 
    DirFx(0.44,"HG_GB_PA_attack_B_001_hit",25,1)	
    HurtBoneFx(0.44,"FX_Violent_blood_PT","Spine1",1.2,0,0,0,0,0,0)	
	
    CameraShake(0.44,"ShakeMode = 3","ShakeTimes = 0.5","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8") 	
	ContralShake(0.44,0.4,0,0.9)
	
--------------------------------------------------------------ؤ���Ҽ����ٰ�----------------------------------------------------------------------


STake_Start("HG_GB_PA_attack_B_000_speed","HG_GB_PA_skill_008")   -- �չ�0    17065
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
	
  	Fx(0.1,"HG_GB_PA_skill_008_start","Reference",1,0,0,0,0,0,0,0,1,0)	
  	Fx(1,"HG_GB_PA_skill_008_fire","Reference",1,0,0,-15,0,0,0,0,1.5,0,0,1) 
    Sound(0.010,"GB_New2_attack_003")
    Sound(0.927,"QQJ_Body_Impact")
    Sound(0.010,"HG_SoundVOC_attack1_1")
    Sound(0.889,"HG_SoundVOC_attack2_1")
    HitGroundFx(0.93,"SFX_hitground",1.1,0,0,0)   		 		
    HitGroundFx(0.93,"SFX_hitground2",1.1,0,0,0)	
    PlaySpeed(0,0.9,4.5)	
	Charge(0,0.2,170,20,12) 

  	AttackStart()
    Hurt(0.2,"hurt_71",0,1) 	
    HurtSound(0.2,"QZ_PA_Skill_000_hit") 
    DirFx(0.2,"HG_GB_PA_attack_B_001_hit",25,1)	
    HurtBoneFx(0.2,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
	
	CameraShake(0.93,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	--ContralShake(0.93,0.55,0,0.9)		
	

STake_Start("HG_GB_PA_attack_B_001_speed","HG_GB_PA_attack_B_001")   -- �չ�1    17061
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")

  	Fx(0,"HG_GB_PA_attack_B_001_start","Reference",2,0,0,0,0,0,0,0,3,0)	
  	Fx(0.78,"HG_GB_PA_attack_B_001_fire","",1.3,0,-40,-5,0,-30,0,0,1.5,0,0,1) 

    Sound(0.780,"GB_New2_attack_003")
    Sound(0.780,"HG_SoundVOC_attack2_1")	
    PlaySpeed(0,0.9,3)		
	
    Pause(0.81,0.1,1,0.09)
	Charge(0,0.07,30,20,12) --0.2
	
  	AttackStart()
    Hurt(0.27,"hurt_62",0,1) 	
    HurtSound(0.27,"BTS_CR_HIT_new_2_1") 
    DirFx(0.27,"HG_GB_PA_attack_B_001_hit",25,1)	
    HurtBoneFx(0.27,"FX_Violent_blood_saxue1","Spine1",1.5,0,-30,0,90,0,0,1)	
	
	CameraShake(0.84,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")
	--ContralShake(0.84,0.25,0,0.75)	

STake_Start("HG_GB_PA_attack_B_002_speed","HG_GB_PA_attack_B_002")   -- �չ�2    17062
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
	
  	Fx(0.78,"HG_GB_PA_attack_B_002_fire","",1.3,0,55,-15,10,55,0,0,1.5,0,0,1) 
  	Fx(0,"HG_GB_PA_attack_B_002_start","Reference",2,0,0,0,0,0,0,0,3,0)		
	
    Sound(0.780,"GB_New2_attack_003")
    Sound(0.780,"HG_SoundVOC_attack2_1")	
	
    PlaySpeed(0,0.9,3)	
    Pause(0.81,0.1,1,0.09) 		
	Charge(0,0.07,30,20,12) --0.2
	
  	AttackStart()
    Hurt(0.27,"hurt_62",0,1) 	
    HurtSound(0.27,"BTS_CR_HIT_new_2_1")
    DirFx(0.27,"HG_GB_PA_attack_B_001_hit",25,1)	
    HurtBoneFx(0.27,"FX_Violent_blood_saxue1","Spine1",1.5,0,-30,0,-90,0,0,1)	
	
    CameraShake(0.84,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")
	--ContralShake(0.84,0.25,0,0.75)	
	ContralShake(1.3,0.3,0,0.9)
	
STake_Start("HG_GB_PA_attack_B_003_speed","HG_GB_PA_skill_005")   -- �չ�3     17063
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
    FrameTime(0.48,1.833)
	
    PlaySpeed(0.5,0.9,1.8)		
	Fx(0.48,"HG_GB_PA_attack_B_003","",1.5,0,0,10,-6,0,0,0,2,0,0,1) 
    HitGroundFx(0.93,"SFX_hitground",1,0,-40,0)   		 		
    HitGroundFx(0.93,"SFX_hitground2",1,0,-40,0)	
    Sound(0.780,"GB_New2_attack_003")
    Sound(0.780,"HG_SoundVOC_attack2_1")	
	Charge(0,0.03,30,20,12) --0.24  

  	AttackStart()
    Hurt(0.27,"hurt_62",0,1) 	
    HurtSound(0.27,"QZ_PA_Skill_000_hit") 
    DirFx(0.27,"HG_GB_PA_attack_B_001_hit",25,1)	
    HurtBoneFx(0.27,"FX_Violent_blood_Sharp_R3","Spine1",0.3,0,0,0,180,0,0)	
	
    CameraShake(0.93,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 7")	
	--ContralShake(0.93,0.3,0,0.8)		

STake_Start("HG_GB_PA_attack_B_004_speed","HG_GB_PA_attack_B_003")   -- �չ�4       17064
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")

    PlaySpeed(0,0.56,4)	
    PlaySpeed(0.5,0.6,0.8)	
    PlaySpeed(0.6,0.84,1.5)	
	
	Fx(0,"HG_GB_PA_attack_B_004","",2,0,0,0,-6,0,0,0,1.8)
	Fx(0.56,"HG_GB_PA_attack_B_004_long","",1.2,0,0,15,0,0,0,0,1.4,0,0,1) 	

    Sound(0.010,"GB_New2_attack_004")
    Sound(0.010,"NB_TLKH_skill002start")
    Sound(0.750,"GB_New2_skill_002")
    Sound(0.010,"HG_SoundVOC_attack1_1")
    Sound(0.879,"HG_SoundVOC_attack3_1")
	
	Charge(0,0.06,30,20,12) --0.34   

  	AttackStart()
    Hurt(0.42,"hurt_62",0,1) 	
    HurtSound(0.42,"QZ_PA_Skill_000_hit") 
    DirFx(0.42,"HG_GB_PA_attack_B_001_hit",25,1)	
	HurtBoneFx(0.42,"FX_Violent_blood_Sharp_R4","Spine1",0.7,0,0,0,180,0,0)

    CameraShake(0.84,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	ContralShake(0.76,0.65,0,0.9)
	
STake_Start("HG_GB_PA_attack_B_JA_001_speed","HG_GB_PA_skill_003_combo3")  --JA1   17066
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("steady")    
	
    PlaySpeed(0,0.27,3)	
    PlaySpeed(0.27,0.3,0.3)	
    PlaySpeed(0.3,0.57,3)
	
	Charge(0,0.09,30,20,12)	--0.19
	
  	Fx(0.575,"HG_GB_PA_skill_003_combo3_b","Reference",1.2,0,0,12,0,0,0,0,1.2,0,0,1) 	
	
  	Sound(0.090,"HG_GB_ST_Skill_015_3")  	
  	Sound(0.011,"HG_SoundVOC_attack2_1")
    GroundSound(0.555,"Sound_fall")	
    HitGroundFx(0.575,"SFX_hitground")
  	HitGroundFx(0.575,"SFX_hitground2") 	
	
  	AttackStart()
  	Hurt(0.28,"hurt_71")
  	HurtSound(0.28,"PA_Hit_004")  	
    DirFx(0.28,"HG_GB_PA_attack_B_001_hit",25,1)
	HurtBoneFx(0.28,"FX_Violent_blood_PT","Spine1",0.8,0,0,0,0,0,0)

    CameraShake(0.57,"ShakeMode = 3","ShakeTimes = 0.33","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	--ContralShake(0.57,0.3,0,0.8)	
	
STake_Start("HG_GB_PA_attack_B_JA_002_speed","HG_GB_PA_skill_007")  --JA2   17068
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("steady")       		
    
  	Fx(0.000,"HG_GB_PA_skill_007_fire","",1.1,1,0,-12,5,0,0,0,1.5,0)  	
  	Fx(0.000,"HG_GB_PA_skill_007_fire","",1,1,0,-5,-15,0,0,0,1.5,0,0) 	
  	Fx(0.5,"HG_GB_PA_skill_007_long","Reference",1.2,0,0,-18,0,0,0,0,1.6)  
  	Sound(0.005,"HG_GB_PA_Skill_012") 
    Sound(0.463,"HG_SoundVOC_attack3_1")
  	Sound(0.010,"HG_GB_PA_Skill_003") 
    GroundSound(1.643,"Sound_fall")  
	
     PlaySpeed(0,1,2)   
    --HitGroundFx( 0.56 ,"SFX_hitground")
  	--HitGroundFx(0.56,"SFX_hitground2")
    PlaySpeed(1.5,1.64,1.8)
	
    Charge(0,0.1,30,20,12) --0.28  
    Pause(0.85,0.1,1,0.2) 
    Pause(0.89,0.1,1,0.2) 
	
    AttackStart()
    Hurt(0.3,"hurt_21",0,1)
    Hurt(0.35,"hurt_21",0,1)
    Hurt(0.4,"hurt_81",0,1)
    Hurt(0.47,"hurt_81",0,1)    
    Hurt(0.5,"hurt_81",0,1)    
    DirFx(0.3,"HG_GB_PA_attack_B_001_hit",25,1)  
    AttackHurtFx(0.35,"HG_GB_PA_attack_B_001_hit","Hips",1)   
    AttackHurtFx(0.4,"HG_GB_PA_attack_B_001_hit","Hips",1)   
    AttackHurtFx(0.47,"HG_GB_PA_attack_B_001_hit","Hips",1)      
    AttackHurtFx(0.5,"HG_GB_PA_attack_B_001_hit","Hips",1)   
	HurtBoneFx(0.35,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.4,"","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.47,"","Spine1",1,0,0,0,0,0,0)
    HurtBoneFx(0.5,"FX_Violent_blood_PT","Spine1",1,0,0,0,180,0,0)
   	HurtSound(0.3,"PA_Hit_002")    
  	HurtSound(0.35,"PA_Hit_002")    
  	HurtSound(0.4,"PA_Hit_002")    
  	HurtSound(0.47,"PA_Hit_002")    
  	HurtSound(0.5,"PA_Hit_002")  

    CameraShake(0.56,"ShakeMode = 3","ShakeTimes = 0.5","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")	
	
	ContralShake(0.6,0.3,0,0.9)
	ContralShake(0.85,0.3,0,0.9)
	ContralShake(0.89,0.65,0,0.9)	
	
--------------------------------------------------------------��----------------------------------------------------------------------	
	
STake_Start("HG_GB_PA_def","HG_GB_PA_def")   --��     17401
    BlendMode(0)
    BlendTime(0.0)
    Priority(0,"5")     
    BlendPattern("steady")
	Loop()
    Fx(0,"HG_GB_PA_def_skill_loop","Reference",1,0,0,0,0,0,0,0,0.6,2)	

STake_Start("HG_GB_PA_def_wm","HG_GB_PA_def_wm")   --��wm     17401
    BlendMode(0)
    BlendTime(0.0)
    Priority(0,"5")     
    BlendPattern("steady")	
	Sound(0.01,"qt_def_wm")
	Fx(0,"HG_GB_PA_def_skill_prefect","Reference",1,1,0,0,0,0,0,0,1,1)
	
STake_Start("HG_GB_PA_def_pt","HG_GB_PA_def_pt")   --��pt     17401
    BlendMode(0)
    BlendTime(0.0)
    Priority(0,"5")     
    BlendPattern("steady")
	Sound(0.01,"qt_def_pt")
	Fx(0,"HG_GB_PA_def_skill","Reference",1,1,0,0,0,0,0,0,1,1)


STake_Start("HG_GB_PA_def_air","HG_GB_PA_def_air")   --��_air     17400
    BlendMode(0)
    BlendTime(0.0)
    Priority(0,"5")     
    BlendPattern("steady")
	Loop()
	
STake_Start("HG_GB_PA_def_air_wm","HG_GB_PA_def_air_wm")   --��wm_air     17400
    BlendMode(0)
    BlendTime(0.0)
    Priority(0,"5")     
    BlendPattern("steady")	
	Sound(0.01,"qt_def_wm")
	Fx(0,"HG_GB_PA_def_prefect","Reference",1,1,0,0,0,0,0,0,1,1)
	
STake_Start("HG_GB_PA_def_air_pt","HG_GB_PA_def_air_pt")   --��pt_air     17400
    BlendMode(0)
    BlendTime(0.0)
    Priority(0,"5")     
    BlendPattern("steady")	
	Sound(0.01,"qt_def_pt")
	Fx(0,"HG_GB_PA_def","Reference",1,1,0,0,0,0,0,0,1,1)
	
STake_Start("HG_GB_PA_def_001","HG_GB_PA_wait_new")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("wait") 
    Priority(0,"5")
    FrameTime(0,0.001)
	
	
STake_Start("HG_GB_PA_defJG_pt_001","HG_GB_PA_defJG_pt_001")   --�ƹ� 
    BlendMode(0)
    BlendTime(0.0)
    Priority(0,"5")     
    BlendPattern("steady")
	Fx(0.3,"HG_GB_PA_defJG_pt_001","Reference",1,1,0,0,0,0,0,0,1)
	Sound(0.01,"HG_GB_PA_Skill_000_Combo1")
	
	AttackStart()
	Hurt(0.3,"hurt_21",0,1) 
    AttackHurtFx(0.3,"HG_GB_PA_attack_B_001_hit","Hips",1)   
	HurtBoneFx(0.3,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
   	HurtSound(0.3,"PA_Hit_002")  
	CameraShake(0.3,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	


STake_Start("HG_GB_PA_skill_034","HG_GB_PA_skill_034")   --���� 17610
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
	Fx(0.01,"FX_xihun_GB_01","Reference",1,1,0,0,0,0,0,0,1,3)
	Loop()
	Sound(0.01,"Skill_XiXing")

STake_Start("HG_GB_PA_HX_start","HG_GB_PA_HX")   --����Loop 10605
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
	Loop(2.5,4.5)
	Fx(0,"FX_tiaoxi_GB","Reference",1,1,-4,0,0,0,0,0,1.1)	
	Fx(1.25,"FX_tiaoxi_GB_loop","Reference",1,1,-4,0,0,0,0,0,1,3)
	--Fx(2.5,"FX_tiaoxi_GB_loop","Reference",1,1,-4,0,0,0,0,0,1,2)	
	Sound(0.1,"HG_GB_PA_Skill_015")
	Sound(1.15,"Life_Recover_Loop_1")


STake_Start("HG_GB_PA_HX_fire","HG_GB_PA_HX")   --����fire 10606
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
    FrameTime(4.5,5.466)
	Sound(3.13,"QZ_PA_Attack_11")
	HurtHard(3.33,3.73)
	
	
	
	
	
	
	
	