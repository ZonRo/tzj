
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame
	
	local openBag = indicatorFlow.createIndicatorSpec()
	openBag.frameName     = 'openBag'
	openBag.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	openBag.frameText     = '点击打开包裹进行装备突破'
	openBag.attachFunc    = self.OnOpenBagAttached
	openBag.attachFuncParam = self
	openBag.triggerFunc   = nil
	openBag.triggerFuncParam = nil
	openBag.triggerWin    = 'Root/MainMenu/PackageBtn'
	openBag.attachWin     = 'Root/MainMenu/PackageBtn'
	openBag.attachWinRoot = 'MainMenu_FunctionAndEXP'
	openBag.triggerKey    = 'PackageUI'
	openBag.priority      = 1
	
	local openBagEquipPage = indicatorFlow.createIndicatorSpec()
	openBagEquipPage.frameName     = 'openBagEquipPage'
	openBagEquipPage.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	openBagEquipPage.frameText     = '点击切换至装备包裹'
	openBagEquipPage.attachFunc    = self.OnOpenBagPageEquipAttached
	openBagEquipPage.attachFuncParam = self
	openBagEquipPage.triggerFunc   = nil
	openBagEquipPage.triggerFuncParam = nil
	openBagEquipPage.triggerWin    = 'Root/PackageFrame/TagContainer1/Btn2_1_0'
	openBagEquipPage.attachWin     = 'Root/PackageFrame/TagContainer1/Btn2_1_0'
	openBagEquipPage.attachWinRoot = 'Root/PackageFrame'
	openBagEquipPage.priority      = 2
	
	local decomposeBtnSpec = indicatorFlow.createIndicatorSpec()
	decomposeBtnSpec.frameName     = 'decomposeBtnSpec'
	decomposeBtnSpec.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	decomposeBtnSpec.frameText     = '点击分解按钮鼠标进入分解状态'
	decomposeBtnSpec.attachFunc    = nil
	decomposeBtnSpec.attachFuncParam = nil
	decomposeBtnSpec.triggerFunc   = nil
	decomposeBtnSpec.triggerFuncParam = nil
	decomposeBtnSpec.triggerWin    = 'Root/PackageFrame/DecompositionButton'
	decomposeBtnSpec.attachWin     = 'Root/PackageFrame/DecompositionButton'
	decomposeBtnSpec.attachWinRoot = 'Root/PackageFrame'
	decomposeBtnSpec.priority      = 3
	
	local decomposeItemSpec = indicatorFlow.createIndicatorSpec()
	decomposeItemSpec.frameName     = 'decomposeItemSpec'
	decomposeItemSpec.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	decomposeItemSpec.frameText     = '点击要分解的装备'
	decomposeItemSpec.attachFunc    = nil
	decomposeItemSpec.attachFuncParam = nil
	decomposeItemSpec.triggerFunc   = nil
	decomposeItemSpec.triggerFuncParam = nil
	decomposeItemSpec.attachType    = indicatorFlow.INDICATOR_ATTACH_BAGITEM
	decomposeItemSpec.attachItemID  = '5_99'
	decomposeItemSpec.priority      = 4
	
	self.decomposeItemSpec = decomposeBtnSpec
	self.indicatorSpecs = {
		openBag, openBagEquipPage, decomposeBtnSpec, decomposeItemSpec, 
	}
	
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)

	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnOpenBagAttached(self, indicator)
	local pWin = InitWindowPtr('Root/PackageFrame')
	if pWin and pWin:isVisible() then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function OnOpenBagPageEquipAttached(self, indicator)
	local pBagItemWin = SD.GetFirstBagItemButtonContainer(self.decomposeItemSpec.attachItemID)
	if pBagItemWin then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

