#include "stdafx.h"
#include <Psapi.h>
#include "function.h"
#include "../mhook-lib/mhook.h"
#include <map>
using namespace std;
set<lua_State *> g_mapLuaState;
bool InitGameWindow(const char *className);
string getglobal(string gname)
{
	CAutoSection cs(g_csGlobal);
	auto it = g_mapGlobal.find(gname);
	if (it == g_mapGlobal.end())
		return "";
	else
		return it->second;
}

void setglobal(string gname, string gvalue)
{
	CAutoSection cs(g_csGlobal);
	g_mapGlobal[gname] = string(gvalue);
}

bool n_hackStart = false;
/*
typedef LRESULT(CALLBACK* _DefWindowProc)(__in HWND hWnd, __in UINT Msg, __in WPARAM wParam, __in LPARAM lParam);
_DefWindowProc g_trueDefWinProc = 0;
LRESULT CALLBACK NewDefWindowProc(
	__in HWND hWnd,
	__in UINT msg,
	__in WPARAM wParam,
	__in LPARAM lParam)
{
	if (hWnd == g_hGame)
	{
		printf("hwnd:%d,msg:%d process:%d thread:%d wp:%d \n", hWnd, msg, GetCurrentProcessId(), GetCurrentThreadId(), wParam);
		if (WM_WGCALL == msg && wParam == 123)
		{
			printf("hwnd:%d,msg:%d\n", hWnd, msg);
			//printDebugMessage("WM_WGCALL");
			ScopeGuard sgTmp(g_sgCallwapper);
			sgTmp.Dismiss(false);
			return 0;
		}
	}
	return g_trueDefWinProc(hWnd, msg, wParam, lParam);
}

*/
typedef BOOL
(WINAPI *
_PeekMessageW)(
_Out_ LPMSG lpMsg,
_In_opt_ HWND hWnd,
_In_ UINT wMsgFilterMin,
_In_ UINT wMsgFilterMax,
_In_ UINT wRemoveMsg);
_PeekMessageW g_truePeekMessageW = NULL;

BOOL
WINAPI
MyPeekMessageW(
_Out_ LPMSG lpMsg,
_In_opt_ HWND hWnd,
_In_ UINT wMsgFilterMin,
_In_ UINT wMsgFilterMax,
_In_ UINT wRemoveMsg){
	if (g_bWaitCall && g_dwGameWndThread == GetCurrentThreadId())
	{
		g_bWaitCall = false;
		printf("get a call\n");
		for (auto it = g_mapLuaState.begin(); it != g_mapLuaState.end();++it)
		{
			g_luaGame = *it;
			ScopeGuard sgTmp(g_sgCallwapper);
			sgTmp.Dismiss(false);
		}
		SetEvent(g_hCall);
	}
	return g_truePeekMessageW(lpMsg, hWnd, wMsgFilterMin, wMsgFilterMax, wRemoveMsg);
}

HANDLE g_hLuaOverSign;
bool bLuaInit = false;
int myloadbuff(lua_State *L, const char *buff, size_t size, const char *name)
{
	/*
	if (g_dwGameWndThread != 0 && g_dwGameWndThread == GetCurrentThreadId()){
		g_luaGame = (lua_State *)L;
		InitGameLuaState(g_luaGame);
		PRT("lua state get:%x", g_luaGame);
		Mhook_Unhook((LPVOID *)&g_luaL_loadbuffer);
	}
	*/

	if (false && g_luaGame == L && false == bLuaInit && name && strcmp(name, "cliscript.lua") == 0){
		bLuaInit = true;
		PRT("tid:%d L:%x now print all data", GetCurrentThreadId(), L);
		const char *str = R"oo(
					local print = msk.print
					local PrintedTable = {}
					function Printt(t,tname)
						if tname then
							tname = tname.."."
						else
							tname = ""
						end
						if not t or PrintedTable[t] then return end
						PrintedTable[t] = true
						for k,v in pairs(t) do
							if type(v) == "function" then
								print(tname..tostring(k))
							elseif type(v) == "table" then
								Printt(v,tname..tostring(k))
							elseif type(v) == "userdata" then
								Printt( getmetatable(v) ,tname..tostring(k))
							end
						end
					end
					Printt(_G))oo";
		int mret = g_luaL_loadbuffer(g_luaGame, str, strlen(str), "printalldata") || g_lua_pcall(g_luaGame, 0, LUA_MULTRET, 0);
		if (0 != mret){
			const char *err = g_lua_tolstring(g_luaGame, -1, NULL);
			PRT("printalldata failed:%s", err);
			g_lua_settop(g_luaGame, -2);
		}
		else{
			PRT("printalldata ok");
		}
	}
	if (false && g_luaGame == L ){
		__try{
			if (name != buff && name && strstr(name, ".lua") && strlen(name) < 1024){
				PRT("tid:%d %x loadbuff:%s len:%d", GetCurrentThreadId(), L, name, size);
				char fname[1024] = { 0 };
				strcpy(fname, name);
				for (int i = 0; i < strlen(fname); ++i){
					if (fname[i] == ' ' || fname[i] == ':')
						fname[i] = '_';
				}
				FILE *f = fopen(fname, "a");
				if (f){
					fwrite(buff, size, 1, f);
					fclose(f);
				}
			}
			else{
				PRT("tid:%d %x loadbuff:unknow data len:%d", GetCurrentThreadId(), L, size);
				FILE *f = fopen("unknow.lua", "a");
				if (f){
					fwrite(buff, size, 1, f);
					fclose(f);
				}
			}
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
			PRT("write file error");
		}
	}
	int ret = g_luaL_loadbuffer(L, buff, size, name);
	return  ret;
	//int ret = g_luaL_loadbuffer(L, buff, size, name);
	if (name && strcmp(name, "Tooltip.lua") == 0){
		PRT("Tooltip get,now wait lua sign");
		SignalObjectAndWait(g_hLuaOverSign, g_hLuaOverSign, -1, FALSE);
		PRT("now start gamestart");
		/*
		string fname = getglobal("c_startfile");
		int ret = (g_luaL_loadfile(g_luaGame, fname.c_str()) || g_lua_pcall(g_luaGame, 0, LUA_MULTRET, 0));
		if (0 != ret){
		const char *err = g_lua_tolstring(g_luaGame, -1, NULL);
		printDebugMessage(err);
		g_lua_settop(g_luaGame, -2);
		}
		*/
		size_t sz;
		const char* dstring = lmc_getfile("gamestart", &sz);
		int ret = (g_luaL_loadbuffer(g_luaGame, dstring, sz, dstring) || g_lua_pcall(g_luaGame, 0, LUA_MULTRET, 0));
		if (0 != ret){
			const char *err = g_lua_tolstring(g_luaGame, -1, NULL);
			PRT(err);
			g_lua_settop(g_luaGame, -2);
		}
		//CloseHandle(g_hLuaOverSign);
		g_hLuaOverSign = NULL;
		bLuaInit = true;
		//printDebugMessage("loadbuff1 now ok");
	}
	else if (name && strstr(name, ".lua")){
		if (!bLuaInit){
			PRT(name);
		}
		else{
			char dstring[4096] = { 0 };
			sprintf_s(dstring, 4096, "FileLoadCallBack(\"%s\")", name);
			int ret = (g_luaL_loadbuffer(g_luaGame, dstring, strlen(dstring), dstring) || g_lua_pcall(g_luaGame, 0, LUA_MULTRET, 0));
			if (0 != ret){
				const char *err = g_lua_tolstring(g_luaGame, -1, NULL);
				PRT(err);
				g_lua_settop(g_luaGame, -2);
			}
		}
	}
	return ret;
}

lua_State *mylua_newstate(lua_Alloc f, void *ud)
{
	lua_State * L = g_lua_newstate(f, ud);
	/*
	if (NULL == g_luaGame){
		InitGameLuaState(g_luaGame);
		g_luaGame = L;
	}
	*/
	//PRT("lua state create:%x", L);
	//g_mapLuaState.insert(L);
	//g_dwGameWndThread = GetCurrentThreadId();
	//Mhook_Unhook((PVOID*)&g_lua_newstate);
	return L;
}

typedef LRESULT(CALLBACK* _DefWindowProc)(__in HWND hWnd, __in UINT Msg, __in WPARAM wParam, __in LPARAM lParam);
_DefWindowProc g_trueDefWinProc = 0;
LRESULT CALLBACK NewDefWindowProc(
	__in HWND hWnd,
	__in UINT msg,
	__in WPARAM wParam,
	__in LPARAM lParam)
{
	if (g_bWaitCall && g_dwGameWndThread == GetCurrentThreadId())
	{
		g_bWaitCall = false;
		PRT("get a call\n");
		{
			ScopeGuard sgTmp(g_sgCallwapper);
			sgTmp.Dismiss(false);
		}
		SetEvent(g_hCall);
	}
	return g_trueDefWinProc(hWnd, msg, wParam, lParam);
}


void HanckINit()
{
	DWORD baseAddress = (DWORD)GetModuleHandleA("SDClient.exe");
	if (baseAddress){
		//2B C2 50 8D 4C 24 18 51 57 E8
		g_luaL_loadbuffer = (_luaL_loadbuffer)(baseAddress + 0x027B5DA0 - 0x00970000);
		PRT("g_luaL_loadbuffer get:%x", g_luaL_loadbuffer);
		//33 C4 89 84 24 0C 02 00 00 53
		g_luaL_loadfile = (_luaL_loadfile)(baseAddress + 0x027B5B80 - 0x00970000);
		PRT("g_luaL_loadfile get:%x", g_luaL_loadfile);
		//83 C4 10 85 C0 75 10 50 50 50 57
		g_lua_pcall = (_lua_pcall)(baseAddress + 0x027B4EB0 - 0x00970000);
		PRT("g_lua_pcall get:%x", g_lua_pcall);
		//56 8B 74 24 08 E8
		g_luaL_register = (_luaL_register)(baseAddress + 0x027B6660 - 0x00970000);
		PRT("g_luaL_register get:%x", g_luaL_register);
		//56 6A 00 68 10 5E 7B 02 E8
		g_lua_newstate = (_lua_newstate)(baseAddress + 0x026E6990 - 0x008a0000);
		PRT("g_lua_newstate get:%x", g_lua_newstate);
		//8B 4C 24 18 51 6A 03 56
		g_lua_tolstring = (_lua_tolstring)(baseAddress + 0x027B4530 - 0x00970000);
		PRT("g_lua_tolstring get:%x", g_lua_tolstring);
		//83 C4 08 6A 02 56 E8 
		g_lua_settop = (_lua_settop)(baseAddress + 0x027B40B0 - 0x00970000);
		PRT("g_lua_settop get:%x", g_lua_settop);
		Mhook_SetHook((PVOID*)&g_lua_newstate, mylua_newstate);//拦截文件
		Mhook_SetHook((PVOID*)&g_luaL_loadbuffer, myloadbuff);//拦截文件
	}
	CloseHandle((HANDLE)_beginthreadex(0, 0, GameHackStart, NULL, 0, 0));
	return;
}

unsigned int __stdcall GameHackStart(void *){
	if (n_hackStart) return 0;
	PRT("GameHackStart begin");
	char appPath[MAX_PATH];
	GetModuleFileNameA(NULL, appPath, MAX_PATH);
	PRT(appPath);
	//hook 消息函数
	//g_trueDefWinProc = DefWindowProcW;
	//Mhook_SetHook((PVOID*)&g_trueDefWinProc, NewDefWindowProc);
	g_hLuaOverSign = CreateEventA(0, FALSE, FALSE, NULL);
	//g_truePeekMessageW = PeekMessageW;
	//Mhook_SetHook((PVOID*)&g_truePeekMessageW, MyPeekMessageW);
	g_trueDefWinProc = DefWindowProcW;
	Mhook_SetHook((PVOID*)&g_trueDefWinProc, NewDefWindowProc);
	g_hCall = CreateEvent(NULL, FALSE, FALSE, NULL);
	//hook lua系列函数
	PRT("TG_GuiLib fun load ok");
	//CAutoConsole ac;
	InitGameWindow("Direct3DWindowClass");
	DWORD dwProcessid;
	g_dwGameWndThread = GetWindowThreadProcessId(g_hGame, &dwProcessid);
	//auto it = g_mapLuaState.find(dwThreadid);
	//while (it == g_mapLuaState.end()){
		//PRT("lua state not get");
		//Sleep(3000);
	//}
	//g_luaState = (lua_State *)it->second;
	//Sleep(20000);
	while (g_luaGame == NULL){
		Sleep(3000);
	}
	//Sleep(2000);
	PRT("now start tid:%d %x", g_dwGameWndThread, g_luaGame);
	/*
	//等待游戏初始化lua完毕
	WaitForSingleObject(g_hLuaOverSign, -1);
	{
		char sg[10] = { 0 };
		sprintf_s(sg, 10, "%d", g_hLuaOverSign);
		setglobal("c_luasign", sg);
	};
	*/
	PRT("g_hLuaOverSign ok");
	while (true){
		g_luaState = luaL_newstate();
		luaL_openlibs(g_luaState);
		InitLuaBase(g_luaState);
		size_t sz;
		const char* retstr = lmc_getfile("start", &sz);
		int ret = luaL_loadbuffer(g_luaState, retstr, sz, retstr) || lua_pcall(g_luaState, 0, LUA_MULTRET, 0);
		delete[] retstr;
		if (ret != 0){
			PRT("restart because");
			PRT(lua_tostring(g_luaState, -1));
			Sleep(3000);
		}
		Sleep(5000);
		PRT("start ok");
		lua_close(g_luaState);
	}
	CloseHandle(g_hCall);
	n_hackStart = true;
	return 0;
}

void GameHackEnd(){
	//hook 消息函数
	if (!n_hackStart) return;
	n_hackStart = false;
	Mhook_Unhook((PVOID*)&g_trueDefWinProc);
	DeleteCriticalSection(&g_csSafeCall);
	DeleteCriticalSection(&g_csGlobal);
	lua_close(g_luaState);
	return;
}