#include "stdafx.h"
#include <Psapi.h>
#include "function.h"
#include "../mhook-lib/mhook.h"
#include <winsock2.h>


string getglobal(string gname)
{
	CAutoSection cs(g_csGlobal);
	auto it = g_mapGlobal.find(gname);
	if (it == g_mapGlobal.end())
		return "";
	else
		return it->second;
}

void setglobal(string gname,string gvalue)
{
	CAutoSection cs(g_csGlobal);
	g_mapGlobal[gname] = string(gvalue);
}

bool n_hackStart = false;
typedef LRESULT(CALLBACK* _DefWindowProc)(__in HWND hWnd, __in UINT Msg, __in WPARAM wParam, __in LPARAM lParam);
_DefWindowProc g_trueDefWinProc = 0;
LRESULT CALLBACK NewDefWindowProc(
	__in HWND hWnd,
	__in UINT msg,
	__in WPARAM wParam,
	__in LPARAM lParam)
{
	if (hWnd == g_hGame)
	{
		printf("hwnd:%d,msg:%d process:%d thread:%d wp:%d \n", hWnd, msg, GetCurrentProcessId(), GetCurrentThreadId(), wParam);
		if (WM_WGCALL == msg && wParam == 123)
		{
			printf("hwnd:%d,msg:%d\n", hWnd, msg);
			//printDebugMessage("WM_WGCALL");
			ScopeGuard sgTmp(g_sgCallwapper);
			sgTmp.Dismiss(false);
			return 0;
		}
	}
	return g_trueDefWinProc(hWnd, msg, wParam, lParam);
}


typedef BOOL
(WINAPI *
_PeekMessageW)(
_Out_ LPMSG lpMsg,
_In_opt_ HWND hWnd,
_In_ UINT wMsgFilterMin,
_In_ UINT wMsgFilterMax,
_In_ UINT wRemoveMsg);
_PeekMessageW g_truePeekMessageW = NULL;

BOOL
WINAPI
MyPeekMessageW(
_Out_ LPMSG lpMsg,
_In_opt_ HWND hWnd,
_In_ UINT wMsgFilterMin,
_In_ UINT wMsgFilterMax,
_In_ UINT wRemoveMsg){
	if (g_bWaitCall && g_dwGameWndThread == GetCurrentThreadId())
	{
		g_bWaitCall = false;
		printf("get a call\n");
		{
			ScopeGuard sgTmp(g_sgCallwapper);
			sgTmp.Dismiss(false);
		}
		SetEvent(g_hCall);
	}
	return g_truePeekMessageW(lpMsg, hWnd, wMsgFilterMin, wMsgFilterMax, wRemoveMsg);
}

HOOKAPIPRE(PeekMessageA);
BOOL
WINAPI
myPeekMessageA(
_Out_ LPMSG lpMsg,
_In_opt_ HWND hWnd,
_In_ UINT wMsgFilterMin,
_In_ UINT wMsgFilterMax,
_In_ UINT wRemoveMsg){
	if (g_bWaitCall && g_dwGameWndThread == GetCurrentThreadId())
	{
		g_bWaitCall = false;
		printf("get a call\n");
		{
			ScopeGuard sgTmp(g_sgCallwapper);
			sgTmp.Dismiss(false);
		}
		SetEvent(g_hCall);
	}
	return truePeekMessageA(lpMsg, hWnd, wMsgFilterMin, wMsgFilterMax, wRemoveMsg);
}

unsigned int __stdcall  SocketProc(void *)
{
	mskL_SetProtectThreadId(GetCurrentThreadId());
	SOCKET socketArray[WSA_MAXIMUM_WAIT_EVENTS];
	WSAEVENT eventArray[WSA_MAXIMUM_WAIT_EVENTS];
	DWORD dwTotal = 0;  //记录接入socket的数量
	// 创建socket操作，建立流式套接字，返回套接字号sockClient  
	g_sockClient = socket(AF_INET, SOCK_STREAM, 0);
	if (g_sockClient == INVALID_SOCKET) {
		PRT("Error at socket():%ld", WSAGetLastError());
		return -1;
	}
	PRT("g_sockClient create OK");
	SOCKADDR_IN addrSrv;
	addrSrv.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");      // 本地回路地址是127.0.0.1;   
	addrSrv.sin_family = AF_INET;
	addrSrv.sin_port = htons(g_iSocketPort);
	int connecterr = 0;
	while (SOCKET_ERROR == connect(g_sockClient, (SOCKADDR*)&addrSrv, sizeof(SOCKADDR))){
		connecterr = WSAGetLastError();
		PRT("服务器连接失败:%d", connecterr);
		Sleep(5000);
	}
	PRT("g_sockClient connect OK");
	WSAEVENT newEvent = WSACreateEvent();
	WSAEventSelect(g_sockClient, newEvent, FD_READ | FD_CLOSE);
	socketArray[dwTotal] = g_sockClient;
	eventArray[dwTotal] = newEvent;
	dwTotal++;
	DWORD Index;
	char buf[2048];
	WSANETWORKEVENTS NetworkEvents;
	int recvret;
	while (dwTotal > 0)
	{
		Index = WSAWaitForMultipleEvents(dwTotal, eventArray, FALSE, -1, FALSE);
		WSAEnumNetworkEvents(socketArray[Index - WSA_WAIT_EVENT_0], eventArray[Index - WSA_WAIT_EVENT_0], &NetworkEvents);
		//WSAResetEvent(eventArray[Index - WSA_WAIT_EVENT_0]);
		if (NetworkEvents.lNetworkEvents & FD_CLOSE)
		{
			PRT("系统消息: 客户端退出。。。");
			closesocket(socketArray[Index - WSA_WAIT_EVENT_0]);
			WSACloseEvent(eventArray[Index - WSA_WAIT_EVENT_0]);
			for (int i = Index - WSA_WAIT_EVENT_0; i < dwTotal; i++)
			{
				socketArray[i] = socketArray[i + 1];
				eventArray[i] = eventArray[i + 1];
			}
			dwTotal--;
			g_sockClient = socket(AF_INET, SOCK_STREAM, 0);
			if (g_sockClient == INVALID_SOCKET) {
				PRT("Error at socket():%ld", WSAGetLastError());
				return -1;
			}
			PRT("g_sockClient create OK");
			SOCKADDR_IN addrSrv;
			addrSrv.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");      // 本地回路地址是127.0.0.1;   
			addrSrv.sin_family = AF_INET;
			addrSrv.sin_port = htons(g_iSocketPort);
			int connecterr = 0;
			while (SOCKET_ERROR == connect(g_sockClient, (SOCKADDR*)&addrSrv, sizeof(SOCKADDR))){
				connecterr = WSAGetLastError();
				PRT("服务器连接失败:%d", connecterr);
				Sleep(5000);
			}
			PRT("g_sockClient connect OK");

			newEvent = WSACreateEvent();
			WSAEventSelect(g_sockClient, newEvent, FD_READ | FD_CLOSE);
			socketArray[dwTotal] = g_sockClient;
			eventArray[dwTotal] = newEvent;
			dwTotal++;
		}
		else if (NetworkEvents.lNetworkEvents & FD_READ)
		{
			if (NetworkEvents.iErrorCode[FD_READ_BIT] != 0)
			{
				PRT("接受客户端数据失败。。。");
				continue;
			}
			PRT("系统消息: 数据到达。。。");
			memset(buf, 0, 2048);
			recvret = recv(socketArray[Index - WSA_WAIT_EVENT_0], (char *)buf, 2048, 0);
			if (recvret > 0){
				for (int i = 0; i < recvret; ++i){
					buf[i] ^= 'm';
				}
				//PRT("ConsoleData c:%s", buf);
				MSKCALL(
					PRT("now call back ConsoleData");
					g_lua_getfield(g_luaGame, LUA_GLOBALSINDEX, "ConsoleData");
					lua_pushlstring(g_luaGame, buf, recvret);
					int ret = g_lua_pcall(g_luaGame, 1, 0, 0);
					if (0 != ret){
						const char *err = g_lua_tolstring(g_luaGame, -1, NULL);
						PRT("call ConsoleData err:%s",err);
						g_lua_settop(g_luaGame, -2);
					}
				);
			}
		}
	}
	PRT("客户端监听结束");
	return 0;
}


HANDLE g_hLuaOverSign;
bool bLuaInit = false;
int myloadbuff(lua_State *L, const char *buff, size_t size,const char *name) 
{
	int ret = g_luaL_loadbuffer(L, buff, size, name);
#if 0
	if ( name && strstr(name, ".lua"))
	{
		char fname[MAX_PATH] = R"(G:\work\tg\luac\)";
		strcat(fname, name);
		PRT("write lua file now %s", fname);
		FILE *fp = fopen(fname, "w");
		fwrite(buff, size, 1, fp);
		fclose(fp);
		return ret;
	}
#endif
	if (name && strcmp(name, "Tooltip.lua") == 0){
		//PRT("Tooltip get,now wait lua sign");
		//SignalObjectAndWait(g_hLuaOverSign, g_hLuaOverSign, -1, FALSE);
		//SetEvent(g_hLuaOverSign);
		//PRT("now start gamestart");
		/*
		string fname = getglobal("c_startfile");
		int ret = (g_luaL_loadfile(g_luaGame, fname.c_str()) || g_lua_pcall(g_luaGame, 0, LUA_MULTRET, 0));
		if (0 != ret){
			const char *err = g_lua_tolstring(g_luaGame, -1, NULL);
			printDebugMessage(err);
			g_lua_settop(g_luaGame, -2);
		}
		*/
		size_t sz;
		const char* dstring = lmc_getfile("gamestart",&sz);
		int ret = (g_luaL_loadbuffer(g_luaGame, dstring, sz, dstring) || g_lua_pcall(g_luaGame, 0, LUA_MULTRET, 0));
		if (0 != ret){
			const char *err = g_lua_tolstring(g_luaGame, -1, NULL);
			PRT("gamestart load error:%s",err);
			g_lua_settop(g_luaGame, -2);
		}
		//CloseHandle(g_hLuaOverSign);
		g_hLuaOverSign = NULL;
		bLuaInit = true;
		//printDebugMessage("loadbuff1 now ok");
		CloseHandle((HANDLE)_beginthreadex(0, 0, SocketProc, NULL, 0, 0));
	}
	else if (name && strstr(name, ".lua")){
		if (!bLuaInit){
			PRT(name);
		}
		else{
			char dstring[4096] = { 0 };
			sprintf_s(dstring, 4096, "FileLoadCallBack(\"%s\")", name);
			int ret = (g_luaL_loadbuffer(g_luaGame, dstring, strlen(dstring), dstring) || g_lua_pcall(g_luaGame, 0, LUA_MULTRET, 0));
			if (0 != ret){
				const char *err = g_lua_tolstring(g_luaGame, -1, NULL);
				PRT(err);
				g_lua_settop(g_luaGame, -2);
			}
		}
	}
	return ret;
}

lua_State *mylua_newstate(lua_Alloc f, void *ud)
{
	g_luaGame = g_lua_newstate(f, ud);
	InitGameLuaState(g_luaGame);
	PRT("state now ok");
	Mhook_Unhook((PVOID*)&g_lua_newstate);
	return g_luaGame;
}

HOOKAPIPRE(GetTickCount);
DWORD WINAPI myGetTickCount()
{
	if (g_bWaitCall && g_dwGameWndThread == GetCurrentThreadId())
	{
		g_bWaitCall = false;
		printf("get a call\n");
		{
			ScopeGuard sgTmp(g_sgCallwapper);
			sgTmp.Dismiss(false);
		}
		SetEvent(g_hCall);
	}
	return trueGetTickCount();
}

unsigned int __stdcall GameHackStart(void *){
	if (n_hackStart) return 0;
	//mskL_SetProtectThreadId(GetCurrentThreadId());
	PRT("GameHackStart begin");
	char appPath[MAX_PATH];
	GetModuleFileNameA(NULL, appPath, MAX_PATH);
	PRT(appPath);
	//hook 消息函数
	//g_trueDefWinProc = DefWindowProcW;
	//Mhook_SetHook((PVOID*)&g_trueDefWinProc, NewDefWindowProc);
	g_hLuaOverSign = CreateEventA(0, FALSE, FALSE, NULL);
	g_truePeekMessageW = PeekMessageW;
	Mhook_SetHook((PVOID*)&g_truePeekMessageW, MyPeekMessageW);
	HOOKAPI(PeekMessageA);
	//HOOKAPI(GetTickCount);
	g_hCall = CreateEvent(NULL, FALSE, FALSE, NULL);
	//hook lua系列函数
	HMODULE hDll = LoadLibraryA("TG_GuiLib.dll");
	if (hDll){
		g_luaL_loadbuffer = (_luaL_loadbuffer)GetProcAddress(hDll, "luaL_loadbuffer");
		g_luaL_loadfile = (_luaL_loadfile)GetProcAddress(hDll, "luaL_loadfile");
		g_lua_pcall = (_lua_pcall)GetProcAddress(hDll, "lua_pcall");
		g_luaL_register = (_luaL_register)GetProcAddress(hDll, "luaL_register");
		g_lua_newstate = (_lua_newstate)GetProcAddress(hDll, "lua_newstate");
		g_lua_tolstring = (_lua_tolstring)GetProcAddress(hDll, "lua_tolstring");
		g_lua_settop = (_lua_settop)GetProcAddress(hDll, "lua_settop");
		g_lua_getfield = (_lua_getfield)GetProcAddress(hDll, "lua_getfield");
		Mhook_SetHook((PVOID*)&g_lua_newstate, mylua_newstate);//拦截文件
		Mhook_SetHook((PVOID*)&g_luaL_loadbuffer, myloadbuff);//拦截文件
		FreeLibrary(hDll);
	}
	PRT("TG_GuiLib fun load ok");
	char sg[10] = { 0 };
	sprintf_s(sg, 10, "%d", g_hLuaOverSign);
	setglobal("c_luasign", sg);
	return 0;
	//CAutoConsole ac;
	//等待游戏初始化lua完毕
	WaitForSingleObject(g_hLuaOverSign, -1);
	{
		char sg[10] = { 0 };
		sprintf_s(sg, 10, "%d", g_hLuaOverSign);
		setglobal("c_luasign", sg);
	};
	PRT("WaitForSingleObject g_hLuaOverSign ok");
	//PRT("g_hLuaOverSign ok");
	//while (true){
	//	g_luaState = luaL_newstate();
	//	luaL_openlibs(g_luaState);
	//	InitLuaBase(g_luaState);
	//	size_t sz;
	//	const char* retstr = lmc_getfile("start",&sz);
	//	int ret = luaL_loadbuffer(g_luaState, retstr, sz, retstr) || lua_pcall(g_luaState, 0, LUA_MULTRET, 0);
	//	delete[] retstr;
	//	if (ret != 0){
	//		PRT("restart because");
	//		PRT(lua_tostring(g_luaState, -1));
	//		Sleep(3000);
	//	}
	//	lua_close(g_luaState);
	//}
	//CloseHandle(g_hCall);
	//n_hackStart = true;
	return 0;
}

void GameHackEnd(){
	//hook 消息函数
	if (!n_hackStart) return;
	n_hackStart = false;
	Mhook_Unhook((PVOID*)&g_trueDefWinProc);
	DeleteCriticalSection(&g_csSafeCall);
	DeleteCriticalSection(&g_csGlobal);
	lua_close(g_luaState);
	return;
}