// dll_template.cpp : 定义 DLL 应用程序的导出函数。
//
/************************************************************************/
/* 基本函数 通用                                                              
/************************************************************************/
#include "stdafx.h"
#include <Psapi.h>
#include <atlconv.h>
#include "function.h"
#include "../mhook-lib/mhook.h"
//#include "../MyRSA/MyRSA.h"
//#include "../Crypto++/des.h"
//using namespace CryptoPP;
//#pragma comment(lib, "../Crypto++/cryptlib.lib")
struct _LockId
{
	DWORD id;
	int nlock;
};
/*
#pragma data_seg("SHARED_OPCDATASET")//名称可以
_LockId share_LockObj[30] = { 0 };
int dwLockIdx = 0;
#pragma data_seg()
#pragma comment(linker,"/SECTION:SHARED_OPCDATASET,RWS")
*/
HANDLE m_hMapFile;
void* m_pBaseMapFile;
#define SHARE_SIZE sizeof(share_LockObj)/sizeof(_LockId)
typedef unsigned char byte;
//c function by msk
unsigned __stdcall cm_runfunction(void* L)
{
	lua_State *nl = (lua_State *)L;
	int ret = lua_pcall(nl,0,0,0);
	if(ret != 0)
	{
		printf("cm_runfunction error:%s\n",lua_tostring(nl,-1));
		lua_pop(nl,1);
	}
	lua_close(nl);
	return 0;
}
//跳转模板
__declspec(naked) void cm_jmptemplate()
{
	
}

template <typename T>
T _CommonCall(DWORD callAddress, DWORD ecxAddress, DWORD *argArray, int len)
{
	T retInt;
	DWORD dEsp = len * 4;
	__asm pushad
	__asm{
		mov ecx, len
			mov edx, 0
			mov eax, argArray
		PUSHARG :
		cmp edx, ecx
			jae GAMECALL
			push[eax + edx * 4]
			inc edx
			jmp PUSHARG
		GAMECALL :
		mov ecx, ecxAddress
			mov eax, callAddress
			SAFECALL(eax)
		ENDCODE :
				mov retInt, eax
	}
	if (ecxAddress == 0x12345678)
		__asm add esp, dEsp
	__asm popad
	return retInt;
}

BOOL CALLBACK EnumGameWindowsProc(HWND hwnd, LPARAM lParam)
{
	char classNamep[255] = { 0 };
	GetClassNameA(hwnd, classNamep, 255 * sizeof(TCHAR));
	const char *className = (const char *)lParam;
	if (0 == g_dwGameWndThread && strstr(classNamep, className))
	{
		DWORD dThreadId, dProcessId;
		dThreadId = GetWindowThreadProcessId(hwnd, &dProcessId);
		if (dProcessId == GetCurrentProcessId())
		{
			g_hGame = hwnd;
			char windowTxt[MAX_PATH] = { 0 };
			g_dwGameWndThread = GetWindowThreadProcessId(hwnd, NULL);
			GetWindowTextA(hwnd, windowTxt, MAX_PATH);
			printf("get the gamewnd:%d %s %s\n", g_dwGameWndThread, className, windowTxt);
			char msk[255] = { 0 };
			//::ShowWindow(g_hGame, SW_MINIMIZE);
			//char msg[1024] = { 0 };
			//sprintf(msg, "g_hGame = %x", hwnd);
			//printDebugMessage(msg);
			return FALSE;
		}
	}
	return TRUE;
}
//CD3DWnd g_dlgD3D;
bool InitGameWindow(const char *className)
{
	if (g_dwGameWndThread != 0) return true;
	while (g_dwGameWndThread == 0)
	{
		Sleep(5000);
		EnumWindows(EnumGameWindowsProc, (LPARAM)className);
	}
	return g_dwGameWndThread != 0;
}
/*
5B 89 1D 1C 6C 9D 74 5B FF D3 FF 25 1C 6C 9D 74


*/
bool g_bSafeCallInit = false;
UCHAR g_ucSafeCallJmpSaved[16] = { 0 };
bool InitSafeCall(DWORD address)
{
	static int s_iRet;
	if (g_bSafeCallInit) return true;
	g_dwSafeCallJmp = address;
	DWORD old;
	VirtualProtect((LPVOID)g_dwSafeCallJmp, 16, PAGE_EXECUTE_READWRITE, &old);
	memcpy(g_ucSafeCallJmpSaved, (void *)g_dwSafeCallJmp, 15);
	BYTE *pByte = (BYTE *)g_dwSafeCallJmp;
	*pByte = 0x5b;//pop eax                                  			;  弹出返回地址
	pByte++;
	*pByte = 0x89;
	pByte++;
	*pByte = 0x1d;
	pByte++;
	*(DWORD *)pByte = (DWORD)&s_iRet;
	pByte += 4;
	*pByte = 0x5b;
	pByte++;
	*pByte = 0xff;
	pByte++;
	*pByte = 0xd3;
	pByte++;
	*pByte = 0xff;
	pByte++;
	*pByte = 0x25;
	pByte++;
	*(DWORD *)pByte = (DWORD)&s_iRet;
	pByte += 4;
	VirtualProtect((LPVOID)g_dwSafeCallJmp, 16, old, &old);
	g_bSafeCallInit = true;
	return g_bSafeCallInit;
}
bool InitSafeCall2(DWORD address)
{
	static int s_iRet;
	if (g_bSafeCallInit) return true;
	g_dwSafeCallJmp = address;
	DWORD old;
	VirtualProtect((LPVOID)g_dwSafeCallJmp, 15, PAGE_EXECUTE_READWRITE, &old);
	memcpy(g_ucSafeCallJmpSaved, (void *)g_dwSafeCallJmp, 15);
	BYTE *pByte = (BYTE *)g_dwSafeCallJmp;
	*pByte = 0x58;//pop eax                                  			;  弹出返回地址
	pByte++;
	*pByte = 0xa3;
	pByte++;
	*(DWORD *)pByte = (DWORD)&s_iRet;
	pByte += 4;
	*pByte = 0x58;
	pByte++;
	*pByte = 0xff;
	pByte++;
	*pByte = 0xd0;
	pByte++;
	*pByte = 0xff;
	pByte++;
	*pByte = 0x25;
	pByte++;
	*(DWORD *)pByte = (DWORD)&s_iRet;
	pByte += 4;
	VirtualProtect((LPVOID)g_dwSafeCallJmp, 15, old, &old);
	g_bSafeCallInit = true;
	return g_bSafeCallInit;
}

int lastIdx = -1;
int lm_lockobj(lua_State *L)
{
	if (INVALID_HANDLE_VALUE == m_hMapFile)
		return 0;
	auto share_LockObj = (_LockId *)((int)m_pBaseMapFile + sizeof(int));
	auto dwLockIdx = *(int *)m_pBaseMapFile;
	DWORD dwId = lua_tonumber(L, -1);
	if (lastIdx != -1)
		share_LockObj[lastIdx].nlock--;
	lastIdx = dwLockIdx;
	for (int i = 0; i < SHARE_SIZE; i++)
	{
		if (share_LockObj[dwLockIdx].id == dwId){
			share_LockObj[dwLockIdx].nlock++;
			return 0;
		}
	}
	share_LockObj[dwLockIdx].id = dwId;
	share_LockObj[dwLockIdx].nlock = 1;
	if (dwLockIdx == SHARE_SIZE - 1)
		dwLockIdx = 0;
	else
		dwLockIdx++;
	*(int *)m_pBaseMapFile = dwLockIdx;
	return 0;
}

int lm_isobjlock(lua_State *L)
{
	if (INVALID_HANDLE_VALUE != m_hMapFile)
	{
		auto share_LockObj = (_LockId *)((int)m_pBaseMapFile + sizeof(int));
		DWORD dwId = lua_tonumber(L, -1);
		for (int i = 0; i < SHARE_SIZE; ++i){
			if (share_LockObj[i].id == dwId && share_LockObj[i].nlock > 0){
				lua_pushboolean(L, 1);
				return 1;
			}
		}
	}
	lua_pushboolean(L, 0);
	return 1;
}

int lm_unlockobj(lua_State *L)
{
	if (INVALID_HANDLE_VALUE == m_hMapFile)
		return 0;
	auto share_LockObj = (_LockId *)((int)m_pBaseMapFile + sizeof(int));
	DWORD dwId = lua_tonumber(L, -1);
	for (int i = 0; i < SHARE_SIZE; ++i){
		if (share_LockObj[i].id == dwId){
			share_LockObj[i].nlock--;
			break;
		}
	}
	return 0;
}

//lua function by msk
int lm_newthread(lua_State *L)
{
	/*
	lua_State *nl = luaL_newstate();
	luaL_openlibs(nl);
	InitLuaBase(nl);
	lua_pushlightuserdata(L,nl);
	*/
	//lua_State *nl = lua_newthread(L);
	return 1;
}

int lm_setglobal(lua_State *L)
{
	CAutoSection cs(g_csGlobal);
	char *gname = (char *)lua_tostring(L, 1);
	char *gvalue = (char *)lua_tostring(L, 2);
	g_mapGlobal[gname] = string(gvalue);
	return 0;
}

int lm_getglobal(lua_State *L)
{
	CAutoSection cs(g_csGlobal);
	const char *gname = lua_tostring(L, 1);
	auto it = g_mapGlobal.find(gname);
	if (it == g_mapGlobal.end())
		lua_pushstring(L, "");
	else
		lua_pushstring(L, it->second.c_str());
	return 1;
}

int lm_pid(lua_State *L)
{
	lua_pushinteger(L, GetCurrentProcessId());
	return 1;
}

int lm_tid(lua_State *L)
{
	lua_pushinteger(L, GetCurrentThreadId());
	return 1;
}

int lm_runthread(lua_State *L)
{
	lua_State *nl = lua_tothread(L,1);
	lua_xmove(L,nl,1);
	_beginthreadex(0,0,cm_runfunction,nl,0,0);
	return 0;
}

int lm_todword(lua_State *L)
{
	__try{
		DWORD addrss = (DWORD)lua_tonumber(L,1);
		lua_pushinteger(L,*(DWORD *)addrss);
	}
	__except(EXCEPTION_EXECUTE_HANDLER ){
		lua_pushinteger(L,0);
	}
	return 1;
}

int lm_toword(lua_State *L)
{
	__try{
		DWORD addrss = (DWORD)lua_tonumber(L,1);
		lua_pushinteger(L,*(WORD *)addrss);
	}
	__except(EXCEPTION_EXECUTE_HANDLER ){
		lua_pushinteger(L,0);
	}
	return 1;
}

int lm_tobyte(lua_State *L)
{
	__try{
		DWORD addrss = (DWORD)lua_tonumber(L,1);
		lua_pushinteger(L,*(byte *)addrss);
	}
	__except(EXCEPTION_EXECUTE_HANDLER ){
		lua_pushinteger(L,0);
	}
	return 1;
}

int lm_tobool(lua_State *L)
{
	__try{
		DWORD addrss = (DWORD)lua_tonumber(L,1);
		lua_pushboolean(L,*(byte *)addrss);
	}
	__except(EXCEPTION_EXECUTE_HANDLER ){
		lua_pushboolean(L,0);
	}
	return 1;
}

int lm_tofloat(lua_State *L)
{
	__try{
		DWORD addrss = (DWORD)lua_tonumber(L,1);
		lua_pushnumber(L,*(float *)addrss);
	}
	__except(EXCEPTION_EXECUTE_HANDLER ){
		lua_pushnumber(L,0);
	}
	return 1;
}

int lm_todouble(lua_State *L)
{
	__try{
		DWORD addrss = (DWORD)lua_tonumber(L,1);
		lua_pushnumber(L,*(double *)addrss);
	}
	__except(EXCEPTION_EXECUTE_HANDLER ){
		lua_pushnumber(L,0);
	}
	return 1;
}

int lm_tolonglong(lua_State *L)
{
	__try{
		LONGLONG *addrss = (LONGLONG *)(DWORD)lua_tonumber(L, 1);
		lua_pushnumber(L, *addrss);
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
		lua_pushnumber(L, 0);
	}
	return 1;
}

int lm_todata(lua_State *L)
{
	__try{
		const char *addrss = (char *)(DWORD)lua_tonumber(L, 1);
		size_t sz = lua_tointeger(L,2);
		lua_pushlstring(L,addrss,sz);
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
		lua_pushnumber(L, 0);
	}
	return 1;
}

int lm_setdata(lua_State *L)
{
	__try{
		char *addrss = (char *)(DWORD)lua_tonumber(L, 1);
		const char *data = lua_tostring(L, 2);
		strcpy(addrss,data);
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
	}
	return 0;
}

int lm_changetolonglong(lua_State *L)
{
	__try{
		LONGLONG ret = lua_tonumber(L, 1);
		lua_pushnumber(L, ret);
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
		lua_pushnumber(L, 0);
	}
	return 1;
}

int lm_changetodouble(lua_State *L)
{
	__try{
		double addrss = lua_tonumber(L, 1);
		lua_pushnumber(L, addrss);
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
		lua_pushnumber(L, 0);
	}
	return 1;
}

int lm_copydata(lua_State *L)
{
	__try{
		void *addrss = (void *)(DWORD)lua_tonumber(L, 1);
		const void *addrss2 = (void *)(DWORD)lua_tonumber(L, 2);
		size_t sz = lua_tointeger(L, 3);
		memcpy(addrss, addrss2, sz);
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
	}
	return 0;
}

int lm_setdword(lua_State *L)
{
	__try{
		DWORD *addrss = (DWORD *)(DWORD)lua_tonumber(L, 1);
		DWORD var = lua_tointeger(L, 2);
		*addrss = var;
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
	}
	return 0;
}

int lm_setword(lua_State *L)
{
	__try{
		WORD *addrss = (WORD *)(DWORD)lua_tonumber(L, 1);
		WORD var = lua_tointeger(L, 2);
		*addrss = var;
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
	}
	return 0;
}

int lm_setbyte(lua_State *L)
{
	__try{
		byte *addrss = (byte *)(DWORD)lua_tonumber(L, 1);
		byte var = lua_tointeger(L, 2);
		*addrss = var;
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
	}
	return 0;
}

int lm_setbool(lua_State *L)
{
	__try{
		bool *addrss = (bool *)(DWORD)lua_tonumber(L, 1);
		bool var = lua_tointeger(L, 2);
		*addrss = var;
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
	}
	return 0;
}

int lm_setfloat(lua_State *L)
{
	__try{
		float *addrss = (float *)(DWORD)lua_tonumber(L, 1);
		float var = lua_tonumber(L ,2);
		*addrss = var;
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
	}
	return 0;
}

int lm_setdouble(lua_State *L)
{
	__try{
		double *addrss = (double *)(DWORD)lua_tonumber(L, 1);
		double var = lua_tonumber(L, 2);
		*addrss = var;
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
	}
	return 0;
}

int lm_setlonglong(lua_State *L)
{
	__try{
		LONGLONG *addrss = (LONGLONG *)(DWORD)lua_tonumber(L, 1);
		LONGLONG var = lua_tonumber(L, 2);
		*addrss = var;
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
	}
	return 0;
}

int lm_swstr(lua_State *L)
{
	DWORD memAddress = lua_tonumber(L, 1);
	__try{
		const char * str = lua_tostring(L, 2);
		USES_CONVERSION;
		wcscpy((wchar_t *)memAddress, A2W(str));
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
	}
	return 0;
}

int lm_new(lua_State *L)
{
	size_t len = lua_tointeger(L, 1);
	size_t ret = (size_t)new byte[len];
	memset((void *)ret, 0, len);
	lua_pushinteger(L, ret);
	return 1;
}

int lm_delete(lua_State *L)
{
	DWORD memAddress = lua_tointeger(L, 1);
	delete[](byte *)memAddress;
	return 0;
}

int lm_sleep(lua_State *L)
{
	DWORD tm = lua_tointeger(L, 1);
	Sleep(tm);
	return 0;
}

int lm_w2a(lua_State *L)
{
	size_t len;
	const wchar_t *str = (wchar_t*)lua_tolstring(L, 1, &len);
	USES_CONVERSION;
	lua_pushstring(L, W2A(str));
	return 1;
}

int lm_a2w(lua_State *L)
{
	size_t len;
	const char *str = lua_tostring(L, 1);
	USES_CONVERSION;
	const wchar_t *wstr = A2W(str);
	lua_pushlstring(L, (char *)wstr, wcslen(wstr) * 2 + 2);
	return 1;
}

int lm_a2u8(lua_State *L)
{

	const char *szGbk = lua_tostring(L, -1);
	char *tmp = new char[strlen(szGbk) * 3 + 10];
	memset(tmp, 0, strlen(szGbk) * 3 + 10);
	int n = MultiByteToWideChar(CP_ACP, 0, szGbk, -1, NULL, 0);
	// 字符数乘以 sizeof(WCHAR) 得到字节数  
	WCHAR *str1 = new WCHAR[sizeof(WCHAR)* n];
	// 转换  
	MultiByteToWideChar(CP_ACP,  // MultiByte的代码页Code Page  
		0,            //附加标志，与音标有关  
		szGbk,        // 输入的GBK字符串  
		-1,           // 输入字符串长度，-1表示由函数内部计算  
		str1,         // 输出  
		n             // 输出所需分配的内存  
		);

	// 再将宽字符（UTF-16）转换多字节（UTF-8）  
	n = WideCharToMultiByte(CP_UTF8, 0, str1, -1, NULL, 0, NULL, NULL);
	WideCharToMultiByte(CP_UTF8, 0, str1, -1, tmp, n, NULL, NULL);
	lua_pushlstring(L, tmp, strlen(tmp));
	delete[]str1;
	str1 = NULL;
	delete[]tmp;
	return 1;
}

int lm_u82a(lua_State *L)
{
	DWORD memAddress = lua_tonumber(L, -2);
	size_t len = lua_tonumber(L, -1);
	char szUtf8[2048] = { 0 };
	memcpy(szUtf8, (void *)memAddress, len);
	int n = MultiByteToWideChar(CP_UTF8, 0, szUtf8, -1, NULL, 0);
	char *tmp = new char[n + 10];
	memset(tmp, 0, n + 10);
	WCHAR * wszGBK = new WCHAR[sizeof(WCHAR)* n];
	memset(wszGBK, 0, sizeof(WCHAR)* n);
	MultiByteToWideChar(CP_UTF8, 0, szUtf8, -1, wszGBK, n);

	n = WideCharToMultiByte(CP_ACP, 0, wszGBK, -1, NULL, 0, NULL, NULL);
	WideCharToMultiByte(CP_ACP, 0, wszGBK, -1, tmp, n, NULL, NULL);

	delete[]wszGBK;
	wszGBK = NULL;
	lua_pushstring(L, tmp);
	delete[]tmp;
	return 1;
}

int lm_towstr(lua_State *L)
{
	__try{
		DWORD addrss = (DWORD)lua_tonumber(L, 1);
		lua_pushlstring(L, (char *)addrss, wcslen((wchar_t *)addrss) * 2);
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
		return 0;
	}
	return 1;
}

int lm_tostr(lua_State *L)
{
	__try{
		DWORD addrss = (DWORD)lua_tonumber(L, 1);
		lua_pushstring(L, (char *)addrss);
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
		return 0;
	}
	return 1;
}
//windows 内核对象
int lm_closehandle(lua_State *L){
	HANDLE hc = (HANDLE)(DWORD)lua_tonumber(L, 1);
	for (auto it : g_arrHandle){
		if (it == hc){
			g_arrHandle.erase(hc);
			break;
		}
	}
	CloseHandle(hc);
	return 0;
}

int lm_createevent(lua_State *L){
	HANDLE hEvent = CreateEventA(NULL, lua_tointeger(L, 1), lua_tointeger(L, 2), lua_tostring(L, 3));
	lua_pushlightuserdata(L, hEvent);
	g_arrHandle.insert(hEvent);
	return 1;
}

int lm_createmutex(lua_State *L){
	HANDLE hMutex = CreateMutexA(NULL, lua_tointeger(L, 1), lua_tostring(L, 2));
	lua_pushlightuserdata(L, hMutex);
	g_arrHandle.insert(hMutex);
	return 1;
}

int lm_setevent(lua_State *L){
	HANDLE hEvent = (HANDLE)(DWORD)lua_tonumber(L, 1);
	SetEvent(hEvent);
	return 0;
}

int lm_signalandwait(lua_State *L){
	HANDLE hEvent = (HANDLE)(DWORD)lua_tonumber(L, 1);
	HANDLE hEvent2 = (HANDLE)(DWORD)lua_tonumber(L, 2);
	DWORD ret = SignalObjectAndWait(hEvent, hEvent2, lua_tointeger(L, 3),FALSE );
	lua_pushnumber(L, ret);
	return 1;
}


int lm_waithandle(lua_State *L){
	DWORD ret = WaitForSingleObject((HANDLE)(DWORD)lua_tonumber(L, 1), lua_tointeger(L, 2));
	lua_pushinteger(L,ret);
	return 1;
}

int lm_waitmultihandle(lua_State *L){
	HANDLE arrHandle[MAX_PATH] = { 0 };
	DWORD dwCount = lua_gettop(L) - 2;
	for (int i = 0; i < dwCount; ++i){
		arrHandle[i] = (HANDLE)(DWORD)lua_tonumber(L, i + 3);
	}
	DWORD ret = WaitForMultipleObjects(dwCount, arrHandle, lua_tointeger(L, 1), lua_tointeger(L, 2));
	lua_pushinteger(L, ret);
	return 1;
}

int lm_createmapfile(lua_State *L)
{
	const char *filename = lua_tostring(L, 1);
	int sz = lua_tointeger(L,2);
	HANDLE hmapfile = CreateFileMappingA(INVALID_HANDLE_VALUE,
		NULL,
		PAGE_READWRITE,
		0,
		sz,
		filename);
	if (NULL == hmapfile)
	{
		return 0;
	}
	LPVOID pmapfile = MapViewOfFile(hmapfile,
		FILE_MAP_ALL_ACCESS,
		0,
		0,
		0);
	if (NULL == pmapfile) {
		CloseHandle(hmapfile);
		return 0;
	}
	lua_pushnumber(L, (DWORD)hmapfile);
	lua_pushnumber(L, (DWORD)pmapfile);
	return 2;
}

int lm_closemapfile(lua_State *L)
{
	HANDLE hmapfile = (HANDLE)(DWORD)lua_tonumber(L, 1);
	LPVOID pmapfile = (LPVOID)(DWORD)lua_tonumber(L, 2);
	UnmapViewOfFile(pmapfile);
	CloseHandle(hmapfile);
	return 0;
}

int lm_getmodulecalladdress(lua_State *L)
{
	const char* mname = lua_tostring(L, 1);
	DWORD callAddress = 0;
	if (lua_isnumber(L, 2)){
		DWORD index = (DWORD)(lua_tonumber(L, 2));
		callAddress = (DWORD)GetProcAddress(GetModuleHandleA(mname), (char *)(index));
	}
	else{
		callAddress = (DWORD)GetProcAddress(GetModuleHandleA(mname), lua_tostring(L, 2));
	}
	lua_pushnumber(L, callAddress);
	return 1;
}

int lm_getmodulebase(lua_State *L)
{
	const char* name = lua_tostring(L, -1);
	HMODULE  hFind = GetModuleHandleA(name);
	MODULEINFO mi;
	memset(&mi, 0, sizeof(MODULEINFO));
	GetModuleInformation(GetCurrentProcess(), hFind, &mi, sizeof(mi));
	lua_pushnumber(L, (DWORD)hFind);
	lua_pushnumber(L, mi.SizeOfImage);
	return 2;
}

int lm_createsection(lua_State *L){
	CRITICAL_SECTION *pNewCs = new CRITICAL_SECTION;
	InitializeCriticalSection(pNewCs);
	lua_pushlightuserdata(L, pNewCs);
	return 1;
}

int lm_closesection(lua_State *L){
	CRITICAL_SECTION *pNewCs = (CRITICAL_SECTION *)lua_touserdata(L, 1);
	if (pNewCs == NULL){ return 0; };
	DeleteCriticalSection(pNewCs);
	pNewCs = NULL;
	return 0;
}

int lm_entersection(lua_State *L){
	CRITICAL_SECTION *pNewCs = (CRITICAL_SECTION *)lua_touserdata(L, 1);
	if (pNewCs == NULL){ return 0; };
	EnterCriticalSection(pNewCs);
	return 0;
}

int lm_leavesection(lua_State *L){
	CRITICAL_SECTION *pNewCs = (CRITICAL_SECTION *)lua_touserdata(L, 1);
	if (pNewCs == NULL){ return 0; };
	LeaveCriticalSection(pNewCs);
	return 0;
}


int lm_call(lua_State *L)
{
	DWORD callAddress = (DWORD)lua_tonumber(L, 1);
	DWORD ecxAddress = (DWORD)lua_tonumber(L, 2);
	int retTpye = (int)lua_tonumber(L, 3);
	int len = (int)lua_gettop(L) - 3;
	DWORD argArray[10] = { 0 };
	for (int i = 0; i < len; i++)
		argArray[i] = (DWORD)lua_tonumber(L, i + 4);
	int retInt = 0;
	MSKCALL(retInt = _CommonCall<int>(callAddress, ecxAddress, argArray, len););
	if (retTpye == E_V_UNICODE){
		USES_CONVERSION;
		lua_pushstring(L, W2A((wchar_t *)retInt));
	}
	else if (retTpye == E_V_ASCII){
		lua_pushstring(L, (char *)retInt);
	}
	else if (retTpye == E_V_BYTE){
		lua_pushnumber(L, (byte)retInt);
	}
	else if (retTpye == E_V_WORD){
		lua_pushnumber(L, (WORD)retInt);
	}
	else if (retTpye == E_V_DWORD){
		lua_pushnumber(L, (DWORD)retInt);
	}
	//CheckCrack();
	return 1;
}

int lm_qcall(lua_State *L)
{
	__try{
		DWORD callAddress = (DWORD)lua_tonumber(L, 1);
		DWORD ecxAddress = (DWORD)lua_tonumber(L, 2);
		int retTpye = (int)lua_tonumber(L, 3);
		int len = (int)lua_gettop(L) - 3;
		DWORD argArray[10] = { 0 };
		for (int i = 0; i < len; i++)
			argArray[i] = (DWORD)lua_tonumber(L, i + 4);
		int retInt = 0;
		retInt = _CommonCall<int>(callAddress, ecxAddress, argArray, len);
		if (retTpye == E_V_UNICODE){
			USES_CONVERSION;
			lua_pushstring(L, W2A((wchar_t *)retInt));
		}
		else if (retTpye == E_V_ASCII){
			lua_pushstring(L, (char *)retInt);
		}
		else if (retTpye == E_V_BYTE){
			lua_pushnumber(L, (byte)retInt);
		}
		else if (retTpye == E_V_WORD){
			lua_pushnumber(L, (WORD)retInt);
		}
		else if (retTpye == E_V_DWORD){
			lua_pushnumber(L, (DWORD)retInt);
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
		PRT("lm_qcall error");
		lua_pushnil(L);
		//lua_pushnumber(L, 0);
	}
	return 1;
}

int lm_print(lua_State *L){
	//return 0;
	string strPrint;// = "msk_tg:";
	for (int i = 1; i <= lua_gettop(L); ++i){
		strPrint += lua_tostring(L, i);
		strPrint += " ";
	}
	MyDbgPrint(strPrint.c_str());
	return 0;
}

int lm_hook(lua_State *L){
	return 0;
}

int lm_unhook(lua_State *L){
	return 0;
}

int lm_initdelaydata(lua_State *L){
	const char *cname = lua_tostring(L, 1);
	DWORD safeCallAddress = lua_tointeger(L, 2);
	InitSafeCall(safeCallAddress);
	InitGameWindow(cname);
	return 0;
}

int lm_gamedofile(lua_State *L){
	if (NULL == g_luaGame) return 0;
	const char *fname = lua_tostring(L, 1);
	char serr[1024] = { 0 };
	int ret = 0;
	MSKCALL(
		ret = (g_luaL_loadfile(g_luaGame, fname) || g_lua_pcall(g_luaGame, 0, LUA_MULTRET, 0));
		if (0 != ret){
			const char *err = g_lua_tolstring(g_luaGame, -1, NULL);
			strcpy_s(serr, 1024, err);
			g_lua_settop(g_luaGame, -2);
		}
	);
	if (strlen(serr)){
		lua_pushboolean(L, 0);
		lua_pushstring(L, serr);
		return 2;
	}
	lua_pushboolean(L, 1);
	return 1;
}

int lm_gamedostring(lua_State *L){
	if (NULL == g_luaGame) return 0;
	const char *buff = lua_tostring(L, 1);
	char serr[1024] = {0};
	int ret = 0;
	MSKCALL(
		ret = g_luaL_loadbuffer(g_luaGame, buff, strlen(buff),buff) || g_lua_pcall(g_luaGame, 0, LUA_MULTRET, 0);
	if (0 != ret){
		const char *err = g_lua_tolstring(g_luaGame, -1, NULL);
		strcpy_s(serr, 1024, err);
		g_lua_settop(g_luaGame, -2);
	}
	);
	if (strlen(serr)){
		lua_pushboolean(L, 0);
		lua_pushstring(L, serr);
		return 2;
	}
	lua_pushboolean(L, 1);
	return 1;
}
char *lmc_readfile(const char *fname, size_t *sz = NULL)
{
	string strconsole = getglobal("mskdata");
	if (strconsole == "") return 0;
	strconsole += ":";
	strconsole += fname;
	PRT("try readfile:%s", strconsole.c_str());
	USES_CONVERSION;
	HANDLE hOpenFile = MyCreateFileW(A2W(strconsole.c_str()), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == hOpenFile) {
		*sz = 0;
		return 0;
	}
	char *pBuffer;
	DWORD RSize;
	DWORD fileSize = 0;
	fileSize = GetFileSize(hOpenFile, NULL);
	pBuffer = new char[fileSize];
	ReadFile(hOpenFile, pBuffer, fileSize, &RSize, NULL);
	CloseHandle(hOpenFile);
	PRT("readfile:%s size:%d", fname, RSize);
	if (sz) *sz = RSize;
	return pBuffer;
}


//string lmc_rasdecode(const char * ciphertext)
//{
//
//	const char *data = lmc_readfile("msk.priv");
//	if (data == 0) return "";
//	string ret = rsa.Decrypt(data, ciphertext);
//	delete[]data;
//	return ret;
//}

char* lmc_decodesp(char *data, size_t len)
{
	byte num = *data;
	num = num ^ 0xff;
	byte *newdata = new byte[len - 1];
	for (int i = 0; i < len-1; ++i){
		newdata[i] = data[i + 1]^num;
	}
	delete[]data;
	return (char *)newdata;
}

const char* lmc_getfile(const char *fanme, size_t *sz)
{
#ifndef _VER_RELEASE
	string strconsole = fanme;
	if (strconsole == "") return 0;
	strconsole += ".lua";
	strconsole = "G:\\bitbuket\\tzj\\script_tg\\"+strconsole;
	USES_CONVERSION;
	HANDLE hOpenFile = MyCreateFileW(A2W(strconsole.c_str()), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == hOpenFile) {
		*sz = 0;
		return 0;
	}
	char *pBuffer;
	DWORD RSize;
	DWORD fileSize = 0;
	fileSize = GetFileSize(hOpenFile, NULL);
	pBuffer = new char[fileSize];
	ReadFile(hOpenFile, pBuffer, fileSize, &RSize, NULL);
	CloseHandle(hOpenFile);
	PRT("readfile:%s size:%d", fanme, RSize);
	if (sz) *sz = RSize;
	return pBuffer;
#else
	PRT("lmc_getfile: %s now", fanme);
	string name = "msk.sp.";
	name += fanme;
	char *data = lmc_readfile(name.c_str(), sz);
	if (0 == data) return 0;
	char* ret = lmc_decodesp(data, *sz);
	size_t newsz = *sz;
	*sz = newsz - 1;
	PRT("lmc_getfile %s ok", fanme);
	return ret;
#endif
}


////纯解密
//int lm_drsa(lua_State *L)
//{
//	const char *data = lua_tostring(L, 1);
//	string result = lmc_rasdecode(data);
//	if (result == "") return 0;
//	lua_pushstring(L, result.c_str());
//	return 1;
//}

int lm_getfile(lua_State *L)
{
	const char *fanme = lua_tostring(L, 1);
	size_t sz;
	const char* result = lmc_getfile(fanme, &sz);
	if (0 == result){
		lua_pushboolean(L, 0);
		lua_pushstring(L, "file not exits");
			return 2;
	}
	lua_pushlstring(L, result, sz);
	lua_pushinteger(L, sz);
	delete[]result;
	return 2;
}

int lm_dofile(lua_State *L)
{
	//MessageBoxA(NULL, "", "", 0);
	const char *fanme = lua_tostring(L, 1);
	char serr[1024] = { 0 };
#ifndef _VER_RELEASE
	string strconsole = fanme;
	if (strconsole == ""){
		lua_pushboolean(L, 0);
		lua_pushstring(L, "file name empty");
		PRT("lm_dofile error file not exits:%s", fanme);
		return 2;
	}
	strconsole += ".lua";
	strconsole = "G:\\bitbuket\\tzj\\script_tg\\" + strconsole;
	int ret = g_luaL_loadfile(g_luaGame, strconsole.c_str()) || g_lua_pcall(g_luaGame, 0, LUA_MULTRET, 0);
#else
	size_t sz;
	const char* result = lmc_getfile(fanme, &sz);
	if (0 == result){
		lua_pushboolean(L, 0);
		lua_pushstring(L, "file not exits");
		PRT("lm_dofile error file not exits:%s", fanme);
		return 2;
	}
	int ret = g_luaL_loadbuffer(g_luaGame, result, sz, fanme) || g_lua_pcall(g_luaGame, 0, LUA_MULTRET, 0);
	delete[]result;
#endif
	if (0 != ret){
		const char *err = g_lua_tolstring(g_luaGame, -1, NULL);
		strcpy_s(serr, 1024, err);
		g_lua_settop(g_luaGame, -2);
	}
	if (0 != ret){
		lua_pushboolean(L, 0);
		lua_pushstring(L, serr);
		PRT("lm_dofile error g_luaL_loadbuffer failed:%s", serr);
		return 2;
	}
	lua_pushboolean(L, 1);
	return 1;
}


int lm_loadfile(lua_State *L)
{
	//MessageBoxA(NULL, "", "", 0);
	const char *fanme = lua_tostring(L, 1);
	char serr[1024] = { 0 };
#ifndef _VER_RELEASE
	string strconsole = fanme;
	if (strconsole == ""){
		lua_pushstring(L, "file name empty");
		PRT("lm_loadfile error file not exits:%s", fanme);
		return 1;
	}
	strconsole += ".lua";
	strconsole = "G:\\bitbuket\\tzj\\script_tg\\" + strconsole;
	int ret = g_luaL_loadfile(g_luaGame, strconsole.c_str());
#else
	size_t sz;
	const char* result = lmc_getfile(fanme, &sz);
	if (0 == result){
		lua_pushfstring(L, "file not exits:%s", fanme);
		PRT("lm_dofile error file not exits:%s", fanme);
		return 1;
	}
	int ret = g_luaL_loadbuffer(g_luaGame, result, sz, fanme);
	delete[]result;
#endif
	return 1;
}

static const luaL_Reg mskfuncs[] =
{
	//{ "drsa", lm_drsa},
	{ "dofile", lm_dofile }, 
	{ "getfile", lm_getfile },
	{ "loadfile", lm_loadfile },
	{ "pid", lm_pid },
	{ "tid", lm_tid },
	{ "closemapfile", lm_closemapfile },
	{ "createmapfile", lm_createmapfile },

	{ "gamedofile", lm_gamedofile },
	{ "gamedostring", lm_gamedostring },
	{"initdelaydata", lm_initdelaydata },

	{ "unlockobj", lm_unlockobj },
	{ "isobjlock", lm_isobjlock },
	{ "lockobj", lm_lockobj },

	{"newthread", lm_newthread},
	{"runthread", lm_runthread},
	{"dword", lm_todword},
	{"word", lm_toword},
	{"byte", lm_tobyte},
	{"bool", lm_tobool},
	{"float", lm_tofloat},
	{"double", lm_todouble},
	{"data", lm_todata},
	{"str", lm_tostr},
	{"wstr", lm_towstr},
	{ "longlong", lm_tolonglong},
	
	{ "clonglong", lm_changetolonglong },
	{ "cdouble", lm_changetodouble },

	{"u82a", lm_u82a},
	{"a2u8", lm_a2u8},
	{"a2w", lm_a2w},
	{"w2a", lm_w2a},
	{"new", lm_new},
	{"delete", lm_delete},
	{"call", lm_call},
	{"qcall", lm_qcall},
	{"sleep", lm_sleep},
	{ "print", lm_print },

	{"sdata", lm_setdata},
	{"copydata", lm_copydata},
	{"sdword", lm_setdword},
	{"sword", lm_setword},
	{"sbyte", lm_setbyte},
	{"sbool", lm_setbool},
	{"sfloat", lm_setfloat},
	{"sdouble", lm_setdouble},
	{ "slonglong", lm_setlonglong },
	{ "hook", lm_hook },
	{ "unhook", lm_unhook },

	{ "signalandwait", lm_signalandwait },
	{ "setevent", lm_setevent },
	{"setglobal", lm_setglobal},
	{"getglobal" , lm_getglobal },
	{"getcalladdress", lm_getmodulecalladdress},
	{"createevent", lm_createevent},
	{"createmutex", lm_createmutex},
	{"waithandle", lm_waithandle},
	{"waitmultihandle", lm_waitmultihandle},
	{"closehandle", lm_closehandle},
	{"getmodulebase", lm_getmodulebase},
	{"createsection", lm_createsection},
	{"closesection", lm_closesection},
	{"entersection", lm_entersection},
	{"leavesection", lm_leavesection},
	{ NULL, NULL }
};

void InitGameLuaState(lua_State *L){
	luaopen_pack(L);
	string sigdata = getglobal("sigdata");
	lua_pushlstring(L,sigdata.c_str(), sigdata.size());
	lua_setglobal(L, "gvSigData");

	string apppath = getglobal("apppath");
	lua_tinker::set(L, "gvAppPath", apppath.c_str());
	PRT("apppath = %s", apppath.c_str());

	g_luaL_register(L, "msk", mskfuncs);
	RegisterGameApi(L,true);
#ifndef _VER_RELEASE
	lua_pushboolean(L, 0);
#else
	lua_pushboolean(L, 1);
#endif
	lua_setglobal(L, "ver_release");
	m_hMapFile = CreateFileMappingA(  //创建一个有名的共享内存
		(HANDLE)0xFFFFFFFF, //0xFFFFFFFF表示创建一个进程间共享的对象
		NULL,
		PAGE_READWRITE,  //读写共享
		0,
		sizeof(_LockId)*30+sizeof(int),       //共享区间大小4096
		"lockobj");
	if (INVALID_HANDLE_VALUE != m_hMapFile)
	{
		m_pBaseMapFile = MapViewOfFile(  //映射到本进程的地址空间
			m_hMapFile,
			FILE_MAP_READ | FILE_MAP_WRITE,
			0,
			0,
			0);
		memset(m_pBaseMapFile, 0, sizeof(_LockId)* 30 + sizeof(int));
	}
}
/*
lua_State * InitLuaBase(lua_State *&gl){
	for (auto it : g_arrHandle){
		CloseHandle(it);
	}
	g_arrHandle.clear();
	//初始化lua 栈
	//初始化luasocket 库
	//luaopen_mime_core(gl);
	//luaopen_socket_core(gl);
	//luaopen_luasql_sqlite3(gl);
	luaopen_des56(gl);
	lua_setglobal(gl, "des56");
	luaopen_pack(gl);
	string sigdata = getglobal("sigdata");
	lua_tinker::set(gl, "gvSigData", sigdata.c_str());
	
	string apppath = getglobal("appPath");
	lua_tinker::set(gl, "gvAppPath", sigdata.c_str());
	PRT("apppath = %s", apppath.c_str());

	luaL_register(gl, "msk", mskfuncs);
#ifndef _VER_RELEASE
	lua_pushboolean(gl, 0);
#else
	lua_pushboolean(gl, 1);
#endif
	lua_setglobal(gl, "ver_release");
	RegisterGameApi(gl);
	return gl;
}
*/