if gUI and not gUI.Email then
  gUI.Email = {}
end
local Email_nMailNpcType = 0
local Email_nCurShowMailType = 0
local Email_nCurMailPage = 0
local Email_nMailCountPerPage = 6
local Email_nSendItemSlot = -1
local Email_nSendItemBag = -1
local Email_nMailCount = 0
local Email_nCurShowMail = -1
local Email_bCheckSpaceMail = false
local Email_SysEmail = 2
local Email_NormalEmail = 0
local Email_SelectAll = false
local Email_MaxTopic = 24
local Email_MaxContext = 400
local Email_Type = {
  [0] = "全部显示",
  "系统邮件",
  "玩家邮件"
}
local Email_Tips_Pic = {
  [1] = "UiBtn001:Image_mail01",
  [2] = "UiBtn001:Image_mail03",
  [3] = "UiBtn001:Image_mail02"
}
local Email_UseItemTopic = false
local Email_Cost = 50
function Lua_Email_DelMail()
  for i = 1, Email_nMailCountPerPage do
    Lua_Email_DelMailByIndex(i)
  end
end
function Lua_Email_CheckDelMail()
  local curpage = Email_nCurMailPage * Email_nMailCountPerPage
  local count = curpage + Email_nMailCountPerPage - 1
  for i = curpage, count do
    Lua_Email_ShowDelMsgBox(i)
  end
end
function Lua_Email_ShowDelMsgBox(index)
  local wndParent
  local number = index + 1
  wndParent = WindowSys_Instance:GetWindow("Email.Main_bg.Part1_bg.Mail" .. number .. "_slab")
  local wnd = wndParent:GetChildByName("Tips_pic")
  Messagebox_Show("DELETE_MAIL", false)
end
function Lua_Email_DelMailByIndex(index)
  local curpage = Email_nCurMailPage * Email_nMailCountPerPage
  local wndParent
  wndParent = WindowSys_Instance:GetWindow("Email.Main_bg.Part1_bg.Mail" .. index .. "_slab")
  if wndParent then
    local checked = wndParent:GetChildByName("ChooseMail1_cbtn")
    checked = WindowToCheckButton(checked)
    local bChecked = checked:GetProperty("Checked")
    if bChecked == "true" then
      DeleteMail(Email_nCurShowMailType, curpage + index - 1)
    end
  end
end
function Lua_EmailWrite_ConfirmSend()
  local parent = WindowSys_Instance:GetWindow("EmailWrite.Write_bg")
  local townd = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Picture1_bg.WriterName_ebox")
  local to = townd:GetProperty("Text")
  local strObjId = ""
  local nObjId = -1
  if to == "" and nObjId == 0 then
    ShowErrorMessage("请输入收件人!")
    return
  end
  local topicwnd = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Picture2_bg.MailCaption_ebox")
  local topic = topicwnd:GetProperty("Text")
  if topic == "" then
    ShowErrorMessage("请输入邮件主题!")
    return
  end
  if string.len(topic) > Email_MaxTopic then
    ShowErrorMessage("邮件主题过长")
    return
  end
  local contextwnd = parent:GetGrandChild("VisceraImage_pg.Viscera_ebox")
  local context = contextwnd:GetProperty("Text")
  if string.len(context) > Email_MaxContext then
    ShowErrorMessage("邮件内容过长")
    return
  end
  local over_time = 0
  local gold = tonumber(parent:GetGrandChild("Money_bg.MoneyAu_ebox"):GetProperty("Text"))
  local silver = tonumber(parent:GetGrandChild("Money_bg.MoneyAg_ebox"):GetProperty("Text"))
  local copper = tonumber(parent:GetGrandChild("Money_bg.MoneyCu_ebox"):GetProperty("Text"))
  local money = gold * 10000 + silver * 100 + copper
  SendMail(to, topic, context, Email_nMailNpcType, over_time, money, Email_nSendItemBag, Email_nSendItemSlot, nObjId)
end
function Lua_Email_WriteMail()
  local wnd = WindowSys_Instance:GetWindow("EmailWrite")
  if wnd:IsVisible() then
    wnd:SetProperty("Visible", "false")
    wnd = WindowSys_Instance:GetRootWindow()
    WindowSys_Instance:SetKeyboardCaptureWindow(wnd)
    Lua_EmailWrite_ClearInfo()
    Messagebox_Hide("SEND_MAIL")
  else
    wnd:SetProperty("Visible", "true")
  end
end
function Lua_EmailWrite_ClearInfo()
  Email_nSendItemSlot = -1
  Email_nSendItemBag = -1
  local wnd = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Accessories_gbox")
  wnd = WindowToGoodsBox(wnd)
  wnd:ClearGoodsItem(0)
  local parent = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Money_bg")
  wnd = parent:GetGrandChild("MoneyAu_ebox")
  wnd:SetProperty("Text", "0")
  wnd = parent:GetGrandChild("MoneyAg_ebox")
  wnd:SetProperty("Text", "0")
  wnd = parent:GetGrandChild("MoneyCu_ebox")
  wnd:SetProperty("Text", "0")
  parent = WindowSys_Instance:GetWindow("EmailWrite.Write_bg")
  wnd = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Picture1_bg.WriterName_ebox")
  local name = wnd:GetProperty("Text")
  wnd:SetProperty("Text", "")
  wnd = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Picture2_bg.MailCaption_ebox")
  wnd:SetProperty("Text", "")
  wnd = parent:GetGrandChild("VisceraImage_pg.Viscera_ebox")
  wnd:SetProperty("Text", "")
end
function Lua_Email_TakeMailItemCallback()
  TakenMailItem(Email_nCurShowMailType, Email_nCurShowMail, 0)
end
function Lua_Email_DelSelectedMail()
  DeleteMail(Email_nCurShowMailType, Email_nCurShowMail)
end
function Lua_Email_formattime(time)
  local timestr
  if time > 86400 then
    timestr = string.format("%d天", math.ceil(time / 86400))
  elseif time > 3600 then
    local hour = math.ceil(time / 3600)
    timestr = string.format("%d小时", hour)
  else
    local min = math.ceil(time / 60)
    timestr = string.format("%d分钟", min)
  end
  return timestr
end
function Lua_Email_AddItem(bagType, index)
  _Email_AddSendItem(bagType, index)
end
function Lua_Email_Close()
  local wnd = WindowSys_Instance:GetWindow("Email")
  local wndReceive = WindowSys_Instance:GetWindow("Email.Receive_bg")
  Email_nCurMailPage = 0
  local isClose = false
  if wnd:IsVisible() then
    wnd:SetProperty("Visible", "false")
    wndReceive:SetProperty("Visible", "false")
    isClose = true
  end
  wnd = WindowSys_Instance:GetWindow("EmailWrite")
  if wnd:IsVisible() then
    wnd:SetProperty("Visible", "false")
    Lua_EmailWrite_ClearInfo()
    isClose = true
  end
  if isClose then
    wnd = WindowSys_Instance:GetRootWindow()
    WindowSys_Instance:SetKeyboardCaptureWindow(wnd)
    CloseMailBox()
  end
end
function Lua_Email_UpdateMailCount(count)
  local Unread, Ungain, Total = GetMailGlobalInfo(Email_nCurShowMailType)
  Email_nMailCount = Total
  local pageno = math.floor((Email_nMailCount + 5) / Email_nMailCountPerPage)
  if pageno < 1 then
    pageno = 1
  end
  Email_nCurMailPage = math.min(Email_nCurMailPage, pageno - 1)
  local wnd = WindowSys_Instance:GetWindow("Email.Main_bg.Part1_bg.PageNum_dlab")
  wnd:SetProperty("Text", Email_nCurMailPage + 1 .. "/" .. pageno)
  local wnd2 = WindowSys_Instance:GetWindow("Email.Main_bg.Part1_bg.Button1")
  local wnd3 = WindowSys_Instance:GetWindow("Email.Main_bg.Delete_btn")
  if Email_nMailCount == 0 then
    wnd2:SetProperty("Enable", "false")
    wnd3:SetProperty("Enable", "false")
  else
    wnd2:SetProperty("Enable", "true")
  end
  local wndp = WindowSys_Instance:GetWindow("Email.Main_bg.Part1_bg.PreviousPage_btn")
  local wndn = WindowSys_Instance:GetWindow("Email.Main_bg.Part1_bg.NextPage_btn")
  if Email_nCurMailPage + 1 == pageno and pageno == 1 then
    wndp:SetProperty("Enable", "false")
    wndn:SetProperty("Enable", "false")
  elseif Email_nCurMailPage + 1 == pageno then
    wndp:SetProperty("Enable", "true")
    wndn:SetProperty("Enable", "false")
  elseif Email_nCurMailPage + 1 == 1 and pageno ~= 1 then
    wndp:SetProperty("Enable", "false")
    wndn:SetProperty("Enable", "true")
  else
    wndp:SetProperty("Enable", "true")
    wndn:SetProperty("Enable", "true")
  end
end
function Email_ClearCurSelMailInfo()
  local wndParent = WindowSys_Instance:GetWindow("Email.Receive_bg.Part2_bg")
  local wnd = wndParent:GetGrandChild("Title_bg.MailCaption_dlab")
  wnd:SetProperty("Text", "")
  wnd = wndParent:GetGrandChild("Name_bg.WriterName_dlab")
  wnd:SetProperty("Text", "")
  wnd = wndParent:GetGrandChild("Viscera_bg.Viscera_dlab")
  wnd:SetProperty("Text", "")
  wnd = wndParent:GetGrandChild("Accessories_gbox")
  wnd = WindowToGoodsBox(wnd)
  wnd:ClearGoodsItem(0)
  wnd:SetProperty("Visible", "true")
  wnd = wndParent:GetGrandChild("MainBag_bg.MoneyAu_dlab")
  wnd:SetProperty("Text", Lua_UI_Money2String(0))
  wnd = wndParent:GetGrandChild("MainBag_bg.BindMoneyGroup_bg")
  wnd:SetProperty("Visible", "false")
  wnd = wndParent:GetGrandChild("Viscera_bg.MailType_bg")
  wnd:SetProperty("Visible", "false")
end
function Email_SetCurSelMailInfo(index)
  local wndReceive = WindowSys_Instance:GetWindow("Email.Receive_bg")
  wndReceive:SetProperty("Visible", "true")
  MarkMailAsRead(Email_nCurShowMailType, index)
  Email_nCurShowMail = index
end
function _Email_SetMailInfo()
  if Email_nCurShowMail == -1 then
    local wndReceive = WindowSys_Instance:GetWindow("Email.Receive_bg")
    wndReceive:SetProperty("Visible", "false")
    return
  end
  local from, topic, context, money, time, icon1, count1 = GetMailDetail(Email_nCurShowMailType, Email_nCurShowMail)
  Email_ClearCurSelMailInfo()
  if from and money then
    local wndParent = WindowSys_Instance:GetWindow("Email.Receive_bg.Part2_bg")
    local wnd = wndParent:GetGrandChild("Name_bg.WriterName_dlab")
    local mailTypewnd = wndParent:GetGrandChild("Viscera_bg.MailType_bg")
    if Email_nCurShowMailType == 0 then
      wnd:SetProperty("Text", from)
      mailTypewnd:SetProperty("Visible", "true")
    elseif Email_nCurShowMailType == 2 then
      wnd:SetProperty("Text", "系统邮件")
      mailTypewnd:SetProperty("Visible", "false")
    end
    wnd = wndParent:GetGrandChild("Title_bg.MailCaption_dlab")
    wnd:SetProperty("Text", topic)
    wnd = wndParent:GetGrandChild("Viscera_bg.Viscera_dlab")
    if Email_nCurShowMailType == 0 then
      wnd:SetProperty("Text", context .. "( 发送于" .. time .. " )\n")
    else
      context = _Email_CheckHadMoney(context)
      wnd:SetProperty("Text", context)
    end
    wnd = wndParent:GetGrandChild("Accessories_gbox")
    wnd = WindowToGoodsBox(wnd)
    if icon1 then
      local slot = 0
      if Email_nCurShowMailType == 0 then
        slot = 0
      else
        slot = 1
      end
      wnd:SetItemGoods(0, icon1, Lua_Bag_GetQuality(gUI_GOODSBOX_BAG.mail, slot))
      local intenLev = Lua_Bag_GetStarInfo(gUI_GOODSBOX_BAG.mail, slot)
      if 0 < ForgeLevel_To_Stars[intenLev] then
        wnd:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
      else
        wnd:SetItemStarState(0, 0)
      end
      if count1 > 1 then
        wnd:SetItemSubscript(0, tostring(count1))
      end
    else
      wnd:ClearGoodsItem(0)
    end
    wnd = wndParent:GetGrandChild("MainBag_bg.MoneyAu_dlab")
    wnd:SetProperty("Text", Lua_UI_Money2String(money))
  end
end
function _Email_CheckHadMoney(textContent)
  local idxBegin, idxEnd = string.find(textContent, "获得金钱：")
  if Email_nCurShowMailType == 2 and idxEnd then
    local money = string.sub(textContent, idxEnd + 1, -1)
    local strMoney = Lua_UI_Money2String(tonumber(money))
    textContent = string.sub(textContent, 1, idxEnd) .. strMoney
  end
  return textContent
end
function _Email_AddSendItem(bagType, index)
  if bagType == gUI_GOODSBOX_BAG.backpack1 then
    Lua_Chat_ShowOSD("EMAIL_QUEST_ITEM")
    return
  elseif bagType ~= gUI_GOODSBOX_BAG.backpack0 then
    Lua_Chat_ShowOSD("EMAIL_ITEM_INVALID")
    return
  end
  local bindType = Lua_Bag_GetBindInfo(bagType, index)
  if bindType == 1 then
    Lua_Chat_ShowOSD("EMAIL_ITEM_BIND")
    return
  end
  ItemAddSendMail(bagType, index)
end
function _Email_UpdateMailPage()
  local wnd = WindowSys_Instance:GetWindow("Email.Main_bg.MailTips_tctl")
  local back = WindowSys_Instance:GetWindow("Email.Receive_bg.Reply_btn")
  local tablectrl = WindowToTableCtrl(wnd)
  if tablectrl:GetSelectItem() == 0 then
    Email_nCurShowMailType = Email_NormalEmail
    back:SetProperty("Enable", "true")
  else
    Email_nCurShowMailType = Email_SysEmail
    back:SetProperty("Enable", "false")
  end
  _OnEmail_ClearAll()
  AskMailList(Email_nCurShowMailType)
  Email_ClearCurSelMailInfo()
  UpdateMailList(Email_nCurShowMailType, Email_nCurMailPage, false)
end
function _Email_UpdateMailList(mailtype, index, name, topic, nRead, expireTime, time)
  local nIndex = index - Email_nCurMailPage * Email_nMailCountPerPage
  if nIndex >= 0 and nIndex < Email_nMailCountPerPage then
    local wnd
    local root = WindowSys_Instance:GetWindow("Email.Main_bg.Part1_bg")
    local ParentWnd = root:GetChildByName("Mail" .. nIndex + 1 .. "_slab")
    ParentWnd:SetProperty("Visible", "true")
    local wnd = ParentWnd:GetChildByName("GM_pic")
    if Email_nCurShowMailType == Email_SysEmail then
      name = "系统邮件"
      wnd:SetProperty("Visible", "true")
    else
      wnd:SetProperty("Visible", "false")
    end
    wnd = ParentWnd:GetChildByName("MailName_dlab")
    if nRead > 0 then
      wnd:SetProperty("Text", "{#C^0xff909090^" .. topic .. "}")
    else
      wnd:SetProperty("Text", topic)
    end
    wnd = ParentWnd:GetChildByName("UserName_dlab")
    if nRead > 0 then
      wnd:SetProperty("Text", "{#C^0xff909090^" .. name .. "}")
    else
      wnd:SetProperty("Text", name)
    end
    wnd = ParentWnd:GetChildByName("Pres_Num_dlab")
    if expireTime > 1 then
      wnd:SetProperty("Text", "{#G^" .. Lua_Email_formattime(expireTime) .. "}")
    else
      wnd:SetProperty("Text", "{#R^" .. Lua_Email_formattime(expireTime) .. "}")
    end
    local from2, topic2, context2, money2, time2, icon2, count2 = GetMailDetail(mailtype, index)
    wnd = ParentWnd:GetChildByName("Tips_pic")
    wnd:SetProperty("Visible", "true")
    if icon2 ~= nil then
      wnd:SetProperty("BackImage", icon2)
    elseif money2 > 0 then
      wnd:SetProperty("BackImage", Email_Tips_Pic[2])
    elseif nRead > 0 then
      wnd:SetProperty("BackImage", Email_Tips_Pic[3])
    else
      wnd:SetProperty("BackImage", Email_Tips_Pic[1])
    end
    time2 = string.match(time2, "^%d+%-%d+%-%d+")
    wnd = ParentWnd:GetChildByName("Date_dlab")
    wnd:SetProperty("Text", time2)
  end
end
function UI_Email_Select0(msg)
  if Email_nCurMailPage < math.floor(Email_nMailCount / Email_nMailCountPerPage) or Email_nMailCount > Email_nCurMailPage * Email_nMailCountPerPage then
    Email_SetCurSelMailInfo(Email_nCurMailPage * Email_nMailCountPerPage)
  end
end
function UI_Email_Select1(msg)
  if Email_nCurMailPage < math.floor(Email_nMailCount / Email_nMailCountPerPage) or Email_nMailCount > Email_nCurMailPage * Email_nMailCountPerPage + 1 then
    Email_SetCurSelMailInfo(Email_nCurMailPage * Email_nMailCountPerPage + 1)
  end
end
function UI_Email_Select2(msg)
  if Email_nCurMailPage < math.floor(Email_nMailCount / Email_nMailCountPerPage) or Email_nMailCount > Email_nCurMailPage * Email_nMailCountPerPage + 2 then
    Email_SetCurSelMailInfo(Email_nCurMailPage * Email_nMailCountPerPage + 2)
  end
end
function UI_Email_Select3(msg)
  if Email_nCurMailPage < math.floor(Email_nMailCount / Email_nMailCountPerPage) or Email_nMailCount > Email_nCurMailPage * Email_nMailCountPerPage + 3 then
    Email_SetCurSelMailInfo(Email_nCurMailPage * Email_nMailCountPerPage + 3)
  end
end
function UI_Email_Select4(msg)
  if Email_nCurMailPage < math.floor(Email_nMailCount / Email_nMailCountPerPage) or Email_nMailCount > Email_nCurMailPage * Email_nMailCountPerPage + 4 then
    Email_SetCurSelMailInfo(Email_nCurMailPage * Email_nMailCountPerPage + 4)
  end
end
function UI_Email_Select5(msg)
  if Email_nCurMailPage < math.floor(Email_nMailCount / Email_nMailCountPerPage) or Email_nMailCount > Email_nCurMailPage * Email_nMailCountPerPage + 5 then
    Email_SetCurSelMailInfo(Email_nCurMailPage * Email_nMailCountPerPage + 5)
  end
end
function UI_Email_CloseBtnClick(msg)
  local wnd = WindowSys_Instance:GetWindow("Email")
  if wnd:IsVisible() then
    wnd:SetProperty("Visible", "false")
    CloseMailBox()
    wnd = WindowSys_Instance:GetRootWindow()
    WindowSys_Instance:SetKeyboardCaptureWindow(wnd)
    Email_nCurMailPage = 0
    Email_SelectAll = false
    Messagebox_Hide("DELETE_MAIL")
  end
end
function UI_Email_HelpClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Email.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_EmailWrite_CloseBtnClick(msg)
  Lua_Email_WriteMail()
end
function UI_EmailWrite_HelpClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Email_WriteMailClickBtn(msg)
  local _, _, _, _, level = GetPlaySelfProp(4)
  if GetRemainProtectTime() > 0 then
    Lua_Chat_ShowOSD("SAFE_TIME_LIMIT")
    return
  end
  if level < 20 then
    Lua_Chat_ShowOSD("EMAIL_NEED_LEVEL")
    return
  end
  Lua_EmailWrite_ClearInfo()
  Lua_Email_WriteMail()
end
function UI_Email_ItemLbtndown(msg)
  local index = msg:get_lparam()
  if Email_nCurMailPage < math.floor(Email_nMailCount / Email_nMailCountPerPage) or Email_nMailCount > Email_nCurMailPage * Email_nMailCountPerPage + index then
    Email_SetCurSelMailInfo(Email_nCurMailPage * Email_nMailCountPerPage + index)
  end
end
function UI_Email_SendBtnClick(msg)
  local townd = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Picture1_bg.WriterName_ebox")
  local to = townd:GetProperty("Text")
  if to == "" then
    ShowErrorMessage("请输入收件人!")
    return
  end
  local topicwnd = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Picture2_bg.MailCaption_ebox")
  local topic = topicwnd:GetProperty("Text")
  if topic == "" then
    ShowErrorMessage("请输入邮件主题!")
    return
  end
  if string.len(topic) > Email_MaxTopic then
    ShowErrorMessage("邮件主题过长")
    return
  end
  local contextwnd = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.VisceraImage_pg.Viscera_ebox")
  local context = contextwnd:GetProperty("Text")
  if string.len(context) > Email_MaxContext then
    ShowErrorMessage("邮件内容过长")
    return
  end
  local parent = WindowSys_Instance:GetWindow("EmailWrite.Write_bg")
  local gold = tonumber(parent:GetGrandChild("Money_bg.MoneyAu_ebox"):GetProperty("Text"))
  local silver = tonumber(parent:GetGrandChild("Money_bg.MoneyAg_ebox"):GetProperty("Text"))
  local copper = tonumber(parent:GetGrandChild("Money_bg.MoneyCu_ebox"):GetProperty("Text"))
  local money = 0
  if gold and silver and copper then
    money = gold * 10000 + silver * 100 + copper
  end
  local strMoney = ""
  if money > 0 then
    strMoney = Lua_UI_Money2String(money)
  end
  local strItemName = ""
  local wnd = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Accessories_gbox")
  wnd = WindowToGoodsBox(wnd)
  if wnd:IsItemHasIcon(0) and Email_nSendItemBag ~= -1 then
    local num, _, _, name = GetItemBaseInfoBySlot(Email_nSendItemBag, Email_nSendItemSlot)
    if num and name then
      strItemName = string.format("[%s]*%d", name, num)
    end
  end
  Messagebox_Show("SEND_MAIL", to, strItemName, strMoney)
end
function UI_Email_LBtnClick(msg)
  UI_Email_GetRecvtem(msg)
end
function UI_Email_GetRecvtem(msg)
  if Email_nCurShowMailType == -1 then
    ShowErrorMessage("请先选中邮件!")
    return
  end
  local index = 0
  if Email_nCurShowMailType == 0 then
    index = 0
  elseif Email_nCurShowMailType == 2 then
    index = 1
  end
  TakenMailItem(Email_nCurShowMailType, Email_nCurShowMail, index)
end
function UI_Email_GetAllRecvtem(msg)
  if Email_nCurShowMail == -1 then
    ShowErrorMessage("请先选中邮件!")
    return
  end
  TakenAllMailItem(Email_nCurShowMailType, Email_nCurShowMail)
end
function UI_Email_RecvMoney(msg)
  if Email_nCurShowMail == -1 then
    ShowErrorMessage("请先选中邮件!")
    return
  end
  TakenMailMoney(Email_nCurShowMailType, Email_nCurShowMail)
end
function UI_Email_RecvItemUserBoxShowtip(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  if to_win:GetItemIcon(0) == "" then
    return
  end
  local slot = 0
  if Email_nCurShowMailType == 0 then
    slot = 0
  else
    slot = 1
  end
  Lua_Tip_Item(to_win:GetToolTipWnd(0), slot, gUI_GOODSBOX_BAG.mail, nil, nil, nil)
  Lua_Tip_ShowEquipCompare(to_win, gUI_GOODSBOX_BAG.mail, slot)
end
function UI_Email_ReplyBtnClick(msg)
  Lua_EmailWrite_ClearInfo()
  if Email_nCurShowMail == -1 then
    ShowErrorMessage("请选中需要回复的邮件!")
    return
  end
  Lua_Email_WriteMail()
  local from, topic, context, money, time, icon1, icon2, icon3, icon4 = GetMailDetail(Email_nCurShowMailType, Email_nCurShowMail)
  if from and topic then
    wnd = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Picture1_bg.WriterName_ebox")
    wnd:SetProperty("Text", from)
    wnd = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Picture2_bg.MailCaption_ebox")
    local reTopic = "回复:" .. topic
    if string.len(reTopic) >= Email_MaxTopic then
      local asc2Num = 0
      for i = 1, Email_MaxTopic - 2 do
        if string.byte(reTopic, i) < 128 then
          asc2Num = asc2Num + 1
        end
      end
      reTopic = string.sub(reTopic, 1, Email_MaxTopic - 2 + asc2Num % 2)
    end
    wnd:SetProperty("Text", reTopic)
  end
end
function UI_Email_DelReadMailBtnClick(msg)
  DeleteAllReadMail(Email_nCurShowMailType)
end
function UI_Email_DelMailBtnClick(msg)
  local wndReceive = WindowSys_Instance:GetWindow("Email.Receive_bg")
  wndReceive:SetProperty("Visible", "false")
  local IsHasBox = false
  local IsChecked = false
  for i = 1, Email_nMailCountPerPage do
    local wndParent
    wndParent = WindowSys_Instance:GetWindow("Email.Main_bg.Part1_bg.Mail" .. i .. "_slab")
    if wndParent then
      local checked = wndParent:GetChildByName("ChooseMail1_cbtn")
      checked = WindowToCheckButton(checked)
      local bChecked = checked:GetProperty("Checked")
      if bChecked == "true" then
        IsChecked = true
        local boxflag = wndParent:GetChildByName("Tips_pic")
        local boximage = boxflag:GetProperty("BackImage")
        if boximage ~= Email_Tips_Pic[1] and boximage ~= Email_Tips_Pic[3] then
          IsHasBox = true
        end
      end
    end
  end
  if IsChecked then
    Messagebox_Show("DELETE_MAIL", IsHasBox)
  else
    WorldGroup_ShowNewStr(UI_SYS_MSG.EMAIL_NEED_CHECK)
  end
end
function UI_Email_SelectAllClick(msg)
  local bSelectAll = false
  if Email_SelectAll == false then
    Email_SelectAll = true
    bSelectAll = Email_SelectAll
  else
    Email_SelectAll = false
    bSelectAll = Email_SelectAll
  end
  for i = 1, Email_nMailCountPerPage do
    local parentwnd
    parentwnd = WindowSys_Instance:GetWindow("Email.Main_bg.Part1_bg.Mail" .. i .. "_slab")
    local wnd = parentwnd:GetGrandChild("ChooseMail1_cbtn")
    wnd = WindowToCheckButton(wnd)
    wnd:SetCheckedState(bSelectAll)
  end
  local wnd2 = WindowSys_Instance:GetWindow("Email.Main_bg.Delete_btn")
  if bSelectAll then
    wnd2:SetProperty("Enable", "true")
  else
    wnd2:SetProperty("Enable", "false")
  end
end
function UI_Email_ClickChooseMailButton(msg)
  local count = 0
  local wnd = WindowSys_Instance:GetWindow("Email.Main_bg.Delete_btn")
  for i = 1, Email_nMailCountPerPage do
    local wndParent
    wndParent = WindowSys_Instance:GetWindow("Email.Main_bg.Part1_bg.Mail" .. i .. "_slab")
    if wndParent then
      local checked = wndParent:GetChildByName("ChooseMail1_cbtn")
      checked = WindowToCheckButton(checked)
      local bChecked = checked:GetProperty("Checked")
      if bChecked == "true" then
        count = count + 1
      end
    end
  end
  if count > 0 then
    wnd:SetProperty("Enable", "true")
  else
    wnd:SetProperty("Enable", "false")
  end
end
function UI_Email_RemoveSendItem(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  wnd:ClearGoodsItem(0)
  ItemRemoveSendMail()
end
function UI_Email_AddSendItem(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local from_off = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    from_off = Lua_Bag_GetRealSlot(from_win, from_off)
    _Email_AddSendItem(gUI_GOODSBOX_BAG[from_type], from_off)
  end
end
function UI_Email_SendItemShowtip(msg)
  if WindowToGoodsBox(msg:get_window()):GetItemIcon(0) ~= "" then
    local to_win = WindowToGoodsBox(msg:get_window())
    Lua_Tip_Item(to_win:GetToolTipWnd(0), Email_nSendItemSlot, Email_nSendItemBag, nil, nil, nil)
    Lua_Tip_ShowEquipCompare(to_win, Email_nSendItemBag, Email_nSendItemSlot)
  end
end
function UI_Email_PrePageBtnClick(msg)
  local last = Email_nCurMailPage
  Email_nCurMailPage = Email_nCurMailPage - 1
  Email_nCurMailPage = math.max(0, Email_nCurMailPage)
  if Email_nCurMailPage ~= last then
    _OnEmail_ClearAll()
    UpdateMailList(Email_nCurShowMailType, Email_nCurMailPage, false)
    local pageno = math.floor((Email_nMailCount + 5) / Email_nMailCountPerPage)
    if pageno < 1 then
      pageno = 1
    end
    local wnd = WindowSys_Instance:GetWindow("Email.Main_bg.Part1_bg.PageNum_dlab")
    wnd:SetProperty("Text", Email_nCurMailPage + 1 .. "/" .. pageno)
  end
  local wndp = WindowSys_Instance:GetWindow("Email.Main_bg.Part1_bg.PreviousPage_btn")
  if Email_nCurMailPage + 1 == 1 then
    wndp:SetProperty("Enable", "false")
  else
    wndp:SetProperty("Enable", "true")
  end
  Email_SelectAll = false
end
function UI_Email_NextPageBtnClick(msg)
  local last = Email_nCurMailPage
  Email_nCurMailPage = Email_nCurMailPage + 1
  local pageno = math.floor((Email_nMailCount + 5) / Email_nMailCountPerPage)
  if pageno < 1 then
    pageno = 1
  end
  Email_nCurMailPage = math.min(Email_nCurMailPage, pageno - 1)
  if Email_nCurMailPage ~= last then
    _OnEmail_ClearAll()
    UpdateMailList(Email_nCurShowMailType, Email_nCurMailPage, false)
    local wnd = WindowSys_Instance:GetWindow("Email.Main_bg.Part1_bg.PageNum_dlab")
    wnd:SetProperty("Text", Email_nCurMailPage + 1 .. "/" .. pageno)
  end
  local wndn = WindowSys_Instance:GetWindow("Email.Main_bg.Part1_bg.NextPage_btn")
  if Email_nCurMailPage + 1 == pageno then
    wndn:SetProperty("Enable", "false")
  else
    wndn:SetProperty("Enable", "true")
  end
  Email_SelectAll = false
end
function UI_Email_TabctrlClick(msg)
  local wndReceive = WindowSys_Instance:GetWindow("Email.Receive_bg")
  wndReceive:SetProperty("Visible", "false")
  Email_SelectAll = false
  _Email_UpdateMailPage()
end
function UI_Email_TypeComboClick(msg)
  local wndReceive = WindowSys_Instance:GetWindow("Email.Receive_bg")
  wndReceive:SetProperty("Visible", "false")
  Email_SelectAll = false
  _Email_UpdateMailPage()
end
function UI_Email_SendMoneyChanged(msg)
  local parent = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Money_bg")
  local gold = parent:GetGrandChild("MoneyAu_ebox"):GetProperty("Text")
  local silver = parent:GetGrandChild("MoneyAg_ebox"):GetProperty("Text")
  local copper = parent:GetGrandChild("MoneyCu_ebox"):GetProperty("Text")
  local _, bagmoney, bindmoney = GetBankAndBagMoney()
  if bindmoney < Email_Cost and bagmoney > Email_Cost then
    bagmoney = bagmoney - (Email_Cost - bindmoney)
  end
  if gold == "" then
    gold = "0"
    parent:GetGrandChild("MoneyAu_ebox"):SetProperty("Text", "0")
  end
  if silver == "" then
    silver = "0"
    parent:GetGrandChild("MoneyAg_ebox"):SetProperty("Text", "0")
  end
  if copper == "" then
    copper = "0"
    parent:GetGrandChild("MoneyCu_ebox"):SetProperty("Text", "0")
  end
  if bagmoney < tonumber(gold) * 10000 + tonumber(silver) * 100 + tonumber(copper) then
    local mygold, mysilver, mycopper = Lua_UI_FormatMoneyByEdit(bagmoney)
    parent:GetGrandChild("MoneyAu_ebox"):SetProperty("Text", tostring(mygold))
    parent:GetGrandChild("MoneyAg_ebox"):SetProperty("Text", tostring(mysilver))
    parent:GetGrandChild("MoneyCu_ebox"):SetProperty("Text", tostring(mycopper))
  end
end
function UI_Email_ShowMail(type)
  local reliveWnd = WindowSys_Instance:GetWindow("ReliveFrame")
  local reviveWnd = WindowSys_Instance:GetWindow("Revive")
  if reliveWnd:IsVisible() or reviveWnd:IsVisible() then
    return
  end
  local wnd = WindowSys_Instance:GetWindow("Email")
  local wndReceive = WindowSys_Instance:GetWindow("Email.Receive_bg")
  if wnd:IsVisible() then
    wnd:SetProperty("Visible", "false")
    wndReceive:SetProperty("Visible", "false")
    CloseMailBox()
    wnd = WindowSys_Instance:GetRootWindow()
    WindowSys_Instance:SetKeyboardCaptureWindow(wnd)
  else
    wnd:SetProperty("Visible", "true")
    wndReceive:SetProperty("Visible", "false")
    local back = WindowSys_Instance:GetWindow("Email.Receive_bg.Reply_btn")
    local wnd1 = WindowSys_Instance:GetWindow("Email.Main_bg.MailTips_tctl")
    tablectrl = WindowToTableCtrl(wnd1)
    if type == Email_NormalEmail then
      tablectrl:SetSelectedState(0)
      back:SetProperty("Enable", "true")
    else
      tablectrl:SetSelectedState(1)
      back:SetProperty("Enable", "false")
    end
    Email_nCurShowMailType = type
    _OnEmail_ClearAll()
    AskMailList(Email_nCurShowMailType)
    Email_ClearCurSelMailInfo()
    UpdateMailList(Email_nCurShowMailType, Email_nCurMailPage, false)
  end
end
function UI_OnEmail_ReceiveClose()
  local wndReceive = WindowSys_Instance:GetWindow("Email.Receive_bg")
  if wnd:IsVisible() then
    wndReceive:SetProperty("Visible", "false")
  else
    wndReceive:SetProperty("Visible", "true")
  end
end
function UI_Email_FindBoxChange(msg)
  local root = WindowSys_Instance:GetWindow("MainMenu")
  local menu = WindowToRightClickMenu(root:GetChildByName("ListMenu"))
  gUI.Rcm.CurMenuListName = "FriendSearchList"
  menu:SetProperty("Visible", "false")
  local str = msg:get_window():GetProperty("Text")
  if str ~= "" then
    local reList = GetSearchFriendList(str)
    if reList[0] ~= nil then
      local rect = msg:get_window():GetWndRect()
      local posx, posy = GetMousePos()
      local menux = posx - rect:get_left()
      local menuy = posy - rect:get_bottom()
      menu:Clear()
      local i = 0
      for index, item in pairs(reList) do
        if i == 10 then
          menu:InsertItemEx("......", -1, "0xFFFFFFFF", "", false)
          break
        end
        menu:InsertItem(item.name, -1, nil, "")
        if msg:get_window():GetProperty("WindowName") == "WriterName_ebox" then
          menu:SetItemData(i, 1)
        else
          menu:SetItemData(i, 2)
        end
        menu:SetItemData64(i, item.GUID)
        i = i + 1
      end
      menu:SetAnchorPosition(1)
      menu:SetPositionOffset(-menux, menuy + 0.02)
      menu:SetProperty("Visible", "true")
    end
  end
end
function UI_Email_OpenMail()
  _OnEmail_ShowMailWnd()
end
function UI_Email_TopicChange()
  Email_UseItemTopic = false
end
function _OnEmail_ClearAll()
  local ParentWnd
  local root = WindowSys_Instance:GetWindow("Email.Main_bg.Part1_bg")
  for i = 1, Email_nMailCountPerPage do
    ParentWnd = root:GetChildByName("Mail" .. i .. "_slab")
    ParentWnd:SetProperty("Visible", "false")
    wnd = ParentWnd:GetChildByName("MailName_dlab")
    wnd:SetProperty("Text", "")
    wnd = ParentWnd:GetChildByName("UserName_dlab")
    wnd:SetProperty("Text", "")
    wnd = ParentWnd:GetChildByName("Pres_Num_dlab")
    wnd:SetProperty("Text", "")
    wnd = ParentWnd:GetChildByName("Date_dlab")
    wnd:SetProperty("Text", "")
    wnd = ParentWnd:GetChildByName("Tips_pic")
    wnd:SetProperty("Visible", "false")
    wnd = ParentWnd:GetChildByName("ChooseMail1_cbtn")
    wnd:SetProperty("Checked", "false")
  end
  Email_nCurShowMail = -1
end
function _OnEmail_AddMail(mailtype, index, icon, count, name, topic, nRead, expireTime)
  _Email_SetMailInfo()
  _Email_UpdateMailList(mailtype, index, name, topic, nRead, expireTime, nil)
end
function _OnEmail_AddSystemMail(mailtype, index, sid, nRead, name, topic, expireTime, time)
  _Email_SetMailInfo()
  _Email_UpdateMailList(mailtype, index, name, topic, nRead, expireTime, time)
end
function _OnEmail_ShowMailWnd(type)
  local _SysEmaliCount = GetUnreadCount(Email_SysEmail)
  local _NormalEmaliCount = GetUnreadCount(Email_NormalEmail)
  if _NormalEmaliCount == 0 and _SysEmaliCount > 0 then
    UI_Email_ShowMail(Email_SysEmail)
  else
    UI_Email_ShowMail(Email_NormalEmail)
  end
end
function _OnEmail_Close()
  local wnd = WindowSys_Instance:GetWindow("Email")
  local wndReceive = WindowSys_Instance:GetWindow("Email.Receive_bg")
  if wnd:IsVisible() then
    wnd:SetProperty("Visible", "false")
    wndReceive:SetProperty("Visible", "false")
    wnd = WindowSys_Instance:GetWindow("EmailWrite")
    if wnd:IsVisible() then
      wnd:SetProperty("Visible", "false")
    end
    wnd = WindowSys_Instance:GetRootWindow()
    WindowSys_Instance:SetKeyboardCaptureWindow(wnd)
    CloseMailBox()
    Email_SelectAll = false
    Messagebox_Hide("DELETE_MAIL")
    Messagebox_Hide("SEND_MAIL")
  end
end
function _OnEmail_Result(arg1)
  if arg1 > 0 then
    wnd = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Picture1_bg.WriterName_ebox")
    local name = wnd:GetProperty("Text")
    ShowErrorMessage("邮件已成功发送给" .. name)
    Lua_Email_WriteMail()
  end
end
function _OnEmail_Delete(arg1)
  local count = arg1
  Email_ClearCurSelMailInfo()
  _OnEmail_ClearAll()
  Lua_Email_UpdateMailCount(count)
  UpdateMailList(Email_nCurShowMailType, Email_nCurMailPage, false)
end
function _OnEmail_ShowContext(textContent, textTime)
  _Email_SetMailInfo()
end
function _OnEmail_ShowSystemContext(textContent)
  _Email_SetMailInfo()
end
function _OnEmail_AddSendItem(arg1, arg2, arg3, arg4, arg5, arg6)
  local wnd = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Accessories_gbox")
  wnd = WindowToGoodsBox(wnd)
  local topicwWnd = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Picture2_bg.MailCaption_ebox")
  local reTopic = topicwWnd:GetProperty("Text")
  if arg4 > 0 then
    Email_nSendItemSlot = arg2
    Email_nSendItemBag = arg1
    wnd:SetItemGoods(0, arg3, Lua_Bag_GetQuality(Email_nSendItemBag, Email_nSendItemSlot))
    local intenLev = Lua_Bag_GetStarInfo(Email_nSendItemBag, Email_nSendItemSlot)
    if 0 < ForgeLevel_To_Stars[intenLev] then
      wnd:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
    else
      wnd:SetItemStarState(0, 0)
    end
    if arg5 > 1 then
      wnd:SetItemSubscript(0, tostring(arg5))
    else
      wnd:SetItemSubscript(0, "")
    end
    if reTopic == "" or Email_UseItemTopic then
      reTopic = arg6
      if string.len(reTopic) >= Email_MaxTopic then
        local asc2Num = 0
        for i = 1, Email_MaxTopic - 2 do
          if string.byte(reTopic, i) < 128 then
            asc2Num = asc2Num + 1
          end
        end
        reTopic = string.sub(reTopic, 1, Email_MaxTopic - 2 + asc2Num % 2)
      end
      topicwWnd:SetProperty("Text", reTopic)
      Email_UseItemTopic = true
    end
  else
    wnd:ClearGoodsItem(0)
    Email_nSendItemSlot = -1
    Email_nSendItemBag = -1
    if reTopic == arg6 then
      topicwWnd:SetProperty("Text", "")
    end
    Email_UseItemTopic = false
  end
end
function _OnEmail_RecvNew(count)
  local wnd = WindowSys_Instance:GetWindow("Email")
  if wnd:IsVisible() and count > 0 then
    AskMailList(Email_nCurShowMailType)
  end
end
function Script_Email_OnEvent(event)
  if "MAIL_DEL_ALL" == event then
    _OnEmail_ClearAll()
  elseif "MAIL_RECV_NEW" == event then
    _OnEmail_RecvNew(arg1)
  elseif "MAIL_RECV_LIST" == event then
    _OnEmail_ClearAll()
    UpdateMailList(Email_nCurShowMailType, Email_nCurMailPage, false)
  elseif "MAIL_COUNT_UPDATE" == event then
    Lua_Email_UpdateMailCount(arg1)
  elseif "MAIL_ADD" == event then
    _OnEmail_AddMail(Email_nCurShowMailType, arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif "MAIL_ADD_SEND_ITEM" == event then
    _OnEmail_AddSendItem(arg1, arg2, arg3, arg4, arg5, arg6)
  elseif "MAIL_SHOW_WINDOW" == event then
    _OnEmail_ShowMailWnd(arg1)
  elseif "MAIL_SHOW_MAIL_CONTEXT" == event then
    _OnEmail_ShowContext(arg1, arg2)
  elseif "MAIL_DEL" == event then
    _OnEmail_Delete(arg1)
  elseif "SEND_MAIL_RESULT" == event then
    _OnEmail_Result(arg1)
  elseif "CLOSE_NPC_RELATIVE_DIALOG" == event then
    _OnEmail_Close()
  elseif "SYSMSG_ADD" == event then
    _OnEmail_AddSystemMail(Email_nCurShowMailType, arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif "SYSMSG_LIST" == event then
    _OnEmail_ClearAll()
    UpdateMailList(Email_nCurShowMailType, Email_nCurMailPage, false)
  elseif "SYSMSG_SHOW_MAIL_CONTEXT" == event then
    _OnEmail_ShowSystemContext(arg1)
  elseif "SYSMSG_RECV_NEW" == event then
    _OnEmail_RecvNew(arg1)
  elseif event == "SYSMSG_DEL_ALL" or event == "SYSMSG_DEL" then
    _OnEmail_Delete(arg1)
  elseif "SYSMSG_REFRUSH_DETAIL" == event then
  end
end
function Script_Email_OnHide()
  UI_Email_CloseBtnClick()
end
function Script_EmailWrite_OnHide()
  wnd = WindowSys_Instance:GetRootWindow()
  WindowSys_Instance:SetKeyboardCaptureWindow(wnd)
  Lua_EmailWrite_ClearInfo()
  CloseMailBox(1)
end
function Script_Email_OnLoad(event)
  gUI.Email.Combobox = WindowToComboBox(WindowSys_Instance:GetWindow("Email.Main_bg.MailTips_cb"))
  gUI.Email.Combobox:RemoveAllItems()
  for i = 0, #Email_Type do
    gUI.Email.Combobox:InsertString(Email_Type[i], i)
  end
  gUI.Email.Combobox:SetProperty("Visible", "false")
end
