if gUI and not gUI.GoalP then
  gUI.GoalP = {}
end
local _GoalP_RowNum = 4
local _GoalP_ActiveList = {
  [0] = 0,
  [1] = 0,
  [2] = 0,
  [3] = 0
}
local _GoalP_TreasureStateName = {
  [0] = "UiBtn001:Image_unswap",
  [1] = "UiBtn001:Image_exchange",
  [2] = "UiBtn001:Image_swap"
}
local _GoalP_StrActive = "活跃值%s宝箱"
function _GoalP_InitFormulaList()
  local list, active, allact = GetFormulaList()
  if list[0] == nil then
    debugN("error InitFormulaList")
    return
  end
  local wnd = gUI.GoalP.GoalPossess:GetChildByName("Pic1_bg")
  local wnd2 = gUI.GoalP.GoalPossess:GetChildByName("Pic2_bg")
  local itemID, cost, desc, state, costtext, onlinetime, onlinetext
  local hasBoxActive = false
  local MaxBoxCost = 0
  for i = 0, table.maxn(list) do
    itemID = list[i].Itemid
    cost = list[i].Cost
    formulaID = list[i].FormulaID
    state = list[i].State
    if active < cost then
      costtext = string.format("{#R^%d}", cost)
    else
      costtext = string.format("{#G^%d}", cost)
    end
    onlinetime = list[i].OnlineTime * 60
    onlinetext = Lua_UI_DetailTime(onlinetime) .. "后可领取"
    desc = string.format(_GoalP_StrActive, costtext)
    wnd:GetChildByName("Label" .. i):SetProperty("Text", desc)
    wnd:GetChildByName("Box" .. i):SetCustomUserData(0, itemID)
    wnd:GetChildByName("Box" .. i):SetCustomUserData(1, formulaID)
    wnd:GetChildByName("Box" .. i):SetUserData64(i)
    wnd:GetChildByName("Picture" .. i):SetCustomUserData(0, itemID)
    wnd:GetChildByName("State_pic" .. i):SetProperty("BackImage", _GoalP_TreasureStateName[state])
    wnd:GetChildByName("Time" .. i):SetProperty("Text", onlinetext)
    wnd:GetChildByName("Time" .. i):SetVisible(onlinetime > 0)
    _GoalP_ActiveList[i] = cost
    if state ~= 2 then
      wnd:GetChildByName("Box" .. i):SetProperty("Enable", "false")
      wnd:GetChildByName("Picture" .. i):SetProperty("Visible", "true")
    else
      wnd:GetChildByName("Box" .. i):SetProperty("Enable", "true")
      wnd:GetChildByName("Picture" .. i):SetProperty("Visible", "false")
      hasBoxActive = true
    end
    MaxBoxCost = cost
  end
  wnd2:GetChildByName("Label3_dlab"):SetProperty("Text", tostring(active))
  wnd2:GetChildByName("All_dlab"):SetProperty("Text", tostring(allact))
  local progress = 0
  if MaxBoxCost > 0 then
    progress = active / MaxBoxCost
  end
  local preciousBtn = gUI.GoalP.GoalPossess:GetChildByName("Precious_btn")
  preciousBtn:SetEnable(hasBoxActive)
end
function Lua_GoalP_ShowGoalPossess(bShow)
  gUI.GoalP.GoalPossess:SetVisible(bShow)
  if gUI.GoalP.GoalPossess:IsVisible() then
    _GoalP_InitFormulaList()
    AddTimerEvent(0, "GoalActiveBoxTime", Lua_GoalP_UpdateGoalActiveBoxTime)
  else
    DelTimerEvent(0, "GoalActiveBoxTime")
  end
end
function Lua_GoalP_UpdateGoalActiveBoxTime(timer)
  local list = GetFormulaList()
  if list[0] == nil then
    return
  end
  local wnd = gUI.GoalP.GoalPossess:GetChildByName("Pic1_bg")
  local state, onlinetime, onlinetext
  for i = 0, table.maxn(list) do
    state = list[i].State
    onlinetime = list[i].OnlineTime * 60
    onlinetext = Lua_UI_DetailTime(onlinetime) .. "后可领取"
    wnd:GetChildByName("Time" .. i):SetProperty("Text", onlinetext)
    wnd:GetChildByName("Time" .. i):SetVisible(onlinetime > 0)
    if state ~= 2 then
      wnd:GetChildByName("Box" .. i):SetProperty("Enable", "false")
      wnd:GetChildByName("Picture" .. i):SetProperty("Visible", "true")
    else
      wnd:GetChildByName("Box" .. i):SetProperty("Enable", "true")
      wnd:GetChildByName("Picture" .. i):SetProperty("Visible", "false")
    end
  end
end
function _OnGoalP_UpdateActive(active)
  _GoalP_InitFormulaList()
end
function _OnGoalP_UpdateMain()
  gUI.GoalP.DailyCnt:DeleteAll()
  local goalList = GetGoalList()
  local newItemBg
  local indexDaily = 0
  for index = 1, table.maxn(goalList) do
    local _item = goalList[index]
    if true == _item.daily then
      local nColNum = indexDaily % _GoalP_RowNum
      indexDaily = indexDaily + 1
      if nColNum == 0 then
        newItemBg = gUI.GoalP.DailyCnt:Insert(-1, gUI.GoalP.DailyTmp)
        newItemBg:SetVisible(true)
      end
      local newItem = newItemBg:GetChildByName("DailyItem" .. tostring(nColNum + 1) .. "_btn")
      newItem:SetVisible(true)
      local _, _desc, _, _, _, _count, _countmax, _, _, _, _, _, _, _, _image = GetGoalInfo(_item.id)
      newItem:GetChildByName("Label1_dlab"):SetProperty("Text", "活跃值+" .. tostring(_item.revactive))
      newItem:GetChildByName("Label2_dlab"):SetProperty("Text", tostring(_item.name))
      newItem:GetChildByName("Label3_dlab"):SetProperty("Text", "" .. _count .. "/" .. _countmax)
      newItem:GetChildByName("Label4_slab"):SetVisible(_item.complated)
      local btnItem = WindowToButton(newItem)
      btnItem:SetProperty("NormalImage", tostring(_image))
      btnItem:SetProperty("HoverImage", tostring(_image) .. "_h")
      btnItem:SetProperty("PushImage", tostring(_image))
      btnItem:SetProperty("SelectedImage", tostring(_image))
      btnItem:SetProperty("TooltipText", tostring(_desc))
      btnItem:SetCustomUserData(0, _item.id)
    end
  end
  gUI.GoalP.DailyCnt:SizeSelf()
end
function _OnGoalP_ComplateGoal(goalID)
  local qCount = gUI.GoalP.DailyCnt:GetChildCount()
  for indexBg = 0, qCount - 1 do
    local itemBgTmp = gUI.GoalP.DailyCnt:GetChild(indexBg)
    if itemBgTmp ~= nil then
      for indexItem = 1, _GoalP_RowNum do
        local qButton = itemBgTmp:GetChildByName("DailyItem" .. indexItem .. "_btn")
        if qButton ~= nil then
          local itemId = qButton:GetCustomUserData(0)
          if itemId == goalID then
            local _, _, _, _, _complated, _count, _countmax = GetGoalInfo(itemId)
            qButton:GetChildByName("Label3_dlab"):SetProperty("Text", "" .. _count .. "/" .. _countmax)
            qButton:GetChildByName("Label4_slab"):SetVisible(_complated)
            return
          end
        end
      end
    end
  end
end
function UI_GoalP_CloseFormulaList(msg)
  Lua_GoalP_ShowGoalPossess(false)
end
function UI_GoalP_OpenFormulaList(msg)
  if gUI.GoalP.GoalPossess:IsVisible() then
    Lua_GoalP_ShowGoalPossess(false)
  else
    Lua_GoalP_ShowGoalPossess(true)
  end
end
function UI_GoalP_BoxClickAll()
  local list, active, allact = GetFormulaList()
  local wnd = gUI.GoalP.GoalPossess:GetChildByName("Pic1_bg")
  for i = 0, table.maxn(list) do
    local box = wnd:GetChildByName("Box" .. i)
    if box:IsEnable() then
      local FormulaID = wnd:GetCustomUserData(1)
      local BoxId = wnd:GetUserData64()
      ExchangeRecommend(FormulaID, BoxId)
    end
  end
end
function UI_GoalP_ShowButtonItemTip(msg)
  local wnd = msg:get_window()
  local id = wnd:GetCustomUserData(0)
  if id ~= 0 then
    Lua_Tip_Item(wnd:GetToolTipWnd(0), nil, nil, nil, nil, id)
  end
end
function UI_GoalP_BoxClick(msg)
  local wnd = msg:get_window()
  local Itemid = wnd:GetCustomUserData(0)
  local FormulaID = wnd:GetCustomUserData(1)
  local BoxId = wnd:GetUserData64()
  local wndName = wnd:GetProperty("WindowName")
  local beginPos, endPos = string.find(wndName, "%d+$")
  index = tonumber(string.sub(wndName, beginPos, endPos))
  local Item = GetItemInfoBySlot(-1, Itemid, nil)
  Messagebox_Show("CARNIVAL_BOX", Item, FormulaID, BoxId)
end
function Script_GoalP_OnLoad()
  gUI.GoalP.GoalPossess = WindowSys_Instance:GetWindow("GoalPossess")
  gUI.GoalP.DailyCnt = WindowToContainer(WindowSys_Instance:GetWindow("GoalPossess.DailyGoal_cnt"))
  gUI.GoalP.DailyTmp = WindowSys_Instance:GetWindow("GoalPossess.DailyTmp_bg")
end
function Script_GoalP_OnEvent(event)
  if event == "CARNIVAL_ACTIVE_CHANGE" then
    _OnGoalP_UpdateActive(arg1)
  elseif event == "STAGEGOAL_REFRESH_UI" then
    _OnGoalP_UpdateMain()
  elseif event == "STAGEGOAL_COMPLATE" then
    _OnGoalP_ComplateGoal(arg1)
  end
end
function Script_GoalP_OnHide()
end
