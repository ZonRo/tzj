if gUI and not gUI.DaysMatters then
  gUI.DaysMatters = {}
end
function On_Daysmatters_AutoOpenPanel(bNotity)
  local b_flagOpen = GetDaymattersDatas()
  if not b_flagOpen then
    if not bNotity then
      Lua_Chat_ShowOSD("DAYMATTERS_CANOPEN")
    end
    return
  end
  if not gUI.DaysMatters.Root:IsVisible() then
    gUI.DaysMatters.Root:SetVisible(true)
    Lua_Daysmatters_UpdateDatas_All()
    return true
  end
  return false
end
function On_Daysmatters_notity_Openpanel()
  local Flag = DaymattersGetCurrentCheckOnline()
  if Flag then
    On_Daysmatters_AutoOpenPanel(true)
    gUI.DaysMatters.RemindCheckBtn:SetCheckedState(true)
  else
    gUI.DaysMatters.RemindCheckBtn:SetCheckedState(false)
  end
end
function On_Daysmatters_notity_update_Carnival()
  if gUI.DaysMatters.Root:IsVisible() then
    Lua_Daysmatters_UpdateDatas_Carnival()
  end
end
function On_Daysmatters_notity_update_Task()
  if gUI.DaysMatters.Root:IsVisible() then
    Lua_Daysmatters_UpdateDatas_Task()
  end
end
function On_Daysmatters_notity_update_Weal()
  if gUI.DaysMatters.Root:IsVisible() then
    Lua_Daysmatters_UpdateDatas_Weal()
  end
end
function On_Daysmatters_notity_UpdateCheckState(flag)
  gUI.DaysMatters.RemindCheckBtn:SetCheckedState(flag)
end
function On_Daysmatters_notity_Levelup()
  if gUI.DaysMatters.Root:IsVisible() then
    Lua_Daysmatters_UpdateDatas_All()
  end
end
function UI_Daysmatter_ShowGameOver()
  Lua_Detail_ShowChildWnd("Remind_bg", true)
  gUI.DaysMatters.ExitListBox = WindowToListBox(WindowSys_Instance:GetWindow("Detail.Remind_bg.Image_bg.Detail_lbox"))
  gUI.DaysMatters.ExitListBox:RemoveAllItems()
  local m_sTitle, m_sHandle, m_State, m_ToTimes, m_CurTimes, m_nId
  local List = GetDayHintInfos()
  for i = 1, table.maxn(List) do
    m_sTitle = List[i].m_sTitle
    m_sHandle = List[i].m_sHandle
    m_State = List[i].m_State
    m_ToTimes = List[i].m_ToTimes
    m_CurTimes = List[i].m_CurTimes
    m_nId = List[i].m_nId
    if m_State == 0 then
      local m_sShowInfo
      if m_ToTimes == 0 then
        if m_CurTimes > 0 then
          m_sShowInfo = string.format("%s|%s", m_sTitle, m_sHandle)
        end
      else
        m_sShowInfo = string.format("%s %d/%d(未完成)|%s", m_sTitle, m_CurTimes, m_ToTimes, m_sHandle)
      end
      local item = gUI.DaysMatters.ExitListBox:InsertString(m_sShowInfo, -1)
      gUI.DaysMatters.ExitListBox:SetProperty("TextHorzAlign", "Left")
      item:SetUserData(m_nId)
    end
  end
end
function UI_ExitGame(msg)
  Exit()
end
function UI_ExitWindow(msg)
  UI_Daysmatter_HideGameOver()
end
function UI_Daysmatter_HideGameOver()
  Lua_Detail_ShowChildWnd("Remind_bg", false)
end
function UI_Daysmatters_Click(msg)
end
function Lua_Daysmatters_UpdateDatas_Carnival()
  gUI.DaysMatters.CarListBox:RemoveAllItems()
  local m_sTitle, m_sHandle, m_CarivalState, m_CarivalToTimes, m_CarivalCurTimes
  local CarnivalList = GetDayHintCarnivalInfos()
  table.sort(CarnivalList, function(v1, v2)
    return v1.m_CarivalState < v2.m_CarivalState
  end)
  for i = 1, table.maxn(CarnivalList) do
    m_sTitle = CarnivalList[i].m_sTitle
    m_sHandle = CarnivalList[i].m_sHandle
    m_CarivalState = CarnivalList[i].m_CarivalState
    m_CarivalToTimes = CarnivalList[i].m_CarivalToTimes
    m_CarivalCurTimes = CarnivalList[i].m_CarivalCurTimes
    m_nId = CarnivalList[i].m_nId
    local m_sShowInfo
    if m_CarivalState == 0 then
      if m_CarivalToTimes > 0 then
        m_sShowInfo = string.format("%s(%d/%d)(未完成)|%s", m_sTitle, m_CarivalCurTimes, m_CarivalToTimes, m_sHandle)
      else
        m_sShowInfo = string.format("%s(未完成)|%s", m_sTitle, m_sHandle)
      end
      local item = gUI.DaysMatters.CarListBox:InsertString(m_sShowInfo, -1)
      gUI.DaysMatters.CarListBox:SetProperty("TextHorzAlign", "Left")
      item:SetUserData(m_nId)
    elseif m_CarivalState == 1 then
    end
  end
  for i = 1, table.maxn(CarnivalList) do
    m_sTitle = CarnivalList[i].m_sTitle
    m_sHandle = CarnivalList[i].m_sHandle
    m_CarivalState = CarnivalList[i].m_CarivalState
    m_CarivalToTimes = CarnivalList[i].m_CarivalToTimes
    m_CarivalCurTimes = CarnivalList[i].m_CarivalCurTimes
    m_nId = CarnivalList[i].m_nId
    local m_sShowInfo
    if m_CarivalState == 0 then
    elseif m_CarivalState == 1 then
      m_sShowInfo = string.format("{#G^%s(已完成)}|%s", m_sTitle, "")
      local item = gUI.DaysMatters.CarListBox:InsertString(m_sShowInfo, -1)
      gUI.DaysMatters.CarListBox:SetProperty("TextHorzAlign", "Left")
      item:SetUserData(m_nId)
    end
  end
end
function Lua_Daysmatters_UpdateDatas_Task()
  gUI.DaysMatters.TaskListBox:RemoveAllItems()
  local m_sTitle, m_sHandle, m_TaskState, m_TaskToTimes, m_TaskCurTimes, m_nId
  local TaskList = GetDayHintTaskInfos()
  for i = 1, table.maxn(TaskList) do
    m_sTitle = TaskList[i].m_sTitle
    m_sHandle = TaskList[i].m_sHandle
    m_TaskState = TaskList[i].m_TaskState
    m_TaskToTimes = TaskList[i].m_TaskToTimes
    m_TaskCurTimes = TaskList[i].m_TaskCurTimes
    m_nId = TaskList[i].m_nId
    local m_sShowInfos
    if m_TaskState == 0 then
      if m_TaskToTimes > 0 then
        m_sShowInfos = string.format("%s(%d/%d)(未完成)|%s", m_sTitle, m_TaskCurTimes, m_TaskToTimes, m_sHandle)
      else
        m_sShowInfos = string.format("%s(未完成)|%s", m_sTitle, m_sHandle)
      end
      local item = gUI.DaysMatters.TaskListBox:InsertString(m_sShowInfos, -1)
      gUI.DaysMatters.TaskListBox:SetProperty("TextHorzAlign", "Left")
      item:SetUserData(m_nId)
    else
    end
  end
  for i = 1, table.maxn(TaskList) do
    m_sTitle = TaskList[i].m_sTitle
    m_sHandle = TaskList[i].m_sHandle
    m_TaskState = TaskList[i].m_TaskState
    m_TaskToTimes = TaskList[i].m_TaskToTimes
    m_TaskCurTimes = TaskList[i].m_TaskCurTimes
    m_nId = TaskList[i].m_nId
    local m_sShowInfos
    if m_TaskState == 0 then
    else
      m_sShowInfos = string.format("{#G^%s(已完成)}|%s", m_sTitle, "")
      local item = gUI.DaysMatters.TaskListBox:InsertString(m_sShowInfos, -1)
      gUI.DaysMatters.TaskListBox:SetProperty("TextHorzAlign", "Left")
      item:SetUserData(m_nId)
    end
  end
end
function Lua_Daysmatters_UpdateDatas_Weal()
  gUI.DaysMatters.WealListBox:RemoveAllItems()
  local m_sTitle, m_sHandle, m_RemindTimes, m_nId
  local WealList = GetDayHintWealInfos()
  for i = 1, table.maxn(WealList) do
    m_sTitle = WealList[i].m_sTitle
    m_sHandle = WealList[i].m_sHandle
    m_RemindTimes = WealList[i].m_RemindTimes
    m_nId = WealList[i].m_nId
    local m_sShowInfo
    if m_RemindTimes > 0 then
      m_sShowInfo = string.format("%s|%s", m_sTitle, m_sHandle)
      local item = gUI.DaysMatters.WealListBox:InsertString(m_sShowInfo, -1)
      gUI.DaysMatters.WealListBox:SetProperty("TextHorzAlign", "Left")
      item:SetUserData(m_nId)
    end
  end
end
function Lua_Daysmatters_UpdateDatas_All()
  Lua_Daysmatters_UpdateDatas_Carnival()
  Lua_Daysmatters_UpdateDatas_Task()
  Lua_Daysmatters_UpdateDatas_Weal()
end
function UI_Daysmatters_RemindBtn_Click(msg)
  if msg == nil then
    return
  end
  local CheckBtn = msg:get_window()
  CheckBtn = WindowToCheckButton(CheckBtn)
  Daymatters_SetMsgToServer(CheckBtn:IsChecked())
end
function UI_Daysmatters_ExitBtn_Click(msg)
  UI_Daysmatters_Click(msg)
end
function Script_Daysmatters_OnEvent(event)
  if event == "DAYSMATTERS_UPDATE_CARNIVAL_INFO" then
    On_Daysmatters_notity_update_Carnival()
  elseif event == "DAYSMATTERS_UPDATE_TASK_INFO" then
    On_Daysmatters_notity_update_Task()
  elseif event == "DAYSMATTERS_UPDATE_WEAL_INFO" then
    On_Daysmatters_notity_update_Weal()
  elseif event == "DAYSMATTERS_NOTITY_OPENPANEL" then
  elseif event == "DAYSMATTERS_NOTITY_CHECKSTATE" then
    On_Daysmatters_notity_UpdateCheckState(arg1)
  elseif event == "DAYSMATTERS_NOTITY_LEVELUP" then
    On_Daysmatters_notity_Levelup()
  end
end
function Script_Daysmatters_OnLoad()
  gUI.DaysMatters.Root = WindowToPicture(WindowSys_Instance:GetWindow("Daysmatters"))
  gUI.DaysMatters.CancelBtn = WindowToButton(WindowSys_Instance:GetWindow("Daysmatters.Cancel_btn"))
  gUI.DaysMatters.CarListBox = WindowToListBox(WindowSys_Instance:GetWindow("Daysmatters.Daysmatters_bg.Left_bg.Recommend_lbox"))
  gUI.DaysMatters.TaskListBox = WindowToListBox(WindowSys_Instance:GetWindow("Daysmatters.Daysmatters_bg.Right_bg.Days_lbox"))
  gUI.DaysMatters.WealListBox = WindowToListBox(WindowSys_Instance:GetWindow("Daysmatters.Daysmatters_bg.Down_bg.Welfare_lbox"))
  gUI.DaysMatters.RemindCheckBtn = WindowToCheckButton(WindowSys_Instance:GetWindow("Daysmatters.Daysmatters_bg.Remind_cbtn"))
end
function Script_Daysmatters_OnHide()
end

