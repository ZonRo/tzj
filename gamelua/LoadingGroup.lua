function LoadingGroup_OnLoad()
  LoadingGroup_SetBackground()
  local bar = WindowSys_Instance:GetWindow("Picture1.Picture2_bg.loadingbar_pbar")
  bar = WindowToProgressBar(bar)
  bar:SetProgress(0)
  local back = WindowSys_Instance:GetWindow("Picture1")
  UILoadingBar:setloadingbar(back)
end
function LoadingGroup_SetBackground()
  local pic = WindowSys_Instance:GetWindow("Picture1.background_pic")
  local mapid = UILoadingBar:GetCurrLoadingMap()
  local image
  if mapid < 0 then
    image = gUI_MapLoadImage[5]
  else
    image = UILoadingBar:GetLoadingImage()
  end
  pic:SetProperty("BackImage", tostring(image))
  local tip = GetLoadingTip()
  if tip then
    local tipLabel = WindowSys_Instance:GetWindow("Picture1.Picture2_bg.Label1_dlab")
    tipLabel:SetProperty("Text", tip)
  end
end
function _LoadingGroup_DownLoadingText()
  local tipLabel = WindowSys_Instance:GetWindow("Picture1.Picture2_bg.Label1_dlab")
  tipLabel:SetProperty("Text", "正在下载资源...")
end
function LoadingGroup_OnEvent(event)
  if event == "LOADING_START" then
    LoadingGroup_SetBackground()
    local back = WindowSys_Instance:GetWindow("Picture1.back")
    local pic = WindowSys_Instance:GetWindow("Picture1")
    local par = WindowToProgressBar(WindowSys_Instance:GetWindow("Picture1.Picture2_bg.loadingbar_pbar"))
    local gStage = gUI_CurrentStage
    pic:SetProperty("Visible", "true")
    par:SetProperty("Visible", "true")
    par:SetProgress(arg2)
    wnd = WindowSys_Instance:GetWindow("Picture1.Picture2_bg.Label2_dlab")
    wnd:SetProperty("Text", "0%")
  elseif event == "LOADING_PROGRESS" then
    local bar = WindowSys_Instance:GetWindow("Picture1.Picture2_bg.loadingbar_pbar")
    if bar then
      bar = WindowToProgressBar(bar)
      bar:SetProgress(arg2)
      wnd = WindowSys_Instance:GetWindow("Picture1.Picture2_bg.Label2_dlab")
      local str = tostring(math.ceil(arg2 * 100))
      wnd:SetProperty("Text", str .. "%")
    end
    if arg3 == true then
      _LoadingGroup_DownLoadingText()
    end
  end
end

