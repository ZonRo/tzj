if gUI and not gUI.GuildQ then
  gUI.GuildQ = {}
end
local _GuildQ_CompStr = "UiIamge012:Image_Finish"
local _GuildQ_BeginStr = "UiIamge012:Image_UnderWay"
local _GuildQ_NoStr = "UiIamge012:Image_Close"
local _GuildQ_Fail = "UiIamge012:Image_Faild"
local _GuildQ_ActiveStr1 = "帮会本周活跃值"
local _GuildQ_ActiveStr2 = "需要本周活跃值"
local _GuildQ_Active = "UiIamge012:Image_Guildh"
local _GuildQ_NoActive = "UiIamge012:Image_Guildn"
local _GuildQ_BarPos = -1
local _GuildQ_MainBarPos = -1
local _GuildQ_AthenaStr = "当前守护波数为: %d/%d"
local _GuildQ_Athena_Map = 99
local _GuildQ_ActiveLvDesc = {
  [0] = {
    Name = "普通帮会",
    Image = "UiIamge018:Image_GuildCom"
  },
  [1] = {
    Name = "普通活跃帮会",
    Image = "UiIamge018:Image_GuildLevPri"
  },
  [2] = {
    Name = "中级活跃帮会",
    Image = "UiIamge018:Image_GuildLevMid"
  },
  [3] = {
    Name = "高级活跃帮会",
    Image = "UiIamge018:Image_GuildLevHigh"
  }
}
local _GuildQ_WeekList = {
  "周一",
  "周二",
  "周三",
  "周四",
  "周五",
  "周六",
  "周日"
}
local _GuildQ_UnopenedList = {}
local _GuildQ_ProcessList = {}
local _GuildQ_CompleteList = {}
local _GuildQ_UnopenedShow = true
local _GuildQ_ProcessShow = true
local _GuildQ_CompleteShow = true
local _GuildQ_InteriorQuest = {
  Spring = {
    name = "帮会修炼",
    icon = "UiIamge019:09",
    id = -1,
    Tip = "开启期间持续获得经验",
    x = 1901,
    y = _GuildQ_Athena_Map
  },
  Boss = {
    name = "召唤帮会守护兽",
    icon = "UiIamge019:10",
    id = -2,
    Tip = "击倒召唤兽可获得奖励宝箱",
    x = 1905,
    y = _GuildQ_Athena_Map
  }
}
local _GuildQ_QuestState = {
  ["unopened"] = 0,
  ["begin"] = 1,
  ["end"] = 2
}
function _GuildQ_SetQuestBtnSelected(questID)
  if questID < 0 then
    return false
  end
  local res = false
  local isBegin = false
  local qCount = gUI.GuildQ.QuestContainer:GetChildCount()
  for i = 0, qCount - 1 do
    local qButton = WindowToButton(gUI.GuildQ.QuestContainer:GetChild(i))
    local qBtnID = qButton:GetCustomUserData(0)
    isBegin = 0 ~= wnd:GetCustomUserData(1)
    if qBtnID ~= 0 then
      if qBtnID == questID then
        qButton:SetStatus("selected")
        res = true
      else
        qButton:SetStatus("normal")
      end
    end
  end
  return res
end
function _GuildQ_ShowSelectedQuestInfo(questID, isShow)
  local isShow = isShow and questID >= 0
  if isShow then
    _GuildQ_ShowQuestInfo(questID)
  else
    _GuildQ_ClaerQuestInfo()
  end
  gUI.GuildQ.Board:SetVisible(isShow)
end
function _GuildQ_ShowQuestInfo(questID)
  local BoardTask = gUI.GuildQ.BoardContainer
  local BoardFixGoodsBox = WindowToGoodsBox(BoardTask:GetGrandChild("Reward_gbox"))
  local BoardAward = WindowToDisplayBox(BoardTask:GetGrandChild("Reward_dbox"))
  local BoardTime = WindowToDisplayBox(BoardTask:GetGrandChild("Time_dbox"))
  local BoardDetail = WindowToDisplayBox(BoardTask:GetGrandChild("Explain_dbox"))
  local BoardTerm = WindowToDisplayBox(BoardTask:GetGrandChild("Term_dbox"))
  local BoardLimit = WindowToDisplayBox(BoardTask:GetGrandChild("Limit_dbox"))
  BoardTask:ResetScrollBar()
  BoardFixGoodsBox:ResetAllGoods(true)
  BoardAward:ClearAllContent()
  BoardTime:ClearAllContent()
  BoardDetail:ClearAllContent()
  BoardLimit:ClearAllContent()
  BoardTerm:ClearAllContent()
  local v = {}
  local w = {}
  local strBigIcon, RewExp, RewMoney, RewNimbus, RewBindedMoney, RewWrathValue, GuildMoney, GuildActive, GuildContribution, GuildConstruction, GuildPractice, IsGuildExp, strQuestDetail, hasWeek, time1, time2, timeLimit, hasLv
  strBigIcon, RewExp, RewMoney, RewNimbus, RewBindedMoney, RewWrathValue, GuildMoney, GuildActive, GuildContribution, GuildConstruction, GuildPractice, IsGuildExp, strQuestDetail, v[0], v[1], v[2], v[3], v[4], v[5], v[6], hasWeek, time1, time2, timeLimit, w[0], w[1], w[2], w[3], w[4], hasLv = GetGuildQuestInfoByQuestId(questID, gUI_QuestRewardItemSourceID.GUILD_QUEST_WINDOW)
  gUI.GuildQ.BoardPic:SetProperty("BackImage", strBigIcon)
  local strReward = ""
  if RewExp and RewExp > 0 then
    strReward = strReward .. string.format("经验值：%d点\n", RewExp)
    if IsGuildExp > 0 then
      local currentExp, nextExp = GetGuildLevMulitExp()
      if currentExp > 0 then
        strReward = strReward .. string.format("(当前帮会等级加成经验值: %d)\n", tonumber(RewExp * currentExp / 100))
      end
      if nextExp > 0 and currentExp > 0 then
        strReward = strReward .. string.format("(下一级加成经验值: %d)\n", tonumber(RewExp * nextExp / 100))
      end
    end
  end
  if RewNimbus and RewNimbus > 0 then
    strReward = strReward .. string.format("精魄值：%d点\n", RewNimbus)
  end
  if RewMoney and RewMoney > 0 then
    strReward = strReward .. "金  币：" .. Lua_UI_Money2String(RewMoney) .. "\n"
  end
  if RewBindedMoney and RewBindedMoney > 0 then
    strReward = strReward .. "绑  金：" .. Lua_UI_Money2String(RewBindedMoney) .. "\n"
  end
  if RewWrathValue and RewWrathValue ~= 0 then
    strReward = strReward .. "减少天诛值：" .. tostring(-RewWrathValue) .. "\n"
  end
  if GuildMoney and GuildMoney > 0 then
    strReward = strReward .. string.format("帮会资金：%s\n", Lua_UI_Money2String(GuildMoney * 10000))
  end
  if GuildActive and GuildActive > 0 then
    strReward = strReward .. string.format("帮会活跃值：%d点\n", GuildActive)
  end
  if GuildContribution and GuildContribution > 0 then
    strReward = strReward .. string.format("帮会贡献度：%d点\n", GuildContribution)
  end
  if GuildConstruction and GuildConstruction > 0 then
    strReward = strReward .. string.format("帮会发展度：%d点\n", GuildConstruction)
  end
  if GuildPractice and GuildPractice > 0 then
    strReward = strReward .. string.format("守护兽成长值：%d点\n", GuildPractice)
  end
  if strReward ~= "" then
    BoardAward:setEnableHeightCal(true)
    BoardAward:InsertBack(strReward, 0, 0, 0)
    BoardFixGoodsBox:GetChildByName("Reward_bg"):SetVisible(false)
  else
    BoardAward:setEnableHeightCal(false)
    BoardFixGoodsBox:GetChildByName("Reward_bg"):SetVisible(true)
  end
  if BoardFixGoodsBox:IsItemHasIcon(0) then
    BoardFixGoodsBox:setEnableHeightCal(true)
  else
    BoardFixGoodsBox:setEnableHeightCal(false)
  end
  local term = ""
  if hasLv then
    for i = 0, #w do
      if w[i] ~= -1 then
        term = term .. w[i] .. "级、"
      end
    end
    term = string.sub(term, 1, #term - 2) .. "帮会"
  else
    term = "不限等级"
  end
  if term ~= "" then
    BoardTerm:setEnableHeightCal(true)
    BoardTerm:InsertBack(term, 0, 0, 0)
  else
    BoardTerm:setEnableHeightCal(false)
  end
  local openTime = ""
  local openTime1 = "每天不限时间"
  if time2 > 0 then
    openTime1 = string.format("%02d:%02d - %02d:%02d", math.floor(time1 / 100), math.floor(time1 % 100), math.floor(time2 / 100), math.floor(time2 % 100))
  end
  if hasWeek then
    local week = {
      1,
      2,
      3,
      4,
      5,
      6,
      7
    }
    openTime = "每"
    for i = 0, #v do
      if v[i] ~= -1 then
        for j = 0, #week do
          if v[i] == week[j] then
            table.remove(week, j)
          end
        end
      end
    end
    for i = 1, #week do
      openTime = openTime .. _GuildQ_WeekList[week[i]] .. "、"
    end
    openTime = string.sub(openTime, 1, #openTime - 2) .. "的" .. openTime1
  else
    openTime = openTime1
  end
  if openTime ~= "" then
    BoardTime:setEnableHeightCal(true)
    BoardTime:InsertBack(openTime, 0, 0, 0)
  else
    BoardTime:setEnableHeightCal(false)
  end
  if timeLimit > 0 then
    BoardLimit:setEnableHeightCal(true)
    BoardLimit:InsertBack(tostring(Lua_Tip_ConvertTime(timeLimit * 1000)), 0, 0, 0)
  else
    BoardLimit:setEnableHeightCal(false)
  end
  if strQuestDetail then
    strQuestDetail = Lua_UI_TranslateText(strQuestDetail)
    BoardDetail:InsertBack(strQuestDetail, 0, 0, 0)
  end
end
function _GuildQ_ClaerQuestInfo()
  gUI.GuildQ.Board:SetVisible(false)
end
function _GuildQ_InsterMainQuest(insertList, openQuest)
  local list = insertList
  for i = 1, table.maxn(list) do
    local _item = list[i]
    local newItem = gUI.GuildM.MainQuestContainer:Insert(-1, gUI.GuildM.MainQuestChildTemp)
    newItem:SetProperty("Visible", "true")
    newItem:SetCustomUserData(0, _item.id)
    local name = _item.name
    if _item.begin then
      if _item.faild then
        name = name .. "(已失败)"
      end
    elseif 0 < _item.time1 and 0 < _item.time2 then
      local openTime = string.format("(%02d:%02d-%02d:%02d)", math.floor(_item.time1 / 100), math.floor(_item.time1 % 100), math.floor(_item.time2 / 100), math.floor(_item.time2 % 100))
      if not _item.IsOpenTime then
        openTime = string.format("{#R^%s}", openTime)
      end
      name = name .. " " .. openTime
    end
    newItem:GetChildByName("Name_slab"):SetProperty("Text", name)
    newItem:GetChildByName("Name_slab"):SetCustomUserData(0, _item.id)
    if _item.icon ~= "" then
      local ItemGoodsBox = WindowToGoodsBox(newItem:GetChildByName("Item_gbox"))
      ItemGoodsBox:SetItemGoods(0, _item.icon, -1)
      ItemGoodsBox:SetItemData(0, _item.id)
    end
    local operateBtn = newItem:GetChildByName("Operate_btn")
    operateBtn:SetCustomUserData(0, _item.id)
    local openBtn = newItem:GetChildByName("Open_btn")
    openBtn:SetCustomUserData(0, _item.id)
    if _item.begin then
      openBtn:SetVisible(false)
      if _item.complated then
        operateBtn:SetProperty("Text", "领奖")
        operateBtn:SetVisible(true)
      else
        operateBtn:SetVisible(false)
      end
    elseif _item.IsWorldQuest then
      if openQuest then
        openBtn:SetVisible(true)
      else
        openBtn:SetVisible(false)
      end
      operateBtn:SetVisible(false)
    else
      openBtn:SetVisible(false)
      operateBtn:SetProperty("Text", "接取")
      operateBtn:SetVisible(true)
    end
  end
end
function _GuildQ_InsterInteriorQuest(questType, open, hasPermission)
  if _GuildQ_InteriorQuest[questType] == nil then
    return
  end
  local id = _GuildQ_InteriorQuest[questType].id
  local newItem = gUI.GuildM.MainQuestContainer:Insert(-1, gUI.GuildM.MainQuestChildTemp)
  newItem:SetProperty("Visible", "true")
  newItem:SetCustomUserData(0, id)
  newItem:GetChildByName("Name_slab"):SetProperty("Text", _GuildQ_InteriorQuest[questType].name)
  newItem:GetChildByName("Name_slab"):SetCustomUserData(0, id)
  if _GuildQ_InteriorQuest[questType].icon ~= "" then
    local ItemGoodsBox = WindowToGoodsBox(newItem:GetChildByName("Item_gbox"))
    ItemGoodsBox:SetItemGoods(0, _GuildQ_InteriorQuest[questType].icon, -1)
    ItemGoodsBox:SetItemData(0, id)
  end
  local operateBtn = newItem:GetChildByName("Operate_btn")
  operateBtn:SetCustomUserData(0, id)
  local openBtn = newItem:GetChildByName("Open_btn")
  openBtn:SetCustomUserData(0, id)
  operateBtn:SetVisible(false)
  openBtn:SetVisible(false)
  if not open and hasPermission then
    operateBtn:SetProperty("Text", "开启")
    operateBtn:SetVisible(true)
  end
end
function Lua_GuildQ_UpdateBaseInfo()
  _OnGuildQ_UpdateBaseInfo(0)
end
function UI_GuildQ_ClickQuestBtn(msg)
  local wnd = msg:get_window()
  local questID = wnd:GetCustomUserData(0)
  _GuildQ_ShowSelectedQuestInfo(questID, questID >= 0)
  _GuildQ_SetQuestBtnSelected(questID)
  gUI.GuildQ.SelectedQuestID = questID
end
function UI_GuildQ_ClickBeginBtn(msg)
  local wnd = msg:get_window()
  if not gUI.GuildM.MainQuestContainer:IsMouseIn() then
    return
  end
  local questID = wnd:GetCustomUserData(0)
  if questID > 0 then
    BeginGuildQuest(questID)
  end
end
function UI_GuildQ_ShowQuestPrizeTip(msg)
  local wnd = msg:get_window()
  local index = wnd:GetCustomUserData(0)
  local briskList = GetGuildActiveInfo()
  if type(briskList) ~= "table" or #briskList < 3 then
    Lua_Chat_AddSysLog(UI_SYS_MSG.GUILD_BASEINFO_GETFAIL)
  else
    local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
    local active = briskList[index].Active
    local contribution = briskList[index].Contribution
    local bind = briskList[index].BindMoney
    local item = briskList[index].Item
    local num = briskList[index].ItemNum
    Lua_Tip_Begin(tooltip)
    local str = _GuildQ_ActiveLvDesc[index].Name
    tooltip:InsertLeftText(str, colorUserDefine, "kaiti_13", 0, 0)
    str = string.format("%s %d点", _GuildQ_ActiveStr2, active)
    tooltip:InsertLeftText(str, colorYellow, "fzheiti_11", 0, 0)
    tooltip:InsertLeftText("下周每日奖励", colorUserDefine, "kaiti_13", 0, 0)
    local itemid = tooltip:InsertLeftText("绑定金:", colorYellow, "fzheiti_11", 0, 0)
    _Tip_ShowMoney(tooltip, bind, itemid)
    if contribution > 0 then
      str = "帮会贡献度:" .. tostring(contribution)
      tooltip:InsertLeftText(str, colorYellow, "fzheiti_11", 0, 0)
    end
    if item ~= "" then
      str = string.format("%s x %d", item, num)
      tooltip:InsertLeftText(str, colorYellow, "fzheiti_11", 0, 0)
    end
    Lua_Tip_End(tooltip)
  end
end
function UI_GuildQ_ClickQuestType(msg)
  local wnd = msg:get_window()
  local questType = wnd:GetCustomUserData(0)
  if questType == 0 then
    _GuildQ_UnopenedShow = not _GuildQ_UnopenedShow
  elseif questType == 1 then
    _GuildQ_ProcessShow = not _GuildQ_ProcessShow
  elseif questType == 2 then
    _GuildQ_CompleteShow = not _GuildQ_CompleteShow
  end
  _OnGuildQ_UpdateQuestList(true)
end
function UI_GuildQ_ShowQuestTip(msg)
  if not gUI.GuildM.MainQuestContainer:IsMouseIn() then
    return
  end
  local questID = 0
  local wnd = msg:get_window()
  local wndType = wnd:GetProperty("ClassName")
  if wndType == "GoodsBox" then
    local wnd = WindowToGoodsBox(wnd)
    questID = wnd:GetItemData(0)
  elseif wndType == "Label" then
    questID = wnd:GetCustomUserData(0)
  else
    return
  end
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  if questID > 0 then
    local strTitle, strObjective, strIncomplete, nQuestLevel, nStatus, strQuestDetail, bDisableAbandon, MapName, NpcName, nCurrTimes, nTotalTimes, nQuestType, bCalLimitTime, IsTracking, RewExp, RewMoney, RewNimbus, RewBindedMoney, RewWrathValue, GuildMoney, GuildActive, GuildContribution, GuildConstruction, GuildPractice, IsGuildExp, bHaveChoiceItem, nCommission, v1, v2, v3, v4, v5, v6, v7 = GetQuestInfoLua(questID, gUI_QuestRewardItemSourceID.INVALID)
    Lua_Tip_Begin(tooltip)
    if v1 then
      strObjective = string.format(strObjective, v1, v2, v3, v4, v5, v6, v7)
    end
    local ObjectiveId = -1
    if strObjective ~= "" then
      tooltip:InsertLeftText("任务追踪", colorUserDefine, "kaiti_13", 0, 0)
      strObjective = GetParsedStr(strObjective)
      strObjective = Lua_UI_TranslateText(strObjective)
      strObjective = Lua_Tip_TranslateSpecil(strObjective)
      ObjectiveId = tooltip:InsertLeftText(strObjective, colorYellow, "fzheiti_11", 0, 0)
      tooltip:InsertLeftImage("UiIamge001:Image_Tooltip", 5, 5, 0, 0, false)
    end
    tooltip:InsertLeftText("任务奖励", colorUserDefine, "kaiti_13", 0, 0)
    local str = ""
    if RewExp and RewExp > 0 then
      str = string.format("经验值: %d点", RewExp)
      tooltip:InsertLeftText(str, colorYellow, "fzheiti_11", 0, 0)
      if IsGuildExp > 0 then
        local currentExp, nextExp = GetGuildLevMulitExp()
        if currentExp > 0 then
          str = string.format("(当前帮会等级加成经验值: %d)", tonumber(RewExp * currentExp / 100))
          tooltip:InsertLeftText(str, colorYellow, "fzheiti_11", 0, 0)
        end
        if nextExp > 0 and currentExp > 0 then
          str = string.format("(下一级加成经验值: %d)\n", tonumber(RewExp * nextExp / 100))
          tooltip:InsertLeftText(str, colorYellow, "fzheiti_11", 0, 0)
        end
      end
    end
    if RewNimbus and RewNimbus > 0 then
      str = string.format("精魄值: %d点", RewExp)
      tooltip:InsertLeftText(str, colorYellow, "fzheiti_11", 0, 0)
    end
    if RewMoney and RewMoney > 0 then
      local itemid = tooltip:InsertLeftText("金 币: ", colorYellow, "fzheiti_11", 0, 0)
      _Tip_ShowMoney(tooltip, RewMoney, itemid)
    end
    if RewBindedMoney and RewBindedMoney > 0 then
      local itemid = tooltip:InsertLeftText("绑 金: ", colorYellow, "fzheiti_11", 0, 0)
      _Tip_ShowMoney(tooltip, RewBindedMoney, itemid)
    end
    if RewWrathValue and RewWrathValue ~= 0 then
      str = string.format("减少天诛值: %d点", RewWrathValue)
      tooltip:InsertLeftText(str, colorYellow, "fzheiti_11", 0, 0)
    end
    if GuildMoney and GuildMoney > 0 then
      local itemid = tooltip:InsertLeftText("帮会资金: ", colorYellow, "fzheiti_11", 0, 0)
      _Tip_ShowMoney(tooltip, GuildMoney * 10000, itemid)
    end
    if GuildActive and GuildActive > 0 then
      str = string.format("帮会活跃值: %d点", GuildActive)
      tooltip:InsertLeftText(str, colorYellow, "fzheiti_11", 0, 0)
    end
    if GuildContribution and GuildContribution > 0 then
      str = string.format("帮会贡献度: %d点", GuildContribution)
      tooltip:InsertLeftText(str, colorYellow, "fzheiti_11", 0, 0)
    end
    if GuildConstruction and GuildConstruction > 0 then
      str = string.format("帮会发展度: %d点", GuildConstruction)
      tooltip:InsertLeftText(str, colorYellow, "fzheiti_11", 0, 0)
    end
    if GuildPractice and GuildPractice > 0 then
      str = string.format("守护兽成长值: %d点", GuildPractice)
      tooltip:InsertLeftText(str, colorYellow, "fzheiti_11", 0, 0)
    end
    Lua_Tip_End(tooltip)
  else
    for key, value in pairs(_GuildQ_InteriorQuest) do
      if value.id == questID then
        Lua_Tip_Begin(tooltip)
        tooltip:InsertLeftText(value.Tip, colorYellow, "fzheiti_11", 0, 0)
        Lua_Tip_End(tooltip)
      end
    end
  end
end
function UI_GuildQ_QuestOperate(msg)
  local wnd = msg:get_window()
  if not gUI.GuildM.MainQuestContainer:IsMouseIn() then
    return
  end
  local questID = wnd:GetCustomUserData(0)
  local x, y, mapid
  if questID > 0 then
    local _, _, _, _, _, _, _, _, NpcName = GetQuestInfoLua(questID, gUI_QuestRewardItemSourceID.INVALID)
    if NpcName == "" then
      x, y, mapid = 1919, 99, -1
    else
      x, y, mapid = string.match(NpcName, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)")
    end
  else
    for key, value in pairs(_GuildQ_InteriorQuest) do
      if value.id == questID then
        x = value.x
        y = value.y
        mapid = -1
        break
      end
    end
  end
  if tonumber(mapid) == -1 then
    local _, CurrentMapID = GetCurrentMiniMapInfo()
    if CurrentMapID ~= _GuildQ_Athena_Map and tonumber(y) == _GuildQ_Athena_Map then
      TalkToNpc(5, 50)
    else
      TalkToNpc(tonumber(x), tonumber(y))
    end
  else
    MoveToTargetLocation(tonumber(x), tonumber(y), tonumber(mapid))
  end
end
function _OnGuildQ_UpdateQuestListAll()
  gUI.GuildQ.Board:SetVisible(false)
  local scrollbar = WindowSys_Instance:GetWindow("GuildMain.GuildAct_bg.Image_bg.Left_bg.TaskList_cnt.vertSB_")
  if scrollbar then
    scrollbar = WindowToScrollBar(scrollbar)
    _GuildQ_BarPos = scrollbar:GetCurrentPos()
  end
  gUI.GuildQ.QuestContainer:DeleteAll()
  local questList = GetGuildQuestListAll()
  if questList == nil then
    Lua_Chat_AddSysLog(UI_SYS_MSG.GUILD_BASEINFO_GETFAIL)
    questList = {}
  end
  for i = 1, table.maxn(questList) do
    local _item = questList[i]
    local newItem = gUI.GuildQ.QuestContainer:Insert(-1, gUI.GuildQ.CntChildTemp)
    newItem:SetProperty("Visible", "true")
    newItem:SetCustomUserData(0, _item.id)
    newItem:GetChildByName("Name_dlab"):SetProperty("Text", _item.name)
    if _item.icon ~= "" then
      local ItemGoodsBox = WindowToGoodsBox(newItem:GetChildByName("Name_gbox"))
      ItemGoodsBox:SetItemGoods(0, _item.icon, -1)
    end
  end
  if 0 > gUI.GuildQ.SelectedQuestID then
    local qButton = WindowToButton(gUI.GuildQ.QuestContainer:GetChild(0))
    local qBtnID = qButton:GetCustomUserData(0)
    gUI.GuildQ.SelectedQuestID = qBtnID
  end
  local res = _GuildQ_SetQuestBtnSelected(gUI.GuildQ.SelectedQuestID)
  if not res then
    gUI.GuildQ.SelectedQuestID = -1
  end
  _GuildQ_ShowSelectedQuestInfo(gUI.GuildQ.SelectedQuestID, res)
  scrollbar = WindowSys_Instance:GetWindow("GuildMain.GuildAct_bg.Image_bg.Left_bg.TaskList_cnt.vertSB_")
  if scrollbar and scrollbar:IsVisible() and _GuildQ_BarPos >= 0 then
    scrollbar = WindowToScrollBar(scrollbar)
    scrollbar:SetCurrentPos(_GuildQ_BarPos)
  end
end
function _OnGuildQ_UpdateQuestList(isUpdate)
  if not gUI.GuildM.AllInfoUI:IsVisible() then
    return
  end
  local scrollbarMain = WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg.Left_bg.Act_cnt.vertSB_")
  if scrollbarMain then
    scrollbarMain = WindowToScrollBar(scrollbarMain)
    _GuildQ_MainBarPos = scrollbarMain:GetCurrentPos()
  end
  local openQuest, interior = GetGuildQuestPermission()
  gUI.GuildM.MainQuestContainer:DeleteAll()
  _GuildQ_UnopenedList = {}
  _GuildQ_ProcessList = {}
  _GuildQ_CompleteList = {}
  local questList = GetGuildQuestList()
  if questList == nil then
    Lua_Chat_AddSysLog(UI_SYS_MSG.GUILD_BASEINFO_GETFAIL)
    questList = {}
  end
  for i = 1, table.maxn(questList) do
    local _item = questList[i]
    if _item.begin then
      if _item.complated then
        if not _item.IsDone then
          table.insert(_GuildQ_CompleteList, _item)
        end
      else
        table.insert(_GuildQ_ProcessList, _item)
      end
    else
      table.insert(_GuildQ_UnopenedList, _item)
    end
  end
  local nCountComplete = table.maxn(_GuildQ_CompleteList)
  if nCountComplete > 0 then
    local newItem = gUI.GuildM.MainQuestContainer:Insert(-1, gUI.GuildM.Complete)
    newItem:SetProperty("Visible", "true")
    newItem = WindowToCheckButton(newItem)
    newItem:SetChecked(_GuildQ_CompleteShow)
    if _GuildQ_CompleteShow then
      _GuildQ_InsterMainQuest(_GuildQ_CompleteList, openQuest)
    end
  end
  local spring, boss = GetGuildQuestInterior()
  local nCountProcess = table.maxn(_GuildQ_ProcessList)
  if nCountProcess > 0 then
    local newItem = gUI.GuildM.MainQuestContainer:Insert(-1, gUI.GuildM.Process)
    newItem:SetProperty("Visible", "true")
    newItem = WindowToCheckButton(newItem)
    newItem:SetChecked(_GuildQ_ProcessShow)
    if _GuildQ_ProcessShow then
      _GuildQ_InsterMainQuest(_GuildQ_ProcessList, openQuest)
    end
  end
  if spring == _GuildQ_QuestState.begin and _GuildQ_ProcessShow then
    _GuildQ_InsterInteriorQuest("Spring", true, interior)
  end
  if boss == _GuildQ_QuestState.begin and _GuildQ_ProcessShow then
    _GuildQ_InsterInteriorQuest("Boss", true, interior)
  end
  local nCountUnopened = table.maxn(_GuildQ_UnopenedList)
  if nCountUnopened > 0 then
    local newItem = gUI.GuildM.MainQuestContainer:Insert(-1, gUI.GuildM.Unopened)
    newItem:SetProperty("Visible", "true")
    newItem = WindowToCheckButton(newItem)
    newItem:SetChecked(_GuildQ_UnopenedShow)
    if _GuildQ_UnopenedShow then
      _GuildQ_InsterMainQuest(_GuildQ_UnopenedList, openQuest)
    end
  end
  if spring == _GuildQ_QuestState.unopened and _GuildQ_UnopenedShow then
    _GuildQ_InsterInteriorQuest("Spring", false, interior)
  end
  if boss == _GuildQ_QuestState.unopened and _GuildQ_UnopenedShow then
    _GuildQ_InsterInteriorQuest("Boss", false, interior)
  end
  scrollbarMain = WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg.Left_bg.Act_cnt.vertSB_")
  if scrollbarMain and scrollbarMain:IsVisible() and _GuildQ_MainBarPos >= 0 then
    scrollbarMain = WindowToScrollBar(scrollbarMain)
    scrollbarMain:SetCurrentPos(_GuildQ_MainBarPos)
  end
end
function _OnGuildQ_UpdateQuestSelected(questID, isComplete)
end
function _OnGuildQ_UpdateBaseInfo(flag)
  if flag == 0 then
    local briskList, active, activeMax, activeLast = GetGuildActiveInfo()
    if type(briskList) ~= "table" or #briskList < 3 then
      Lua_Chat_AddSysLog(UI_SYS_MSG.GUILD_BASEINFO_GETFAIL)
    else
      local progress = active / activeMax
      gUI.GuildQ.ActivePbar:SetProperty("Progress", tostring(progress))
      local tips = string.format("%s：%d/%d", _GuildQ_ActiveStr1, active, activeMax)
      gUI.GuildQ.ActivePbar:SetProperty("TooltipText", tips)
      gUI.GuildQ.Brisk1:SetCustomUserData(0, 1)
      gUI.GuildQ.Brisk2:SetCustomUserData(0, 2)
      gUI.GuildQ.Brisk3:SetCustomUserData(0, 3)
      local active1 = briskList[1].Active
      local active2 = briskList[2].Active
      local active3 = briskList[3].Active
      local activeLv = 0
      if active > active1 then
        gUI.GuildQ.Brisk1:SetProperty("BackImage", _GuildQ_Active)
        activeLv = 1
      else
        gUI.GuildQ.Brisk1:SetProperty("BackImage", _GuildQ_NoActive)
      end
      if active > active2 then
        gUI.GuildQ.Brisk2:SetProperty("BackImage", _GuildQ_Active)
        activeLv = 2
      else
        gUI.GuildQ.Brisk2:SetProperty("BackImage", _GuildQ_NoActive)
      end
      if active > active3 then
        gUI.GuildQ.Brisk3:SetProperty("BackImage", _GuildQ_Active)
        activeLv = 3
      else
        gUI.GuildQ.Brisk3:SetProperty("BackImage", _GuildQ_NoActive)
      end
      local activeLastlv = 0
      if activeLast > active1 then
        activeLastlv = 1
      end
      if activeLast > active2 then
        activeLastlv = 2
      end
      if activeLast > active3 then
        activeLastlv = 3
      end
      gUI.GuildQ.ActiveName:SetProperty("Text", _GuildQ_ActiveLvDesc[activeLv].Name)
      gUI.GuildQ.LastActiveName:SetProperty("Text", _GuildQ_ActiveLvDesc[activeLastlv].Name)
      gUI.GuildQ.ActivePic:SetProperty("BackImage", _GuildQ_ActiveLvDesc[activeLastlv].Image)
    end
  end
end
function _OnGuildQ_ShowAthena(bShow, mapID)
  if _GuildQ_Athena_Map == mapID then
    gUI.GuildQ.Athena:SetVisible(bShow)
  end
end
function _OnGuildQ_AthenaChange(currentCount, maxCount, mapID)
  if _GuildQ_Athena_Map == mapID then
    if not gUI.GuildQ.Athena:IsVisible() then
      gUI.GuildQ.Athena:SetVisible(true)
    end
    local wnd = WindowSys_Instance:GetWindow("GuildQuest_Athena.GuildQuest_Athena.Pro_bg.Pro_dlab")
    local str = string.format(_GuildQ_AthenaStr, currentCount, maxCount)
    wnd:SetProperty("Text", str)
  end
end
function Script_GuildQ_OnEvent(event)
  if event == "GUILD_QUEST_UPDATELIST" then
    _OnGuildQ_UpdateQuestList(arg1)
  elseif event == "PLAYER_QUEST_UPDATESINGLE" then
    _OnGuildQ_UpdateQuestSelected(arg1, arg2)
  elseif event == "GUILD_BASE_INFO_UPDATE" then
    _OnGuildQ_UpdateBaseInfo(arg1)
  elseif event == "GUILD_SHOW_ATHENA" then
    _OnGuildQ_ShowAthena(arg1, arg2)
  elseif event == "GUILD_ATHENA_COUNT" then
    _OnGuildQ_AthenaChange(arg1, arg2, arg3)
  end
end
function Script_GuildQ_OnLoad()
  gUI.GuildQ.Main = WindowSys_Instance:GetWindow("GuildMain")
  gUI.GuildQ.QuestMain = WindowSys_Instance:GetWindow("GuildMain.GuildAct_bg")
  gUI.GuildQ.QuestContainer = WindowToContainer(WindowSys_Instance:GetWindow("GuildMain.GuildAct_bg.Image_bg.Left_bg.TaskList_cnt"))
  gUI.GuildQ.CntChildTemp = WindowSys_Instance:GetWindow("GuildMain.GuildAct_bg.Image_bg.Left_bg.Item_btn")
  gUI.GuildQ.Board = WindowSys_Instance:GetWindow("GuildMain.GuildAct_bg.Image_bg.Right_bg")
  gUI.GuildQ.BoardContainer = WindowToContainer(WindowSys_Instance:GetWindow("GuildMain.GuildAct_bg.Image_bg.Right_bg.Quest_cnt"))
  gUI.GuildQ.ActivePbar = WindowToProgressBar(WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg.Center_bg.Up_bg.Active_bg.Active_bar"))
  gUI.GuildQ.Brisk1 = WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg.Center_bg.Up_bg.Active_bg.LowBrisk_pic")
  gUI.GuildQ.Brisk2 = WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg.Center_bg.Up_bg.Active_bg.CentreBrisk_pic")
  gUI.GuildQ.Brisk3 = WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg.Center_bg.Up_bg.Active_bg.HighBrisk_pic")
  gUI.GuildQ.ActiveName = WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg.Center_bg.Up_bg.Lev_dlab")
  gUI.GuildQ.ActivePic = WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg.Center_bg.Up_bg.Lev_pic")
  gUI.GuildQ.LastActiveName = WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg.Center_bg.Up_bg.LastLev_dlab")
  gUI.GuildQ.BoardPic = WindowSys_Instance:GetWindow("GuildMain.GuildAct_bg.Image_bg.Right_bg.Map_pic")
  gUI.GuildQ.Board:SetVisible(false)
  gUI.GuildQ.SelectedQuestID = -1
  _GuildQ_BarPos = 0
  local scrollbar = WindowSys_Instance:GetWindow("GuildMain.GuildAct_bg.Image_bg.Left_bg.TaskList_cnt.vertSB_")
  if scrollbar then
    scrollbar = WindowToScrollBar(scrollbar)
    scrollbar:SetCurrentPos(_GuildQ_BarPos)
  end
  _GuildQ_MainBarPos = 0
  local scrollbarMain = WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg.Left_bg.Act_cnt.vertSB_")
  if scrollbarMain then
    scrollbarMain = WindowToScrollBar(scrollbarMain)
    scrollbarMain:SetCurrentPos(_GuildQ_MainBarPos)
  end
  gUI.GuildQ.Athena = WindowSys_Instance:GetWindow("GuildQuest_Athena")
  gUI.GuildM.MainQuestContainer = WindowToContainer(WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg.Left_bg.Act_cnt"))
  gUI.GuildM.MainQuestChildTemp = WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg.Left_bg.Item_bg")
  gUI.GuildM.Unopened = WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg.Left_bg.CheckButton1")
  gUI.GuildM.Process = WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg.Left_bg.CheckButton2")
  gUI.GuildM.Complete = WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg.Left_bg.CheckButton3")
end

