if gUI and not gUI.Party then
  gUI.Party = {}
end
local _Party_DefaultBackImage = "UiIamge012:Image_Leave"
local _Party_DeadBackImage = "UiIamge012:Dead_Party"
local _Party_STR_YOU = "您"
local _Party_Index_Max = 3
local USER_GUIDE_TEAM_ID = 87
local USER_GUIDE_TEAMINVITE_ID = 88
local CHANGE_TYPE = {
  leaveTeam = 0,
  change = 1,
  offline = 2
}
function _Party_Invite_Reject()
  RejectTeamInvitations()
  gUI.Party.Invite_list:RemoveAllItems()
  gUI.Party._root:SetProperty("Visible", "false")
  DeleteHint(USER_GUIDE_TEAM_ID)
end
function _Party_Invite_SingleReject()
  local list = gUI.Party.Invite_list
  local item = list:GetSelectedItem()
  if item then
    local guid = item:GetUserData64()
    SingleRejectAskLeaderInvite(guid)
    list:RemoveItemByData64(guid)
  end
  if list:GetItemCount() == 0 then
    gUI.Party._root:SetProperty("Visible", "false")
    DeleteHint(USER_GUIDE_TEAM_ID)
  end
end
function _Party_Apply_Reject(msg)
  RejectTeamApplys()
  gUI.Party.Apply_list:RemoveAllItems()
  gUI.Party._root:SetProperty("Visible", "false")
  DeleteHint(USER_GUIDE_TEAM_ID)
end
function _Party_Apply_SingleReject()
  local list = gUI.Party.Apply_list
  local item = list:GetSelectedItem()
  if item then
    local guid = item:GetUserData64()
    SingleRejectTeamApplys(guid)
    list:RemoveItemByData64(guid)
  end
  if list:GetItemCount() == 0 then
    gUI.Party._root:SetProperty("Visible", "false")
    DeleteHint(USER_GUIDE_TEAM_ID)
  end
end
function _Party_Invite_Agree()
  local list = gUI.Party.Invite_list
  local item = list:GetSelectedItem()
  if item then
    local guid = item:GetUserData64()
    AcceptTeamInvitation(guid)
    list:RemoveItemByData64(guid)
    _Party_Invite_Reject()
  end
end
function _Party_Apply_Agree()
  local list = gUI.Party.Apply_list
  local item = list:GetSelectedItem()
  if item then
    local guid = item:GetUserData64()
    AcceptTeamApply(guid)
    list:RemoveItemByData64(guid)
  end
  if list:GetItemCount() == 0 then
    gUI.Party._root:SetProperty("Visible", "false")
    DeleteHint(USER_GUIDE_TEAM_ID)
  end
end
function _Party_Invite_Init()
  local Label2 = gUI.Party._root:GetChildByName("Label2")
  gUI.Party.Invite_list = WindowToListBox(gUI.Party._root:GetChildByName("ListBox"))
  gUI.Party.Invite_list:RemoveAllItems()
  gUI.Party._root:GetChildByName("Agree"):SetProperty("Value", "1")
  gUI.Party._root:GetChildByName("Reject"):SetProperty("Value", "1")
  gUI.Party._root:GetChildByName("Close"):SetProperty("Value", "1")
  gUI.Party._root:GetChildByName("Refuse_btn"):SetProperty("Value", "1")
end
function _Party_Apply_Init()
  local Label2 = gUI.Party._root:GetChildByName("Label2")
  gUI.Party.Apply_list = WindowToListBox(gUI.Party._root:GetChildByName("ListBox"))
  gUI.Party.Apply_list:RemoveAllItems()
  gUI.Party._root:GetChildByName("Agree"):SetProperty("Value", "2")
  gUI.Party._root:GetChildByName("Reject"):SetProperty("Value", "2")
  gUI.Party._root:GetChildByName("Close"):SetProperty("Value", "2")
  gUI.Party._root:GetChildByName("Refuse_btn"):SetProperty("Value", "2")
end
function _OnParty_LeaderChanged(new_leader_name, old_leader_name, change_type)
  local myname = GetMyPlayerStaticInfo()
  if change_type == CHANGE_TYPE.leaveTeam then
    if new_leader_name == myname then
      Lua_Chat_ShowSysLog("GROUP_LEADER_LEAVE_NEWLEADER", old_leader_name)
    else
      Lua_Chat_ShowSysLog("GROUP_LEADER_LEAVE_OTHERMEMBER", old_leader_name, new_leader_name)
    end
  elseif change_type == CHANGE_TYPE.change then
    if new_leader_name == myname then
      Lua_Chat_ShowSysLog("GROUP_LEADER_CHANGE_TOME", old_leader_name)
    elseif old_leader_name == myname then
      Lua_Chat_ShowSysLog("GROUP_LEADER_CHANGE_METO", new_leader_name)
    else
      Lua_Chat_ShowSysLog("GROUP_LEADER_CHANGE", new_leader_name)
    end
  elseif change_type == CHANGE_TYPE.offline then
    if new_leader_name == myname then
      Lua_Chat_ShowSysLog("GROUP_LEADER_OFFLINE_TOME", old_leader_name)
    else
      Lua_Chat_ShowSysLog("GROUP_LEADER_OFFLINE", old_leader_name, new_leader_name)
    end
  end
  _OnParty_UpdateAll()
end
function _OnParty_UpdateAll()
  Lua_MenuHide("ON_TEAMMATE")
  local index = 0
  local l_index = 0
  local isself, name, level, max_hp, hp, max_mp, mp, is_leader, head, hair_model, hair_color, offline, menpai, objid, gender, isDead, _, _, is_same_scene, _, _, _, _, _, _, lineId = GetTeamMemberInfo(l_index, false)
  local iamleader = true
  while name and index <= _Party_Index_Max do
    if not isself then
      local player = gUI.Party.BgRoot:GetChildByName("player" .. index)
      local player2 = gUI.Party.BgRoot2:GetChildByName("player" .. index)
      player:GetChildByName("name"):SetProperty("Text", name)
      player2:GetChildByName("name"):SetProperty("Text", name)
      local progress = not offline and max_hp ~= 0 and hp / max_hp or 1
      local hp_bar = player:GetChildByName("hp")
      local hp_bar2 = player2:GetChildByName("hp")
      if isDead == 0 then
        hp_bar:SetProperty("Progress", tostring(0))
        hp_bar2:SetProperty("Progress", tostring(0))
      else
        hp_bar:SetProperty("Progress", tostring(progress))
        hp_bar2:SetProperty("Progress", tostring(progress))
      end
      if hp < max_hp * 0.4 then
        hp_bar:SetProperty("Blink", "true")
        hp_bar2:SetProperty("Blink", "true")
      else
        hp_bar:SetProperty("Blink", "false")
        hp_bar2:SetProperty("Blink", "false")
      end
      if max_hp == 0 then
        hp_bar:GetChildByName("hpLab"):SetVisible(false)
        hp_bar2:GetChildByName("hpLab"):SetVisible(false)
      else
        hp_bar:GetChildByName("hpLab"):SetVisible(true)
        hp_bar2:GetChildByName("hpLab"):SetVisible(true)
      end
      hp_bar:GetChildByName("hpLab"):SetProperty("Text", tostring(hp) .. "/" .. tostring(max_hp))
      hp_bar2:GetChildByName("hpLab"):SetProperty("Text", tostring(hp) .. "/" .. tostring(max_hp))
      progress = not offline and max_mp ~= 0 and mp / max_mp or 1
      local mp_bar = player:GetChildByName("mp")
      local ep_bar = player:GetChildByName("ep")
      local bar
      if menpai == 4 then
        mp_bar:SetProperty("Visible", "false")
        ep_bar:SetProperty("Visible", "true")
        bar = ep_bar
      else
        ep_bar:SetProperty("Visible", "false")
        mp_bar:SetProperty("Visible", "true")
        bar = mp_bar
        if menpai == 1 then
          mp_bar:SetProperty("FrontImage", "UiIamge012:Image_TeamEP")
        elseif menpai == 0 then
          mp_bar:SetProperty("FrontImage", "UiIamge012:Image_TeamSP")
        else
          mp_bar:SetProperty("FrontImage", "UiIamge012:Image_TeamMP")
        end
        if max_mp == 0 then
          bar:GetChildByName("Bar_Lab"):SetVisible(false)
        else
          bar:GetChildByName("Bar_Lab"):SetVisible(true)
        end
        bar:GetChildByName("Bar_Lab"):SetProperty("Text", tostring(mp) .. "/" .. tostring(max_mp))
      end
      if bar then
        bar:SetProperty("Progress", tostring(progress))
      end
      local portrait = player:GetChildByName("Portrait")
      local buff = WindowToGoodsBox(player:GetChildByName("Buff"))
      local debuff = WindowToGoodsBox(player:GetChildByName("DeBuff"))
      if isDead == 0 then
        portrait:SetProperty("GrayImage", "0")
        portrait:SetProperty("BackImage", _Party_DeadBackImage)
      elseif offline ~= nil and not offline then
        portrait:SetProperty("GrayImage", "0")
        if head < 9 then
          local isInStall, modelID = GetPlayerInfo(false, objid)
          if isInStall and modelID >= -7 and modelID <= -2 then
            if offline ~= nil and not offline and is_same_scene then
            else
              portrait:SetProperty("GrayImage", "1")
            end
            portrait:SetProperty("BackImage", "")
          else
            if offline ~= nil and not offline and is_same_scene then
            else
              portrait:SetProperty("GrayImage", "1")
            end
            portrait:SetProperty("BackImage", "")
          end
        end
      else
        portrait:SetProperty("GrayImage", "0")
        portrait:SetProperty("BackImage", _Party_DefaultBackImage)
        buff:SetItemCount(0)
        buff:ResetAllGoods(true)
        debuff:SetItemCount(0)
        debuff:ResetAllGoods(true)
      end
      iamleader = iamleader and not is_leader
      player:GetChildByName("captain"):SetVisible(is_leader)
      player2:GetChildByName("captain"):SetVisible(is_leader)
      player:GetChildByName("level"):SetProperty("Text", tostring(level))
      player2:GetChildByName("level"):SetProperty("Text", tostring(level))
      local propic = player:GetChildByName("status")
      local pic2 = player2:GetChildByName("profession_pic")
      if menpai >= 0 and menpai <= 4 then
        pic2:SetProperty("TooltipText", gUI_MenPaiName[menpai])
        propic:SetProperty("BackImage", gUI_PROFESSION_INTRODUCE[menpai].sign)
        pic2:SetProperty("BackImage", "")
      else
        pic2:SetProperty("BackImage", "")
        propic:SetProperty("BackImage", "")
      end
      if isDead == 0 then
        pic2:SetProperty("BackImage", _Party_DeadBackImage)
      elseif offline then
        pic2:SetProperty("BackImage", _Party_DefaultBackImage)
      end
      player:SetVisible(true)
      player2:SetVisible(true)
      index = index + 1
    end
    l_index = l_index + 1
    isself, name, level, max_hp, hp, max_mp, mp, is_leader, head, hair_model, hair_color, offline, menpai, objid, gender, isDead, _, _, is_same_scene = GetTeamMemberInfo(l_index, false)
  end
  for i = index, _Party_Index_Max do
    gUI.Party.BgRoot:GetChildByName("player" .. i):SetVisible(false)
    gUI.Party.BgRoot2:GetChildByName("player" .. i):SetVisible(false)
  end
  _OnParty_UpdateFriendBtn()
  local wnd = WindowSys_Instance:GetWindow("PlayerStats.TopSelf_bg.Stats_bg.Teamflag_bg")
  if index > 0 then
    wnd:SetVisible(iamleader)
  else
    wnd:SetVisible(false)
  end
end
function _OnParty_UpdateFriendBtn(friend_type)
  if friend_type ~= nil and friend_type ~= gUI_RELATION_TYPE.friend then
    return
  end
  if WindowSys_Instance:GetWindow("PartyStats"):IsVisible() == false then
    return
  end
  local index = 0
  while index <= _Party_Index_Max do
    local playerPic = gUI.Party.BgRoot:GetChildByName("player" .. index)
    local playerName = playerPic:GetChildByName("name"):GetProperty("Text")
    if playerPic:IsVisible() then
      local friendBtn = playerPic:GetChildByName("friend_btn")
      local _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, mate_guid = GetTeamMemberInfo(playerName, true)
      local info_f = GetRelationInfo(mate_guid, gUI_RELATION_TYPE.friend)
      friendBtn:SetVisible(info_f == nil)
    end
    index = index + 1
  end
end
function _OnParty_UpdateAura(index, debuff, buff)
  local _Party_Aura_Table = GetTeamMemberAuraInfo(index)
  local debuffFlag = _Party_Aura_Table[0][1]
  local buffFlag = _Party_Aura_Table[0][2]
  local buffid = 0
  local resttime = 0
  local count = 0
  local duration = 0
  buff:SetCustomUserData(0, debuffFlag)
  if buffFlag ~= nil and debuffFlag ~= nil then
    buff:SetItemCount(buffFlag - debuffFlag)
    debuff:SetItemCount(debuffFlag)
    if debuffFlag > 0 then
      for i = 1, debuffFlag do
        buffid = _Party_Aura_Table[i][1]
        resttime = _Party_Aura_Table[i][2] * 1000
        count = _Party_Aura_Table[i][3]
        duration = _Party_Aura_Table[i][4]
        if buffid then
          debuff:SetItemGoods(i - 1, buffid, 2)
          if resttime > 0 and duration ~= 0 then
            debuff:SetItemCoolTime(i - 1, duration, resttime)
          end
          if count > 1 then
            debuff:SetItemSubscript(i - 1, tostring(count))
          end
        else
          debuff:ClearGoodsItem(i - 1)
        end
      end
    end
    if debuffFlag < buffFlag then
      for i = debuffFlag + 1, buffFlag do
        buffid = _Party_Aura_Table[i][1]
        resttime = _Party_Aura_Table[i][2] * 1000
        count = _Party_Aura_Table[i][3]
        duration = _Party_Aura_Table[i][4]
        if buffid then
          buff:SetItemGoods(i - debuffFlag - 1, buffid, 2)
          if resttime > 0 and duration ~= 0 then
            buff:SetItemCoolTime(i - debuffFlag - 1, duration, resttime)
          end
          if count > 1 then
            buff:SetItemSubscript(i - debuffFlag - 1, tostring(count))
          end
        else
          buff:ClearGoodsItem(i - 1)
        end
      end
    end
  end
end
function _OnParty_DisMissed()
  _OnParty_UpdateAll()
  local wnd = WindowSys_Instance:GetWindow("PlayerStats.TopSelf_bg.Stats_bg.Teamflag_bg")
  wnd:SetVisible(false)
  local root = WindowSys_Instance:GetWindow("PartyStats")
  root:SetVisible(false)
end
function _OnParty_Created(arg1)
  local wnd = WindowSys_Instance:GetWindow("PlayerStats.TopSelf_bg.Stats_bg.Teamflag_bg")
  wnd:SetVisible(true)
  local myname = GetMyPlayerStaticInfo()
  if arg1 == myname then
    WorldStage:playSoundByID(26)
  end
end
function _OnParty_DistributeModeChanged(arg1, isModify)
  Lua_Rcm_SetDistributeMode(arg1)
  if isModify then
    Lua_Chat_ShowSysLog("CHANGE_DISTRIBUTE_MODE_MODIFY", gUI_GOODS_DISTRIBUTE_MODE[arg1 + 1])
  else
    Lua_Chat_ShowSysLog("CHANGE_DISTRIBUTE_MODE", gUI_GOODS_DISTRIBUTE_MODE[arg1 + 1])
  end
  local mode_dlab = WindowSys_Instance:GetWindow("PartyStats.Image_bg.Allot_bg.Allot_dlab")
  mode_dlab:SetProperty("Text", gUI_GOODS_DISTRIBUTE_MODE[arg1 + 1])
  if arg1 == 0 then
    mode_dlab:SetProperty("FontColor", gUI_GOODS_QUALITY_COLOR_PREFIX[4])
  else
    mode_dlab:SetProperty("FontColor", gUI_GOODS_QUALITY_COLOR_PREFIX[1])
  end
end
function _OnParty_DistributeLevelChanged(arg1, isModify)
  Lua_Rcm_SetDistributeLevel(arg1)
  if isModify then
    Lua_Chat_ShowSysLog("CHANGE_DISTRIBUTE_LEVEL_MODIFY", gUI_GOODS_DISTRIBUTE_QUALITY[arg1 + 1])
  else
    Lua_Chat_ShowSysLog("CHANGE_DISTRIBUTE_LEVEL", gUI_GOODS_DISTRIBUTE_QUALITY[arg1 + 1])
  end
  local mode_dlab = WindowSys_Instance:GetWindow("PartyStats.Image_bg.Collect_bg.Collect_dlab")
  mode_dlab:SetProperty("Text", gUI_GOODS_DISTRIBUTE_QUALITY[arg1 + 1])
  mode_dlab:SetProperty("FontColor", gUI_GOODS_QUALITY_COLOR_PREFIX[arg1])
end
function _OnParty_MemberInfoUpdate(member_name, index)
  local isself, name, level, max_hp, hp, max_mp, mp, is_leader, head, hair_model, hair_color, offline, menpai, objid, gender, IsDead, _, _, is_same_scene = GetTeamMemberInfo(index, false)
  local player, player2
  for i = 0, 3 do
    player = gUI.Party.BgRoot:GetGrandChild("player" .. i)
    player2 = gUI.Party.BgRoot2:GetGrandChild("player" .. i)
    if player:GetChildByName("name"):GetProperty("Text") == member_name then
      break
    end
  end
  local progress = not offline and max_hp ~= 0 and hp / max_hp or 1
  local hp_bar = player:GetChildByName("hp")
  local hp_bar2 = player2:GetChildByName("hp")
  if IsDead == 0 then
    hp_bar:SetProperty("Progress", tostring(progress))
    hp_bar2:SetProperty("Progress", tostring(progress))
  else
    hp_bar:SetProperty("Progress", tostring(progress))
    hp_bar2:SetProperty("Progress", tostring(progress))
  end
  if hp < max_hp * 0.4 then
    hp_bar:SetProperty("Blink", "true")
    hp_bar2:SetProperty("Blink", "true")
  else
    hp_bar:SetProperty("Blink", "false")
    hp_bar2:SetProperty("Blink", "false")
  end
  if max_hp == 0 then
    hp_bar:GetChildByName("hpLab"):SetVisible(false)
    hp_bar2:GetChildByName("hpLab"):SetVisible(false)
  else
    hp_bar:GetChildByName("hpLab"):SetVisible(true)
    hp_bar2:GetChildByName("hpLab"):SetVisible(true)
  end
  hp_bar:GetChildByName("hpLab"):SetProperty("Text", tostring(hp) .. "/" .. tostring(max_hp))
  hp_bar2:GetChildByName("hpLab"):SetProperty("Text", tostring(hp) .. "/" .. tostring(max_hp))
  progress = not offline and max_mp ~= 0 and mp / max_mp or 1
  local mp_bar = player:GetChildByName("mp")
  local ep_bar = player:GetChildByName("ep")
  local bar
  if menpai == 4 then
    mp_bar:SetProperty("Visible", "false")
    ep_bar:SetProperty("Visible", "true")
    bar = ep_bar
  else
    ep_bar:SetProperty("Visible", "false")
    mp_bar:SetProperty("Visible", "true")
    bar = mp_bar
    if menpai == 1 then
      mp_bar:SetProperty("FrontImage", "UiIamge012:Image_TeamEP")
    elseif menpai == 0 then
      mp_bar:SetProperty("FrontImage", "UiIamge012:Image_TeamSP")
    else
      mp_bar:SetProperty("FrontImage", "UiIamge012:Image_TeamMP")
    end
    if max_mp == 0 then
      bar:GetChildByName("Bar_Lab"):SetVisible(false)
    else
      bar:GetChildByName("Bar_Lab"):SetVisible(true)
    end
    bar:GetChildByName("Bar_Lab"):SetProperty("Text", tostring(mp) .. "/" .. tostring(max_mp))
  end
  if bar then
    bar:SetProperty("Progress", tostring(progress))
  end
  local portrait = player:GetChildByName("Portrait")
  if IsDead == 0 then
    portrait:SetProperty("BackImage", _Party_DeadBackImage)
  else
    if head < 9 then
      portrait:SetProperty("GrayImage", "0")
      local isInStall, modelID = GetPlayerInfo(false, objid)
      if isInStall and modelID >= -7 and modelID <= -2 then
        if offline ~= nil and not offline and is_same_scene then
        else
          portrait:SetProperty("GrayImage", "1")
        end
        portrait:SetProperty("BackImage", "")
      else
        if offline ~= nil and not offline and is_same_scene then
        else
          portrait:SetProperty("GrayImage", "1")
        end
        portrait:SetProperty("BackImage", "")
      end
    end
    if offline ~= nil and not offline then
    else
      portrait:SetProperty("GrayImage", "0")
      portrait:SetProperty("BackImage", _Party_DefaultBackImage)
    end
  end
  if IsDead == 0 then
    local pic2 = player2:GetChildByName("profession_pic")
    pic2:SetProperty("BackImage", _Party_DefaultBackImage)
  elseif offline then
    local pic2 = player2:GetChildByName("profession_pic")
    pic2:SetProperty("BackImage", _Party_DefaultBackImage)
  end
  player:GetChildByName("level"):SetProperty("Text", tostring(level))
  player2:GetChildByName("level"):SetProperty("Text", tostring(level))
  if is_same_scene ~= nil and not is_same_scene then
    _OnParty_MemberAuraUpdate(name, -1)
  end
  _OnParty_UpdateFriendBtn()
end
function _OnParty_MemberAuraUpdate(member_name, index)
  local _, _, _, _, _, _, _, _, _, _, _, offline, _, _, _, _, _, _, isSameScene = GetTeamMemberInfo(index, false)
  local player
  for i = 0, 3 do
    player = gUI.Party.BgRoot:GetGrandChild("player" .. i)
    if player:GetChildByName("name"):GetProperty("Text") == member_name then
      break
    end
  end
  local buff = WindowToGoodsBox(player:GetChildByName("Buff"))
  local debuff = WindowToGoodsBox(player:GetChildByName("DeBuff"))
  if offline ~= nil and not offline and isSameScene then
    _OnParty_UpdateAura(index, debuff, buff)
  else
    buff:SetItemCount(0)
    buff:ResetAllGoods(true)
    debuff:SetItemCount(0)
    debuff:ResetAllGoods(true)
  end
end
function _OnParty_MemberJoin(name)
  local root = WindowSys_Instance:GetWindow("PartyStats")
  root:SetVisible(true)
  local myname = GetMyPlayerStaticInfo()
  if name ~= myname then
    local str = string.format(UI_SYS_MSG.GROUP_NEW_MEMBER, name)
    Lua_Chat_AddSysLog(str)
  else
    WorldStage:playSoundByID(26)
  end
  _OnParty_UpdateAll()
end
function _OnParty_MemberLeave(name, iskick)
  local myname = GetMyPlayerStaticInfo()
  local ismyname = false
  if name == myname then
    ismyname = true
  end
  local str = ""
  if iskick then
    if ismyname then
      str = string.format(UI_SYS_MSG.GROUP_KICK_MEMBER_ME)
    else
      str = string.format(UI_SYS_MSG.GROUP_KICK_MEMBER, name)
    end
    Lua_Chat_AddSysLog(str)
  else
    if ismyname then
      str = string.format(UI_SYS_MSG.GROUP_DEL_MEMBER_ME)
    else
      str = string.format(UI_SYS_MSG.GROUP_DEL_MEMBER, name)
    end
    Lua_Chat_AddSysLog(str)
  end
  _OnParty_UpdateAll()
  if ismyname then
    local root = WindowSys_Instance:GetWindow("PartyStats")
    root:SetVisible(false)
  end
end
function _OnParty_MemberOnline(name, isonline)
  local str
  if isonline then
    str = string.format(UI_SYS_MSG.GROUP_ONLINE_MEMBER, name)
  else
    str = string.format(UI_SYS_MSG.GROUP_OFFLINE_MEMBER, name)
  end
  Lua_Chat_AddSysLog(str)
end
function _OnParty_ReceiveInvite(count, max_count)
  _Party_Invite_Init()
  if count > 0 then
    if max_count < count then
      count = max_count
    end
    local termax = count - 1
    local m_names = gUI_MenPaiName
    for i = 0, termax do
      local name, level, menpai, member_guid = GetTeamInvitationInfo(i)
      if name then
        local str = string.format("%s|%d|%s", name, level, m_names[menpai])
        local item = gUI.Party.Invite_list:InsertString(str, -1)
        item:SetUserData64(member_guid)
      end
    end
    gUI.Party.Invite_list:SetSelectItem(0)
  else
    gUI.Party._root:SetProperty("Visible", "false")
    DeleteHint(USER_GUIDE_TEAM_ID)
  end
end
function _OnParty_ReceiveApply(count, max_count)
  _Party_Apply_Init()
  if count > 0 then
    if max_count < count then
      count = max_count
    end
    local termax = count - 1
    local m_names = gUI_MenPaiName
    for i = 0, termax do
      local name, level, is_friend, menpai, member_guid = GetTeamApplyInfo(i)
      if name then
        local str = string.format("%s|%d|%s", name, level, m_names[menpai])
        local item = gUI.Party.Apply_list:InsertString(str, -1)
        item:SetUserData64(member_guid)
      end
    end
    gUI.Party.Apply_list:SetSelectItem(0)
  else
    gUI.Party._root:SetProperty("Visible", "false")
    DeleteHint(USER_GUIDE_TEAM_ID)
  end
end
function _OnTeam_AskLeaderInvite(member_guid, sName, dName, menpai, level)
  gUI.Party.Ask_List = WindowToListBox(gUI.Party._rootTeamInvite:GetChildByName("ListBox_lbox"))
  local m_names = gUI_MenPaiName
  local str = string.format("%s|%s|%d|%s", dName, sName, level, m_names[menpai])
  local item = gUI.Party.Ask_List:InsertString(str, -1)
  item:SetUserData64(member_guid)
  gUI.Party.Ask_List:SetSelectItem(0)
end
function _OnTeam_InstanceKick(timeSec, isshow)
  if isshow then
    Messagebox_Show("INSTANCE_KICK", timeSec)
    return
  end
  Messagebox_Hide("INSTANCE_KICK")
end
function Lua_Party_GetPaoPaoPos(memName)
  local party = WindowSys_Instance:GetWindow("PartyStats")
  local root = gUI.Party.BgRoot
  if root:IsVisible() == false then
    root = gUI.Party.BgRoot2
  end
  for i = 0, 3 do
    local wnd = root:GetChildByName("player" .. i)
    local picture = wnd:GetChildByName("Picture1")
    local nameLab = wnd:GetChildByName("name")
    local nameStr = nameLab:GetProperty("Text")
    if nameStr == memName and wnd:IsVisible() then
      return party:GetLeft() + root:GetLeft() + wnd:GetLeft() + picture:GetRight(), party:GetTop() + root:GetTop() + wnd:GetTop() + nameLab:GetTop() + 0.012
    end
  end
  return 0, 0
end
function UI_Party_BufferTooltip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local parent = wnd:GetParent()
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  if tooltip then
    local index = msg:get_wparam()
    local name = parent:GetChildByName("name"):GetProperty("Text")
    if wnd:GetProperty("WindowName") == "Buff" then
      index = index + wnd:GetCustomUserData(0)
    end
    local name, remaintime, des = GetTeamMemberAuraTipInfo(index, name)
    if name then
      Lua_Tip_Begin(tooltip, nil, "false")
      do
        local count = tonumber(wnd:GetItemSubscript(msg:get_wparam())) or 1
        des = string.gsub(des, "%$(%d+)", function(s)
          return tonumber(s) * count
        end)
        local itemid = tooltip:InsertLeftText(name, "FF1E90FF", "kaiti_12", 0, 0)
        itemid = tooltip:InsertLeftText(des, "FFFAFAFA", "fzheiti_11", 0, 0)
        Lua_Tip_End(tooltip)
      end
    end
  end
end
function UI_Party_OnTeammateLClick(msg)
  local wnd = msg:get_window():GetParent()
  local index = tonumber(string.sub(wnd:GetProperty("WindowName"), -1, -1))
  SetUITarget(index)
end
function UI_Party_OnFriendClick(msg)
  local wnd = msg:get_window():GetParent()
  local name = wnd:GetChildByName("name"):GetProperty("Text")
  Lua_Fri_AddFriend(name)
end
function UI_Party_Tooltip(msg)
  local wnd = msg:get_window():GetParent()
  local index = tonumber(string.sub(wnd:GetProperty("WindowName"), -1, -1))
  local name = wnd:GetChildByName("name"):GetProperty("Text")
  local _, _, _, _, _, _, _, _, _, _, _, offline, _, objid = GetTeamMemberInfo(name, true)
  if offline == true then
    Lua_Tip_ShowOfflineTooltip(name)
  else
    Lua_Tip_ShowPartyTooltip(name)
  end
end
function UI_Party_TooltipClose()
  Lua_Tip_HideObjTooltip()
end
function UI_Party_Agree(msg)
  local wnd = msg:get_window()
  if wnd:GetProperty("Value") == "1" then
    _Party_Invite_Agree()
  else
    _Party_Apply_Agree()
  end
end
function UI_Party_Reject(msg)
  local wnd = msg:get_window()
  if wnd:GetProperty("Value") == "1" then
    _Party_Invite_Reject()
  else
    _Party_Apply_Reject()
  end
end
function UI_Party_SingleReject(msg)
  local wnd = msg:get_window()
  if wnd:GetProperty("Value") == "1" then
    _Party_Invite_SingleReject()
  else
    _Party_Apply_SingleReject()
  end
end
function UI_Party_Close(msg)
  gUI.Party._root:SetProperty("Visible", "false")
end
function UI_Party_AgreeAskLeaderInvite(msg)
  local list = gUI.Party.Ask_List
  local item = list:GetSelectedItem()
  if item then
    local guid = item:GetUserData64()
    local inviteeName = item:GetText(0)
    AgreeAskLeaderInvite(guid, inviteeName)
    list:RemoveItemByData64(guid)
  end
  if list:GetItemCount() == 0 then
    gUI.Party._rootTeamInvite:SetProperty("Visible", "false")
    DeleteHint(USER_GUIDE_TEAMINVITE_ID)
  end
end
function UI_Party_CloseTeamInvite()
  gUI.Party._rootTeamInvite:SetProperty("Visible", "false")
end
function UI_Party_RejectAskLeaderInvite(msg)
  local list = gUI.Party.Ask_List
  local item = list:GetSelectedItem()
  if item then
    local guid = item:GetUserData64()
    RejectAskLeaderInvite(guid)
    list:RemoveItemByData64(guid)
    local memberName = item:GetText(1)
    Lua_Chat_ShowSysLog("TEAM_LEADER_REFUSESEND", memberName)
  end
  if list:GetItemCount() == 0 then
    gUI.Party._rootTeamInvite:SetProperty("Visible", "false")
    DeleteHint(USER_GUIDE_TEAMINVITE_ID)
  end
end
function UI_Party_RejectAllAskLeaderInvite()
  local list = gUI.Party.Ask_List
  local item
  for i = 0, list:GetItemCount() do
    item = list:GetItem(i)
    if item then
      local guid = item:GetUserData64()
      RejectAskLeaderInvite(guid)
      list:RemoveItemByData64(guid)
      local memberName = item:GetText(1)
      Lua_Chat_ShowSysLog("TEAM_LEADER_REFUSESEND", memberName)
    end
  end
  gUI.Party._rootTeamInvite:SetProperty("Visible", "false")
  DeleteHint(USER_GUIDE_TEAMINVITE_ID)
end
function UI_Party_ChangeGroupMode(msg)
  local change_cbtn = WindowSys_Instance:GetWindow("PartyStats.Image_bg.Change_cbtn")
  if gUI.Party.BgRoot:IsVisible() then
    gUI.Party.BgRoot:SetProperty("Visible", "false")
    gUI.Party.BgRoot2:SetProperty("Visible", "true")
  else
    gUI.Party.BgRoot:SetProperty("Visible", "true")
    gUI.Party.BgRoot2:SetProperty("Visible", "false")
  end
end
function Guide_OnTeamShow(msg)
  gUI.Party._root:SetProperty("Visible", "true")
end
function Guide_OnTeamInviteShow(msg)
  gUI.Party._rootTeamInvite:SetProperty("Visible", "true")
end
function Script_Party_InviteList_OnHide()
end
function Script_Party_TeamInviteList_OnHide()
end
function Script_Party_OnLoad()
  gUI.Party._root = WindowSys_Instance:GetWindow("InviteList")
  gUI.Party._rootTeamInvite = WindowSys_Instance:GetWindow("TeamInviteList")
  gUI.Party.BgRoot = WindowSys_Instance:GetWindow("PartyStats.Normal_bg")
  gUI.Party.BgRoot2 = WindowSys_Instance:GetWindow("PartyStats.Simple_bg")
  local root = WindowSys_Instance:GetWindow("PartyStats")
  root:SetVisible(false)
  local wnd = WindowSys_Instance:GetWindow("PlayerStats.TopSelf_bg.Stats_bg.Teamflag_bg")
  wnd:SetProperty("Visible", "false")
end
function Script_Party_OnEvent(event)
  if event == "TEAM_MEMBER_INFO_UPDATE" then
    _OnParty_MemberInfoUpdate(arg1, arg2)
  elseif event == "TEAM_MEMBER_AURA_UPDATE" then
    _OnParty_MemberAuraUpdate(arg1, arg2)
  elseif event == "TEAM_MEMBER_JOIN" then
    _OnParty_MemberJoin(arg1)
  elseif event == "TEAM_MEMBER_LEAVE" then
    _OnParty_MemberLeave(arg1, arg2)
  elseif event == "TEAM_MEMBER_ONLINE" then
    _OnParty_MemberOnline(arg1, arg2)
  elseif event == "TEAM_DISMISSED" then
    _OnParty_DisMissed()
  elseif event == "TEAM_LEADER_CHANGED" then
    _OnParty_LeaderChanged(arg1, arg2, arg3)
  elseif event == "TEAM_CREATED" then
    _OnParty_Created(arg1)
  elseif event == "DISTRIBUTE_MODE_CHANGED" then
    _OnParty_DistributeModeChanged(arg1, arg2)
  elseif event == "DISTRIBUTE_LEVEL_CHANGED" then
    _OnParty_DistributeLevelChanged(arg1, arg2)
  elseif event == "TEAM_BE_REQUESTED" then
    _OnParty_ReceiveApply(arg1, arg2)
  elseif event == "TEAM_BE_INVITED" then
    _OnParty_ReceiveInvite(arg1, arg2)
  elseif event == "TEAM_ASKLEADER_INVITE" then
    _OnTeam_AskLeaderInvite(arg1, arg2, arg3, arg4, arg5)
  elseif event == "TEAM_INSTANCE_KICK" then
    _OnTeam_InstanceKick(arg1, arg2)
  elseif event == "TEAM_UPDATE_ALLINFO" then
    _OnParty_UpdateAll()
  elseif event == "COMMUNITY_UPDATE_RELATION" then
    _OnParty_UpdateFriendBtn(arg1)
  elseif event == "COMMUNITY_DEL_RELATION" then
    _OnParty_UpdateFriendBtn(arg1)
  end
end
