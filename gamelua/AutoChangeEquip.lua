if gUI and not gUI.AutoChangeEquip then
  gUI.AutoChangeEquip = {}
end
local AutoChangeBag = -1
local AutoChangeSlot = -1
local AutoChangeType = -1
local AutoChangeGuid = 0
function AutoChangeClearSlots()
  AutoChangeBag = -1
  AutoChangeSlot = -1
  AutoChangeType = -1
  gUI.AutoChangeEquip.EgBox:ClearGoodsItem(0)
  gUI.AutoChangeEquip.NameLab:SetProperty("Text", "")
end
function UI_AutoChangeAutoOpen(bag, slot, index)
  AutoChangeBag = bag
  AutoChangeSlot = slot
  AutoChangeType = index
  AutoChangeGuid = GetItemGUIDByPos(bag, slot, 0)
  gUI.AutoChangeEquip.Root:SetVisible(true)
  local ItemName, ItemIcon, quality = GetItemInfoBySlot(bag, slot, 0)
  gUI.AutoChangeEquip.EgBox:SetItemGoods(0, ItemIcon, quality)
  local itemname = "{#C^" .. gUI_GOODS_QUALITY_COLOR_PREFIX[quality] .. "^" .. tostring(ItemName) .. "}"
  gUI.AutoChangeEquip.NameLab:SetProperty("Text", itemname)
end
function UI_ShowAutoChangeTip(msg)
  if gUI.AutoChangeEquip.EgBox:IsItemHasIcon(0) then
    local win = msg:get_window()
    local boxName = win:GetProperty("WindowName")
    local tooltip = win:GetToolTipWnd(0)
    Lua_Tip_Item(tooltip, AutoChangeSlot, AutoChangeBag, nil, nil, nil)
    Lua_Tip_ShowEquipCompare(win, AutoChangeBag, AutoChangeSlot)
  end
end
function UI_CloseAutoChangeClick(msg)
  AutoChangeClearSlots()
  UI_AutoChangeAutoClose()
end
function UI_AutoChangeEquipClick(msg)
  if AutoChangeBag == gUI_GOODSBOX_BAG.backpack0 and AutoChangeType ~= -1 and gUI.AutoChangeEquip.EgBox:IsItemHasIcon(0) then
    MoveItem(AutoChangeBag, AutoChangeSlot, gUI_GOODSBOX_BAG.myequipbox, AutoChangeType)
  end
  AutoChangeClearSlots()
  UI_AutoChangeAutoClose()
end
function UI_AutoChangeAutoClose()
  gUI.AutoChangeEquip.Root:SetVisible(false)
end
function _OnAutoChangeEquip_NewItem(bagType, slot)
  local playLevel = gUI_MainPlayerAttr.Level
  if playLevel <= HINT_MAX_PLYAE_LEVEL then
    SetMsgToItemNewHint(bagType, slot)
  else
    return
  end
end
function Check_NewItemNotHint(bagType, slot)
  local MaterName, MaterIconName, _, _, _, _, _, _, _, _, _, MaterItemNum, _, _, _, _, _, _, _, _, _, _, _, _, _, _, ItemIds = GetItemInfoBySlot(bagType, slot, 0)
  for i = 1, table.maxn(G_ALLJOBFOREQUIP) do
    if ItemIds == G_ALLJOBFOREQUIP[i] then
      return false
    end
  end
  return true
end
function _OnAutoChangeEquip_AutoOpen(bagType, slot, index)
  if not Check_NewItemNotHint(bagType, slot) then
    return
  end
  if bagType == gUI_GOODSBOX_BAG.backpack0 and index ~= -1 then
    UI_AutoChangeAutoOpen(bagType, slot, index)
  end
end
function _OnAutoChangeEquip_ItemDelete(bag, slot, isAdd)
  local NewGuid = GetItemGUIDByPos(bag, slot, 0)
  if isAdd ~= nil then
    if bag == AutoChangeBag and AutoChangeSlot == slot and NewGuid ~= AutoChangeGuid then
      AutoChangeClearSlots()
      UI_AutoChangeAutoClose()
    else
    end
  elseif bag == AutoChangeBag and AutoChangeSlot == slot then
    AutoChangeClearSlots()
    UI_AutoChangeAutoClose()
  else
    return
  end
end
function Script_AutoChangeEquip_OnLoad()
  gUI.AutoChangeEquip.Root = WindowSys_Instance:GetWindow("AutoChangeEquip")
  gUI.AutoChangeEquip.EgBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("AutoChangeEquip.AutoChangeEquip.Equip_gbox"))
  gUI.AutoChangeEquip.NameLab = WindowToLabel(WindowSys_Instance:GetWindow("AutoChangeEquip.AutoChangeEquip.Label2_dlab"))
  AutoChangeClearSlots()
  gUI.AutoChangeEquip.Root:SetVisible(false)
end
function Lua_AutoEquipChange()
  if AutoChangeBag ~= -1 and AutoChangeSlot ~= -1 and AutoChangeType ~= -1 then
    UI_AutoChangeAutoOpen(AutoChangeBag, AutoChangeSlot, AutoChangeType)
  end
end
function Script_AutoChangeEquip_OnEvent(event)
  if event == "ITEM_BAG_NEW" then
    _OnAutoChangeEquip_NewItem(arg1, arg2)
  elseif event == "ITEM_AUTO_OPEN" then
    _OnAutoChangeEquip_AutoOpen(arg1, arg2, arg3)
  elseif event == "ITEM_DEL_FROM_BAG" then
    _OnAutoChangeEquip_ItemDelete(arg1, arg2)
  elseif event == "ITEM_ADD_TO_BAG" then
    _OnAutoChangeEquip_ItemDelete(arg1, arg2, arg3)
  end
end
function Script_AutoChangeEquip_OnHide()
end
