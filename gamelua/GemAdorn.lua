if gUI and not gUI.GemAdorn then
  gUI.GemAdorn = {}
end
GEMADORNSLOT = {EQUIPSLOT = 0, MATERSLOT = 1}
GEMADORNSTATE = {Beset = 0, Extraction = 1}
function GemAdorn_ShowInterface()
  if gUI.GemAdorn.Root:IsVisible() then
    gUI.GemAdorn.Root:SetVisible(false)
  else
    gUI.GemAdorn.Root:SetVisible(true)
    Lua_Bag_ShowUI(true)
    gUI.GemAdorn.AdornCtrl:SetSelectedState(GEMADORNSTATE.Beset)
    gUI.GemAdorn.bRoot:SetVisible(true)
    gUI.GemAdorn.eRoot:SetVisible(false)
    GemAdorn_InitData()
  end
end
function GemAdorn_CloseInterface()
  if gUI.GemAdorn.Root:IsVisible() then
    gUI.GemAdorn.Root:SetVisible(false)
  end
end
function UI_GemAdorn_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("GemAdorn.GemAdorn_bg.Title_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function GemAdorn_GetCurrMode()
  if gUI.GemAdorn.AdornCtrl:GetSelectItem() == GEMADORNSTATE.Beset then
    return GEMADORNSTATE.Beset
  else
    return GEMADORNSTATE.Extraction
  end
end
function GemAdorn_InitData()
  if GemAdorn_GetCurrMode() == GEMADORNSTATE.Beset then
    local bagMain, slotMain = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_EMBED, GEMADORNSLOT.EQUIPSLOT)
    if slotMain then
      local _, icon, quality = GetItemInfoBySlot(bagMain, slotMain, 0)
      if not icon then
        return
      end
      gUI.GemAdorn.bMainSlot:SetItemGoods(0, icon, quality)
      local intenLev = Lua_Bag_GetStarInfo(bagMain, slotMain)
      if 0 < ForgeLevel_To_Stars[intenLev] then
        gUI.GemAdorn.bMainSlot:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
      else
        gUI.GemAdorn.bMainSlot:SetItemStarState(0, 0)
      end
      local bagMater, slotMater = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_EMBED, GEMADORNSLOT.MATERSLOT)
      if slotMater then
        local _, icon, quality = GetItemInfoBySlot(bagMater, slotMater, 0)
        if not icon then
          return
        end
        gUI.GemAdorn.bMaterSlot:SetItemGoods(0, icon, quality)
      else
        gUI.GemAdorn.bMaterSlot:ClearGoodsItem(0)
      end
      local attrList = GetEquipExAttr()
      GemAdornBSetBlendAttr(attrList)
    else
      gUI.GemAdorn.bMainSlot:ClearGoodsItem(0)
      gUI.GemAdorn.bMaterSlot:ClearGoodsItem(0)
      gUI.GemAdorn.bTips:RemoveAllItems()
    end
  else
    local bagMain, slotMain = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_MOVEOUT, GEMADORNSLOT.EQUIPSLOT)
    if bagMain then
      local _, icon, quality = GetItemInfoBySlot(bagMain, slotMain, 0)
      if not icon then
        return
      end
      gUI.GemAdorn.eMainSlot:SetItemGoods(0, icon, quality)
      local gemId, gemIcon, gemType, gemValue, gemName, gemNa = GetEquipHasGemInfo(1)
      if gemId ~= 0 then
        gUI.GemAdorn.eMaterSlot:SetItemGoods(0, gemIcon, Lua_Bag_GetQuality(-1, gemId))
        gUI.GemAdorn.eMaterSlot:SetCustomUserData(0, gemId)
      end
      GemAdorn_UpdateeMoney()
    else
      gUI.GemAdorn.eMainSlot:ClearGoodsItem(0)
      gUI.GemAdorn.eMaterSlot:ClearGoodsItem(0)
      gUI.GemAdorn.eMonLab:SetProperty("Text", Lua_UI_Money2String(0))
    end
  end
end
function GemAdorn_UpdateeMoney(newMoney, changeMoney, event)
  if not gUI.GemAdorn.Root:IsVisible() or not gUI.GemAdorn.eRoot:IsVisible() then
    return
  end
  local gemId, gemIcon, gemType, gemValue, gemName, gemNa = GetEquipHasGemInfo(1)
  local money, materialId, materialNum, materialToNum, materialIcon = GetGemExtarInfos(gemId)
  if money ~= nil then
    if money > 0 then
      gUI.GemAdorn.eMonLab:SetProperty("Text", Lua_UI_Money2String(money, false, false, false, true, newMoney, event))
    else
      gUI.GemAdorn.eMonLab:SetProperty("Text", Lua_UI_Money2String(0))
    end
  end
end
function GemAdornBSetBlendAttr(attrList)
  if attrList[0] == nil then
    return
  end
  gUI.GemAdorn.bTips:RemoveAllItems()
  local _, _, _, _, _, _, attrtypes1, attrvlaues1 = GetSubGemBesetInfo(GEMADORNSLOT.MATERSLOT)
  local bag1, slot1 = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_EMBED, GEMADORNSLOT.MATERSLOT)
  for i = 0, table.maxn(attrList) do
    if attrList[i].name ~= nil then
      if Lua_Tip_NeedShowRate(attrList[i].typeId) then
        attrList[i].value = attrList[i].value / 100
      end
      local showStr = ""
      showStr = tostring(attrList[i].name) .. " " .. tostring(attrList[i].value)
      if Lua_Tip_NeedShowRate(attrList[i].typeId) then
        showStr = showStr .. "%"
      end
      local listAttr = GetItemGemAttr(bag1, slot1, 0)
      if listAttr ~= nil then
        if listAttr[1].MainAttrValue ~= 0 and listAttr[1].MaintypeId == attrList[i].typeId then
          local newValue = math.floor(listAttr[1].MainAttrValue * attrList[i].value / 100)
          if Lua_Tip_NeedShowRate(attrList[i].typeId) then
            showStr = showStr .. "  {#G^+" .. string.format("%.2f", Lua_Tip_PostProcessVal(attrList[i].typeId, newValue)) .. "%" .. "(" .. tostring(listAttr[1].MainAttrValue) .. "%)" .. "}"
          else
            showStr = showStr .. "  {#G^+" .. tostring(newValue) .. "(" .. tostring(listAttr[1].MainAttrValue) .. "%)" .. "}"
          end
        end
        for j = 0, listAttr[1].AttrCount - 1 do
          if listAttr[1][j].typeId == attrList[i].typeId then
            if Lua_Tip_NeedShowRate(attrList[i].typeId) then
              local newValue = listAttr[1][j].AttrValue * (attrList[i].value * 100) / 10000
              showStr = showStr .. string.format(" {#G^  +%.2f%%(%d %%)}", newValue, listAttr[1][j].AttrValue)
            else
              local newValue = math.floor(tonumber(listAttr[1][j].AttrValue) * tonumber(attrList[i].value) / 100)
              showStr = showStr .. string.format(" {#G^  +%.f(%d %%)}", newValue, listAttr[1][j].AttrValue)
            end
          end
        end
      end
      gUI.GemAdorn.bTips:InsertString(showStr, -1)
    end
  end
end
function UI_GemAdorn_CtrlClick(msg)
  GemAdorn_CancelOperateE()
  GemAdorn_CancelOperateB()
  if GemAdorn_GetCurrMode() == GEMADORNSTATE.Beset then
    gUI.GemAdorn.bRoot:SetVisible(true)
    gUI.GemAdorn.eRoot:SetVisible(false)
  else
    gUI.GemAdorn.bRoot:SetVisible(false)
    gUI.GemAdorn.eRoot:SetVisible(true)
  end
end
function GemAdorn_bResult()
  Lua_Chat_ShowOSD("GEM_EMBED_SUCC")
  GemAdorn_CancelOperateB()
  WorldStage:playSoundByID(6)
end
function GemAdorn_CancelOperateB()
  SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, 0, OperateMode.Cancel, GEMADORNSLOT.EQUIPSLOT, NpcFunction.NPC_FUNC_GEM_EMBED)
end
function GemAdorn_CancelOperateE()
  SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, 0, OperateMode.Cancel, GEMADORNSLOT.EQUIPSLOT, NpcFunction.NPC_FUNC_GEM_MOVEOUT)
end
function UI_GemAdorn_bMainRClick(msg)
  GemAdorn_CancelOperateB()
end
function UI_GemAdorn_bSubRClick(msg)
  local bagMater, slotMater = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_EMBED, GEMADORNSLOT.MATERSLOT)
  SetNpcFunctionEquip(bagMater, slotMater, OperateMode.Remove, GEMADORNSLOT.MATERSLOT, NpcFunction.NPC_FUNC_GEM_EMBED)
end
function UI_GemAdorn_bMainDropItem(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_type = to_win:GetProperty("BoxType")
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    slot = Lua_Bag_GetRealSlot(from_win, slot)
    SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, slot, OperateMode.Add, GEMADORNSLOT.EQUIPSLOT, NpcFunction.NPC_FUNC_GEM_EMBED)
  end
end
function UI_GemAdorn_bMaterDropItem(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_type = to_win:GetProperty("BoxType")
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    slot = Lua_Bag_GetRealSlot(from_win, slot)
    SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, slot, OperateMode.Add, GEMADORNSLOT.MATERSLOT, NpcFunction.NPC_FUNC_GEM_EMBED)
  end
end
function UI_GemAdorn_bMainTip(msg)
  local bagMain, slotMain = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_EMBED, GEMADORNSLOT.EQUIPSLOT)
  local win = WindowToGoodsBox(msg:get_window())
  local tooltip = win:GetToolTipWnd(0)
  if slotMain == nil then
    return
  end
  Lua_Tip_Item(tooltip, slotMain, bagMain, nil, nil, nil)
  Lua_Tip_ShowEquipCompare(gUI.GemAdorn.bMainSlot, bagMain, slotMain)
end
function UI_GemAdorn_bMaterTip(msg)
  local bagMater, slotMater = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_EMBED, GEMADORNSLOT.MATERSLOT)
  local win = WindowToGoodsBox(msg:get_window())
  local tooltip = win:GetToolTipWnd(0)
  Lua_Tip_Item(tooltip, slotMater, bagMater, nil, nil, nil)
end
function UI_GemAdorn_bSureClick(msg)
  if not gUI.GemAdorn.bMainSlot:IsItemHasIcon(0) then
    Lua_Chat_ShowOSD("GEM_EMBED_NO_EQUIP")
    return
  end
  DoGemBeset(1)
end
function UI_GemAdorn_eItemDrop(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_type = to_win:GetProperty("BoxType")
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    slot = Lua_Bag_GetRealSlot(from_win, slot)
    SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, slot, OperateMode.Add, GEMADORNSLOT.EQUIPSLOT, NpcFunction.NPC_FUNC_GEM_MOVEOUT)
  end
end
function UI_GemAdorn_eRClick(msg)
  local bagMater, slotMater = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_MOVEOUT, GEMADORNSLOT.EQUIPSLOT)
  SetNpcFunctionEquip(bagMater, slotMater, OperateMode.Remove, GEMADORNSLOT.EQUIPSLOT, NpcFunction.NPC_FUNC_GEM_MOVEOUT)
end
function UI_GemAdorn_eMainTip(msg)
  local bagMain, slotMain = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_MOVEOUT, GEMADORNSLOT.EQUIPSLOT)
  local win = WindowToGoodsBox(msg:get_window())
  local tooltip = win:GetToolTipWnd(0)
  if slotMain == nil then
    return
  end
  Lua_Tip_Item(tooltip, slotMain, bagMain, nil, nil, nil)
  Lua_Tip_ShowEquipCompare(gUI.GemAdorn.eMainSlot, bagMain, slotMain)
end
function UI_GemAdorn_eMaterTip(msg)
  local goodsbox = msg:get_window()
  goodsbox = WindowToGoodsBox(goodsbox)
  if goodsbox:GetItemIcon(0) ~= "" then
    local tooltip = goodsbox:GetToolTipWnd(0)
    local slot = goodsbox:GetItemData(0)
    local itemid = goodsbox:GetCustomUserData(0)
    Lua_Tip_Item(tooltip, nil, nil, nil, nil, itemid, nil, nil, nil, true)
  end
end
function UI_GemAdorn_Comfim()
  SplitGem(0)
end
function GemAdorn_eResult()
  GemAdorn_CancelOperateE()
  Lua_Chat_ShowOSD("GEM_EXTRAC_SUCC")
  WorldStage:playSoundByID(7)
end
function Script_GemAdorn_OnEvent(event)
  if event == "GEM_INLAY_SHOW" then
    GemAdorn_ShowInterface()
  elseif event == "CLOSE_NPC_RELATIVE_DIALOG" then
    GemAdorn_CloseInterface()
  elseif event == "GEM_INLAY_RESULT" then
    GemAdorn_bResult()
  elseif event == "GEM_INLAY_UPDATE" then
    GemAdorn_InitData()
  elseif event == "GEM_REMOVE_RESULT" then
    GemAdorn_eResult()
  elseif event == "GEM_REMOVE_UPDATE" then
    GemAdorn_InitData()
  elseif event == "PLAYER_MONEY_CHANGED" then
    GemAdorn_UpdateeMoney(arg1, arg2, event)
  end
end
function Script_GemAdorn_OnHide()
  GemAdorn_CancelOperateE()
  GemAdorn_CancelOperateB()
end
function Script_GemAdorn_OnLoad()
  gUI.GemAdorn.Root = WindowSys_Instance:GetWindow("GemAdorn")
  gUI.GemAdorn.AdornCtrl = WindowToTableCtrl(WindowSys_Instance:GetWindow("GemAdorn.GemAdorn_bg.Image_bg.Gem_tctl"))
  gUI.GemAdorn.bRoot = WindowSys_Instance:GetWindow("GemAdorn.GemAdorn_bg.Image_bg.Inlay_bg")
  gUI.GemAdorn.bMainSlot = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemAdorn.GemAdorn_bg.Image_bg.Inlay_bg.Gem_gbox"))
  gUI.GemAdorn.bMaterSlot = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemAdorn.GemAdorn_bg.Image_bg.Inlay_bg.GemMaterial_gbox"))
  gUI.GemAdorn.bTips = WindowToListBox(WindowSys_Instance:GetWindow("GemAdorn.GemAdorn_bg.Image_bg.Inlay_bg.Tooltip_lbox"))
  gUI.GemAdorn.eRoot = WindowSys_Instance:GetWindow("GemAdorn.GemAdorn_bg.Image_bg.Dis_bg")
  gUI.GemAdorn.eMainSlot = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemAdorn.GemAdorn_bg.Image_bg.Dis_bg.Dis_gbox"))
  gUI.GemAdorn.eMaterSlot = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemAdorn.GemAdorn_bg.Image_bg.Dis_bg.DisMaterial_gbox"))
  gUI.GemAdorn.eMonLab = WindowToLabel(WindowSys_Instance:GetWindow("GemAdorn.GemAdorn_bg.Image_bg.Dis_bg.Money_bg.Money_dlab"))
end
