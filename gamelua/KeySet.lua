if gUI and not gUI.KeySet then
  gUI.KeySet = {}
end
local _KeySet_HotKeyUINumber = 79
local _KeySet_CurHotKeyButton
local _KeySet_Vehicle_Begin = 72
local _KeySet_Vehicle_End = 76
local _KeySet_HotKeyUIInfo = {
  [0] = {
    name = "BaseSet",
    size = 9,
    msg = ""
  },
  [1] = {
    name = "UISet",
    size = 26,
    msg = ""
  },
  [2] = {
    name = "QuickBar1Set",
    size = 36,
    msg = "gb_itemRB"
  },
  [3] = {
    name = "QuickBar2Set",
    size = 46,
    msg = "gb_itemRB"
  },
  [4] = {
    name = "QuickBar3Set",
    size = 56,
    msg = "gb_itemRB"
  },
  [5] = {
    name = "OtherSet",
    size = 70,
    msg = "gb_itemRB"
  }
}
local _KeySet_ExKeyVaule = {
  ALT = 1,
  CTRL = 2,
  SHIFT = 4
}
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["1"] = 2
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["2"] = 3
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["3"] = 4
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["4"] = 5
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["5"] = 6
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["6"] = 7
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["7"] = 8
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["8"] = 9
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["9"] = 10
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["0"] = 11
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.Q = 16
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.W = 17
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.E = 18
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.R = 19
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.T = 20
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.Y = 21
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.U = 22
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.I = 23
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.O = 24
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.P = 25
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.A = 30
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.S = 31
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.D = 32
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.F = 33
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.G = 34
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.H = 35
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.J = 36
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.K = 37
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.L = 38
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.Z = 44
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.X = 45
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.C = 46
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.V = 47
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.B = 48
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.N = 49
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}.M = 50
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["["] = 26
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["]"] = 27
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["\\"] = 43
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}[";"] = 39
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["\""] = 40
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["~"] = 41
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}[","] = 51
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["。"] = 52
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["/"] = 53
{
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["-"] = 12
local {
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}["="], _KeySet_StrToOISKey = 13, {
  ["F1"] = 59,
  ["F2"] = 60,
  ["F3"] = 61,
  ["F4"] = 62,
  ["F5"] = 63,
  ["F6"] = 64,
  ["F7"] = 65,
  ["F8"] = 66,
  ["F9"] = 67,
  ["F10"] = 68,
  ["F11"] = 87,
  ["F12"] = 88,
  ["PAD0"] = 82,
  ["PAD1"] = 79,
  ["PAD2"] = 80,
  ["PAD3"] = 81,
  ["PAD4"] = 75,
  ["PAD5"] = 76,
  ["PAD6"] = 77,
  ["PAD7"] = 71,
  ["PAD8"] = 72,
  ["PAD9"] = 73,
  ["SPACE"] = 57,
  ["ESC"] = 1,
  ["ENTER"] = 28,
  ["TAB"] = 15,
  ["←"] = 203,
  ["→"] = 205,
  ["↑"] = 200,
  ["↓"] = 208,
  ["ALT"] = 56,
  ["CTRL"] = 29,
  ["SHIFT"] = 42,
  ["SYSRQ"] = 183,
  ["PADENTER"] = 156
}
function _KeySet_GetKeyDateByPage()
  local tablectrl = WindowToTableCtrl(gUI.KeySet.Key_tctl)
  local index = 0
  local indexbegin = 1
  if tablectrl:GetSelectItem() == 0 then
    index = 0
  elseif tablectrl:GetSelectItem() == 1 then
    index = 1
  elseif tablectrl:GetSelectItem() == 2 then
    index = 4
  else
    index = 5
  end
  if index == 0 then
    indexbegin = 1
  elseif index == 4 then
    indexbegin = _KeySet_HotKeyUIInfo[1].size + 1
  else
    indexbegin = _KeySet_HotKeyUIInfo[index - 1].size + 1
  end
  for i = indexbegin, _KeySet_HotKeyUIInfo[index].size do
    SetHotKeyDataToUI(i)
  end
  for i = _KeySet_HotKeyUINumber + indexbegin, _KeySet_HotKeyUINumber + _KeySet_HotKeyUIInfo[index].size do
    SetHotKeyDataToUI(i)
  end
end
function _KeySet_UpdateKeySetPage()
  local tablectrl = WindowToTableCtrl(gUI.KeySet.Key_tctl)
  gUI.KeySet.Base:SetProperty("Visible", "false")
  gUI.KeySet.UI:SetProperty("Visible", "false")
  gUI.KeySet.Other:SetProperty("Visible", "false")
  gUI.KeySet.Short:SetProperty("Visible", "false")
  if tablectrl:GetSelectItem() == 0 then
    gUI.KeySet.Base:SetProperty("Visible", "true")
  elseif tablectrl:GetSelectItem() == 1 then
    gUI.KeySet.UI:SetProperty("Visible", "true")
  elseif tablectrl:GetSelectItem() == 2 then
    gUI.KeySet.Short:SetProperty("Visible", "true")
  else
    gUI.KeySet.Other:SetProperty("Visible", "true")
  end
  _KeySet_GetKeyDateByPage()
end
function _KeySet_HotKeyToShort(strHotKey)
  local resHotKey = string.upper(strHotKey)
  if string.find(resHotKey, "+") then
    resHotKey = string.gsub(resHotKey, "ALT", "A")
    resHotKey = string.gsub(resHotKey, "SHIFT", "S")
    resHotKey = string.gsub(resHotKey, "CTRL", "C")
  end
  resHotKey = string.sub(resHotKey, 1, 6)
  return resHotKey
end
function _KeySet_ParseCompoundHotKey(strHotKey)
  local hotKey = ""
  local exKey = 0
  strHotKey = string.upper(strHotKey)
  local index = 0
  while index do
    local i, j = string.find(strHotKey, "([^+]*)", index)
    if i <= j then
      local tempStr = string.sub(strHotKey, i, j)
      if _KeySet_ExKeyVaule[tempStr] then
        exKey = exKey + _KeySet_ExKeyVaule[tempStr]
      else
        hotKey = tempStr
      end
      index = j + 2
    else
      index = nil
    end
  end
  if 0 >= string.len(hotKey) and _KeySet_StrToOISKey[strHotKey] then
    hotKey = strHotKey
    exKey = 0
  end
  return _KeySet_StrToOISKey[hotKey], exKey
end
function _KeySet_GetTypeAndTypeIndex(nIndex)
  if nIndex > _KeySet_HotKeyUINumber then
    nIndex = nIndex - _KeySet_HotKeyUINumber
  end
  for i = 0, 5 do
    if nIndex <= _KeySet_HotKeyUIInfo[i].size then
      if i > 0 then
        return _KeySet_HotKeyUIInfo[i].name, nIndex - _KeySet_HotKeyUIInfo[i - 1].size, _KeySet_HotKeyUIInfo[i].msg
      else
        return _KeySet_HotKeyUIInfo[0].name, nIndex, _KeySet_HotKeyUIInfo[0].msg
      end
    end
  end
end
function _KeySet_GetCurHotKeyButtonIndex()
  if _KeySet_CurHotKeyButton ~= nil then
    local wndName = _KeySet_CurHotKeyButton:GetParent():GetProperty("WindowName")
    local beginPos, endPos = string.find(wndName, "%d+$")
    local typeName = string.sub(wndName, 0, beginPos - 1)
    local index = tonumber(string.sub(wndName, beginPos, endPos))
    wndName = _KeySet_CurHotKeyButton:GetProperty("WindowName")
    beginPos, endPos = string.find(string.gsub(wndName, "_btn", ""), "%d+$")
    local keyType = tonumber(string.sub(wndName, beginPos, endPos))
    local uiKeyIndex = index
    for i = 0, 5 do
      if _KeySet_HotKeyUIInfo[i].name == typeName then
        if i > 0 then
          uiKeyIndex = index + _KeySet_HotKeyUIInfo[i - 1].size
        end
        break
      end
    end
    local keyIndex = uiKeyIndex
    if keyType == 2 then
      keyIndex = keyIndex + _KeySet_HotKeyUINumber
    end
    return keyIndex
  end
end
function _KeySet_OnConfirm(compoundKey, nCrashIndex)
  if _KeySet_CurHotKeyButton ~= nil then
    local keyIndex = _KeySet_GetCurHotKeyButtonIndex()
    SetHotKeyMapping(keyIndex, nCrashIndex, compoundKey)
    _KeySet_CurHotKeyButton = WindowToButton(_KeySet_CurHotKeyButton)
    _KeySet_CurHotKeyButton:SetStatus("normal")
    if nCrashIndex > 0 then
      if IsBindUIWindow(nCrashIndex) == 0 then
        if nCrashIndex > _KeySet_HotKeyUINumber then
          nCrashIndex = nCrashIndex - _KeySet_HotKeyUINumber
        end
        _, hotKeyName = GetHotKeyDefaultData(nCrashIndex, 1)
        gUI.KeySet.TiShi:SetProperty("Text", "{#G^按键设置成功！}" .. "{#R^[" .. hotKeyName .. "]未设置快捷键}")
      end
    else
      gUI.KeySet.TiShi:SetProperty("Text", "{#Y^按键设置成功！}")
    end
  end
  gUI.KeySet.Unbind:SetProperty("Enable", "false")
  _KeySet_GetKeyDateByPage()
end
function Lua_KeySet_Show()
  InitHotKeyMapping()
  _KeySet_GetKeyDateByPage()
  gUI.KeySet.Root:SetProperty("Visible", "true")
end
function HotKey_SetToolTip(wnd, text)
  local tooltip = wnd:GetToolTipWnd(0)
  tooltip = WindowToTooltip(tooltip)
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(text, "FF00FF00", "", 0, 0)
  Lua_Tip_End(tooltip)
end
function _OnKeySet_OnPressed(compoundKey, nCrashIndex)
  if nCrashIndex == -100 then
    gUI.KeySet.TiShi:SetProperty("Text", "{#R^不能将按键设置为快捷键，设置失败！}")
    return
  end
  _KeySet_OnConfirm(compoundKey, nCrashIndex)
end
function _OnKeySet_SetToUI(nIndex, strHotKey)
  local keyType = 1
  if nIndex > _KeySet_HotKeyUINumber then
    nIndex = nIndex - _KeySet_HotKeyUINumber
    keyType = 2
  end
  local typeName, index = _KeySet_GetTypeAndTypeIndex(nIndex)
  local tablectrl = WindowToTableCtrl(gUI.KeySet.Key_tctl)
  local _root
  if tablectrl:GetSelectItem() == 0 then
    _root = "KeySet.Beijing_bg.Center_bg.Key_bg.Base_cnt."
  elseif tablectrl:GetSelectItem() == 1 then
    _root = "KeySet.Beijing_bg.Center_bg.Key_bg.UI_cnt."
  elseif tablectrl:GetSelectItem() == 2 then
    _root = "KeySet.Beijing_bg.Center_bg.Key_bg.Short_cnt."
  else
    _root = "KeySet.Beijing_bg.Center_bg.Key_bg.Other_cnt."
  end
  local wndName = _root .. tostring(typeName) .. tostring(index) .. ".Key" .. tostring(keyType) .. "_btn"
  local wnd = WindowSys_Instance:GetWindow(wndName)
  if wndName == "KeySet.Beijing_bg.Center_bg.Key_bg.UI_cnt.UISet10.Key1_btn" then
    wnd:SetProperty("Enable", "false")
  end
  if wndName == "KeySet.Beijing_bg.Center_bg.Key_bg.UI_cnt.UISet10.Key2_btn" then
    wnd:SetProperty("Enable", "false")
  end
  if wndName == "KeySet.Beijing_bg.Center_bg.Key_bg.UI_cnt.UISet14.Key1_btn" then
    wnd:SetProperty("Enable", "false")
  end
  if wndName == "KeySet.Beijing_bg.Center_bg.Key_bg.UI_cnt.UISet14.Key2_btn" then
    wnd:SetProperty("Enable", "false")
  end
  if wndName == "KeySet.Beijing_bg.Center_bg.Key_bg.UI_cnt.UISet8.Key1_btn" then
    wnd:SetProperty("Enable", "false")
  end
  if wndName == "KeySet.Beijing_bg.Center_bg.Key_bg.UI_cnt.UISet8.Key2_btn" then
    wnd:SetProperty("Enable", "false")
  end
  wnd:SetProperty("Text", strHotKey)
end
function _OnKeySet_SetDefaultHotKey(index, strHotKeyName, strMsgWnd, strMsgEvent, strDefaultKey, strDefaultKey2, wparam, lparam)
  if index == 23 or index == 19 or index == 17 then
    return
  end
  local wnd = WindowSys_Instance:GetWindow(strMsgWnd)
  local hotKey, flag = _KeySet_ParseCompoundHotKey(strDefaultKey)
  local hotKey2, flag2 = _KeySet_ParseCompoundHotKey(strDefaultKey2)
  if hotKey and hotKey ~= 0 then
    local compoundKey = hotKey * 16 + flag
    UIInterface:bindUIWindow(compoundKey, wnd, strMsgEvent, wparam, lparam, index)
  end
  if hotKey2 and hotKey2 ~= 0 then
    local compoundKey2 = hotKey2 * 16 + flag2
    UIInterface:bindUIWindow(compoundKey2, wnd, strMsgEvent, wparam, lparam, index + _KeySet_HotKeyUINumber)
  end
  if index > _KeySet_HotKeyUIInfo[1].size and index <= _KeySet_HotKeyUIInfo[4].size then
    local goodsBox = WindowToGoodsBox(wnd)
    local shortHotKey
    if strDefaultKey ~= "" then
      shortHotKey = _KeySet_HotKeyToShort(tostring(strDefaultKey))
    else
      shortHotKey = _KeySet_HotKeyToShort(tostring(strDefaultKey2))
    end
    goodsBox:SetItemSuperscript(lparam, tostring(shortHotKey))
  elseif index > _KeySet_HotKeyUIInfo[0].size and index <= _KeySet_HotKeyUIInfo[1].size then
    if wnd then
      local bar_index = wnd:GetCustomUserData(0)
      if bar_index == 100 then
      end
      if strDefaultKey ~= "" then
        gUI.KeySet.ActionToolBar[bar_index] = strDefaultKey
      elseif strDefaultKey2 ~= "" then
        gUI.KeySet.ActionToolBar[bar_index] = strDefaultKey2
      else
        gUI.KeySet.ActionToolBar[bar_index] = ""
      end
    end
  elseif index >= _KeySet_Vehicle_Begin and index <= _KeySet_Vehicle_End then
    local goodsBox = WindowToGoodsBox(wnd)
    goodsBox:SetItemSuperscript(lparam, tostring(strDefaultKey))
  end
end
function _OnKeySet_OnInit()
  SetHotKey(2)
end
function UI_KeySet_Recover_Default(msg)
  SetHotKey(1)
  InitHotKeyMapping()
  _KeySet_GetKeyDateByPage()
  if _KeySet_CurHotKeyButton ~= nil then
    _KeySet_CurHotKeyButton = WindowToButton(_KeySet_CurHotKeyButton)
    _KeySet_CurHotKeyButton:SetStatus("normal")
    gUI.KeySet.Unbind:SetProperty("Enable", "false")
  end
end
function UI_KeySet_Close(msg)
  local tablectrl = WindowToTableCtrl(gUI.KeySet.Key_tctl)
  tablectrl:SetSelectedState(0)
  gUI.KeySet.UI:SetProperty("Visible", "false")
  gUI.KeySet.Other:SetProperty("Visible", "false")
  gUI.KeySet.Short:SetProperty("Visible", "false")
  gUI.KeySet.Base:SetProperty("Visible", "true")
  gUI.KeySet.Root:SetProperty("Visible", "false")
  gUI.KeySet.TiShi:SetProperty("Text", "")
  SetOnHotKeyDef(false)
  if _KeySet_CurHotKeyButton ~= nil then
    _KeySet_CurHotKeyButton = WindowToButton(_KeySet_CurHotKeyButton)
    _KeySet_CurHotKeyButton:SetStatus("normal")
    gUI.KeySet.Unbind:SetProperty("Enable", "false")
  end
end
function UI_KeySet_Btn(msg)
  if _KeySet_CurHotKeyButton then
    _KeySet_CurHotKeyButton = WindowToButton(_KeySet_CurHotKeyButton)
    _KeySet_CurHotKeyButton:SetStatus("normal")
  end
  _KeySet_CurHotKeyButton = msg:get_window()
  _KeySet_CurHotKeyButton = WindowToButton(_KeySet_CurHotKeyButton)
  _KeySet_CurHotKeyButton:SetStatus("selected")
  local wndName = _KeySet_CurHotKeyButton:GetParent():GetChildByName("Name_slab"):GetProperty("Text")
  gUI.KeySet.TiShi:SetProperty("Text", "{#G^按下键盘按键，可以给[" .. wndName .. "]设置快捷键}")
  SetOnHotKeyDef(true)
  gUI.KeySet.Unbind:SetProperty("Enable", "true")
end
function UI_KeySet_TabctrlClick(msg)
  SetOnHotKeyDef(false)
  _KeySet_UpdateKeySetPage()
  if _KeySet_CurHotKeyButton ~= nil then
    _KeySet_CurHotKeyButton = WindowToButton(_KeySet_CurHotKeyButton)
    _KeySet_CurHotKeyButton:SetStatus("normal")
    gUI.KeySet.Unbind:SetProperty("Enable", "false")
  end
end
function UI_KeySet_UseSetting(msg)
  SetHotKey(3)
  InitHotKeyMapping()
  _KeySet_GetKeyDateByPage()
  gUI.KeySet.TiShi:SetProperty("Text", "")
  if _KeySet_CurHotKeyButton ~= nil then
    _KeySet_CurHotKeyButton = WindowToButton(_KeySet_CurHotKeyButton)
    _KeySet_CurHotKeyButton:SetStatus("normal")
    gUI.KeySet.Unbind:SetProperty("Enable", "false")
  end
end
function UI_KeySet_OK(msg)
  UI_KeySet_UseSetting()
  UI_KeySet_Close()
end
function UI_KeySet_Unbind(msg)
  if _KeySet_CurHotKeyButton ~= nil then
    local keyIndex = _KeySet_GetCurHotKeyButtonIndex()
    SetHotKeyMapping(keyIndex, -1, -1)
    if IsBindUIWindow(keyIndex) == 0 then
      local wndName = _KeySet_CurHotKeyButton:GetParent():GetChildByName("Name_slab"):GetProperty("Text")
      gUI.KeySet.TiShi:SetProperty("Text", "{#R^[" .. wndName .. "]未设置快捷键}")
    end
    _KeySet_CurHotKeyButton = WindowToButton(_KeySet_CurHotKeyButton)
    _KeySet_CurHotKeyButton:SetStatus("normal")
    gUI.KeySet.Unbind:SetProperty("Enable", "false")
  end
  SetOnHotKeyDef(false)
  _KeySet_GetKeyDateByPage()
end
function Script_KeySet_OnLoad()
  gUI.KeySet.ActionToolBar = {}
  gUI.KeySet.Root = WindowSys_Instance:GetWindow("KeySet")
  gUI.KeySet.TiShi = WindowSys_Instance:GetWindow("KeySet.Beijing_bg.Bottom_bg.TiShi_bg.TiShi_dlab")
  gUI.KeySet.Base = WindowSys_Instance:GetWindow("KeySet.Beijing_bg.Center_bg.Key_bg.Base_cnt")
  gUI.KeySet.UI = WindowSys_Instance:GetWindow("KeySet.Beijing_bg.Center_bg.Key_bg.UI_cnt")
  gUI.KeySet.Short = WindowSys_Instance:GetWindow("KeySet.Beijing_bg.Center_bg.Key_bg.Short_cnt")
  gUI.KeySet.Other = WindowSys_Instance:GetWindow("KeySet.Beijing_bg.Center_bg.Key_bg.Other_cnt")
  gUI.KeySet.Key_tctl = WindowSys_Instance:GetWindow("KeySet.Beijing_bg.Center_bg.Key_tctl")
  gUI.KeySet.Unbind = WindowSys_Instance:GetWindow("KeySet.Beijing_bg.Bottom_bg.JieChu_btn")
  local tablectrl = WindowToTableCtrl(gUI.KeySet.Key_tctl)
  tablectrl:SetSelectedState(0)
  gUI.KeySet.Base:SetProperty("Visible", "true")
  gUI.KeySet.Other:SetProperty("Enable", "false")
  gUI.KeySet.UI:SetProperty("Enable", "false")
  gUI.KeySet.Short:SetProperty("Enable", "false")
  gUI.KeySet.Unbind:SetProperty("Enable", "false")
  SetHotKey(1)
  SetOnHotKeyDef(false)
end
function Script_KeySet_OnEvent(event)
  if event == "HOT_KEY_SET_DEFAULT" then
    _OnKeySet_SetDefaultHotKey(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
  elseif event == "HOT_KEY_SET_TO_UI" then
    _OnKeySet_SetToUI(arg1, arg2)
  elseif event == "HOT_KEY_ON_DEFINE" then
    _OnKeySet_OnPressed(arg1, arg2)
  elseif event == "HOT_KEY_INIT" then
    _OnKeySet_OnInit()
  end
end
 
