g_Preview_AllAvatar = {
  [0] = "Menpai0",
  [1] = "Menpai1",
  [2] = "Menpai2",
  [3] = "Menpai3",
  [4] = "Menpai4"
}
local _Preview_Root, _Preview_MeipaiIntroduce, _Preview_MeipaiTrait, _Preview_FightPic, _Preview_Animation, _Preview_NextStep, _Preview_LastStep, _Preview_SelectPic
local _Preview_SelectedMenpai = -1
local _Preview_OldSelectedMenpai = -1
local _Preview_SelectedSex = -1
local _Preview_OldSelectedSex = -1
local _Preview_IsCanChange = false
function _ProPreview_HideWnd()
  _Preview_MeipaiIntroduce:SetVisible(false)
  _Preview_MeipaiTrait:SetVisible(false)
  _Preview_FightPic:SetVisible(false)
  _Preview_Animation:SetVisible(false)
  _Preview_NextStep:SetVisible(false)
  _Preview_LastStep1:SetVisible(false)
end
function _ProPreview_SettingSelectedWnd(sexid)
  _Preview_MeipaiIntroduce:SetVisible(true)
  _Preview_MeipaiTrait:SetVisible(true)
  _Preview_FightPic:SetVisible(true)
  local bio = gUI_PROFESSION_INTRODUCE[_Preview_SelectedMenpai].biography
  local MenPai = _Preview_MeipaiIntroduce:GetChildByName("Picture1_pic")
  MenPai:SetProperty("BackImage", bio.menpaiPic)
  local Introduce = _Preview_MeipaiIntroduce:GetChildByName("Label1_dlab")
  Introduce:SetProperty("Text", bio.proviewText)
  local MainAttribute = _Preview_MeipaiTrait:GetGrandChild("Picture2_bg.Label1_dlab")
  MainAttribute:SetProperty("Text", bio.mainAttribute)
  local FightTendency = _Preview_MeipaiTrait:GetGrandChild("Picture2_bg.Label2_dlab")
  FightTendency:SetProperty("Text", bio.fightTendency)
  local TeamTendency = _Preview_MeipaiTrait:GetGrandChild("Picture2_bg.Label3_dlab")
  TeamTendency:SetProperty("Text", bio.teamTendency)
  local FightPic = _Preview_FightPic:GetChildByName("Picture2_pic")
  FightPic:SetProperty("BackImage", bio.fightPic)
  local OperDiff = WindowToProgressBar(_Preview_MeipaiTrait:GetGrandChild("Dif_bg.Dif_Par"))
  OperDiff:SetProperty("Progress", tostring(bio.operdiff * 2 / 10))
end
function _ProPreview_SetSelectBtnVisible(bValue)
  _Preview_SelectPic:SetVisible(bValue)
  _Preview_Animation:SetVisible(bValue)
  _Preview_NextStep:SetVisible(bValue)
  _Preview_LastStep:SetVisible(bValue)
  _Preview_LastStep1:SetVisible(not bValue)
  _Preview_IsCanChange = bValue
end
function _ProPreview_SetSelectBtnEnable(index, bValue, sexId)
  local btn
  if sexId == 0 then
    btn = _Preview_SelectPic:GetChildByName(string.format("Pro%df_btn", index))
  else
    btn = _Preview_SelectPic:GetChildByName(string.format("Pro%dm_btn", index))
  end
  if btn then
    btn:SetProperty("Enable", tostring(bValue))
  end
end
function _ProPreview_SetAllSelectbtnEnable()
  _Preview_SelectPic:SetVisible(true)
  _Preview_LastStep1:SetVisible(true)
  for i = 0, 4 do
    _ProPreview_SetSelectBtnEnable(i, true, 0)
    _ProPreview_SetSelectBtnEnable(i, true, 1)
  end
end
function _ProPreview_GetAvatarName(index, sexid)
  local guid = index * 10 + sexid
  local name = "PreviewCharModel" .. guid
  local show_box = UIInterface:getShowbox(g_Preview_AllAvatar[index])
  show_box:selCurrentAvatar(name)
  return guid, name, show_box
end
function _ProPreview_InitSetting()
  for index, value in pairs(g_Preview_AllAvatar) do
    local show_box = UIInterface:getShowbox(value)
    local avatar = gUI_PROFESSION_INTRODUCE[index].avatar
    local animation = gUI_PROFESSION_INTRODUCE[index].animation
    local guid1, name1 = _ProPreview_GetAvatarName(index, 1)
    if not show_box:isAvatarExist(name1) then
      show_box:createPreviewAvatar(guid1, index, 1, name1, false)
    end
    show_box:setAvatarAnimation(animation.default, true)
    show_box:setAvatarPosition(avatar.mx, avatar.my, avatar.mz)
    show_box:setAvatarOrientation(avatar.mrotation)
    show_box:setAvatarScale(avatar.mscale)
    local guid0, name0 = _ProPreview_GetAvatarName(index, 0)
    if not show_box:isAvatarExist(name0) then
      show_box:createPreviewAvatar(guid0, index, 0, name0, false)
    end
    show_box:setAvatarAnimation(animation.default, true)
    show_box:setAvatarPosition(avatar.fx, avatar.fy, avatar.fz)
    show_box:setAvatarOrientation(avatar.frotation)
    show_box:setAvatarScale(avatar.fscale)
    if index == 0 then
      local previewShare = gUI_PROFESSION_INTRODUCE.previewShare
      show_box:setCameraPos(previewShare.cx, previewShare.cy, previewShare.cz)
      show_box:setCameraDir(previewShare.dx, previewShare.dy, previewShare.dz)
    end
  end
end
function _ProPreview_CheckAnimationEnd(show_box, key, index, sexid)
  local avatar = gUI_PROFESSION_INTRODUCE[index].avatar
  local animation = gUI_PROFESSION_INTRODUCE[index].animation
  if key == "_out1" then
    show_box:setAvatarPosition(avatar.showx, avatar.showy, avatar.showz)
    show_box:setAvatarOrientation(avatar.srotation)
    show_box:setAvatarScale(avatar.sscale)
    show_box:setAvatarAnimation(animation._out2, false)
  elseif key == "_out2" then
    show_box:setAvatarAnimation(animation.show, false)
  elseif key == "show" then
    show_box:setAvatarAnimation(animation.standard, true)
    _ProPreview_SetSelectBtnVisible(true)
    _ProPreview_SetSelectBtnEnable(index, false, sexid)
  elseif key == "_in1" then
    if sexid == 1 then
      show_box:setAvatarPosition(avatar.mx, avatar.my, avatar.mz)
      show_box:setAvatarOrientation(avatar.mrotation)
      show_box:setAvatarScale(avatar.mscale)
    else
      show_box:setAvatarPosition(avatar.fx, avatar.fy, avatar.fz)
      show_box:setAvatarOrientation(avatar.frotation)
      show_box:setAvatarScale(avatar.fscale)
    end
    show_box:setAvatarAnimation(animation._in2, false)
  elseif key == "_in2" then
    show_box:setAvatarAnimation(animation.default, true)
    _ProPreview_SetSelectBtnEnable(index, true, sexid)
  end
end
function _ProPreview_UpdataAnimation()
  if _Preview_SelectedMenpai ~= -1 then
    _, _, show_box = _ProPreview_GetAvatarName(_Preview_SelectedMenpai, _Preview_SelectedSex)
    for key, value in pairs(gUI_PROFESSION_INTRODUCE[_Preview_SelectedMenpai].animation) do
      if (key == "_out1" or key == "_out2" or key == "show") and show_box:getAvatarAnimationIsEnded(value) then
        _ProPreview_CheckAnimationEnd(show_box, key, _Preview_SelectedMenpai, _Preview_SelectedSex)
      end
    end
    if _Preview_OldSelectedMenpai ~= -1 and _Preview_OldSelectedSex ~= -1 then
      _, _, show_box = _ProPreview_GetAvatarName(_Preview_OldSelectedMenpai, _Preview_OldSelectedSex)
      local value1 = gUI_PROFESSION_INTRODUCE[_Preview_OldSelectedMenpai].animation._in1
      if show_box:getAvatarAnimationIsEnded(value1) then
        _ProPreview_CheckAnimationEnd(show_box, "_in1", _Preview_OldSelectedMenpai, _Preview_OldSelectedSex)
        return
      end
      local value2 = gUI_PROFESSION_INTRODUCE[_Preview_OldSelectedMenpai].animation._in2
      if show_box:getAvatarAnimationIsEnded(value2) then
        _ProPreview_CheckAnimationEnd(show_box, "_in2", _Preview_OldSelectedMenpai, _Preview_OldSelectedSex)
      end
    end
  end
end
function _ProPreview_SelectPro(index, sexid)
  if _Preview_SelectedMenpai == index and _Preview_SelectedSex == sexid then
    return
  end
  if not _Preview_IsCanChange then
    return
  end
  if _Preview_SelectedMenpai ~= -1 then
    local _, _, show_box = _ProPreview_GetAvatarName(_Preview_SelectedMenpai, _Preview_SelectedSex)
    show_box:setAvatarAnimation(gUI_PROFESSION_INTRODUCE[_Preview_SelectedMenpai].animation._in1, false)
  end
  _Preview_OldSelectedSex = _Preview_SelectedSex
  _Preview_SelectedSex = sexid
  local guid, name, show_box = _ProPreview_GetAvatarName(index, _Preview_SelectedSex)
  show_box:setAvatarAnimation(gUI_PROFESSION_INTRODUCE[index].animation._out1, false)
  _Preview_OldSelectedMenpai = _Preview_SelectedMenpai
  _Preview_SelectedMenpai = index
  _ProPreview_SettingSelectedWnd(sexid)
  _ProPreview_SetSelectBtnVisible(false)
  _Preview_LastStep1:SetVisible(false)
end
function Lua_ProPreview_AllShow()
  WindowSys_Instance:GetWindow("CharList"):SetVisible(false)
  WindowSys_Instance:GetWindow("CharConfigure"):SetVisible(false)
  _Preview_Root:SetVisible(true)
  _ProPreview_InitSetting()
  _ProPreview_HideWnd()
  _ProPreview_SetAllSelectbtnEnable()
  _Preview_SelectedMenpai = -1
  _Preview_OldSelectedMenpai = -1
  _Preview_SelectedSex = -1
  _Preview_OldSelectedSex = -1
  _Preview_IsCanChange = true
  AddTimerEvent(2, "PreviewEvent", _ProPreview_UpdataAnimation)
end
function Lua_ProPreview_GetSelectedProfession()
  return _Preview_SelectedMenpai
end
function _OnProPreview_SelectPro(index, sexid)
  _ProPreview_SelectPro(index, sexid)
end
function UI_ProPreview_Select(msg)
  local button = msg:get_window()
  local index = string.match(button:GetProperty("WindowName"), "%d")
  local sexid = button:GetCustomUserData(0)
  _ProPreview_SelectPro(tonumber(index), sexid)
end
function UI_ProPreview_ShowAnimation()
  if _Preview_SelectedMenpai ~= -1 then
    local _, _, show_box = _ProPreview_GetAvatarName(_Preview_SelectedMenpai, _Preview_SelectedSex)
    show_box:setAvatarAnimation(gUI_PROFESSION_INTRODUCE[_Preview_SelectedMenpai].animation.show, false)
    _ProPreview_SetSelectBtnVisible(false)
  end
end
function UI_ProPreview_BackToLogin(msg)
  _Preview_Root:SetVisible(false)
  DelTimerEvent(2, "PreviewEvent")
  for index, value in pairs(g_Preview_AllAvatar) do
    local show_box = UIInterface:getShowbox(value)
    show_box:setCurrentAvatar("")
  end
  Lua_CharList_Return()
end
function UI_ProPreview_EnterIntoConfigure()
  _Preview_Root:SetVisible(false)
  DelTimerEvent(2, "PreviewEvent")
  for index, value in pairs(g_Preview_AllAvatar) do
    local show_box = UIInterface:getShowbox(value)
    show_box:setCurrentAvatar("")
  end
  Lua_CharConfigure_Show(_Preview_SelectedSex)
end
function Script_ProPreview_OnEvent(event)
  if event == "PROPREVIEW_SELECTEDCHAR" then
    _OnProPreview_SelectPro(arg1, arg2)
  end
end
function Script_ProPreview_OnLoad()
  _Preview_Root = WindowSys_Instance:GetWindow("AllStars")
  _Preview_MeipaiIntroduce = WindowSys_Instance:GetWindow("AllStars.Picture1_bg")
  _Preview_MeipaiTrait = WindowSys_Instance:GetWindow("AllStars.Picture4_bg")
  _Preview_FightPic = WindowSys_Instance:GetWindow("AllStars.Picture3_bg")
  _Preview_Animation = WindowSys_Instance:GetWindow("AllStars.Picture5_bg")
  _Preview_NextStep = WindowSys_Instance:GetWindow("AllStars.Picture5_bg.Next_btn")
  _Preview_LastStep = WindowSys_Instance:GetWindow("AllStars.Picture5_bg.Last_btn")
  _Preview_LastStep1 = WindowSys_Instance:GetWindow("AllStars.Btn_bg.Back_btn")
  _Preview_SelectPic = WindowSys_Instance:GetWindow("AllStars.Pic_pic")
end
