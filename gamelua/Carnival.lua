if gUI and not gUI.Carnival then
  gUI.Carnival = {}
end
local _Carnival_bShowDesc = false
local _Carnival_StateName = {
  [0] = "",
  [1] = "UiBtn001:Image_close",
  [2] = "UiBtn001:Image_proceed",
  [3] = "UiBtn001:Image_open",
  [4] = "UiBtn001:Image_finish",
  [5] = "UiBtn001:Image_join",
  [6] = "UiBtn001:Image_fail"
}
local _Carnival_TreasureStateName = {
  [0] = "UiBtn001:Image_unswap",
  [1] = "UiBtn001:Image_exchange",
  [2] = "UiBtn001:Image_swap"
}
local _Carnival_ActiveList = {
  [0] = 0,
  [1] = 0,
  [2] = 0,
  [3] = 0
}
local _Carnival_List = {}
local _Carnival_StrActive = "需要%s点今日活跃值"
local _Carnival_Count = 0
local _Carnival_TrackCount = 0
local _Carnival_AllTrack = 5
local _Carnival_TrackHintCount = 4
local _Carnival_TrackNpcLink, _Carnival_ActiveRecommendNpcLink
local _Carnival_RewardSelected = 0
local _Carnival_BarPos
function _Carnival_UpdateTrack()
  gUI.Carnival.Track_gb:ResetAllGoods(true)
  _Carnival_TrackHintCount = 4
  CarnivalUpdateTrack()
end
function _Carnival_UpdateTodayList(rewardType)
  local scrollbar = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Left_bg.TaskList_cnt.vertSB_")
  if scrollbar then
    scrollbar = WindowToScrollBar(scrollbar)
    _Carnival_BarPos = scrollbar:GetCurrentPos()
  end
  gUI.Carnival.Container:DeleteAll()
  local list = GetTodayCarnivalList(rewardType)
  if list[0] ~= nil then
    for i = table.maxn(list), 0, -1 do
      local bShow = false
      if gUI.Carnival.JoinCbtn:IsChecked() then
        if list[i].CanTake then
          bShow = true
        end
      else
        bShow = true
      end
      if bShow then
        local newItem = gUI.Carnival.Container:Insert(-1, gUI.Carnival.CntChildTemp)
        newItem:SetProperty("Visible", "true")
        newItem:SetCustomUserData(0, list[i].Id)
        newItem:SetCustomUserData(1, list[i].Schedule)
        newItem:GetChildByName("Name_dlab"):SetProperty("Text", list[i].Title)
        if 0 < list[i].Recommend then
          newItem:GetChildByName("Recommend_pbar"):SetProperty("Progress", tostring(list[i].Recommend / 100))
        end
      end
    end
  end
  local res = _Carnival_SetBtnSelected(gUI.Carnival.SelectedCarnival.id, gUI.Carnival.SelectedCarnival.Schedule)
  if not res then
    local wndChild = gUI.Carnival.Container:GetChild(0)
    gUI.Carnival.SelectedCarnival.id = wndChild:GetCustomUserData(0)
    gUI.Carnival.SelectedCarnival.Schedule = wndChild:GetCustomUserData(1)
    _Carnival_SetBtnSelected(gUI.Carnival.SelectedCarnival.id, gUI.Carnival.SelectedCarnival.Schedule)
    gUI.Carnival.Container:SizeSelf()
    _Carnival_BarPos = nil
  end
  if 0 < gUI.Carnival.SelectedCarnival.id and 0 < gUI.Carnival.SelectedCarnival.Schedule then
    _Carnival_ShowSelectedCarnivalInfo(gUI.Carnival.SelectedCarnival.id, gUI.Carnival.SelectedCarnival.Schedule, true)
  else
    _Carnival_ShowSelectedCarnivalInfo(gUI.Carnival.SelectedCarnival.id, gUI.Carnival.SelectedCarnival.Schedule, false)
  end
  scrollbar = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Left_bg.TaskList_cnt.vertSB_")
  if scrollbar and scrollbar:IsVisible() and _Carnival_BarPos then
    scrollbar = WindowToScrollBar(scrollbar)
    scrollbar:SetCurrentPos(_Carnival_BarPos)
  end
end
function _Carnival_SetBtnSelected(carnivalID, scheduleID)
  local res = false
  local Count = gUI.Carnival.Container:GetChildCount()
  for i = 0, Count - 1 do
    local wndButton = WindowToButton(gUI.Carnival.Container:GetChild(i))
    local btnId = wndButton:GetCustomUserData(0)
    local schedule = wndButton:GetCustomUserData(1)
    if btnId ~= 0 then
      if btnId == carnivalID and schedule == scheduleID then
        wndButton:SetStatus("selected")
        res = true
      else
        wndButton:SetStatus("normal")
      end
    end
  end
  return res
end
function _Carnival_ShowSelectedCarnivalInfo(carnivalID, scheduleID, isShow)
  local isShow = isShow and carnivalID >= 0 and scheduleID >= 0
  if isShow then
    _Carnival_ShowInfo(carnivalID, scheduleID)
  else
    _Carnival_ClearInfo()
  end
end
function _Carnival_ShowInfo(carnivalID, scheduleID)
  gUI.Carnival.Info:SetCustomUserData(0, carnivalID)
  gUI.Carnival.Info:SetCustomUserData(1, scheduleID)
  local title, acttime, lvl, times, desc, recommend, extra_reward, npc, icon, bCan, extra_rewardDesc, recommendNum = GetCarnivalInfo(carnivalID, scheduleID)
  if title == nil then
    _Carnival_ClearInfo()
    return
  end
  local wnd = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.Map_bg.MapName_dlab")
  wnd:SetProperty("Text", title)
  wnd = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.Time_bg.Time_dlab")
  wnd:SetProperty("Text", acttime)
  wnd = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.Level_bg.Level_dlab")
  wnd:SetProperty("Text", lvl)
  wnd = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.TimeCount_bg")
  wnd:SetVisible(false)
  local displaybox = WindowToDisplayBox(WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.NPC_bg.Npc_db"))
  displaybox:ClearAllContent()
  displaybox:InsertBack(npc, 4294967295, 0, 0)
  wnd = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.Num_bg.Num_dlab")
  wnd:SetProperty("Text", recommendNum)
  wnd = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.Contevt_bg.Rule_dlab")
  wnd:SetProperty("Text", desc)
  wnd = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.Map_bg.Carnival_pic")
  wnd:SetProperty("BackImage", icon)
  local rewardGoodsBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.Reward_bg.Reward_gbox"))
  rewardGoodsBox:ResetAllGoods(true)
  GetCarnivalReward(carnivalID)
  gUI.Carnival.Info:SetVisible(true)
end
function _Carnival_ClearInfo()
  gUI.Carnival.Info:SetCustomUserData(0, -1)
  gUI.Carnival.Info:SetCustomUserData(1, -1)
  local wnd = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.Map_bg.MapName_dlab")
  wnd:SetProperty("Text", "")
  wnd = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.Time_bg.Time_dlab")
  wnd:SetProperty("Text", "")
  wnd = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.Level_bg.Level_dlab")
  wnd:SetProperty("Text", "")
  wnd = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.TimeCount_bg")
  wnd:SetVisible(false)
  local displaybox = WindowToDisplayBox(WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.NPC_bg.Npc_db"))
  displaybox:ClearAllContent()
  wnd = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.Num_bg.Num_dlab")
  wnd:SetProperty("Text", "")
  wnd = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.Contevt_bg.Rule_dlab")
  wnd:SetProperty("Text", "")
  wnd = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.Map_bg.Carnival_pic")
  wnd:SetProperty("BackImage", "")
  local rewardGoodsBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.Reward_bg.Reward_gbox"))
  rewardGoodsBox:ResetAllGoods(true)
  gUI.Carnival.Info:SetVisible(false)
end
function _Carnival_SelectedReward(sel)
  for i = 0, #gUI.Carnival.wndRewardList do
    local wndButton = WindowToButton(gUI.Carnival.wndRewardList[i])
    local btnId = wndButton:GetCustomUserData(0)
    if btnId == sel then
      wndButton:SetStatus("selected")
    else
      wndButton:SetStatus("normal")
    end
  end
end
function _OnCarnival_Update()
  if gUI.Carnival.Root:IsVisible() then
    _Carnival_SelectedReward(_Carnival_RewardSelected)
    _Carnival_UpdateTodayList(_Carnival_RewardSelected)
  end
end
function _OnCarnival_SetTime(date, time)
  if gUI.Carnival.Root:IsVisible() then
    local strTime = date .. " " .. time
    gUI.Carnival.Root:GetChildByName("NewTime_dlab"):SetProperty("Text", strTime)
  end
end
function _OnCarnival_UpdateActive(active)
  _Carnival_InitFormulaList()
end
function _OnCarnival_UpdateTrackNum(nTrackNum)
  _Carnival_TrackCount = nTrackNum
  gUI.Carnival.CarnivalList:GetChildByName("Track_lab"):SetProperty("Text", "关注提醒 " .. tostring(_Carnival_TrackCount) .. "/" .. tostring(_Carnival_AllTrack))
end
function _OnCarnival_TrackChange(nCarnivalID, bTrack)
  if _Carnival_List[nCarnivalID] then
    for i = 0, table.maxn(_Carnival_List[nCarnivalID]) do
      local nCount = _Carnival_List[nCarnivalID][i]
      if nCount then
        local wnd = gUI.Carnival.Container:GetChildByName("CarnivalData" .. nCount)
        if wnd then
          local cbwnd = wnd:GetChildByName("CheckButton_cbtn")
          local cbtn = WindowToCheckButton(cbwnd)
          if bTrack == 0 then
            cbtn:SetCheckedState(false)
          elseif bTrack == 1 then
            cbtn:SetCheckedState(true)
          end
        end
      end
    end
  end
  CarnivalUpdateTrackNum()
  _Carnival_UpdateTrack()
end
function _OnCarnival_AddTrack(nCarnivalID, strIcon)
  if _Carnival_TrackHintCount >= 0 then
    gUI.Carnival.Track_gb:SetItemGoods(_Carnival_TrackHintCount, strIcon, -1)
    gUI.Carnival.Track_gb:SetItemData(_Carnival_TrackHintCount, nCarnivalID)
    _Carnival_TrackHintCount = _Carnival_TrackHintCount - 1
  end
end
function _OnCarnival_ShowTrack(nCarnivalID, strNpc, strDesc)
  local wnd = WindowSys_Instance:GetWindow("ActionBar_frm.Carnival_Track")
  local cbtn = WindowToCheckButton(wnd:GetChildByName("CheckButton1"))
  if wnd:IsVisible() then
    wnd:SetProperty("Visible", "false")
    cbtn:SetCheckedState(false)
  end
  wnd:SetProperty("Visible", "true")
  cbtn:SetCustomUserData(0, nCarnivalID)
  wnd:GetChildByName("Detail_dlab"):SetProperty("Text", strDesc)
  if strNpc == "" then
    _Carnival_TrackNpcLink = nil
    wnd:GetChildByName("Notice_dlab"):SetProperty("Text", "此活动无自动寻路")
    wnd:GetChildByName("Join_btn"):SetProperty("Enable", "false")
  else
    _Carnival_TrackNpcLink = strNpc
    wnd:GetChildByName("Notice_dlab"):SetProperty("Text", "点击前往活动NPC处")
    wnd:GetChildByName("Join_btn"):SetProperty("Enable", "true")
  end
end
function _OnCarnival_UpdateRewardItem(dataId, Index, GoodsId, Icon)
  local carnival = gUI.Carnival.Info:GetCustomUserData(0)
  if dataId == carnival then
    local RewardGoodsBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg.Reward_bg.Reward_gbox"))
    local _, icon, quality = GetItemInfoBySlot(-1, GoodsId, 0)
    RewardGoodsBox:SetItemGoods(Index, icon, quality)
    RewardGoodsBox:SetItemData(Index, GoodsId)
  end
end
function UI_Carnival_ShowItemTip(msg)
  local wnd = msg:get_window()
  wnd = WindowToGoodsBox(wnd)
  local index = msg:get_wparam()
  local id = wnd:GetItemData(index)
  if id ~= 0 then
    Lua_Tip_Item(wnd:GetToolTipWnd(0), nil, nil, nil, nil, id)
  end
end
function UI_Carnival_Close(msg)
  gUI.Carnival.Root:SetProperty("Visible", "false")
end
function UI_Carnival_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Top_bg.Title_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Carnival_Show()
  if gUI.Carnival.Root:IsVisible() then
    UI_Carnival_Close()
  else
    gUI.Carnival.Root:SetProperty("Visible", "true")
    _OnCarnival_Update()
  end
end
function Lua_Carnival_ShowByTab(Tab)
  gUI.Carnival.Root:SetProperty("Visible", "true")
  _OnCarnival_Update()
end
function UI_Carnival_RewardChanged(msg)
  gUI.Carnival.SelectedCarnival.id = 0
  gUI.Carnival.SelectedCarnival.Schedule = 0
  local wnd = msg:get_window()
  local sel = wnd:GetCustomUserData(0)
  _Carnival_SelectedReward(sel)
  _Carnival_UpdateTodayList(sel)
  _Carnival_RewardSelected = sel
end
function UI_Carnival_TabctrlClick(msg)
end
function UI_Carnival_GotoBtnClick(msg)
  if _Carnival_ActiveRecommendNpcLink then
    local x, y, mapid = string.match(_Carnival_ActiveRecommendNpcLink, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
    if tonumber(mapid) == -1 then
      TalkToNpc(tonumber(x), tonumber(y))
    else
      MoveToTargetLocation(tonumber(x), tonumber(y), tonumber(mapid))
    end
  end
end
function UI_Carnival_ClickTrackButton(msg)
  local wnd = msg:get_window()
  local id = wnd:GetCustomUserData(0)
  DoTrackCarnival(id)
end
function UI_Carnival_ClickTrackButton2(msg)
  local wnd = msg:get_window()
  local cbtn = WindowToCheckButton(wnd)
  cbtn:SetCheckedState(false)
end
function UI_Carnival_ClickTrackHint(msg)
  local nCarnivalID = msg:get_wparam()
  ShowTrackDesc(nCarnivalID)
end
function UI_Carnival_ClickTrackClose(msg)
  local wnd = WindowSys_Instance:GetWindow("ActionBar_frm.Carnival_Track")
  wnd:SetProperty("Visible", "false")
  local cbtn = WindowToCheckButton(wnd:GetChildByName("CheckButton1"))
  cbtn:SetCheckedState(false)
end
function UI_Carnival_TrackAcceptClick(msg)
  if _Carnival_TrackNpcLink then
    local x, y, mapid = string.match(_Carnival_TrackNpcLink, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
    if tonumber(mapid) == -1 then
      TalkToNpc(tonumber(x), tonumber(y))
    else
      MoveToTargetLocation(tonumber(x), tonumber(y), tonumber(mapid))
    end
  end
end
function UI_Carnival_TrackHintTip(msg)
  local wnd = msg:get_window()
  wnd = WindowToGoodsBox(wnd)
  local index = msg:get_wparam()
  local nCarnivalID = wnd:GetItemData(index)
  local strTip = GetTrackTip(nCarnivalID)
  if strTip then
    local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
    Lua_Tip_Begin(tooltip)
    tooltip:InsertLeftText(strTip, "FFFAFAFA", "", 0, 0)
    Lua_Tip_End(tooltip)
  end
end
function UI_Carnival_ClickCarnivalBtn(msg)
  local wnd = msg:get_window()
  local carnivalID = wnd:GetCustomUserData(0)
  local scheduleID = wnd:GetCustomUserData(1)
  _Carnival_ShowSelectedCarnivalInfo(carnivalID, scheduleID, carnivalID >= 0 and scheduleID >= 0)
  _Carnival_SetBtnSelected(carnivalID, scheduleID)
  gUI.Carnival.SelectedCarnival.id = carnivalID
  gUI.Carnival.SelectedCarnival.Schedule = scheduleID
end
function UI_Carnival_ClickJoin(msg)
  gUI.Carnival.SelectedCarnival.id = 0
  gUI.Carnival.SelectedCarnival.Schedule = 0
  _OnCarnival_Update()
end
function Script_Carnival_OnEvent(event)
  if event == "CARNIVAL_SETTIME" then
    _OnMiniMap_SetTime()
  elseif event == "CARNIVAL_UPDATE" then
    _OnCarnival_Update()
  elseif event == "CARNIVAL_LEVEL_CHANGE" then
    _OnCarnival_Update()
  elseif event == "CARNIVAL_REWARD_ITEM" then
    _OnCarnival_UpdateRewardItem(arg1, arg2, arg3, arg4)
  end
end
function Script_Carnival_OnLoad(event)
  gUI.Carnival.Root = WindowSys_Instance:GetWindow("Carnival")
  gUI.Carnival.Container = WindowToContainer(WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Left_bg.TaskList_cnt"))
  gUI.Carnival.CntChildTemp = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Left_bg.Item_btn")
  gUI.Carnival.Info = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Right_bg")
  gUI.Carnival.wndRewardList = {}
  gUI.Carnival.wndRewardList[0] = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Top_bg.Choose_bg.Level_btn")
  gUI.Carnival.wndRewardList[1] = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Top_bg.Choose_bg.Money_btn")
  gUI.Carnival.wndRewardList[2] = WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Top_bg.Choose_bg.Strong_btn")
  gUI.Carnival.JoinCbtn = WindowToCheckButton(WindowSys_Instance:GetWindow("Carnival.Carnival_bg.Left_bg.Join_cbtn"))
  gUI.Carnival.JoinCbtn:SetCheckedState(true)
  gUI.Carnival.SelectedCarnival = {id = 0, Schedule = 0}
end
tWindow("Carnival.Carnival_bg.Left_bg.Join_cbtn"))
  gUI.Carnival.JoinCbtn:SetCheckedState(true)
  gUI.Carnival.SelectedCarnival = {id = 0, Schedule = 0}
end
