if gUI and not gUI.Experience then
  gUI.Experience = {}
end
local DEFULT_SEL_ITEM_INDEX = 0
local CURRENT_SEL_ITEM_INDEX = 0
local DEFULT_CKECKEDSEL = 0
local LEVEL_MIN = 45
local EXCHECKNAME = {
  [0] = "Check1_cbtn",
  [1] = "Check2_cbtn",
  [2] = "Check3_cbtn"
}
function UI_Experiece_ShowPanel(msg)
  RequestDailyReplevin()
end
function _Experience_OnPanelOpen(count)
  local _, _, _, _, level = GetPlaySelfProp(4)
  if level < LEVEL_MIN then
    Lua_Chat_ShowOSD("EXPERIENCE_FINDBACK")
    return
  end
  if not gUI.Experience.Root:IsVisible() then
    gUI.Experience.DateTc:SetProperty("ItemCount", tostring(count))
    local DayList = GetDailyReplevinInfo()
    gUI.Experience.DesLab:SetProperty("Text", "")
    if DayList[0] ~= nil then
      local strItemsText = ""
      for i = 0, #DayList do
        strItemsText = string.format("%s%d月%s日", strItemsText, DayList[i].Month, DayList[i].Day)
        if #DayList ~= i then
          strItemsText = strItemsText .. "|"
        end
        gUI.Experience.DateTc:SetProperty("ItemsText", tostring(strItemsText))
        CURRENT_SEL_ITEM_INDEX = DEFULT_SEL_ITEM_INDEX
        Experience_Notify_SelItem()
      end
    else
      gUI.Experience.DesLab:SetProperty("Text", "可经验找回的任务已全部完成")
    end
    gUI.Experience.Root:SetVisible(true)
  else
    gUI.Experience.Root:SetVisible(false)
  end
end
function UI_Experience_Close(msg)
  gUI.Experience.Root:SetVisible(false)
end
function UI_Experience_TCSelChange(msg)
  local curr_page = msg:get_wparam()
  if curr_page ~= CURRENT_SEL_ITEM_INDEX then
    CURRENT_SEL_ITEM_INDEX = curr_page
    Experience_Notify_SelItem()
  end
end
function _OnExperience_ShowTab(arg1, arg2)
  if not gUI.Experience.Root:IsVisible() then
    return
  end
  if CURRENT_SEL_ITEM_INDEX == arg1 and arg1 ~= -1 then
    Experience_Notify_SelItem()
  end
end
function Experience_Notify_SelItem()
  local ExList = Send_Experience_SelItem(CURRENT_SEL_ITEM_INDEX)
  gUI.Experience.TaskContain:DeleteAll()
  if ExList[0] == nil then
    gUI.Experience.DesLab:SetProperty("Text", "当日可经验找回的任务已全部完成")
    return
  else
    gUI.Experience.DesLab:SetProperty("Text", "")
  end
  for i = 0, #ExList do
    if ExList[i].QUESTNAME ~= "" then
      local taskTemp = gUI.Experience.TaskContain:Insert(-1, gUI.Experience.TaskTemp)
      taskTemp:SetCustomUserData(1, ExList[i].INDEX)
      taskTemp:SetCustomUserData(2, DEFULT_CKECKEDSEL)
      local strTemp = string.format("剩余%d次,每次找回%d经验", ExList[i].LEFTTIME, tonumber(ExList[i].BASEEXP * ExList[i].PROPER1 / 100))
      taskTemp:GetGrandChild("Pic1_bg.Count_dlab"):SetProperty("Text", tostring(strTemp))
      taskTemp:GetGrandChild("Pic1_bg.Count_dlab"):SetCustomUserData(0, ExList[i].LEFTTIME)
      taskTemp:GetGrandChild("Pic1_bg.Count_dlab"):SetCustomUserData(1, ExList[i].BASEEXP)
      taskTemp:GetGrandChild("Pic2_bg.Task_dlab"):SetProperty("Text", tostring(ExList[i].QUESTNAME))
      local checkTemp1 = GenCheckTmepStr(tostring(ExList[i].PRODES1), tonumber(ExList[i].PROTOKENTYPE1), tonumber(ExList[i].PROTOKENID1), tonumber(ExList[i].PROTOKENNUM1), tonumber(ExList[i].PROPER1))
      if checkTemp1 ~= "" then
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check1_cbtn")):SetProperty("Text", tostring(""))
        WindowToDisplayBox(taskTemp:GetGrandChild("Pic2_bg.des1_dbox")):ClearAllContent()
        WindowToDisplayBox(taskTemp:GetGrandChild("Pic2_bg.des1_dbox")):InsertBack(checkTemp1, 4294966016, 0, 0)
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check1_cbtn")):SetChecked(true)
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check1_cbtn")):SetProperty("Visible", "true")
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check1_cbtn")):SetCustomUserData(1, 0)
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check1_cbtn")):SetCustomUserData(0, ExList[i].PROPER1)
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check3_cbtn")):SetCustomUserData(2, ExList[i].PROTOKENTYPE1)
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check3_cbtn")):SetCustomUserData(3, ExList[i].PROTOKENID1)
      else
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check1_cbtn")):SetProperty("Visible", "false")
      end
      local checkTemp2 = GenCheckTmepStr(tostring(ExList[i].PRODES2), tonumber(ExList[i].PROTOKENTYPE2), tonumber(ExList[i].PROTOKENID2), tonumber(ExList[i].PROTOKENNUM2), tonumber(ExList[i].PROPER2))
      if checkTemp2 ~= "" then
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check2_cbtn")):SetProperty("Text", tostring(""))
        WindowToDisplayBox(taskTemp:GetGrandChild("Pic2_bg.des2_dbox")):ClearAllContent()
        WindowToDisplayBox(taskTemp:GetGrandChild("Pic2_bg.des2_dbox")):InsertBack(checkTemp2, 4294966016, 0, 0)
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check2_cbtn")):SetChecked(false)
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check2_cbtn")):SetProperty("Visible", "true")
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check2_cbtn")):SetCustomUserData(1, 1)
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check2_cbtn")):SetCustomUserData(0, ExList[i].PROPER2)
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check3_cbtn")):SetCustomUserData(2, ExList[i].PROTOKENTYPE2)
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check3_cbtn")):SetCustomUserData(3, ExList[i].PROTOKENID2)
      else
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check2_cbtn")):SetProperty("Visible", "false")
      end
      local checkTemp3 = GenCheckTmepStr(tostring(ExList[i].PRODES3), tonumber(ExList[i].PROTOKENTYPE3), tonumber(ExList[i].PROTOKENID3), tonumber(ExList[i].PROTOKENNUM3), tonumber(ExList[i].PROPER3))
      if checkTemp3 ~= "" then
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check3_cbtn")):SetProperty("Text", tostring(""))
        WindowToDisplayBox(taskTemp:GetGrandChild("Pic2_bg.des3_dbox")):ClearAllContent()
        WindowToDisplayBox(taskTemp:GetGrandChild("Pic2_bg.des3_dbox")):InsertBack(checkTemp3, 4294966016, 0, 0)
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check3_cbtn")):SetChecked(false)
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check3_cbtn")):SetProperty("Visible", "true")
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check3_cbtn")):SetCustomUserData(1, 2)
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check3_cbtn")):SetCustomUserData(0, ExList[i].PROPER3)
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check3_cbtn")):SetCustomUserData(2, ExList[i].PROTOKENTYPE3)
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check3_cbtn")):SetCustomUserData(3, ExList[i].PROTOKENID3)
      else
        WindowToCheckButton(taskTemp:GetGrandChild("Pic2_bg.Check3_cbtn")):SetProperty("Visible", "false")
      end
      taskTemp:SetProperty("Visible", "true")
    end
  end
end
function GenCheckTmepStr(des, CType, CId, Cprice, Per)
  if CType < 0 or CType > 22 then
    return ""
  end
  local itemName
  if TokenTypeName[CType].TokenName == "物品" or TokenTypeName[CType].TokenName == "物品策划类型" then
    des = string.format(des, tostring(Cprice), tostring(Per))
    return des
  else
    itemName = TokenTypeName[CType].TokenName
    if CType == 0 or CType == 1 then
      Cprice = Lua_UI_Money2String(Cprice)
      des = string.format(des, tostring(itemName), tostring(Cprice), tostring(Per))
      return des
    end
  end
  if des ~= "" then
  end
  return des
end
function UI_Experience_Ret(msg)
  local wnd = msg:get_window()
  local wnd_parent = wnd:GetParent():GetParent()
  local sel = wnd_parent:GetCustomUserData(2)
  local item = wnd_parent:GetCustomUserData(1)
  Ask_Experience_Item(CURRENT_SEL_ITEM_INDEX, item, sel)
end
function UI_Experience_Con_Click(msg)
  local wnd_Check = msg:get_window()
  local wnd_parent = wnd_Check:GetParent():GetParent()
  local CurrSelItem = wnd_parent:GetCustomUserData(2)
  local CurrItem = wnd_Check:GetCustomUserData(1)
  if CurrItem ~= CurrSelItem then
    CurrSelItem = CurrItem
    local leftTime = wnd_parent:GetGrandChild("Pic1_bg.Count_dlab"):GetCustomUserData(0)
    local baseExp = wnd_parent:GetGrandChild("Pic1_bg.Count_dlab"):GetCustomUserData(1)
    local per = wnd_Check:GetCustomUserData(0)
    local strTemp = string.format("剩余%d次,每次找回%d经验", leftTime, baseExp * per / 100)
    wnd_parent:GetGrandChild("Pic1_bg.Count_dlab"):SetProperty("Text", tostring(strTemp))
  else
  end
  Experience_UpdateItem(wnd_parent, CurrSelItem)
end
function Experience_UpdateItem(wnd, currSel)
  wnd:SetCustomUserData(2, currSel)
  local wndClild = wnd:GetGrandChild("Pic2_bg")
  for i = 0, #EXCHECKNAME do
    local wndSonClild = wndClild:GetGrandChild(tostring(EXCHECKNAME[i]))
    local CurrValue = wndSonClild:GetCustomUserData(1)
    if CurrValue == currSel then
      WindowToCheckButton(wndSonClild):SetChecked(true)
    else
      WindowToCheckButton(wndSonClild):SetChecked(false)
    end
  end
end
function _OnExperience_ShowPanel(count)
  _Experience_OnPanelOpen(count)
end
function Script_Experience_OnHide()
end
function Script_Experience_OnEvent(event)
  if event == "PROFITREPLEVIN_OPENPANEL" then
    _OnExperience_ShowPanel(arg1)
  elseif event == "PROFITREPLEVIN_UPDATESUCC" then
    _OnExperience_ShowTab(arg1, arg2)
  end
end
function Script_Experience_OnLoad()
  gUI.Experience.Root = WindowSys_Instance:GetWindow("Experience")
  gUI.Experience.DateTc = WindowToTableCtrl(WindowSys_Instance:GetWindow("Experience.Experience_bg.Date_tctl"))
  gUI.Experience.TaskTemp = WindowSys_Instance:GetWindow("Experience.Experience_bg.Image_bg.Tem_bg")
  gUI.Experience.TaskContain = WindowToContainer(WindowSys_Instance:GetWindow("Experience.Experience_bg.Image_bg.Tem_cnt"))
  gUI.Experience.DesLab = WindowToLabel(WindowSys_Instance:GetWindow("Experience.Experience_bg.Image_bg.Exp_dlab"))
end
nce.TaskTemp = WindowSys_Instance:GetWindow("Experience.Experience_bg.Image_bg.Tem_bg")
  gUI.Experience.TaskContain = WindowToContainer(WindowSys_Instance:GetWindow("Experience.Experience_bg.Image_bg.Tem_cnt"))
  gUI.Experience.DesLab = WindowToLabel(WindowSys_Instance:GetWindow("Experience.Experience_bg.Image_bg.Exp_dlab"))
end
