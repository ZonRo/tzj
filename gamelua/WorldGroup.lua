local gUI_SKILL_PROGRESS_DES = gUI_SKILL_PROGRESS_DES
local nStorageDuration = 0
local nStorageDuration2 = 0
local spell_duration = 0
local _Instance_timer
local _Instance_Count = 0
function WorldGroup_ShowNewStr(str)
  ShowErrorMessage(str)
end
function WorldGroup_KeyDown(msg)
  if msg:get_wparam() == 42 then
    Lua_ActBr_UnLock()
  end
  if msg:get_wparam() == 201 and IsKeyPressed(29) then
    Lua_Chat_ChangeChannelByPageBtn("forward")
  end
  if msg:get_wparam() == 209 and IsKeyPressed(29) then
    Lua_Chat_ChangeChannelByPageBtn("backward")
  end
  if msg:get_wparam() == 1 then
    Script_Bag_OnHide()
  end
  if gUI.GemIneff.Root:IsVisible() and IsKeyPressed(1) then
    local listAttr = GetItemGemAttr(0, 0, 0, 1)
    if listAttr ~= nil then
      Messagebox_Show("GEM_INEFF_CHOOSE", 1, OPRATER_TYPE_FORGEM.CLOSEPANEL, 0)
    else
      GemIneff_OnClosePanel()
    end
  end
end
function WorldGroup_KeyUp(msg)
  if msg:get_wparam() == 42 then
    Lua_ActBr_Lock()
  end
end
function WorldGroup_SetInputCapture()
  local box = WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Inputborder_bg.Input_ebox")
  local wnd = WindowSys_Instance:GetKeyboardCaptureWindow()
  if wnd == nil then
    WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
    wnd = WindowSys_Instance:GetKeyboardCaptureWindow()
  end
  local wndType = wnd:GetProperty("ClassName")
  local currWndName = wnd:GetProperty("WindowName")
  if wndType == "EditBox" and currWndName ~= "name_ebox" and currWndName ~= box:GetProperty("WindowName") then
    return
  end
  local edit_box = WindowToEditBox(box)
  if not edit_box:IsKeyboardCaptureWindow() then
    WindowSys_Instance:SetKeyboardCaptureWindow(box)
    WindowSys_Instance:SetFocusWindow(box)
    Lua_Chat_SelectDisplayChannelNew(0)
    WindowToTableCtrl(WindowSys_Instance:GetWindow("ChatDialog.Input_bg.Chat_bg.Image_bg.Info_tctl")):SelectItem(0)
    local pic = WindowSys_Instance:GetWindow("ChatDialog.Input_bg")
    pic = WindowToPicture(pic)
    pic:OnFadeIn()
  else
    local text = box:GetProperty("Text")
    if text == "" then
      WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
      WindowSys_Instance:SetFocusWindow(WindowSys_Instance:GetRootWindow())
      local pic = WindowSys_Instance:GetWindow("ChatDialog.Input_bg")
      pic = WindowToPicture(pic)
      pic:OnFadeOut()
    end
  end
  WorldStage:playSoundByID(32)
end
function WorldGroup_Rbuttondown(msg)
end
function WorldGroup_Lbuttondown(msg)
  local faceWnd = WindowSys_Instance:GetWindow("ChatFace")
  if faceWnd:IsVisible() then
    faceWnd:SetProperty("Visible", "false")
  end
  local actionbar = WindowToGoodsBox(gUI.Bag.GoodsBoxBag:GetParent())
  local from_off = actionbar:GetDragItemIndex()
  Lua_Fri_SetFoucsToWorld()
  if from_off ~= -1 then
    if actionbar:IsSplitItem() then
      ShowErrorMessage("拆分失败")
      actionbar:ResetDragItem()
    else
      local from_win = actionbar:GetDragItemParent()
      local from_type = from_win:GetProperty("BoxType")
      if from_type == "backpack0" then
        from_off = Lua_Bag_GetRealSlot(from_win, from_off)
        local bag = gUI_GOODSBOX_BAG[from_type]
        local num, _, quality, name = GetItemBaseInfoBySlot(bag, from_off)
        if num then
          Messagebox_Show("DESTORY_ITEM", name, bag, from_off, num, quality)
        end
        actionbar:ResetDragItem()
      elseif from_type == "backpack1" then
        local bag = gUI_GOODSBOX_BAG[from_type]
        local num, _, quality, name = GetItemBaseInfoBySlot(bag, from_off)
        if num then
          Messagebox_Show("DESTORY_ITEM", name, bag, from_off, num, quality)
        end
        actionbar:ResetDragItem()
      elseif Lua_Bag_IsActionbtn(from_type) then
        if gUI.ActBr.DragIconFlag == true then
          WorldStage:delActionBtn(Lua_ActBr_GetIndexByOffset(from_type, from_off))
          gUI.ActBr.DragIconFlag = false
          actionbar:ResetDragItem()
        else
          gUI.ActBr.DragIconFlag = true
        end
      elseif Lua_Bag_IsTemp(from_type) then
        gUI.ActBr.DragIconFlag = false
        actionbar:ResetDragItem()
      end
    end
  end
end
function WorldGroup_OnPlayHurtSoundWithFileName(str, num)
  if num and num > 0 then
    local index = math.random(num)
    str = str .. tostring(0) .. tostring(index) .. ".wav"
    WorldStage:playSoundByMainPlayer(str)
  end
end
function WorldGroup_OnPlayHitSoundWithFileName(str, weaponType, armorType)
  if str == "PLAYER_ON_ATTACKING" or str == "PLAYER_BE_ATTACKED" then
    local strSound = gUI_WEAPON_ARMOR_SOUND_MAP[weaponType][armorType]
    WorldStage:playSoundByMainPlayer(strSound)
  end
end
function WorldGroup_OnSoundPlay(eventName, charID, weaponType, armorType, actorSound)
  if actorSound == 1 then
    WorldGroup_PlayActorSound(eventName, charID)
  end
  WorldGroup_PlayMeleeHitSound(weaponType, armorType)
end
function WorldGroup_PlayActorSound(eventName, charID)
  if gUI_PLAYER_SOUND_EFFECT[eventName][charID] ~= nil then
    local str = gUI_PLAYER_SOUND_EFFECT[eventName][charID].FileName
    local num = gUI_PLAYER_SOUND_EFFECT[eventName][charID].FileNumb
    if num and num > 0 then
      local index = math.random(num)
      str = str .. tostring(0) .. tostring(index) .. ".wav"
      WorldStage:playSoundByMainPlayer(str)
    end
  end
end
function WorldGroup_PlayMeleeHitSound(weaponType, armorType)
  local strSound = gUI_WEAPON_ARMOR_SOUND_MAP[weaponType][armorType]
  WorldStage:playSoundByMainPlayer(strSound)
end
function WorldGroup_OnLoad()
  gUI_CurrentStage = 3
  local root = WindowSys_Instance:GetRootWindow()
  if root then
    WindowSys_Instance:SetKeyboardCaptureWindow(root)
  end
  AddDebugCmd("/hidedebug")
  AddDebugCmd("/showdebug")
  AddDebugCmd("/showui")
  AddDebugCmd("/hideui")
  AddDebugCmd("/quit")
  AddDebugCmd("/moveto")
  AddDebugCmd("/move")
  AddDebugCmd("/addteam")
  AddDebugCmd("/quitteam")
  AddDebugCmd("/kickteam")
  AddDebugCmd("/teamleader")
  AddDebugCmd("/pos")
  local DetailWin = WindowSys_Instance:GetWindow("Detail")
  DetailWin:SetProperty("Visible", "true")
  gUI.ActBr.SpellBarMain = WindowSys_Instance:GetWindow("ActionBar_frm.SkillMain_pbar")
  gUI.ActBr.SpellBarMain_Break = WindowSys_Instance:GetWindow("ActionBar_frm.SkillMainBreak_pbar")
  gUI.ActBr.SpellBarMain_Over = WindowSys_Instance:GetWindow("ActionBar_frm.SkillMainOver_pbar")
  gUI.ActBr.SpellBarMain:SetProperty("Visible", "false")
  gUI.ActBr.SpellBarMain_Break:SetProperty("Visible", "false")
  gUI.ActBr.SpellBarMain_Over:SetProperty("Visible", "false")
  local wnd = gUI.ActBr.SpellBarMain:GetToolTipWnd(0)
  wnd:SetProperty("Visible", "false")
  wnd = WindowSys_Instance:GetWindow("ReliveFrame")
  wnd:SetProperty("Visible", "false")
  wnd = WindowSys_Instance:GetWindow("TaskPrompt")
  wnd:SetProperty("Visible", "false")
  wnd = WindowSys_Instance:GetWindow("UseItemBack")
  wnd:SetProperty("Visible", "false")
  DelTimerEvent(3, "spell_chanting")
  DelTimerEvent(3, "spell_overing")
  wnd = WindowSys_Instance:GetWindow("Autopick")
  wnd:SetProperty("Visible", "false")
  Lua_FlyIc_Init()
  Lua_Guide_InitState()
  Lua_Pick_Init()
end
function WorldGroup_OnShow()
  math.randomseed(os.time())
end
function WorldGroup_BreakTimer(timer)
  local alpha = tonumber(gUI.ActBr.SpellBarMain_Break:GetProperty("Alpha"))
  alpha = alpha - timer:GetRealInterval()
  if alpha > 0.1 then
    gUI.ActBr.SpellBarMain_Break:SetProperty("Alpha", tostring(alpha))
  else
    gUI.ActBr.SpellBarMain_Break:SetProperty("Visible", "false")
    DelTimerEvent(3, "spell_chanting")
  end
end
function WorldGroup_OverTimer(timer)
  local alpha = tonumber(gUI.ActBr.SpellBarMain_Over:GetProperty("Alpha"))
  alpha = alpha - timer:GetRealInterval()
  if alpha > 0.1 then
    gUI.ActBr.SpellBarMain_Over:SetProperty("Alpha", tostring(alpha))
  else
    gUI.ActBr.SpellBarMain_Over:SetProperty("Visible", "false")
    DelTimerEvent(3, "spell_overing")
  end
end
function UI_WorldGroup_Instance_ScrollEnd(msg)
  local Post_bar = WindowSys_Instance:GetWindow("Post.Post2_dlab")
  Post_bar:SetProperty("Visible", "false")
end
function _WorldGroup_PasswordExchange(msg)
  SendPasswordExchangeToServer(msg)
end
function WorldGroup_Instance_board(open, timer, curfloor, maxfloor)
  local Instance_board = WindowSys_Instance:GetWindow("Template.Scoreboard_pic")
  if open then
    Instance_board:SetCustomUserData(0, 1)
    if _Instance_timer == nil then
      _Instance_timer = TimerSys_Instance:CreateTimerObject("InstanceTimer", 1, -1, "_WG_Instance_Timer", true, false)
    end
    if _Instance_timer ~= nil then
      if not timer then
        _Instance_timer:Stop()
      else
        _Instance_timer:Start()
      end
    end
    local Floor = WindowSys_Instance:GetWindow("Template.Scoreboard_pic.Floor_Lab.FloorNum_Lab")
    Floor:SetProperty("Text", tostring(curfloor) .. "/" .. tostring(maxfloor))
    local Post_bar = WindowSys_Instance:GetWindow("Post.Post2_dlab")
    Post_bar:SetProperty("Visible", "true")
    Post_bar:SetProperty("Text", "第" .. tostring(curfloor) .. "层")
  else
    Instance_board:SetCustomUserData(0, 0)
    TimerSys_Instance:DestroyTimerObject("InstanceTimer")
    _Instance_timer = nil
    _Instance_Count = 0
  end
end
function _WG_Instance_Timer_Reset()
  if _Instance_timer ~= nil then
    TimerSys_Instance:DestroyTimerObject("InstanceTimer")
    _Instance_timer = nil
    _Instance_Count = 0
  end
  local Floor = WindowSys_Instance:GetWindow("Template.Scoreboard_pic")
  Floor:SetCustomUserData(0, 0)
end
function _WG_Instance_Timer()
  _Instance_Count = _Instance_Count + 1
  local time_board = WindowSys_Instance:GetWindow("Template.Scoreboard_pic.Time_Lab.Timer_Lab")
  if time_board then
    time_board:SetProperty("Text", tostring(Lua_Tip_ConvertTime(_Instance_Count * 1000)))
  end
end
function _WG_ShowObjTooltip(leaveOrEnter, objId, info)
  if leaveOrEnter == 1 then
    Lua_Tip_ShowCharTooltip(objId, info)
  elseif leaveOrEnter == 2 then
    Lua_Tip_HideObjTooltip()
  elseif leaveOrEnter == 3 then
    Lua_Tip_ShowObjTooltip(objId, info)
  elseif leaveOrEnter == 4 then
    Lua_Tip_HideObjTooltip()
  end
end
function _OnWorldGroup_SpellStart(dura, stype, other, desc, flag)
  if flag == gUI_CharType.CHAR_MAIN_PLAYER then
    local spell_type = stype
    local skill_des = gUI_SKILL_PROGRESS_DES[other]
    spell_duration = dura / 1000
    gUI.ActBr.SpellBarMain:SetProperty("Visible", "true")
    gUI.ActBr.SpellBarMain:SetProperty("Text", skill_des or tostring(desc) or "")
    if spell_type == 0 then
      gUI.ActBr.SpellBarMain:SetProperty("Progress", "0")
    else
      gUI.ActBr.SpellBarMain:SetProperty("Progress", "1")
    end
  end
end
function _OnWorldGroup_SpellDelay(dura, flag)
  if flag == 0 then
    local spell_delay = dura ~= -1 and dura / (spell_duration * 1000) or dura
    if spell_delay == -1 and gUI.ActBr.SpellBarMain:IsVisible() then
      gUI.ActBr.SpellBarMain_Break:SetProperty("Visible", "true")
      gUI.ActBr.SpellBarMain_Break:SetProperty("Alpha", "1.0")
      AddTimerEvent(3, "spell_chanting", WorldGroup_BreakTimer)
      gUI.ActBr.SpellBarMain_Break:SetProperty("Text", gUI.ActBr.SpellBarMain:GetProperty("Text"))
    end
  end
end
function _OnWorldGroup_SpellOver(isleadtype, flag)
  if flag == 0 then
    gUI.ActBr.SpellBarMain_Over:SetProperty("Visible", "true")
    gUI.ActBr.SpellBarMain_Over:SetProperty("Alpha", "1.0")
    AddTimerEvent(3, "spell_overing", WorldGroup_OverTimer)
    gUI.ActBr.SpellBarMain_Over:SetProperty("Text", gUI.ActBr.SpellBarMain:GetProperty("Text"))
    if isleadtype then
      gUI.ActBr.SpellBarMain_Over:SetProperty("Progress", "0")
    else
      gUI.ActBr.SpellBarMain_Over:SetProperty("Progress", "1")
    end
  end
end
function _OnWorldGroup_SpellDisturb(dura)
  local progress = tonumber(gUI.ActBr.SpellBarMain:GetProperty("Progress"))
  if progress < 1 then
    progress = progress - dura / 1000 / spell_duration
    gUI.ActBr.SpellBarMain:SetProperty("Progress", tostring(progress))
  end
end
function _OnWorldGroup_ChangeMapImage(arg1)
  local efxpic = WindowSys_Instance:GetWindow("EfxNoLoop_pic")
  if arg1 == 0 then
    efxpic:setEffectFile("")
    gUI.ActBr.SpellBarMain_Break:SetProperty("Visible", "false")
    gUI.ActBr.SpellBarMain_Over:SetProperty("Visible", "false")
    DelTimerEvent(3, "spell_chanting")
    DelTimerEvent(3, "spell_overing")
  else
    efxpic:setEffectDistance(5)
    efxpic:setEffectFile("efxc_ui_qiehuanjingxiang")
  end
end
function _OnWorldGroup_StartTimer()
  AddTimerEvent(1, "RollTimer", Lua_Pick_ThrowItemUpdateTime)
  AddTimerEvent(2, "FlyIconLoad", Lua_FlyIc_ProcessLoadingList)
  AddTimerEvent(3, "FlyIconList", Lua_FlyIc_UpdataIconList)
  AddTimerEvent(1, "paopaoTimer", Lua_Chat_UpdataPaopaoList)
  Debug_ClearLog()
  GemIneff_OnClosePanel()
end
function _OnWorldGroup_ChangeStage()
  ResetTimer()
  Lua_CarnivalUI_Timer_Reset()
  _OnNotifty_ShowHideBtn(false)
  Lua_Pick_GiveUpAll()
  local wnd = WindowSys_Instance:GetWindow("Messagebox")
  if wnd then
    wnd:SetProperty("Visible", "false")
  end
  wnd = WindowSys_Instance:GetWindow("GuildQuest_Athena")
  if wnd then
    wnd:SetProperty("Visible", "false")
  end
  Lua_CarnivalUI_CloseSpecial()
  Lua_CarnivalUI_CloseBattle()
  wnd = WindowSys_Instance:GetWindow("EfxNoLoop_pic.Quest_pic")
  wnd:setEffectFile("")
  Lua_ActBr_SetTimer()
  Battle_ClosePVP()
end
function _OnWorldGroup_ExchangePasswords()
  local passWin = Messagebox_Show("EXCHANGEPASSWORDS")
  local pass = WindowToEditBox(passWin.binded_widget:GetGrandChild("passwords_bg.Edit_ebox"))
  WindowToEditBox(pass):SetProperty("Text", "")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowToEditBox(pass))
  WindowSys_Instance:SetFocusWindow(pass)
end
function WorldGroup_OnEvent(event)
  if event == "PLAYER_SPELL_START" then
    _OnWorldGroup_SpellStart(arg1, arg2, arg3, arg4, arg5)
  elseif event == "PLAYER_SPELL_BREAK" then
    _OnWorldGroup_SpellDelay(arg1, arg2)
  elseif event == "PLAYER_SPELL_OVER" then
    _OnWorldGroup_SpellOver(arg1, arg2)
  elseif event == "PLAYER_SPELL_DISTURB" then
    _OnWorldGroup_SpellDisturb(arg1)
  elseif event == "PLAYER_CHANGEMAP_IMAGE" then
    _OnWorldGroup_ChangeMapImage(arg1)
  elseif event == "UI_ERROR_MESSAGE" then
    Lua_Chat_ShowOSD(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif event == "UI_NOTIY_MESSAGE" then
    Lua_Chat_ShowNotity(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
  elseif event == "PLAYER_DEAD" then
    WorldGroup_PlayActorSound("PLAYER_DEAD", arg1)
  elseif event == "CREATURE_DEAD" then
    WorldGroup_PlayActorSound("CREATURE_DEAD", arg1)
  elseif event == "STAGE_ENTER" then
    _OnWorldGroup_StartTimer()
  elseif event == "STAGE_CHANGED" then
    _OnWorldGroup_ChangeStage()
  elseif event == "UNSIGN_WARNING" then
    Messagebox_Show("UNSIGN_WARNING")
  elseif event == "ENTER_BATTLEGROUP" then
    Messagebox_Show("WILL_ENTER_BATTLE", arg1, arg2)
  elseif event == "ADD_AUTO_ACCEPT_QUEST" then
    local wnd = WindowSys_Instance:GetWindow("TaskPrompt")
    if wnd and not wnd:IsVisible() then
      wnd:SetProperty("Visible", "true")
      wnd:SetCustomUserData(0, arg1)
    end
  elseif event == "SHOW_OBJECT_TOOLTIP" then
    _WG_ShowObjTooltip(arg1, arg2, arg3)
  elseif event == "OPEN_CHAT" then
    WorldGroup_SetInputCapture()
  elseif event == "REPLY_COLLOGUE" then
    Lua_Chat_ReplyByR()
  elseif "LINK_CHAT_ITEM_CLICK" == event then
    _OnLinkE_ChatItemClickNew(arg1, arg2, arg3, arg4, arg5)
  elseif "LINK_CHAT_PLAYER_CLICK" == event then
    _OnLinkE_ChatPlayerClick(arg1, arg2, arg3, arg4)
  elseif "LINK_CHAT_CHAN_CLICK" == event then
    _OnLinkE_ChatChanClick(arg1, arg2)
  elseif "LINK_CHAT_QUEST_CLICK" == event then
    _OnLinkE_ChatQuestClick(arg1, arg2)
  elseif "LINK_OPENUI_CLICK" == event then
    _OnLinkE_OpenUIClick(arg1)
  elseif event == "SHOW_EXCHANGEPASSWORDS" then
    _OnWorldGroup_ExchangePasswords()
  elseif event == "ON_FLOWERS_GETNOTFRI" then
    _OnMiscell_GetFlowers(arg1, arg2, arg3, arg4)
  elseif event == "ON_RANSOMEQUIP" then
    _OnMiscell_GetRanSomEquip(arg1, arg2)
  elseif event == "USER_GUIDE_WALLOW" then
    _OnMisscell_Wallow(arg1)
  elseif event == "USER_GUIDE_COMFIMGUILDCONTEST" then
    _OnMisscell_AllowContest()
  elseif event == "USER_SHOW_ITEMDETAILTIP" then
    _OnLinkE_ChatShowToolTip(arg1)
  elseif event == "ON_HOME_BEWATER" then
    _OnMiscell_OnBeWater(arg1, arg2)
  elseif event == "HOME_FACILITY_BRON" then
    _OnMiscell_HomeFacilityBron(arg1, arg2)
  end
end
function WorldGroup_TaskBtnClick(msg)
  local wnd = msg:get_window()
  local id = wnd:GetCustomUserData(0)
  wnd:SetProperty("Visible", "false")
  ShowAutoAcceptTask(id)
end
