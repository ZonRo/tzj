if gUI and not gUI.GuildM then
  gUI.GuildM = {}
end
local _GuildM_DismissButtonText1 = "解散帮会"
local _GuildM_DismissButtonText2 = "取消解散"
local _GuildM_CurrSelType = 1
local GUILD_ORISON = 361661
gUI_GuildM_VEL_STR_MAP = {
  [1] = "载具1",
  [2] = "载具2",
  [3] = "载具3"
}
gUI_GuildM_ITEM_STR_MAP = {
  [1] = "神器1",
  [2] = "神器2",
  [3] = "神器3",
  [4] = "神器4",
  [5] = "神器5"
}
local _Guild_SortContion = {
  PName = 0,
  PLevel = 1,
  PPos = 2,
  PJob = 3,
  PCont = 4,
  PHCont = 5,
  PCPos = 6,
  PLasTt = 7
}
local _Guild_InviteList = {}
local _Guild_RecruitList = {}
local USER_GUIDE_ADD_UI = 90
local USER_GUIDE_RECRUIT_UI = 91
local _GuildM_DailyMoney = {
  [2] = 1000,
  [3] = 1500,
  [4] = 2000,
  [5] = 2500
}
local _GuildS_AddTypeImage = {
  [1] = "UiTitle003:Title_InviteGuild",
  [2] = "UiTitle003:Title_InviteAlliance",
  [3] = "UiTitle003:Title_AddFoe"
}
local _GuildS_HostileCost = 500000
local _GuildM_TypeStrMap = {
  [0] = "[%s]%s捐献",
  [1] = "[%s]升级%s消耗",
  [2] = "[%s]制造%s消耗",
  [3] = "[%s]升级%s消耗",
  [4] = "[%s]制造%s消耗",
  [5] = "[%s]宣战消耗",
  [6] = "[%s]%s等级提升到%d,增加",
  [7] = "[%s]%s从帮会提取",
  [8] = "[%s]添加敌对帮会消耗",
  [9] = "[%s]%s完成[%s]任务增加"
}
local _GuildM_MaxBankLogCount = 200
local _GuildM_MaxMoneyLogCount = 200
function _GuildM_CtrlShow(selected)
  gUI.GuildM.AllInfoUI:SetVisible(false)
  gUI.GuildM.MemberUI:SetVisible(false)
  gUI.GuildM.WarUI:SetVisible(false)
  gUI.GuildM.InfoUI:SetVisible(false)
  gUI.GuildQ.QuestMain:SetVisible(false)
  if selected == 0 then
    gUI.GuildM.AllInfoUI:SetVisible(true)
  elseif selected == 1 then
    gUI.GuildM.MemberUI:SetVisible(true)
  elseif selected == 2 then
    gUI.GuildM.InfoUI:SetVisible(true)
  elseif selected == 4 then
    gUI.GuildM.WarUI:SetVisible(true)
  elseif selected == 3 then
    gUI.GuildQ.QuestMain:SetVisible(true)
  end
  gUI.GuildM.Welcome:SetVisible(selected < 0)
  gUI.GuildM.tableCtrl:SetVisible(selected >= 0)
end
function _GuildM_ShowGuildInfo()
  local name, createTime, level, createPeople, leader, population, money, buildValue, developValue, studyTime, pracVal = GetGuildDetailInfo()
  gUI.GuildM.InfoBase:RemoveAllItems()
  gUI.GuildM.InfoBase:setEnableHeightCal(true)
  gUI.GuildM.InfoBase:InsertString("帮会名称：|" .. tostring(name), -1)
  gUI.GuildM.InfoBase:InsertString("帮会创建时间：|" .. tostring(createTime), -1)
  gUI.GuildM.InfoBase:InsertString("帮会创建人：|" .. tostring(createPeople), -1)
  gUI.GuildM.InfoBase:InsertString("现任帮主：|" .. tostring(leader), -1)
  gUI.GuildM.InfoBase:InsertString("帮会等级：|" .. tostring(level), -1)
  gUI.GuildM.InfoBase:InsertString("帮会人数：|" .. tostring(population), -1)
  gUI.GuildM.InfoBase:InsertString("帮会资金：|" .. Lua_UI_Money2String(money * 10000), -1)
  gUI.GuildM.InfoBase:InsertString("帮会发展度：|" .. tostring(developValue), -1)
  gUI.GuildM.InfoBase:InsertString("帮会修炼次数：|" .. tostring(studyTime), -1)
  local list = GetGuildBuildInfo()
  gUI.GuildM.InfoHome:RemoveAllItems()
  _GuildM_ShowOneBuildInfo("乾元殿", list[0].lv, list[0].bBuild)
  _GuildM_ShowOneBuildInfo("修行台", list[1].lv, list[1].bBuild)
  _GuildM_ShowOneBuildInfo("觅宝堂", list[2].lv, list[2].bBuild)
  _GuildM_ShowOneBuildInfo("育灵树", list[5].lv, list[5].bBuild)
  local wnd = gUI.GuildM.AllInfoUI:GetGrandChild("Right_bg.Down_bg.Lev_dlab")
  wnd:SetProperty("Text", tostring(level) .. "级")
  wnd = gUI.GuildM.AllInfoUI:GetGrandChild("Right_bg.Down_bg.Money_dlab")
  wnd:SetProperty("Text", Lua_UI_Money2String(money * 10000))
  wnd = gUI.GuildM.AllInfoUI:GetGrandChild("Right_bg.Down_bg.Dev_dlab")
  wnd:SetProperty("Text", tostring(developValue))
  local _, _, _, _, _, _, _, my_guid = GetMyPlayerStaticInfo()
  local hisPoint, dailyPoint, weekPros, hisPros = GetGuildMemberExInfo(my_guid)
  wnd = gUI.GuildM.AllInfoUI:GetGrandChild("Right_bg.Down_bg.Con_dlab")
  wnd:SetProperty("Text", tostring(dailyPoint))
  WorldStage:playSoundByID(38)
end
function _GuildM_ShowGuildBuildLog()
  gUI.GuildM.InfoMain:SetVisible(false)
  gUI.GuildM.ListDisplay:SetVisible(true)
  gUI.GuildM.ListDisplay:ClearAllContent()
  gUI.GuildM.Mon:SetVisible(false)
  local TYPE_STR_MAP = {
    [0] = "[%s][升级%s]消耗%d建设值",
    [1] = "[%s][制造%s]消耗%d建设值",
    [2] = "[%s][升级%s]消耗%d建设值"
  }
  local logCount = GetGBuildValueLogCount()
  if logCount == nil or logCount <= 0 then
    return
  end
  for i = 0, logCount - 1 do
    local typeId, subTypeId, num, timeStr, name = GetGBuildValueLog(i)
    if typeId then
      local subName
      if typeId == 0 then
        subName = gUI_BuildName[subTypeId]
      elseif typeId == 1 or typeId == 2 then
        subName = VEL_STR_MAP[subTypeId]
      elseif typeId == 3 then
        subName = ITEM_STR_MAP[subTypeId]
      end
      local strLog = string.format(TYPE_STR_MAP[typeId], timeStr, subName, num)
      gUI.GuildM.ListDisplay:InsertBack(strLog, 0, 0, 0)
    end
  end
  gUI.GuildM.ListDisplay:MoveToTop()
end
function _GuildM_ShowOneBuildInfo(name, lv, bBuild)
  local str = string.format("%s：|%d级", name, lv)
  if bBuild == true then
    if lv >= 1 then
      str = str .. "[建设中]"
    else
      str = str .. "[升级中]"
    end
  end
  gUI.GuildM.InfoHome:InsertString(str, -1)
end
function _GuildM_CloseAddMember()
  gUI.GuildM.AddMember:SetProperty("Visible", "false")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function _GuildM_CloseRequestList()
  gUI.GuildM.RequestList:SetProperty("Visible", "false")
end
function _GuildM_CloseAdminister()
  gUI.GuildM.Administer:SetProperty("Visible", "false")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function _GuildM_CloseBranch()
  gUI.GuildM.Branch:SetProperty("Visible", "false")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function _GuildM_CloseNotice()
  gUI.GuildM.Notice:SetProperty("Visible", "false")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function _GuildM_CloseMain()
  _GuildM_CloseAddMember()
  _GuildM_CloseRequestList()
  _GuildM_CloseAdminister()
  _GuildM_CloseBranch()
  _GuildM_CloseNotice()
  gUI.GuildM.Main:SetProperty("Visible", "false")
  local wnd = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg.Guild_btn")
  wnd:SetProperty("NormalImage", "UiBtn012:Guild_n")
end
function _GuildM_UpdateBaseInfo()
  local v = {}
  v[1], v[2], v[3], v[4], v[5], v[6], v[7], v[8] = GetGuildBaseInfo()
  local _, curOnline = GetGuildPopulation()
  local label = gUI.GuildM.Main:GetGrandChild("GuildMember_bg.BasicInfo_bg.GuildLev_dlab")
  label:SetProperty("Text", tostring(v[4]))
  label = gUI.GuildM.Main:GetGrandChild("GuildMember_bg.BasicInfo_bg.GuildNum_dlab")
  label:SetProperty("Text", tostring(v[6]) .. "/" .. tostring(v[8]) .. "(在线" .. tostring(curOnline) .. "人)")
end
function _GuildM_UpdateNotice()
  local label = gUI.GuildM.Main:GetGrandChild("GuildAllInfo_bg.Right_bg.Up_bg.Affiche_dlab")
  local text = GetGuildNotice()
  if text ~= nil then
    label:SetProperty("Text", text)
  end
end
function _GuildM_UpdateMyAuthority()
  local pos, branch = GetGuildAuthority()
  local enable = {
    true,
    true,
    true,
    true,
    true,
    true,
    true,
    false
  }
  local visable = {
    false,
    true,
    true,
    true,
    true,
    true,
    true,
    true
  }
  if pos == gUI_GUILD_POSITION.minister then
    enable[1] = false
    enable[4] = false
    enable[5] = false
  elseif pos == gUI_GUILD_POSITION.elder then
    enable[1] = false
    enable[4] = false
  elseif pos == gUI_GUILD_POSITION.viceleader then
    enable[1] = false
    enable[8] = true
  elseif pos == gUI_GUILD_POSITION.leader then
    enable[7] = true
    enable[8] = true
    visable[1] = true
    visable[7] = true
  else
    for i = 1, 6 do
      enable[i] = false
    end
  end
  for i, widget in ipairs(gUI.GuildM.AuthorityButton) do
    widget:SetEnable(enable[i])
    widget:SetVisible(visable[i])
  end
end
function _GuildM_MemberListUpdate()
  if gUI.GuildM.IsGuildMenberListReady == false then
    AskGuildMemberList()
  end
  local list = WindowToListBox(gUI.GuildM.Main:GetGrandChild("GuildMember_bg.GuildImage_bg.MemberList_lbox"))
  list:RemoveAllItems()
  local population = GetGuildPopulation() - 1
  for i = 0, population do
    _GuildM_MemberListAdd(nil, i)
  end
end
function _GuildM_ConvertPosNameToId(posName)
  for i, n in pairs(gUI_GUILD_POSITION_NAME) do
    if string.find(posName, n) then
      local num = GetGuildBranchCount()
      for j = 0, num - 1 do
        local _, branchName = GetGuildBranchInfo(j)
        if string.find(posName, branchName) then
          return i * 10 + j
        end
      end
      return i * 10
    end
  end
  return 1
end
function _GuildM_ConvertTimeToStr(lastTimeHour, is_online)
  local str
  if is_online then
    str = string.format("在线")
  elseif lastTimeHour == 0.5 then
    str = string.format("%s", "小于1小时")
  elseif lastTimeHour < 24 then
    str = string.format("%d小时", lastTimeHour)
  elseif lastTimeHour < 720 then
    local hour = lastTimeHour - math.floor(lastTimeHour / 24) * 24
    local day = math.floor(lastTimeHour / 24)
    str = string.format("%d天%d小时", day, hour)
  elseif lastTimeHour == 720 then
    str = string.format("1个月")
  elseif lastTimeHour < 8640 then
    local day = math.floor(lastTimeHour / 24) - math.floor(lastTimeHour / 24 / 30) * 30
    local month = math.floor(lastTimeHour / 24 / 30)
    str = string.format("%d月%d天", month, day)
  else
    str = string.format("一年以上")
  end
  return str
end
function _GuildM_MemberListAdd(guid, index)
  local guid, name, pos, branch, activeValue, donation, is_online, level, menpai, last_time, is_myself, nickName, mapName, lineId = GetGuildMemberInfo(guid, index)
  if mapName ~= "" and lineId >= 0 then
    mapName = mapName .. "(" .. tostring(lineId + 1) .. "线)"
  end
  if pos > 6 then
    return
  end
  local need_insert = false
  local view_offline = gUI.GuildM.ViewButton:IsChecked()
  local color
  if is_online then
    last_time = 0
  end
  if not is_online and last_time < 1 then
    last_time = 1
  end
  if is_online then
    color = "0xFFFFFFFF"
    need_insert = true
  elseif view_offline then
    color = "0xFF808080"
    need_insert = true
  end
  if need_insert and gUI.GuildM.Main:IsVisible() then
    local list = WindowToListBox(gUI.GuildM.Main:GetGrandChild("GuildMember_bg.GuildImage_bg.MemberList_lbox"))
    local branchName
    if branch > 0 then
      _, branchName = GetGuildBranchInfo(branch - 1)
      if branchName == "" then
        branchName = gUI_CHINA_NUMBER[branch] .. "分堂"
      end
    else
      branchName = ""
    end
    local online = 0
    if is_online then
      online = 1
    end
    local posName = branchName .. gUI_GUILD_POSITION_NAME[pos]
    local prop = {
      name,
      level,
      posName,
      gUI_MenPaiName[menpai],
      activeValue,
      donation,
      mapName,
      _GuildM_ConvertTimeToStr(last_time, is_online)
    }
    local insert_index = -1
    local size = list:GetItemCount()
    if size > 0 then
      for i = 0, size - 1 do
        local key_Main1, key_sub
        local item1 = list:GetItem(i)
        if tonumber(gUI.GuildM.SortIndex) == tonumber(_Guild_SortContion.PPos) or tonumber(gUI.GuildM.SortIndex) == tonumber(_Guild_SortContion.PLevel) or tonumber(gUI.GuildM.SortIndex) == tonumber(_Guild_SortContion.PJob) then
          local key_sub1 = item1:GetColumnText(_Guild_SortContion.PPos)
          local key_sub2 = item1:GetColumnText(_Guild_SortContion.PLevel)
          local key_sub3 = item1:GetColumnText(_Guild_SortContion.PJob)
          key_sub1 = GetParsedStr(key_sub1)
          key_sub2 = GetParsedStr(key_sub2)
          key_sub3 = GetParsedStr(key_sub3)
          key_sub3 = gUI_MenPaiId[key_sub3]
          if key_sub1 == nil then
            key_sub1 = 0
          end
          if key_sub2 == nil then
            key_sub2 = 0
          end
          if key_sub3 == nil then
            key_sub3 = 0
          end
          local key_subn = _GuildM_ConvertPosNameToId(key_sub1)
          key_sub = 0
          if gUI.GuildM.SortIndex == _Guild_SortContion.PPos then
            key_Main1 = (pos * 10 + branch) * 10000 + level * 100 + (10 - menpai)
            key_sub = tonumber(key_subn) * 10000 + tonumber(key_sub2) * 100 + (10 - tonumber(key_sub3))
          elseif gUI.GuildM.SortIndex == _Guild_SortContion.PLevel then
            key_Main1 = tonumber(pos * 10 + branch) + tonumber(level) * 10000 + (10 - menpai) * 100
            key_sub = tonumber(key_subn) + tonumber(key_sub2) * 10000 + (10 - tonumber(key_sub3)) * 100
          elseif gUI.GuildM.SortIndex == _Guild_SortContion.PJob then
            key_Main1 = tonumber(pos * 10 + branch) + tonumber(level) * 100 + tonumber(10 - menpai) * 10000
            key_sub = tonumber(key_subn) + tonumber(key_sub2) * 100 + tonumber(10 - tonumber(key_sub3)) * 10000
          end
        elseif tonumber(gUI.GuildM.SortIndex) == tonumber(_Guild_SortContion.PCont) then
          key_Main1 = tonumber(activeValue)
          key_sub = item1:GetColumnText(gUI.GuildM.SortIndex)
          key_sub = tonumber(GetParsedStr(tostring(key_sub)))
          if key_sub == nil then
            key_sub = 0
          end
        elseif tonumber(gUI.GuildM.SortIndex) == tonumber(_Guild_SortContion.PHCont) then
          key_Main1 = tonumber(donation)
          key_sub = item1:GetColumnText(gUI.GuildM.SortIndex)
          key_sub = tonumber(GetParsedStr(tostring(key_sub)))
          if key_sub == nil then
            key_sub = 0
          end
        elseif tonumber(gUI.GuildM.SortIndex) == tonumber(_Guild_SortContion.PLasTt) then
          key_sub = item1:GetUserData()
          key_Main1 = last_time
        elseif tonumber(gUI.GuildM.SortIndex) == tonumber(_Guild_SortContion.PCPos) then
          key_sub = item1:GetColumnText(gUI.GuildM.SortIndex)
          key_sub = GetParsedStr(key_sub)
          key_Main1 = mapName
        end
        if gUI.GuildM.SortFlag[gUI.GuildM.SortIndex + 1] == 0 then
          if key_sub > key_Main1 then
            insert_index = i
            break
          end
        elseif key_sub < key_Main1 then
          insert_index = i
          break
        end
      end
    end
    local str = string.format("{#C^%s^%s}|{#C^%s^%d}|{#C^%s^%s}|{#C^%s^%s}|{#C^%s^%d}|{#C^%s^%d}|{#C^%s^%s}|{#C^%s^%s}", color, prop[1], color, prop[2], color, prop[3], color, prop[4], color, prop[5], color, prop[6], color, prop[7], color, prop[8])
    local item = list:InsertString(str, insert_index)
    item:SetUserData64(guid)
    item:SetUserData(last_time)
    list:SetProperty("TextHorzAlign", "Left")
    list:SetProperty("TextHorzAlign", "HCenter")
  end
  _GuildM_UpdateBaseInfo()
end
function _GuildM_MemberListDel(guid)
  local list = WindowToListBox(gUI.GuildM.Main:GetGrandChild("GuildMember_bg.GuildImage_bg.MemberList_lbox"))
  local index = list:FindItemPosByData64(guid)
  if index ~= -1 then
    local item = list:GetItem(index)
    list:RemoveItem(index)
  end
  _GuildM_UpdateBaseInfo()
end
function _GuildM_SaveBranch()
  local list = WindowToListBox(gUI.GuildM.Branch:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  local item = list:GetSelectedItem()
  if not item then
    return
  end
  local ebox = WindowToEditBox(gUI.GuildM.Branch:GetGrandChild("Image1_bg.ChangeName_ebox"))
  local text = ebox:GetProperty("Text")
  if string.len(text) == 0 then
    ShowErrorMessage("分堂名称不能为空，请重新输入")
    return
  end
  if WorldStage:isValidString(text) == 0 then
    WorldGroup_ShowNewStr(UI_SYS_MSG.CHAT_INVALID)
    return
  end
  if string.match(text, "%s") then
    ShowErrorMessage("分堂名称不能包含空格字符")
    return
  end
  for i = 0, list:GetItemCount() - 1 do
    if text == list:GetItem(i):GetColumnText(0) then
      ShowErrorMessage("分堂名称重复")
      return
    end
  end
  ModifyGuildBranchName(item:GetUserData(), text)
end
function _GuildM_ReCreateBranchList(list, combo)
  if list ~= nil then
    list:RemoveAllItems()
  end
  if combo ~= nil then
    combo:RemoveAllItems()
    combo:InsertString("", -1)
    combo:SetItemData(0, 0)
  end
  local index = 0
  while true do
    index = index + 1
    local minister_guid, name = GetGuildBranchInfo(index - 1)
    if minister_guid == nil then
      break
    end
    if name == "" then
      name = gUI_CHINA_NUMBER[index] .. "分堂"
    end
    if list ~= nil then
      local item = list:InsertString(name, -1)
      item:SetUserData(index - 1)
      list:SetProperty("TextHorzAlign", "Left")
      list:SetProperty("TextHorzAlign", "HCenter")
    end
    if combo ~= nil then
      local item = combo:InsertString(name, -1)
      combo:SetItemData(index, index)
    end
  end
end
function _GuildM_UpdateBranchList(index, new_name, old_name)
  local ebox = WindowToEditBox(gUI.GuildM.Branch:GetGrandChild("Image1_bg.ChangeName_ebox"))
  ebox:SetProperty("Text", "")
  local list1 = WindowToListBox(gUI.GuildM.Branch:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  local index_find = list1:FindItemPosByData(index - 1)
  if index_find ~= -1 then
    local item = list1:GetItem(index_find)
    item:SetColumnText(new_name, 0)
  end
  gUI.GuildM.adm.branch_combo:SelectItemByData(index - 1)
  gUI.GuildM.adm.branch_combo:SetSelectString(new_name)
  _GuildM_MemberListUpdate()
end
function _GuildM_UpdateGuildInfoBySelected(selected)
  if selected == 0 then
    _GuildM_ShowGuildInfo()
    _OnGuildM_UpdateBaseInfo(0)
    Lua_GuildQ_UpdateBaseInfo(0)
    _OnGuildQ_UpdateQuestList(true)
  elseif selected == 1 then
    RequestMemberList()
  elseif selected == 2 then
    _GuildM_CurrSelType = 1
    _OnGuildM_ChooseCertainInfo()
    _GuildM_ShowGuildInfo()
  elseif selected == 3 then
    _OnGuildQ_UpdateQuestListAll()
  elseif selected == 4 then
    Lua_GuildS_ShowInit()
  end
  Lua_Fri_SetFoucsToWorld()
end
function _GuideList_FoundItemByGid(list, gid)
  for key, value in pairs(list) do
    if value and value.Gid == gid then
      return value
    end
  end
  return nil
end
function _GuildM_UpdateOrisonInfo()
  local count = UserBagCountItemByID(GUILD_ORISON)
  local name, icon, quality = GetItemInfoBySlot(-1, GUILD_ORISON, 0)
  local orisonBox = WindowToGoodsBox(gUI.GuildM.Orison:GetGrandChild("GuildMainPray_bg.Image_bg.Pray_gbox"))
  orisonBox:SetItemGoods(0, icon, quality)
  orisonBox:SetItemSubscript(0, tostring(count))
end
function _OnGuild_Guide_WarResult_Update()
  if gUI.GuildM.WarUI:IsVisible() then
    Lua_GuildS_ShowWarInfo()
  end
end
function _OnGuildM_ChooseCertainInfo()
  local btn1 = WindowSys_Instance:GetWindow("GuildMain.GuildInfo_bg.Left_bg.XiXin_btn")
  local btn2 = WindowSys_Instance:GetWindow("GuildMain.GuildInfo_bg.Left_bg.CangKu_btn")
  local btn3 = WindowSys_Instance:GetWindow("GuildMain.GuildInfo_bg.Left_bg.ShouHu_btn")
  local btn4 = WindowSys_Instance:GetWindow("GuildMain.GuildInfo_bg.Left_bg.ZiJin_btn")
  btn1:SetProperty("Enable", "true")
  btn2:SetProperty("Enable", "true")
  btn3:SetProperty("Enable", "true")
  btn4:SetProperty("Enable", "true")
  gUI.GuildM.InfoMain:SetVisible(false)
  gUI.GuildM.ListDisplay:SetVisible(false)
  gUI.GuildM.Mon:SetVisible(false)
  if tonumber(btn1:GetProperty("CustomUserData")) == _GuildM_CurrSelType then
    btn1:SetProperty("Enable", "false")
    gUI.GuildM.InfoMain:SetVisible(true)
  elseif tonumber(btn2:GetProperty("CustomUserData")) == _GuildM_CurrSelType then
    btn2:SetProperty("Enable", "false")
    gUI.GuildM.ListDisplay:SetVisible(true)
  elseif tonumber(btn3:GetProperty("CustomUserData")) == _GuildM_CurrSelType then
    btn3:SetProperty("Enable", "false")
    gUI.GuildM.Mon:SetVisible(true)
  elseif tonumber(btn4:GetProperty("CustomUserData")) == _GuildM_CurrSelType then
    btn4:SetProperty("Enable", "false")
    gUI.GuildM.ListDisplay:SetVisible(true)
  end
end
function _OnGuildM_Call_Boss(bossType)
  if bossType < 1 or bossType > 5 then
    return
  end
  local bossNameMap = {
    [1] = "初级守护兽",
    [2] = "中级级守护兽",
    [3] = "高级级守护兽",
    [4] = "超级守护兽",
    [5] = "神级守护兽"
  }
  Lua_Chat_ShowNotity(0, "GUILD_CALL_BOSS", bossNameMap[bossType])
end
function Lua_GuildM_ReadyBaseInfo(isready)
  gUI.GuildM.IsGuildInfoReady = isready
  if not isready then
    _GuildM_CloseMain()
  end
  if gUI.GuildM.Main:IsVisible() then
    local guildID, _, _, level = GetGuildBaseInfo()
    if level >= 1 and gUI.GuildM.IsGuildInfoReady then
      local selected = gUI.GuildM.tableCtrl:GetSelectItem()
      _GuildM_CtrlShow(selected)
      _GuildM_UpdateGuildInfoBySelected(selected)
    end
  end
end
function Lua_GuildM_ShowDonateUI()
  _OnGuildM_ContributeShow()
end
function Lua_GuildM_ShowAddMember(addType)
  if gUI.GuildM.AddMember:IsVisible() then
    gUI.GuildM.AddMember:SetVisible(false)
    WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
    return
  end
  gUI.GuildM.IsAddMemberChar = addType
  local label = WindowToLabel(gUI.GuildM.AddMember:GetGrandChild("UserNameTips_dlab"))
  local titleImage = WindowToLabel(gUI.GuildM.AddMember:GetGrandChild("Title_slab"))
  titleImage:SetProperty("BackImage", _GuildS_AddTypeImage[addType])
  local guildHostile = gUI.GuildM.AddMember:GetGrandChild("GuildHostile_lb")
  guildHostile:SetProperty("Visible", "false")
  if gUI.GuildM.IsAddMemberChar == 1 then
    label:SetProperty("Text", "请输入角色名")
  elseif gUI.GuildM.IsAddMemberChar == 2 then
    label:SetProperty("Text", "请输入帮会名")
  elseif gUI.GuildM.IsAddMemberChar == 3 then
    label:SetProperty("Text", "请输入帮会名")
    guildHostile:SetProperty("Text", "(添加敌对帮会需扣除帮会资金" .. Lua_UI_Money2String(_GuildS_HostileCost) .. ")")
    guildHostile:SetProperty("Visible", "true")
  end
  gUI.GuildM.AddMember:SetProperty("Visible", "true")
  local ebox = WindowToEditBox(gUI.GuildM.AddMember:GetGrandChild("Image_bg.UserName_ebox"))
  ebox:SetProperty("Text", "")
  WindowSys_Instance:SetKeyboardCaptureWindow(ebox)
end
function Lua_GuildM_KickMember(target_guid, target_name)
  local _, _, _, _, _, _, _, my_guid = GetMyPlayerStaticInfo()
  if my_guid == target_guid then
    ShowErrorMessage("无法开除自己")
    return
  end
  local my_pos, _ = GetGuildAuthority()
  local _, _, target_pos, _ = GetGuildMemberInfo(target_guid)
  if my_pos < target_pos then
    ShowErrorMessage("你没有此权限")
    return
  end
  Messagebox_Show("GUILD_KICK", target_guid, target_name)
end
function Lua_GuildM_LeaderAlter(target_guid, target_name)
  local _, _, cur_leader_guid = GetGuildAbdicateInfo()
  local _, _, target_pos, target_branch = GetGuildMemberInfo(target_guid)
  if cur_leader_guid == target_guid then
    ShowErrorMessage("无法向自己转让帮主")
    return
  end
  if gUI_GUILD_POSITION.viceleader ~= target_pos then
    ShowErrorMessage("只能让位给副帮主")
    return
  end
  Messagebox_Show("GUILDLEADER_ABDICATE_TO_SOMEONE", target_guid, target_name)
end
function _OnGuildM_ReadyBaseInfo(time_remain, guild_Level)
  Lua_GuildM_ReadyBaseInfo(true)
end
function _OnGuildM_UpdateBaseInfo(flag)
  if not gUI.GuildM.Main:IsVisible() then
    return
  end
  if flag == 0 then
    _GuildM_UpdateNotice()
    _GuildM_UpdateMyAuthority()
  elseif flag == 1 then
    _GuildM_UpdateBaseInfo()
  elseif flag == 2 then
    _GuildM_UpdateNotice()
  elseif flag == 3 then
    _GuildM_UpdateBaseInfo()
    _GuildM_UpdateMyAuthority()
    _GuildM_MemberListUpdate()
  end
end
function _OnGuildM_UpdateRequestListMember(guid, name, level, menpai, apply_time)
  local list = WindowToListBox(gUI.GuildM.RequestList:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  local str = string.format("%s|%s|%d", name, gUI_MenPaiName[menpai], level)
  local item = list:InsertString(str, -1)
  list:SetProperty("TextHorzAlign", "Left")
  list:SetProperty("TextHorzAlign", "HCenter")
  item:SetUserData64(guid)
end
function _OnGuildM_AgreeRequestListMember(guid)
  Lua_UI_ShowMessageInfo("批准申请成功！")
  local list = WindowToListBox(gUI.GuildM.RequestList:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  list:RemoveItemByData64(guid)
end
function _OnGuildM_RespondAddMember(guid, pop_name, gid, guild_name, response_time, guild_lev, guild_growPoint, guildPop_num)
  if guild_lev == 0 then
    local item = _GuideList_FoundItemByGid(_Guild_RecruitList, gid)
    if item then
      item.ResTime = response_time
    else
      table.insert(_Guild_RecruitList, 1, {
        Gid = gid,
        LeaderName = pop_name,
        GuildName = guild_name,
        LeaderGuid = guid,
        ResTime = response_time,
        GuildNum = guildPop_num
      })
    end
    AddTimerEvent(1, "RecruitTimer", _RecruitTimerCallback)
  else
    if _GuideList_FoundItemByGid(_Guild_InviteList, gid) then
      return
    end
    table.insert(_Guild_InviteList, 1, {
      Gid = gid,
      LeaderName = pop_name,
      GuildName = guild_name,
      LeaderGuid = guid,
      GuildLev = guild_lev,
      GuildGrowPoint = guild_growPoint
    })
  end
end
function _OnGuildM_UpdatePosition(guid, is_myself)
  if gUI.GuildM.Main:IsVisible() then
    if is_myself then
      _GuildM_UpdateMyAuthority()
    end
    _GuildM_MemberListDel(guid)
    _GuildM_MemberListAdd(guid)
  end
  local _, name, pos = GetGuildMemberInfo(guid)
  if gUI_GUILD_POSITION_NAME[pos] ~= "帮主" then
    local str = string.format(UI_SYS_MSG.GUILD_CHANGE_POSITION, name, gUI_GUILD_POSITION_NAME[pos])
    Lua_Chat_AddSysLog(str)
  end
end
function _OnGuildM_DismissSucc(isimmediate, time_remain)
  local dismiss_button = WindowToButton(gUI.GuildM.Main:GetGrandChild("GuildMember_bg.Disband_btn"))
  if isimmediate then
    Lua_GuildM_ReadyBaseInfo(false)
    Lua_Chat_AddSysLog(UI_SYS_MSG.GUILD_DISMISS_SUCC1)
    dismiss_button:SetProperty("Text", _GuildM_DismissButtonText1)
  elseif time_remain == 0 then
    Lua_Chat_AddSysLog(UI_SYS_MSG.GUILD_DISMISS_SUCC2_1)
    dismiss_button:SetProperty("Text", _GuildM_DismissButtonText1)
  else
    local strTime = Lua_UI_DetailTime(time_remain)
    local str = string.format(UI_SYS_MSG.GUILD_DISMISS_SUCC2, strTime)
    Lua_Chat_AddSysLog(str)
    dismiss_button:SetProperty("Text", _GuildM_DismissButtonText2)
  end
end
function _OnGuildM_ContributeShow()
  local groupId = GetGuildBaseInfo()
  if groupId == -1 then
    Lua_Chat_ShowOSD("GUILD_NO_GUILD")
    return
  end
  gUI.GuildM.ContributeEditBox:SetProperty("Text", "")
  local _, playerNonBindGold, playerBindGold = GetBankAndBagMoney()
  local curGold, maxGold, curDaliyGold, maxDaliyGold = GetGuildContributeInfo()
  local limit = 0
  gUI.GuildM.ContributeEditBox:SetProperty("MaxValue", tostring(maxDaliyGold - curDaliyGold))
  local label1 = WindowSys_Instance:GetWindow("GuildContribute.Label2_dlab")
  local label2 = WindowSys_Instance:GetWindow("GuildContribute.Label3_dlab")
  label1:SetProperty("Text", string.format("帮会资金目前为：%d金/%d金", curGold, maxGold))
  label2:SetProperty("Text", string.format("今日还可捐献%d金", maxDaliyGold - curDaliyGold))
  WindowSys_Instance:SetKeyboardCaptureWindow(gUI.GuildM.ContributeEditBox)
  gUI.GuildM.ContributeRoot:SetVisible(true)
end
function _OnGuildM_BuildUpdate(flag, buildType)
  if flag == 0 then
    Lua_Chat_AddSysLog(string.format(UI_SYS_MSG.GUILD_BUILD_UPGRADING, gUI_BuildName[buildType]))
  else
    Lua_Chat_AddSysLog(string.format(UI_SYS_MSG.GUILD_BUILD_UPGRADED, gUI_BuildName[buildType]))
  end
end
function _OnGuildM_ReadyMemberInfo()
  gUI.GuildM.IsGuildMenberListReady = true
end
function UI_GuildM_CloseAddMember(msg)
  _GuildM_CloseAddMember()
end
function UI_GuildM_CloseContribute(msg)
  gUI.GuildM.ContributeRoot:SetVisible(false)
  Lua_Fri_SetFoucsToWorld()
end
function UI_GuildM_DoContribute(msg)
  local amount = 0
  if gUI.GuildM.ContributeEditBox:GetProperty("Text") ~= "" then
    amount = tonumber(gUI.GuildM.ContributeEditBox:GetProperty("Text"))
  end
  if amount > 0 then
    GetGuildDoContribute(amount)
  end
  UI_GuildM_CloseContribute()
end
function UI_GuildM_ListBoxRClick(msg)
  local index = msg:get_wparam()
  local list = WindowToListBox(gUI.GuildM.Main:GetGrandChild("GuildMember_bg.GuildImage_bg.MemberList_lbox"))
  local guid = list:GetItemData64(index)
  local _, name, _, _, _, _, is_online = GetGuildMemberInfo(guid)
  if name ~= gUI_MainPlayerAttr.Name then
    Lua_MenuShow("ON_GUILD_MEMBER", name, guid, is_online)
  end
end
function UI_GuildM_CtrlChange(msg)
  local selected = gUI.GuildM.tableCtrl:GetSelectItem()
  _GuildM_CtrlShow(selected)
  _GuildM_UpdateGuildInfoBySelected(selected)
end
function UI_GuildM_CloseRequestList(msg)
  _GuildM_CloseRequestList()
end
function UI_GuildM_CloseAdminister(msg)
  UI_GuildM_CloseModifyAnounce()
  _GuildM_CloseAdminister()
end
function UI_GuildM_CloseBranch(msg)
  _GuildM_CloseBranch()
end
function UI_GuildM_CloseNotice(msg)
  _GuildM_CloseNotice()
end
function UI_GuildM_CloseMain(msg)
  _GuildM_CloseMain()
end
function UI_GuildMain_HelpBtnClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("GuildMain.Top_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_GuildM_ShowAddMember()
  Lua_GuildM_ShowAddMember(1)
end
function UI_GuildM_ShowRequestList()
  if gUI.GuildM.RequestList:IsVisible() then
    _GuildM_CloseRequestList()
  else
    local list = WindowToListBox(gUI.GuildM.RequestList:GetGrandChild("GuildImage_bg.MemberList_lbox"))
    list:RemoveAllItems()
    gUI.GuildM.RequestList:SetProperty("Visible", "true")
    RequestGuildApplicants()
    local checkBtn = WindowToCheckButton(gUI.GuildM.RequestList:GetGrandChild("Level_bg.CheckButton_cbtn"))
    local _, _, _, _, _, _, _, _, _, autoLv = GetGuildBaseInfo()
    if autoLv > 0 then
      checkBtn:SetCheckedState(true)
      gUI.GuildM.RequestList:GetGrandChild("Level_bg.Label3_dlab"):SetProperty("Text", tostring(autoLv))
      gUI.GuildM.RequestList:GetGrandChild("Level_bg.Reduce_btn"):SetEnable(true)
      gUI.GuildM.RequestList:GetGrandChild("Level_bg.Add_btn"):SetEnable(true)
      if autoLv == 1 then
        gUI.GuildM.RequestList:GetGrandChild("Level_bg.Reduce_btn"):SetEnable(false)
      end
      if autoLv == 59 then
        gUI.GuildM.RequestList:GetGrandChild("Level_bg.Add_btn"):SetEnable(false)
      end
    else
      checkBtn:SetCheckedState(false)
      gUI.GuildM.RequestList:GetGrandChild("Level_bg.Label3_dlab"):SetProperty("Text", "1")
      gUI.GuildM.RequestList:GetGrandChild("Level_bg.Reduce_btn"):SetEnable(false)
    end
  end
end
function UI_GuildM_RequestListAdd(msg)
  local wnd = gUI.GuildM.RequestList:GetGrandChild("Level_bg.Label3_dlab")
  local lv = tonumber(wnd:GetProperty("Text"))
  wnd:SetProperty("Text", tostring(lv + 1))
  if lv + 1 >= 59 then
    gUI.GuildM.RequestList:GetGrandChild("Level_bg.Add_btn"):SetEnable(false)
  end
  gUI.GuildM.RequestList:GetGrandChild("Level_bg.Reduce_btn"):SetEnable(true)
end
function UI_GuildM_RequestListReduce(msg)
  local wnd = gUI.GuildM.RequestList:GetGrandChild("Level_bg.Label3_dlab")
  local lv = tonumber(wnd:GetProperty("Text"))
  wnd:SetProperty("Text", tostring(lv - 1))
  if lv - 1 <= 1 then
    gUI.GuildM.RequestList:GetGrandChild("Level_bg.Reduce_btn"):SetEnable(false)
  end
  gUI.GuildM.RequestList:GetGrandChild("Level_bg.Add_btn"):SetEnable(true)
end
function UI_GuildM_RequestListAutoLvClick(msg)
  local checkBtn = WindowToCheckButton(gUI.GuildM.RequestList:GetGrandChild("Level_bg.CheckButton_cbtn"))
  local bCheck = checkBtn:IsChecked()
  if bCheck then
    local lv = gUI.GuildM.RequestList:GetGrandChild("Level_bg.Label3_dlab"):GetProperty("Text")
    SetGuildAutoLv(tonumber(lv))
  else
    SetGuildAutoLv(-1)
  end
end
function UI_GuildM_ShowAdminister()
  local list = WindowToListBox(gUI.GuildM.Main:GetGrandChild("GuildMember_bg.GuildImage_bg.MemberList_lbox"))
  local item = list:GetSelectedItem()
  if not item then
    ShowErrorMessage("请选择需要管理的成员")
    return
  end
  local target_guid = item:GetUserData64()
  local my_pos, my_branch = GetGuildAuthority()
  local guid, name, target_pos, target_branch, donation, activeValue, is_online, level, menpai, last_time, is_myself, nickname = GetGuildMemberInfo(target_guid)
  gUI.GuildM.adm.target_guid = target_guid
  gUI.GuildM.adm.target_pos = target_pos
  gUI.GuildM.adm.target_branch = target_branch
  local has_permission = false
  if my_pos == gUI_GUILD_POSITION.minister then
    has_permission = (target_branch == my_branch or target_pos < gUI_GUILD_POSITION.elite) and not is_myself
    gUI.GuildM.adm.nname_edit:SetProperty("ReadOnly", tostring(not has_permission))
  else
    for i, v in ipairs(gUI.GuildM.adm.permission[my_pos]) do
      if target_pos == v then
        has_permission = true
        break
      end
    end
    gUI.GuildM.adm.nname_edit:SetProperty("ReadOnly", tostring(not has_permission))
  end
  _GuildM_ReCreateBranchList(nil, gUI.GuildM.adm.branch_combo)
  gUI.GuildM.adm.branch_combo:SetEnable(true)
  gUI.GuildM.adm.position_combo:RemoveAllItems()
  gUI.GuildM.adm.position_combo:SetEnable(true)
  if has_permission then
    for i, option in ipairs(gUI.GuildM.adm.position_option[my_pos]) do
      local item = gUI.GuildM.adm.position_combo:InsertString(gUI.GuildM.adm.position_name[option], -1)
      item:SetUserData(option)
    end
  else
    local item = gUI.GuildM.adm.position_combo:InsertString(gUI.GuildM.adm.position_name[target_pos], -1)
    item:SetUserData(target_pos)
  end
  if has_permission and my_pos == gUI_GUILD_POSITION.minister then
    gUI.GuildM.adm.branch_combo:SelectItemByData(my_branch)
    gUI.GuildM.adm.branch_combo:SetEnable(false)
  else
    gUI.GuildM.adm.branch_combo:SelectItemByData(target_branch)
  end
  if is_myself then
    gUI.GuildM.adm.position_combo:RemoveAllItems()
    local item = gUI.GuildM.adm.position_combo:InsertString(gUI.GuildM.adm.position_name[my_pos], -1)
    item:SetUserData(my_pos)
    gUI.GuildM.adm.position_combo:SelectItemByData(gUI.GuildM.adm.target_pos)
    gUI.GuildM.adm.position_combo:SetEnable(false)
  end
  gUI.GuildM.adm.position_combo:SelectItemByData(target_pos)
  gUI.GuildM.adm.position_combo:SetEnable(has_permission)
  if target_pos > gUI_GUILD_POSITION.minister or target_pos == gUI_GUILD_POSITION.member then
    gUI.GuildM.adm.branch_combo:SetEnable(false)
  else
    gUI.GuildM.adm.branch_combo:SetEnable(has_permission)
  end
  if is_myself or gUI.GuildM.adm.branch_combo:GetItemCount() == 0 then
    gUI.GuildM.adm.branch_combo:SetEnable(false)
  end
  gUI.GuildM.adm.name_lab:SetProperty("Text", name)
  gUI.GuildM.adm.level_lab:SetProperty("Text", tostring(level))
  gUI.GuildM.adm.pro_lab:SetProperty("Text", gUI_MenPaiName[menpai])
  gUI.GuildM.adm.nname_edit:SetProperty("Text", tostring(nickname))
  gUI.GuildM.adm.old_nickname = nickname
  gUI.GuildM.Administer:SetProperty("Visible", "true")
end
function UI_GuildM_ShowBranchSetting()
  local list = WindowToListBox(gUI.GuildM.Branch:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  _GuildM_ReCreateBranchList(list)
  gUI.GuildM.Branch:SetProperty("Visible", "true")
end
function UI_GuildM_ModifyAnounce()
  gUI.GuildM.ModifyAnnonceRoot:SetProperty("Visible", "true")
  local editbox = WindowSys_Instance:GetWindow("GuildAnounce.Manifesto_bg.Picture1_bg.Picture1_bg.EditBox_ebox")
  local text = GetGuildRecruitBoard()
  editbox:SetProperty("Text", text)
  WindowSys_Instance:SetFocusWindow(editbox)
  WindowSys_Instance:SetKeyboardCaptureWindow(editbox)
end
function UI_GuildM_CloseModifyAnounce()
  gUI.GuildM.ModifyAnnonceRoot:SetProperty("Visible", "false")
  Lua_Fri_SetFoucsToWorld()
end
function UI_GuildM_DoModifyAnounce()
  local editbox = WindowSys_Instance:GetWindow("GuildAnounce.Manifesto_bg.Picture1_bg.Picture1_bg.EditBox_ebox")
  local text = editbox:GetProperty("Text")
  DoModifyGuildAnounce(text)
  UI_GuildM_CloseModifyAnounce()
end
function UI_GuildM_KickMember()
  local list = WindowToListBox(gUI.GuildM.Main:GetGrandChild("GuildMember_bg.GuildImage_bg.MemberList_lbox"))
  local item = list:GetSelectedItem()
  if not item then
    ShowErrorMessage("请选择需要开除的成员")
    return
  end
  local guid = item:GetUserData64()
  local _, _, name = string.find(item:GetColumnText(0), "([^%s]+)")
  Lua_GuildM_KickMember(guid, name)
end
function UI_GuildM_LeaderAlter()
  local list = WindowToListBox(gUI.GuildM.Main:GetGrandChild("GuildMember_bg.GuildImage_bg.MemberList_lbox"))
  local item = list:GetSelectedItem()
  if not item then
    ShowErrorMessage("请选择需要转让的成员")
    return
  end
  if item then
    local _, _, target_name = string.find(item:GetColumnText(0), "([^%s]+)")
    local target_guid = item:GetUserData64()
    Lua_GuildM_LeaderAlter(target_guid, target_name)
  end
end
function UI_GuildM_Disband()
  local _, guid_name, _, _, _, _, _, _, dismiss_time = GetGuildBaseInfo()
  Messagebox_Show("GUILD_DISMISS", guid_name, dismiss_time, dismiss_time > 0)
end
function UI_GuildM_SelfQuit()
  local my_pos, my_branch, isLeader = GetGuildAuthority()
  if isLeader then
    Lua_Chat_ShowOSD("GUILD_LEADER_QUIT")
  else
    local _, guild_name, _, _, _, currentPop, _, _, dismiss_time = GetGuildBaseInfo()
    Messagebox_Show("GUILD_QUIT", guild_name)
  end
end
function UI_GuildM_ChangeNotice()
  local label = gUI.GuildM.Main:GetGrandChild("GuildAllInfo_bg.Right_bg.Up_bg.Affiche_dlab")
  local text = label:GetProperty("Text")
  local ebox = WindowToEditBox(gUI.GuildM.Notice:GetGrandChild("Image_bg.SearchGuild_ebox"))
  ebox:SetProperty("Text", text)
  gUI.GuildM.Notice:SetProperty("Visible", "true")
end
function UI_GuildM_ViewOffLineCheck()
  _GuildM_MemberListUpdate()
end
function UI_GuildM_ShowMain(msg)
  gUI.GuildM.SortIndex = 2
  gUI.GuildM.SortFlag[gUI.GuildM.SortIndex + 1] = 1
  local guildID, _, _, level = GetGuildBaseInfo()
  if gUI.GuildM.Main:IsVisible() then
    gUI.GuildM.Main:SetProperty("Visible", "false")
  else
    gUI.GuildM.Main:SetProperty("Visible", "true")
    if level >= 1 and gUI.GuildM.IsGuildInfoReady then
      local selected = gUI.GuildM.tableCtrl:GetSelectItem()
      _GuildM_CtrlShow(selected)
      _GuildM_UpdateGuildInfoBySelected(selected)
    else
      if level >= 1 and gUI.GuildM.IsGuildInfoReady == false then
        RequestGuildInfo()
      end
      _GuildM_CtrlShow(-1)
      local wnd = WindowSys_Instance:GetWindow("GuildMain.GuildBase_bg.Create_bg.Refer_bg.hint_pg")
      local wnd2 = WindowSys_Instance:GetWindow("GuildMain.GuildBase_bg.Create_bg.Finish_bg.hint_bg")
      local referbtn = WindowSys_Instance:GetWindow("GuildMain.GuildBase_bg.Create_bg.Refer_bg.Refer_btn")
      local guild_id, _, _, guild_level, _, count_now, _, count_limit = GetGuildBaseInfo()
      if level == 0 and guildID ~= gUI_INVALIDE then
        wnd:SetProperty("Visible", "true")
        referbtn:SetProperty("Enable", "true")
        if count_now == count_limit then
          wnd2:SetProperty("Visible", "true")
          wnd:SetProperty("Visible", "false")
        else
          wnd2:SetProperty("Visible", "false")
        end
      else
        wnd:SetProperty("Visible", "false")
        referbtn:SetProperty("Enable", "false")
        wnd2:SetProperty("Visible", "false")
      end
    end
  end
end
function UI_GuildM_ApplyRequestList()
  local list = WindowToListBox(gUI.GuildM.RequestList:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  local item = list:GetSelectedItem()
  if not item then
    ShowErrorMessage("需要先选择一个玩家")
    return
  end
  local guid = item:GetUserData64()
  AcceptGuildApply(guid)
end
function UI_GuildM_CancelRequestList()
  local list = WindowToListBox(gUI.GuildM.RequestList:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  local item = list:GetSelectedItem()
  if not item then
    ShowErrorMessage("需要先选择一个玩家")
    return
  end
  local guid = item:GetUserData64()
  local result = RejectGuildApply(guid)
  if true == result then
    local index = list:GetFirstSelected()
    list:RemoveItem(index)
  end
end
function UI_GuildM_CancelAllRequestList()
  local list = WindowToListBox(gUI.GuildM.RequestList:GetGrandChild("GuildImage_bg.MemberList_lbox"))
  local count = list:GetItemCount()
  for i = 0, count - 1 do
    local guid = list:GetItemData64(i)
    if guid ~= nil then
      local result = RejectGuildApply(guid)
    end
  end
  list:RemoveAllItems()
end
function UI_GuildM_ConfirmAddMember()
  local ebox = WindowToEditBox(gUI.GuildM.AddMember:GetGrandChild("Image_bg.UserName_ebox"))
  local text = ebox:GetProperty("Text")
  if gUI.GuildM.IsAddMemberChar == 1 then
    RecruitGuildMember(text)
  elseif gUI.GuildM.IsAddMemberChar == 2 then
    Lua_GuildS_ConfirmAddMemberAlliance(text)
  elseif gUI.GuildM.IsAddMemberChar == 3 then
    Lua_GuildS_ConfirmAddMemberHostile(text)
  end
  UI_GuildM_CloseAddMember()
end
function UI_GuildM_AdministerChooseChanged()
  local slot1 = gUI.GuildM.adm.position_combo:GetSelectItem()
  local slot2 = gUI.GuildM.adm.branch_combo:GetSelectItem()
  local pos = slot1 ~= -1 and gUI.GuildM.adm.position_combo:GetItemData(slot1) - 1 or -1
  local branch = slot2 ~= -1 and gUI.GuildM.adm.branch_combo:GetItemData(slot2) - 1 or -1
  local my_pos, my_branch = GetGuildAuthority()
  gUI.GuildM.adm.branch_combo:SetEnable(true)
  if pos == 2 or pos == 1 then
    if my_pos == gUI_GUILD_POSITION.minister and pos == 1 and branch == -1 then
      gUI.GuildM.adm.branch_combo:SetSelectItem(my_branch)
      gUI.GuildM.adm.branch_combo:SetEnable(false)
    elseif pos == 2 and branch == -1 then
      gUI.GuildM.adm.branch_combo:SetSelectItem(1)
    end
  else
    gUI.GuildM.adm.branch_combo:SetSelectItem(0)
    gUI.GuildM.adm.branch_combo:SetEnable(false)
  end
end
function UI_GuildM_ConfirmAdminister()
  local new_nickname = gUI.GuildM.adm.nname_edit:GetProperty("Text")
  if gUI.GuildM.adm.nname_edit:GetProperty("ReadOnly") == "false" then
    if gUI.GuildM.adm.old_nickname ~= new_nickname then
      if string.find(new_nickname, "%s") or WorldStage:isValidString(new_nickname) == 0 and new_nickname ~= "" then
        WorldGroup_ShowNewStr(UI_SYS_MSG.CHAT_INVALID)
      else
        ModifyGuildMemberNName(gUI.GuildM.adm.target_guid, new_nickname)
      end
    else
      ModifyGuildMemberNName(gUI.GuildM.adm.target_guid, new_nickname)
    end
  end
  local slot1 = gUI.GuildM.adm.position_combo:GetSelectItem()
  local slot2 = gUI.GuildM.adm.branch_combo:GetSelectItem()
  local pos = slot1 ~= -1 and gUI.GuildM.adm.position_combo:GetItemData(slot1) - 1 or -1
  local branch = slot2 ~= -1 and gUI.GuildM.adm.branch_combo:GetItemData(slot2) - 1 or -1
  if (pos == 2 or pos == 1) and branch == -1 then
    local str = string.format("请选择分堂！")
    Messagebox_Show("DEFAULT", str)
    return
  end
  ModifyGuildMemberPosition(gUI.GuildM.adm.target_guid, pos, branch)
  UI_GuildM_CloseAdminister()
end
function UI_GuildM_ConfirmNotice()
  local ebox = WindowToEditBox(gUI.GuildM.Notice:GetGrandChild("Image_bg.SearchGuild_ebox"))
  local text = ebox:GetProperty("Text")
  ReviseGuildNotice(text)
  _GuildM_CloseNotice()
end
function UI_GuildM_ConfirmSaveBranch()
  _GuildM_SaveBranch()
end
function UI_GuildM_ConfirmBranch()
  _GuildM_SaveBranch()
  _GuildM_CloseBranch()
end
function UI_GuildM_SortMember(msg)
  local button = WindowToButton(msg:get_window())
  local sort_index = tonumber(button:GetCustomUserData(0))
  gUI.GuildM.SortIndex = sort_index
  if gUI.GuildM.SortFlag[gUI.GuildM.SortIndex + 1] == 0 then
    gUI.GuildM.SortFlag[gUI.GuildM.SortIndex + 1] = 1
  else
    gUI.GuildM.SortFlag[gUI.GuildM.SortIndex + 1] = 0
  end
  _GuildM_MemberListUpdate()
end
function UI_GuildM_UseInviteItemBtn()
  local name = gUI.GuildM.InviteItemEditBox:GetProperty("Text")
  if name == "" then
    Lua_Chat_ShowOSD("GUILD_INVITE_NO_NAME")
    return
  end
  UseInviteItem(gUI.GuildM.InviteItemGuid, name)
  UI_GuildM_CancelInviteItemBtn()
end
function UI_GuildM_CancelInviteItemBtn()
  Lua_Fri_SetFoucsToWorld()
  gUI.GuildM.InviteItemUI:SetVisible(false)
end
function UI_GuildM_ShowOrisonTips(msg)
  local win = msg:get_window()
  local boxName = win:GetProperty("WindowName")
  local tooltip = win:GetToolTipWnd(0)
  Lua_Tip_Item(tooltip, nil, nil, nil, nil, GUILD_ORISON)
end
function _OnGuildM_InviteItemPass(guid)
  Messagebox_Show("GUILD_USE_INVITE_ITEM", guid)
end
function _OnGuild_Show_Shop_Reset(id)
  if IsGuidlShopCanReset() then
    Messagebox_Show("GUILD_SHOW_SHOPRESET", id)
  end
end
function _OnGuildM_InviteItem(itemGuid)
  gUI.GuildM.InviteItemUI:SetVisible(true)
  gUI.GuildM.InviteItemEditBox:SetProperty("Text", "")
  WindowSys_Instance:SetKeyboardCaptureWindow(gUI.GuildM.InviteItemEditBox)
  gUI.GuildM.InviteItemGuid = itemGuid
end
function _OnGuildM_New_Apply(strApplyName)
  Lua_Chat_ShowSysLog("GUILD_RECEIVE_APPLY", strApplyName)
  local wnd = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg.Guild_btn")
  wnd:SetProperty("NormalImage", "UiBtn012:Guild_h")
end
function _OnGuildM_Close_Orison()
  if gUI.GuildM.Orison:IsVisible() then
    gUI.GuildM.Orison:SetVisible(false)
  end
end
function _OnGuildM_Show_Orison()
  if not gUI.GuildM.Orison:IsVisible() then
    gUI.GuildM.Orison:SetVisible(true)
    _GuildM_UpdateOrisonInfo()
    gUI.GuildM.OrisonEdit:SetProperty("Text", "1")
    WindowSys_Instance:SetKeyboardCaptureWindow(gUI.GuildM.OrisonEdit)
  end
end
function _OnGuildM_Orison_Result()
  Lua_Chat_ShowOSD("ORISON_SUCC")
  if gUI.GuildM.Orison:IsVisible() then
    _GuildM_UpdateOrisonInfo()
  end
end
function _OnGuildM_Orison_UpdateItem()
  if gUI.GuildM.Orison:IsVisible() then
    _GuildM_UpdateOrisonInfo()
  end
end
function _OnGuildM_Show_GetMoney(update)
  if update then
    if not gUI.GuildM.GetMoney:IsVisible() then
      return
    end
  else
    gUI.GuildM.GetMoney:SetVisible(true)
  end
  local _, _, _, lv, money, _, _, _, _, _, DailyMoney = GetGuildBaseInfo()
  if lv < 2 then
    return
  end
  gUI.GuildM.GetMoney:SetVisible(true)
  local currentMoneyWnd = WindowSys_Instance:GetWindow("GuildExtract.GuildExtract_bg.Extract_bg.Present_dlab")
  currentMoneyWnd:SetProperty("Text", Lua_UI_Money2String(money * 10000))
  local canMoneyWnd = WindowSys_Instance:GetWindow("GuildExtract.GuildExtract_bg.Extract1_bg.Present_dlab")
  local canGet = _GuildM_DailyMoney[lv] - DailyMoney
  canMoneyWnd:SetProperty("Text", Lua_UI_Money2String(canGet * 10000))
  local getMoneyWnd = WindowSys_Instance:GetWindow("GuildExtract.GuildExtract_bg.Money_bg.Money_bg.Money_ebox")
  getMoneyWnd:SetProperty("Text", "0")
end
function _OnGuildM_UpdateLog()
  if gUI.GuildM.InfoUI:IsVisible() then
    if _GuildM_CurrSelType == 2 then
      UI_GuildM_ShowGuildMoneyLog()
    elseif _GuildM_CurrSelType == 4 then
      UI_GuildM_ShowGuildItemLog()
    end
  end
end
function _GuildM_Guide_SetItemNilByGid(gid, list)
  for key, value in pairs(list) do
    if value and value.Gid == gid then
      value = nil
      return
    end
  end
end
function Guide_OnGuildAdd(keepSelect)
  gUI.GuildM.GuildInvite:SetVisible(true)
  local oldSelect, ScrollBarPos, ScrollBar
  if keepSelect == true then
    oldSelect = gUI.GuildM.GuildInviteList:GetSelectedItemIndex()
    ScrollBar = WindowToScrollBar(gUI.GuildM.GuildInviteList:GetChildByName("vertSB_"))
    if ScrollBar:IsVisible() then
      ScrollBarPos = ScrollBar:GetCurrentPos()
    end
  end
  local oldCount = gUI.GuildM.GuildInviteList:GetItemCount()
  gUI.GuildM.GuildInviteList:RemoveAllItems()
  for key, value in pairs(_Guild_InviteList) do
    if value then
      local str = string.format("%s|%s|%d|%d", value.GuildName, value.LeaderName, value.GuildLev, value.GuildGrowPoint)
      local item = gUI.GuildM.GuildInviteList:InsertString(str, -1)
      item:SetUserData64Ex(value.Gid)
      item:SetUserData64(value.LeaderGuid)
    end
  end
  local newCount = gUI.GuildM.GuildInviteList:GetItemCount()
  if keepSelect == true and oldSelect >= 0 then
    gUI.GuildM.GuildInviteList:SetSelectItem(oldSelect + newCount - oldCount)
    if ScrollBarPos and ScrollBar and ScrollBar:IsVisible() then
      ScrollBar:SetCurrentPos(ScrollBarPos)
    end
  else
    gUI.GuildM.GuildInviteList:SetSelectItem(0)
  end
end
function UI_GuildM_Guide_Add_Close()
  gUI.GuildM.GuildInvite:SetVisible(false)
end
function UI_GuildM_Guide_Add_Refuse()
  local selItem = gUI.GuildM.GuildInviteList:GetSelectedItem()
  if selItem then
    local guid = selItem:GetUserData64()
    local gid = selItem:GetUserData64Ex()
    ReplyRecruitment(gid, guid, false)
    gUI.GuildM.GuildInviteList:RemoveItemByData64(guid)
    _GuildM_Guide_SetItemNilByGid(gid, _Guild_InviteList)
  end
  if gUI.GuildM.GuildInviteList:GetItemCount() == 0 then
    _Guild_InviteList = {}
    UI_GuildM_Guide_Add_Close()
    DeleteHint(USER_GUIDE_ADD_UI)
  end
end
function UI_GuildM_Guide_Add_AllRefuse()
  for key, value in pairs(_Guild_InviteList) do
    if value then
      ReplyRecruitment(value.Gid, value.LeaderGuid, false)
    end
  end
  gUI.GuildM.GuildInviteList:RemoveAllItems()
  _Guild_InviteList = {}
  UI_GuildM_Guide_Add_Close()
  DeleteHint(USER_GUIDE_ADD_UI)
end
function UI_GuildM_Guide_Add_Accept()
  local selItem = gUI.GuildM.GuildInviteList:GetSelectedItem()
  if selItem then
    local guid = selItem:GetUserData64()
    local gid = selItem:GetUserData64Ex()
    ReplyRecruitment(gid, guid, true)
    gUI.GuildM.GuildInviteList:RemoveAllItems()
    _Guild_InviteList = {}
    UI_GuildM_Guide_Add_Close()
    DeleteHint(USER_GUIDE_ADD_UI)
  end
end
function UI_GuildM_ShowGuildMoneyLog()
  _GuildM_CurrSelType = 2
  _OnGuildM_ChooseCertainInfo()
  gUI.GuildM.ListDisplay:ClearAllContent()
  local logCount = GetGMoneyLogCount()
  if logCount == nil or logCount <= 0 then
    return
  end
  local minCount = 0
  if logCount > _GuildM_MaxMoneyLogCount then
    minCount = logCount - _GuildM_MaxMoneyLogCount
  end
  for i = logCount - 1, minCount, -1 do
    local typeId, subTypeId, num, timeStr, guid, name, qustName = GetGMoneyLog(i)
    if typeId then
      local money = tonumber(num) * 10000
      local subName = ""
      if typeId == 0 or typeId == 6 or typeId == 5 or typeId == 7 or typeId == 9 then
        subName = name
      elseif typeId == 1 then
        subName = gUI_BuildName[subTypeId]
      elseif typeId == 2 or typeId == 3 then
        subName = gUI_GuildM_VEL_STR_MAP[subTypeId]
      elseif typeId == 4 then
        subName = gUI_GuildM_ITEM_STR_MAP[subTypeId]
      end
      local strLog = ""
      local strType = _GuildM_TypeStrMap[typeId]
      if strType == nil then
        strType = "[%s]unknow log.."
      end
      if typeId == 6 then
        strLog = string.format(strType, timeStr, subName, subTypeId)
        strLog = strLog .. Lua_UI_Money2String(money)
      elseif typeId == 9 then
        strLog = string.format(strType, timeStr, subName, qustName)
        strLog = strLog .. Lua_UI_Money2String(money)
      elseif typeId == 5 then
        strLog = string.format(strType, timeStr)
        strLog = strLog .. Lua_UI_Money2String(money)
      else
        strLog = string.format(strType, timeStr, subName)
        strLog = strLog .. Lua_UI_Money2String(money)
      end
      gUI.GuildM.ListDisplay:InsertBack(strLog, 0, 0, 0)
    end
  end
  gUI.GuildM.ListDisplay:MoveToTop()
end
function UI_GuildM_ShowGuildMon()
  _GuildM_CurrSelType = 3
  _OnGuildM_ChooseCertainInfo()
  local monsterNameMap = {
    [1] = "初级守护兽(1级)",
    [2] = "中级级守护兽(2级)",
    [3] = "高级级守护兽(3级)",
    [4] = "超级守护兽(4级)",
    [5] = "神级守护兽(5级)"
  }
  local monsterAward = {
    [1] = {Item = 390054, Count = 20},
    [2] = {Item = 390055, Count = 30},
    [3] = {Item = 390056, Count = 40},
    [4] = {Item = 390057, Count = 45},
    [5] = {Item = 390058, Count = 50}
  }
  local currentLv, curPractive, guildmoney, BossConsume = GetGuildBossDate()
  for i, n in pairs(BossConsume) do
    local levWnd = gUI.GuildM.Mon:GetChildByName("Level" .. tostring(i) .. "_bg")
    local wnd1 = levWnd:GetChildByName("Level_dlab")
    wnd1:SetProperty("Text", monsterNameMap[i])
    local ItemIcon = GetItemIconFromItemID(monsterAward[i].Item)
    if ItemIcon ~= nil then
      local ItemGoodsBox = WindowToGoodsBox(levWnd:GetChildByName("Reward_gbox"))
      ItemGoodsBox:SetItemGoods(0, ItemIcon, -1)
      ItemGoodsBox:SetItemData(0, monsterAward[i].Item)
      wnd1 = levWnd:GetChildByName("RequireLevel_dlab")
      wnd1:SetProperty("Text", "*" .. tostring(monsterAward[i].Count))
    else
      wnd1 = levWnd:GetChildByName("RequireLevel_dlab")
      wnd1:SetProperty("Text", "")
    end
    wnd1 = levWnd:GetChildByName("Lev_dlab")
    if i <= currentLv then
      wnd1:SetVisible(false)
    else
      wnd1:SetVisible(true)
      wnd1:SetProperty("Text", "{#R^需要育灵树等级：" .. tostring(i) .. "级}")
    end
    wnd1 = levWnd:GetChildByName("Money_dlab")
    local wnd2 = levWnd:GetChildByName("Money_slab")
    local needMoney = n.Money * 10000
    if guildmoney >= n.Money then
      wnd1:SetProperty("Text", Lua_UI_Money2String(needMoney))
      wnd2:SetProperty("FontColor", colorWhite)
    else
      wnd1:SetProperty("Text", Lua_UI_Money2String(needMoney, false, false, false, false, nil, nil, colorRed))
      wnd2:SetProperty("FontColor", colorRed)
    end
  end
end
function UI_GuildM_ShowGuildItemLog()
  _GuildM_CurrSelType = 4
  _OnGuildM_ChooseCertainInfo()
  gUI.GuildM.ListDisplay:ClearAllContent()
  local logCount = GetGBankLogCount()
  if logCount == nil or logCount <= 0 then
    return
  end
  local minCount = 0
  if logCount > _GuildM_MaxBankLogCount then
    minCount = logCount - _GuildM_MaxBankLogCount
  end
  for i = logCount - 1, minCount, -1 do
    local itemName, guid, playName, count, operateType, timeStr = GetGBankLog(i)
    if itemName then
      local typeStr
      if operateType == 0 then
        typeStr = "{#G^存入}"
      else
        typeStr = "{#R^取出}"
      end
      local strLog = string.format("[%s]%s%s%s*%d", timeStr, playName, typeStr, itemName, count)
      gUI.GuildM.ListDisplay:InsertBack(strLog, 0, 0, 0)
    end
  end
  gUI.GuildM.ListDisplay:MoveToTop()
end
function UI_GuildM_ShowGuildInfo()
  _GuildM_CurrSelType = 1
  _OnGuildM_ChooseCertainInfo()
  _GuildM_ShowGuildInfo()
end
function UI_GuildM_GotoNpc()
  TalkToNpc(5, 50)
end
function UI_GuildM_EditboxEnter(msg)
  local ebox = WindowToEditBox(gUI.GuildM.Notice:GetGrandChild("Image_bg.SearchGuild_ebox"))
  local text = ebox:GetProperty("Text")
  ebox:SetProperty("Text", text .. "\n")
end
function UI_GuildM_AnounceEditboxEnter(msg)
  local ebox = WindowToEditBox(WindowSys_Instance:GetWindow("GuildAnounce.Manifesto_bg.Picture1_bg.Picture1_bg.EditBox_ebox"))
  local text = ebox:GetProperty("Text")
  ebox:SetProperty("Text", text .. "\n")
end
function UI_GuildM_RecruitMemberShow(msg)
  Lua_GuildC_RecruitMemberShow(true)
end
function _RecruitTimerCallback()
  local nCount = 0
  for key, value in pairs(_Guild_RecruitList) do
    if value then
      value.ResTime = value.ResTime - 1000
      if 0 >= value.ResTime then
        local guid = value.LeaderGuid
        local gid = value.Gid
        ReplyRecruitment(gid, guid, false)
        gUI.GuildM.GuildRecruitList:RemoveItemByData64(guid)
        value = nil
      else
        local item = gUI.GuildM.GuildRecruitList:FindItemByData64Ex(value.Gid)
        if item then
          local strResTime = Lua_UI_DetailTime(value.ResTime / 1000)
          item:SetColumnText(strResTime, 2)
        end
        nCount = nCount + 1
      end
    end
  end
  if nCount == 0 then
    DelTimerEvent(1, "RecruitTimer")
  end
end
function Guide_OnGuildRecruit(keepSelect)
  gUI.GuildM.GuildRecruit:SetVisible(true)
  local oldSelect, ScrollBarPos, ScrollBar
  if keepSelect == true then
    oldSelect = gUI.GuildM.GuildRecruitList:GetSelectedItemIndex()
    ScrollBar = WindowToScrollBar(gUI.GuildM.GuildRecruitList:GetChildByName("vertSB_"))
    if ScrollBar:IsVisible() then
      ScrollBarPos = ScrollBar:GetCurrentPos()
    end
  end
  local oldCount = gUI.GuildM.GuildRecruitList:GetItemCount()
  gUI.GuildM.GuildRecruitList:RemoveAllItems()
  for key, value in pairs(_Guild_RecruitList) do
    if value then
      local strNum = string.format("%d/10", value.GuildNum)
      local strResTime = Lua_UI_DetailTime(value.ResTime / 1000)
      local str = string.format("%s|%s|%s|%s", value.GuildName, value.LeaderName, strResTime, strNum)
      local item = gUI.GuildM.GuildRecruitList:InsertString(str, -1)
      item:SetUserData(value.Gid)
      item:SetUserData64(value.LeaderGuid)
    end
  end
  local newCount = gUI.GuildM.GuildRecruitList:GetItemCount()
  if keepSelect == true and oldSelect >= 0 then
    gUI.GuildM.GuildRecruitList:SetSelectItem(oldSelect + newCount - oldCount)
    if ScrollBarPos and ScrollBar and ScrollBar:IsVisible() then
      ScrollBar:SetCurrentPos(ScrollBarPos)
    end
  else
    gUI.GuildM.GuildRecruitList:SetSelectItem(0)
  end
end
function UI_GuildM_Guide_Recruit_Close()
  gUI.GuildM.GuildRecruit:SetVisible(false)
end
function UI_GuildM_Guide_Recruit_Refuse()
  local selItem = gUI.GuildM.GuildRecruitList:GetSelectedItem()
  if selItem then
    local guid = selItem:GetUserData64()
    local gid = selItem:GetUserData64Ex()
    ReplyRecruitment(gid, guid, false)
    gUI.GuildM.GuildRecruitList:RemoveItemByData64(guid)
    _GuildM_Guide_SetItemNilByGid(gid, _Guild_RecruitList)
  end
  if gUI.GuildM.GuildRecruitList:GetItemCount() == 0 then
    _Guild_RecruitList = {}
    UI_GuildM_Guide_Recruit_Close()
    DeleteHint(USER_GUIDE_RECRUIT_UI)
  end
end
function UI_GuildM_Guide_Recruit_AllRefuse()
  for key, value in pairs(_Guild_RecruitList) do
    if value then
      ReplyRecruitment(value.Gid, value.LeaderGuid, false)
    end
  end
  gUI.GuildM.GuildRecruitList:RemoveAllItems()
  _Guild_RecruitList = {}
  UI_GuildM_Guide_Recruit_Close()
  DeleteHint(USER_GUIDE_RECRUIT_UI)
end
function UI_GuildM_Guide_Recruit_Accept()
  local selItem = gUI.GuildM.GuildRecruitList:GetSelectedItem()
  if selItem then
    local guid = selItem:GetUserData64()
    local gid = selItem:GetUserData64Ex()
    ReplyRecruitment(gid, guid, true)
    gUI.GuildM.GuildRecruitList:RemoveAllItems()
    _Guild_RecruitList = {}
    UI_GuildM_Guide_Recruit_Close()
    DeleteHint(USER_GUIDE_RECRUIT_UI)
  end
end
function _OnGuild_Guide_HintUpdate(guideID)
  if guideID == USER_GUIDE_ADD_UI and gUI.GuildM.GuildInvite:IsVisible() then
    Guide_OnGuildAdd(true)
  elseif guideID == USER_GUIDE_RECRUIT_UI and gUI.GuildM.GuildRecruit:IsVisible() then
    Guide_OnGuildRecruit(true)
  end
end
function UI_GuildM_Guide_ShowItemTip(msg)
  local wnd = msg:get_window()
  wnd = WindowToGoodsBox(wnd)
  local index = msg:get_wparam()
  local id = wnd:GetItemData(index)
  if id ~= 0 then
    Lua_Tip_Item(wnd:GetToolTipWnd(0), nil, nil, nil, nil, id)
  end
end
function UI_GuildM_GetMoneyChange(msg)
  local wnd = msg:get_window()
  local text = wnd:GetProperty("Text")
  if text == "" then
    wnd:SetProperty("Text", "0")
    return
  end
  local _, _, _, lv, money, _, _, _, _, _, DailyMoney = GetGuildBaseInfo()
  local glod = tonumber(text)
  local DailyMoney = _GuildM_DailyMoney[lv] - DailyMoney
  local canGetMoney = money
  if money > DailyMoney then
    canGetMoney = DailyMoney
  end
  if glod > canGetMoney then
    wnd:SetProperty("Text", tostring(canGetMoney))
  end
end
function UI_GuildM_CloseGetMoney(msg)
  gUI.GuildM.GetMoney:SetVisible(false)
end
function UI_GuildM_GetMoneyClick(msg)
  local getMoneyWnd = WindowSys_Instance:GetWindow("GuildExtract.GuildExtract_bg.Money_bg.Money_bg.Money_ebox")
  local money = tonumber(getMoneyWnd:GetProperty("Text"))
  if money > 0 then
    local _, maxMoney = GetPlaySelfProp(7)
    local _, _, bagMoney = GetBankAndBagMoney()
    local editMoney = money * 10000
    if maxMoney < editMoney + bagMoney then
      ShowErrorMessage("提取的资金超过你的金币携带上限")
    else
      GetGuildMoney(money)
    end
  end
end
function UI_GuildM_CloseOrison()
  if gUI.GuildM.Orison:IsVisible() then
    gUI.GuildM.Orison:SetVisible(false)
  end
end
function UI_GuildM_AskOrison()
  local num = gUI.GuildM.OrisonEdit:GetProperty("Text")
  AskOrison(GUILD_ORISON, num)
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function Script_GuildM_OnLoad()
  Script_GuildS_OnLoad()
  Script_GuildQ_OnLoad()
  gUI.GuildM.IsGuildInfoReady = false
  gUI.GuildM.IsGuildMenberListReady = false
  gUI.GuildM.SortIndex = 2
  gUI.GuildM.AddMember = WindowSys_Instance:GetWindow("GuildAdd")
  gUI.GuildM.RequestList = WindowSys_Instance:GetWindow("GuildRequestList")
  gUI.GuildM.Administer = WindowSys_Instance:GetWindow("GuildAdminister")
  gUI.GuildM.Branch = WindowSys_Instance:GetWindow("GuildUnit")
  gUI.GuildM.Notice = WindowSys_Instance:GetWindow("GuildBoard")
  gUI.GuildM.Main = WindowSys_Instance:GetWindow("GuildMain")
  gUI.GuildM.Welcome = WindowSys_Instance:GetWindow("GuildMain.GuildBase_bg")
  gUI.GuildM.ContributeRoot = WindowSys_Instance:GetWindow("GuildContribute")
  gUI.GuildM.Main:SetProperty("Visible", "false")
  gUI.GuildM.ViewButton = WindowToCheckButton(gUI.GuildM.Main:GetGrandChild("GuildMember_bg.BasicInfo_bg.ShowFallOut_cbtn"))
  gUI.GuildM.AuthorityButton = {}
  local authority = 8
  for i, name in ipairs({
    "Attorn_btn",
    "Administer_btn",
    "Request_btn",
    "Setting_btn",
    "FireOut_btn",
    "AddMember_btn",
    "Quit_btn"
  }) do
    gUI.GuildM.AuthorityButton[i] = gUI.GuildM.Main:GetGrandChild("GuildMember_bg.Down_bg." .. name)
  end
  gUI.GuildM.AuthorityButton[authority] = gUI.GuildM.Main:GetGrandChild("GuildAllInfo_bg.Right_bg.Up_bg.Change_btn")
  gUI.GuildM.SortFlag = {
    0,
    0,
    1,
    0,
    0,
    1,
    0,
    1
  }
  gUI.GuildM.adm = {}
  gUI.GuildM.ModifyAnnonceRoot = WindowSys_Instance:GetWindow("GuildAnounce")
  gUI.GuildM.adm.position_combo = WindowToComboBox(gUI.GuildM.Administer:GetGrandChild("Image2_bg.UserJob_dlab"))
  gUI.GuildM.adm.nname_edit = WindowToEditBox(gUI.GuildM.Administer:GetGrandChild("Image3_bg.NickName_ebox"))
  gUI.GuildM.adm.branch_combo = WindowToComboBox(gUI.GuildM.Administer:GetGrandChild("Image4_bg.Branch_dlab"))
  gUI.GuildM.adm.name_lab = gUI.GuildM.Administer:GetChildByName("UserName_dlab")
  gUI.GuildM.adm.level_lab = gUI.GuildM.Administer:GetChildByName("Level_dlab")
  gUI.GuildM.adm.pro_lab = gUI.GuildM.Administer:GetChildByName("UserMetier_dlab")
  gUI.GuildM.AllInfoUI = WindowSys_Instance:GetWindow("GuildMain.GuildAllInfo_bg")
  gUI.GuildM.MemberUI = WindowSys_Instance:GetWindow("GuildMain.GuildMember_bg")
  gUI.GuildM.InfoUI = WindowSys_Instance:GetWindow("GuildMain.GuildInfo_bg")
  gUI.GuildM.InfoMain = WindowSys_Instance:GetWindow("GuildMain.GuildInfo_bg.Right_bg.Inf_bg")
  gUI.GuildM.InfoCon = WindowToContainer(WindowSys_Instance:GetWindow("GuildMain.GuildInfo_bg.Right_bg.Inf_bg.Inf_cnt"))
  gUI.GuildM.InfoBase = WindowToListBox(WindowSys_Instance:GetWindow("GuildMain.GuildInfo_bg.Right_bg.Inf_bg.Inf_cnt.Ji_lbox"))
  gUI.GuildM.InfoHome = WindowToListBox(WindowSys_Instance:GetWindow("GuildMain.GuildInfo_bg.Right_bg.Inf_bg.Inf_cnt.Yuan_lbox"))
  gUI.GuildM.ListDisplay = WindowToDisplayBox(WindowSys_Instance:GetWindow("GuildMain.GuildInfo_bg.Right_bg.ZiJin_lbox"))
  gUI.GuildM.Mon = WindowSys_Instance:GetWindow("GuildMain.GuildInfo_bg.Right_bg.ShouHu_bg")
  gUI.GuildM.WarUI = WindowSys_Instance:GetWindow("GuildMain.GuildDip_bg")
  gUI.GuildM.ContributeEditBox = WindowToEditBox(WindowSys_Instance:GetWindow("GuildContribute.EditBox_ebox"))
  gUI.GuildM.tableCtrl = WindowToTableCtrl(WindowSys_Instance:GetWindow("GuildMain.Top_bg.Change_cbtn"))
  gUI.GuildM.tableCtrl:SelectItem(0)
  gUI.GuildM.adm.position_name = gUI_GUILD_POSITION_NAME
  gUI.GuildM.adm.position = gUI_GUILD_POSITION
  gUI.GuildM.adm.target_guid = -1
  gUI.GuildM.adm.target_pos = -1
  gUI.GuildM.adm.target_branch = -1
  gUI.GuildM.adm.permission = {
    [3] = {
      gUI_GUILD_POSITION.member,
      gUI_GUILD_POSITION.elite
    },
    [4] = {
      gUI_GUILD_POSITION.member,
      gUI_GUILD_POSITION.elite,
      gUI_GUILD_POSITION.minister
    },
    [5] = {
      gUI_GUILD_POSITION.member,
      gUI_GUILD_POSITION.elite,
      gUI_GUILD_POSITION.minister,
      gUI_GUILD_POSITION.elder
    },
    [6] = {
      gUI_GUILD_POSITION.member,
      gUI_GUILD_POSITION.elite,
      gUI_GUILD_POSITION.minister,
      gUI_GUILD_POSITION.elder,
      gUI_GUILD_POSITION.viceleader,
      gUI_GUILD_POSITION.leader
    }
  }
  gUI.GuildM.adm.position_option = {
    [3] = {
      gUI_GUILD_POSITION.member,
      gUI_GUILD_POSITION.elite
    },
    [4] = {
      gUI_GUILD_POSITION.member,
      gUI_GUILD_POSITION.minister,
      gUI_GUILD_POSITION.elite
    },
    [5] = {
      gUI_GUILD_POSITION.member,
      gUI_GUILD_POSITION.elite,
      gUI_GUILD_POSITION.minister,
      gUI_GUILD_POSITION.elder
    },
    [6] = {
      gUI_GUILD_POSITION.member,
      gUI_GUILD_POSITION.elite,
      gUI_GUILD_POSITION.minister,
      gUI_GUILD_POSITION.elder,
      gUI_GUILD_POSITION.viceleader
    }
  }
  _GuildM_CtrlShow(-1)
  gUI.GuildM.InviteItemUI = WindowSys_Instance:GetWindow("Detail.ItemZhaoxian")
  gUI.GuildM.InviteItemEditBox = WindowToEditBox(WindowSys_Instance:GetWindow("Detail.ItemZhaoxian.Picture1_bg.Username_ebox"))
  gUI.GuildM.InviteItemGuid = 0
  gUI.GuildM.GetMoney = WindowSys_Instance:GetWindow("GuildExtract")
  _Guild_InviteList = {}
  _Guild_RecruitList = {}
  gUI.GuildM.GuildInvite = WindowSys_Instance:GetWindow("GuildInvite")
  gUI.GuildM.GuildInviteList = WindowToListBox(WindowSys_Instance:GetWindow("GuildInvite.List_bg.List_lbox"))
  gUI.GuildM.GuildRecruit = WindowSys_Instance:GetWindow("GuildRecruit")
  gUI.GuildM.GuildRecruitList = WindowToListBox(WindowSys_Instance:GetWindow("GuildRecruit.List_bg.List_lbox"))
  gUI.GuildM.Orison = WindowSys_Instance:GetWindow("GuildMainPray")
  gUI.GuildM.OrisonEdit = WindowToEditBox(WindowSys_Instance:GetWindow("GuildMainPray.GuildMainPray_bg.Image_bg.Count_bg.Count_ebox"))
end
function Script_GuildM_OnEvent(event)
  Script_GuildS_OnEvent(event)
  Script_GuildQ_OnEvent(event)
  if event == "GUILD_MEMBER_POSITION_UPDATE" then
    _OnGuildM_UpdatePosition(arg1, arg2)
  elseif event == "GUILD_BASE_INFO_READY" then
    _OnGuildM_ReadyBaseInfo(arg1, arg2)
  elseif event == "GUILD_BASE_INFO_UPDATE" then
    _OnGuildM_UpdateBaseInfo(arg1)
  elseif event == "GUILD_ADD_MEMBER" then
    _GuildM_MemberListAdd(arg1)
  elseif event == "GUILD_DEL_MEMBER" then
    _GuildM_MemberListDel(arg1)
  elseif event == "GUILD_BRANCH_NAME_UPDATE" then
    _GuildM_UpdateBranchList(arg1 + 1, arg2, arg3)
  elseif event == "GUILD_APPLICANT_UPDATE" then
    _OnGuildM_UpdateRequestListMember(arg1, arg2, arg3, arg4, arg5)
  elseif event == "GUILD_APPLICANT_AGREE_SUCC" then
    _OnGuildM_AgreeRequestListMember(arg1)
  elseif event == "GUILD_REPLY_RECRUIT" then
    _OnGuildM_RespondAddMember(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
  elseif event == "GUILD_DISMISS_SUCC" then
    _OnGuildM_DismissSucc(arg1, arg2)
  elseif event == "GUILD_CONTRIBUTE_SHOW" then
    _OnGuildM_ContributeShow()
  elseif event == "GUILD_BUILD_UPGRADE" then
    _OnGuildM_BuildUpdate(arg1, arg2)
  elseif event == "GUILD_USE_INVITE_ITEM_PASS" then
    _OnGuildM_InviteItemPass(arg1)
  elseif event == "GUILD_USE_INVITE_ITEM" then
    _OnGuildM_InviteItem(arg1)
  elseif event == "GUIDE_HINT_UPDATE_UI" then
    _OnGuild_Guide_HintUpdate(arg1)
  elseif event == "GUILD_WAR_RESULT_UPDATE" then
    _OnGuild_Guide_WarResult_Update()
  elseif event == "GUILD_SHOW_SHOP_RESET" then
    _OnGuild_Show_Shop_Reset(arg1)
  elseif event == "GUILD_NEW_APPLY" then
    _OnGuildM_New_Apply(arg1)
  elseif event == "GUILD_SHOW_GETMONEY" then
    _OnGuildM_Show_GetMoney(arg1)
  elseif event == "GUILD_BOSS_CALL_SHOW" then
    _OnGuildM_Call_Boss(arg1)
  elseif event == "GUILD_MEMBER_INFO_READY" then
    _OnGuildM_ReadyMemberInfo()
  elseif event == "GUILD_UPDATE_LOG" then
    _OnGuildM_UpdateLog()
  elseif event == "GUILD_SHOW_ORISON" then
    _OnGuildM_Show_Orison()
  elseif event == "GUILD_ORISON_RESULT" then
    _OnGuildM_Orison_Result()
  elseif event == "ITEM_ADD_TO_BAG" then
    _OnGuildM_Orison_UpdateItem()
  elseif "CLOSE_NPC_RELATIVE_DIALOG" == event then
    _OnGuildM_Close_Orison()
  end
end
function Script_GuildM_OnHide()
  _GuildM_CloseMain()
end
