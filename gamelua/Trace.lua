local times = 0
function trace(str)
  if not IsFinal() then
    if str == "" or str == nil then
      Lua_Chat_AddSysLog("------this is test-----")
      ShowErrorMessage("------this is test-----")
    else
      times = times + 1
      Lua_Chat_AddFightLog("Debug Info: " .. tostring(str) .. " times " .. tostring(times))
      ShowErrorMessage("DEBUG Info " .. str .. " times " .. tostring(times))
      SendChatMessage(gUI_SENDMSG_CHANNEL_MAP[1], "DEBUG Info " .. str .. " times " .. tostring(times), "")
    end
  end
  LUA_LOG(str)
end
function LUA_LOG(str)
  gFile = io.open("log/LUA_DEBUG_INFO.log", "a")
  local curTime = TimerSys_Instance:GetCurrTime()
  gFile:write("Time:" .. tostring(curTime) .. tostring(str) .. "\n")
  gFile:close()
end
function Debug_ClearLog()
  gFile = io.open("log/LUA_DEBUG_INFO.log", "w")
  local curTime = TimerSys_Instance:GetCurrTime()
  gFile:write("")
  gFile:close()
end
function DTrace(str)
  LUA_LOG("Debug Info: " .. str)
end
