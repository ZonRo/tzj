local _UI_Number_Str = gUI_CHINA_NUMBER
local _UI_Unit1 = {
  "千",
  "",
  "十",
  "百"
}
local _UI_Unit2 = {
  "",
  "万",
  "亿"
}
local _UI_Temp = {
  0,
  0,
  0,
  0
}
UNKOWNNUM = -1
gUI = {}
gUI_AddRMBWeb = "http://baidu.com"
g_strBackImage = "UiIamge004:Image_equipsign"
g_strStarImage = {
  [1] = "UiBtn001:Yellowstar_d",
  [2] = "UiBtn001:Yellowstar_h",
  [3] = "UiBtn001:Yellowstar_n"
}
gUI_INVALIDE = -1
gUI_CurrentStage = 1
gUI_CharType = {
  CHAR_MAIN_PLAYER = 0,
  CHAR_PLAYER = 1,
  CHAR_NPC = 2,
  CHAR_PET = 3,
  CHAR_VEHICLE = 4,
  CHAR_SPIRIT = 5,
  CHAR_FOLLOW_NPC = 6,
  CHAR_END = 7
}
gUI_MenPaiName = {
  [0] = "神将",
  [1] = "七杀",
  [2] = "灵霄",
  [3] = "沉香",
  [4] = "飞翎",
  [255] = "--"
}
gUI_MenPaiId = {
  ["神将"] = 0,
  ["七杀"] = 1,
  ["灵霄"] = 2,
  ["沉香"] = 3,
  ["飞翎"] = 4,
  ["--"] = 0
}
gUI_MenPaiColor = {
  [0] = "0xFFfc8034",
  [1] = "0xFF985af6",
  [2] = "0xFF4aa0f5",
  [3] = "0xFFf369d7",
  [4] = "0xFF56ce8c"
}
gUI_RANK = {
  Level = 1,
  Weapon = 2,
  Equip = 3,
  Pet = 4,
  Guild = 5,
  Wrath = 6,
  BGRward = 14
}
gUI_MenPaiID = {
  ZS = 0,
  CK = 1,
  FS = 2,
  MS = 3,
  GS = 4
}
ForgeLevel_To_Stars = {
  [0] = 0,
  [1] = 0,
  [2] = 0,
  [3] = 0,
  [4] = 21,
  [5] = 21,
  [6] = 21,
  [7] = 22,
  [8] = 23,
  [9] = 24,
  [10] = 25,
  [11] = 25,
  [12] = 25
}
OperateMode = {
  Error = -1,
  None = 0,
  Add = 1,
  Remove = 2,
  Cancel = 3
}
NpcFunction = {
  NPC_FUNC_NONE = 0,
  NPC_FUNC_CHAT = 1,
  NPC_FUNC_BANK = 2,
  NPC_FUNC_SHOP = 3,
  NPC_FUNC_TRANSMIT = 4,
  NPC_FUNC_GUILDSTORAGE = 5,
  NPC_FUNC_EQUIP_INTENSIFY = 9,
  NPC_FUNC_RESET_INSTANCE = 10,
  NPC_FUNC_MAIL_NORMAL = 11,
  NPC_FUNC_MAIL_PAY = 12,
  NPC_FUNC_CLEARPOLISH = 13,
  NPC_FUNC_GUARD_START = 14,
  NPC_FUNC_EQUIP_CHECK_IN = 15,
  NPC_FUNC_PET_COMPOUND = 16,
  NPC_FUNC_TRADE_SHOW = 17,
  NPC_FUNC_GAMESEND_CHAT = 18,
  NPC_FUNC_LEAVE_DYNAMICSCENE = 20,
  NPC_FUNC_REPEAT_QUEST = 22,
  NPC_FUNC_QUEST_CONTINUE = 27,
  NPC_FUNC_RANDOM_QUEST = 28,
  NPC_FUNC_GUILD_CREATE = 29,
  NPC_FUNC_GUILD_TERRITORY_CREATE = 30,
  NPC_FUNC_GUILD_APPLY = 31,
  NPC_FUNC_ITEM_MAKE = 32,
  NPC_FUNC_GUILD_SHOP_RESET = 33,
  NPC_FUNC_GUILD_ALLIANCE = 34,
  NPC_FUNC_GEM_EMBED = 35,
  NPC_FUNC_GEM_MOVEOUT = 36,
  NPC_FUNC_REWARD = 38,
  NPC_FUNC_YABIAO = 40,
  NPC_FUNC_PROPSCARD = 41,
  NPC_FUNC_GUILD_DONATE = 42,
  NPC_FUNC_QA = 43,
  NPC_FUNC_EQUIP_SMRITI = 44,
  NPC_FUNC_GUILD_VEHICLE_UPGRADE = 45,
  NPC_FUNC_GUILD_VEHICLE_MAKE = 46,
  NPC_FUNC_FASHION_REACTIVE = 47,
  NPC_FUNC_SPRING = 48,
  NPC_FUNC_RETURE_ITEM = 49,
  NPC_FUNC_PRAY_TEMPLE = 50,
  NPC_FUNC_BOSS = 51,
  NPC_FUNC_MATERIAL_UPGRADE = 52,
  NPC_FUNC_LEARN_ARTISAN = 53,
  NPC_FUNC_DAK = 54,
  NPC_FUNC_SCRIPT_EVENT = 55,
  NPC_FUNC_AUCTION_ITEM = 56,
  NPC_FUNC_AUCTION_MONEY = 57,
  NPC_FUNC_GUILD_UPGRADE = 58,
  NPC_FUNC_GUILD_DAILY_QUEST = 59,
  NPC_FUNC_GUILD_LEVEL = 60,
  NPC_FUNC_ENTER_FTL = 61,
  NPC_FUNC_RELATION_UP = 62,
  NPC_FUNC_CLEAR_PK = 63,
  NPC_FUNC_GOOD_CONVERT = 64,
  NPC_FUNC_SIEGE_SPRING = 65,
  NPC_FUNC_GOOD_GIFT = 66,
  NPC_FUNC_SIEGE_CALLBOSS = 67,
  NPC_FUNC_SIEGE_HISTORY = 70,
  NPC_FUNC_SUMMON_EXP = 73,
  NPC_FUNC_GET_DTAURA = 74,
  NPC_FUNC_STOREMAP_IDENTIFY = 75,
  NPC_FUNC_RETRACTILE_STALL = 84,
  NPC_FUNC_NO_USE = 87,
  NPC_FUNC_ARTISAN_ERASE = 88,
  NPC_FUNC_MY_SELL_SHOP = 89,
  NPC_FUNC_GOLD_SELL_SHOP = 90,
  NPC_FUNC_BUY_SELL_SHOP = 91,
  NPC_FUNC_SELL_SELL_SHOP = 92,
  NPC_FUNC_OPEN_SELL_SHOP = 93,
  NPC_FUNC_FIND_OPEN_SELL_SHOP = 94,
  NPC_FUNC_GET_SELL_SHOP_MONEY = 95,
  NPC_FUNC_ABOUT_SELL_SHOP = 96,
  NPC_FUNC_ABOUT_GOLD_SHOP = 97,
  NPC_FUNC_ABOUT_BUY_SELL_SHOP = 98,
  NPC_FUNC_ITEM_EXCHANGE = 99,
  NPC_FUNC_GEM_BLEND = 102,
  NPC_FUNC_QUERY_CHECK_RIDE_VEHICLE = 104,
  NPC_FUNC_EQUIP_BLEND = 107,
  NPC_FUNC_GUILD_RECRUIT = 108,
  NPC_FUNC_GUILD_RECRUIT_LIST = 109,
  NPC_FUNC_GUILD_RECRUIT_RESPOND = 110,
  NPC_FUNC_GUILD_RECRUIT_REQUEST = 111,
  NPC_FUNC_GUILD_GETMONEY = 112,
  NPC_FUNC_BUY_SHOP = 120,
  NPC_FUNC_SELL_SHOP = 121,
  NPC_FUNC_GEM_SACRIFICE = 122,
  NPC_FUNC_CARNIVAL_TREASURE = 123,
  NPC_FUNC_QUEST_COMMIT = 124,
  NPC_FUNC_YABIAO_OPTIONNEW = 125,
  NPC_FUNC_YABIAO_OPTIONGOLD = 126,
  NPC_FUNC_YABIAO_OPTIONZUAN = 127,
  NPC_FUNC_ITEM_SIGN = 128,
  NPC_FUNC_QUALITY_IDENT = 130,
  NPC_FUNC_CHARM = 131,
  NPC_FUNC_ITEM_DECOMPOSE = 132,
  NPC_FUNC_MOUNT_INTENSIFY = 133,
  NPC_FUNC_FUSION_DECOMPOSE = 134,
  NPC_FUNC_GUILDLEAGUE_TABLE = 135,
  NPC_FUNC_GUILDLEAGUE_ENTER = 136,
  NPC_FUNC_GUILDLEAGUE_PRACTICE = 137,
  NPC_FUNC_GUILDLEAGUE_PRATICE_ENTER = 138,
  NPC_FUNC_GUILDLEAGUE_MEMBER = 139,
  NPC_FUNC_GEM_EXCHANGE = 140,
  NPC_FUNC_ENTER_MANOR = 145,
  NPC_FUNC_ENTER_FRI_MANOR = 146,
  NPC_FUNC_SET_ACCESS = 147,
  NPC_FUNC_SCENARIO_SAY = 150,
  NPC_FUNC_SCENARIO_ANIM = 151,
  NPC_FUNC_LOGIC_END = 200,
  NPC_FUNC_HOMELAND = 201,
  NPC_FUNC_GAMEMALL = 202,
  NPC_FUNC_CURRENCY = 203,
  NPC_FUNC_SIEGE = 204,
  NPC_FUNC_SIGN_EQUIP = 205,
  NPC_FUNC_UNSIGN_EQUIP = 206,
  NPC_FUNC_HIGHGEMBLEND = 209,
  NPC_FUNC_END = 210,
}
gUI_BuildName = {
  [0] = "乾元殿",
  [1] = "修行台",
  [2] = "觅宝堂",
  [3] = "藏金阁",
  [4] = "炼术坊",
  [5] = "育灵树",
  [6] = "画影壁",
  [7] = "九州铸天鼎",
  [8] = "月中镜"
}
gUI_BuildDescribe = {
  [0] = "乾元殿是帮会中最重要的中枢机构,外形会随着帮会等级而提升,一般用于帮会处理重要事物的场所.帮会高级成员可在此执行升级帮会开辟其他区域等功能.",
  [1] = "修行台由深海寒玉石铺筑而成,宜运气,通经脉,乃是帮会成员群体修炼的场所.每日可由帮主开启修炼功能,帮众在此修炼,时段内能持续获得经验值.",
  [2] = "觅宝堂是帮会成员消耗帮贡购买各种物品的地方,这里有神秘商人贩卖的来自三界五行的各种日常用品与奇珍异宝,堂中商品会随着等级提升而增加可购买种类.",
  [3] = "以上好鎏金包铜铸建而成的藏金阁兼有华美牢固的特点,是帮会众人储藏公共财产的大型仓库.仓库储藏量随帮会等级提升,仓库存储权限由帮会职位决定.",
  [4] = "炼术坊",
  [5] = "育灵树的根系与灵界母树相通,能通过不同的养育方式孕育出不同的灵兽作为帮会守护兽.育灵树成长等级越高,可孕育的灵兽级别就越高.",
  [6] = "画影壁",
  [7] = "九州铸天鼎",
  [8] = "月中镜"
}
gUI_MainPlayerAttr = {
  Name = "",
  MenPai = 0,
  Level = 1,
  Title = "",
  Sex = 0,
  __index = function(t, k)
    return loadstring("return " .. k)()
  end
}
gUI_MapLoadImage = {
  [-1] = "ld1:ld1",
  [0] = "ld2:ld2",
  [1] = "ld1:ld1",
  [2] = "ld2:ld2",
  [3] = "ld3:ld3",
  [4] = "ld4:ld4",
  [5] = "ld5:ld5",
  [6] = "ld1:ld1",
  [7] = "ld1:ld1",
  [888] = "ld1:ld1"
}
gUI_SKILL_TYPE = {
  [0] = "吟唱",
  [1] = "引导",
  [2] = "瞬发",
  [3] = "被动",
  [4] = "触发",
  [5] = "蓄力",
  [6] = "技能类型错误",
  [1000] = "瞬发"
}
gUI_SKILLGIFT_TYPE = {
  [1] = "属性强化    ",
  [2] = "技能强化    "
}
gUI_WEAPON_ARMOR_SOUND_MAP = {
  [-1] = {
    [-2] = "hand_miss.wav",
    [-1] = "hand_miss.wav",
    [0] = "hand_leather.wav",
    [1] = "hand_crusta.wav",
    [2] = "hand_cloth.wav",
    [3] = "hand_leather.wav",
    [4] = "hand_plate.wav",
    [5] = "hand_plate.wav",
    [6] = "hand_wood.wav",
    [7] = "hand_stone.wav"
  },
  [0] = {
    [-2] = "cut_miss.wav",
    [-1] = "cut_miss.wav",
    [0] = "cut_leather.wav",
    [1] = "cut_crusta.wav",
    [2] = "cut_cloth.wav",
    [3] = "cut_leather.wav",
    [4] = "cut_plate.wav",
    [5] = "cut_plate.wav",
    [6] = "cut_wood.wav",
    [7] = "cut_stone.wav"
  },
  [1] = {
    [-2] = "stab_miss.wav",
    [-1] = "stab_miss.wav",
    [0] = "stab_leather.wav",
    [1] = "stab_crusta.wav",
    [2] = "stab_cloth.wav",
    [3] = "stab_leather.wav",
    [4] = "stab_plate.wav",
    [5] = "stab_plate.wav",
    [6] = "stab_wood.wav",
    [7] = "stab_stone.wav"
  },
  [2] = {
    [-2] = "blunt_miss.wav",
    [-1] = "blunt_miss.wav",
    [0] = "blunt_leather.wav",
    [1] = "blunt_crusta.wav",
    [2] = "blunt_cloth.wav",
    [3] = "blunt_leather.wav",
    [4] = "blunt_plate.wav",
    [5] = "blunt_plate.wav",
    [6] = "blunt_wood.wav",
    [7] = "blunt_stone.wav"
  },
  [3] = {
    [-2] = "hack_miss.wav",
    [-1] = "hack_miss.wav",
    [0] = "hack_leather.wav",
    [1] = "hack_crusta.wav",
    [2] = "hack_cloth.wav",
    [3] = "hack_leather.wav",
    [4] = "hack_plate.wav",
    [5] = "hack_plate.wav",
    [6] = "hack_wood.wav",
    [7] = "hack_stone.wav"
  },
  [4] = {
    [-2] = "claw_miss.wav",
    [-1] = "claw_miss.wav",
    [0] = "claw_leather.wav",
    [1] = "claw_crusta.wav",
    [2] = "claw_cloth.wav",
    [3] = "claw_leather.wav",
    [4] = "claw_plate.wav",
    [5] = "claw_plate.wav",
    [6] = "claw_wood.wav",
    [7] = "claw_stone.wav"
  },
  [5] = {
    [-2] = "wood_miss.wav",
    [-1] = "wood_miss.wav",
    [0] = "wood_leather.wav",
    [1] = "wood_crusta.wav",
    [2] = "wood_cloth.wav",
    [3] = "wood_leather.wav",
    [4] = "wood_plate.wav",
    [5] = "wood_plate.wav",
    [6] = "wood_wood.wav",
    [7] = "wood_stone.wav"
  }
}
gUI_Quilty_Param = {
  EQUALITY_NORMAL = 0,
  EQUALITY_GREEN = 1,
  EQUALITY_BLUE = 2,
  EQUALITY_PURPLE = 3,
  EQUALITY_YELLOW = 4,
  EQUALITY_ORANGE = 5,
  EQUALITY_NUMBER = 6
}
gUI_PLAYER_SOUND_EFFECT = {
  PLAYER_ON_ATTACKING = {
    [1001] = {
      FileName = "man_attack_",
      FileNumb = 2
    },
    [2101] = {
      FileName = "woman_attack_",
      FileNumb = 2
    },
    [1201] = {
      FileName = "man_attack_",
      FileNumb = 2
    }
  },
  PLAYER_BE_ATTACKED = {
    [1001] = {FileName = "man_wound_", FileNumb = 2},
    [2101] = {
      FileName = "woman_wound_",
      FileNumb = 2
    },
    [1201] = {FileName = "man_wound_", FileNumb = 2}
  },
  PLAYER_DEAD = {
    [1001] = {FileName = "man_dead_", FileNumb = 1},
    [2101] = {
      FileName = "woman_dead_",
      FileNumb = 1
    },
    [1201] = {FileName = "man_dead_", FileNumb = 1},
    [2301] = {
      FileName = "woman_dead_",
      FileNumb = 1
    }
  },
  CREATURE_DEAD = {
    [3001] = {
      FileName = "monstermxgw_die_",
      FileNumb = 1
    },
    [3004] = {
      FileName = "monsterhz_die_",
      FileNumb = 1
    },
    [3005] = {
      FileName = "monsterddg_die_",
      FileNumb = 1
    },
    [3006] = {
      FileName = "monsterbghy_die_",
      FileNumb = 1
    },
    [3007] = {
      FileName = "monstermm_die_",
      FileNumb = 1
    },
    [3008] = {
      FileName = "monsterboss001_die_",
      FileNumb = 1
    },
    [3009] = {
      FileName = "monsterym_die_",
      FileNumb = 1
    },
    [3010] = {
      FileName = "monsterstg_die_",
      FileNumb = 1
    },
    [3011] = {
      FileName = "monsterdxg_die_",
      FileNumb = 1
    },
    [3012] = {
      FileName = "monsterssg_die_",
      FileNumb = 1
    },
    [3013] = {
      FileName = "monsterdjg_die_",
      FileNumb = 1
    },
    [3014] = {
      FileName = "monsterboss002_die_",
      FileNumb = 1
    },
    [3015] = {
      FileName = "monstervwflier_die_",
      FileNumb = 1
    },
    [3016] = {
      FileName = "monsterfly02_die_",
      FileNumb = 1
    },
    [3017] = {
      FileName = "monsterboar_die_",
      FileNumb = 1
    }
  }
}
gUI_CHANNEL_NAME = {
  [0] = "综合",
  [1] = "当前",
  [2] = "世界",
  [3] = "密语",
  [4] = "队伍",
  [5] = "帮会",
  [6] = "联盟",
  [7] = "系统",
  [8] = "跨服"
}
gUI_NAME_CHANNEL = {
  ["当前"] = 1,
  ["世界"] = 2,
  ["密语"] = 3,
  ["队伍"] = 4,
  ["帮会"] = 5,
  ["联盟"] = 6,
  ["系统"] = 7,
  ["跨服"] = 8
}
g_CHAT_TYPE = {
  SELF = 0,
  SYSTEM = 1,
  GMCMD = 2,
  BATTLEGROUND = 3,
  NORMAL = 5,
  TELL = 10,
  TELLSELF = 11,
  ONEBYONE = 12,
  ONEBYONESELF = 13,
  TEAM = 14,
  GROUP = 15,
  FRIEND = 16,
  GUILD = 17,
  ALLIANCE = 18,
  MENPAI = 19,
  WORLD = 20,
  PROP = 21,
  ROAR = 22,
  RUMOR = 23,
  BROADCASTA = 24,
  BROADCASTB = 25,
  BROADCASTC = 26,
  MAP = 27,
 }
Chat_TypeDefine = {
  CURRENT = 1,
  GUILD = 5,
  TELL = 3,
  MAPS = 8,
  TEAM = 4,
  WORLD = 2,
  ALLIANCE = 6,
  SYSTEM = 7
}
gUI_SENDMSG_CHANNEL_MAP = {
  [1] = g_CHAT_TYPE.NORMAL,
  [2] = g_CHAT_TYPE.WORLD,
  [3] = g_CHAT_TYPE.TELL,
  [4] = g_CHAT_TYPE.TEAM,
  [5] = g_CHAT_TYPE.GUILD,
  [6] = g_CHAT_TYPE.ALLIANCE,
  [7] = g_CHAT_TYPE.FRIEND,
  [8] = g_CHAT_TYPE.MAP,
  [9] = g_CHAT_TYPE.BATTLEGROUND
}
gUI_CHANNEL_INDEX_RELATION = {
  [0] = 1,
  [1] = 2,
  [2] = 6,
  [3] = 5,
  [4] = 4,
  [5] = 7,
  [6] = 3,
  [7] = 9,
  [8] = 8,
  [10] = 10,
  [11] = 11,
  [100] = 100
}
gUI_CHANNEL_INDEX_RELATION1 = {
  [1] = 0,
  [2] = 1,
  [3] = 6,
  [4] = 4,
  [5] = 3,
  [6] = 2,
  [7] = 5,
  [8] = 8,
  [9] = 7
}
gUI_CHANNEL_STR1 = {
  Normal = 0,
  World = 1,
  Tell = 6,
  Team = 4,
  Guild = 3,
  Alliance = 2,
  Friend = 5,
  Map = 8,
  BattleGrounp = 9,
  Roar = 10,
  Broadcasta = 11
}
gUI_CHANNEL_STR = {
  Integration = 0,
  Normal = 1,
  World = 2,
  Tell = 3,
  Team = 4,
  Guild = 5,
  Alliance = 6,
  Friend = 7,
  Map = 8,
  BattleGrounp = 9,
  Roar = 10,
  Broadcasta = 11
}
gUI_CHAT_CHANNEL_SETTINGINFO = {
  [0] = {
    MAXWORDLIMIT = 0,
    COLOR = "0xFFFFFFFF",
    IMAGE = "",
    DESCRIPTION = "综合频道",
    CHANNEL_NAME = "综合",
    TIPDES = ""
  },
  [1] = {
    MAXWORDLIMIT = 48,
    COLOR = "0xFFFFFFFF",
    IMAGE = "UiBtn012:Common_n^UiBtn012:Common_h^UiBtn012:Common_p^UiBtn012:Common_d",
    DESCRIPTION = "当前频道",
    CHANNEL_NAME = "当前",
    TIPDES = "左键点击查看{#Y^当前}频道聊天信息；右键点击可以自定义{#Y^当前}频道显示的聊天信息"
  },
  [2] = {
    MAXWORDLIMIT = 120,
    COLOR = "0xFF54cdff",
    IMAGE = "UiBtn012:World_n^UiBtn012:World_h^UiBtn012:World_p^UiBtn012:World_d",
    DESCRIPTION = "世界频道",
    CHANNEL_NAME = "世界",
    TIPDES = "左键点击查看{#Y^世界}频道聊天信息；右键点击可以自定义{#Y^世界}频道显示的聊天信息"
  },
  [3] = {
    MAXWORDLIMIT = 120,
    COLOR = "0xFFf3a5ff",
    IMAGE = "",
    DESCRIPTION = "密语频道",
    CHANNEL_NAME = "密语",
    TIPDES = "左键点击查看{#Y^密语}频道聊天信息；右键点击可以自定义{#Y^密语}频道显示的聊天信息"
  },
  [4] = {
    MAXWORDLIMIT = 120,
    COLOR = "0xFFffdc89",
    IMAGE = "UiBtn012:Team_n^UiBtn012:Team_h^UiBtn012:Team_p^UiBtn012:Team_d",
    DESCRIPTION = "队伍频道",
    CHANNEL_NAME = "队伍",
    TIPDES = "左键点击查看{#Y^队伍}频道聊天信息；右键点击可以自定义{#Y^队伍}频道显示的聊天信息"
  },
  [5] = {
    MAXWORDLIMIT = 120,
    COLOR = "0xFF24fc3d",
    IMAGE = "UiBtn012:ChatGuild_n^UiBtn012:ChatGuild_h^UiBtn012:ChatGuild_p^UiBtn012:ChatGuild_d",
    DESCRIPTION = "帮会频道",
    CHANNEL_NAME = "帮会",
    TIPDES = "左键点击查看{#Y^帮会}频道聊天信息；右键点击可以自定义{#Y^帮会}频道显示的聊天信息"
  },
  [6] = {
    MAXWORDLIMIT = 120,
    COLOR = "0xFF00ffd8",
    IMAGE = "UiBtn012:Alliance_n^UiBtn012:Alliance_h^UiBtn012:Alliance_p^UiBtn012:Alliance_d",
    DESCRIPTION = "联盟频道",
    CHANNEL_NAME = "联盟",
    TIPDES = "左键点击查看{#Y^联盟}频道聊天信息；右键点击可以自定义{#Y^联盟}频道显示的聊天信息"
  },
  [7] = {
    MAXWORDLIMIT = 120,
    COLOR = "0xFFc1ff4a",
    IMAGE = "UiBtn012:ChatFriend_n^UiBtn012:ChatFriend_h^UiBtn012:ChatFriend_p^UiBtn012:ChatFriend_d",
    DESCRIPTION = "好友频道",
    CHANNEL_NAME = "好友",
    TIPDES = "左键点击查看{#Y^好友}频道聊天信息；右键点击可以自定义{#Y^好友}频道显示的聊天信息"
  },
  [8] = {
    MAXWORDLIMIT = 120,
    COLOR = "0xFFFF7A91",
    IMAGE = "UiBtn012:StepServer_n^UiBtn012:StepServer_h^UiBtn012:StepServer_p^UiBtn012:StepServer_d",
    DESCRIPTION = "跨服频道",
    CHANNEL_NAME = "跨服",
    TIPDES = ""
  },
  [9] = {
    MAXWORDLIMIT = 120,
    COLOR = "0xFFFF7A91",
    IMAGE = "UiBtn012:ChatBattle_n^UiBtn012:ChatBattle_h^UiBtn012:ChatBattle_p^UiBtn012:ChatBattle_d",
    DESCRIPTION = "战场频道",
    CHANNEL_NAME = "战场",
    TIPDES = "左键点击查看{#Y^战场}频道聊天信息；右键点击可以自定义{#Y^战场}频道显示的聊天信息"
  },
  [10] = {
    MAXWORDLIMIT = 0,
    COLOR = "0xFFFF8000",
    IMAGE = "UiBtn012:Image_System^UiBtn012:Image_System^UiBtn012:Image_System^UiBtn012:Image_System",
    DESCRIPTION = "传闻频道",
    CHANNEL_NAME = "",
    TIPDES = ""
  },
  [11] = {
    MAXWORDLIMIT = 0,
    COLOR = "0xFFFFFF80",
    IMAGE = "",
    DESCRIPTION = "",
    CHANNEL_NAME = "",
    TIPDES = ""
  },
  [100] = {
    MAXWORDLIMIT = 0,
    COLOR = "0xFFFFC0CB",
    IMAGE = "",
    DESCRIPTION = "",
    CHANNEL_NAME = "",
    TIPDES = ""
  }
}
gUI_CHATMSG_COLOR = {
  [0] = "0xFFFFFFFF",
  [1] = "0xFFffffff",
  [2] = "0xFF54cdff",
  [3] = "0xFFf3a5ff",
  [4] = "0xFFffdc89",
  [5] = "0xFF24fc3d",
  [6] = "0xFF00ffd8",
  [7] = "0xFFc1ff4a",
  [8] = "0xFFFF7A91",
  [9] = "0xFFFF7A91",
  [10] = "0xFFFF8000",
  [11] = "0xFFFFFF80",
  [100] = "0xFFFFC0CB"
}
gUI_CHAR_IMG = {
  [1] = "UiBtn012:Common_n^UiBtn012:Common_h^UiBtn012:Common_p^UiBtn012:Common_d",
  [2] = "UiBtn012:World_n^UiBtn012:World_h^UiBtn012:World_p^UiBtn012:World_d",
  [4] = "UiBtn012:Team_n^UiBtn012:Team_h^UiBtn012:Team_p^UiBtn012:Team_d",
  [5] = "UiBtn012:ChatGuild_n^UiBtn012:ChatGuild_h^UiBtn012:ChatGuild_p^UiBtn012:ChatGuild_d",
  [6] = "UiBtn012:Alliance_n^UiBtn012:Alliance_h^UiBtn012:Alliance_p^UiBtn012:Alliance_d",
  [7] = "UiBtn012:ChatFriend_n^UiBtn012:ChatFriend_h^UiBtn012:ChatFriend_p^UiBtn012:ChatFriend_d",
  [8] = "UiBtn012:StepServer_n^UiBtn012:StepServer_h^UiBtn012:StepServer_p^UiBtn012:StepServer_d",
  [9] = "UiBtn012:ChatBattle_n^UiBtn012:ChatBattle_h^UiBtn012:ChatBattle_p^UiBtn012:ChatBattle_d",
  [10] = "UiBtn012:Image_System^UiBtn012:Image_System^UiBtn012:Image_System^UiBtn012:Image_System"
}
gUI_SETTING_CHANNAL = {
  [0] = "当前频道",
  [1] = "世界频道",
  [2] = "密语频道",
  [3] = "队伍频道",
  [4] = "帮会频道",
  [5] = "联盟频道",
  [6] = "好友频道"
}
gUI_SENDMSG_CHANNEL_MAX_WORD = {
  [1] = 48,
  [2] = 120,
  [3] = 120,
  [4] = 120,
  [5] = 120,
  [6] = 120,
  [7] = 120,
  [8] = 120,
  [9] = 120
}
{
  [g_CHAT_TYPE.NORMAL] = 1,
  [g_CHAT_TYPE.TEAM] = 4,
  [g_CHAT_TYPE.WORLD] = 2,
  [g_CHAT_TYPE.TELL] = 3,
  [g_CHAT_TYPE.SYSTEM] = 7,
  [g_CHAT_TYPE.SELF] = 7,
  [g_CHAT_TYPE.GMCMD] = 7,
  [g_CHAT_TYPE.GROUP] = nil,
  [g_CHAT_TYPE.GUILD] = 5,
  [g_CHAT_TYPE.MENPAI] = nil,
  [g_CHAT_TYPE.TELLSELF] = 3,
  [g_CHAT_TYPE.FRIEND] = 7,
  [g_CHAT_TYPE.ONEBYONE] = nil,
  [g_CHAT_TYPE.ALLIANCE] = 6,
  [g_CHAT_TYPE.PROP] = 2,
  [g_CHAT_TYPE.ROAR] = 10,
  [g_CHAT_TYPE.RUMOR] = 10,
  [g_CHAT_TYPE.BROADCASTA] = 11,
  [g_CHAT_TYPE.BROADCASTB] = 11,
  [g_CHAT_TYPE.BROADCASTC] = 11
}[g_CHAT_TYPE.ONEBYONESELF] = nil
{
  [g_CHAT_TYPE.NORMAL] = 1,
  [g_CHAT_TYPE.TEAM] = 4,
  [g_CHAT_TYPE.WORLD] = 2,
  [g_CHAT_TYPE.TELL] = 3,
  [g_CHAT_TYPE.SYSTEM] = 7,
  [g_CHAT_TYPE.SELF] = 7,
  [g_CHAT_TYPE.GMCMD] = 7,
  [g_CHAT_TYPE.GROUP] = nil,
  [g_CHAT_TYPE.GUILD] = 5,
  [g_CHAT_TYPE.MENPAI] = nil,
  [g_CHAT_TYPE.TELLSELF] = 3,
  [g_CHAT_TYPE.FRIEND] = 7,
  [g_CHAT_TYPE.ONEBYONE] = nil,
  [g_CHAT_TYPE.ALLIANCE] = 6,
  [g_CHAT_TYPE.PROP] = 2,
  [g_CHAT_TYPE.ROAR] = 10,
  [g_CHAT_TYPE.RUMOR] = 10,
  [g_CHAT_TYPE.BROADCASTA] = 11,
  [g_CHAT_TYPE.BROADCASTB] = 11,
  [g_CHAT_TYPE.BROADCASTC] = 11
}[g_CHAT_TYPE.MAP] = 8
gUI_SENDMSG_CHANNEL_MAP2, {
  [g_CHAT_TYPE.NORMAL] = 1,
  [g_CHAT_TYPE.TEAM] = 4,
  [g_CHAT_TYPE.WORLD] = 2,
  [g_CHAT_TYPE.TELL] = 3,
  [g_CHAT_TYPE.SYSTEM] = 7,
  [g_CHAT_TYPE.SELF] = 7,
  [g_CHAT_TYPE.GMCMD] = 7,
  [g_CHAT_TYPE.GROUP] = nil,
  [g_CHAT_TYPE.GUILD] = 5,
  [g_CHAT_TYPE.MENPAI] = nil,
  [g_CHAT_TYPE.TELLSELF] = 3,
  [g_CHAT_TYPE.FRIEND] = 7,
  [g_CHAT_TYPE.ONEBYONE] = nil,
  [g_CHAT_TYPE.ALLIANCE] = 6,
  [g_CHAT_TYPE.PROP] = 2,
  [g_CHAT_TYPE.ROAR] = 10,
  [g_CHAT_TYPE.RUMOR] = 10,
  [g_CHAT_TYPE.BROADCASTA] = 11,
  [g_CHAT_TYPE.BROADCASTB] = 11,
  [g_CHAT_TYPE.BROADCASTC] = 11
}[g_CHAT_TYPE.BATTLEGROUND] = {
  [g_CHAT_TYPE.NORMAL] = 1,
  [g_CHAT_TYPE.TEAM] = 4,
  [g_CHAT_TYPE.WORLD] = 2,
  [g_CHAT_TYPE.TELL] = 3,
  [g_CHAT_TYPE.SYSTEM] = 7,
  [g_CHAT_TYPE.SELF] = 7,
  [g_CHAT_TYPE.GMCMD] = 7,
  [g_CHAT_TYPE.GROUP] = nil,
  [g_CHAT_TYPE.GUILD] = 5,
  [g_CHAT_TYPE.MENPAI] = nil,
  [g_CHAT_TYPE.TELLSELF] = 3,
  [g_CHAT_TYPE.FRIEND] = 7,
  [g_CHAT_TYPE.ONEBYONE] = nil,
  [g_CHAT_TYPE.ALLIANCE] = 6,
  [g_CHAT_TYPE.PROP] = 2,
  [g_CHAT_TYPE.ROAR] = 10,
  [g_CHAT_TYPE.RUMOR] = 10,
  [g_CHAT_TYPE.BROADCASTA] = 11,
  [g_CHAT_TYPE.BROADCASTB] = 11,
  [g_CHAT_TYPE.BROADCASTC] = 11
}, 9
gUI_SENDMSG_CHANNEL_MAP3 = {
  [g_CHAT_TYPE.NORMAL] = 0,
  [g_CHAT_TYPE.WORLD] = 1,
  [g_CHAT_TYPE.TELL] = 2,
  [g_CHAT_TYPE.TELLSELF] = 2,
  [g_CHAT_TYPE.TEAM] = 3,
  [g_CHAT_TYPE.GUILD] = 4,
  [g_CHAT_TYPE.ALLIANCE] = 5,
  [g_CHAT_TYPE.SYSTEM] = 9,
  [g_CHAT_TYPE.SELF] = 9,
  [g_CHAT_TYPE.GMCMD] = 9,
  [g_CHAT_TYPE.ROAR] = 7,
  [g_CHAT_TYPE.RUMOR] = 7,
  [g_CHAT_TYPE.FRIEND] = 10,
  [g_CHAT_TYPE.PROP] = 1,
  [g_CHAT_TYPE.BATTLEGROUND] = 11
}
gUI_CHATCHANNEL_QUICK_NAME, {
  s = 1,
  w = 2,
  m = 3,
  t = 4,
  g = 5,
  l = 6,
  k = 8,
  f = 7,
  b = 9,
  S = 1,
  W = 2,
  M = 3,
  T = 4,
  G = 5,
  L = 6,
  K = 8,
  F = 7,
  B =  9,
 }
gUI_GOODS_QUALITY_COLOR_PREFIX = {
  [-1] = "0xFFFFFFFF",
  [0] = "0xFFFAFAFA",
  [1] = "0xFF00FF00",
  [2] = "0xFF1E90FF",
  [3] = "0xFFC500FF",
  [4] = "0xFFFFFF00",
  [5] = "0xFFFFD700",
  [6] = "0xFFFF8C00"
}
gUI_GOODS_QUALITY_COLOR = {
  [-1] = "FFFFFFFF",
  [0] = "FFFAFAFA",
  [1] = "FF00FF00",
  [2] = "FF1E90FF",
  [3] = "FFC500FF",
  [4] = "FFFFFF00",
  [5] = "FFFFD700",
  [6] = "FFFF8C00"
}
gUI_GOODS_QUALITY_IMAGE = {
  [-1] = "UiIamge013:Image_White",
  [0] = "UiIamge013:Image_White",
  [1] = "UiIamge013:Image_Green",
  [2] = "UiIamge013:Image_Blue",
  [3] = "UiIamge013:Image_Pueple",
  [4] = "UiIamge013:Image_Yellow",
  [5] = "UiIamge013:Image_Orange",
  [6] = "UiIamge013:Image_Orange"
}
gUI_TIPFLAG = {
  TIPNONE = 0,
  TIPNPC = 1,
  TIPTEAM = 2,
  TIPTRAN = 3,
  TIPTASK = 4,
  TIPGUARD = 5,
  TIPSPECIAL = 6
}
gUI_MAPSPCIALINFO = {
  [23] = "已摧毁",
  [24] = "蓝方夺取中",
  [25] = "蓝方占领",
  [26] = "红方夺取中",
  [27] = "红方占领",
  [28] = "未占领",
  [29] = "蓝方夺取中",
  [30] = "蓝方占领",
  [31] = "红方夺取中",
  [32] = "红方占领",
  [33] = "",
  [34] = "",
  [35] = "中立",
  [36] = "蓝方占领",
  [37] = "红方占领",
  [39] = "已死亡",
  [40] = "已死亡",
  [41] = "",
  [42] = "",
  [44] = "",
 }
gUI_GOODS_QUAL_COLORNEW = {
  [0] = "FFFAFAFA",
  [1] = "FF00FF00",
  [2] = "FF1E90FF",
  [3] = "FF9400D3",
  [4] = "FFFFFF00",
  [5] = "FFFF0611",
  [6] = "FFFF8C00",
  [7] = "FFFF8C00"
}
gUI_ACTIONBAR_OFFSETS = {
  action_left = 0,
  action_mid1 = 48,
  action_mid2 = 60,
  action_right = 70,
  action_special = 80,
  action_end = 82
}
gUI_GOODSBOX_BAG = {
  template = -1,
  backpack0 = 1,
  backpack1 = 2,
  backpackworld = 3,
  myequipbox = 4,
  bankbag = 5,
  exchange = 6,
  pet1 = 7,
  pet2 = 7,
  othequipbox = 9,
  rankequipbox = 13,
  mail = 14,
  sold = 15,
  stallbag1 = 16,
  stallbag2 = 17,
  chattempitem = 23,
  stallshopbag1 = 24,
  stallshopbag2 = 27,
  pet_equips = 30,
  npcreusltitem = 31,
  dropbox = 35,
  gemblendres = 36,
  Gbankbag = 37,
  RankItem = 38,
  DropItem = 39,
  GbankbagLog = 40,
gUI_AURABOX_INDEXES = {
  buffer1 = 0,
  debuffer1 = 0,
  targetbuff = 1,
  debuffbyme = 1,
  buffer9 = 9,
  debuffer9 = 9,
  buffer3 = 2,
  buffer4 = 3,
  buffer5 = 4,
  buffer6 = 5,
  buffer7 = 6,
  buffer8 = 7
}
gUI_HUMAN_EQUIP = {
  HEQUIP_NUMBER = 11,
  HEQUIP_WEAPON2 = 16,
  HEQUIP_RING2 = 17,
  HEQUIP_TOTAL = 20,
  PET_EQUIP_ARMOR = HEQUIP_TOTAL
}
gUI_GOODS_BASE_ATTRIBUTE_DES = {
  [0] = "增加气血上限",
  [1] = "增加气血的上限",
  [2] = "增加气血恢复速度",
  [3] = "增加内力的上限",
  [4] = "增加内力的上限",
  [5] = "增加内力恢复速度",
  [6] = "水攻攻击",
  [7] = "水功抵抗",
  [8] = "火功攻击",
  [9] = "火功抵抗",
  [10] = "金功攻击",
  [11] = "金功抵抗",
  [12] = "木功攻击",
  [13] = "木功抵抗",
  [14] = "土功攻击",
  [15] = "土功抵抗",
  [16] = "按百分比抵消所有属性攻击",
  [17] = "最小外功攻击",
  [18] = "最大外功攻击",
  [19] = "增加外功百分比",
  [20] = "增加装备基础外功百分比",
  [21] = "增加外功防御",
  [22] = "增加外功防御百分比",
  [23] = "增加装备基础外功防御百分比",
  [24] = "抵消外功伤害百分比",
  [25] = "最小内功攻击",
  [26] = "最大内功攻击",
  [27] = "增加内功百分比",
  [28] = "增加装备基础内功百分比",
  [29] = "增加内功防御",
  [30] = "增加内功防御百分比",
  [31] = "增加装备基础内功防御百分比",
  [32] = "抵消内功伤害百分比",
  [33] = "增加出招速度",
  [34] = "加快技能冷却时间",
  [35] = "增加命中",
  [36] = "增加闪避",
  [37] = "增加重创值",
  [38] = "增加重创伤害",
  [39] = "增加御劲值",
  [40] = "增加化劲率",
  [41] = "增加无视对方防御概率",
  [42] = "增加移动速度百分比",
  [43] = "增加力量",
  [44] = "增加元气",
  [45] = "增加体质",
  [46] = "增加根骨",
  [47] = "增加身法",
  [48] = "增加灵巧",
  [49] = "增加人物一级属性",
  [50] = "增加伤害反射",
  [51] = "以内力吸收伤害增加",
  [52] = "增加气血偷取",
  [53] = "增加内力偷取",
  [54] = "激活某个使用技能",
  [55] = "增加某个随机技能",
  [56] = "随机技能发动概率",
  [57] = "外功攻击",
  [58] = "内功攻击",
  [59] = "内外功攻击",
  [60] = "化解攻击",
  [61] = "uistring没有配置",
  [62] = "uistring没有配置",
  [63] = "uistring没有配置",
  [64] = "uistring没有配置",
  [65] = "uistring没有配置",
  [66] = "增加能量上限",
  [67] = "增加能量上限百分比",
  [68] = "增加能量恢复速度",
  [69] = "增加伤害偏斜",
  [70] = "增加伤害偏斜百分比",
  [71] = "增加直接伤害",
  [72] = "增加直接伤害百分比",
 }
gUI_CHARACTER_ATTR = {
  [1] = "力量",
  [2] = "元气",
  [3] = "体质",
  [4] = "根骨",
  [5] = "身法",
  [6] = "灵巧",
  [7] = "最小外功攻击",
  [8] = "最大外功攻击",
  [9] = "外功防御",
  [10] = "最小内功攻击",
  [11] = "最大内功攻击",
  [12] = "内功防御",
  [13] = "普通最小攻击",
  [14] = "普通最大攻击",
  [15] = "普通防御",
  [16] = "最大内力值",
  [17] = "最大气血值",
  [18] = "闪避",
  [19] = "命中",
  [20] = "重创",
  [21] = "御劲值",
  [22] = "保留",
  [23] = "保留",
  [24] = "重创伤害",
  [25] = "化劲率",
  [26] = "金攻击",
  [27] = "金防御",
  [28] = "金减抗",
  [29] = "木攻击",
  [30] = "木防御",
  [31] = "木减抗",
  [32] = "水攻击",
  [33] = "水防御",
  [34] = "水减抗",
  [35] = "火攻击",
  [36] = "火防御",
  [37] = "火减抗",
  [38] = "土攻击",
  [39] = "土防御",
  [40] = "土减抗",
  [41] = "攻击吸收",
  [42] = "最大活力",
  [43] = "最大体力",
  [44] = "气血恢复速度",
  [45] = "内力恢复速度",
  [46] =  ""
 }
gUI_FORGE_SLOT = {
  Equip = 0,
  LuckyStone1 = 1,
  LuckyStone2 = 2,
  LuckyStone3 = 3,
  SafeStone = 4
}
gUI_CHINA_NUMBER = {
  [0] = "零",
  [1] = "一",
  [2] = "二",
  [3] = "三",
  [4] = "四",
  [5] = "五",
  [6] = "六",
  [7] = "七",
  [8] = "八",
  [9] = "九",
  [10] = "十",
  [11] = "百",
  [12] = "千"
}
gUI_GOODS_DISTRIBUTE_MODE = {
  [1] = "自由拾取",
  [2] = "投掷拾取",
  [3] = "",
  [4] = "职业分配"
}
gUI_GOODS_DISTRIBUTE_QUALITY = {
  [1] = "白色",
  [2] = "绿色",
  [3] = "蓝色",
  [4] = "紫色",
  [5] = "黄色"
}
gUI_GOODS_PK_MODE = {
  [1] = "和平模式",
  [2] = "善恶模式",
  [3] = "帮会模式",
  [4] = "组队模式",
  [5] = "全体模式"
}
gUI_STALLMODEL_ICON = {
  [-2] = "tg_NPChead01:head_47",
  [-3] = "tg_NPChead01:head_47",
  [-4] = "tg_NPChead01:head_47"
}
gUI_PLAYER_ICON = {
  [0] = {
    [0] = {
      [0] = "tg_TouXiang02:head_01",
      [1] = "tg_TouXiang02:head_01"
    },
    [1] = {
      [0] = "tg_TouXiang02:head_01",
      [1] = "tg_TouXiang02:head_01"
    }
  },
  [1] = {
    [0] = {
      [0] = "tg_TouXiang02:head_02",
      [1] = "tg_TouXiang02:head_02"
    },
    [1] = {
      [0] = "tg_TouXiang02:head_02",
      [1] = "tg_TouXiang02:head_02"
    }
  },
  [2] = {
    [0] = {
      [0] = "tg_TouXiang02:head_03",
      [1] = "tg_TouXiang02:head_03"
    },
    [1] = {
      [0] = "tg_TouXiang02:head_03",
      [1] = "tg_TouXiang02:head_03"
    }
  },
  [3] = {
    [0] = {
      [0] = "tg_TouXiang02:head_05",
      [1] = "tg_TouXiang02:head_05"
    },
    [1] = {
      [0] = "tg_TouXiang02:head_05",
      [1] = "tg_TouXiang02:head_05"
    }
  },
  [4] = {
    [0] = {
      [0] = "tg_TouXiang02:head_04",
      [1] = "tg_TouXiang02:head_04"
    },
    [1] = {
      [0] = "tg_TouXiang02:head_07",
      [1] = "tg_TouXiang02:head_07"
    }
  }
}
gUI_PLAYER_ICON_LEAVE = {
  [0] = {
    [0] = "tg_TouXiang01:head_01",
    [1] = "tg_TouXiang01:head_01"
  },
  [1] = {
    [0] = "tg_TouXiang01:head_02",
    [1] = "tg_TouXiang01:head_02"
  },
  [2] = {
    [0] = "tg_TouXiang01:head_03",
    [1] = "tg_TouXiang01:head_03"
  },
  [3] = {
    [0] = "tg_TouXiang01:head_05",
    [1] = "tg_TouXiang01:head_05"
  },
  [4] = {
    [0] = "tg_TouXiang01:head_04",
    [1] = "tg_TouXiang01:head_07"
  }
}
gUI_PROFESSION_INTRODUCE = {
  [0] = {
    ["sign"] = "UiIamge004:ShenjiangImage_01",
    ["name"] = "UiIamge004:ShenjiangImage_01",
    [0] = {
      icon = {
        [0] = "tg_TouXiang02:head_01",
        [1] = "tg_TouXiang02:head_01",
        ["maxnum"] = 2
      },
      hairModel = {
        [0] = "tg_hair:head_01",
        [1] = "tg_hair:head_02"
      },
      hairColor = {
        [0] = "tg_hair:head_14",
        [1] = "tg_hair:head_15",
        [2] = "tg_hair:head_13",
        [3] = "tg_hair:head_11",
        [4] = "tg_hair:head_12"
      },
      faceModel = {
        [0] = "tg_face:head_01",
        [1] = "tg_face:head_02",
        [2] = "tg_face:head_03"
      }
    },
    [1] = {
      icon = {
        [0] = "tg_TouXiang02:head_01",
        [1] = "tg_TouXiang02:head_01",
        ["maxnum"] = 2
      },
      hairModel = {
        [0] = "tg_hair:head_01",
        [1] = "tg_hair:head_02"
      },
      hairColor = {
        [0] = "tg_hair:head_14",
        [1] = "tg_hair:head_15",
        [2] = "tg_hair:head_13",
        [3] = "tg_hair:head_11",
        [4] = "tg_hair:head_12"
      },
      faceModel = {
        [0] = "tg_face:head_01",
        [1] = "tg_face:head_02",
        [2] = "tg_face:head_03"
      }
    },
    ["biography"] = {
      menpaiPic = "tg_chuangjue01:chuangjue01_01",
      proviewText = "    远古一支英勇骁战的战队夺得显赫战功，被黄帝封为神将，并赐府邸坤王府。神将军世代以能人传承，他们威武刚毅的身躯利盾让敌人无法突破其战线；他们坚定不摧的意志精神让敌人无法撼动其军魂，是战场上让敌人最闻风丧胆的一支天将神军。",
      mainAttribute = "力量",
      fightTendency = "近战",
      teamTendency = "伤害承受",
      fightPic = "tg_chuangjue02:chuangjue02_12",
      recommend = 1,
      operdiff = 3,
      male_enable = true,
      female_enable = false
    },
    ["avatar"] = {
      mx = 189,
      my = 23.16,
      mz = 170.36,
      mscale = 3.5,
      mrotation = 110,
      fx = 0,
      fy = 0,
      fz = 0,
      fscale = 3,
      frotation = 110,
      showx = 189.859,
      showy = 22.9346,
      showz = 187.365,
      sscale = 3,
      srotation = 110
    },
    ["create"] = {
      cx = 198.349,
      cy = 27.6206,
      cz = 207.281,
      dx = -0.361614,
      dy = 0.00140123,
      dz = -0.932327,
      rx = 189.859,
      ry = 22.9346,
      rz = 187.365,
      scale = 3,
      rotation = 110
    },
    ["animation"] = {
      _out1 = 105,
      _out2 = 109,
      show = 106,
      standard = 111,
      _in1 = 110,
      _in2 = 108,
      default = 107
    }
  },
  [1] = {
    ["sign"] = "UiIamge004:QishaImage_01",
    ["name"] = "UiIamge004:QishaImage_01",
    [0] = {
      icon = {
        [0] = "tg_TouXiang02:head_02",
        [1] = "tg_TouXiang02:head_02",
        ["maxnum"] = 2
      },
      hairModel = {
        [0] = "tg_hair:head_09",
        [1] = "tg_hair:head_10"
      },
      hairColor = {
        [0] = "tg_hair:head_14",
        [1] = "tg_hair:head_15",
        [2] = "tg_hair:head_13",
        [3] = "tg_hair:head_11",
        [4] = "tg_hair:head_12"
      },
      faceModel = {
        [0] = "tg_face:head_10",
        [1] = "tg_face:head_11",
        [2] = "tg_face:head_12"
      }
    },
    [1] = {
      icon = {
        [0] = "tg_TouXiang02:head_02",
        [1] = "tg_TouXiang02:head_02",
        ["maxnum"] = 2
      },
      hairModel = {
        [0] = "tg_hair:head_09",
        [1] = "tg_hair:head_10"
      },
      hairColor = {
        [0] = "tg_hair:head_14",
        [1] = "tg_hair:head_15",
        [2] = "tg_hair:head_13",
        [3] = "tg_hair:head_11",
        [4] = "tg_hair:head_12"
      },
      faceModel = {
        [0] = "tg_face:head_10",
        [1] = "tg_face:head_11",
        [2] = "tg_face:head_12"
      }
    },
    ["biography"] = {
      menpaiPic = "tg_chuangjue01:chuangjue01_02",
      proviewText = "    上天界南斗星君主动请缨转世下凡为七杀，世代监守在幽都天地陵这个三界诡道的端口。即便由于降世为人剥离了原本强大的能力，即便要接受再度轮回之苦，即便从此无法摆脱不被理解的孤独，他们也依旧坚守最初的信念：慈悲无救，杀伐开道。",
      mainAttribute = "敏捷",
      fightTendency = "近战、潜行、爆发",
      teamTendency = "伤害输出、控场",
      fightPic = "tg_chuangjue02:chuangjue02_13",
      recommend = 1,
      operdiff = 5,
      male_enable = true,
      female_enable = false
    },
    ["avatar"] = {
      mx = 200.13,
      my = 21.94,
      mz = 158,
      mscale = 3.4,
      mrotation = 110,
      fx = 0,
      fy = 0,
      fz = 0,
      fscale = 3,
      frotation = 110,
      showx = 189.859,
      showy = 22.9346,
      showz = 187.365,
      sscale = 3,
      srotation = 110
    },
    ["create"] = {
      cx = 198.349,
      cy = 27.6206,
      cz = 207.281,
      dx = -0.361614,
      dy = 0.00140123,
      dz = -0.932327,
      rx = 189.859,
      ry = 22.9346,
      rz = 187.365,
      scale = 3,
      rotation = 110
    },
    ["animation"] = {
      _out1 = 105,
      _out2 = 109,
      show = 106,
      standard = 111,
      _in1 = 110,
      _in2 = 108,
      default = 107
    }
  },
  [2] = {
    ["sign"] = "UiIamge004:LingxiaoImage_01",
    ["name"] = "UiIamge004:LingxiaoImage_01",
    [0] = {
      icon = {
        [0] = "tg_TouXiang02:head_03",
        [1] = "tg_TouXiang02:head_03",
        ["maxnum"] = 2
      },
      hairModel = {
        [0] = "tg_hair:head_07",
        [1] = "tg_hair:head_08"
      },
      hairColor = {
        [0] = "tg_hair:head_14",
        [1] = "tg_hair:head_15",
        [2] = "tg_hair:head_13",
        [3] = "tg_hair:head_11",
        [4] = "tg_hair:head_12"
      },
      faceModel = {
        [0] = "tg_face:head_07",
        [1] = "tg_face:head_08",
        [2] = "tg_face:head_09"
      }
    },
    [1] = {
      icon = {
        [0] = "tg_TouXiang02:head_03",
        [1] = "tg_TouXiang02:head_03",
        ["maxnum"] = 2
      },
      hairModel = {
        [0] = "tg_hair:head_07",
        [1] = "tg_hair:head_08"
      },
      hairColor = {
        [0] = "tg_hair:head_14",
        [1] = "tg_hair:head_15",
        [2] = "tg_hair:head_13",
        [3] = "tg_hair:head_11",
        [4] = "tg_hair:head_12"
      },
      faceModel = {
        [0] = "tg_face:head_07",
        [1] = "tg_face:head_08",
        [2] = "tg_face:head_09"
      }
    },
    ["biography"] = {
      menpaiPic = "tg_chuangjue01:chuangjue01_03",
      proviewText = "    盘古开天辟地之后，天地间尚有部分灵力无法归位，百年后化为灵霄游历天下斩妖除魔，建立灵霄拂水阁。灵霄一脉多修为甚高，以山为志以水为乐。他们仙术精湛，擅长群攻，以强大的法力毁天憾地，足以令对手胆寒丧志。",
      mainAttribute = "智力",
      fightTendency = "远程、法阵",
      teamTendency = "伤害输出、控场",
      fightPic = "tg_chuangjue02:chuangjue02_14",
      recommend = 0,
      operdiff = 4.5,
      male_enable = true,
      female_enable = false
    },
    ["avatar"] = {
      mx = 0,
      my = 0,
      mz = 0,
      mscale = 3,
      mrotation = 110,
      fx = 174.28,
      fy = 23.18,
      fz = 164.89,
      fscale = 3.8,
      frotation = 120,
      showx = 189.859,
      showy = 22.9346,
      showz = 187.365,
      sscale = 3,
      srotation = 110
    },
    ["create"] = {
      cx = 198.349,
      cy = 27.6206,
      cz = 207.281,
      dx = -0.361614,
      dy = 0.00140123,
      dz = -0.932327,
      rx = 189.859,
      ry = 22.9346,
      rz = 187.365,
      scale = 3,
      rotation = 110
    },
    ["animation"] = {
      _out1 = 105,
      _out2 = 109,
      show = 106,
      standard = 111,
      _in1 = 110,
      _in2 = 108,
      default = 107
    }
  },
  [3] = {
    ["sign"] = "UiIamge004:ChenImage_01",
    ["name"] = "UiIamge004:ChenImage_01",
    [0] = {
      icon = {
        [0] = "tg_TouXiang02:head_05",
        [1] = "tg_TouXiang02:head_05",
        ["maxnum"] = 2
      },
      hairModel = {
        [0] = "tg_hair:head_03",
        [1] = "tg_hair:head_04"
      },
      hairColor = {
        [0] = "tg_hair:head_14",
        [1] = "tg_hair:head_15",
        [2] = "tg_hair:head_13",
        [3] = "tg_hair:head_11",
        [4] = "tg_hair:head_12"
      },
      faceModel = {
        [0] = "tg_face:head_13",
        [1] = "tg_face:head_14",
        [2] = "tg_face:head_15"
      }
    },
    [1] = {
      icon = {
        [0] = "tg_TouXiang02:head_05",
        [1] = "tg_TouXiang02:head_05",
        ["maxnum"] = 2
      },
      hairModel = {
        [0] = "tg_hair:head_03",
        [1] = "tg_hair:head_04"
      },
      hairColor = {
        [0] = "tg_hair:head_14",
        [1] = "tg_hair:head_15",
        [2] = "tg_hair:head_13",
        [3] = "tg_hair:head_11",
        [4] = "tg_hair:head_12"
      },
      faceModel = {
        [0] = "tg_face:head_13",
        [1] = "tg_face:head_14",
        [2] = "tg_face:head_15"
      }
    },
    ["biography"] = {
      menpaiPic = "tg_chuangjue01:chuangjue01_05",
      proviewText = "    神农于幽兰山坳创立沉香谷，并广收战乱孤儿传医教术。沉香谷中弟子均谨记神农教诲，渡生救世，她们医术精湛，总是毫无报偿为生病的人们行医布药，一时悬壶济世的名声广为流传。",
      mainAttribute = "智力",
      fightTendency = "远程治疗",
      teamTendency = "治疗辅助",
      fightPic = "tg_chuangjue02:chuangjue02_16",
      recommend = 0,
      operdiff = 3.5,
      male_enable = false,
      female_enable = true
    },
    ["avatar"] = {
      mx = 0,
      my = 0,
      mz = 0,
      mscale = 3,
      mrotation = 110,
      fx = 197.02,
      fy = 23.6,
      fz = 188.844,
      fscale = 2,
      frotation = 90,
      showx = 189.859,
      showy = 22.9346,
      showz = 187.365,
      sscale = 3,
      srotation = 110
    },
    ["create"] = {
      cx = 198.349,
      cy = 27.6206,
      cz = 207.281,
      dx = -0.361614,
      dy = 0.00140123,
      dz = -0.932327,
      rx = 189.859,
      ry = 22.9346,
      rz = 187.365,
      scale = 3,
      rotation = 110
    },
    ["animation"] = {
      _out1 = 105,
      _out2 = 109,
      show = 106,
      standard = 111,
      _in1 = 110,
      _in2 = 108,
      default = 107
    }
  },
  [4] = {
    ["sign"] = "UiIamge004:FeiImage_01",
    ["name"] = "UiIamge004:FeiImage_01",
    [0] = {
      icon = {
        [0] = "tg_TouXiang02:head_04",
        [1] = "tg_TouXiang02:head_04",
        ["maxnum"] = 2
      },
      hairModel = {
        [0] = "tg_hair:head_06",
        [1] = "tg_hair:head_05"
      },
      hairColor = {
        [0] = "tg_hair:head_14",
        [1] = "tg_hair:head_15",
        [2] = "tg_hair:head_13",
        [3] = "tg_hair:head_11",
        [4] = "tg_hair:head_12"
      },
      faceModel = {
        [0] = "tg_face:head_04",
        [1] = "tg_face:head_05",
        [2] = "tg_face:head_06"
      }
    },
    [1] = {
      icon = {
        [0] = "tg_TouXiang02:head_05",
        [1] = "tg_TouXiang02:head_05",
        ["maxnum"] = 2
      },
      hairModel = {
        [0] = "tg_hair:head_06",
        [1] = "tg_hair:head_05"
      },
      hairColor = {
        [0] = "tg_hair:head_14",
        [1] = "tg_hair:head_15",
        [2] = "tg_hair:head_13",
        [3] = "tg_hair:head_11",
        [4] = "tg_hair:head_12"
      },
      faceModel = {
        [0] = "tg_face:head_17",
        [1] = "tg_face:head_16",
        [2] = "tg_face:head_18"
      }
    },
    ["biography"] = {
      menpaiPic = "tg_chuangjue01:chuangjue01_04",
      proviewText = "   灵界羽山高贵的凤凰一脉，远古时诞下后裔建成飞翎族。飞翎族生性自由，喜欢在山野林间穿梭；他们的弓弦可乘风万里，精准的射击绝不放过任何一个猎物；他们的箭矢如同族群的名字，穿破了风冲过了云，直达天际。",
      mainAttribute = "敏捷",
      fightTendency = "远程、陷阱",
      teamTendency = "伤害输出",
      fightPic = "tg_chuangjue02:chuangjue02_15",
      recommend = 0,
      operdiff = 4,
      male_enable = false,
      female_enable = true
    },
    ["avatar"] = {
      mx = 176.506,
      my = 23,
      mz = 188.316,
      mscale = 3.1,
      mrotation = 140,
      fx = 181.611,
      fy = 23.1426,
      fz = 189.787,
      fscale = 3,
      frotation = 110,
      showx = 189.859,
      showy = 22.9346,
      showz = 187.365,
      sscale = 3,
      srotation = 110
    },
    ["create"] = {
      cx = 198.349,
      cy = 27.6206,
      cz = 207.281,
      dx = -0.361614,
      dy = 0.00140123,
      dz = -0.932327,
      rx = 189.859,
      ry = 22.9346,
      rz = 187.365,
      scale = 3,
      rotation = 110
    },
    ["animation"] = {
      _out1 = 105,
      _out2 = 109,
      show = 106,
      standard = 111,
      _in1 = 110,
      _in2 = 108,
      default = 107
    }
  },
  ["selectShare"] = {
    cx = 198.349,
    cy = 27.6206,
    cz = 207.281,
    dx = -0.361614,
    dy = 0.00140123,
    dz = -0.932327,
    rx = 189.859,
    ry = 22.9346,
    rz = 187.365,
    scale = 3,
    rotation = 110
  },
  ["previewShare"] = {
    cx = 198.349,
    cy = 27.6206,
    cz = 207.281,
    dx = -0.361614,
    dy = 0.00140123,
    dz = -0.932327
  }
}
gUI_RELATION_POINT_TO_CHAT = 0
gUI_STALL_INFO = {
  [0] = "托管摆摊过期,请到#c0080C0#aB[绑定元宝商]{86 79 2}#aC#W处回收",
  [1] = "#c0080C0#aB[自己的摊位]{%d %d %d}#aC"
}
gUI_GEM_INEFF_DES = {
  [1] = "你没有选择属性，取出主神玉后会默认选择主神玉原有属性，是否确定继续？",
  [2] = "选择主神玉的原有属性，",
  [3] = "你选择主神玉的注灵后的属性，"
}
gUI_HOLY_WEAP_STATE = {
  [-1] = "",
  [0] = "已激活",
  [1] = "未激活",
  [2] = "未开启",
  [3] = "不是神器",
  [4] = "无需激活"
}
gUI_HOLY_WEAP_STATE_COLOR = {
  [-1] = "FF000000",
  [0] = "FFFF00FF",
  [1] = "FF98794A",
  [2] = "FF5F5644",
  [3] = "FFFFFFFF",
  [4] = "FFE1B97B"
}
gUI_HOLY_WEAP_STATE_COLOR_NUMBER = {
  [-1] = 4278190080,
  [0] = 4294902015,
  [1] = 4288182602,
  [2] = 4284438084,
  [3] = 4294967295,
  [4] = 4292983163
}
gUI_SKILL_PROGRESS_DES = {
  [51] = "宠物捕捉中",
  [52] = "宠物召唤中",
  [455] = "宠物捕捉中",
  [456] = "宠物捕捉中"
}
gUI_PARTNER_POS = {
  myself = 0,
  other = 1,
  trade = 2,
  stall = 3,
  auction = 4,
  rank = 5,
  relevel = 6,
  evolve = 7,
  refactor = 8,
  nourish = 9,
  realm = 10,
  apperceive = 11,
  breed_single = 12,
  learn_skill = 13,
  forget_skill = 14,
  manual_skill = 15,
  shopOtherStall = 16
}
gUI_GUILD_POSITION = {
  member = 1,
  elite = 2,
  minister = 3,
  elder = 4,
  viceleader = 5,
  leader = 6
}
gUI_GUILD_POSITION_NAME = {
  "帮众",
  "精英",
  "堂主",
  "长老",
  "副帮主",
  "帮主"
}
gUI_GUILD_BUILDING_NAME = {
  "帮会主建筑",
  "厢房",
  "温泉",
  "商店",
  "任务",
  "BOSS",
  "书房"
}
gUI_GUILD_BOSS_CALL_STR = "挑战%s可在%s晚上18:00至23:00之间#r召唤,必需帮主或副帮主职位可进行召唤,并#r且每周只可召唤1次,\n                           成功召唤后将会在帮会领地中#r[125：194]坐标生成该Boss,击倒后将掉落大量宝箱,快召集帮众共同挑战吧.\n                           #r（注:如果在24:00前Boss未击倒则该Boss将逃跑）"
gUI_GUILD_INVITE_LEVEL = 1
gUI_GUILD_SIEGE_CITY = {
  "蓬莱",
  "昆仑",
  "黑水"
}
gUI_Mount_Rank = {
  [-1] = "",
  [0] = "人阶",
  [1] = "地阶",
  [2] = "天阶",
  [3] = "仙阶",
  [4] = "神阶",
  [5] = "圣阶",
  [6] = "人阶",
  [7] = "地阶",
  [8] = "天阶",
  [9] = "仙阶",
  [10] = "神阶",
  [11] = "圣阶",
  [12] = "圣阶"
}
gUI_Color_GemBlend = {
  [0] = "FF00FF00",
  [1] = "FF00FF00",
  [2] = "FF00FF00",
  [3] = "FF1E90FF",
  [4] = "FFC500FF",
  [5] = "FFFFFF00",
  [6] = "FFFF8C00"
}
gUI_RELATION_TYPE = {
  friend = 1,
  blacklist = 2,
  enemy = 3
}
gUI_DropItemSound = {
  [1] = "Receivemsg.wav"
}
gUI_QuestRewardItemSourceID = {
  QUEST_WINDOW = 0,
  NPC_GOSSIP = 1,
  MAP_TOOLTIP = 2,
  CHAT_QUEST_LINK = 3,
  INVALID = -1,
  COMMISSION_WINDOW = 4,
  GUILD_QUEST_WINDOW = 5
}
gUI_MoneyPic = {
  g = "UiIamge004:Image_gold",
  s = "UiIamge004:Image_ag",
  c = "UiIamge004:Image_cu"
}
gUI_Chara_AttrTip = {
  [-1] = {des1 = "", des2 = ""},
  [1] = {
    des1 = "",
    des2 = "灵力点上限为1000点,在聚灵时可以获得,每分钟获得1点灵力,到修炼使者处消耗灵力进行修炼(修炼可以获得大量经验和精魄)"
  },
  [2] = {
    des1 = "",
    des2 = "可以找到主城的修炼使者消耗修炼点和灵力值、修炼果进行修炼，修炼可以获得大量经验。前一天没有用完的活力值,会转换为修炼点数。修炼点上限为1000点。"
  },
  [3] = {
    des1 = "",
    des2 = "活力值上限为120点,每天6点重置活力值。某些日常的任务和活动在完成时需要消耗活力值。当天没有用完的活力值会转化成修炼点。"
  },
  [4] = {
    des1 = "",
    des2 = "侠义值可以到侠义值商人处兑换精美道具(获得方式：超过副本推荐等级的玩家带推荐等级内的玩家击杀白羽之巅和堕龙台副本的怪物可以获得；每天最多可以获得3000点，上限为1000000点)"
  },
  [5] = {
    des1 = "",
    des2 = "经验的获得受到人物等级影响"
  },
  [6] = {
    des1 = "",
    des2 = "恶意击倒玩家会增加善恶值，善恶值达到112时自己名字会变黄色、达到600时名字变红色、达到900时名字变紫色。黄名后被其他玩家击倒，其他玩家不增加善恶值；红名后不能通过传送师进行传送，并且被其他玩家击倒后，有几率掉落身上或背包中的防具或首饰； 紫名除了红名的惩罚外，死亡后100%几率掉落装备且不能使用职业技能。"
  },
  [7] = {
    des1 = "",
    des2 = "当前装备品质总分"
  },
  [8] = {
    des1 = "",
    des2 = "通过帮会的任务、活动和捐献获得。"
  },
  [9] = {
    des1 = "",
    des2 = "聚灵时每分钟获得1点灵力值，上限为1000点。"
  },
  [10] = {
    des1 = "",
    des2 = "侠义值可以到侠义值商人处兑换精美道具。"
  },
  [11] = {
    des1 = "",
    des2 = "被其他玩家天诛后，会获得10点天诛值，当天诛值达到100后，会减少攻击和防御还会降低移动速度。超过100后，每10分钟会消除1点天诛值，直至到99点止。天诛值上限为300点。"
  },
  [12] = {
    des1 = "",
    des2 = "在使用仙灵类道具时需要消耗念力值，念力值每在线1分钟获得1点，上限为360点。"
  },
  [13] = {
    des1 = "",
    des2 = "参加战场可获得荣誉值，单场最高可获得1000点，每日最高可获得5000点，荣誉值可在战场管理员处兑换奖励。"
  },
  [14] = {
    des1 = "",
    des2 = "击倒其他玩家的总次数"
  },
  [15] = {
    des1 = "",
    des2 = "今日击倒其他玩家的次数"
  },
  [16] = {
    des1 = "",
    des2 = "本周参加战场获得胜利的次数，胜利次数超过10次即可进入战场排行榜，战场排行榜每周周一结算奖励，排行前30名的角色获得特殊时限坐骑。"
  }
}
gUI_SpecialGuideInfo = {
  [1] = {
    [0] = {
      pciture = "UiIamge024:Image_ForgeOne"
    },
    [1] = {
      pciture = "UiIamge025:Image_ForgeTwo"
    }
  },
  [2] = {
    [0] = {
      pciture = "UiIamge022:Image_BlendOne"
    },
    [1] = {
      pciture = "UiIamge023:Image_BlendTwo"
    }
  },
  [3] = {
    [0] = {
      pciture = "UiIamge026:Image_FaerieOne"
    },
    [1] = {
      pciture = "UiIamge027:Image_FaerieTwo"
    },
    [2] = {
      pciture = "UiIamge028:Image_FaerieThree"
    }
  },
  [4] = {
    [0] = {
      pciture = "UiIamge029:Image_HomeLandOne"
    },
    [1] = {
      pciture = "UiIamge030:Image_HomeLandTwo"
    },
    [2] = {
      pciture = "UiIamge035:Image_HomeLandThree"
    },
    [3] = {
      pciture = "UiIamge031:Image_HomeLandFour"
    }
  },
  [5] = {
    [0] = {
      pciture = "UiIamge032:Image_SkillOne"
    },
    [1] = {
      pciture = "UiIamge033:Image_SkillTwo"
    },
    [2] = {
      pciture = "UiIamge034:Image_SkillThree"
    }
  }
}
gUI_MainBtn_ActiveFlag = {
  AmassLevel = 30,
  NimbusLevel = 1,
  BlendLevel = 12,
  SignatureLevel = 10,
  StallLevel = 30,
  ResovleLevel = 30,
  ManufaceLevel = 30,
  ActiveCheckLev = 25,
  ForgeLevel = 20,
  IneffLevel = 0
}
gUI_PK_Level = 26
gUI_PK_Deul = 21
gUI_Faerie_LvName = {
  [1] = "炼气",
  [2] = "筑基",
  [3] = "结丹",
  [4] = "元婴",
  [5] = "化神",
  [6] = "合体",
  [7] = "大乘",
  [8] = "飞仙"
}
function spairs(map, sortfunction)
  local keys = {}
  for mapid in pairs(map) do
    table.insert(keys, mapid)
  end
  table.sort(keys, sortfunction)
  local i = 0
  local function iter()
    i = i + 1
    if keys == nil or keys[i] == nil then
      return nil, nil
    else
      return keys[i], map[keys[i]]
    end
  end
  return iter
end
function Lua_UI_TranslateText(Text)
  return string.gsub(Text, "%$(.-)%$", function(s)
    return gUI_MainPlayerAttr[s]
  end)
end
function Lua_UI_GetGoldColor(go, alh)
  local color = "FFFFFF"
  if go == nil then
  elseif go >= 10 and go < 100 then
    color = "00FF00"
  elseif go >= 100 and go < 1000 then
    color = "00FFFF"
  elseif go >= 1000 and go < 10000 then
    color = "FFFF00"
  elseif go >= 10000 and go < 100000 then
    color = "FF8C00"
  elseif go >= 100000 then
    color = "800080"
  end
  if alh then
    return "FF" .. color
  end
  return color
end
function Lua_UI_Niubus2String(Niubus, IsRed)
  local SNiubus = tostring(Niubus)
  if IsRed then
    local _, _, SelfNiubus, _, _, _, _, lingli, _, _, xiayi = GetPlaySelfProp(4)
    if Niubus > SelfNiubus then
      SNiubus = "{#C^" .. tostring(colorRed) .. "^" .. tostring(Niubus) .. "}"
    else
      SNiubus = "{#C^" .. tostring(colorGreen) .. "^" .. tostring(Niubus) .. "}"
    end
  end
  return SNiubus
end
function Lua_UI_Money2String(money, IsRed, tipFormat, HasZero, IsAllMoney, newMoney, event, forceColor)
  if money == nil then
    return
  end
  local go = math.floor(money / 10000)
  local ag = math.floor((money - math.floor(money / 10000) * 10000) / 100)
  local cu = money - math.floor(money / 100) * 100
  local color = Lua_UI_GetGoldColor(go)
  local str_go = ""
  local str_ag = ""
  local str_cu = ""
  if IsRed then
    local bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
    if event == "PLAYER_MONEY_CHANGED" then
      nonBindemoney = newMoney
    end
    if money > nonBindemoney then
      color = colorRed
    end
  end
  if IsAllMoney then
    local bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
    if event == "PLAYER_MONEY_CHANGED" then
      nonBindemoney = newMoney
    elseif event == "PLAYER_BIND_MONEY_CHANGED" then
      bindMoney = newMoney
    end
    if money > nonBindemoney + bindMoney then
      color = colorRed
    end
  end
  if forceColor ~= nil then
    color = forceColor
  end
  if not tipFormat then
    str_go = string.format("{#C^%s^%d}{ImAgE^%s}", color, go, gUI_MoneyPic.g)
    str_ag = string.format("{#C^%s^%d}{ImAgE^%s}", color, ag, gUI_MoneyPic.s)
    str_cu = string.format("{#C^%s^%d}{ImAgE^%s}", color, cu, gUI_MoneyPic.c)
  else
    str_go = string.format("%dg", go)
    str_ag = string.format("%ds", ag)
    str_cu = string.format("%dc", cu)
  end
  if not HasZero then
    if go <= 0 then
      str_go = ""
    end
    if ag <= 0 then
      str_ag = ""
    end
    if cu <= 0 and (go > 0 or ag > 0) then
      str_cu = ""
    end
  end
  local result = str_go .. str_ag .. str_cu
  return result
end
function Lua_UI_String2Money(PicStrMoney)
  local money = 0
  local colorStrSize = 12
  local AuFIdx, AuEIdx = string.find(PicStrMoney, "}{ImAgE^" .. gUI_MoneyPic.g .. "}")
  if AuFIdx then
    local strAu = string.sub(PicStrMoney, colorStrSize, AuFIdx - 1)
    money = tonumber(strAu) * 10000
  end
  local AgFIdx, AgEIdx = string.find(PicStrMoney, "}{ImAgE^" .. gUI_MoneyPic.s .. "}")
  if AgFIdx then
    local strAg
    if AuEIdx then
      strAg = string.sub(PicStrMoney, AuEIdx + colorStrSize, AgFIdx - 1)
    else
      strAg = string.sub(PicStrMoney, colorStrSize, AgFIdx - 1)
    end
    money = money + tonumber(strAg) * 100
  end
  local CuFIdx, CuEIdx = string.find(PicStrMoney, "}{ImAgE^" .. gUI_MoneyPic.c .. "}")
  if CuFIdx then
    local strCu
    if AgEIdx then
      strCu = string.sub(PicStrMoney, AgEIdx + colorStrSize, CuFIdx - 1)
    elseif AuEIdx then
      strCu = string.sub(PicStrMoney, AuEIdx + colorStrSize, CuFIdx - 1)
    else
      strCu = string.sub(PicStrMoney, colorStrSize, CuFIdx - 1)
    end
    money = money + tonumber(strCu)
  end
  return money
end
function Lua_UI_FormatMoneyByEdit(num)
  local gold = 0
  local silver = 0
  local copper = 0
  gold = math.floor(num / 10000)
  num = num % 10000
  silver = math.floor(num / 100)
  copper = num % 100
  return gold, silver, copper
end
function Lua_UI_ShowMessageInfo(Text)
  ShowErrorMessage(Text)
end
function Lua_UI_ReplaceStr(str)
  str = string.gsub(str, "%$(.-)%$", function(s)
    return gUI_MainPlayerAttr[s]
  end)
  return str
end
function Lua_UI_ExecuteStr(str)
  str = string.gsub(str, "%$(.-)%$", function(s)
    return loadstring("return " .. s)()
  end)
  return str
end
function Lua_UI_ExecuteStr2(str)
  str = string.gsub(str, "%$(.-)%$", function(s)
    return loadstring("return " .. s)()
  end)
  return loadstring("return " .. str)()
end
function Lua_UI_PlayMouseClickSound(file)
  if file and file ~= "" then
    WorldStage:playSound(file)
  else
    WorldStage:playSound("Clickbutton.wav")
  end
end
function Lua_UI_LoseFouse(wndName)
  local wnd = WindowSys_Instance:GetWindow("ChanFiltem.ChanFiltem_bg")
  if wnd == nil or "ChanTemp_cbtn" == wndName or "ChanTemp_cnt" == wndName or "Channel_btn" == wndName then
  else
    wnd:SetProperty("Visible", "false")
  end
end
function Lua_UI_PlayDropItemSound(msg)
  local wnd = msg:get_window()
  local offset = msg:get_wparam()
  wnd = WindowToGoodsBox(wnd)
  local wnd1 = wnd:GetDragItemParent()
  if wnd1 then
    local boxtype = wnd1:GetProperty("BoxType")
    local boxtypeIndex = gUI_GOODSBOX_BAG[boxtype]
    if boxtypeIndex ~= nil and boxtypeIndex ~= -1 then
      LogMsg("play_drop_item_sound,item name=" .. tostring(name))
    end
  end
end
function Lua_UI_NumberToHanzi(num)
  if num == 0 then
    return _UI_Number_Str[0]
  else
    local remainder = num
    for i = 9, 1, -1 do
      local divisor = math.pow(10, i - 1)
      _UI_Temp[i] = math.floor(remainder / divisor)
      remainder = remainder % divisor
    end
    local str = ""
    local zero_seq_len = 0
    for bit = 1, 9 do
      local n = _UI_Temp[bit]
      if n == 0 then
        zero_seq_len = zero_seq_len + 1
        if _UI_Temp[bit + 1] and _UI_Temp[bit + 1] ~= 0 and zero_seq_len ~= bit then
          str = _UI_Number_Str[0] .. str
        end
      else
        local offset = bit % 4
        if offset == 2 and n == 1 then
          str = _UI_Number_Str[10] .. str
        elseif offset == 1 then
          str = _UI_Number_Str[n] .. _UI_Unit1[offset + 1] .. _UI_Unit2[math.floor(bit / 4) + 1] .. str
        else
          str = _UI_Number_Str[n] .. _UI_Unit1[offset + 1] .. str
        end
        zero_seq_len = 0
      end
    end
    return str
  end
end
function Lua_UI_ClickRadioButton(msg)
  local wnd = msg:get_window()
  wnd = WindowToCheckButton(wnd)
  if wnd:IsChecked() then
    local parent = wnd:GetParent()
    local name = wnd:GetProperty("WindowName")
    local edit1 = parent:GetGrandChild("Name.Edit")
    local edit2 = parent:GetGrandChild("Number.Edit")
    local button, other
    if name == "Radio1" then
      other = "Radio2"
      WindowSys_Instance:SetKeyboardCaptureWindow(edit1)
      edit2:SetProperty("Text", "")
    else
      other = "Radio1"
      WindowSys_Instance:SetKeyboardCaptureWindow(edit2)
      edit1:SetProperty("Text", "")
    end
    button = parent:GetChildByName(other)
    button = WindowToCheckButton(button)
    button:SetCheckedState(false)
  else
    wnd:SetCheckedState(true)
  end
end
function Lua_UI_ClickEditBox(msg)
  local wnd = msg:get_window()
  WindowSys_Instance:SetKeyboardCaptureWindow(wnd)
  local parent = wnd:GetParent()
  local is_edit1 = parent:GetProperty("WindowName") == "Name"
  local grand = parent:GetParent()
  local radio1 = grand:GetChildByName("Radio1")
  radio1 = WindowToCheckButton(radio1)
  radio1:SetChecked(is_edit1)
  local radio2 = grand:GetChildByName("Radio2")
  radio2 = WindowToCheckButton(radio2)
  radio2:SetChecked(not is_edit1)
end
function Lua_UI_Close(msg)
  local wnd = msg:get_window():GetParent()
  wnd:SetProperty("Visible", "false")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function Lua_UI_DealRadioButton(msg, stateArry)
  if msg == nil then
    return
  end
  local wnd = msg:get_window()
  wnd = WindowToCheckButton(wnd)
  local curName = wnd:GetProperty("WindowName")
  if wnd:IsChecked() then
    local parent = wnd:GetParent()
    for name, isSelect in pairs(stateArry) do
      if isSelect and name ~= curName then
        local button = parent:GetChildByName(name)
        button = WindowToCheckButton(button)
        button:SetCheckedState(false)
        stateArry[name] = false
      end
    end
  else
    wnd:SetCheckedState(true)
  end
  stateArry[curName] = true
end
function Lua_UI_Formattime(time)
  local timestr
  if time > 0 then
    if time > 86400 then
      timestr = string.format("%d天", math.ceil(time / 86400))
    elseif time > 3600 then
      local hour = math.ceil(time / 3600)
      timestr = string.format("%d小时", hour)
    else
      timestr = "{#R^不足1小时}"
    end
  else
    timestr = "{#R^已过期}"
  end
  return timestr
end
function Lua_UI_DetailTime(time)
  local timestr = ""
  if time > 0 then
    if time > 86400 then
      local day = math.floor(time / 86400)
      timestr = string.format("%d天", day)
      local remainTime = time - day * 86400
      if remainTime > 0 then
        local hour = math.floor(remainTime / 3600)
        timestr = timestr .. string.format("%d小时", hour)
        remainTime = remainTime - hour * 3600
        if remainTime > 0 then
          local minute = math.floor(remainTime / 60)
          timestr = timestr .. string.format("%d分钟", minute)
        end
      end
    elseif time > 3600 then
      local hour = math.floor(time / 3600)
      timestr = string.format("%d小时", hour)
      local remainTime = time - hour * 3600
      if remainTime > 0 then
        local minute = math.floor(remainTime / 60)
        timestr = timestr .. string.format("%d分钟", minute)
      end
    elseif time > 60 then
      local minute = math.floor(time / 60)
      timestr = timestr .. string.format("%d分钟", minute)
    else
      timestr = string.format("{#R^%d秒}", time)
    end
  else
    timestr = "{#R^已过期}"
  end
  return timestr
end
function Lua_UI_StringFormatNum(num)
  local numf = string.format("%.1f", num)
  local numd = string.format("%d", num)
  if tonumber(numf) == tonumber(numd) then
    return string.format("%d", num)
  end
  return string.format("%.2f", num)
end
function Lua_UI_cmd_help()
end
function Lua_UI_cmd_hidedebug()
  WorldStage:showDebugInfo(false)
end
function Lua_UI_cmd_showdebug()
  WorldStage:showDebugInfo(true)
end
function Lua_UI_cmd_showui()
  WorldStage:showUI(true)
end
function Lua_UI_cmd_hideui()
  WorldStage:showUI(false)
end
function Lua_UI_cmd_moveto(arg1, arg2, arg3)
  local strMapName, mapid = GetCurrentMiniMapInfo()
  Teleport(mapid, arg1, arg2, arg3)
end
function Lua_UI_GetWebURL(sceneNodeID)
  local name = UIConfig:getAccountName()
  local url = "http://pay.iwgame.com/sc/g/auth.do?gameNodeId=" .. sceneNodeID .. "&identityId=" .. name .. "&random=" .. math.random(1, 999999)
  UIInterface:getWebURLCallBack(url, 800, 600)
end
