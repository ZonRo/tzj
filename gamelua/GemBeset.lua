if gUI and not gUI.GemBeset then
  gUI.GemBeset = {}
end
local GemBesetPlace = -1
local GEMBESETMAX = 3
GEMBESETSLOTCOMP = {
  EQUIPSLOT = 0,
  MATERIALSLOT = 1,
  MATERIALSLOT1 = 2,
  MATERIALSLOT2 = 3
}
function _OnGemBeset_Show()
  _GemBeset_Show()
end
function _GemBeset_Cancel()
  for i = 0, 2 do
    gUI.GemBeset.SubBox[i]:SetEnable(true)
    gUI.GemBeset.SubBox[i]:ClearGoodsItem(0)
    gUI.GemBeset.SubBox[i]:SetProperty("Visible", "false")
    gUI.GemBeset.SubBox[i]:SetCustomUserData(0, -1)
    gUI.GemBeset.SubBox[i]:SetEnable(false)
  end
  gUI.GemBeset.MainBox:ClearGoodsItem(0)
  gUI.GemBeset.ShowTip:RemoveAllItems()
end
function _GemBeset_Show()
  _GemBeset_Cancel()
  gUI.GemBeset.Root:SetProperty("Visible", "true")
  Lua_Bag_ShowUI(true)
end
function UI_GemBeset_DropItem(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_type = to_win:GetProperty("BoxType")
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  local toName = to_win:GetProperty("WindowName")
  if from_type == "backpack0" then
    local GemGuid = GetGemItemGuids(gUI_GOODSBOX_BAG[from_type], slot)
    if toName == "GoodsBox3_gbox" then
      if not gUI.GemBeset.SubBox[0]:IsEnable() then
      else
        SetGemBeset(gUI_GOODSBOX_BAG[from_type], slot, OperateMode.Add, GEMBESETSLOTCOMP.MATERIALSLOT)
      end
    elseif toName == "GoodsBox2_gbox" then
      if not gUI.GemBeset.SubBox[1]:IsEnable() then
      else
        SetGemBeset(gUI_GOODSBOX_BAG[from_type], slot, OperateMode.Add, GEMBESETSLOTCOMP.MATERIALSLOT1)
      end
    elseif toName ~= "GoodsBox4_gbox" or not gUI.GemBeset.SubBox[1]:IsEnable() then
    else
      SetGemBeset(gUI_GOODSBOX_BAG[from_type], slot, OperateMode.Add, GEMBESETSLOTCOMP.MATERIALSLOT2)
    end
  end
end
function UI_GemEmbed_HelpBtnClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("GemBeset.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_GemEm_GemRC(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_type = to_win:GetProperty("BoxType")
  local slot = msg:get_wparam()
  local toName = to_win:GetProperty("WindowName")
  if toName == "GoodsBox3_gbox" then
    if not gUI.GemBeset.SubBox[0]:IsEnable() then
    else
      SetGemBeset(1, 0, OperateMode.Remove, GEMBESETSLOTCOMP.MATERIALSLOT)
    end
  elseif toName == "GoodsBox2_gbox" then
    if not gUI.GemBeset.SubBox[1]:IsEnable() then
    else
      SetGemBeset(1, 0, OperateMode.Remove, GEMBESETSLOTCOMP.MATERIALSLOT1)
    end
  elseif toName ~= "GoodsBox4_gbox" or not gUI.GemBeset.SubBox[1]:IsEnable() then
  else
    SetGemBeset(1, 0, OperateMode.Remove, GEMBESETSLOTCOMP.MATERIALSLOT2)
  end
end
function UI_GemBeset_DropItemEquip(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_type = to_win:GetProperty("BoxType")
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    slot = Lua_Bag_GetRealSlot(from_win, slot)
    GemBeset_SetMsgToServer(1, slot)
  end
end
function GemBeset_SetMsgToServer(bag, slot)
  if bag == 1 then
    local name, icon, quality, minLevel, job, sex, isUnique, canBank, canSell, canDiscard, bindinfo, count, maxCount, itemType = GetItemInfoBySlot(bag, slot, 0)
    if itemType ~= 2 then
      Lua_Chat_ShowOSD("GEM_EMBED_EQUIP")
      return
    end
    SetGemBeset(bag, slot, OperateMode.Add, GEMBESETSLOTCOMP.EQUIPSLOT)
  end
end
function GemBeset_SetMaterMsgToServer(bag, slot)
  if not gUI.GemBeset.MainBox:IsItemHasIcon(0) then
    Lua_Chat_ShowOSD("GEM_EMBEN_EQUIPFRIST")
    return
  end
  if bag == 1 then
    local equipIcon, bagType, bagslot, nbindinfo, GemId0, GemIcon0, GemType0, GemValue0, GemId1, GemIcon1, GemType1, GemValue1, GemId2, GemIcon2, GemType2, GemValue2, nBesetValue = GetGemBesetEquipInfo()
    if gUI.GemBeset.SubBox[GEMBESETSLOTCOMP.MATERIALSLOT]:IsEnable() and nBesetValue > 0 then
      SetGemBeset(bag, slot, OperateMode.Add, GEMBESETSLOTCOMP.MATERIALSLOT)
    elseif gUI.GemBeset.SubBox[GEMBESETSLOTCOMP.MATERIALSLOT1]:IsEnable() and nBesetValue > 1 then
      SetGemBeset(bag, slot, OperateMode.Add, GEMBESETSLOTCOMP.MATERIALSLOT1)
    elseif gUI.GemBeset.SubBox[GEMBESETSLOTCOMP.MATERIALSLOT2]:IsEnable() and nBesetValue > 2 then
      SetGemBeset(bag, slot, OperateMode.Add, GEMBESETSLOTCOMP.MATERIALSLOT2)
    end
  end
end
function _OnGemBeset_Updata(index, slot)
  if index == 1 then
    if slot == 0 then
      local equipIcon, bagType, bagslot, nbindinfo, GemId0, GemIcon0, GemType0, GemValue0, GemId1, GemIcon1, GemType1, GemValue1, GemId2, GemIcon2, GemType2, GemValue2, nBesetValue = GetGemBesetEquipInfo()
      _GemBeset_Cancel()
      gUI.GemBeset.MainBox:SetItemGoods(0, equipIcon, Lua_Bag_GetQuality(bagType, bagslot))
      local intenLev = Lua_Bag_GetStarInfo(bagType, bagslot)
      if 0 < ForgeLevel_To_Stars[intenLev] then
        gUI.GemBeset.MainBox:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
      else
        gUI.GemBeset.MainBox:SetItemStarState(0, 0)
      end
      for i = 0, nBesetValue - 1 do
        gUI.GemBeset.SubBox[i]:SetProperty("Visible", "true")
      end
      if GemId0 ~= 0 then
        gUI.GemBeset.SubBox[0]:SetEnable(false)
        gUI.GemBeset.SubBox[0]:SetItemGoods(0, GemIcon0, Lua_Bag_GetQuality(-1, GemId0))
        gUI.GemBeset.SubBox[0]:SetCustomUserData(0, GemId0)
      else
        gUI.GemBeset.SubBox[0]:SetEnable(true)
        gUI.GemBeset.SubBox[0]:ClearGoodsItem(0)
        gUI.GemBeset.SubBox[0]:SetCustomUserData(0, -1)
      end
      if GemId1 ~= 0 then
        gUI.GemBeset.SubBox[1]:SetEnable(false)
        gUI.GemBeset.SubBox[1]:SetItemGoods(0, GemIcon1, Lua_Bag_GetQuality(-1, GemId1))
        gUI.GemBeset.SubBox[1]:SetCustomUserData(0, GemId1)
      else
        gUI.GemBeset.SubBox[1]:SetEnable(true)
        gUI.GemBeset.SubBox[1]:ClearGoodsItem(0)
        gUI.GemBeset.SubBox[1]:SetCustomUserData(0, -1)
      end
      if GemId2 ~= 0 then
        gUI.GemBeset.SubBox[2]:SetEnable(false)
        gUI.GemBeset.SubBox[2]:SetItemGoods(0, GemIcon2, Lua_Bag_GetQuality(-1, GemId2))
        gUI.GemBeset.SubBox[2]:SetCustomUserData(0, GemId2)
      else
        gUI.GemBeset.SubBox[2]:SetEnable(true)
        gUI.GemBeset.SubBox[2]:ClearGoodsItem(0)
        gUI.GemBeset.SubBox[2]:SetCustomUserData(0, -1)
      end
      local attrList = {}
      local attrList = GetEquipExAttr()
      GemBesetSetBlendAttr(attrList)
    elseif slot == 1 then
      local icon, bag, slot, isBindGem, nitemid, GemGuid = GetSubGemBesetInfo(1)
      for i = 0, 2 do
        if gUI.GemBeset.SubBox[i]:IsEnable() then
          gUI.GemBeset.SubBox[i]:ClearGoodsItem(0)
          gUI.GemBeset.SubBox[i]:SetCustomUserData(0, -1)
        end
      end
      gUI.GemBeset.SubBox[0]:SetItemGoods(0, icon, Lua_Bag_GetQuality(-1, nitemid))
      gUI.GemBeset.SubBox[0]:SetCustomUserData(0, nitemid)
      local attrList = {}
      local attrList = GetEquipExAttr()
      GemBesetSetBlendAttr(attrList)
    elseif slot == 2 then
      local icon, bag, slot, isBindGem, nitemid, GemGuid = GetSubGemBesetInfo(2)
      for i = 0, 2 do
        if gUI.GemBeset.SubBox[i]:IsEnable() then
          gUI.GemBeset.SubBox[i]:ClearGoodsItem(0)
          gUI.GemBeset.SubBox[i]:SetCustomUserData(0, -1)
        end
      end
      gUI.GemBeset.SubBox[1]:SetItemGoods(0, icon, Lua_Bag_GetQuality(-1, nitemid))
      gUI.GemBeset.SubBox[1]:SetCustomUserData(0, nitemid)
      local attrList = {}
      local attrList = GetEquipExAttr()
      GemBesetSetBlendAttr(attrList)
    elseif slot == 3 then
      local icon, bag, slot, isBindGem, nitemid, GemGuid = GetSubGemBesetInfo(3)
      for i = 0, 2 do
        if gUI.GemBeset.SubBox[i]:IsEnable() then
          gUI.GemBeset.SubBox[i]:ClearGoodsItem(0)
          gUI.GemBeset.SubBox[i]:SetCustomUserData(0, -1)
        end
      end
      gUI.GemBeset.SubBox[2]:SetItemGoods(0, icon, Lua_Bag_GetQuality(-1, nitemid))
      gUI.GemBeset.SubBox[2]:SetCustomUserData(0, nitemid)
      local attrList = {}
      local attrList = GetEquipExAttr()
      GemBesetSetBlendAttr(attrList)
    end
  elseif index == 2 then
    if slot == 0 then
      _GemBeset_Cancel()
    elseif slot == 1 then
      for i = 0, 2 do
        if gUI.GemBeset.SubBox[i]:IsEnable() then
          gUI.GemBeset.SubBox[i]:ClearGoodsItem(0)
        end
      end
      local attrList = {}
      local attrList = GetEquipExAttr()
      GemBesetSetBlendAttr(attrList)
    elseif slot == 2 then
      for i = 0, 2 do
        if gUI.GemBeset.SubBox[i]:IsEnable() then
          gUI.GemBeset.SubBox[i]:ClearGoodsItem(0)
        end
      end
      local attrList = {}
      local attrList = GetEquipExAttr()
      GemBesetSetBlendAttr(attrList)
    elseif slot == 3 then
      for i = 0, 2 do
        if gUI.GemBeset.SubBox[i]:IsEnable() then
          gUI.GemBeset.SubBox[i]:ClearGoodsItem(0)
        end
      end
      local attrList = {}
      local attrList = GetEquipExAttr()
      GemBesetSetBlendAttr(attrList)
    end
  elseif index == 3 then
    _GemBeset_Cancel()
  end
end
function UI_GemBeset_Comfirm()
  if not gUI.GemBeset.MainBox:IsItemHasIcon(0) then
    Lua_Chat_ShowOSD("GEM_EMBED_NO_EQUIP")
    return
  end
  local Num = -1
  for i = 0, 2 do
    if gUI.GemBeset.SubBox[i]:IsEnable() and gUI.GemBeset.SubBox[i]:IsItemHasIcon(0) then
      Num = i
    end
  end
  local _, _, _, isBindEquip = GetGemBesetEquipInfo()
  local _, _, _, isBindGem = GetSubGemBesetInfo(GEMBESETSLOTCOMP.MATERIALSLOT)
  local _, _, _, isBindGem1 = GetSubGemBesetInfo(GEMBESETSLOTCOMP.MATERIALSLOT1)
  local _, _, _, isBindGem2 = GetSubGemBesetInfo(GEMBESETSLOTCOMP.MATERIALSLOT2)
  local isBindGemInfo
  if isBindGem ~= nil then
    isBindGemInfo = isBindGem
  elseif isBindGem1 ~= nil then
    isBindGemInfo = isBindGem1
  elseif isBindGem2 ~= nil then
    isBindGemInfo = isBindGem2
  end
  if isBindGemInfo ~= nil and Num ~= -1 then
    Num = Num + 1
    DoGemBeset(Num)
    break
  else
  end
end
function UI_GemBeset_EquipRC()
  SetGemBeset(1, 0, OperateMode.Remove, GEMBESETSLOTCOMP.EQUIPSLOT)
end
function GemBesetSetBlendAttr(attrList)
  if attrList[0] == nil then
    return
  end
  gUI.GemBeset.ShowTip:RemoveAllItems()
  local _, _, _, _, _, _, attrtypes1, attrvlaues1 = GetSubGemBesetInfo(GEMBESETSLOTCOMP.MATERIALSLOT)
  local bag1, slot1 = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_EMBED, GEMBESETSLOTCOMP.MATERIALSLOT)
  for i = 0, table.maxn(attrList) do
    if attrList[i].name ~= nil then
      if Lua_Tip_NeedShowRate(attrList[i].typeId) then
        attrList[i].value = Lua_UI_StringFormatNum(attrList[i].value / 100)
      end
      local showStr = ""
      showStr = tostring(attrList[i].name) .. " " .. tostring(attrList[i].value)
      local listAttr = GetItemGemAttr(bag1, slot1, 0)
      if listAttr ~= nil then
        if listAttr[1].MainAttrValue ~= 0 and listAttr[1].MaintypeId == attrList[i].typeId then
          local newValue = math.floor(listAttr[1].MainAttrValue * attrList[i].value / 100)
          showStr = showStr .. "  {#G^+" .. tostring(newValue) .. "(" .. tostring(listAttr[1].MainAttrValue) .. "%)" .. "}"
        end
        for j = 0, listAttr[1].AttrCount - 1 do
          if listAttr[1][j].typeId == attrList[i].typeId then
            local newValue = listAttr[1][j].AttrValue * attrList[i].value / 100
            showStr = showStr .. string.format(" {#G^  +%.2f(%d %%)}", newValue, listAttr[1][j].AttrValue)
          end
        end
      end
      gUI.GemBeset.ShowTip:InsertString(showStr, -1)
    end
  end
end
function _OnGemBeset_Result()
  Lua_Chat_ShowOSD("GEM_EMBED_SUCC")
  SetGemBeset(1, 0, OperateMode.Cancel, GEMBESETSLOTCOMP.EQUIPSLOT)
  _GemBeset_Cancel()
  WorldStage:playSoundByID(6)
end
function _OnGemBeset_Close()
  _GemBeset_Cancel()
  gUI.GemBeset.Root:SetProperty("Visible", "false")
end
function UI_GemBeset_Close()
  _OnGemBeset_Close()
end
function UI_GemBeset_ShowEquipTooltip(msg)
  local icon, bag, slot = GetGemBesetEquipInfo()
  local tooltip = gUI.GemBeset.MainBox:GetToolTipWnd(0)
  if bag then
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
    Lua_Tip_ShowEquipCompare(gUI.GemBeset.MainBox, bag, slot)
  end
end
function UI_GemBeset_ShowGemTooltip(msg)
  local goodsbox = msg:get_window()
  goodsbox = WindowToGoodsBox(goodsbox)
  if goodsbox:GetItemIcon(0) ~= "" then
    local tooltip = goodsbox:GetToolTipWnd(0)
    local slot = goodsbox:GetItemData(0)
    local itemid = goodsbox:GetCustomUserData(0)
    local bag1, slot1 = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_EMBED, GEMBESETSLOTCOMP.MATERIALSLOT)
    Lua_Tip_Item(tooltip, slot1, bag1, nil, nil, nil)
  end
end
function Script_GemBeset_OnEvent(event)
end
function Script_GemBeset_OnHide()
  _GemBeset_Cancel()
  SetGemBeset(1, 0, OperateMode.Cancel, GEMBESETSLOTCOMP.EQUIPSLOT)
  gUI.GemBeset.Root:SetProperty("Visible", "false")
end
function Script_GemBeset_OnLoad()
  gUI.GemBeset.Root = WindowSys_Instance:GetWindow("GemBeset")
  gUI.GemBeset.ShowTip = WindowToListBox(WindowSys_Instance:GetWindow("GemBeset.Tooltip1_lbox"))
  gUI.GemBeset.MainBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemBeset.GoodsBox1_gbox"))
  gUI.GemBeset.SubBox = {}
  gUI.GemBeset.SubBox[0] = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemBeset.GoodsBox3_gbox"))
  gUI.GemBeset.SubBox[1] = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemBeset.GoodsBox2_gbox"))
  gUI.GemBeset.SubBox[2] = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemBeset.GoodsBox4_gbox"))
end
