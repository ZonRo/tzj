local _AutoPick = {
  root = nil,
  autopickAll = nil,
  autoPickSwitch = nil,
  autoPickDetail = nil,
  autoEquipPick = nil,
  autoTaskPick = nil,
  autoRmbPick = nil,
  autoTailManPick = nil,
  autoComItemPick = nil,
  autoMaterialPick = nil,
  autoOtherPick = nil,
  miniWnd = nil
}
local mapIndexToWnd = {}
local Qulity = {
  [0] = "白色品质",
  [1] = "绿色品质",
  [2] = "蓝色品质",
  [3] = "紫色品质",
  [4] = "黄色品质",
  [5] = "橙色品质"
}
function _AutoPick:Init()
  self.root = WindowSys_Instance:GetWindow("Detail.Autopickset")
  local root = self.root
  local main = root:GetChildByName("Main1")
  self.autopickAll = main:GetChildByName("All")
  self.autopickAll = WindowToCheckButton(self.autopickAll)
  self.autoPickSwitch = main:GetChildByName("Open")
  self.autoPickSwitch = WindowToCheckButton(self.autoPickSwitch)
  self.autoPickDetail = main:GetChildByName("Customize")
  self.autoPickDetail = WindowToCheckButton(self.autoPickDetail)
  local cmbbox, tempWnd
  local vice1 = root:GetChildByName("Vice1")
  self.autoEquipPick = vice1:GetChildByName("Equip")
  self.autoEquipPick = WindowToCheckButton(self.autoEquipPick)
  tempWnd = self.autoEquipPick
  cmbbox = WindowToComboBox(tempWnd:GetChildByName("ComboBox"))
  cmbbox:RemoveAllItems()
  for i = 0, 5 do
    cmbbox:InsertString(Qulity[i], i)
  end
  self.autoTaskPick = vice1:GetChildByName("TaskItem")
  self.autoTaskPick = WindowToCheckButton(self.autoTaskPick)
  tempWnd = self.autoTaskPick
  cmbbox = WindowToComboBox(tempWnd:GetChildByName("ComboBox"))
  cmbbox:RemoveAllItems()
  for i = 0, 5 do
    cmbbox:InsertString(Qulity[i], i)
  end
  self.autoRmbPick = vice1:GetChildByName("RmbItem")
  self.autoRmbPick = WindowToCheckButton(self.autoRmbPick)
  tempWnd = self.autoRmbPick
  cmbbox = WindowToComboBox(tempWnd:GetChildByName("ComboBox"))
  cmbbox:RemoveAllItems()
  for i = 0, 5 do
    cmbbox:InsertString(Qulity[i], i)
  end
  self.autoTailManPick = vice1:GetChildByName("TalisMan")
  self.autoTailManPick = WindowToCheckButton(self.autoTailManPick)
  tempWnd = self.autoTailManPick
  cmbbox = WindowToComboBox(tempWnd:GetChildByName("ComboBox"))
  cmbbox:RemoveAllItems()
  for i = 0, 5 do
    cmbbox:InsertString(Qulity[i], i)
  end
  self.autoComItemPick = vice1:GetChildByName("ComItem")
  self.autoComItemPick = WindowToCheckButton(self.autoComItemPick)
  tempWnd = self.autoComItemPick
  cmbbox = WindowToComboBox(tempWnd:GetChildByName("ComboBox"))
  cmbbox:RemoveAllItems()
  for i = 0, 5 do
    cmbbox:InsertString(Qulity[i], i)
  end
  self.autoMaterialPick = vice1:GetChildByName("Material")
  self.autoMaterialPick = WindowToCheckButton(self.autoMaterialPick)
  tempWnd = self.autoMaterialPick
  cmbbox = WindowToComboBox(tempWnd:GetChildByName("ComboBox"))
  cmbbox:RemoveAllItems()
  for i = 0, 5 do
    cmbbox:InsertString(Qulity[i], i)
  end
  self.autoOtherPick = vice1:GetChildByName("Other")
  self.autoOtherPick = WindowToCheckButton(self.autoOtherPick)
  tempWnd = self.autoOtherPick
  cmbbox = WindowToComboBox(tempWnd:GetChildByName("ComboBox"))
  cmbbox:RemoveAllItems()
  for i = 0, 5 do
    cmbbox:InsertString(Qulity[i], i)
  end
  self.miniWnd = WindowSys_Instance:GetWindow("Autopick")
  mapIndexToWnd[0] = _AutoPick.autoTaskPick
  mapIndexToWnd[1] = _AutoPick.autoRmbPick
  mapIndexToWnd[2] = _AutoPick.autoEquipPick
  mapIndexToWnd[3] = _AutoPick.autoTailManPick
  mapIndexToWnd[4] = _AutoPick.autoComItemPick
  mapIndexToWnd[5] = _AutoPick.autoMaterialPick
  mapIndexToWnd[6] = _AutoPick.autoOtherPick
  mapIndexToWnd[13] = _AutoPick.autoPickDetail
  mapIndexToWnd[14] = _AutoPick.autopickAll
  mapIndexToWnd[15] = _AutoPick.autoPickSwitch
end
function _AutoPick:ShowMain()
  SetCurAutoPickInfoToUI()
  self.root:SetProperty("Visible", "true")
end
function _AutoPick:ShowMini()
  self.root:SetVisible(false)
  local miniWnd = self.miniWnd
  local activeWnd = WindowToTableCtrl(miniWnd:GetChildByName("Switch1"))
  local isActive = GetAutoPickInfoByIndex(15)
  if isActive then
    activeWnd:SetSelectedState(0)
  else
    activeWnd:SetSelectedState(1)
  end
  local mode1Wnd = WindowSys_Instance:GetWindow("Autopick.Mode1")
  local mode2Wnd = WindowSys_Instance:GetWindow("Autopick.Mode2")
  local isHasFreeSlot = IsHaveFreeBagSlot(1)
  if not isHasFreeSlot then
    mode1Wnd:SetProperty("Text", "包裹已满")
  else
    if GetAutoPickInfoByIndex(13) then
      mode1Wnd:SetProperty("Text", "自定义拾取中...")
    end
    if GetAutoPickInfoByIndex(14) then
      mode1Wnd:SetProperty("Text", "全部拾取中...")
    end
  end
  mode1Wnd:SetProperty("Visible", tostring(isActive))
  mode2Wnd:SetProperty("Visible", tostring(not isActive))
  miniWnd:SetProperty("Visible", "true")
end
function _AutoPick:OnSetInfo(index, isActive, nQulity, nMax, nMin)
  local wnd = mapIndexToWnd[index]
  if not wnd then
    return
  end
  local activeWnd = wnd
  if index <= 6 then
    if nQulity < 0 or nQulity > 5 then
      nQulity = 0
    end
    local qulityWnd = wnd:GetChildByName("ComboBox")
    qulityWnd = WindowToComboBox(qulityWnd)
    qulityWnd:SetSelectItem(nQulity)
    local maxWnd = wnd:GetGrandChild("Lev.Max")
    maxWnd:SetProperty("Text", tostring(nMax))
    local minWnd = wnd:GetGrandChild("Lev.Smal")
    minWnd:SetProperty("Text", tostring(nMin))
    activeWnd = WindowToCheckButton(wnd:GetChildByName("CheckButton"))
  end
  activeWnd:SetCheckedState(isActive)
  if index == 13 then
    local vice1 = self.root:GetChildByName("Vice1")
    vice1:SetProperty("Visible", tostring(isActive))
  end
end
function UI_AutoPick_SetDefaultClick(msg)
  SetAutoPickDefault()
end
function UI_AutoPick_SetCancelClick(msg)
  _AutoPick:ShowMini()
end
function UI_AutoPick_SetConfirmClick(msg)
  for index, wnd in pairs(mapIndexToWnd) do
    local activeWnd, qulityWnd, minWnd, maxWnd
    if index <= 6 then
      activeWnd = WindowToCheckButton(wnd:GetChildByName("CheckButton"))
      qulityWnd = WindowToComboBox(wnd:GetChildByName("ComboBox"))
      minWnd = wnd:GetGrandChild("Lev.Smal")
      maxWnd = wnd:GetGrandChild("Lev.Max")
    else
      activeWnd = WindowToCheckButton(wnd)
    end
    local isActive, nQulity, nMin, nMax
    if activeWnd then
      isActive = activeWnd:IsChecked()
    end
    if qulityWnd then
      nQulity = qulityWnd:GetSelectItem()
    end
    if minWnd then
      nMin = tonumber(minWnd:GetProperty("Text"))
    end
    if maxWnd then
      nMax = tonumber(maxWnd:GetProperty("Text"))
    end
    SetAotoPickData(index, isActive, nQulity, nMin, nMax)
  end
  SaveAutoPickData()
end
function UI_AutoPick_SwitchSelChanged(msg)
  local activeWnd = WindowToTableCtrl(msg:get_window())
  local isActive = activeWnd:GetSelectItem() == 0
  local mode1Wnd = WindowSys_Instance:GetWindow("Autopick.Mode1")
  local mode2Wnd = WindowSys_Instance:GetWindow("Autopick.Mode2")
  mode1Wnd:SetProperty("Visible", tostring(isActive))
  mode2Wnd:SetProperty("Visible", tostring(not isActive))
  SetAotoPickData(15, isActive, nil, nil, nil)
  SaveAutoPickData()
end
function UI_AutoPick_ExecuteCheckHandler(msg)
  local activeWnd = WindowToCheckButton(msg:get_window())
  local isActive = activeWnd:IsChecked()
  SetAotoPickData(15, isActive, nil, nil, nil)
  SaveAutoPickData()
end
function UI_AutoPick_PickAllClicked(msg)
  local activeWnd = WindowToCheckButton(msg:get_window())
  local isActive = activeWnd:IsChecked()
  SetAotoPickData(14, isActive, nil, nil, nil)
  SetAotoPickData(13, not isActive, nil, nil, nil)
  SaveAutoPickData()
end
function UI_AutoPick_DefineClicked(msg)
  local activeWnd = WindowToCheckButton(msg:get_window())
  local isActive = activeWnd:IsChecked()
  SetAotoPickData(13, isActive, nil, nil, nil)
  SetAotoPickData(14, not isActive, nil, nil, nil)
  SaveAutoPickData()
end
function UI_AutoPick_OnMinVauleRangeChange(msg)
  local minWnd = msg:get_window()
  local maxWnd = minWnd:GetParent():GetChildByName("Max")
  local minVaule = tonumber(minWnd:GetProperty("Text"))
  local maxVaule = tonumber(maxWnd:GetProperty("Text"))
  if minVaule > maxVaule then
    minVaule = maxVaule
    minWnd:SetProperty("Text", tostring(minVaule))
  end
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function UI_AutoPick_OnMaxVauleRangeChange(msg)
  local maxWnd = msg:get_window()
  local minWnd = maxWnd:GetParent():GetChildByName("Smal")
  local minVaule = tonumber(minWnd:GetProperty("Text"))
  local maxVaule = tonumber(maxWnd:GetProperty("Text"))
  if minVaule > maxVaule then
    maxVaule = minVaule
    maxWnd:SetProperty("Text", tostring(maxVaule))
  end
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function UI_AutoPick_ShowClick(msg)
  _AutoPick.miniWnd:SetProperty("Visible", "false")
  _AutoPick:ShowMain()
end
function UI_AutoPick_MiniClick(msg)
  _AutoPick:ShowMini()
end
function AutoPickSet_Close_Click(msg)
  _AutoPick.root:SetProperty("Visible", "false")
end
function _OnAutoPick_SetPickInfo(arg1, arg2, arg3, arg4, arg5)
  _AutoPick:OnSetInfo(arg1, arg2, arg3, arg4, arg5)
  local activeWnd = WindowToTableCtrl(_AutoPick.miniWnd:GetChildByName("Switch1"))
  local isActive = GetAutoPickInfoByIndex(15)
  if isActive then
    activeWnd:SelectItem(0)
  else
    activeWnd:SelectItem(1)
  end
end
function _OnAutoPick_AuraPickEnd()
  _AutoPick.root:SetProperty("Visible", "false")
  _AutoPick.miniWnd:SetProperty("Visible", "false")
end
function _OnAutoPick_MemberItem(playerName, serialKey, guid, count, quality, tableId)
  local strMsg
  if count > 1 then
    strMsg = string.format("[%s]{#C^0xFfFFFB00^获得了物品}{ItEm^%d^%s^%d^%d}{#C^0xFFFFFB00^%d个}", playerName, serialKey, guid, tableId, quality, count)
  else
    strMsg = string.format("[%s]{#C^0xFFFFFB00^获得了物品}{ItEm^%d^%s^%d^%d}", playerName, serialKey, guid, tableId, quality)
  end
  Lua_Chat_AddSysLog(strMsg)
end
function _OnAutoPick_ThrowItem(boxID, itemID, index, second, count, itemName)
  local thorwItem = {}
  thorwItem.boxID = boxID
  thorwItem.itemID = itemID
  thorwItem.index = index
  thorwItem.time = second
  thorwItem.wnd = nil
  thorwItem.count = count
  thorwItem.maxTime = second
  thorwItem.itemName = itemName
  Lua_Pick_InsertLootDialog(1, thorwItem)
end
function _OnAutoPick_ThrowItemResult(worldserialId, name, rollNumber, guid, quality, tableId)
  local strMsg
  if rollNumber > 0 and rollNumber <= 100 then
    if name == gUI_MainPlayerAttr.Name then
      strMsg = string.format("{#C^0xFFFFFB00^你对}{ItEm^%d^%s^%d^%d}{#C^0xFFFFFB00^投掷出了}{#C^0xFF00FF00^%d}{#C^0xFFFFFB00^点}", worldserialId, guid, tableId, quality, rollNumber)
    else
      strMsg = string.format("{#C^0xFF00FF00^[%s]}{#C^0xFFFFFB00^对}{ItEm^%d^%s^%d^%d}{#C^0xFFFFFB00^投掷出了}{#C^0xFF00FF00^%d}{#C^0xFFFFFB00^点}", name, worldserialId, guid, tableId, quality, rollNumber)
    end
    Lua_Chat_AddSysLog(strMsg)
  elseif rollNumber == 127 then
    if name == gUI_MainPlayerAttr.Name then
      strMsg = string.format("{#C^0xFFFFFB00^你放弃了物品：}{ItEm^%d^%s^%d^%d}", worldserialId, guid, tableId, quality)
    else
      strMsg = string.format("{#C^0xFF00FF00^[%s]}{#C^0xFFFFFB00^放弃了物品：}{ItEm^%d^%s^%d^%d}", name, worldserialId, guid, tableId, quality)
    end
    Lua_Chat_AddSysLog(strMsg)
  elseif rollNumber == 200 then
    if gUI_MainPlayerAttr.Name ~= name then
    else
    end
  elseif rollNumber == 128 then
    if gUI_MainPlayerAttr.Name == name then
      strMsg = string.format("已经拥有物品{ItEm^%d^%s^%d^%d}", worldserialId, guid, tableId, quality)
      Lua_Chat_AddSysLog(strMsg)
    end
  elseif rollNumber == 255 then
  end
end
function Script_AutoPick_OnLoad()
  _AutoPick:Init()
end
function Script_AutoPick_OnEvent(event)
  if event == "UI_SET_AUTO_PICK_INFO" then
    _OnAutoPick_SetPickInfo(arg1, arg2, arg3, arg4, arg5)
  elseif event == "SET_AUTO_PICK_SUCCESS" then
    SetCurAutoPickInfoToUI()
  elseif event == "PLAYER_SPELL_AUTOPICK" then
    _AutoPick:ShowMini()
  elseif event == "UI_SHOW_AUTO_PICK_WINDOW" then
    _AutoPick:ShowMini()
  elseif event == "AURA_AUTO_PICK_END" then
    _OnAutoPick_AuraPickEnd()
  elseif event == "THROW_TEAM_ITEM" then
    _OnAutoPick_ThrowItem(arg1, arg2, arg3, arg4, arg5, arg6)
  elseif event == "THROW_RESULT" then
    _OnAutoPick_ThrowItemResult(arg1, arg2, arg3, arg4, arg5, arg6)
  elseif event == "MEMBER_PICK_ITEM" then
    _OnAutoPick_MemberItem(arg1, arg2, arg3, arg4, arg5, arg6)
  end
end

