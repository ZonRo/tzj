if gUI and not gUI.Commission then
  gUI.Commission = {}
end
local _Commission_AskType = {
  Publish = 0,
  all = 1,
  Accept = 2
}
local _Commission_Poundage = 5000
local _Commission_QuestTime = {
  [0] = "2小时",
  "12小时",
  "24小时"
}
local _Commission_QuestType = {
  [0] = "日常",
  "副本"
}
local _Commission_QuestStatus = {
  [0] = "可接取",
  "进行中",
  "已接取",
  "已发布"
}
local _Commission_Choose = {
  [0] = "全部",
  "可接取"
}
local _Commission_CurrentPage = 0
local _Commission_Max_Record = 4
local _Commission_CurrentPageCount = 0
local _Commission_CurrentPageOrderType = 0
local _Commission_CurrentPageOrder = 0
local _Commission_QuoteType = {
  NONE = 0,
  ACCEPT = 1,
  SUBMIT = 2,
  SELF = 3,
  OTHER = 4
}
local _Commission_CurrentChoose = 0
local _Commission_Current = 0
local _Commission_Max_AcceptTimes = 3
local _Commission_Max_PublicTimes = 6
local _Commission_DepositBase = 1000
local _Commission_ChooseType = {
  [0] = 6,
  [1] = 2
}
function _Commission_SetOrder(mode)
  gUI.Commission.Level:SetCustomUserData(1, mode)
  gUI.Commission.Time:SetCustomUserData(1, mode)
  gUI.Commission.ZuanShi:SetCustomUserData(1, mode)
  gUI.Commission.Money:SetCustomUserData(1, mode)
end
function _Commission_Update_List(list_type)
  local list, nAcceptDayTimes, nPublicDayTimes, UniqueId, Title, Level, Type, Expire, QuestStatus, ZuanShi, Money, ItemIndex, ItemNum, ListBox
  local index = gUI.Commission.TableCtrl:GetSelectItem()
  if list_type == _Commission_AskType.all then
    ListBox = gUI.Commission.CommissionListBox
    list, nAcceptDayTimes, nPublicDayTimes = GetCommissionList(_Commission_AskType.all)
    _Commission_CurrentPageCount = table.maxn(list)
    if _Commission_CurrentPageCount < _Commission_Max_Record then
      gUI.Commission.NextPage:SetProperty("Enable", "false")
    else
      gUI.Commission.NextPage:SetProperty("Enable", "true")
    end
  elseif list_type == _Commission_AskType.Publish then
    if index ~= 1 then
      return
    end
    ListBox = gUI.Commission.CommissionListBox2
    list, nAcceptDayTimes, nPublicDayTimes = GetCommissionList(_Commission_AskType.Publish)
  elseif list_type == _Commission_AskType.Accept then
    if index ~= 0 then
      return
    end
    ListBox = gUI.Commission.CommissionListBox2
    list, nAcceptDayTimes, nPublicDayTimes = GetCommissionList(_Commission_AskType.Accept)
  end
  ListBox:RemoveAllItems()
  for i = 1, table.maxn(list) do
    UniqueId = list[i].UniqueId
    Title = list[i].Title
    Level = list[i].Level
    Type = list[i].Type
    Expire = list[i].Expire
    QuestStatus = list[i].QuestStatus
    ZuanShi = list[i].ZuanShi
    Money = list[i].Money
    ItemIndex = list[i].ItemIndex
    ItemNum = list[i].ItemNum
    local ex_time = Lua_UI_Formattime(Expire)
    strMoney = Lua_UI_Money2String(Money)
    local strItem = "无"
    if ItemIndex > 0 and ItemNum > 0 then
      local name = GetItemInfoBySlot(-1, ItemIndex, nil)
      strItem = string.format("%s*%d", name, tostring(ItemNum))
    end
    local str = string.format("%s|%d|%s|%s|%s|%d|%s|%s", Title, Level, _Commission_QuestType[Type], ex_time, _Commission_QuestStatus[QuestStatus], ZuanShi, strMoney, strItem)
    local item = ListBox:InsertString(str, -1)
    ListBox:SetProperty("TextHorzAlign", "Left")
    ListBox:SetProperty("TextHorzAlign", "HCenter")
    item:SetUserData64(UniqueId)
    item:SetUserData(QuestStatus)
  end
  gUI.Commission.PublishTimes:SetProperty("Text", tostring(nPublicDayTimes) .. "/" .. tostring(_Commission_Max_PublicTimes))
  gUI.Commission.AcceptTimes:SetProperty("Text", tostring(nAcceptDayTimes) .. "/" .. tostring(_Commission_Max_AcceptTimes))
  gUI.Commission.CommissionListBox:SetSelectItem(-1)
  gUI.Commission.Cancel_btn:SetProperty("Enable", "false")
  gUI.Commission.AcceptTask_btn:SetProperty("Enable", "false")
end
function _Commission_ShowDetail(id)
  gUI.Commission.Describe_gbox:ResetAllGoods(true)
  gUI.Commission.DetailTask:ResetScrollBar()
  gUI.Commission.Objective_dbox:ClearAllContent()
  gUI.Commission.Award_dbox:ClearAllContent()
  gUI.Commission.CommissionAward_dbox:ClearAllContent()
  local QuestId, Expire, QuestStatus, ZuanShi, Money, ItemIndex, ItemNum, QuestLvl = GetCommissionDetail(id)
  if QuestStatus ~= 0 then
    gUI.Commission.Accept_btn:SetProperty("Enable", "false")
  end
  if QuestId == nil then
    debugN("error _Commission_ShowDetail")
    return
  end
  local _, strObjective, _, _, _, _, _, _, _, _, _, nQuestType, _, _, RewExp, RewMoney, RewNimbus, RewBindedMoney, RewWrathValue, GuildMoney, GuildActive, GuildContribution, GuildConstruction, GuildPractice, IsGuildExp, bHaveChoiceItem, nCommission, v1, v2, v3, v4, v5, v6, v7 = GetQuestInfoLua(QuestId, gUI_QuestRewardItemSourceID.COMMISSION_WINDOW)
  if v1 then
    strObjective = string.format(strObjective, v1, v2, v3, v4, v5, v6, v7)
  end
  if strObjective ~= 0 then
    strObjective = Lua_UI_TranslateText(strObjective)
    gUI.Commission.Objective_dbox:InsertBack("\n任务目标：\n  " .. strObjective .. [[


]], 0, 0, 0)
  end
  local strReward = ""
  if RewExp and RewExp > 0 then
    strReward = strReward .. string.format("%d点经验值\n", RewExp)
    if IsGuildExp > 0 then
      local currentExp, nextExp = GetGuildLevMulitExp()
      if currentExp > 0 then
        strReward = strReward .. string.format("(当前帮会等级加成经验值: %d)\n", tonumber(RewExp * currentExp / 100))
      end
      if nextExp > 0 and currentExp > 0 then
        strReward = strReward .. string.format("(下一级加成经验值: %d)\n", tonumber(RewExp * nextExp / 100))
      end
    end
  end
  if RewNimbus and RewNimbus > 0 then
    strReward = strReward .. string.format("%d点精魄值\n", RewNimbus)
  end
  if RewMoney and RewMoney > 0 then
    strReward = strReward .. Lua_UI_Money2String(RewMoney) .. "\n"
  end
  if RewBindedMoney and RewBindedMoney > 0 then
    strReward = strReward .. Lua_UI_Money2String(RewBindedMoney) .. "(绑  金)\n"
  end
  if RewWrathValue and RewWrathValue ~= 0 then
    strReward = strReward .. string.format("减少%d点天诛值\n", -RewWrathValue)
  end
  if GuildMoney and GuildMoney > 0 then
    strReward = strReward .. string.format("帮会资金：%s\n", Lua_UI_Money2String(GuildMoney * 10000))
  end
  if GuildActive and GuildActive > 0 then
    strReward = strReward .. string.format("帮会活跃值：%d点\n", GuildActive)
  end
  if GuildContribution and GuildContribution > 0 then
    strReward = strReward .. string.format("帮会贡献度：%d点\n", GuildContribution)
  end
  if GuildConstruction and GuildConstruction > 0 then
    strReward = strReward .. string.format("帮会发展度：%d点\n", GuildConstruction)
  end
  if GuildPractice and GuildPractice > 0 then
    strReward = strReward .. string.format("守护兽成长值：%d点\n", GuildPractice)
  end
  if strReward ~= "" then
    gUI.Commission.Award_dbox:InsertBack("\n任务奖励：\n" .. strReward, 0, 0, 0)
    if bHaveChoiceItem then
      gUI.Commission.Award_dbox:InsertBack("\n可选奖励：", 0, 0, 0)
    end
  end
  gUI.Commission.DetailTask:SizeSelf()
  gUI.Commission.CommissionAward_dbox:InsertBack("\n剩余时间：    " .. Lua_UI_Formattime(Expire), 0, 0, 0)
  gUI.Commission.CommissionAward_dbox:InsertBack("委托奖励：    ", 0, 0, 0)
  gUI.Commission.CommissionAward_dbox:InsertBack("  钻石奖励：    " .. tostring(ZuanShi), 0, 0, 0)
  gUI.Commission.CommissionAward_dbox:InsertBack("  金币奖励：    " .. Lua_UI_Money2String(Money), 0, 0, 0)
  local strItem = "无"
  if ItemIndex > 0 and ItemNum > 0 then
    local name = GetItemInfoBySlot(-1, ItemIndex, nil)
    strItem = string.format("%s*%d", name, tostring(ItemNum))
  end
  gUI.Commission.CommissionAward_dbox:InsertBack("  物品奖励：    " .. strItem, 0, 0, 0)
  gUI.Commission.CommissionAward_dbox:InsertBack("押金：    " .. Lua_UI_Money2String(QuestLvl * _Commission_DepositBase), 0, 0, 0)
  gUI.Commission.CommissionInfo:SetProperty("Visible", "true")
end
function _Commission_ClearInfo()
  gUI.Commission.RewardNum_G:SetProperty("Text", "0")
  gUI.Commission.RewardNum_S:SetProperty("Text", "0")
  gUI.Commission.RewardNum_C:SetProperty("Text", "0")
  gUI.Commission.RewardNum_Item:SetProperty("Text", "0")
  gUI.Commission.RewardNum_Zuanshi:SetProperty("Text", "0")
  gUI.Commission.ChooseTask:RemoveAllItems()
  gUI.Commission.RewardItem_cbox:RemoveAllItems()
  gUI.Commission.ChooseType:RemoveAllItems()
end
function _Commission_InitAll()
  _Commission_CurrentPage = 0
  local nAskType = _Commission_AskType.all
  AskCommissionList(nAskType, 0, 0, 0, 0)
  gUI.Commission.Page:SetProperty("Text", tostring(_Commission_CurrentPage + 1))
  gUI.Commission.LastPage:SetProperty("Enable", "false")
end
function _OnCommission_Show()
  gUI.Commission.Choosebox:SetSelectItem(0)
  _Commission_CurrentChoose = 0
  gUI.Commission.TableCtrl:SetSelectedState(0)
  _Commission_Current = 0
  gUI.Commission.LastPage:SetProperty("Enable", "false")
  _Commission_SetOrder(0)
  _Commission_CurrentPage = 0
  local nAskType = _Commission_AskType.all
  AskCommissionList(nAskType, 0, 0, 0, 0)
  nAskType = _Commission_AskType.Accept
  AskCommissionList(nAskType, 0, 0, 0, 0)
  gUI.Commission.CommissionTask:SetProperty("Visible", "true")
  gUI.Commission.Page:SetProperty("Text", tostring(_Commission_CurrentPage + 1))
  gUI.Commission.Accomplish:SetProperty("Enable", "false")
end
function _OnCommission_UpdateList(list_type)
  if list_type == 4 then
    _Commission_InitAll()
    return
  end
  _Commission_Update_List(list_type)
  gUI.Commission.TaskInfoBtn:SetProperty("Enable", "false")
end
function _OnCommission_UpdateRewardItem(Index, GoodsId, Icon, GoodsCount, sourceId)
  if sourceId == gUI_QuestRewardItemSourceID.COMMISSION_WINDOW then
    gUI.Commission.Describe_gbox:SetItemGoods(Index, Icon, -1)
    gUI.Commission.Describe_gbox:SetItemData(Index, GoodsId)
    if GoodsCount > 1 then
      gUI.Commission.Describe_gbox:SetItemSubscript(Index, tostring(GoodsCount))
    end
  end
end
function UI_Commission_CloseMenu(msg)
  gUI.Commission.CommissionTask:SetProperty("Visible", "false")
  gUI.Commission.Choosebox:SetSelectItem(-1)
  _Commission_CurrentChoose = 0
  _Commission_SetOrder(0)
  _Commission_CurrentPageQuestType = 0
  _Commission_CurrentPageOrder = 0
  if gUI.Commission.CommissionInfo:IsVisible() then
    UI_Commission_CloseDetail(msg)
  end
  gUI.Commission.ChooseType:RemoveAllItems()
end
function UI_Commission_CloseDetail(msg)
  gUI.Commission.Accept_btn:SetProperty("Enable", "true")
  gUI.Commission.CommissionInfo:SetProperty("Visible", "false")
end
function UI_CommissionTaskInfo_HelpClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("CommissionTaskInfo.Common_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Commission_ShowDetail(msg)
  local item = gUI.Commission.CommissionListBox:GetSelectedItem()
  if item == nil then
    return
  end
  local id = item:GetUserData64()
  _Commission_ShowDetail(id)
end
function UI_Commission_ChooseChanged(msg)
  _Commission_CurrentPage = 0
  local index = msg:get_wparam()
  local nAskType = _Commission_AskType.all
  if index == 0 and _Commission_CurrentChoose ~= index then
    AskCommissionList(nAskType, index, 0, 0, _Commission_CurrentPage)
  elseif index == 1 then
    AskCommissionList(nAskType, index, 0, 0, _Commission_CurrentPage)
  end
  gUI.Commission.Page:SetProperty("Text", tostring(_Commission_CurrentPage + 1))
  gUI.Commission.LastPage:SetProperty("Enable", "false")
  _Commission_CurrentChoose = index
end
function UI_Commission_TabctrlClick(msg)
  local nAskType = _Commission_AskType.Accept
  local index = gUI.Commission.TableCtrl:GetSelectItem()
  if index == 0 and _Commission_Current ~= index then
    AskCommissionList(nAskType, 0, 0, 0, 0)
  elseif index == 1 then
    nAskType = _Commission_AskType.Publish
    AskCommissionList(nAskType, 0, 0, 0, 0)
  end
  gUI.Commission.LastPage:SetProperty("Enable", "false")
  gUI.Commission.Accomplish:SetProperty("Enable", "false")
  _Commission_Current = index
end
function UI_Commission_CancelPublish(msg)
  local item = gUI.Commission.CommissionListBox2:GetSelectedItem()
  if item == nil then
    return
  end
  local id = item:GetUserData64()
  Messagebox_Show("CANCEL_PUBLISH", id)
end
function UI_Commission_AcceptCommssion(msg)
  local questNum = GetAcceptedQuestNum()
  if questNum >= 25 then
    Lua_Chat_ShowOSD("QUEST_NUM_LIMIT_OSD")
    Lua_Chat_ShowSysLog("QUEST_NUM_LIMIT")
    return
  end
  local item = gUI.Commission.CommissionListBox:GetSelectedItem()
  if item == nil then
    return
  end
  local id = item:GetUserData64()
  local level = tonumber(item:GetText(1))
  local _, currMoney, bindMoney = GetBankAndBagMoney()
  if currMoney + bindMoney < level * 1000 then
    Lua_Chat_ShowOSD("COMMISSION_NEED_MONEY")
    return
  end
  local _, _, _, _, myLevel = GetPlaySelfProp(4)
  if level > myLevel then
    Lua_Chat_ShowOSD("QUEST_NEED_LEVEL")
    return
  end
  local list, nAcceptDayTimes, nPublicDayTimes = GetCommissionList(_Commission_AskType.Accept)
  if nAcceptDayTimes >= 3 then
    Lua_Chat_ShowOSD("QUEST_TIMES_OVER", nAcceptDayTimes)
    return
  end
  if #list > 0 then
    Lua_Chat_ShowOSD("QUEST_ACCEPTED")
    return
  end
  QuoteQuestCommission(id, _Commission_QuoteType.ACCEPT)
  gUI.Commission.TableCtrl:SetSelectedState(0)
  _Commission_Current = 0
  if gUI.Commission.CommissionInfo:IsVisible() then
    UI_Commission_CloseDetail(msg)
  end
end
function UI_Commission_SubmitCommssion(msg)
  local item = gUI.Commission.CommissionListBox2:GetSelectedItem()
  if item == nil then
    return
  end
  local id = item:GetUserData64()
  QuoteQuestCommission(id, _Commission_QuoteType.SUBMIT)
  gUI.Commission.TableCtrl:SetSelectedState(0)
  _Commission_Current = 0
end
function UI_Commission_PublishClick(msg)
  local index = gUI.Commission.TableCtrl:GetSelectItem()
  local item2 = gUI.Commission.CommissionListBox2:GetSelectedItem()
  if index == 1 and item2 ~= nil then
    local nState = item2:GetUserData()
    if nState ~= 3 then
      gUI.Commission.Cancel_btn:SetProperty("Enable", "false")
    else
      gUI.Commission.Cancel_btn:SetProperty("Enable", "true")
    end
  end
  if index == 0 then
    local item = gUI.Commission.CommissionListBox2:GetSelectedItem()
    if item == nil then
      return
    end
    local id = item:GetUserData64()
    local IsComplete = IsCommissionComplete(id)
    if IsComplete then
      gUI.Commission.Accomplish:SetProperty("Enable", "true")
    else
      gUI.Commission.Accomplish:SetProperty("Enable", "false")
    end
  end
end
function UI_Commission_CommissionListClick(msg)
  local item = gUI.Commission.CommissionListBox:GetSelectedItem()
  if item == nil then
    return
  end
  local id = item:GetUserData()
  if id == 0 then
    gUI.Commission.AcceptTask_btn:SetProperty("Enable", "true")
  else
    gUI.Commission.AcceptTask_btn:SetProperty("Enable", "false")
  end
  gUI.Commission.TaskInfoBtn:SetProperty("Enable", "true")
end
function UI_Commission_PageDownClick(msg)
  gUI.Commission.LastPage:SetProperty("Enable", "true")
  _Commission_CurrentPage = _Commission_CurrentPage + 1
  local nAskType = _Commission_AskType.all
  AskCommissionList(nAskType, _Commission_CurrentChoose, _Commission_CurrentPageOrderType, _Commission_CurrentPageOrder, _Commission_CurrentPage * _Commission_Max_Record)
  gUI.Commission.Page:SetProperty("Text", tostring(_Commission_CurrentPage + 1))
end
function UI_Commission_PageUpClick(msg)
  _Commission_CurrentPage = _Commission_CurrentPage - 1
  if _Commission_CurrentPage <= 0 then
    gUI.Commission.LastPage:SetProperty("Enable", "false")
    _Commission_CurrentPage = 0
  end
  local nAskType = _Commission_AskType.all
  AskCommissionList(nAskType, _Commission_CurrentChoose, _Commission_CurrentPageOrderType, _Commission_CurrentPageOrder, _Commission_CurrentPage * _Commission_Max_Record)
  gUI.Commission.Page:SetProperty("Text", tostring(_Commission_CurrentPage + 1))
end
function UI_Commission_ChangeOrder(msg)
  local nAskType = _Commission_AskType.all
  local wnd = msg:get_window()
  local OrderType = wnd:GetCustomUserData(0)
  local Order = wnd:GetCustomUserData(1)
  if Order == 0 then
    AskCommissionList(nAskType, _Commission_CurrentChoose, OrderType, 1, _Commission_CurrentPage * _Commission_Max_Record)
    _Commission_CurrentPageOrderType = OrderType
    _Commission_CurrentPageOrder = 1
    wnd:SetCustomUserData(1, 1)
  else
    AskCommissionList(nAskType, _Commission_CurrentChoose, OrderType, 0, _Commission_CurrentPage * _Commission_Max_Record)
    _Commission_CurrentPageOrderType = OrderType
    _Commission_CurrentPageOrder = 0
    wnd:SetCustomUserData(1, 0)
  end
end
function UI_Commission_PublishCommissionShow(msg)
  if GetRemainProtectTime() > 0 then
    Lua_Chat_ShowOSD("SAFE_TIME_LIMIT")
    return
  end
  AskCommissionList(_Commission_AskType.Publish, 0, 0, 0, 0)
  gUI.Commission.ChooseType:RemoveAllItems()
  if gUI.Commission.PromulgateTask:IsVisible() then
    gUI.Commission.PromulgateTask:SetProperty("Visible", "false")
    local wnd = WindowSys_Instance:GetRootWindow()
    WindowSys_Instance:SetKeyboardCaptureWindow(wnd)
    _Commission_ClearInfo()
  else
    gUI.Commission.PromulgateTask:SetProperty("Visible", "true")
    gUI.Commission.Time_cbox:SetSelectItem(0)
    gUI.Commission.ChooseType:SetSelectItem(0)
    gUI.Commission.RewardNum_Zuanshi:SetProperty("Text", "0")
    gUI.Commission.RewardNum_G:SetProperty("Text", "0")
    gUI.Commission.RewardNum_S:SetProperty("Text", "0")
    gUI.Commission.RewardNum_C:SetProperty("Text", "0")
    for i = 0, #_Commission_QuestType do
      gUI.Commission.ChooseType:InsertString(_Commission_QuestType[i], i)
    end
  end
end
function UI_Commission_PublishCommission()
  local x = gUI.Commission.ChooseTask:GetSelectItem()
  local nQuestId = gUI.Commission.ChooseTask:GetItemData(x)
  local nType = gUI.Commission.ChooseType:GetSelectItem()
  local nTime = gUI.Commission.Time_cbox:GetSelectItem()
  local nZuanshi = tonumber(gUI.Commission.RewardNum_Zuanshi:GetProperty("Text"))
  local gold = tonumber(gUI.Commission.RewardNum_G:GetProperty("Text"))
  local silver = tonumber(gUI.Commission.RewardNum_S:GetProperty("Text"))
  local copper = tonumber(gUI.Commission.RewardNum_C:GetProperty("Text"))
  local nMoney = gold * 10000 + silver * 100 + copper
  local nItem = gUI.Commission.RewardItem_cbox:GetSelectItem()
  local nConut = tonumber(gUI.Commission.RewardNum_Item:GetProperty("Text"))
  local list, _, _ = GetCommissionList(_Commission_AskType.Publish)
  if list and #list > 0 then
    Lua_Chat_ShowOSD("QUEST_PUBLICED")
    return
  end
  if nQuestId == 0 then
    Lua_Chat_ShowOSD("QUEST_NEED_QUSETID")
    return
  end
  local _, curMoney = GetBankAndBagMoney()
  if curMoney < 5000 then
    Lua_Chat_ShowOSD("QUEST_NEED_MONEY")
    UI_Commission_PublishCommissionShow()
    return
  end
  if nMoney > curMoney then
    Lua_Chat_ShowOSD("QUEST_NEED_AWARDMONEY")
    return
  end
  if nMoney == 0 and nZuanshi == 0 and nConut == 0 then
    Lua_Chat_ShowOSD("QUEST_NEED_AWARD")
    return
  end
  if nTime < 0 then
    Lua_Chat_ShowOSD("QUEST_NEED_TIEM")
    return
  end
  AcceptCommission(nQuestId, nType, nTime, nZuanshi, nMoney, nItem, nConut)
  _Commission_ClearInfo()
  UI_Commission_PublishCommissionShow()
end
function UI_PromulgateTask_HelpClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("PromulgateTask.Write_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Commission_ChooseType(msg)
  local index = msg:get_wparam()
  local chosseType = _Commission_ChooseType[index]
  local list = GetQuestListForCommission(chosseType)
  gUI.Commission.ChooseTask:RemoveAllItems()
  if list[0] == nil then
    return
  end
  for i = 0, #list do
    gUI.Commission.ChooseTask:InsertString(list[i].Title, i)
    gUI.Commission.ChooseTask:SetItemData(i, list[i].ID)
  end
end
function UI_ComissionTask_HelpClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("CommissionTask.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Commission_ChooseQuest(msg)
  local list = GetQuestItemForCommission()
  gUI.Commission.RewardItem_cbox:RemoveAllItems()
  for i = 0, #list do
    gUI.Commission.RewardItem_cbox:InsertString(list[i].Name, i)
    gUI.Commission.RewardItem_cbox:SetItemData(i, list[i].Index)
  end
  gUI.Commission.RewardNum_Item:SetProperty("Text", "0")
  gUI.Commission.RewardItem_cbox:SetSelectItem(0)
end
function UI_Commission_ItemChange(msg)
  local x = gUI.Commission.RewardItem_cbox:GetSelectItem()
  local id = gUI.Commission.RewardItem_cbox:GetItemData(x)
  gUI.Commission.RewardNum_Item:SetProperty("MaxValue", tostring(id))
end
function UI_Commission_MoneyChange()
  local _, curMoney, bindMoney = GetBankAndBagMoney()
  if bindMoney < 5000 then
    curMoney = curMoney - (5000 - bindMoney)
    if curMoney < 1 then
      Lua_Chat_ShowOSD("COMMISSION_SEND_MONEY")
      gUI.Commission.RewardNum_G:SetProperty("Text", tostring(0))
      gUI.Commission.RewardNum_S:SetProperty("Text", tostring(0))
      gUI.Commission.RewardNum_C:SetProperty("Text", tostring(0))
      return
    end
  end
  local gold, silver, copper = Lua_UI_FormatMoneyByEdit(curMoney)
  local setGold = gUI.Commission.RewardNum_G:GetProperty("Text")
  if setGold ~= "" then
    setGold = tonumber(setGold)
  else
    setGold = 0
  end
  local setSilver = gUI.Commission.RewardNum_S:GetProperty("Text")
  if setSilver ~= "" then
    setSilver = tonumber(setSilver)
  else
    setSilver = 0
  end
  local setCopper = gUI.Commission.RewardNum_C:GetProperty("Text")
  if setCopper ~= "" then
    setCopper = tonumber(setCopper)
  else
    setCopper = 0
  end
  if gold < setGold then
    setGold = gold
  end
  if setGold == gold then
    if silver < setSilver then
      setSilver = silver
    end
    if setSilver == silver and copper < setCopper then
      setCopper = copper
    end
  end
  gUI.Commission.RewardNum_G:SetProperty("Text", tostring(setGold))
  gUI.Commission.RewardNum_S:SetProperty("Text", tostring(setSilver))
  gUI.Commission.RewardNum_C:SetProperty("Text", tostring(setCopper))
end
function Script_Commission_OnEvent(event)
  if event == "COMMISSION_MENU_SHOW" then
    _OnCommission_Show()
  elseif event == "COMMISSION_UPDATELIST" then
    _OnCommission_UpdateList(arg1)
  elseif event == "NPC_GOSSIP_ADD_REWARD_ITEM" then
    _OnCommission_UpdateRewardItem(arg1, arg2, arg3, arg4, arg5)
  end
end
function Script_Commission_OnLoad(event)
  gUI.Commission.CommissionTask = WindowSys_Instance:GetWindow("CommissionTask")
  gUI.Commission.CommissionListBox = WindowToListBox(WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.TaskImage01_bg.TaskList_lbox"))
  gUI.Commission.CommissionListBox2 = WindowToListBox(WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.TaskImage02_bg.TaskList_lbox"))
  gUI.Commission.TableCtrl = WindowToTableCtrl(WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.TableCtrl1"))
  gUI.Commission.Choosebox = WindowToComboBox(WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.ComboBox_cbox"))
  gUI.Commission.PublishTimes = WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.Label2_dlab")
  gUI.Commission.AcceptTimes = WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.Label4_dlab")
  gUI.Commission.Cancel_btn = WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.Cancel_btn")
  gUI.Commission.AcceptTask_btn = WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.AcceptTask_btn")
  gUI.Commission.Page = WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.TaskImage01_bg.Page_dlab")
  gUI.Commission.NextPage = WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.TaskImage01_bg.NextPage_btn")
  gUI.Commission.LastPage = WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.TaskImage01_bg.LastPage_btn")
  gUI.Commission.Level = WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.TaskImage01_bg.TitleImage_bg.TaskLev_btn")
  gUI.Commission.Time = WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.TaskImage01_bg.TitleImage_bg.TaskTime_btn")
  gUI.Commission.ZuanShi = WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.TaskImage01_bg.TitleImage_bg.TaskAward01_btn")
  gUI.Commission.Money = WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.TaskImage01_bg.TitleImage_bg.TaskAward02_btn")
  gUI.Commission.Accomplish = WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.Accomplish_btn")
  gUI.Commission.TaskInfoBtn = WindowSys_Instance:GetWindow("CommissionTask.TaskInfo_bg.TaskInfo_btn")
  gUI.Commission.CommissionInfo = WindowSys_Instance:GetWindow("CommissionTaskInfo")
  gUI.Commission.DetailTask = WindowToContainer(WindowSys_Instance:GetWindow("CommissionTaskInfo.Common_bg.Task_cnt"))
  gUI.Commission.Objective_dbox = WindowToDisplayBox(WindowSys_Instance:GetWindow("CommissionTaskInfo.Common_bg.Task_cnt.Describe_dbox"))
  gUI.Commission.Award_dbox = WindowToDisplayBox(WindowSys_Instance:GetWindow("CommissionTaskInfo.Common_bg.Task_cnt.Award_dbox"))
  gUI.Commission.Describe_gbox = WindowToGoodsBox(WindowSys_Instance:GetWindow("CommissionTaskInfo.Common_bg.Task_cnt.Describe_gbox"))
  gUI.Commission.CommissionAward_dbox = WindowToDisplayBox(WindowSys_Instance:GetWindow("CommissionTaskInfo.Common_bg.Task_cnt.Award01_dbox"))
  gUI.Commission.Accept_btn = WindowSys_Instance:GetWindow("CommissionTaskInfo.Common_bg.Accept_btn")
  gUI.Commission.PromulgateTask = WindowSys_Instance:GetWindow("PromulgateTask")
  gUI.Commission.ChooseType = WindowToComboBox(WindowSys_Instance:GetWindow("PromulgateTask.Write_bg.ChooseType_cbox"))
  gUI.Commission.ChooseTask = WindowToComboBox(WindowSys_Instance:GetWindow("PromulgateTask.Write_bg.ChooseTask_cbox"))
  gUI.Commission.Time_cbox = WindowToComboBox(WindowSys_Instance:GetWindow("PromulgateTask.Write_bg.Time_cbox"))
  gUI.Commission.RewardItem_cbox = WindowToComboBox(WindowSys_Instance:GetWindow("PromulgateTask.Write_bg.RewardItem_cbox"))
  gUI.Commission.RewardNum_Zuanshi = WindowSys_Instance:GetWindow("PromulgateTask.Write_bg.Picture1_bg.RewardNum_ebox")
  gUI.Commission.RewardNum_G = WindowSys_Instance:GetWindow("PromulgateTask.Write_bg.Money_bg.Gold")
  gUI.Commission.RewardNum_S = WindowSys_Instance:GetWindow("PromulgateTask.Write_bg.Money_bg.Silver")
  gUI.Commission.RewardNum_C = WindowSys_Instance:GetWindow("PromulgateTask.Write_bg.Money_bg.Copper")
  gUI.Commission.RewardNum_Item = WindowSys_Instance:GetWindow("PromulgateTask.Write_bg.Picture1_bg1.RewardItemNum_ebox")
  gUI.Commission.Poundage = WindowSys_Instance:GetWindow("PromulgateTask.Write_bg.Spending_bg.MoneyAu_dlab")
  gUI.Commission.Money_bg = WindowSys_Instance:GetWindow("PromulgateTask.Write_bg.Money_bg")
  gUI.Commission.Poundage:SetProperty("Text", Lua_UI_Money2String(_Commission_Poundage))
  gUI.Commission.Choosebox:RemoveAllItems()
  gUI.Commission.ChooseType:RemoveAllItems()
  gUI.Commission.Time_cbox:RemoveAllItems()
  gUI.Commission.ChooseTask:RemoveAllItems()
  for i = 0, #_Commission_Choose do
    gUI.Commission.Choosebox:InsertString(_Commission_Choose[i], i)
  end
  for i = 0, #_Commission_QuestTime do
    gUI.Commission.Time_cbox:InsertString(_Commission_QuestTime[i], i)
  end
  AskCommissionList(_Commission_AskType.Accept, 0, 0, 0, 0)
end
GetWindow("PromulgateTask.Write_bg.Picture1_bg1.RewardItemNum_ebox")
  gUI.Commission.Poundage = WindowSys_Instance:GetWindow("PromulgateTask.Write_bg.Spending_bg.MoneyAu_dlab")
  gUI.Commission.Money_bg = WindowSys_Instance:GetWindow("PromulgateTask.Write_bg.Money_bg")
  gUI.Commission.Poundage:SetProperty("Text", Lua_UI_Money2String(_Commission_Poundage))
  gUI.Commission.Choosebox:RemoveAllItems()
  gUI.Commission.ChooseType:RemoveAllItems()
  gUI.Commission.Time_cbox:RemoveAllItems()
  gUI.Commission.ChooseTask:RemoveAllItems()
  for i = 0, #_Commission_Choose do
    gUI.Commission.Choosebox:InsertString(_Commission_Choose[i], i)
  end
  for i = 0, #_Commission_QuestTime do
    gUI.Commission.Time_cbox:InsertString(_Commission_QuestTime[i], i)
  end
  AskCommissionList(_Commission_AskType.Accept, 0, 0, 0, 0)
end
