if gUI and not gUI.NpcShop then
  gUI.NpcShop = {}
end
local _Shop_ShopCurPage = 0
local _SHOP_PAGEMAXNUM = 8
local _Shop_PageItemWndList = {}
local _SHOP_BUYBACKITEM = 6
local _CURR_WelfareNum = 0
local CU_GUILD_SCORE = 4
local CU_GOODBAD = 13
local CU_SPIRITVALUE = 14
local CU_NIMBUS = 15
local CU_HONOR = 21
local CU_UNBINDGOLD = 1
local CU_BINDGOLD = 0
local _Shop_Buy_CurrIndex = -1
local _Shop_Buy_CurrType = -1
local _Shop_CU_Type, _Shop_CU_MainType
function _Shop_CreateGoodsByTmp()
  local parent = WindowSys_Instance:GetWindow("NpcShop.Main_bg.Shop_bg")
  local tmpWnd = parent:GetChildByName("Picture1_bg")
  for i = 0, _SHOP_PAGEMAXNUM - 1 do
    _Shop_PageItemWndList[i] = WindowSys_Instance:CreateWndFromTemplate(tmpWnd, parent)
    local rect = tmpWnd:GetWndRect()
    _Shop_PageItemWndList[i]:SetLeft(tmpWnd:GetLeft() + i % 2 * rect:get_width())
    _Shop_PageItemWndList[i]:SetTop(tmpWnd:GetTop() + math.floor(i / 2) * rect:get_height())
    _Shop_PageItemWndList[i]:SetVisible(true)
  end
end
function _Shop_InitWnd()
  gUI.NpcShop.Root = WindowSys_Instance:GetWindow("NpcShop")
  gUI.NpcShop.Title = WindowToLabel(WindowSys_Instance:GetWindow("NpcShop.Main_bg.Title_slab"))
  gUI.NpcShop.Tab = WindowToTableCtrl(WindowSys_Instance:GetWindow("NpcShop.Main_bg.TableCtrl1_tctl"))
  gUI.NpcShop.PageNum = WindowSys_Instance:GetWindow("NpcShop.Main_bg.Shop_bg.Page_dlab")
  gUI.NpcShop.BatchBuy = WindowSys_Instance:GetWindow("NPCShopBatch")
  gUI.NpcShop.TempGoodsBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("NpcShop.Main_bg.Shop_bg.Picture1_bg.GoodsBox_gbox"))
  gUI.NpcShop.SoldItemGoods = WindowToGoodsBox(WindowSys_Instance:GetWindow("NpcShop.Main_bg.Picture1_bg.Picture1_bg.GoodsBox_gbox"))
  gUI.NpcShop.ShowMoney = WindowSys_Instance:GetWindow("NpcShop.Main_bg.Shop_bg.Money_dlab")
  gUI.NpcShop.ShowBindMoney = WindowSys_Instance:GetWindow("NpcShop.Main_bg.Shop_bg.BindMoney_dlab")
  gUI.NpcShop.ShowCurrency = WindowSys_Instance:GetWindow("NpcShop.Main_bg.Shop_bg.Currency_dlab")
  gUI.NpcShop.CurrencyType = WindowSys_Instance:GetWindow("NpcShop.Main_bg.Shop_bg.CurrencyType_dlab")
  gUI.NpcShop.ShowCurrNote = WindowSys_Instance:GetWindow("NpcShop.Main_bg.Shop_bg.MoneyNote_dlab")
  gUI.NpcShop.ShowCurrNote2 = WindowSys_Instance:GetWindow("NpcShop.Main_bg.Shop_bg.MoneyNote2_dlab")
  gUI.NpcShop.EquipCheck = WindowToCheckButton(WindowSys_Instance:GetWindow("NpcShop.Main_bg.Shop_bg.equip_cbtn"))
  gUI.NpcShop.EquipCheck:SetChecked(true)
  if _Shop_PageItemWndList[0] == nil then
    _Shop_CreateGoodsByTmp()
  end
end
function _Shop_GetShopIndexByPage(shopTypeIndex, currPage)
  local MaxCount, UnitType = GetShopItemCount(shopTypeIndex)
  local IndexBegin = math.min(currPage * _SHOP_PAGEMAXNUM, MaxCount)
  local IndexEnd = math.min((currPage + 1) * _SHOP_PAGEMAXNUM, MaxCount) - 1
  gUI.NpcShop.ShowCurrNote2:SetVisible(UnitType == 1)
  _Shop_CU_MainType = UnitType
  return IndexBegin, IndexEnd
end
function _Shop_GetAllChildByTmp(parent)
  local goodsBox = WindowToGoodsBox(parent:GetChildByName("GoodsBox_gbox"))
  local itemName = parent:GetChildByName("Itemname_dlab")
  local moneyParent = parent:GetChildByName("NeedMoney_bg")
  local money_dl = moneyParent:GetChildByName("Money_dlab")
  local currency_dl = moneyParent:GetChildByName("Currency_dlab")
  local currency_pic = moneyParent:GetChildByName("Crrency_pic")
  return goodsBox, itemName, moneyParent, money_dl, currency_dl, currency_pic
end
function _Shop_ClearAllPageItem()
  for index, parent in pairs(_Shop_PageItemWndList) do
    local goodsBox, itemName, moneyParent = _Shop_GetAllChildByTmp(parent)
    goodsBox:ResetAllGoods(true)
    itemName:SetProperty("Text", "")
    moneyParent:SetVisible(false)
  end
end
function _Shop_ShowCurrPageNum(shopTypeIndex, currPage)
  local currTypeMaxItemNum = GetShopItemCount(shopTypeIndex)
  local currTypeMaxNum = math.ceil(currTypeMaxItemNum / _SHOP_PAGEMAXNUM)
  if currTypeMaxNum > 0 then
    if currPage < 0 then
      currPage = currTypeMaxNum - 1
    end
    if currTypeMaxNum <= currPage then
      currPage = 0
    end
    local strPage = string.format("%s/%s", currPage + 1, currTypeMaxNum)
    gUI.NpcShop.PageNum:SetProperty("Text", strPage)
    _Shop_ShopCurPage = currPage
  else
    gUI.NpcShop.PageNum:SetProperty("Text", "0/0")
    _Shop_ShopCurPage = 0
  end
end
function _Shop_ShowShopGoods(shopTypeIndex, currPage, isCanFlag)
  if not isCanFlag then
    _Shop_ClearAllPageItem()
  end
  _Shop_CU_Type = nil
  local IndexBegin, IndexEnd = _Shop_GetShopIndexByPage(shopTypeIndex, currPage)
  for i = IndexBegin, IndexEnd do
    local Icon, Name, Price, Amount, Quality, ItemId, CUType, CUPrice, maxCount, CUItemId, nIndex = GetShopItemInfo(shopTypeIndex, i)
    if Icon then
      local goodsBox, itemName, moneyParent, money_dl, currency_dl, currency_pic = _Shop_GetAllChildByTmp(_Shop_PageItemWndList[i - IndexBegin])
      goodsBox:SetItemGoods(0, Icon, Quality)
      goodsBox:SetItemData(0, nIndex)
      goodsBox:SetCustomUserData(1, i)
      goodsBox:SetCustomUserData(2, nIndex)
      if Amount > 1 then
        goodsBox:SetItemSubscript(0, tostring(Amount))
      end
      if maxCount ~= 255 and maxCount ~= -1 then
        if maxCount >= 1 then
          goodsBox:SetItemSuperscript(0, tostring(maxCount))
        else
          goodsBox:SetItemSuperscript(0, tostring(0))
          goodsBox:SetItemEnable(0, false)
        end
      else
        goodsBox:SetItemSuperscript(0, "")
      end
      itemName:SetProperty("Text", string.format("{#C^%s^%s}", gUI_GOODS_QUALITY_COLOR_PREFIX[Quality], Name))
      moneyParent:SetVisible(true)
      if Price > 0 then
        local strMoney = Lua_UI_Money2String(Price)
        money_dl:SetProperty("Visible", "true")
        money_dl:SetProperty("Text", strMoney)
        money_dl:SetCustomUserData(0, Price)
      else
        money_dl:SetProperty("Visible", "false")
      end
      currency_pic:SetCustomUserData(0, 0)
      currency_pic:SetCustomUserData(1, 0)
      currency_pic:SetCustomUserData(2, 0)
      if not _Shop_CU_Type then
        _Shop_CU_Type = CUType
      end
      if CUPrice >= 1 then
        currency_dl:SetProperty("Text", tostring(CUPrice))
        if CUType ~= 6 then
          currency_pic:SetProperty("BackImage", TokenTypeName[CUType].TokenIcon)
        else
          local _, icon = GetItemInfoBySlot(-1, CUItemId, -1)
          if icon then
            currency_pic:SetProperty("BackImage", tostring(icon))
          end
        end
        currency_pic:SetCustomUserData(0, CUType)
        currency_pic:SetCustomUserData(1, CUItemId)
        currency_pic:SetCustomUserData(2, CUPrice)
        currency_dl:SetVisible(true)
        currency_pic:SetVisible(true)
        local pic = moneyParent:GetChildByName("Crrency_pic")
        pic:SetProperty("TooltipText", TokenTypeName[CUType].TokenName)
      else
        currency_dl:SetVisible(false)
        currency_pic:SetVisible(false)
      end
      if CUPrice >= 1 and Price > 0 then
        currency_dl:SetProperty("Top", "p0")
        currency_pic:SetProperty("Top", "p0")
        itemName:SetProperty("Top", "p4")
        money_dl:SetProperty("Top", "p14")
      elseif CUPrice >= 1 then
        itemName:SetProperty("Top", "p11")
        currency_dl:SetProperty("Top", "p7")
        currency_pic:SetProperty("Top", "p7")
      elseif Price > 0 then
        itemName:SetProperty("Top", "p11")
        money_dl:SetProperty("Top", "p7")
      end
    end
  end
  _OnShop_CheckMoneyCanBuyItem()
end
function _Shop_BatchBuyInitAndShow(Price, name, CUPrice, CUType, CUItemId)
  gUI.NpcShop.BatchBuy:SetVisible(true)
  local Title_lab = gUI.NpcShop.BatchBuy:GetChildByName("Title_slab")
  Title_lab:SetProperty("Text", name)
  local money_dlab = gUI.NpcShop.BatchBuy:GetChildByName("UnitPriceS_slab")
  local money_lab = gUI.NpcShop.BatchBuy:GetChildByName("UnitPriceD_Dlab")
  local strPrice = Lua_UI_Money2String(Price)
  if CUType < 2 or CUPrice <= 0 or CUType > 19 then
    money_lab:SetVisible(false)
    money_dlab:SetVisible(false)
  else
    money_lab:SetVisible(true)
    money_lab:SetProperty("Text", tostring(CUPrice))
    money_dlab:SetVisible(true)
    if CUType ~= 6 then
      money_dlab:SetProperty("Text", TokenTypeName[CUType].TokenName .. ":")
    else
      local name, icon = GetItemInfoBySlot(-1, CUItemId, -1)
      money_dlab:SetProperty("Text", name)
    end
  end
  local num_ebox = gUI.NpcShop.BatchBuy:GetGrandChild("Picture2_bg.Count_ebox")
  num_ebox:SetProperty("Text", "1")
  local totalMoney_lab = gUI.NpcShop.BatchBuy:GetChildByName("AllPriceD_dlab")
  local totalMoney_dlab = gUI.NpcShop.BatchBuy:GetChildByName("AllPriceS_slab")
  if Price > 0 then
    totalMoney_lab:SetProperty("Text", strPrice)
    totalMoney_lab:SetVisible(true)
    totalMoney_dlab:SetVisible(true)
  else
    totalMoney_lab:SetVisible(false)
    totalMoney_dlab:SetVisible(false)
  end
  local Tips_lab = gUI.NpcShop.BatchBuy:GetChildByName("Tips_dlab")
  Tips_lab:SetVisible(false)
  TriggerUserGuide(27, -1, -1)
  if _Shop_CU_MainType == CU_BINDGOLD then
    gUI.NpcShop.BatchBuy:GetChildByName("AllPriceS_slab"):SetProperty("Text", "总价:")
  else
    gUI.NpcShop.BatchBuy:GetChildByName("AllPriceS_slab"):SetProperty("Text", "总价(金币):")
  end
end
function _Shop_BatchBindMoneyCheck(price, currNum, bindMoney)
  local Tips_lab = gUI.NpcShop.BatchBuy:GetChildByName("Tips_dlab")
  if bindMoney < price * currNum then
    Tips_lab:SetVisible(true)
  else
    Tips_lab:SetVisible(false)
  end
end
function Lua_Shop_SellItem(type, pos)
  local itemName, _, quality, _, _, _, _, _, CanSell, _, _, itemCount = GetItemInfoBySlot(type, pos)
  if CanSell == 0 then
    Lua_Chat_ShowOSD("SHOP_NO_SELL")
    return
  end
  if quality and quality > 1 then
    Messagebox_Show("SHOP_RARYITY_GOODS_TO_SELL", type, pos, itemName, quality, itemCount)
  else
    SellItem(type, pos)
  end
end
function Lua_Shop_GetItemWindow(index)
  return _Shop_PageItemWndList[index]
end
function _OnShop_Show(strNPCName)
  _Shop_ShopCurPage = 0
  gUI.NpcShop.ShowCurrNote:SetVisible(false)
  gUI.NpcShop.ShowCurrNote2:SetVisible(false)
  gUI.NpcShop.Root:SetVisible(true)
  gUI.NpcShop.Root:GetGrandChild("Main_bg.Title_slab"):SetProperty("Text", strNPCName)
  if not gUI.Bag.WndRoot:IsVisible() then
    Lua_Bag_ShowUI(true)
  end
  _Shop_ClearAllPageItem()
  gUI.NpcShop.SoldItemGoods:ResetAllGoods(true)
  TriggerUserGuide(26, -1, -1)
  local nIndex = 0
  if gUI.NpcShop.EquipCheck:IsChecked() then
    nIndex = 1
  else
    nIndex = 0
  end
  GenShopItemData(0, nIndex)
end
function _OnShop_ShopResult(TypeNum, typenam1, typenam2, typenam3, typenam4, typenam5, typenam6)
  if TypeNum > 0 then
    gUI.NpcShop.Tab:SetProperty("ItemCount", tostring(TypeNum))
  end
  local tableString = ""
  local typeNameList = {
    typenam1,
    typenam2,
    typenam3,
    typenam4,
    typenam5,
    typenam6
  }
  for i = 1, TypeNum do
    tableString = tableString .. typeNameList[i]
    if i ~= TypeNum then
      tableString = tableString .. "|"
    end
  end
  gUI.NpcShop.Tab:SetSelectedState(0)
  gUI.NpcShop.Tab:SetProperty("ItemsText", tableString)
  _Shop_ShowCurrPageNum(0, 0)
  _Shop_ShowShopGoods(0, 0)
end
function _OnShop_AddSoldItem(count)
  gUI.NpcShop.SoldItemGoods:ResetAllGoods(true)
  for i = 0, count - 1 do
    local iconName, quality, price, num, itemID, itemName = GetSoldItemInfo(i)
    if iconName then
      gUI.NpcShop.SoldItemGoods:SetItemGoods(i, iconName, quality)
      gUI.NpcShop.SoldItemGoods:SetItemData(i, price)
      if num > 1 then
        gUI.NpcShop.SoldItemGoods:SetItemSubscript(i, tostring(num))
      end
    end
  end
end
function _OnShop_CheckMoneyCanBuyItem(newMoney, changeMoney, event)
  if not gUI.NpcShop.Root:IsVisible() then
    return
  end
  gUI.NpcShop.ShowCurrNote:SetVisible(false)
  local _, money, bindMoney = GetBankAndBagMoney()
  if newMoney then
    if event == "PLAYER_BIND_MONEY_CHANGED" then
      bindMoney = newMoney
    elseif event == "PLAYER_MONEY_CHANGED" then
      money = newMoney
    end
  end
  gUI.NpcShop.ShowMoney:SetProperty("Text", Lua_UI_Money2String(money))
  gUI.NpcShop.ShowBindMoney:SetProperty("Text", Lua_UI_Money2String(bindMoney))
  local CuNum, CuTypeStr
  local CurrentIDs = -1
  if _Shop_CU_Type then
    local _, _, Nimbus, _, _, _, _, lingli, _, _, xiayi, TianzhuPoint, DAYKILL, TOTALKILL, BGHONOR = GetPlaySelfProp(4)
    local _, _, donation = GetPlaySelfProp(5)
    if _Shop_CU_Type == CURRENCY_UNIT.CU_GUILD_SCORE then
      CuNum = donation
      CuTypeStr = "贡献度："
      CurrentIDs = 8
    elseif _Shop_CU_Type == CURRENCY_UNIT.CU_GOODBAD then
      CuNum = xiayi
      CuTypeStr = "侠义值："
      CurrentIDs = 10
    elseif _Shop_CU_Type == CURRENCY_UNIT.CU_SPIRITVALUE then
      CuNum = lingli
      CuTypeStr = "灵力值："
      CurrentIDs = 9
    elseif _Shop_CU_Type == CURRENCY_UNIT.CU_NIMBUS then
      CuNum = Nimbus
      CuTypeStr = "精魄值："
    elseif _Shop_CU_Type == CURRENCY_UNIT.CU_BGHONOR then
      CuNum = BGHONOR
      CuTypeStr = "荣誉值："
    end
  end
  if event == "PLAYER_GUILDE_SELFPRO_CHANGED" and _Shop_CU_Type == CURRENCY_UNIT.CU_GUILD_SCORE then
    CuNum = newMoney
  end
  if CuNum and CuTypeStr then
    gUI.NpcShop.CurrencyType:SetProperty("Text", CuTypeStr)
    gUI.NpcShop.ShowCurrency:SetProperty("Text", tostring(CuNum))
    gUI.NpcShop.ShowCurrency:SetProperty("CustomUserData", tostring(CurrentIDs))
    gUI.NpcShop.CurrencyType:SetProperty("CustomUserData", tostring(CurrentIDs))
    gUI.NpcShop.ShowCurrency:SetProperty("MouseTrans", "false")
    gUI.NpcShop.CurrencyType:SetProperty("MouseTrans", "false")
  else
    gUI.NpcShop.CurrencyType:SetProperty("Text", "")
    gUI.NpcShop.ShowCurrency:SetProperty("Text", "")
    gUI.NpcShop.ShowCurrency:SetProperty("MouseTrans", "true")
    gUI.NpcShop.CurrencyType:SetProperty("MouseTrans", "true")
  end
  for index, parent in pairs(_Shop_PageItemWndList) do
    local goodsBox, itemName, moneyParent, money_dl, currency_dl, currency_pic = _Shop_GetAllChildByTmp(parent)
    if goodsBox:IsItemHasIcon(0) then
      if currency_pic:GetCustomUserData(0) == 6 then
        local itemId = currency_pic:GetCustomUserData(1)
        local itemNum = currency_pic:GetCustomUserData(2)
        local num = UserBagCountItemByID(itemId)
        if itemNum > num then
          currency_dl:SetProperty("FontColor", "FFFF0000")
        else
          currency_dl:SetProperty("FontColor", "FFFAFAFA")
        end
        gUI.NpcShop.ShowCurrNote:SetVisible(true)
      elseif currency_dl:IsVisible() then
        local strDonation = currency_dl:GetProperty("Text")
        if CuNum == nil or strDonation == "" or CuNum < tonumber(strDonation) then
          currency_dl:SetProperty("FontColor", "FFFF0000")
        else
          currency_dl:SetProperty("FontColor", "FFFAFAFA")
        end
      end
      local itemMoney = money_dl:GetCustomUserData(0)
      if _Shop_CU_MainType == CU_BINDGOLD then
        money_dl:SetProperty("Text", Lua_UI_Money2String(itemMoney, false, false, false, true, newMoney, event))
      else
        money_dl:SetProperty("Text", Lua_UI_Money2String(itemMoney, true, false, false, false, newMoney, event))
      end
    end
  end
end
function _OnShop_UpdateCurrPage()
  _Shop_ShowShopGoods(gUI.NpcShop.Tab:GetSelectItem(), _Shop_ShopCurPage, true)
end
function UI_Shop_SelChange(msg)
  local wnd = WindowToTableCtrl(msg:get_window())
  local selected = wnd:GetSelectItem()
  local nIndex = 0
  if gUI.NpcShop.EquipCheck:IsChecked() then
    nIndex = 1
  else
    nIndex = 0
  end
  GenShopItemData(selected, nIndex)
  _Shop_ShowCurrPageNum(selected, 0)
  _Shop_ShowShopGoods(selected, 0)
end
function UI_EquipMarkClick(msg)
  local selected = gUI.NpcShop.Tab:GetSelectItem()
  local nIndex = 0
  if gUI.NpcShop.EquipCheck:IsChecked() then
    nIndex = 1
  else
    nIndex = 0
  end
  GenShopItemData(selected, nIndex)
  _Shop_ShowCurrPageNum(selected, 0)
  _Shop_ShowShopGoods(selected, 0)
end
function UI_Shop_CloseClick(msg)
  gUI.NpcShop.Root:SetVisible(false)
  UI_Shop_CloseBatch()
end
function UI_Shop_GoodsBoxTooltip(msg)
  local wnd = msg:get_window()
  wnd = WindowToGoodsBox(wnd)
  local Slot = msg:get_wparam()
  if not wnd:IsItemHasIcon(Slot) then
    ItemId = -1
    return
  end
  local nIndex = wnd:GetCustomUserData(1)
  local Icon, Name, Price, Amount, Quality, ItemId, CUType, CUPrice, _, CUItemId = GetShopItemInfo(gUI.NpcShop.Tab:GetSelectItem(), nIndex)
  if CUType == 4 and CUPrice ~= 0 then
    local text = string.format("需要贡献度：%d", CUPrice)
    Lua_Tip_SetSpeciText(text, "", "")
  end
  Lua_Tip_Item(wnd:GetToolTipWnd(0), nil, nil, nil, nil, ItemId, Price)
  Lua_Tip_ShowEquipCompare(wnd, -1, ItemId)
  WorldStage:SetItemCursor(1)
end
function UI_Shop_BuySingleItem(msg)
  local shopTypeIndex = gUI.NpcShop.Tab:GetSelectItem()
  local goodsBox = WindowToGoodsBox(msg:get_window())
  local Index = goodsBox:GetCustomUserData(1)
  local Index1 = goodsBox:GetCustomUserData(2)
  local _, _, Price = GetShopItemInfo(shopTypeIndex, Index)
  local _, money, bindMoney = GetBankAndBagMoney()
  if _Shop_CU_MainType == CU_BINDGOLD then
    if Price > money + bindMoney then
      Lua_Chat_ShowOSD("NOT_ENOUGH_MONEY")
      return
    end
  elseif Price > money then
    Lua_Chat_ShowOSD("NOT_ENOUGH_NOBINDMONEY")
    return
  end
  if IsBagFull() then
    Lua_Chat_ShowOSD("RMBSHOP_SOLTFULL")
    return
  end
  if _Shop_CU_MainType == CU_BINDGOLD then
    if Price > bindMoney then
      Messagebox_Show("BUYITEM_CONFIRM", shopTypeIndex, Index1, 1)
    else
      BuyItem(shopTypeIndex, Index1, 1)
    end
  else
    if Price > money then
      Lua_Chat_ShowOSD("NOT_ENOUGH_NOBINDMONEY")
      return
    end
    BuyItem(shopTypeIndex, Index1, 1)
  end
end
function UI_Shop_ButBatchItem(msg)
  local shopTypeIndex = gUI.NpcShop.Tab:GetSelectItem()
  local goodsBox = WindowToGoodsBox(msg:get_window())
  local Index = goodsBox:GetCustomUserData(1)
  local Index1 = goodsBox:GetCustomUserData(2)
  gUI.NpcShop.Tab:SetItemData(shopTypeIndex, Index)
  gUI.NpcShop.Tab:SetCustomUserData(1, Index1)
  local Icon, name, Price, Amount, Quality, ItemId, CUType, CUPrice, maxCount, CUItemId = GetShopItemInfo(shopTypeIndex, Index)
  _Shop_Buy_CurrIndex = Index
  _Shop_Buy_CurrType = shopTypeIndex
  local _, money, bindMoney = GetBankAndBagMoney()
  if _Shop_CU_MainType == CU_UNBINDGOLD then
    if Price > money then
      Lua_Chat_ShowOSD("NOT_ENOUGH_NOBINDMONEY")
      return
    end
  elseif Price > money + bindMoney then
    Lua_Chat_ShowOSD("NOT_ENOUGH_MONEY")
    return
  end
  local HasNum = _Shop_Check_CuiType(CUType, CUItemId)
  if CUType >= 2 and CUType <= 19 and CUPrice > HasNum then
    ShowErrorMessage("没有足够的" .. TokenTypeName[CUType].TokenName)
    return
  end
  _Shop_BatchBuyInitAndShow(Price, name, CUPrice, CUType, CUItemId)
  if _Shop_CU_MainType == CU_BINDGOLD then
    _Shop_BatchBindMoneyCheck(Price, 1, bindMoney)
  elseif _Shop_CU_MainType == CU_UNBINDGOLD then
  end
end
function UI_Shop_ButBatchItemConfirm()
  local shopTypeIndex = gUI.NpcShop.Tab:GetSelectItem()
  local Index = gUI.NpcShop.Tab:GetItemData(shopTypeIndex)
  local Index1 = gUI.NpcShop.Tab:GetCustomUserData(1)
  local num_ebox = gUI.NpcShop.BatchBuy:GetGrandChild("Picture2_bg.Count_ebox")
  local strNum = num_ebox:GetProperty("Text")
  if strNum == "" or strNum == "0" then
    Lua_Chat_ShowOSD("SHOP_NEEDNUM")
    return
  end
  BuyItem(shopTypeIndex, Index1, tonumber(strNum))
  UI_Shop_CloseBatch()
end
function UI_Shop_AddBuyNum()
  local num_ebox = gUI.NpcShop.BatchBuy:GetGrandChild("Picture2_bg.Count_ebox")
  local money_dlab = gUI.NpcShop.BatchBuy:GetChildByName("UnitPriceS_slab")
  local money_lab = gUI.NpcShop.BatchBuy:GetChildByName("UnitPriceD_Dlab")
  local totalMoney_dlab = gUI.NpcShop.BatchBuy:GetChildByName("AllPriceS_slab")
  local totalMoney_lab = gUI.NpcShop.BatchBuy:GetChildByName("AllPriceD_dlab")
  local Icon, name, Price, Amount, Quality, ItemId, CUType, CUPrice, maxCount, CUItemId = GetShopItemInfo(_Shop_Buy_CurrType, _Shop_Buy_CurrIndex)
  local _, money, bindMoney = GetBankAndBagMoney()
  local maxNum = 0
  local HasNum = _Shop_Check_CuiType(CUType, CUItemId)
  local CurrMon = money
  if _Shop_CU_MainType == CU_BINDGOLD then
    CurrMon = money + bindMoney
  end
  if CUType >= 2 and CUType <= 19 then
    if Price > 0 then
      if math.floor(CurrMon / Price) > math.floor(HasNum / CUPrice) then
        maxNum = math.floor(HasNum / CUPrice)
      else
        maxNum = math.floor(CurrMon / Price)
      end
    else
      maxNum = math.floor(HasNum / CUPrice)
    end
  else
    maxNum = math.floor(CurrMon / Price)
  end
  local strNum = num_ebox:GetProperty("Text")
  if strNum == "" then
    strNum = "0"
  end
  local currNum = tonumber(strNum)
  local nowNum = currNum + 1
  if maxNum < nowNum then
    nowNum = maxNum
  end
  if nowNum > 99 then
    nowNum = 99
  end
  num_ebox:SetProperty("Text", tostring(nowNum))
  local strTotalMoney = Lua_UI_Money2String(nowNum * Price)
  local strTotalNum = tostring(nowNum * CUPrice)
  totalMoney_lab:SetProperty("Text", strTotalMoney)
  money_lab:SetProperty("Text", strTotalNum)
  if _Shop_CU_MainType == CU_BINDGOLD then
    _Shop_BatchBindMoneyCheck(Price, nowNum, bindMoney)
  end
end
function UI_Shop_DecBuyNum()
  local num_ebox = gUI.NpcShop.BatchBuy:GetGrandChild("Picture2_bg.Count_ebox")
  local money_dlab = gUI.NpcShop.BatchBuy:GetChildByName("UnitPriceS_slab")
  local money_lab = gUI.NpcShop.BatchBuy:GetChildByName("UnitPriceD_Dlab")
  local totalMoney_dlab = gUI.NpcShop.BatchBuy:GetChildByName("AllPriceS_slab")
  local totalMoney_lab = gUI.NpcShop.BatchBuy:GetChildByName("AllPriceD_dlab")
  local Icon, name, Price, Amount, Quality, ItemId, CUType, CUPrice, maxCount, CUItemId = GetShopItemInfo(_Shop_Buy_CurrType, _Shop_Buy_CurrIndex)
  local _, money, bindMoney = GetBankAndBagMoney()
  local strNum = num_ebox:GetProperty("Text")
  if strNum == "" then
    strNum = "0"
  end
  local currNum = tonumber(strNum)
  local nowNum = currNum - 1
  if nowNum <= 0 then
    nowNum = 1
  end
  num_ebox:SetProperty("Text", tostring(nowNum))
  local strTotalMoney = Lua_UI_Money2String(nowNum * Price)
  local strTotalNum = tostring(nowNum * CUPrice)
  totalMoney_lab:SetProperty("Text", strTotalMoney)
  money_lab:SetProperty("Text", strTotalNum)
  if _Shop_CU_MainType == CU_BINDGOLD then
    _Shop_BatchBindMoneyCheck(Price, nowNum, bindMoney)
  end
end
function _Shop_Check_CuiType(CUType, CUItemId)
  local _, _, Nimbus, _, _, _, _, lilingPoint, _, _, xiayiPoint, TianzhuPoint, DAYKILL, TOTALKILL, BGHONOR = GetPlaySelfProp(4)
  local _, _, _, _, _, _, _, my_guid = GetMyPlayerStaticInfo()
  local hisPoint, dailyPoint, weekPros, hisPros = GetGuildMemberExInfo(my_guid)
  if CUType == CURRENCY_UNIT.CU_INGOT then
    return _CURR_WelfareNum
  elseif CUType == CURRENCY_UNIT.CU_GUILD_SCORE then
    return dailyPoint
  elseif CUType == CURRENCY_UNIT.CU_GOODBAD then
    return xiayiPoint
  elseif CUType == CURRENCY_UNIT.CU_SPIRITVALUE then
    return lilingPoint
  elseif CUType == CURRENCY_UNIT.CU_NIMBUS then
    return Nimbus
  elseif CUType == CURRENCY_UNIT.CU_BGHONOR then
    return BGHONOR
  elseif CUType == CURRENCY_UNIT.CU_TOKEN then
    local _, itemCount = GetBaseInfoByItemId(CUItemId)
    return itemCount
  end
  return 0
end
function _Shop_Update_CurrWelfareNum(WelfareNum)
  _CURR_WelfareNum = WelfareNum
end
function UI_Shop_MinNum(msg)
  local num_ebox = gUI.NpcShop.BatchBuy:GetGrandChild("Picture2_bg.Count_ebox")
  local money_dlab = gUI.NpcShop.BatchBuy:GetChildByName("UnitPriceS_slab")
  local money_lab = gUI.NpcShop.BatchBuy:GetChildByName("UnitPriceD_Dlab")
  local totalMoney_dlab = gUI.NpcShop.BatchBuy:GetChildByName("AllPriceS_slab")
  local totalMoney_lab = gUI.NpcShop.BatchBuy:GetChildByName("AllPriceD_dlab")
  local Icon, name, Price, Amount, Quality, ItemId, CUType, CUPrice, maxCount, CUItemId = GetShopItemInfo(_Shop_Buy_CurrType, _Shop_Buy_CurrIndex)
  local _, money, bindMoney = GetBankAndBagMoney()
  num_ebox:SetProperty("Text", "1")
  local strTotalMoney = Lua_UI_Money2String(Price)
  local strTotalNum = tostring(CUPrice)
  totalMoney_lab:SetProperty("Text", strTotalMoney)
  money_lab:SetProperty("Text", strTotalNum)
  if _Shop_CU_MainType == CU_BINDGOLD then
    _Shop_BatchBindMoneyCheck(Price, 1, bindMoney)
  end
end
function UI_Shop_MaxNum(msg)
  local num_ebox = gUI.NpcShop.BatchBuy:GetGrandChild("Picture2_bg.Count_ebox")
  local money_dlab = gUI.NpcShop.BatchBuy:GetChildByName("UnitPriceS_slab")
  local money_lab = gUI.NpcShop.BatchBuy:GetChildByName("UnitPriceD_Dlab")
  local totalMoney_dlab = gUI.NpcShop.BatchBuy:GetChildByName("AllPriceS_slab")
  local totalMoney_lab = gUI.NpcShop.BatchBuy:GetChildByName("AllPriceD_dlab")
  local Icon, name, Price, Amount, Quality, ItemId, CUType, CUPrice, maxCount, CUItemId = GetShopItemInfo(_Shop_Buy_CurrType, _Shop_Buy_CurrIndex)
  local _, money, bindMoney = GetBankAndBagMoney()
  local maxNum = 0
  local HasNum = _Shop_Check_CuiType(CUType, CUItemId)
  local CurrMon = money
  if _Shop_CU_MainType == CU_BINDGOLD then
    CurrMon = money + bindMoney
  end
  if CUType >= 2 and CUType <= 19 then
    if Price > 0 then
      if math.floor(CurrMon / Price) > math.floor(HasNum / CUPrice) then
        maxNum = math.floor(HasNum / CUPrice)
      else
        maxNum = math.floor(CurrMon / Price)
      end
    else
      maxNum = math.floor(HasNum / CUPrice)
    end
  else
    maxNum = math.floor(CurrMon / Price)
  end
  local strNum = num_ebox:GetProperty("Text")
  if strNum == "" then
    strNum = "0"
  end
  local currNum = tonumber(strNum)
  if currNum < 99 then
    currNum = 99
  end
  if maxNum < currNum then
    currNum = maxNum
  end
  num_ebox:SetProperty("Text", tostring(currNum))
  local strTotalMoney = Lua_UI_Money2String(currNum * Price)
  local strTotalNum = tostring(currNum * CUPrice)
  totalMoney_lab:SetProperty("Text", strTotalMoney)
  money_lab:SetProperty("Text", strTotalNum)
  if _Shop_CU_MainType == CU_BINDGOLD then
    _Shop_BatchBindMoneyCheck(Price, currNum, bindMoney)
  end
end
function UI_Shop_NumChange()
  local num_ebox = gUI.NpcShop.BatchBuy:GetGrandChild("Picture2_bg.Count_ebox")
  local money_dlab = gUI.NpcShop.BatchBuy:GetChildByName("UnitPriceS_slab")
  local money_lab = gUI.NpcShop.BatchBuy:GetChildByName("UnitPriceD_Dlab")
  local totalMoney_dlab = gUI.NpcShop.BatchBuy:GetChildByName("AllPriceS_slab")
  local totalMoney_lab = gUI.NpcShop.BatchBuy:GetChildByName("AllPriceD_dlab")
  local Icon, name, Price, Amount, Quality, ItemId, CUType, CUPrice, maxCount, CUItemId = GetShopItemInfo(_Shop_Buy_CurrType, _Shop_Buy_CurrIndex)
  local _, money, bindMoney = GetBankAndBagMoney()
  local maxNum = 0
  local HasNum = _Shop_Check_CuiType(CUType, CUItemId)
  local CurrMoney = money
  if _Shop_CU_MainType == CU_BINDGOLD then
    CurrMoney = money + bindMoney
  end
  if CUType >= 2 and CUType <= 19 then
    if Price > 0 then
      if math.floor(CurrMoney / Price) > math.floor(HasNum / CUPrice) then
        maxNum = math.floor(HasNum / CUPrice)
      else
        maxNum = math.floor(CurrMoney / Price)
      end
    else
      maxNum = math.floor(HasNum / CUPrice)
    end
  else
    maxNum = math.floor(CurrMoney / Price)
  end
  local strNum = num_ebox:GetProperty("Text")
  if strNum == "" then
    strNum = ""
  end
  if strNum ~= "" then
    local currNum = tonumber(strNum)
    if currNum > 99 then
      currNum = 99
    end
    if maxNum < currNum then
      currNum = maxNum
    end
    num_ebox:SetProperty("Text", tostring(currNum))
    local strTotalMoney = Lua_UI_Money2String(currNum * Price)
    local strTotalNum = tostring(currNum * CUPrice)
    totalMoney_lab:SetProperty("Text", strTotalMoney)
    money_lab:SetProperty("Text", strTotalNum)
    if _Shop_CU_MainType == CU_BINDGOLD then
      _Shop_BatchBindMoneyCheck(Price, currNum, bindMoney)
    end
  else
    num_ebox:SetProperty("Text", tostring(""))
    totalMoney_lab:SetProperty("Text", "")
    money_lab:SetProperty("Text", "")
  end
end
function UI_Shop_CloseBatch()
  gUI.NpcShop.BatchBuy:SetVisible(false)
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function UI_Shop_BuyBackItem(msg)
  local Index = msg:get_lparam()
  local Price = msg:get_wparam()
  local _, money, bindMoney = GetBankAndBagMoney()
  local name, icon, quality, minLevel, job, sex, isUnique, canBank, canSell, canDiscard, bindinfo, count, maxCount, itemType, desc1, desc2, desc3, leftEffTime, sellPrice, sellType, equipKind, offlineTime, isTimeLimit, EquipPoint, SkillId, RuneLv, nitemId, nKind, MaxInBag = GetItemInfoBySlot(gUI_GOODSBOX_BAG.sold, Index, -1)
  if sellType == 1 then
    if Price > money then
      Lua_Chat_ShowOSD("NOT_ENOUGH_NOBINDMONEY")
      return
    end
  elseif Price > money + bindMoney then
    Lua_Chat_ShowOSD("NOT_ENOUGH_MONEY")
    return
  end
  if IsBagFull() then
    Lua_Chat_ShowOSD("RMBSHOP_SOLTFULL")
    return
  end
  if sellType == 1 then
    Messagebox_Show("BUYBACKUNBMITEM_CONFIRM", _SHOP_BUYBACKITEM, Index, 1, sellPrice, name, quality)
  elseif Price > bindMoney and bindMoney ~= 0 then
    Messagebox_Show("BUYITEM_CONFIRM", _SHOP_BUYBACKITEM, Index, 1)
  else
    BuyItem(_SHOP_BUYBACKITEM, Index, 1)
    WorldStage:SetItemCursor(0)
  end
end
function UI_Shop_BackItemTooltip(msg)
  local wnd = msg:get_window()
  local index = msg:get_wparam()
  if gUI.NpcShop.SoldItemGoods:IsItemHasIcon(index) then
    local _, _, price, _, itemID = GetSoldItemInfo(index)
    if price then
      local text = string.format("回购价格:")
      Lua_Tip_SetSpeciText(text, "", "", price)
      Lua_Tip_Item(wnd:GetToolTipWnd(0), index, gUI_GOODSBOX_BAG.sold, nil, nil, nil, 0)
      Lua_Tip_ShowEquipCompare(wnd, -1, itemID)
      WorldStage:SetItemCursor(1)
      return
    end
  end
  WorldStage:SetItemCursor(0)
end
function UI_Shop_AddPageClick(msg)
  local shopTypeIndex = gUI.NpcShop.Tab:GetSelectItem()
  local oldPage = _Shop_ShopCurPage
  _Shop_ShowCurrPageNum(shopTypeIndex, _Shop_ShopCurPage + 1)
  if _Shop_ShopCurPage ~= oldPage then
    _Shop_ShowShopGoods(shopTypeIndex, _Shop_ShopCurPage)
  end
end
function UI_Shop_DecPageClick(msg)
  local shopTypeIndex = gUI.NpcShop.Tab:GetSelectItem()
  local oldPage = _Shop_ShopCurPage
  _Shop_ShowCurrPageNum(shopTypeIndex, _Shop_ShopCurPage - 1)
  if _Shop_ShopCurPage ~= oldPage then
    _Shop_ShowShopGoods(shopTypeIndex, _Shop_ShopCurPage)
  end
end
function UI_Shop_ItemDrop(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  local from_off = msg:get_wparam()
  if from_type == "backpack0" then
    from_off = Lua_Bag_GetRealSlot(from_win, from_off)
    Lua_Shop_SellItem(gUI_GOODSBOX_BAG[from_type], from_off)
  else
    Lua_Chat_ShowOSD("SHOP_CANNOTSELL")
  end
end
function UI_Tip_ShowText_NpcShop(msg)
  local win = msg:get_window()
  local tooltip = WindowToTooltip(win:GetToolTipWnd(0))
  if win:GetCustomUserData(0) ~= 6 then
    UI_Tip_ShowText(msg)
  else
    local itemId = win:GetCustomUserData(1)
    Lua_Tip_Item(tooltip, nil, nil, nil, nil, itemId)
  end
end
function UI_Shop_ItemDropToShop()
  if WindowSys_Instance:isDragItemIcon() then
    local from_win = gUI.NpcShop.TempGoodsBox:GetDragItemParent()
    local from_off = gUI.NpcShop.TempGoodsBox:GetDragItemIndex()
    local from_type = from_win:GetProperty("BoxType")
    if from_type == "backpack0" then
      from_off = Lua_Bag_GetRealSlot(from_win, from_off)
      Lua_Shop_SellItem(gUI_GOODSBOX_BAG[from_type], from_off)
    else
      Lua_Chat_ShowOSD("SHOP_CANNOTSELL")
    end
    gUI.NpcShop.TempGoodsBox:ResetDragItem()
  end
end
function UI_Shop_GoodsBoxLeave()
  WorldStage:SetItemCursor(0)
end
function Script_Shop_OnLoad()
  _Shop_InitWnd()
end
function Script_Shop_OnHide()
  gUI.NpcShop.BatchBuy:SetVisible(false)
  ResetShopItemData()
end
function Script_Shop_OnEvent(event)
  if event == "NPCSHOP_SHOW" then
    _OnShop_Show(arg1)
  elseif event == "NPCSHOP_SHOP_RESULT" then
    _OnShop_ShopResult(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif event == "NPCSHOP_ADD_SOLD_ITEM" then
    _OnShop_AddSoldItem(arg1)
  elseif event == "NPCSHOP_UPDATE_ITEM" then
    _OnShop_UpdateCurrPage()
  elseif event == "CLOSE_NPC_RELATIVE_DIALOG" then
    UI_Shop_CloseClick()
  elseif event == "PLAYER_BIND_MONEY_CHANGED" or event == "PLAYER_MONEY_CHANGED" or event == "PLAYER_GUILDE_SELFPRO_CHANGED" or event == "PLAYER_INFO_CHANGED" or event == "ITEM_ADD_TO_BAG" or event == "ITEM_DEL_FROM_BAG" then
    _OnShop_CheckMoneyCanBuyItem(arg1, arg2, event)
  end
end

