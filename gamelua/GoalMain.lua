if gUI and not gUI.GoalM then
  gUI.GoalM = {}
end
local _GoalM_TrackingMaxNum = 2
local _GoalM_ComplateUIMaxTime = 30
local _GoalM_ComplateUICurTime = 0
local _GoalM_InsertItemResult = false
function _GoalM_InsertChildBtn(childType)
  local newItem = gUI.GoalM.GoalContainer:Insert(-1, gUI.GoalM.CntChildTemp2)
  newItem:SetProperty("Visible", "true")
  newItem:SetProperty("Text", childType.text)
  newItem:SetCustomUserData(0, childType.index)
  childType.control = WindowToCheckButton(newItem)
end
function _GoalM_InsertChild(_item, typeIndex, _daily, _defy, _complated)
  if _daily == _item.daily and _defy == _item.defy and _complated == _item.complated then
    _GoalM_InsertItemResult = true
    local _name, _desc, _tacking, _daily, _complate, _count, _countmax, _money, _moneybind, _itemid, _itemcount, _active, _exp, _nim = GetGoalInfo(_item.id)
    local newItem = gUI.GoalM.GoalContainer:Insert(-1, gUI.GoalM.CntChildTemp1)
    newItem:SetProperty("Visible", "true")
    newItem:SetCustomUserData(0, _item.id)
    newItem:SetCustomUserData(1, typeIndex)
    newItem:setEnableHeightCal(typeIndex == gUI.GoalM.CurTypeIndex)
    local itemMember = newItem:GetChildByName("Member_btn")
    itemMember:SetCustomUserData(0, _item.id)
    local itemChecker = WindowToCheckButton(itemMember:GetChildByName("Goal_cbtn"))
    itemChecker:SetCustomUserData(0, _item.id)
    itemChecker:SetChecked(_item.tracking)
    local itemName = itemMember:GetChildByName("Name_dlab")
    itemName:SetProperty("Text", _name)
    if _item.complated then
      itemName:SetProperty("FontColor", "0xFF00FF00")
    else
      itemName:SetProperty("FontColor", "0xFFFFFFFF")
    end
    itemMember:GetChildByName("Image1_pic"):SetVisible(not _item.complated)
    itemMember:GetChildByName("Image2_pic"):SetVisible(_item.complated)
    itemMember:GetChildByName("Desc_dlab"):SetProperty("Text", _desc)
    if _item.complated then
      itemMember:GetChildByName("Award_dlab"):SetProperty("Text", "已完成")
    else
      itemMember:GetChildByName("Award_dlab"):SetProperty("Text", "" .. _count .. "/" .. _countmax)
    end
    local icon1, count1, name1, quilty1 = GetBaseInfoByItemId(_itemid)
    itemMember:GetChildByName("Finish_btn"):SetVisible(_complate)
    itemMember:GetChildByName("Finish_btn"):SetCustomUserData(0, _item.id)
    itemMember:GetChildByName("Reward_gbox"):SetCustomUserData(0, _itemid)
    local gboxAward = WindowToGoodsBox(itemMember:GetChildByName("Reward_gbox"))
    gboxAward:SetVisible(icon1 ~= "" and _itemcount > 0)
    gboxAward:SetItemGoods(0, icon1, quilty1)
    gboxAward:SetItemSubscript(0, tostring(_itemcount))
    local childReward1 = itemMember:GetChildByName("Reward1_dlab")
    local childReward2 = itemMember:GetChildByName("Reward2_dlab")
    local getCont = {}
    if _money > 0 then
      getCont[1] = "金币：" .. Lua_UI_Money2String(_money)
    elseif _moneybind > 0 then
      getCont[2] = "绑金：" .. Lua_UI_Money2String(_moneybind)
    elseif _exp > 0 then
      getCont[3] = "经验：" .. Lua_UI_Money2String(tostring(_exp))
    elseif _nim > 0 then
      getCont[4] = "精魄：" .. Lua_UI_Money2String(tostring(_nim))
    end
    childReward1:SetVisible(false)
    childReward2:SetVisible(false)
    for key, value in pairs(getCont) do
      if gboxAward:IsVisible() then
        if not childReward2:IsVisible() then
          childReward2:SetProperty("Text", value)
          childReward2:SetVisible(true)
        elseif not childReward1:IsVisible() then
          childReward1:SetProperty("Text", value)
          childReward1:SetVisible(true)
        end
      elseif not childReward1:IsVisible() then
        childReward1:SetProperty("Text", value)
        childReward1:SetVisible(true)
      elseif not childReward2:IsVisible() then
        childReward2:SetProperty("Text", value)
        childReward2:SetVisible(true)
      end
    end
  end
end
function _GoalM_UpdateTrackUI()
  local _tacknum = GetGoalMngInfo()
  local trackBg = gUI.GoalM.GoalTrackUI:GetChildByName("Track_bg")
  local nobody = trackBg:GetChildByName("Nobody_slab")
  nobody:SetVisible(_tacknum <= 0)
  local track1 = trackBg:GetChildByName("Goal1_bg")
  local track2 = trackBg:GetChildByName("Goal2_bg")
  track1:SetVisible(false)
  track2:SetVisible(false)
  track1:GetChildByName("Label1_dlab"):SetProperty("Text", "")
  track1:GetChildByName("Label2_dlab"):SetProperty("Text", "")
  track2:GetChildByName("Label1_dlab"):SetProperty("Text", "")
  track2:GetChildByName("Label2_dlab"):SetProperty("Text", "")
  if _tacknum > 0 then
    local trackNum = 1
    local goalList = GetGoalList()
    for i = 1, table.maxn(goalList) do
      local _item = goalList[i]
      if _item.tracking and trackNum <= _GoalM_TrackingMaxNum then
        local trackBgTmp = trackBg:GetChildByName("Goal" .. trackNum .. "_bg")
        local _name, _desc, _, _, _, _count, _countmax = GetGoalInfo(_item.id)
        trackBgTmp:GetChildByName("Label1_dlab"):SetProperty("Text", _name)
        if _item.complated then
          trackBgTmp:GetChildByName("Label2_dlab"):SetProperty("Text", "已完成")
        else
          trackBgTmp:GetChildByName("Label2_dlab"):SetProperty("Text", "" .. _count .. "/" .. _countmax)
        end
        trackBgTmp:SetVisible(true)
        trackNum = trackNum + 1
      end
    end
  end
end
function _GoalM_ShowTimeOver(timer)
  _GoalM_ComplateUICurTime = _GoalM_ComplateUICurTime - timer:GetRealInterval()
  if _GoalM_ComplateUICurTime < 0 then
    gUI.GoalM.GoalFinishUI:SetVisible(false)
    DelTimerEvent(1, "GoalComplateShowTime")
  end
end
function _GoalM_SetQuestBtnSelected(goalID)
  local res = false
  local qCount = gUI.GoalM.GoalContainer:GetChildCount()
  for i = 0, qCount - 1 do
    local itemTmp = gUI.GoalM.GoalContainer:GetChild(i)
    local qButton = itemTmp:GetChildByName("Member_btn")
    if qButton ~= nil then
      qButton = WindowToButton(qButton)
      local qBtnID = qButton:GetCustomUserData(0)
      if qBtnID ~= 0 then
        if qBtnID == goalID then
          qButton:SetStatus("selected")
          res = true
        else
          qButton:SetStatus("normal")
        end
      end
    end
  end
  return res
end
function _OnGoalM_UpdateMain()
  gUI.GoalM.GoalContainer:DeleteAll()
  local goalList = GetGoalList()
  _GoalM_InsertItemResult = false
  for i = 1, table.maxn(goalList) do
    _GoalM_InsertChild(goalList[i], gUI.GoalM.ItemType.Fish.index, false, false, true)
  end
  for i = 1, table.maxn(goalList) do
    _GoalM_InsertChild(goalList[i], gUI.GoalM.ItemType.Fish.index, false, false, false)
  end
  gUI.GoalM.GoalEmpty:SetVisible(not _GoalM_InsertItemResult)
  _GoalM_UpdateTrackUI()
  _GoalM_SetQuestBtnSelected(-1)
  gUI.GoalM.SelectedGoalID = -1
  gUI.GoalM.GoalContainer:SizeSelf()
end
function _OnGoalM_ComplateGoal(goalID)
  local _name = GetGoalInfo(goalID)
  if _name == nil then
    Lua_Chat_AddSysLog("阶段目标ID获取失败" .. tostring(goalID))
    return
  end
  local finishBg = gUI.GoalM.GoalFinishUI:GetChildByName("Finish_bg")
  finishBg:GetChildByName("Name_dlab"):SetProperty("Text", _name)
  gUI.GoalM.GoalFinishUI:SetVisible(true)
  gUI.GoalM.GoalFinishUI:SetCustomUserData(0, goalID)
  _GoalM_ComplateUICurTime = _GoalM_ComplateUIMaxTime
  AddTimerEvent(1, "GoalComplateShowTime", _GoalM_ShowTimeOver)
end
function _OnGoalM_TrackingGoal(goalID, isTrack)
  local itemTmp = gUI.GoalM.GoalContainer:GetChildByData0(goalID)
  if itemTmp ~= nil then
    local itemMember = itemTmp:GetChildByName("Member_btn")
    local itemChecker = WindowToCheckButton(itemMember:GetChildByName("Goal_cbtn"))
    itemChecker:SetChecked(isTrack)
    _GoalM_UpdateTrackUI()
  end
end
function _OnGoalM_ParamChangeGoal(goalID, count)
  _GoalM_UpdateTrackUI()
end
function _OnGoalM_ChangeMap()
  gUI.GoalM.GoalFinishUI:SetVisible(false)
end
function UI_GoalM_OpenGoalM()
  gUI.GoalM.GoalMain:SetVisible(not gUI.GoalM.GoalMain:IsVisible())
end
function UI_GoalM_CloseGoalM()
  gUI.GoalM.GoalMain:SetVisible(false)
end
function UI_GoalM_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("GoalMain.Goal_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_GoalM_ClickFinishBtn(msg)
  local wnd = msg:get_window()
  local goalID = wnd:GetCustomUserData(0)
  if goalID > 0 then
    GoalSubmit(goalID)
  end
end
function UI_GoalM_ClickTacking(msg)
  local wnd = msg:get_window()
  local goalID = wnd:GetCustomUserData(0)
  if goalID > 0 then
    local _name, _desc, _tacking, _daily = GetGoalInfo(goalID)
    local _tacknum = GetGoalMngInfo()
    if not WindowToCheckButton(wnd):IsChecked() then
      GoalTracking(goalID)
      return
    elseif _tacknum >= _GoalM_TrackingMaxNum then
      Lua_UI_ShowMessageInfo("同一时间最多追踪两个目标")
    else
      GoalTracking(goalID)
      return
    end
  end
  WindowToCheckButton(wnd):SetChecked(false)
end
function UI_GoalM_ClickFinishUI(msg)
  local goalID = gUI.GoalM.GoalFinishUI:GetCustomUserData(0)
  local _name, _, _, _daily = GetGoalInfo(goalID)
  if _daily then
    Lua_GoalP_ShowGoalPossess(true)
  else
    gUI.GoalM.GoalMain:SetVisible(true)
  end
  gUI.GoalM.GoalFinishUI:SetVisible(false)
  DelTimerEvent(1, "GoalComplateShowTime")
end
function UI_GoalM_ShowTipsAward(msg)
  local win = msg:get_window()
  local tooltip = win:GetToolTipWnd(0)
  local goalID = win:GetCustomUserData(0)
  Lua_Tip_Item(tooltip, nil, nil, nil, nil, goalID)
end
function Script_GoalM_OnLoad()
  gUI.GoalM.GoalMain = WindowSys_Instance:GetWindow("GoalMain")
  gUI.GoalM.CntChildTemp1 = WindowSys_Instance:GetWindow("GoalMain.GoalTemp_bg")
  gUI.GoalM.CntChildTemp2 = WindowSys_Instance:GetWindow("GoalMain.GoalTemp_cbtn")
  gUI.GoalM.GoalContainer = WindowToContainer(WindowSys_Instance:GetWindow("GoalMain.Goal_bg.Goal_cnt"))
  gUI.GoalM.GoalEmpty = WindowToContainer(WindowSys_Instance:GetWindow("GoalMain.Goal_bg.Goal_slab"))
  gUI.GoalM.GoalTrackUI = WindowSys_Instance:GetWindow("GoalTrack")
  gUI.GoalM.GoalFinishUI = WindowSys_Instance:GetWindow("GoalFinish")
  gUI.GoalM.SelectedGoalID = -1
  gUI.GoalM.CurTypeIndex = 0
  gUI.GoalM.ItemType = {
    Fish = {index = 0, text = "新手目标"},
    Daily = {index = 1, text = "每日目标"},
    Defy = {index = 2, text = "挑战目标"}
  }
  gUI.GoalM.GoalContainer:DeleteAll()
end
function Script_GoalM_OnEvent(event)
  if event == "STAGEGOAL_REFRESH_UI" then
    _OnGoalM_UpdateMain()
  elseif event == "STAGEGOAL_COMPLATE" then
    _OnGoalM_ComplateGoal(arg1)
  elseif event == "STAGEGOAL_TRACKING" then
    _OnGoalM_TrackingGoal(arg1, arg2)
  elseif event == "STAGEGOAL_PARAMCHANGED" then
    _OnGoalM_ParamChangeGoal(arg1, arg2)
  elseif event == "STAGE_CHANGED" then
    _OnGoalM_ChangeMap()
  end
end
function Script_GoalM_OnHide()
end

