local g_strTimeDes = {
  [1] = "未激活",
  [2] = "永久有效",
  [3] = ""
}
{
  [1] = "默认表情",
  [2] = "表情包二",
  [3] = "名称五…",
  [4] = "六十一…",
  [5] = "六十个…",
  [6] = "表情包六",
  [7] = "表情包七",
  [8] = "表情包八",
  [9] = "表情包九",
  [10] = "表情包十",
  [11] = "表情十一",
  [12] = "表情十二",
  [13] = "表情十三",
  [14] = "表情包二",
  [15] = "表情包三",
  [16] = "表情包四",
  [17] = "表情包五",
  [18] = "表情包六",
  [18] = "表情包七",
  [19] = "表情包八",
  [20] = "表情包九"
}[21] = "表情包十"
{
  [1] = "默认表情",
  [2] = "表情包二",
  [3] = "名称五…",
  [4] = "六十一…",
  [5] = "六十个…",
  [6] = "表情包六",
  [7] = "表情包七",
  [8] = "表情包八",
  [9] = "表情包九",
  [10] = "表情包十",
  [11] = "表情十一",
  [12] = "表情十二",
  [13] = "表情十三",
  [14] = "表情包二",
  [15] = "表情包三",
  [16] = "表情包四",
  [17] = "表情包五",
  [18] = "表情包六",
  [18] = "表情包七",
  [19] = "表情包八",
  [20] = "表情包九"
}[22] = "表情包…"
{
  [1] = "默认表情",
  [2] = "表情包二",
  [3] = "名称五…",
  [4] = "六十一…",
  [5] = "六十个…",
  [6] = "表情包六",
  [7] = "表情包七",
  [8] = "表情包八",
  [9] = "表情包九",
  [10] = "表情包十",
  [11] = "表情十一",
  [12] = "表情十二",
  [13] = "表情十三",
  [14] = "表情包二",
  [15] = "表情包三",
  [16] = "表情包四",
  [17] = "表情包五",
  [18] = "表情包六",
  [18] = "表情包七",
  [19] = "表情包八",
  [20] = "表情包九"
}[23] = "表情包…"
{
  [1] = "默认表情",
  [2] = "表情包二",
  [3] = "名称五…",
  [4] = "六十一…",
  [5] = "六十个…",
  [6] = "表情包六",
  [7] = "表情包七",
  [8] = "表情包八",
  [9] = "表情包九",
  [10] = "表情包十",
  [11] = "表情十一",
  [12] = "表情十二",
  [13] = "表情十三",
  [14] = "表情包二",
  [15] = "表情包三",
  [16] = "表情包四",
  [17] = "表情包五",
  [18] = "表情包六",
  [18] = "表情包七",
  [19] = "表情包八",
  [20] = "表情包九"
}[24] = "表情包…"
local {
  [1] = "默认表情",
  [2] = "表情包二",
  [3] = "名称五…",
  [4] = "六十一…",
  [5] = "六十个…",
  [6] = "表情包六",
  [7] = "表情包七",
  [8] = "表情包八",
  [9] = "表情包九",
  [10] = "表情包十",
  [11] = "表情十一",
  [12] = "表情十二",
  [13] = "表情十三",
  [14] = "表情包二",
  [15] = "表情包三",
  [16] = "表情包四",
  [17] = "表情包五",
  [18] = "表情包六",
  [18] = "表情包七",
  [19] = "表情包八",
  [20] = "表情包九"
}[25], g_strTableTile = "特权表情", {
  [1] = "默认表情",
  [2] = "表情包二",
  [3] = "名称五…",
  [4] = "六十一…",
  [5] = "六十个…",
  [6] = "表情包六",
  [7] = "表情包七",
  [8] = "表情包八",
  [9] = "表情包九",
  [10] = "表情包十",
  [11] = "表情十一",
  [12] = "表情十二",
  [13] = "表情十三",
  [14] = "表情包二",
  [15] = "表情包三",
  [16] = "表情包四",
  [17] = "表情包五",
  [18] = "表情包六",
  [18] = "表情包七",
  [19] = "表情包八",
  [20] = "表情包九"
}
{
  [1] = "默认表情",
  [2] = "表情包二",
  [3] = "名称五汉字",
  [4] = "六十一个表情测试",
  [5] = "六十个表情测试",
  [6] = "",
  [7] = "",
  [8] = "",
  [9] = "",
  [10] = "",
  [11] = "",
  [12] = "",
  [13] = "",
  [14] = "",
  [15] = "",
  [16] = "",
  [17] = "",
  [18] = "",
  [18] = "",
  [19] = "",
  [20] = ""
}[21] = ""
{
  [1] = "默认表情",
  [2] = "表情包二",
  [3] = "名称五汉字",
  [4] = "六十一个表情测试",
  [5] = "六十个表情测试",
  [6] = "",
  [7] = "",
  [8] = "",
  [9] = "",
  [10] = "",
  [11] = "",
  [12] = "",
  [13] = "",
  [14] = "",
  [15] = "",
  [16] = "",
  [17] = "",
  [18] = "",
  [18] = "",
  [19] = "",
  [20] = ""
}[22] = "表情包二十二"
{
  [1] = "默认表情",
  [2] = "表情包二",
  [3] = "名称五汉字",
  [4] = "六十一个表情测试",
  [5] = "六十个表情测试",
  [6] = "",
  [7] = "",
  [8] = "",
  [9] = "",
  [10] = "",
  [11] = "",
  [12] = "",
  [13] = "",
  [14] = "",
  [15] = "",
  [16] = "",
  [17] = "",
  [18] = "",
  [18] = "",
  [19] = "",
  [20] = ""
}[23] = "表情包二十三"
{
  [1] = "默认表情",
  [2] = "表情包二",
  [3] = "名称五汉字",
  [4] = "六十一个表情测试",
  [5] = "六十个表情测试",
  [6] = "",
  [7] = "",
  [8] = "",
  [9] = "",
  [10] = "",
  [11] = "",
  [12] = "",
  [13] = "",
  [14] = "",
  [15] = "",
  [16] = "",
  [17] = "",
  [18] = "",
  [18] = "",
  [19] = "",
  [20] = ""
}[24] = "表情包二十四"
local {
  [1] = "默认表情",
  [2] = "表情包二",
  [3] = "名称五汉字",
  [4] = "六十一个表情测试",
  [5] = "六十个表情测试",
  [6] = "",
  [7] = "",
  [8] = "",
  [9] = "",
  [10] = "",
  [11] = "",
  [12] = "",
  [13] = "",
  [14] = "",
  [15] = "",
  [16] = "",
  [17] = "",
  [18] = "",
  [18] = "",
  [19] = "",
  [20] = ""
}[25], g_strTileDes = "", {
  [1] = "默认表情",
  [2] = "表情包二",
  [3] = "名称五汉字",
  [4] = "六十一个表情测试",
  [5] = "六十个表情测试",
  [6] = "",
  [7] = "",
  [8] = "",
  [9] = "",
  [10] = "",
  [11] = "",
  [12] = "",
  [13] = "",
  [14] = "",
  [15] = "",
  [16] = "",
  [17] = "",
  [18] = "",
  [18] = "",
  [19] = "",
  [20] = ""
}
local g_CurChatEditBox
function _CFace_UpdateButtonState()
  local tablectrl = WindowSys_Instance:GetWindow("ChatFace.CommonFace_tctl")
  local tablectrl = WindowToTableCtrl(tablectrl)
  local leftcnt = tonumber(tablectrl:GetProperty("BeginShowIndex"))
  local btnleft = WindowSys_Instance:GetWindow("ChatFace.Page_bg.Left_btn")
  local btnright = WindowSys_Instance:GetWindow("ChatFace.Page_bg.Right_btn")
  local rightcnt = tablectrl:GetHideItemCount()
  if leftcnt + rightcnt > 0 then
    btnleft:SetProperty("Visible", "true")
    btnright:SetProperty("Visible", "true")
    if leftcnt <= 0 then
      btnleft:SetProperty("Enable", "false")
    else
      btnleft:SetProperty("Enable", "true")
    end
    if rightcnt <= 0 then
      btnright:SetProperty("Enable", "false")
    else
      btnright:SetProperty("Enable", "true")
    end
  else
    btnleft:SetProperty("Visible", "false")
    btnright:SetProperty("Visible", "false")
  end
end
function _CFace_ShowPage(pagenum)
  local bActive = false
  local nTime = 1
  local count = 0
  local nMaxPage = 0
  local nSingleActiveCnt = 0
  local wnd
  local wndtime = WindowSys_Instance:GetWindow("ChatFace.Time_dlab")
  bActive, nTime, count, nMaxPage, nSingleActiveCnt = UpdateChatFace(pagenum)
  local tablectrl = WindowSys_Instance:GetWindow("ChatFace.CommonFace_tctl")
  local tablectrl = WindowToTableCtrl(tablectrl)
  local tabcount = tablectrl:GetItemCount()
  for i = tabcount, nMaxPage do
    tablectrl:InsertItem(g_strTableTile[i + 1], i)
  end
  tabcount = tablectrl:GetItemCount()
  if tabcount < nMaxPage + 2 then
    tablectrl:InsertItem(g_strTableTile[25], nMaxPage + 1)
  end
  for i = 0, nMaxPage do
    tablectrl:SetItemToolTipText(i, g_strTileDes[i + 1])
  end
  tablectrl:SetItemToolTipText(nMaxPage + 1, g_strTileDes[25])
  if nTime == 1 or nTime == 2 then
    wndtime:SetProperty("Text", g_strTimeDes[nTime])
  else
    wndtime:SetProperty("Text", os.date("%c", nTime))
  end
  count = count + 1
  local parent = WindowSys_Instance:GetWindow("ChatFace.AllFace_bg")
  for i = count, 60 do
    wnd = parent:GetChildByName("Face" .. i .. "_btn")
    wnd:SetProperty("Visible", "false")
  end
  for i = 1, 60 do
    wnd = parent:GetChildByName("Face" .. i .. "_btn")
    wnd:SetProperty("HasTooltip", "true")
  end
  _CFace_UpdateButtonState()
end
function _CFace_FormatAbsTiime(time)
  local str = ""
  local days = math.floor(time / 86400)
  time = time % 86400
  local hours = math.floor(time / 3600)
  time = time % 3600
  local mins = math.floor(time / 60)
  if days > 0 then
    str = string.format("%d天%d时%d分", days, hours, mins)
  elseif hours > 0 then
    str = string.format("%d时%d分", hours, mins)
  elseif mins > 0 then
    str = string.format("%d分", mins)
  else
    str = string.format("%d秒", time)
  end
  return str
end
function _CFace_SetCurChatEditBox(editBox)
  g_CurChatEditBox = editBox
end
function _CFace_GetCurChatEditBox()
  if g_CurChatEditBox then
    local type = g_CurChatEditBox:GetProperty("ClassName")
    if type ~= "EditBox" then
      return WindowToEditBox(editBox)
    end
  end
  return g_CurChatEditBox
end
function UI_CFace_RightPageClick(msg)
  local tablectrl = WindowSys_Instance:GetWindow("ChatFace.CommonFace_tctl")
  local tablectrl = WindowToTableCtrl(tablectrl)
  local n = tablectrl:GetProperty("BeginShowIndex")
  tablectrl:SetProperty("BeginShowIndex", tostring(tonumber(n) + 1))
  _CFace_UpdateButtonState()
end
function UI_CFace_LeftPageClick(msg)
  local tablectrl = WindowSys_Instance:GetWindow("ChatFace.CommonFace_tctl")
  local tablectrl = WindowToTableCtrl(tablectrl)
  local pageindex = tonumber(tablectrl:GetProperty("BeginShowIndex"))
  if pageindex > 0 then
    tablectrl:SetProperty("BeginShowIndex", tostring(pageindex - 1))
  end
  _CFace_UpdateButtonState()
end
function UI_CFace_ItemChanged(msg)
  local tablectrl = WindowToTableCtrl(msg:get_window())
  local selected = tablectrl:GetSelectItem()
  _CFace_ShowPage(selected)
end
function UI_CFace_FaceMouseClick(msg)
  local wnd = msg:get_window()
  local faceIndex = wnd:GetCustomUserData(0)
  local editChatBox = _CFace_GetCurChatEditBox()
  local parent = WindowSys_Instance:GetWindow("ChatFace")
  parent:SetProperty("Visible", "false")
  if editChatBox ~= nil then
    local edtText = editChatBox:GetProperty("Text")
    local tempStr = GetParsedStr(edtText)
    local textlen = string.len(tempStr)
    if textlen >= gUI_SENDMSG_CHANNEL_MAX_WORD[_Chat_GetChatType()] then
      Lua_Chat_ShowNotity(0, "CHAT_TOO_MANY_WORD", gUI_CHAT_CHANNEL_SETTINGINFO[_Chat_GetChatType()].CHANNEL_NAME, tostring(gUI_SENDMSG_CHANNEL_MAX_WORD[_Chat_GetChatType()] / 2))
      return
    end
    edtText = edtText .. "{##^" .. tostring(faceIndex) .. "}"
    local count = select(2, string.gsub(edtText, "%{##%^%d%d%d%}", "{##%^%d%d%d%}"))
    if count > 6 then
      Lua_Chat_ShowNotity(0, "CHAT_FACE_LIMITED")
      return
    end
    editChatBox:SetProperty("Text", edtText)
    WindowSys_Instance:SetKeyboardCaptureWindow(editChatBox)
  end
end
function UI_CFace_FaceTooltip(msg)
  local wnd = msg:get_window()
  local faceIndex = wnd:GetCustomUserData(0)
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  local des = ""
  if CHAT_FACE_TIP_DES[faceIndex] ~= nil then
    des = CHAT_FACE_TIP_DES[faceIndex]
  end
  Lua_Tip_ChatFace(tooltip, des .. "{##^" .. tostring(faceIndex) .. "}")
end
function Lua_CFace_CloseFaceWnd()
  local parent = WindowSys_Instance:GetWindow("ChatFace")
  parent:SetProperty("Visible", "false")
end
function _OnCFace_AddMotion(arg1, arg2)
  local wnd = WindowSys_Instance:GetWindow("ChatFace.AllFace_bg.Face" .. arg1 + 1 .. "_btn")
  wnd:SetProperty("AnimateId", tostring(arg2))
  wnd:SetCustomUserData(0, arg2)
  wnd:SetProperty("Visible", "true")
end
function _OnCFace_ActiveMotion(arg1, arg2, arg3, arg4)
  if arg1 then
    if arg3 ~= -1 then
      ShowErrorMessage("激活" .. arg2 + 1 .. "表情成功!")
    else
      ShowErrorMessage("激活表情页[" .. g_strTileDes[arg2 + 1] .. "]成功!")
    end
  else
    local days = math.floor(arg4 / 86400)
    ShowErrorMessage("延长表情页" .. g_strTileDes[arg2 + 1] .. days .. "天!")
  end
end
function Script_CFace_OnLoad()
end
function Script_CFace_OnEvent(event)
  if event == "CHAT_FACE_ADD_MOTION" then
    _OnCFace_AddMotion(arg1, arg2)
  elseif event == "CHAT_FACE_ACTIVE_MOTION" then
    _OnCFace_ActiveMotion(arg1, arg2, arg3, arg4)
  end
end
