if gUI and not gUI.Rank then
  gUI.Rank = {}
end
local RANK_KEY_NONE = 0
local RANK_KEY_LEVEL = 1
local RANK_KEY_WEAPON = 2
local RANK_KEY_EQUIP = 3
local RANK_KEY_PET = 4
local RANK_KEY_GUILD_LEVEL = 5
local RANK_KEY_BEWRATH = 6
local RANK_KEY_ONLINE_TIME = 7
local RANK_KEY_GUILD_WARWIN = 8
local RANK_KEY_GUILD_SIEGE = 9
local RANK_KEY_KEJU_XIANGSHI = 10
local RANK_KEY_KEJU_HUISHI = 11
local RANK_KEY_FISTPAST_INST = 12
local RANK_KEY_BATTLE_WIN = 14
local RankIndex_ToReal = {
  [1] = RANK_KEY_LEVEL,
  [2] = RANK_KEY_WEAPON,
  [3] = RANK_KEY_EQUIP,
  [4] = RANK_KEY_GUILD_LEVEL,
  [5] = RANK_KEY_BATTLE_WIN
}
function _Rank_GetKey()
  return RankIndex_ToReal[gUI.Rank.TableCtrl:GetSelectItem() + 1]
end
function _Rank_GetList(key)
  local list
  if key == gUI_RANK.Level then
    list = gUI.Rank.LevelList
  elseif key == gUI_RANK.Weapon then
    list = gUI.Rank.WeaponList
  elseif key == gUI_RANK.Equip then
    list = gUI.Rank.SwordList
  elseif key == gUI_RANK.Pet then
    list = gUI.Rank.PetList
  elseif key == gUI_RANK.Guild then
    list = gUI.Rank.GuildList
  elseif key == gUI_RANK.Wrath then
    list = gUI.Rank.WrathList
  elseif key == gUI_RANK.BGRward then
    list = gUI.Rank.BGRewardList
  end
  return list
end
function _Rank_GetPanel(key)
  local panel
  if key == gUI_RANK.Level then
    panel = gUI.Rank.Level
  elseif key == gUI_RANK.Weapon then
    panel = gUI.Rank.Weapon
  elseif key == gUI_RANK.Equip then
    panel = gUI.Rank.Sword
  elseif key == gUI_RANK.Pet then
    panel = gUI.Rank.Pet
  elseif key == gUI_RANK.Guild then
    panel = gUI.Rank.Guild
  elseif key == gUI_RANK.Wrath then
    panel = gUI.Rank.Wrath
  elseif key == gUI_RANK.BGRward then
    panel = gUI.Rank.BGReward
  end
  return panel
end
function _Rank_HideAllPanel()
  for i = 0, #gUI.Rank.Whole do
    gUI.Rank.Whole[i]:SetVisible(false)
  end
end
function _Rank_GetCurPage()
  local pageStr = gUI.Rank.Page:GetProperty("Text")
  local i, j = string.find(pageStr, "/")
  if i ~= nil then
    local num = string.sub(pageStr, 1, i - 1)
    return tonumber(num)
  end
end
function _Rank_GetMaxPage()
  local pageStr = gUI.Rank.Page:GetProperty("Text")
  local i, j = string.find(pageStr, "/")
  if i ~= nil then
    local num = string.sub(pageStr, i + 1, -1)
    return tonumber(num)
  end
end
function _OnRank_ShowTip(_key)
  if _key == gUI_RANK.Weapon then
    local wnd = WindowSys_Instance:GetWindow("TooltipWindows.Tooltip1")
    Lua_Tip_Item(wnd, _key, gUI_GOODSBOX_BAG.RankItem, nil, -1, nil)
    _OnLinkE_ChatShowTipPos(wnd)
  end
end
function UI_Rank_GotoNPC()
  local key = _Rank_GetKey()
  if key == gUI_RANK.Weapon then
    SetRankCheckIn(0)
  elseif key == gUI_RANK.Equip then
    SetRankCheckIn(1)
  end
end
function UI_Rank_RClickList()
  local key = _Rank_GetKey()
  local list = _Rank_GetList(key)
  local selectId = list:GetSelectedItemIndex()
  if selectId == -1 then
    return
  end
  if key == gUI_RANK.Level then
    local guid = list:GetItemData64(selectId)
    local item1 = list:GetItem(selectId)
    local name = item1:GetColumnText(1)
    local lev = item1:GetColumnText(4)
    local myName = GetPlaySelfProp(6)
    name = GetParsedStr(name)
    lev = GetParsedStr(lev)
    if guid ~= 0 and myName ~= name then
      Lua_MenuShow("ON_RANKINFO", name, guid, tonumber(lev))
    end
  elseif key == gUI_RANK.Weapon then
    local guid = list:GetItemData64(selectId)
    local item1 = list:GetItem(selectId)
    local name = item1:GetColumnText(1)
    local lev = 30
    local myName = GetPlaySelfProp(6)
    name = GetParsedStr(name)
    lev = GetParsedStr(lev)
    if guid ~= 0 and myName ~= name then
      Lua_MenuShow("ON_RANKINFO", name, guid, tonumber(lev))
    end
  elseif key == gUI_RANK.Equip then
    local guid = list:GetItemData64(selectId)
    local item1 = list:GetItem(selectId)
    local name = item1:GetColumnText(1)
    local lev = 30
    local myName = GetPlaySelfProp(6)
    name = GetParsedStr(name)
    lev = GetParsedStr(lev)
    if guid ~= 0 and myName ~= name then
      Lua_MenuShow("ON_RANKINFO", name, guid, tonumber(lev))
    end
  elseif key == gUI_RANK.Pet then
  elseif key == gUI_RANK.Guild then
    local guid = list:GetItemData64(selectId)
    local name = GetGuildRankInfo(guid)
    local item = list:GetItem(selectId)
    local GuildHostName = item:GetColumnText(2)
    GuildHostName = GetParsedStr(GuildHostName)
    if name ~= "" and name ~= nil then
      Lua_MenuShow("ON_GUILD", guid, name, GuildHostName)
    else
    end
  elseif key == gUI_RANK.Wrath then
    local guid = list:GetItemData64(selectId)
    local item1 = list:GetItem(selectId)
    local name = item1:GetColumnText(1)
    name = GetParsedStr(name)
    local lev = gUI_PK_Level
    local myName = GetPlaySelfProp(6)
    if guid ~= 0 and myName ~= name then
      Lua_MenuShow("ON_RANKINFO", name, guid, tonumber(lev))
    end
  end
end
function UI_Rank_LClickList()
  local key = _Rank_GetKey()
  local list = _Rank_GetList(key)
  local selectId = list:GetSelectedItemIndex()
  if selectId == -1 then
    return
  end
  if key == gUI_RANK.Level then
  elseif key == gUI_RANK.Weapon then
    local guid = list:GetItemData64(selectId)
    RequestRankPlayerDetail(guid, key)
  elseif key == gUI_RANK.Equip then
    local guid = list:GetItemData64(selectId)
    RequestRankPlayerDetail(guid, key)
  elseif key == gUI_RANK.Pet then
    local guid = list:GetItemData64(selectId)
    RequestRankPlayerDetail(guid, key)
  elseif key == gUI_RANK.Guild then
  end
end
function UI_Rank_MainCtrlClick()
  local key = _Rank_GetKey()
  if key then
    _Rank_ClearUI(key)
    RequestRankInfo(key)
  end
end
function UI_Rank_PrevBtn()
  local maxPage = _Rank_GetMaxPage()
  local pageNum = _Rank_GetCurPage()
  if pageNum <= 1 then
    return
  end
  local key = _Rank_GetKey()
  _OnRank_UpdateRank(key, pageNum - 1)
end
function UI_Rank_NextBtn()
  local maxPage = _Rank_GetMaxPage()
  local pageNum = _Rank_GetCurPage()
  if maxPage <= pageNum then
    return
  end
  local key = _Rank_GetKey()
  _OnRank_UpdateRank(key, pageNum + 1)
end
function _OnRank_UpdateRank(key, pageNum)
  local list = _Rank_GetList(key)
  if not list then
    return
  end
  local curKey = _Rank_GetKey()
  if curKey ~= key then
    return
  end
  local count = GetRankCount(key)
  list:RemoveAllItems()
  if pageNum == 1 then
    gUI.Rank.Back1st:SetVisible(true)
    gUI.Rank.Back2st:SetVisible(true)
    gUI.Rank.Back3st:SetVisible(true)
    gUI.Rank.Back1stNormal:SetVisible(false)
    gUI.Rank.Back2stNormal:SetVisible(false)
    gUI.Rank.Back3stNormal:SetVisible(false)
  else
    gUI.Rank.Back1st:SetVisible(false)
    gUI.Rank.Back2st:SetVisible(false)
    gUI.Rank.Back3st:SetVisible(false)
    gUI.Rank.Back1stNormal:SetVisible(true)
    gUI.Rank.Back2stNormal:SetVisible(true)
    gUI.Rank.Back3stNormal:SetVisible(true)
  end
  for i = (pageNum - 1) * 10 + 1, (pageNum - 1) * 10 + 10 do
    local userId, name, param2, param3, param4, param5 = GetPersonalRankRecord(key, i)
    local param2Str = ""
    if key ~= gUI_RANK.Wrath then
      param2Str = gUI_MenPaiName[param2]
    end
    if key == gUI_RANK.Pet then
      param2Str = param2
    end
    if userId == nil then
      if i <= 3 then
        if key == gUI_RANK.Guild then
          str = string.format("%s|%s|%s|%s|%s|%s", " ", "--", "--", "--", "--", "--")
        elseif key == gUI_RANK.Wrath then
          str = string.format("%s|%s|%s|%s", " ", "--", "--", "--")
        elseif key == gUI_RANK.BGRward then
          str = string.format("%s|%s|%s|%s|%s|%s", " ", "--", "--", "--", "--", "--")
        else
          str = string.format("%s|%s|%s|%s|%s", " ", "--", "--", "--", "--")
        end
        local item = list:InsertString(str, -1)
        item:SetUserData64(0)
      end
    else
      local str
      if key == gUI_RANK.Guild then
        if i <= 3 then
          str = string.format("%s|%s|%s|%d|%d|%d", " ", name, param2, param5, param4, param3)
        else
          str = string.format("%d|%s|%s|%d|%d|%d", i, name, param2, param5, param4, param3)
        end
      elseif key == gUI_RANK.Wrath then
        if param2 == "" then
          param2 = "无"
        end
        if i <= 3 then
          str = string.format("%s|%s|%s|%d", " ", name, tostring(param2), param3)
        else
          str = string.format("%d|%s|%s|%d", i, name, tostring(param2), param3)
        end
      elseif key == gUI_RANK.BGRward then
        if param4 == "" then
          param4 = "无"
        end
        if i <= 3 then
          str = string.format("%s|%s|%s|%s|%d|%d", " ", name, param2Str, tostring(param4), param3, param5)
        else
          str = string.format("%d|%s|%s|%s|%d|%d", i, name, param2Str, tostring(param4), param3, param5)
        end
      else
        if param4 == "" then
          param4 = "无"
        end
        if i <= 3 then
          str = string.format("%s|%s|%s|%s|%d", " ", name, param2Str, tostring(param4), param3)
        else
          str = string.format("%d|%s|%s|%s|%d", i, name, param2Str, tostring(param4), param3)
        end
      end
      local item = list:InsertString(str, -1)
      item:SetUserData64(userId)
      if key == gUI_RANK.Guild then
        local guildName = GetGuildName()
        if guildName == name then
          if i <= 3 then
            str = string.format("%s|{#G^%s}|{#G^%s}|{#G^%d}|{#G^%d}|{#G^%d}", " ", name, param2, param5, param4, param3)
          else
            str = string.format("{#G^%d}|{#G^%s}|{#G^%s}|{#G^%d}|{#G^%d}|{#G^%d}", i, name, param2, param5, param4, param3)
          end
          item:SetText(str)
        end
      elseif key == gUI_RANK.Wrath then
        local myName = GetMyPlayerStaticInfo()
        if myName == name then
          if param2 == "" then
            param2 = "无"
          end
          if i <= 3 then
            str = string.format("%s|{#G^%s}|{#G^%s}|{#G^%d}", " ", name, tostring(param2), param3)
          else
            str = string.format("{#G^%d}|{{#G^%s}|{#G^%s}|{#G^%d}", i, name, tostring(param2), param3)
          end
          item:SetText(str)
        end
      elseif key == gUI_RANK.BGRward then
        local myName = GetMyPlayerStaticInfo()
        if myName == name then
          if param4 == "" then
            param4 = "无"
          end
          if i <= 3 then
            str = string.format("%s|{#G^%s}|{#G^%s}|{#G^%s}|{#G^%d}|{#G^%d}", " ", name, param2Str, tostring(param4), param3, param5)
          else
            str = string.format("{#G^%d}|{#G^%s}|{#G^%s}|{#G^%s}|{#G^%d}|{#G^%d}", i, name, param2Str, tostring(param4), param3, param5)
          end
          item:SetText(str)
        end
      else
        local myName = GetMyPlayerStaticInfo()
        if myName == name then
          if param4 == "" then
            param4 = "无"
          end
          if i <= 3 then
            str = string.format("%s|{#G^%s}|{#G^%s}|{#G^%s}|{#G^%d}", " ", name, param2Str, tostring(param4), param3)
          else
            str = string.format("{#G^%d}|{#G^%s}|{#G^%s}|{#G^%s}|{#G^%d}", i, name, param2Str, tostring(param4), param3)
          end
          item:SetText(str)
        end
      end
    end
  end
  local maxPageNum = math.ceil(count / 10)
  if maxPageNum == 0 then
    maxPageNum = 1
  end
  gUI.Rank.Page:SetProperty("Text", tostring(pageNum) .. "/" .. tostring(maxPageNum))
  gUI.Rank.PrevBtn:SetEnable(true)
  gUI.Rank.NextBtn:SetEnable(true)
  if pageNum >= maxPageNum then
    gUI.Rank.NextBtn:SetEnable(false)
  end
  if pageNum <= 1 then
    gUI.Rank.PrevBtn:SetEnable(false)
  end
  if key == gUI_RANK.Weapon then
    gUI.Rank.GotoBtn:SetVisible(true)
    gUI.Rank.GotoBtn:SetProperty("Text", "登记武器")
  elseif key == gUI_RANK.Equip then
    gUI.Rank.GotoBtn:SetVisible(true)
    gUI.Rank.GotoBtn:SetProperty("Text", "登记装备")
  else
    gUI.Rank.GotoBtn:SetVisible(false)
  end
  local rankIndex = GetSelfRank(key)
  local rankStr
  if key == gUI_RANK.Guild then
    local guildID, _, _, level = GetGuildBaseInfo()
    if level < 1 then
      rankStr = string.format("你没有加入帮会。")
      gUI.Rank.SelfRankLab:SetProperty("Text", tostring(rankStr))
      return
    end
    if rankIndex >= 0 and rankIndex <= 10000 then
      rankStr = string.format("你的帮会上周活跃值排名为第%d名！", rankIndex)
    else
      rankStr = string.format("你的帮会上周活跃值排名未能上榜，请继续努力。")
    end
    gUI.Rank.SelfRankLab:SetProperty("Text", tostring(rankStr))
    return
  end
  if key == gUI_RANK.Wrath then
    if rankIndex >= 0 and rankIndex <= 100 then
      rankStr = string.format("每周一凌晨4点将清空上一周的天诛排行榜，您当前排名为第%d名！", rankIndex)
    else
      rankStr = string.format("每周一凌晨4点将清空上一周的天诛排行榜，您当前未上榜。")
    end
    gUI.Rank.SelfRankLab:SetProperty("Text", tostring(rankStr))
    return
  end
  if rankIndex ~= -1 or rankIndex ~= 65535 then
    if rankIndex == 1 then
      rankStr = "恭喜荣登榜首！笑傲群雄！"
    elseif rankIndex == 2 then
      rankStr = "恭喜荣登榜眼！"
    elseif rankIndex == 3 then
      rankStr = "恭喜晋级三甲！"
    elseif rankIndex >= 4 and rankIndex <= 10 then
      rankStr = string.format("你当前排名为第%d名，非常不错！", rankIndex)
    elseif rankIndex >= 11 and rankIndex <= 100 then
      rankStr = string.format("你当前排名为%d名，再接再厉！", rankIndex)
    elseif rankIndex >= 101 and rankIndex <= 200 then
      rankStr = string.format("你当前排名为%d名，差一点就上榜了，加油哦！", rankIndex)
    elseif rankIndex >= 201 and rankIndex <= 10000 then
      rankStr = string.format("你当前排名为%d名，没有进入排行榜，继续努力！", rankIndex)
    else
      rankStr = string.format("你没有进入当前排名，请继续努力。")
    end
  else
    rankStr = string.format("你没有进入当前排名，请继续努力。")
  end
  gUI.Rank.SelfRankLab:SetProperty("Text", tostring(rankStr))
end
function _Rank_ClearUI(key)
  gUI.Rank.PrevBtn:SetEnable(false)
  gUI.Rank.NextBtn:SetEnable(false)
  gUI.Rank.Page:SetProperty("Text", "1/1")
  gUI.Rank.Back1st:SetVisible(true)
  gUI.Rank.Back2st:SetVisible(true)
  gUI.Rank.Back3st:SetVisible(true)
  gUI.Rank.Back1stNormal:SetVisible(false)
  gUI.Rank.Back2stNormal:SetVisible(false)
  gUI.Rank.Back3stNormal:SetVisible(false)
  gUI.Rank.SelfRankLab:SetProperty("Text", "")
  local list = _Rank_GetList(key)
  list:RemoveAllItems()
  if not list then
    return
  end
  local str
  for i = 1, 3 do
    if key == gUI_RANK.Guild then
      str = string.format("%s|%s|%s|%s", " ", "--", "--", "--")
    elseif key == gUI_RANK.Guild then
      str = string.format("%s|%s|%s|%s", " ", "--", "--", "--")
    else
      str = string.format("%s|%s|%s|%s|%s", " ", "--", "--", "--", "--")
    end
    list:InsertString(str, -1)
  end
  local panel = _Rank_GetPanel(key)
  _Rank_HideAllPanel()
  if panel then
    panel:SetVisible(true)
  end
end
function UI_Rank_ShowMain()
  if not gUI.Rank.Root:IsVisible() then
    gUI.Rank.TableCtrl:SetSelectedState(0)
    _Rank_ClearUI(gUI_RANK.Level)
    RequestRankInfo(gUI_RANK.Level)
    gUI.Rank.Root:SetVisible(true)
  else
    UI_Rank_CloseMain()
  end
end
function UI_Rank_CloseMain()
  gUI.Rank.Root:SetVisible(false)
end
function Script_Rank_OnLoad()
  gUI.Rank.Root = WindowSys_Instance:GetWindow("Ranking")
  gUI.Rank.TableCtrl = WindowToTableCtrl(WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.Change_tctl"))
  gUI.Rank.Whole = {}
  gUI.Rank.Level = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.LevelImage_bg")
  gUI.Rank.Weapon = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.WeaponImage_bg")
  gUI.Rank.Sword = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.SwordImage_bg")
  gUI.Rank.Pet = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.PetImage_bg")
  gUI.Rank.Guild = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.GuildImage_bg")
  gUI.Rank.Wrath = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.WrathImage_bg")
  gUI.Rank.BGReward = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.Battlefield_bg")
  gUI.Rank.Whole[0] = gUI.Rank.Level
  gUI.Rank.Whole[1] = gUI.Rank.Weapon
  gUI.Rank.Whole[2] = gUI.Rank.Sword
  gUI.Rank.Whole[3] = gUI.Rank.Pet
  gUI.Rank.Whole[4] = gUI.Rank.Guild
  gUI.Rank.Whole[5] = gUI.Rank.Wrath
  gUI.Rank.Whole[6] = gUI.Rank.BGReward
  gUI.Rank.LevelList = WindowToListBox(WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.LevelImage_bg.LevelBox_lbox"))
  gUI.Rank.WeaponList = WindowToListBox(WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.WeaponImage_bg.WeaponBox_lbox"))
  gUI.Rank.SwordList = WindowToListBox(WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.SwordImage_bg.SwordBox_lbox"))
  gUI.Rank.PetList = WindowToListBox(WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.PetImage_bg.PetBox_lbox"))
  gUI.Rank.GuildList = WindowToListBox(WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.GuildImage_bg.GuildBox_lbox"))
  gUI.Rank.WrathList = WindowToListBox(WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.WrathImage_bg.Wrath_lbox"))
  gUI.Rank.BGRewardList = WindowToListBox(WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.Battlefield_bg.Battle_lbox"))
  gUI.Rank.Page = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.PageImage_bg.PageLab_dlab")
  gUI.Rank.PrevBtn = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.Last_btn")
  gUI.Rank.NextBtn = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.Next_btn")
  gUI.Rank.Back1st = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.RankImage_bg.NumberImage_bg.Numberpic11_bg")
  gUI.Rank.Back2st = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.RankImage_bg.NumberImage_bg.Numberpic12_bg")
  gUI.Rank.Back3st = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.RankImage_bg.NumberImage_bg.Numberpic13_bg")
  gUI.Rank.Back1stNormal = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.RankImage_bg.NumberImage_bg.Numberpic1_bg")
  gUI.Rank.Back2stNormal = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.RankImage_bg.NumberImage_bg.Numberpic2_bg")
  gUI.Rank.Back3stNormal = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.RankImage_bg.NumberImage_bg.Numberpic3_bg")
  gUI.Rank.SelfRankLab = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.RankImage_bg.NumberImage_bg.ChannelLab_dlab")
  gUI.Rank.GotoBtn = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.GotoNpc_btn")
end
function Script_Rank_OnEvent(event)
  if event == "RANK_INFO_UPDATE" then
    _OnRank_UpdateRank(arg1, 1)
  elseif event == "ITEM_TOOLTIP_RANK" then
    _OnRank_ShowTip(arg1)
  elseif event == "RANK_SHOW" then
    UI_Rank_ShowMain()
  end
end
function Script_Rank_OnHide()
  Lua_MenuHide("ON_RANKINFO")
end
Ranking.RankingImagePic_bg.Last_btn")
  gUI.Rank.NextBtn = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.Next_btn")
  gUI.Rank.Back1st = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.RankImage_bg.NumberImage_bg.Numberpic11_bg")
  gUI.Rank.Back2st = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.RankImage_bg.NumberImage_bg.Numberpic12_bg")
  gUI.Rank.Back3st = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.RankImage_bg.NumberImage_bg.Numberpic13_bg")
  gUI.Rank.Back1stNormal = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.RankImage_bg.NumberImage_bg.Numberpic1_bg")
  gUI.Rank.Back2stNormal = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.RankImage_bg.NumberImage_bg.Numberpic2_bg")
  gUI.Rank.Back3stNormal = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.RankImage_bg.NumberImage_bg.Numberpic3_bg")
  gUI.Rank.SelfRankLab = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.RankImage_bg.NumberImage_bg.ChannelLab_dlab")
  gUI.Rank.GotoBtn = WindowSys_Instance:GetWindow("Ranking.RankingImagePic_bg.GotoNpc_btn")
end
function Script_Rank_OnEvent(event)
  if event == "RANK_INFO_UPDATE" then
    _OnRank_UpdateRank(arg1, 1)
  elseif event == "ITEM_TOOLTIP_RANK" then
    _OnRank_ShowTip(arg1)
  elseif event == "RANK_SHOW" then
    UI_Rank_ShowMain()
  end
end
function Script_Rank_OnHide()
  Lua_MenuHide("ON_RANKINFO")
end
