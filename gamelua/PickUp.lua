if gUI and not gUI.Pick then
  gUI.Pick = {}
end
function _Pick_InitWndPos()
  local wndWidth = gUI.Pick.Loot_Dialog_Wnd[1]:GetWidth()
  local wndHeight = gUI.Pick.Loot_Dialog_Wnd[1]:GetHeight()
  local x = 0.5
  local xOffset = x - wndWidth / 2
  local y = 0.58
  gUI.Pick.Loot_Dialog_Wnd[1]:SetLeft(xOffset)
  gUI.Pick.Loot_Dialog_Wnd[1]:SetTop(y)
  gUI.Pick.Loot_Dialog_Wnd[1]:SetVisible(false)
  y = y + wndHeight
  gUI.Pick.Loot_Dialog_Wnd[2]:SetLeft(xOffset)
  gUI.Pick.Loot_Dialog_Wnd[2]:SetTop(y)
  gUI.Pick.Loot_Dialog_Wnd[2]:SetVisible(false)
  y = y + wndHeight
  gUI.Pick.Loot_Dialog_Wnd[3]:SetLeft(xOffset)
  gUI.Pick.Loot_Dialog_Wnd[3]:SetTop(y)
  gUI.Pick.Loot_Dialog_Wnd[3]:SetVisible(false)
end
function _Pick_GetRollItemIndexById(index, boxId)
  for i, item in pairs(gUI.Pick.Loot_Dialog) do
    if item.index == index and item.boxID == boxId then
      return i
    end
  end
  return nil
end
function _Pick_GetFreeWnd()
  for ii = 1, 3 do
    if not gUI.Pick.Loot_Dialog_Wnd[ii]:IsVisible() then
      gUI.Pick.Loot_Dialog_Wnd[ii]:SetVisible(true)
      return gUI.Pick.Loot_Dialog_Wnd[ii]
    end
  end
  return nil
end
local ITEM_DISCARD = 1
local ITEM_EXCHANGE_TOOTHER = 2
local ITEM_SELL_TO_SOLDLIST = 3
local ITEM_LOSE = 4
local ITEM_CONSUME = 5
local ITEM_USE = 6
local I_CAN_PLAY_ANIMATION = {
  [1] = 10,
  [2] = 11,
  [3] = 12,
  [4] = 14,
  [5] = 31,
  [6] = 131
}
function Lua_Pick_OnGetNewItem(serialId, guid, num, iconName, sourId, tableId, quality)
  local strMsg
  if num > 1 then
    strMsg = string.format("{#C^0xFFFFFB00^你获得了}{ItEm^%d^%s^%d^%d}{#C^0xFFFFFB00^%d个}", serialId, guid, tableId, quality, num)
  else
    strMsg = string.format("{#C^0xFFFFFB00^你获得了}{ItEm^%d^%s^%d^%d}", serialId, guid, tableId, quality)
  end
  Lua_Chat_AddSysLog(strMsg)
  if PickUp_Check_PlayAnimation(sourId) == true then
    Lua_FlyIc_AddIcon(iconName)
  end
  WorldStage:playSoundByID(21)
  if tableId == 345000 and Lua_Pet_GetCurrentPetIndex() ~= -1 then
    TriggerUserGuide(29, -1, -1)
  end
end
function PickUp_Check_PlayAnimation(sourId)
  for i = 1, table.maxn(I_CAN_PLAY_ANIMATION) do
    if sourId == I_CAN_PLAY_ANIMATION[i] then
      return true
    end
  end
  return false
end
function Lua_Pick_OnRemoveItem(serialId, guid, nNum, iconName, nType, tableId, quality)
  local strMsg = "你%s了"
  if nType == ITEM_DISCARD then
    strMsg = string.format(strMsg, "丢弃")
  elseif nType == ITEM_EXCHANGE_TOOTHER then
    strMsg = string.format(strMsg, "交易")
  elseif nType == ITEM_SELL_TO_SOLDLIST then
    strMsg = string.format(strMsg, "售出")
  elseif nType == ITEM_LOSE then
    strMsg = string.format(strMsg, "失去")
  elseif nType == ITEM_CONSUME then
    strMsg = string.format(strMsg, "消耗")
  elseif nType == ITEM_USE then
    strMsg = string.format(strMsg, "使用")
  else
    return
  end
  if nNum > 1 then
    strMsg = string.format("{#C^0xFFFFFB00^%s}{ItEm^%d^%s^%d^%d}{#C^0xFFFFFB00^%d个}", strMsg, serialId, guid, tableId, quality, nNum)
  else
    strMsg = string.format("{#C^0xFFFFFB00^%s}{ItEm^%d^%s^%d^%d}", strMsg, serialId, guid, tableId, quality)
  end
  Lua_Chat_AddSysLog(strMsg)
end
function Lua_Pick_OnDropItem()
  WorldStage:playSoundByID(22)
end
function Lua_Pick_ThrowItemUpdateTime(Timer)
  local i = table.maxn(gUI.Pick.Loot_Dialog)
  while i > 0 do
    if gUI.Pick.Loot_Dialog[i] then
      gUI.Pick.Loot_Dialog[i].time = gUI.Pick.Loot_Dialog[i].time - 1
      if 0 >= gUI.Pick.Loot_Dialog[i].time then
        local wndParent = gUI.Pick.Loot_Dialog[i].wnd
        if wndParent then
          wndParent:SetProperty("Visible", "false")
        end
        table.remove(gUI.Pick.Loot_Dialog, i)
      else
        local wndParent = gUI.Pick.Loot_Dialog[i].wnd
        if wndParent then
          local str = string.format("剩余时间：%d秒", gUI.Pick.Loot_Dialog[i].time)
          local _wnd = wndParent:GetGrandChild("TimeLab1")
          _wnd:SetProperty("Text", str)
          _wnd = wndParent:GetGrandChild("ProgressBar1")
          _wnd:SetProperty("Progress", tostring(gUI.Pick.Loot_Dialog[i].time / gUI.Pick.Loot_Dialog[i].maxTime))
        else
          gUI.Pick.Loot_Dialog[i].wnd = _Pick_GetFreeWnd()
          if gUI.Pick.Loot_Dialog[i].wnd ~= nil then
            wndParent = gUI.Pick.Loot_Dialog[i].wnd
            local str = string.format("剩余时间：%d秒", gUI.Pick.Loot_Dialog[i].time)
            local _wnd = wndParent:GetGrandChild("TimeLab1")
            _wnd:SetProperty("Text", str)
            _wnd = wndParent:GetGrandChild("ProgressBar1")
            _wnd:SetProperty("Progress", "1")
            local num, id, quality, name, IdTable, class, icon = GetItemBaseInfoBySlot(35, gUI.Pick.Loot_Dialog[i].index, gUI.Pick.Loot_Dialog[i].boxID)
            if name == nil then
              name = gUI.Pick.Loot_Dialog[i].itemName
              quality = 0
              icon = ""
            end
            _wnd = wndParent:GetGrandChild("GoodsName1.Name")
            local color = gUI_GOODS_QUALITY_COLOR_PREFIX[quality]
            _wnd:SetProperty("Text", string.format("{#C^%s^%s}", color, name))
            _wnd = wndParent:GetGrandChild("GoodsBox1")
            _wnd = WindowToGoodsBox(_wnd)
            _wnd:SetItemGoods(0, icon, quality)
            _wnd:SetItemData(0, gUI.Pick.Loot_Dialog[i].index)
            _wnd:SetCustomUserData(0, gUI.Pick.Loot_Dialog[i].boxID)
            if 1 < gUI.Pick.Loot_Dialog[i].count then
              _wnd:SetItemSubscript(0, tostring(gUI.Pick.Loot_Dialog[i].count))
            end
          end
        end
      end
    end
    i = i - 1
  end
end
function Lua_Pick_ThrowItemAssignLootResult(box_id, item_id, target_guid)
  AssignItemByLeader(box_id, item_id, target_guid)
end
function Lua_Pick_GiveUpAll()
  for index, item in pairs(gUI.Pick.Loot_Dialog) do
    SendAgreeRoll(gUI.Pick.Loot_Dialog[index].itemID, gUI.Pick.Loot_Dialog[index].boxID, gUI.Pick.Loot_Dialog[index].index)
  end
  gUI.Pick.Loot_Dialog = {}
  for i = 1, 3 do
    gUI.Pick.Loot_Dialog_Wnd[i]:SetVisible(false)
  end
end
function Lua_Pick_InsertLootDialog(index, item)
  table.insert(gUI.Pick.Loot_Dialog, index, item)
end
function UI_Pick_PickOneItem(msg)
  local button = msg:get_window()
  local group_id = button:GetCustomUserData(0)
  local item_index = button:GetCustomUserData(1)
  PickUpItem(group_id, item_index)
end
function UI_Pick_ThrowItemShowTooltip(msg)
  local wnd = msg:get_window()
  local tooltip = wnd:GetToolTipWnd(0)
  wnd = WindowToGoodsBox(wnd)
  local index = _Pick_GetRollItemIndexById(wnd:GetItemData(0), wnd:GetCustomUserData(0))
  if not index then
    return
  end
  if wnd:GetItemIcon(0) == "" then
    return
  end
  Lua_Tip_Item(tooltip, gUI.Pick.Loot_Dialog[index].index, gUI_GOODSBOX_BAG.dropbox, nil, gUI.Pick.Loot_Dialog[index].boxID, nil, nil)
  Lua_Tip_ShowEquipCompare(msg:get_window(), gUI_GOODSBOX_BAG.dropbox, gUI.Pick.Loot_Dialog[index].index, gUI.Pick.Loot_Dialog[index].boxID)
end
function UI_Pick_ThrowBtnClick(msg)
  local wndParent = msg:get_window():GetParent()
  if wndParent then
    wnd = wndParent:GetGrandChild("GoodsBox1")
    wnd = WindowToGoodsBox(wnd)
    local index = _Pick_GetRollItemIndexById(wnd:GetItemData(0), wnd:GetCustomUserData(0))
    if index ~= nil then
      SendAgreeRoll(gUI.Pick.Loot_Dialog[index].itemID, gUI.Pick.Loot_Dialog[index].boxID, gUI.Pick.Loot_Dialog[index].index)
      table.remove(gUI.Pick.Loot_Dialog, index)
    end
    wndParent:SetProperty("Visible", "false")
  end
end
function UI_Pick_GiveupBtnClick(msg)
  local wndParent = msg:get_window():GetParent()
  if wndParent then
    wnd = wndParent:GetGrandChild("GoodsBox1")
    wnd = WindowToGoodsBox(wnd)
    local index = _Pick_GetRollItemIndexById(wnd:GetItemData(0), wnd:GetCustomUserData(0))
    if index ~= nil then
      SendGiveupRoll(gUI.Pick.Loot_Dialog[index].itemID, gUI.Pick.Loot_Dialog[index].boxID, gUI.Pick.Loot_Dialog[index].index)
      table.remove(gUI.Pick.Loot_Dialog, index)
    end
    wndParent:SetProperty("Visible", "false")
  end
end
function Lua_Pick_Init()
  local root = WindowSys_Instance:GetWindow("PickUp")
  if gUI.Pick.Loot_Dialog_Wnd == nil then
    local wndTemplate = WindowSys_Instance:GetWindow("Template.ThrowItem")
    gUI.Pick.Loot_Dialog_Wnd = {}
    for i = 1, 3 do
      gUI.Pick.Loot_Dialog_Wnd[i] = WindowSys_Instance:CreateWndFromTemplate(wndTemplate, WindowSys_Instance:GetRootWindow())
      gUI.Pick.Loot_Dialog_Wnd[i]:SetVisible(false)
    end
  else
    for i = 1, 3 do
      gUI.Pick.Loot_Dialog_Wnd[i]:SetVisible(false)
    end
  end
  gUI.Pick.Loot_Dialog = {}
  _Pick_InitWndPos()
end
