{
  GB_ItemMainBag = 1,
  GB_TaskMainBag = 2,
  GB_ShortcurMeun1 = 3,
  GB_ShortcurMeun2 = 4,
  GB_ShortcurMeun3 = 5,
  GB_WareHouse = 6,
  GB_GuildWHouse = 7,
  GB_SetMail = 8,
  GB_GetMail = 9,
  GB_Character = 10,
  GB_Task = 11,
  GB_Pet = 12,
  GB_PetSkill = 13,
  GB_Mount = 14,
  GB_MountSkill = 15,
  GB_Stall = 16,
  GB_GetTask = 17,
  GB_NpcShop = 18,
  GB_Forge = 20,
  GB_Blend = 21,
  GB_Extend = 22,
  GB_AddGift = 23,
  GB_GemBlend = 24,
  GB_GemEmbed = 25,
  GB_GemExtraction = 26
}.GB_Blend0 = 27
{
  GB_ItemMainBag = 1,
  GB_TaskMainBag = 2,
  GB_ShortcurMeun1 = 3,
  GB_ShortcurMeun2 = 4,
  GB_ShortcurMeun3 = 5,
  GB_WareHouse = 6,
  GB_GuildWHouse = 7,
  GB_SetMail = 8,
  GB_GetMail = 9,
  GB_Character = 10,
  GB_Task = 11,
  GB_Pet = 12,
  GB_PetSkill = 13,
  GB_Mount = 14,
  GB_MountSkill = 15,
  GB_Stall = 16,
  GB_GetTask = 17,
  GB_NpcShop = 18,
  GB_Forge = 20,
  GB_Blend = 21,
  GB_Extend = 22,
  GB_AddGift = 23,
  GB_GemBlend = 24,
  GB_GemEmbed = 25,
  GB_GemExtraction = 26
}.GB_Scroll = 28
{
  GB_ItemMainBag = 1,
  GB_TaskMainBag = 2,
  GB_ShortcurMeun1 = 3,
  GB_ShortcurMeun2 = 4,
  GB_ShortcurMeun3 = 5,
  GB_WareHouse = 6,
  GB_GuildWHouse = 7,
  GB_SetMail = 8,
  GB_GetMail = 9,
  GB_Character = 10,
  GB_Task = 11,
  GB_Pet = 12,
  GB_PetSkill = 13,
  GB_Mount = 14,
  GB_MountSkill = 15,
  GB_Stall = 16,
  GB_GetTask = 17,
  GB_NpcShop = 18,
  GB_Forge = 20,
  GB_Blend = 21,
  GB_Extend = 22,
  GB_AddGift = 23,
  GB_GemBlend = 24,
  GB_GemEmbed = 25,
  GB_GemExtraction = 26
}.GB_Rune = 29
{
  GB_ItemMainBag = 1,
  GB_TaskMainBag = 2,
  GB_ShortcurMeun1 = 3,
  GB_ShortcurMeun2 = 4,
  GB_ShortcurMeun3 = 5,
  GB_WareHouse = 6,
  GB_GuildWHouse = 7,
  GB_SetMail = 8,
  GB_GetMail = 9,
  GB_Character = 10,
  GB_Task = 11,
  GB_Pet = 12,
  GB_PetSkill = 13,
  GB_Mount = 14,
  GB_MountSkill = 15,
  GB_Stall = 16,
  GB_GetTask = 17,
  GB_NpcShop = 18,
  GB_Forge = 20,
  GB_Blend = 21,
  GB_Extend = 22,
  GB_AddGift = 23,
  GB_GemBlend = 24,
  GB_GemEmbed = 25,
  GB_GemExtraction = 26
}.GB_Auth = 30
{
  GB_ItemMainBag = 1,
  GB_TaskMainBag = 2,
  GB_ShortcurMeun1 = 3,
  GB_ShortcurMeun2 = 4,
  GB_ShortcurMeun3 = 5,
  GB_WareHouse = 6,
  GB_GuildWHouse = 7,
  GB_SetMail = 8,
  GB_GetMail = 9,
  GB_Character = 10,
  GB_Task = 11,
  GB_Pet = 12,
  GB_PetSkill = 13,
  GB_Mount = 14,
  GB_MountSkill = 15,
  GB_Stall = 16,
  GB_GetTask = 17,
  GB_NpcShop = 18,
  GB_Forge = 20,
  GB_Blend = 21,
  GB_Extend = 22,
  GB_AddGift = 23,
  GB_GemBlend = 24,
  GB_GemEmbed = 25,
  GB_GemExtraction = 26
}.GB_Ineff = 31
{
  GB_ItemMainBag = 1,
  GB_TaskMainBag = 2,
  GB_ShortcurMeun1 = 3,
  GB_ShortcurMeun2 = 4,
  GB_ShortcurMeun3 = 5,
  GB_WareHouse = 6,
  GB_GuildWHouse = 7,
  GB_SetMail = 8,
  GB_GetMail = 9,
  GB_Character = 10,
  GB_Task = 11,
  GB_Pet = 12,
  GB_PetSkill = 13,
  GB_Mount = 14,
  GB_MountSkill = 15,
  GB_Stall = 16,
  GB_GetTask = 17,
  GB_NpcShop = 18,
  GB_Forge = 20,
  GB_Blend = 21,
  GB_Extend = 22,
  GB_AddGift = 23,
  GB_GemBlend = 24,
  GB_GemEmbed = 25,
  GB_GemExtraction = 26
}.GB_Disass = 32
{
  GB_ItemMainBag = 1,
  GB_TaskMainBag = 2,
  GB_ShortcurMeun1 = 3,
  GB_ShortcurMeun2 = 4,
  GB_ShortcurMeun3 = 5,
  GB_WareHouse = 6,
  GB_GuildWHouse = 7,
  GB_SetMail = 8,
  GB_GetMail = 9,
  GB_Character = 10,
  GB_Task = 11,
  GB_Pet = 12,
  GB_PetSkill = 13,
  GB_Mount = 14,
  GB_MountSkill = 15,
  GB_Stall = 16,
  GB_GetTask = 17,
  GB_NpcShop = 18,
  GB_Forge = 20,
  GB_Blend = 21,
  GB_Extend = 22,
  GB_AddGift = 23,
  GB_GemBlend = 24,
  GB_GemEmbed = 25,
  GB_GemExtraction = 26
}.GB_MountForge = 33
{
  GB_ItemMainBag = 1,
  GB_TaskMainBag = 2,
  GB_ShortcurMeun1 = 3,
  GB_ShortcurMeun2 = 4,
  GB_ShortcurMeun3 = 5,
  GB_WareHouse = 6,
  GB_GuildWHouse = 7,
  GB_SetMail = 8,
  GB_GetMail = 9,
  GB_Character = 10,
  GB_Task = 11,
  GB_Pet = 12,
  GB_PetSkill = 13,
  GB_Mount = 14,
  GB_MountSkill = 15,
  GB_Stall = 16,
  GB_GetTask = 17,
  GB_NpcShop = 18,
  GB_Forge = 20,
  GB_Blend = 21,
  GB_Extend = 22,
  GB_AddGift = 23,
  GB_GemBlend = 24,
  GB_GemEmbed = 25,
  GB_GemExtraction = 26
}.GB_GemIneffMain = 34
gUI_GOODBOX_BAGTYPES, {
  GB_ItemMainBag = 1,
  GB_TaskMainBag = 2,
  GB_ShortcurMeun1 = 3,
  GB_ShortcurMeun2 = 4,
  GB_ShortcurMeun3 = 5,
  GB_WareHouse = 6,
  GB_GuildWHouse = 7,
  GB_SetMail = 8,
  GB_GetMail = 9,
  GB_Character = 10,
  GB_Task = 11,
  GB_Pet = 12,
  GB_PetSkill = 13,
  GB_Mount = 14,
  GB_MountSkill = 15,
  GB_Stall = 16,
  GB_GetTask = 17,
  GB_NpcShop = 18,
  GB_Forge = 20,
  GB_Blend = 21,
  GB_Extend = 22,
  GB_AddGift = 23,
  GB_GemBlend = 24,
  GB_GemEmbed = 25,
  GB_GemExtraction = 26
}.GB_GemIneffSub = {
  GB_ItemMainBag = 1,
  GB_TaskMainBag = 2,
  GB_ShortcurMeun1 = 3,
  GB_ShortcurMeun2 = 4,
  GB_ShortcurMeun3 = 5,
  GB_WareHouse = 6,
  GB_GuildWHouse = 7,
  GB_SetMail = 8,
  GB_GetMail = 9,
  GB_Character = 10,
  GB_Task = 11,
  GB_Pet = 12,
  GB_PetSkill = 13,
  GB_Mount = 14,
  GB_MountSkill = 15,
  GB_Stall = 16,
  GB_GetTask = 17,
  GB_NpcShop = 18,
  GB_Forge = 20,
  GB_Blend = 21,
  GB_Extend = 22,
  GB_AddGift = 23,
  GB_GemBlend = 24,
  GB_GemEmbed = 25,
  GB_GemExtraction = 26
}, 35
function UI_On_Darg_GB_L_Click(msg)
  local form_type = msg:get_window():GetProperty("GoodBoxIndex")
  if form_type == "GB_ItemMainBag" then
    UI_Bag_ItemLbtndown(msg)
  elseif form_type == "GB_TaskMainBag" then
    UI_Bag_ItemLbtndown(msg)
  elseif form_type == "GB_ShortcurMeun1" then
    UI_ActBr_LBtnClick(msg)
  elseif form_type == "GB_ShortcurMeun2" then
    UI_ActBr_LBtnClick(msg)
  elseif form_type == "GB_ShortcurMeun3" then
    UI_ActBr_LBtnClick(msg)
  elseif form_type == "GB_WareHouse" then
  elseif form_type == "GB_GuildWHouse" then
    UI_GBank_LClickGoodbox(msg)
  elseif form_type == "GB_SetMail" then
  elseif form_type == "GB_GetMail" then
    UI_Email_LBtnClick(msg)
  elseif form_type == "GB_Character" then
  elseif form_type == "GB_Task" then
  elseif form_type == "GB_Pet" then
    UI_Pet_PetLClick(msg)
  elseif form_type == "GB_PetSkill" then
  elseif form_type == "GB_PetEquip" then
  elseif form_type == "GB_Mount" then
    UI_Mounts_MountLClick(msg)
  elseif form_type == "GB_MountSkill" then
  elseif form_type == "GB_Stall" then
  elseif form_type == "GB_GetTask" then
  elseif form_type == "GB_NpcShop" then
    UI_Shop_BuyBackItem(msg)
  elseif form_type == "GB_Forge" then
    UI_Forge_LBtn(msg)
  elseif form_type == "GB_Blend" then
  elseif form_type == "GB_Blend0" then
  elseif form_type == "GB_Extend" then
  elseif form_type == "GB_AddGift" then
    UI_Mounts_AddGift(msg)
  elseif form_type == "GB_GemBlend" then
  elseif form_type == "GB_GemEmbed" then
  elseif form_type == "GB_GemExtraction" then
  elseif form_type == "GB_Rune" then
    UI_Gift_RuneLClick(msg)
  elseif form_type == "GB_Auth" then
  elseif form_type == "GB_Ineff" then
  elseif form_type == "GB_Disass" then
  elseif "GB_MountForge" == form_type then
  elseif "GB_GemIneffMain" == form_type then
  elseif "GB_GemIneffSub" == form_type then
  end
end
function UI_On_Darg_GB_R_Click(msg)
  local form_type = msg:get_window():GetProperty("GoodBoxIndex")
  if form_type == "GB_ItemMainBag" then
    UI_Bag_ItemRbtndown(msg)
  elseif form_type == "GB_TaskMainBag" then
    UI_Bag_ItemRbtndown(msg)
  elseif form_type == "GB_ShortcurMeun1" then
    UI_ActBr_RBtnClick(msg)
  elseif form_type == "GB_ShortcurMeun2" then
    UI_ActBr_RBtnClick(msg)
  elseif form_type == "GB_ShortcurMeun3" then
    UI_ActBr_RBtnClick(msg)
  elseif form_type == "GB_WareHouse" then
    UI_Bank_RClick(msg)
  elseif form_type == "GB_GuildWHouse" then
    UI_GBank_RClick(msg)
  elseif form_type == "GB_SetMail" then
    UI_Email_RemoveSendItem(msg)
  elseif form_type == "GB_GetMail" then
    UI_Email_GetRecvtem(msg)
  elseif form_type == "GB_Character" then
    UI_Chara_UnMountEquip(msg)
  elseif form_type == "GB_Task" then
  elseif form_type == "GB_Pet" then
  elseif form_type == "GB_PetSkill" then
  elseif form_type == "GB_PetEquip" then
    UI_Pet_EquipOff(msg)
  elseif form_type == "GB_Mount" then
    UI_Mounts_MountRClick(msg)
  elseif form_type == "GB_MountSkill" then
    UI_Mounts_UseSkill(msg)
  elseif form_type == "GB_Stall" then
    UI_Stall_ItemClear(msg)
  elseif form_type == "GB_GetTask" then
  elseif form_type == "GB_NpcShop" then
    UI_Shop_BuyBackItem(msg)
  elseif form_type == "GB_Forge" then
    UI_Forge_RClickItem(msg)
  elseif form_type == "GB_Blend" then
    UI_Blend_SideEquipRClick(msg)
  elseif form_type == "GB_Blend0" then
    UI_Blend_MainEquipRClick(msg)
  elseif form_type == "GB_Extend" then
  elseif form_type == "GB_AddGift" then
  elseif form_type == "GB_GemBlend" then
    UI_GemCompose_ItemRClick(msg)
  elseif form_type == "GB_GemEmbed" then
    UI_GemBeset_EquipRC(msg)
  elseif form_type == "GB_GemExtraction" then
    UI_GemEx_EquipRC(msg)
  elseif form_type == "GB_Scroll" then
  elseif form_type == "GB_Auth" then
    UI_Authenticate_OnRClick(msg)
  elseif form_type == "GB_Ineff" then
    UI_Ineffective_EquipOnRClick(msg)
  elseif form_type == "GB_Disass" then
    UI_Resolve_EquipOnRClick(msg)
  elseif "GB_MountForge" == form_type then
    UI_MountForge_RClick(msg)
  elseif "GB_GemIneffMain" == form_type then
    UI_GemIneff_MainRClick(msg)
  elseif "GB_GemIneffSub" == form_type then
    UI_GemIneff_SubRClick(msg)
  end
end
function UI_On_Darg_HoverOnClick(msg)
  local form_type = msg:get_window():GetProperty("GoodBoxIndex")
  if form_type == "GB_ItemMainBag" then
    UI_Bag_GetItem(msg)
  elseif form_type == "GB_TaskMainBag" then
    UI_Bag_GetItem(msg)
  elseif form_type == "GB_ShortcurMeun1" then
    UI_ActBr_ItemDrop(msg)
  elseif form_type == "GB_ShortcurMeun2" then
    UI_ActBr_ItemDrop(msg)
  elseif form_type == "GB_ShortcurMeun3" then
    UI_ActBr_ItemDrop(msg)
  elseif form_type == "GB_WareHouse" then
    UI_Bank_Drop(msg)
  elseif form_type == "GB_GuildWHouse" then
    UI_GBank_Drop(msg)
  elseif form_type == "GB_SetMail" then
    UI_Email_AddSendItem(msg)
  elseif form_type == "GB_GetMail" then
  elseif form_type == "GB_Character" then
    UI_Chara_PlaceEquip(msg)
  elseif form_type == "GB_Task" then
  elseif form_type == "GB_Pet" then
    UI_PetPlacedIn(msg)
  elseif form_type == "GB_PetSkill" then
  elseif form_type == "GB_PetEquip" then
    UI_Pet_EquipOn(msg)
  elseif form_type == "GB_Mount" then
    UI_Mounts_DragDownItem(msg)
  elseif form_type == "GB_MountSkill" then
  elseif form_type == "GB_Stall" then
    UI_Stall_ItemDrop(msg)
  elseif form_type == "GB_GetTask" then
  elseif form_type == "GB_NpcShop" then
    UI_Shop_ItemDrop(msg)
  elseif form_type == "GB_Forge" then
    UI_Forge_ItemDrop(msg)
  elseif form_type == "GB_Blend" then
    UI_Blend_SideEquipDrop(msg)
  elseif form_type == "GB_Blend0" then
    UI_Blend_MainEquipDrop(msg)
  elseif form_type == "GB_Extend" then
  elseif form_type == "GB_AddGift" then
  elseif form_type == "GB_GemBlend" then
    UI_GemComp_GemDrop(msg)
  elseif form_type == "GB_GemEmbed" then
    UI_GemBeset_DropItemEquip(msg)
  elseif form_type == "GB_GemExtraction" then
    UI_GemEx_ItemDrop(msg)
  elseif form_type == "GB_Scroll" then
    UI_Scroll_ItemDrop(msg)
  elseif form_type == "GB_Rune" then
    UI_Gift_RuneDrop(msg)
  elseif form_type == "GB_Auth" then
    UI_Authenticate_OnDraged(msg)
  elseif form_type == "GB_Ineff" then
    UI_Ineffective_OnDraged(msg)
  elseif form_type == "GB_Disass" then
    UI_Resolve_OnDraged(msg)
  elseif "GB_MountForge" == form_type then
    UI_MountForge_Darg(msg)
  elseif "GB_GemIneffMain" == form_type then
    UI_GemIneff_DargMainSlot(msg)
  elseif "GB_GemIneffSub" == form_type then
    UI_GemIneff_DargSubSlot(msg)
  end
end
