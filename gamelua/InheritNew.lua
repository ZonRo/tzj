if gUI and not gUI.InheritNew then
  gUI.InheritNew = {}
end
INHERITNEW_SLOT_COLLATE = {
  MAINEQUIP = 1,
  SUBEQUIP = 0,
  MATERIAL = 2
}
function UI_MainEquip_OnDarged(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    slot = Lua_Bag_GetRealSlot(from_win, slot)
    Lua_InheritNew_CheckMainEquip(slot)
  else
    return
  end
end
function UI_SubEquip_OnDarged(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    slot = Lua_Bag_GetRealSlot(from_win, slot)
    Lua_InheritNew_CheckSubEquip(slot)
  else
    return
  end
end
function UI_Material_OnDarged(msg)
end
function UI_MainEquip_OnRClick(msg)
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.MAINEQUIP)
  if not slot then
    return
  end
  SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, slot, OperateMode.Cancel, INHERITNEW_SLOT_COLLATE.MAINEQUIP, NpcFunction.NPC_FUNC_EQUIP_SMRITI)
end
function UI_SubEquip_OnRClick(msg)
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.SUBEQUIP)
  if not slot then
    return
  end
  SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, slot, OperateMode.Remove, INHERITNEW_SLOT_COLLATE.SUBEQUIP, NpcFunction.NPC_FUNC_EQUIP_SMRITI)
end
function UI_Material_OnRClick(msg)
end
function UI_MainEquip_OnHold(msg)
  local win = msg:get_window()
  local tooltip = win:GetToolTipWnd(0)
  if WindowToGoodsBox(win):IsItemHasIcon(0) then
    local sbag, sslot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.MAINEQUIP)
    Lua_Tip_Item(tooltip, sslot, gUI_GOODSBOX_BAG.backpack0, nil, 0, nil)
    Lua_Tip_ShowEquipCompare(win, gUI_GOODSBOX_BAG.backpack0, sslot)
  end
end
function UI_SubEquip_OnHold(msg)
  local win = msg:get_window()
  local tooltip = win:GetToolTipWnd(0)
  if WindowToGoodsBox(win):IsItemHasIcon(0) then
    local sbag, sslot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.SUBEQUIP)
    Lua_Tip_Item(tooltip, sslot, gUI_GOODSBOX_BAG.backpack0, nil, 0, nil)
    Lua_Tip_ShowEquipCompare(win, gUI_GOODSBOX_BAG.backpack0, sslot)
  end
end
function UI_Material_OnHold(msg)
  local win = msg:get_window()
  local tooltip = win:GetToolTipWnd(0)
  if WindowToGoodsBox(win):IsItemHasIcon(0) then
    local SBag, SSlot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.SUBEQUIP)
    local MBag, MSlot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.MAINEQUIP)
    if MBag and MSlot then
      local nEInCtrlM, SuitJobM, nEPointM, nForgeLevM, nClrLevM, nUseLevM = GetInheritInfo(MSlot, MBag)
      local nEInCtrlS, SuitJobS, nEPointS, nForgeLevS, nClrLevS, nUseLevS = GetInheritInfo(SSlot, SBag)
      local nItemBind, nItemUnBind, nItemNum, nMoneyType, nMoney, nItemBind1, nItemUnBind1, nItemNum1, nMoneyType1, nMoney1, totalNum, totalNum1 = GetInheritNeedInfo(nEInCtrlM, nForgeLevS)
      local nClassM = math.floor(nUseLevM / 10)
      local nClassS = math.floor(nUseLevS / 10)
      if nClassM == nClassS and nItemNum then
        Lua_Tip_Item(tooltip, nil, nil, nil, nil, nItemUnBind)
      elseif nClassM - nClassS == 1 and nItemNum1 then
        Lua_Tip_Item(tooltip, nil, nil, nil, nil, nItemUnBind1)
      end
    end
  end
end
function UI_InheritNew_OpenPanel(msg)
  if gUI.InheritNew.Root:IsVisible() then
    _InheritNew_CloseWindow()
  else
    _InheritNew_ShowWindow()
  end
end
function UI_InheritNew_OkBtnClick(msg)
  local MBag, MSlot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.MAINEQUIP)
  local SBag, SSlot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.SUBEQUIP)
  if not MBag and not MSlot then
    Lua_Chat_ShowOSD("INHERT_FRISTPLACEMAIN")
    return
  end
  if not SBag and not SSlot then
    Lua_Chat_ShowOSD("INHERT_TASKSUB")
    return
  end
  local IsBindedMain = GetBindInfo(MSlot, MBag)
  local IsBindedSub = GetBindInfo(SSlot, SBag)
  if IsBindedMain == false and IsBindedSub == false then
    Messagebox_Show("INHERIT_ASK_CONFIRM_BINDINFO1")
  elseif IsBindedMain == true and IsBindedSub == false then
    Messagebox_Show("INHERIT_ASK_CONFIRM_BINDINFO2")
  elseif IsBindedMain == false and IsBindedSub == true then
    Messagebox_Show("INHERIT_ASK_CONFIRM_BINDINFO3")
  elseif IsBindedMain == true and IsBindedSub == true then
    Messagebox_Show("INHERIT_ASK_CONFIRM_BINDINFO4")
  end
end
function UI_InheritNew_Close(msg)
  _InheritNew_CloseWindow()
end
function UI_InheritNew_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Inherit.Inherit_bg.Top_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function Lua_InheritNew_CheckMainEquip(slot)
  if NPC_IsEquip(slot) == false then
    Lua_Chat_ShowOSD("INHERT_NOT_EQUIP")
    return false
  end
  local nEInCtrl, SuitJob, nEPoint = GetInheritInfo(slot, gUI_GOODSBOX_BAG.backpack0)
  if nEInCtrl == -1 then
    Lua_Chat_ShowOSD("INHERT_CANNOT")
    return false
  end
  if SuitJob == 0 then
    Lua_Chat_ShowOSD("INHERT_NOT_SUIT")
    return false
  end
  SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, slot, OperateMode.Add, INHERITNEW_SLOT_COLLATE.MAINEQUIP, NpcFunction.NPC_FUNC_EQUIP_SMRITI)
  return true
end
function Lua_InheritNew_CheckSubEquip(slot)
  if NPC_IsEquip(slot) == false then
    Lua_Chat_ShowOSD("INHERT_NOT_EQUIP")
    return false
  end
  local mBag, mSlot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.MAINEQUIP)
  if not mBag and not mSolt then
    Lua_Chat_ShowOSD("INHERT_FRISTPLACEMAIN")
    return false
  end
  local nEInCtrl, SuitJob, nEPoint, nForgeLev, nClrLev, nUseLev = GetInheritInfo(slot, gUI_GOODSBOX_BAG.backpack0)
  local _, _, nEPointMain, nForgeLevMain, _, nUseLevMain = GetInheritInfo(mSlot, mBag)
  if SuitJob == 0 then
    Lua_Chat_ShowOSD("INHERT_NOT_SUIT2")
    return false
  end
  if nEPointMain ~= nEPoint then
    Lua_Chat_ShowOSD("INHERT_NOT_SUITMAINPOINT")
    return false
  end
  if nForgeLev <= nForgeLevMain then
    Lua_Chat_ShowOSD("INHERT_SUCC_LEV_ERR")
    return false
  end
  if nUseLev > nUseLevMain then
    Lua_Chat_ShowOSD("INHERT_SUCC_LEV_ERR2")
    return false
  end
  local nClass = nUseLev / 10
  local nClassMain = nUseLevMain / 10
  if nClass > nClassMain or nClassMain > nClass + 1 then
    Lua_Chat_ShowOSD("INHERT_CLASS_LEV_ERR2")
    return false
  end
  local isEmbbed = GetEquipGemInfo(gUI_GOODSBOX_BAG.backpack0, slot, 0)
  if isEmbbed then
    Lua_Chat_ShowOSD("INHERT_GEM_ERR")
    return false
  end
  if nClassMain - nClass == 1 and nForgeLev < 7 then
    Lua_Chat_ShowOSD("INHERT_FORGE_ERR")
    return false
  end
  SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, slot, OperateMode.Add, INHERITNEW_SLOT_COLLATE.SUBEQUIP, NpcFunction.NPC_FUNC_EQUIP_SMRITI)
  return true
end
function Lua_InheritNew_CheckMaterial(slot)
end
function InheritNew_SetMainSlot()
  local MBag, MSlot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.MAINEQUIP)
  if MBag and MSlot then
    local _, icon, quality = GetItemInfoBySlot(MBag, MSlot, 0)
    if not icon then
      return
    end
    gUI.InheritNew.MainEquipSlot:SetItemGoods(0, icon, quality)
    local intenLev = Lua_Bag_GetStarInfo(MBag, MSlot)
    if 0 < ForgeLevel_To_Stars[intenLev] then
      gUI.InheritNew.MainEquipSlot:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
    else
      gUI.InheritNew.MainEquipSlot:SetItemStarState(0, 0)
    end
    gUI.InheritNew.MainEquipSlot:SetItemBindState(0, Lua_Bag_GetBindInfo(MBag, MSlot))
  end
end
function InheritNew_SetSubSlot()
  local SBag, SSlot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.SUBEQUIP)
  if SBag and SSlot then
    local _, icon, quality = GetItemInfoBySlot(SBag, SSlot, 0)
    if not icon then
      return
    end
    gUI.InheritNew.SubEquipSlot:SetItemGoods(0, icon, quality)
    local intenLev = Lua_Bag_GetStarInfo(SBag, SSlot)
    if 0 < ForgeLevel_To_Stars[intenLev] then
      gUI.InheritNew.SubEquipSlot:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
    else
      gUI.InheritNew.SubEquipSlot:SetItemStarState(0, 0)
    end
    gUI.InheritNew.SubEquipSlot:SetItemBindState(0, Lua_Bag_GetBindInfo(SBag, SSlot))
  end
end
function InheritNew_SetCostSlot(slot)
end
function InheritNew_SetMoneyAndMaterial()
  local MBag, MSlot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.MAINEQUIP)
  local SBag, SSlot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.SUBEQUIP)
  if MBag and MSlot and SBag and SSlot then
    local nEInCtrlM, SuitJobM, nEPointM, nForgeLevM, nClrLevM, nUseLevM = GetInheritInfo(MSlot, MBag)
    local nEInCtrlS, SuitJobS, nEPointS, nForgeLevS, nClrLevS, nUseLevS = GetInheritInfo(SSlot, SBag)
    local nItemBind, nItemUnBind, nItemNum, nMoneyType, nMoney, nItemBind1, nItemUnBind1, nItemNum1, nMoneyType1, nMoney1, totalNum, totalNum1 = GetInheritNeedInfo(nEInCtrlM, nForgeLevS)
    local nClassM = math.floor(nUseLevM / 10)
    local nClassS = math.floor(nUseLevS / 10)
    if nClassM == nClassS and nItemNum then
      if nMoney and nMoney > 0 then
        gUI.InheritNew.CostSLab:SetProperty("Visible", "true")
        gUI.InheritNew.CostLab:SetProperty("Text", Lua_UI_Money2String(nMoney))
      else
        gUI.InheritNew.CostSLab:SetProperty("Visible", "false")
      end
      if nItemBind and nItemNum > 0 then
        gUI.InheritNew.CostMaterSlot:SetProperty("Visible", "true")
        gUI.InheritNew.CostMaterLab:SetProperty("Visible", "true")
        local _, icon, quality, _, _, _, _, _, _, _, _, itemCount = GetItemInfoBySlot(-1, nItemBind, 0)
        if not icon then
          return
        end
        gUI.InheritNew.CostMaterSlot:SetItemGoods(0, icon, quality)
        if totalNum < nItemNum then
          gUI.InheritNew.CostMaterSlot:SetProperty("Alpha", "0.3")
        else
          gUI.InheritNew.CostMaterSlot:SetProperty("Alpha", "1")
        end
        local itemCount = tostring(totalNum) .. "/" .. tostring(nItemNum)
        gUI.InheritNew.CostMaterSlot:SetItemSubscript(0, tostring(itemCount))
      else
        gUI.InheritNew.CostMaterSlot:SetProperty("Visible", "false")
        gUI.InheritNew.CostMaterLab:SetProperty("Visible", "false")
      end
    elseif nClassM - nClassS == 1 and nItemNum1 then
      if nMoney1 and nMoney1 > 0 then
        gUI.InheritNew.CostSLab:SetProperty("Visible", "true")
        gUI.InheritNew.CostLab:SetProperty("Text", Lua_UI_Money2String(nMoney1))
      else
        gUI.InheritNew.CostSLab:SetProperty("Visible", "false")
      end
      if nItemBind1 and nItemNum1 > 0 then
        gUI.InheritNew.CostMaterSlot:SetProperty("Visible", "true")
        gUI.InheritNew.CostMaterLab:SetProperty("Visible", "true")
        local _, icon, quality, _, _, _, _, _, _, _, _, itemCount = GetItemInfoBySlot(-1, nItemBind1, 0)
        if not icon then
          return
        end
        gUI.InheritNew.CostMaterSlot:SetItemGoods(0, icon, quality)
        if totalNum1 < nItemNum1 then
          gUI.InheritNew.CostMaterSlot:SetProperty("Alpha", "0.3")
        else
          gUI.InheritNew.CostMaterSlot:SetProperty("Alpha", "1")
        end
        local itemCount = tostring(totalNum1) .. "/" .. tostring(nItemNum1)
        gUI.InheritNew.CostMaterSlot:SetItemSubscript(0, tostring(itemCount))
      else
        gUI.InheritNew.CostMaterSlot:SetProperty("Visible", "false")
        gUI.InheritNew.CostMaterLab:SetProperty("Visible", "false")
      end
    else
      gUI.InheritNew.CostLab:SetProperty("Text", Lua_UI_Money2String(0))
      gUI.InheritNew.CostSLab:SetProperty("Visible", "false")
      gUI.InheritNew.CostMaterSlot:ClearGoodsItem(0)
    end
  else
    gUI.InheritNew.CostLab:SetProperty("Text", Lua_UI_Money2String(0))
    gUI.InheritNew.CostSLab:SetProperty("Visible", "false")
    gUI.InheritNew.CostMaterSlot:ClearGoodsItem(0)
  end
end
function InheritNew_SetPreview()
  local SBag, SSlot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.SUBEQUIP)
  local MBag, MSlot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.MAINEQUIP)
  gUI.InheritNew.DescriptSlot:RemoveAllItems()
  if SBag and SSlot and MBag and MSlot then
    local nEInCtrl, SuitJob, nEPoint, nForgeLev, nClrLev, nUseLev = GetInheritInfo(SSlot, SBag)
    local nEInCtrlM, SuitJobM, nEPointM, nForgeLevM, nClrLevM, nUseLevM = GetInheritInfo(MSlot, MBag)
    local colorForge = ""
    local colorClr = ""
    if nForgeLev < nForgeLevM then
      colorForge = colorRed
    elseif nForgeLev == nForgeLevM then
      colorForge = colorWhite
    else
      colorForge = colorGreen
    end
    if nClrLev < nClrLevM then
      colorClr = colorRed
    elseif nClrLev == nClrLevM then
      colorClr = colorWhite
    else
      colorClr = colorGreen
    end
    local strF = "历史最高锻造等级|" .. "{#C^" .. colorWhite .. "^" .. tostring(nForgeLevM) .. "}" .. "|{#C^" .. colorForge .. "^" .. tostring(nForgeLev) .. "}"
    local strC = "当前锻造等级|" .. "{#C^" .. colorWhite .. "^" .. tostring(nClrLevM) .. "}" .. "|{#C^" .. colorClr .. "^" .. tostring(nClrLev) .. "}"
    gUI.InheritNew.DescriptSlot:InsertString(strF, -1)
    gUI.InheritNew.DescriptSlot:InsertString(strC, -1)
  elseif MBag and MSlot then
    local nEInCtrl, SuitJob, nEPoint, nForgeLev, nClrLev, nUseLev = GetInheritInfo(MSlot, MBag)
    local strF = "历史最高锻造等级|" .. "{#C^" .. colorWhite .. "^" .. tostring(nForgeLev) .. "}" .. "|" .. ""
    local strC = "当前锻造等级|" .. "{#C^" .. colorWhite .. "^" .. tostring(nClrLev) .. "}" .. "|" .. ""
    gUI.InheritNew.DescriptSlot:InsertString(strF, -1)
    gUI.InheritNew.DescriptSlot:InsertString(strC, -1)
  end
end
function _OnInheritNew_CloseWindw()
  _InheritNew_CloseWindow()
end
function _OnInheritNew_ShowWindw()
  _InheritNew_ShowWindow()
end
function _InheritNew_CloseWindow()
  if not gUI.InheritNew.Root:IsVisible() then
    return
  end
  gUI.InheritNew.Root:SetProperty("Visible", "false")
end
function _InheritNew_ShowWindow()
  if gUI.InheritNew.Root:IsVisible() then
    return
  end
  gUI.InheritNew.Root:SetProperty("Visible", "true")
  gUI.InheritNew.ShowLab:SetProperty("Visible", "false")
  gUI.InheritNew.DownLab:SetProperty("Visible", "false")
  gUI.InheritNew.FimBtn:SetProperty("Enable", "false")
end
function _OnInheritNew_AddItem(slot, itemType)
  if itemType == INHERITNEW_SLOT_COLLATE.MAINEQUIP then
    _InheritNew_AddMainEquip(slot)
  elseif itemType == INHERITNEW_SLOT_COLLATE.SUBEQUIP then
    _InheritNew_AddSubEquip(slot)
  elseif itemType == INHERITNEW_SLOT_COLLATE.MATERIAL then
    _InheritNew_AddMaterial(slot)
  end
end
function _OnInheritNew_DelItem(slot, itemType)
  if itemType == INHERITNEW_SLOT_COLLATE.MAINEQUIP then
    _InheritNew_DelMainEquip(slot)
  elseif itemType == INHERITNEW_SLOT_COLLATE.SUBEQUIP then
    _InheritNew_DelSubEquip()
  elseif itemType == INHERITNEW_SLOT_COLLATE.MATERIAL then
    _InheritNew_DelMaterial()
  end
end
function _InheritNew_AddMainEquip(slot)
  InheritNew_SetMainSlot()
  InheritNew_SetPreview()
  gUI.InheritNew.ShowLab:SetProperty("Visible", "true")
end
function _InheritNew_AddSubEquip(slot)
  InheritNew_SetSubSlot()
  InheritNew_SetPreview()
  InheritNew_SetMoneyAndMaterial()
  gUI.InheritNew.DownLab:SetProperty("Visible", "true")
  gUI.InheritNew.FimBtn:SetProperty("Enable", "true")
end
function _InheritNew_AddMaterial(slot)
  InheritNew_SetCostSlot(slot)
end
function _InheritNew_DelMainEquip(slot)
  gUI.InheritNew.ShowLab:SetProperty("Visible", "false")
  gUI.InheritNew.MainEquipSlot:ClearGoodsItem(0)
  gUI.InheritNew.MainEquipSlot:SetItemBindState(0, 0)
  gUI.InheritNew.SubEquipSlot:ClearGoodsItem(0)
  gUI.InheritNew.SubEquipSlot:SetItemBindState(0, 0)
  gUI.InheritNew.CostMaterSlot:ClearGoodsItem(0)
  gUI.InheritNew.DescriptSlot:RemoveAllItems()
  gUI.InheritNew.CostLab:SetProperty("Text", Lua_UI_Money2String(0))
end
function _InheritNew_DelSubEquip()
  gUI.InheritNew.DownLab:SetProperty("Visible", "false")
  gUI.InheritNew.FimBtn:SetProperty("Enable", "false")
  gUI.InheritNew.SubEquipSlot:ClearGoodsItem(0)
  gUI.InheritNew.SubEquipSlot:SetItemBindState(0, 0)
  InheritNew_SetPreview()
  InheritNew_SetMoneyAndMaterial()
end
function _InheritNew_DelMaterial()
end
function _OnInheritNew_GetResult()
  Lua_Chat_ShowSysLog("INHERT_SUCC")
  Lua_Chat_ShowOSD("INHERT_SUCC")
  gUI.InheritNew.MainEquipSlot:ClearGoodsItem(0)
  gUI.InheritNew.MainEquipSlot:SetItemBindState(0, 0)
  gUI.InheritNew.SubEquipSlot:ClearGoodsItem(0)
  gUI.InheritNew.SubEquipSlot:SetItemBindState(0, 0)
  gUI.InheritNew.CostMaterSlot:ClearGoodsItem(0)
  InheritNew_SetPreview()
  gUI.InheritNew.CostLab:SetProperty("Text", Lua_UI_Money2String(0))
  gUI.InheritNew.CostSLab:SetProperty("Visible", "false")
  SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, slot, OperateMode.Cancel, INHERITNEW_SLOT_COLLATE.MAINEQUIP, NpcFunction.NPC_FUNC_EQUIP_SMRITI)
end
function _OnInheritNew_BagItemChange()
  InheritNew_SetMoneyAndMaterial()
end
function Script_InheritNew_OnEvent(event)
  if "CLOSE_NPC_RELATIVE_DIALOG" == event then
    _OnInheritNew_CloseWindw()
  elseif "INHERIT_SHOW" == event then
    _OnInheritNew_ShowWindw()
  elseif "INHERIT_ADD_ITEM" == event then
    _OnInheritNew_AddItem(arg1, arg2)
  elseif "INHERIT_DEL_ITEM" == event then
    _OnInheritNew_DelItem(arg1, arg2)
  elseif "INHERIT_SHOW_RESULT" == event then
    _OnInheritNew_GetResult()
  elseif "ITEM_ADD_TO_BAG" == event then
    _OnInheritNew_BagItemChange()
  elseif "ITEM_DEL_FROM_BAG" == event then
    _OnInheritNew_BagItemChange()
  end
end
function Script_InheritNew_OnLoad()
  gUI.InheritNew.Root = WindowSys_Instance:GetWindow("Inherit")
  gUI.InheritNew.MainEquipSlot = WindowToGoodsBox(WindowSys_Instance:GetWindow("Inherit.Inherit_bg.Back_bg.Left_bg.Top_bg.Main_bg.Main_gbox"))
  gUI.InheritNew.SubEquipSlot = WindowToGoodsBox(WindowSys_Instance:GetWindow("Inherit.Inherit_bg.Back_bg.Left_bg.Top_bg.Subsidiary_bg.Subsidiary_gbox"))
  gUI.InheritNew.DescriptSlot = WindowToListBox(WindowSys_Instance:GetWindow("Inherit.Inherit_bg.Back_bg.Left_bg.Top_bg.Inherit_bg.Inherit_lbox"))
  gUI.InheritNew.CostMaterSlot = WindowToGoodsBox(WindowSys_Instance:GetWindow("Inherit.Inherit_bg.Back_bg.Left_bg.Down_bg.Material_bg.Material_gbox"))
  gUI.InheritNew.CostMaterLab = WindowToLabel(WindowSys_Instance:GetWindow("Inherit.Inherit_bg.Back_bg.Left_bg.Down_bg.Material_bg.Material_slab"))
  gUI.InheritNew.CostLab = WindowToLabel(WindowSys_Instance:GetWindow("Inherit.Inherit_bg.Back_bg.Left_bg.Down_bg.Money_bg.Money_dlab"))
  gUI.InheritNew.CostSLab = WindowSys_Instance:GetWindow("Inherit.Inherit_bg.Back_bg.Left_bg.Down_bg.Money_bg")
  gUI.InheritNew.ShowLab = WindowSys_Instance:GetWindow("Inherit.Inherit_bg.Back_bg.Left_bg.Top_bg.Inherit_bg")
  gUI.InheritNew.DownLab = WindowSys_Instance:GetWindow("Inherit.Inherit_bg.Back_bg.Left_bg.Down_bg")
  gUI.InheritNew.FimBtn = WindowSys_Instance:GetWindow("Inherit.Inherit_bg.Back_bg.Left_bg.Firm_btn")
end
function Script_InheritNew_OnHide()
  SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, slot, OperateMode.Cancel, INHERITNEW_SLOT_COLLATE.MAINEQUIP, NpcFunction.NPC_FUNC_EQUIP_SMRITI)
end
