if gUI and not gUI.Battle then
  gUI.Battle = {}
end
local _Battle_BGHieghtOpen = 180
local _Battle_BGHieghtClose = 30
local _Battle_BGHonour = 741
local _Battle_BGExHonour = 700
local _Battle_SelectType = 0
local _Battle_PVPMapID = 133
local _Battle_PVPResult = -1
local _Battle_dogfall = 1
local _Battle_lose = 0
local _Battle_win = 2
local _Battle_Blue = 0
local _Battle_Red = 1
local _Battle_All = 2
local _Battle_DogfallImage = "UiIamge018:Image_DogFall"
local _Battle_BlueWinImage = "UiIamge018:Image_BlueWin"
local _Battle_RedWinImage = "UiIamge018:Image_RedWin"
local _Battle_BlueImage = "UiIamge018:Image_BlueBg"
local _Battle_RedImage = "UiIamge018:Image_RedgBg"
local _Battle_SortContion = {
  "Name",
  "Menpai",
  "Lv",
  "Kill",
  "Death",
  "Total",
  "Heal",
  "Defence",
  "SubTime",
  "Honor"
}
local _Battle_SortTypeTotal = 6
local _Battle_SortTypeHonor = 10
local _Battle_SortIndex = _Battle_SortTypeTotal
local _Battle_SortFlag = {
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0
}
function _Battle_UpdateBGinfo()
  local blue, red = BattleScoreBoardInfo()
  gUI.Battle.ScoreBlue:SetProperty("Text", tostring(blue))
  gUI.Battle.ScoreRed:SetProperty("Text", tostring(red))
end
function _Battle_UpdateNumber()
  local blueNum, redNum, _ = BattleTeamBaseInfo()
  gUI.Battle.BattleBlueNum:SetProperty("Text", tostring(blueNum))
  gUI.Battle.BattleRedNum:SetProperty("Text", tostring(redNum))
end
function _Battle_SortTeam(ItemA, ItemB)
  local sortType = _Battle_SortContion[_Battle_SortIndex]
  if _Battle_SortFlag[_Battle_SortIndex] == 0 then
    return ItemA[sortType] > ItemB[sortType]
  else
    return ItemA[sortType] < ItemB[sortType]
  end
end
function _Battle_UpdateTeamList(typeIndex)
  gUI.Battle.BattleTeamContainer:SetAllChildVisible(false, 0, 0)
  local TeamList = GetBattleTeamInfo(typeIndex)
  if TeamList[1] == nil then
    return
  end
  local myName = GetPlaySelfProp(6)
  table.sort(TeamList, _Battle_SortTeam)
  for i = 1, table.maxn(TeamList) do
    local _item = TeamList[i]
    local newItem
    newItem = gUI.Battle.BattleTeamContainer:GetChildByData1(i)
    newItem = newItem or gUI.Battle.BattleTeamContainer:Insert(-1, gUI.Battle.BattleTeamTemp)
    newItem:SetProperty("Visible", "true")
    newItem:SetUserData64(_item.Guid)
    newItem:SetCustomUserData(0, 0)
    newItem:SetCustomUserData(1, i)
    local textColor = "FFEFD3A3"
    if myName == _item.Name then
      textColor = colorGreen
    end
    newItem:GetChildByName("Name_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, _item.Name))
    newItem:GetChildByName("Pro_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, gUI_MenPaiName[_item.Menpai]))
    newItem:GetChildByName("Lev_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, tostring(_item.Lv)))
    newItem:GetChildByName("Kill_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, tostring(_item.Kill)))
    newItem:GetChildByName("Die_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, tostring(_item.Death)))
    newItem:GetChildByName("Harm_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, tostring(_item.Total)))
    newItem:GetChildByName("Remedy_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, tostring(_item.Heal)))
    newItem:GetChildByName("Stay_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, tostring(_item.Defence)))
    newItem:GetChildByName("Tran_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, tostring(_item.SubTime)))
    if _item.Team == _Battle_Blue then
      newItem:SetProperty("BackImage", _Battle_BlueImage)
    end
    if _item.Team == _Battle_Red then
      newItem:SetProperty("BackImage", _Battle_RedImage)
    end
    local wndHonour = newItem:GetChildByName("Honour_dlab")
    local wndHonourEx = newItem:GetChildByName("Win_dlab")
    if _Battle_PVPResult ~= -1 then
      wndHonour:SetVisible(true)
      wndHonour:SetProperty("Text", string.format("{#C^%s^%s}", textColor, tostring(_item.Honor)))
      wndHonourEx:SetVisible(true)
      local strReslut = ""
      gUI.Battle.BattleReslut:SetVisible(true)
      local _, _, isBlue = BattleTeamBaseInfo()
      local resultBGBlue = _Battle_dogfall
      local resultBGRed = _Battle_dogfall
      if _Battle_PVPResult ~= _Battle_dogfall then
        if _Battle_PVPResult == _Battle_win then
          if isBlue then
            resultBGBlue = _Battle_win
            resultBGRed = _Battle_lose
          else
            resultBGBlue = _Battle_lose
            resultBGRed = _Battle_win
          end
        end
        if _Battle_PVPResult == _Battle_lose then
          if isBlue then
            resultBGBlue = _Battle_lose
            resultBGRed = _Battle_win
          else
            resultBGBlue = _Battle_win
            resultBGRed = _Battle_lose
          end
        end
      else
        gUI.Battle.BattleReslut:SetProperty("BackImage", _Battle_DogfallImage)
      end
      if resultBGBlue ~= _Battle_dogfall then
        if resultBGBlue == _Battle_win then
          gUI.Battle.BattleReslut:SetProperty("BackImage", _Battle_BlueWinImage)
          if _item.Team == _Battle_Blue then
            strReslut = "(胜利 +50)"
          end
        else
          gUI.Battle.BattleReslut:SetProperty("BackImage", _Battle_RedWinImage)
          if _item.Team == _Battle_Red then
            strReslut = "(胜利 +50)"
          end
        end
      end
      if 0 < _item.ExtraHonor then
        if strReslut ~= "" then
          strReslut = strReslut .. "\n(精彩战斗 +" .. tostring(_item.ExtraHonor) .. ")"
        else
          strReslut = "(精彩战斗 +" .. tostring(_item.ExtraHonor) .. ")"
        end
        if not gUI.Battle.BattleSplend:IsVisible() then
          gUI.Battle.BattleSplend:SetVisible(true)
        end
      end
      if strReslut ~= "" then
        wndHonour:SetProperty("Left", "p" .. tostring(_Battle_BGExHonour))
      else
        wndHonour:SetProperty("Left", "p" .. tostring(_Battle_BGHonour))
      end
      wndHonourEx:SetProperty("Text", string.format("{#C^%s^%s}", textColor, strReslut))
    else
      wndHonour:SetVisible(false)
      wndHonourEx:SetVisible(false)
      gUI.Battle.BattleReslut:SetVisible(false)
    end
    gUI.Battle.BattleTeamContainer:SetAllChildVisible(true, 1, i)
  end
end
function _Battle_UpdateByGuid(team, guid)
  local nCount = gUI.Battle.BattleTeamContainer:GetChildCount()
  for i = 0, nCount - 1 do
    local itemTmp = gUI.Battle.BattleTeamContainer:GetChild(i)
    local itemGuid = itemTmp:GetUserData64()
    if itemGuid == guid then
      local myName = GetPlaySelfProp(6)
      local findGuid, Name, Menpai, Lv, Kill, Death, Total, Heal, Defence, SubTime = GetBattleNumberInfoByGuid(guid)
      if not findGuid then
        return
      end
      local textColor = "FFEFD3A3"
      if myName == Name then
        textColor = colorGreen
      end
      itemTmp:GetChildByName("Name_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, Name))
      itemTmp:GetChildByName("Pro_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, gUI_MenPaiName[Menpai]))
      itemTmp:GetChildByName("Lev_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, tostring(Lv)))
      itemTmp:GetChildByName("Kill_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, tostring(Kill)))
      itemTmp:GetChildByName("Die_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, tostring(Death)))
      itemTmp:GetChildByName("Harm_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, tostring(Total)))
      itemTmp:GetChildByName("Remedy_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, tostring(Heal)))
      itemTmp:GetChildByName("Stay_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, tostring(Defence)))
      itemTmp:GetChildByName("Tran_dlab"):SetProperty("Text", string.format("{#C^%s^%s}", textColor, tostring(SubTime)))
      return
    end
  end
end
function _Battle_SetShowTeam(typeIndex)
  local BtnShowAll = WindowToButton(WindowSys_Instance:GetWindow("BattleCount.BattleCount_bg.Btn_bg.All_btn"))
  local BtnShowBlue = WindowToButton(WindowSys_Instance:GetWindow("BattleCount.BattleCount_bg.Btn_bg.Blue_btn"))
  local BtnShowRed = WindowToButton(WindowSys_Instance:GetWindow("BattleCount.BattleCount_bg.Btn_bg.Red_btn"))
  if typeIndex == 2 then
    BtnShowAll:SetStatus("selected")
  else
    BtnShowAll:SetStatus("normal")
  end
  if typeIndex == _Battle_Blue then
    BtnShowBlue:SetStatus("selected")
  else
    BtnShowBlue:SetStatus("normal")
  end
  if typeIndex == _Battle_Red then
    BtnShowRed:SetStatus("selected")
  else
    BtnShowRed:SetStatus("normal")
  end
  if _Battle_PVPResult ~= -1 then
    _Battle_SortIndex = _Battle_SortTypeHonor
  else
    _Battle_SortIndex = _Battle_SortTypeTotal
  end
  for i = 1, #_Battle_SortFlag do
    if i ~= _Battle_SortIndex then
      _Battle_SortFlag[i] = 1
    else
      _Battle_SortFlag[i] = 0
    end
  end
  _Battle_SelectType = typeIndex
  _Battle_UpdateTeamList(typeIndex)
end
function _Battle_ShowBattleCountAll()
  if not gUI.Battle.BattleTeamCount:IsVisible() then
    return
  end
  if gUI.Battle.BGTime:IsVisible() then
    gUI.Battle.BattleTeamTime:SetVisible(true)
    gUI.Battle.BattleTeamTimeBg:SetVisible(true)
  else
    gUI.Battle.BattleTeamTime:SetVisible(false)
    gUI.Battle.BattleTeamTimeBg:SetVisible(false)
  end
  _Battle_UpdateNumber()
  _Battle_SetShowTeam(_Battle_All)
end
function _OnBattle_ShowBattle(BattleType, bShow, useTime)
  if tonumber(BattleType) == 2 then
    if bShow then
      gUI.Battle.BattleUI:SetVisible(true)
      gUI.Battle.BattleInfo:SetProperty("Visible", "true")
      gUI.Battle.BattleTitle:SetProperty("Height", "p" .. tostring(_Battle_BGHieghtOpen))
      if useTime > 0 then
        gUI.Battle.BGTime:SetVisible(true)
        gUI.Battle.BGTime:GetChildByName("Time_dlab"):SetProperty("Text", tostring("&" .. tostring(useTime) .. "&"))
        gUI.Battle.BattleTeamTime:SetProperty("Text", tostring("&" .. tostring(useTime) .. "&"))
        if gUI.Battle.BattleTeamCount:IsVisible() then
          gUI.Battle.BattleTeamTime:SetVisible(true)
          gUI.Battle.BattleTeamTimeBg:SetVisible(true)
        end
      else
        gUI.Battle.BGTime:SetVisible(false)
      end
      _Battle_UpdateBGinfo()
    else
      gUI.Battle.Battle:SetVisible(false)
      _Battle_PVPResult = -1
    end
  end
end
function _OnBattle_UpdateBGResult(mapID, bWin)
  if mapID ~= _Battle_PVPMapID then
    return
  end
  _Battle_PVPResult = bWin
  gUI.Battle.BattleTeamCount:SetProperty("Visible", "true")
  gUI.Battle.BGTime:SetVisible(false)
  gUI.Battle.BattleTeamTime:SetVisible(false)
  _Battle_ShowBattleCountAll()
end
function _OnBattle_NumberChange(teamType)
  if not gUI.Battle.BattleTeamCount:IsVisible() then
    return
  end
  _Battle_UpdateNumber()
  if teamType == _Battle_All or _Battle_SelectType == _Battle_All or teamType == _Battle_SelectType then
    _Battle_SetShowTeam(_Battle_SelectType)
  end
end
function _OnBattle_UpdateInfo(teamType, guid)
  _Battle_UpdateBGinfo()
  if not gUI.Battle.BattleTeamCount:IsVisible() then
    return
  end
  if _Battle_SelectType ~= _Battle_All and teamType ~= _Battle_SelectType then
    return
  end
  _Battle_UpdateByGuid(teamType, guid)
end
function UI_Battle_BGClick(msg)
  if gUI.Battle.BattleInfo:IsVisible() then
    gUI.Battle.BattleInfo:SetProperty("Visible", "false")
    gUI.Battle.BattleTitle:SetProperty("Height", "p" .. tostring(_Battle_BGHieghtClose))
  else
    gUI.Battle.BattleInfo:SetProperty("Visible", "true")
    gUI.Battle.BattleTitle:SetProperty("Height", "p" .. tostring(_Battle_BGHieghtOpen))
  end
end
function UI_Battle_ShowCount(msg)
  if not gUI.Battle.BattleTeamCount:IsVisible() then
    gUI.Battle.BattleTeamCount:SetProperty("Visible", "true")
    _Battle_ShowBattleCountAll()
  else
    gUI.Battle.BattleTeamCount:SetProperty("Visible", "false")
  end
end
function UI_Battle_LeaveBG()
  if gUI.Battle.BattleTeamCount:IsVisible() then
    gUI.Battle.BattleTeamCount:SetProperty("Visible", "false")
  end
  InstanceExit()
end
function UI_Battle_CloseCount()
  gUI.Battle.BattleTeamCount:SetProperty("Visible", "false")
  _Battle_SelTeam = 0
end
function UI_Battle_ShowTeam(msg)
  local wnd = msg:get_window()
  local userdata = wnd:GetCustomUserData(0)
  _Battle_SetShowTeam(userdata)
end
function Battle_ClosePVP()
  _Battle_PVPResult = -1
  gUI.Battle.BattleTeamContainer:DeleteAll()
  if gUI.Battle.BattleTeamCount:IsVisible() then
    gUI.Battle.BattleTeamCount:SetProperty("Visible", "false")
  end
  gUI.Battle.BattleUI:SetVisible(false)
end
function UI_Battle_SortTeam(msg)
  local wnd = msg:get_window()
  local sortType = wnd:GetCustomUserData(0)
  _Battle_SortIndex = sortType
  for i = 1, #_Battle_SortFlag do
    if i ~= sortType then
      _Battle_SortFlag[i] = 1
    end
  end
  if _Battle_SortFlag[sortType] == 0 then
    _Battle_SortFlag[sortType] = 1
  else
    _Battle_SortFlag[sortType] = 0
  end
  _Battle_UpdateTeamList(_Battle_SelectType)
end
function Script_Battle_OnEvent(event)
  if event == "SHOW_BATTLEGROUND" then
    _OnBattle_ShowBattle(arg1, arg2, arg3)
  elseif "GUILDCONTEST_RESULT" == event then
    _OnBattle_UpdateBGResult(arg1, arg2)
  elseif "BATTLE_PVE_NUMBER_CHANGE" == event then
    _OnBattle_NumberChange(arg1)
  elseif "BATTLE_PVE_UPDATE" == event then
    _OnBattle_UpdateInfo(arg1, arg2)
  end
end
function Script_Battle_OnLoad(event)
  gUI.Battle.BattleUI = WindowSys_Instance:GetWindow("BattleInfo")
  gUI.Battle.BattleTitle = WindowSys_Instance:GetWindow("BattleInfo.Inf_bg")
  gUI.Battle.BattleInfo = WindowSys_Instance:GetWindow("BattleInfo.Inf_bg.Image_bg")
  gUI.Battle.BGTime = WindowSys_Instance:GetWindow("BattleInfo.Inf_bg.Image_bg.Time_bg")
  gUI.Battle.ScoreBlue = WindowSys_Instance:GetWindow("BattleInfo.Inf_bg.Image_bg.Blue_dlab")
  gUI.Battle.ScoreRed = WindowSys_Instance:GetWindow("BattleInfo.Inf_bg.Image_bg.Red_dlab")
  gUI.Battle.BattleTeamCount = WindowSys_Instance:GetWindow("BattleCount")
  gUI.Battle.BattleTeamContainer = WindowToContainer(WindowSys_Instance:GetWindow("BattleCount.BattleCount_bg.Image_bg.Tem_cnt"))
  gUI.Battle.BattleTeamTemp = WindowSys_Instance:GetWindow("BattleCount.BattleCount_bg.Image_bg.Tem_bg")
  gUI.Battle.BattleTeamTime = WindowSys_Instance:GetWindow("BattleCount.BattleCount_bg.Down_bg.Time_dlab")
  gUI.Battle.BattleTeamTimeBg = WindowSys_Instance:GetWindow("BattleCount.BattleCount_bg.Down_bg.Time_slab")
  gUI.Battle.BattleReslut = WindowSys_Instance:GetWindow("BattleCount.BattleCount_bg.Down_bg.Score_dlab")
  gUI.Battle.BattleOpBtn = WindowSys_Instance:GetWindow("BattleCount.BattleCount_bg.Down_bg.Operate_btn")
  gUI.Battle.BattleLeaveBtn = WindowSys_Instance:GetWindow("BattleCount.BattleCount_bg.Down_bg.Leave_btn")
  gUI.Battle.BattleSplend = WindowSys_Instance:GetWindow("BattleCount.BattleCount_bg.Top_bg.Splend_slab")
  gUI.Battle.BattleBlueNum = WindowSys_Instance:GetWindow("BattleCount.BattleCount_bg.Down_bg.Blue_dlab")
  gUI.Battle.BattleRedNum = WindowSys_Instance:GetWindow("BattleCount.BattleCount_bg.Down_bg.Red_dlab")
  _Battle_BarPos = nil
end
leCount.BattleCount_bg.Down_bg.Red_dlab")
  _Battle_BarPos = nil
end
