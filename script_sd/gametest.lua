--打印级别
pt_lv_print = 0
pt_lv_debug = 1
pt_lv_warn = 2
pt_lv_error = 3
pt_lv_cur = pt_lv_debug
function mprint(lv,...)
	if lv > pt_lv_cur then return end--级别判断
	local sprint = "<--"
	for _,v in ipairs{...} do
		sprint = sprint..tostring(v).."  "
	end
	sprint = sprint.."-->"
	msk.print(sprint)
end
print = function (...) return mprint(pt_lv_debug,...) end
dstring = function (...) return mprint(pt_lv_debug,...) end

local PrintedTable = {}
function Printt(t,tname,fun)
	tname = tname
	if not t or PrintedTable[t] then return end
	PrintedTable[t] = true
	local pname
	if tname then 
		pname = tname.."."
	else
		pname = ""
	end
	for k,v in pairs(t) do
		if type(v) == "table" then
			Printt(v,pname..tostring(k))
			Printt( getmetatable(v) ,tname)
		elseif type(v) == "userdata" then
			Printt( getmetatable(v) ,tname)
		elseif fun and type(v) == "function" then
			print(pname..tostring(k))
		elseif not fun then
			local tv = v
			if type(tv) == "string" then tv = "\""..tv.."\"" end
			print(pname..tostring(k).." = "..tostring(tv))
		else
			print("skip "..pname..tostring(k))
		end
	end
end

local sysfun = {
  "assert",
  "collectgarbage",
  "dofile",
  "error",
  "getfenv",
  "getmetatable",
  "ipairs",
  "load","loadfile","loadstring","next","pairs",
  "pcall","print","rawget","rawequal","rawset","select",
  "setfenv","setmetatable","tonumber","tostring","type","unpack",
  "xpcall","IsBaseFun","mprint","dstring"
}

local skipFun={
	"GetTimeString","GetAcceptedQuestNum","GetPlaySelfProp","WindowTo%a+","String2Color","GetShowChannelConfig","GetNpcQuestList",
	"PetIsInFight","Lua_Tip[%a_]+","HookFunction2","IsSkipTable","IsSkipFun","UnHookFuncion2","HookAll","UnHookAll"
}

--UIInterface  WorldStage
local skipTable={
	"table","io","string","coroutine","package","math","os","debug","msk","socket","mime","mc_tools","lab2","UIConfig","WindowSys_Instance",
	"lab","AnimateManager_Instance","wnd","_Preview_LastStep1","RenderSys_Instance","_Trade_TradeNote_lbox","p","_RMBWnd_ScrollBar","ImeManager_Instance",
	"gFile","UILoadingBar","_RMBWnd_BuyWnd_New","TimerSys_Instance","win","gUI","hookedFun2","hookedFunf2","hookedFun","hookedFunf"
}

local filtertabel = {
	"cpp_private%.%.%?AV.+",
}

function IsSkipTable(name)
	for i=1,#skipTable do
		if skipTable[i] == name then
		  return true
		end
	end
	for i=1,#filtertabel do
		if string.find(name,filtertabel[i]) then
		  return true
		end
	end
  return false
end

function IsBaseFun(k,name)
	if k == "" then k = "_G" end
	if k ~= "_G" then return false end
	for i=1,#sysfun do
		if sysfun[i] == name then
		  return true
		end
	end
	return false
end

function IsSkipFun(tname,name)
	if IsBaseFun(tname,name) then return true end
	if tname == "" then tname = "_G" end
	if tname ~= "_G" then return false end
	for i=1,#skipFun do
		if string.find(name,skipFun[i]) then
		  return true
		end
	end
  return false
end

hookedFun2 = hookedFun2 or {}
hookedFunf2 = hookedFunf2 or {}
function HookFunction2(fname,hfun,isPre,t,tname)
	if not hfun then 
		print("hook failed hfun not set",fname)
		return 
	end
	if type(fname) == "string" and string.find(fname,"__%a+") then return end
	t = t or _G
	local oldfun = t[fname]
	if not oldfun or type(oldfun) ~= "function" then print("HookFunction failed not a fun",fname) return end
	tname = tname or ""
	if IsSkipFun(tname,fname) then return end
	UnHookFuncion2(fname,t)
	print("HookFunction2:",tostring(fname))
	local newhk = {}
	newhk = {t,fname,oldfun}
	if isPre then
		local newfun = hookedFunf2[oldfun]
		if not newfun then
			newfun = function(...)
				if hfun(...) then return end
				return oldfun(...)
			end
		end
		t[fname] = newfun
		newhk[4] = newfun
		hookedFunf2[oldfun] = newfun
	else
		local newfun = hookedFunf2[oldfun]
		if not newfun then
			newfun = function(...)
				local ret = {oldfun(...)}
				hfun(...)
				if ret then
					return unpack(ret)
				end
			end
		end
		t[fname] = newfun
		newhk[4] = newfun
		hookedFunf2[oldfun] = newfun
	end
	hookedFun2[#hookedFun2+1] = newhk
end

function UnHookFuncion2(name,t)
	t = t or _G
    if name then
		for i=1,#hookedFun2 do
			if hookedFun2[i][1] == t and hookedFun2[i][2] == name then
				print("suc unhook sig2",name)
				t[name] = hookedFun2[i][3]
				hookedFunf2[hookedFun2[i][3]] = nil
				table.remove(hookedFun2,i)
				return
			end
		end
		return
	end
	for i=1,#hookedFun2 do
		t = hookedFun2[i][1]
		name = hookedFun2[i][2]
		t[name] = hookedFun2[i][3]
		--print("suc unhook mul",name)
	end
	hookedFun2 = {}
	hookedFunf2 ={}
end

hookedt = hookedt or {}
function HookAll(t,tname,ftname)
	t = t or _G
	tname = tname or ""
	ftname = ftname or ""
	tname = tostring(tname)
	ftname = tostring(ftname)
	if hookedt[t] or IsSkipTable(tname) then return end
	print("hookt:"..ftname)
	hookedt[t] = true
	local fname
	for k,v in pairs(t) do
		fname = ftname..tostring(k)
		if type(v) == "table" then
			HookAll(v,k,fname..".")
		elseif type(v) == "userdata" then
			HookAll(getmetatable(v),k,fname..".")
		elseif type(v) == "function" then
			local newprint = function (...)
				if g_filterhook then
					UnHookFuncion2(k,t)
					return
				end
				print(k.."(",...,")")
			end
			HookFunction2(k,newprint,false,t,ftname)
		end
	end
end

function UnHookAll()
	UnHookFuncion2()
	hookedt = {}
end

g_filterhook = false
--UnHookFuncion2("cast",tolua)
HookFunction2("cast",function(arg,argname) Printt(arg.EVENTNAME) end,false,tolua,"tolua")
--HookFunction2("Update",function(...) print("Update(",...,")") end)
--print(type(Fishing))
--HookAll()
--UnHookAll()
--Printt(_G)
print("ok")

-- Printt(_G)
--local playerEnt = Utility.GetPlayerEntity()
--Printt(getmetatable(playerEnt:GetCharacterTitle()),"player")
--print("---------------------------------------")

--print(dt:AngleDeg())
--print(dt:Cross())

--[[
print(playerEnt:GetID())
print(playerEnt:GetDirection())--X() Y()
print(playerEnt:GetPosition())--X() Y() Z()
--print(playerEnt:GetUnit())


print(playerEnt:IsAlive())
print(playerEnt:GetCharacterLevel())
print(playerEnt:GetWeaponType())
print(playerEnt:GetCurSkillID())
print(playerEnt:IsPlayer())
print(playerEnt:GetCharacterName():c_str())
print(playerEnt:GetSex())
print(playerEnt:GetEntityType())
--print(playerEnt:SetEntityType())
print(playerEnt:GetNormalWeaponType())--结构未知
print(playerEnt:GetCharacterTitle())
--]]
