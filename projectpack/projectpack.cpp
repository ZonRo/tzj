// projectpack.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <windows.h>
#include "../lua514/lua.hpp"
#include "../MyRSA/MyRSA.h"
using namespace CryptoPP;
#ifdef _DEBUG
#pragma comment(lib, "../Crypto++/cryptlib_debug.lib")
#else
#pragma comment(lib, "../Crypto++/cryptlib.lib")
#endif

#define DllImport   __declspec( dllimport )
#define DllExport   extern "C" __declspec( dllexport )
MyRSA rsa;
int Crypt(lua_State *L)
{
	const char *data = lua_tostring(L, 1);
	string ret = rsa.Encrypt(lua_tostring(L, 2), data);
	lua_pushlstring(L, ret.data(), ret.size());
	return 1;
}

int GenKey(lua_State *L)
{
	rsa.GenerateRSAKey(lua_tonumber(L, 1), lua_tostring(L, 2), lua_tostring(L, 3));
	return 0;
}

int Xor(lua_State *L)
{
	size_t len;
	const char *data = lua_tolstring(L, 1, &len);
	byte *newbuf = new byte[len+1];
	byte num = (byte)lua_tonumber(L, 2);
	newbuf[0] = num ^ 0xff;
	for (int i = 0; i < len; ++i){
		newbuf[i+1] = data[i] ^ num;
	}
	lua_pushlstring(L, (char *)newbuf,len+1);
	delete[]newbuf;
	return 1;
}

char* lmc_decodesp(char *data, size_t len)
{
	byte num = *data;
	num = num ^ 0xff;
	byte *newdata = new byte[len - 1];
	for (int i = 0; i < len - 1; ++i){
		newdata[i] = data[i + 1] ^ num;
	}
	delete[]data;
	return (char *)newdata;
}


int getfile(lua_State *L)
{
	size_t sz;
	const char *data = lua_tolstring(L, 1, &sz);
	char* ret = lmc_decodesp((char *)data, sz);
	lua_pushlstring(L, ret, sz-1);
	lua_pushinteger(L, sz-1);
	delete[]ret;
	return 2;
}


static const luaL_Reg mskfuncs[] = {
	{ "crypt", Crypt },
	{ "key", GenKey },
	{ "getfile", getfile },
	{ "xor", Xor },
	{ NULL, NULL }
};

DllExport int luaopen_cryptodll(lua_State *L)
{
	luaL_register(L, "msk", mskfuncs);
	return 1;
}

int _tmain(int argc, _TCHAR* argv[])
{
	lua_State *L = luaL_newstate();
	luaL_openlibs(L);
	lua_register(L, "crypt", Crypt);
	int ret = luaL_dofile(L, "G:/bitbuket/tg_dll/aide/pack.lua");
	if (ret != 0){
		printf("lua error:%s", lua_tostring(L, -1));
	}
	lua_close(L);
	system("pause");
	return 0;
}

