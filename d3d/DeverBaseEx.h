// DeverBaseEx.h: interface for the CDeverBase class.
//
//////////////////////////////////////////////////////////////////////
#ifndef  DEVERBASE_H
#define DEVERBASE_H
//////////////////////////////////////////////////////////////////////////
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//////////////////////////////////////////////////////////////////////////
#include "InlineHook.h"
//////////////////////////////////////////////////////////////////////////
#include <afxtempl.h>
#include <d3dx9shader.h>
#pragma comment (lib,"ddraw.lib")
#pragma comment (lib,"dxguid.lib")

#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")
#include <d3d9.h>
#include <d3dx9.h>
//////////////////////////////////////////////////////////////////////////

// extern  D3DDISPLAYMODE         d3ddm; //D3D显示模式
// extern  D3DPRESENT_PARAMETERS  d3dpp; //D3D图像显示方法
// extern  LPDIRECT3D9            g_pD3D; //D3D对象
// extern  LPDIRECT3DDEVICE9      g_pDev; //D3D设备
// extern  LPDIRECT3DDEVICE9      g_Game_pDev;//游戏的的D3D设备
// extern	LPD3DXMESH             g_pMesh;
// extern  LPDIRECT3DTEXTURE9    g_pTexture;
// extern  LPD3DXFONT            g_pfont;
// extern  LPD3DXSPRITE          g_d3dSprite;
// extern  HMODULE               g_hD3dDll;
//////////////////////////////////////////////////////////////////////////
typedef struct _D3DGAMEBASE
{
	DWORD DrawRectPatch_off;
	DWORD DrawPrimitive_off;
	DWORD DrawIndexedPrimitive_off;
	DWORD DrawPrimitiveUP_off;
	DWORD DrawIndexedPrimitiveUP_off;
	DWORD DrawTriPatch_off;
	DWORD Present_off;
    DWORD EndScene_off;
    DWORD DrawSubset_off;
	DWORD GameRun_off;
}D3DGAMEBASE,*LPD3DGAMEBASE;
//////////////////////////////////////////////////////////////////////////
bool WINAPI Start3DBlack(HWND hWnd);
void WINAPI End3DBlack();
void WINAPI InitD3dData();
//////////////////////////////////////////////////////////////////////////
class CDeverBase  
{
public:
	static DWORD GetOutFunAddres(CStringA D3dFunName);
	BOOL InitD3DXSprite();
	static CStringA GetGameD3dDllName();
	BOOL InitLight();
	BOOL InitTexture();
	BOOL InitMesh();
	BOOL Init3DDever(HWND hWnd);
	BOOL InitFont();
	CDeverBase();
	virtual ~CDeverBase();

};
//////////////////////////////////////////////////////////////////////////
class CDeverBaseHook
{
public:
	CDeverBaseHook();
	virtual ~CDeverBaseHook();
	//////////////////////////////////////////////////////////////////////////
public:
	void Init(DWORD pDev);
	void UnHook();
	void SetAllHook();
	DWORD GetBaseFunAddr(DWORD Dev_off);
   static  void disableAll(DWORD lpDeve);
	static void Naked_SetRenderState(LPDIRECT3DDEVICE9 pDev,D3DRENDERSTATETYPE log,DWORD val);
	static void naked_Present();
	static void naked_SetSamplerState();
	static void naked_DrawRectPatch();
	static void naked_DrawPrimitive();
	static void naked_DrawIndexedPrimitive();
	static void naked_DrawPrimitiveUP();
	static void naked_DrawIndexedPrimitiveUP();
	static void naked_DrawTriPatch();
	static void naked_EndScene(LPDIRECT3DDEVICE9 pDev);
	static void naked_LightEnable();
	static void naked_SetLight();
	static void naked_SetTexture();
	static void MySetRenderState(DWORD lpDeve,DWORD log,DWORD val);
	static void nakde_SetTransform();
	static void naked_Rese();
	 static void naked_SetStreamSource();
protected:
 	DWORD m_Dev;
	InlineHook SetSamplerState_hook;
	InlineHook DrawRectPatch_hook;
	InlineHook DrawPrimitive_hook;
	InlineHook DrawIndexedPrimitive_hook;
	InlineHook DrawPrimitiveUP_hook;
	InlineHook DrawIndexedPrimitiveUP_hook;
	InlineHook DrawTriPatch_hook;
	InlineHook Present_hook;
    InlineHook SetRenderState_hook;
    InlineHook EndScene_hook;
    InlineHook SetLight_hook;
    InlineHook LightEnable_hook;
	InlineHook SetTexture_hook;
	InlineHook SetTransform_hook;
	InlineHook Rese_hook;
	InlineHook SetStreamSource_hook;
};
//////////////////////////////////////////////////////////////////////////
class  CMeshFunHook
{
public:
	CMeshFunHook();
	virtual~CMeshFunHook();
	//////////////////////////////////////////////////////////////////////////
public:
	DWORD GetMeshFunAddr(DWORD off);
	void UnHook();
	void SetAllHook();
	void Init(DWORD pMesh);
    static void	naked_DrawSubset();
	static void naked_OptimizeInplace();
	static void naked_Optimize();
	//////////////////////////////////////////////////////////////////////////
	static void naked_D3DXLoadMeshFromXA(
		LPCSTR pFilename, 
        DWORD Options, 
        LPDIRECT3DDEVICE9 pD3DDevice, 
        LPD3DXBUFFER *ppAdjacency,
        LPD3DXBUFFER *ppMaterials, 
        LPD3DXBUFFER *ppEffectInstances, 
        DWORD *pNumMaterials,
        LPD3DXMESH *ppMesh);//32,
	//////////////////////////////////////////////////////////////////////////
	static void naked_D3DXLoadMeshFromXInMemory(     
	LPCVOID Memory,
        DWORD SizeOfMemory,
        DWORD Options, 
        LPDIRECT3DDEVICE9 pD3DDevice, 
        LPD3DXBUFFER *ppAdjacency,
        LPD3DXBUFFER *ppMaterials, 
        LPD3DXBUFFER *ppEffectInstances, 
        DWORD *pNumMaterials,
        LPD3DXMESH *ppMesh);//36,
	//////////////////////////////////////////////////////////////////////////
	
	static void naked_D3DXLoadMeshFromXResource(
		 HMODULE Module,
        LPCSTR Name,
        LPCSTR Type,
        DWORD Options, 
        LPDIRECT3DDEVICE9 pD3DDevice, 
        LPD3DXBUFFER *ppAdjacency,
        LPD3DXBUFFER *ppMaterials, 
        LPD3DXBUFFER *ppEffectInstances, 
        DWORD *pNumMaterials,
        LPD3DXMESH *ppMesh);//40,
	//////////////////////////////////////////////////////////////////////////
	static void naked_D3DXLoadMeshFromXW(  
		LPCWSTR pFilename, 
        DWORD Options, 
        LPDIRECT3DDEVICE9 pD3DDevice, 
        LPD3DXBUFFER *ppAdjacency,
        LPD3DXBUFFER *ppMaterials, 
        LPD3DXBUFFER *ppEffectInstances, 
        DWORD *pNumMaterials,
        LPD3DXMESH *ppMesh);//32,
	//////////////////////////////////////////////////////////////////////////
	static void naked_D3DXLoadMeshFromXof( 
		LPD3DXFILEDATA pxofMesh, 
        DWORD Options, 
        LPDIRECT3DDEVICE9 pD3DDevice, 
        LPD3DXBUFFER *ppAdjacency,
        LPD3DXBUFFER *ppMaterials, 
        LPD3DXBUFFER *ppEffectInstances, 
        DWORD *pNumMaterials,
        LPD3DXMESH *ppMesh);//32,
	//////////////////////////////////////////////////////////////////////////
	
	static void naked_D3DXLoadPatchMeshFromXof(
	    LPD3DXFILEDATA pXofObjMesh,
        DWORD Options,
        LPDIRECT3DDEVICE9 pD3DDevice,
        LPD3DXBUFFER *ppMaterials,
        LPD3DXBUFFER *ppEffectInstances, 
        PDWORD pNumMaterials,
        LPD3DXPATCHMESH *ppMesh);//28,
	//////////////////////////////////////////////////////////////////////////
	static void naked_D3DXLoadSkinMeshFromXof(  
        LPD3DXFILEDATA pxofMesh, 
        DWORD Options,
        LPDIRECT3DDEVICE9 pD3DDevice,
        LPD3DXBUFFER* ppAdjacency,
        LPD3DXBUFFER* ppMaterials,
        LPD3DXBUFFER *ppEffectInstances, 
        DWORD *pMatOut,
        LPD3DXSKININFO* ppSkinInfo,
        LPD3DXMESH* ppMesh);//36,
	//////////////////////////////////////////////////////////////////////////
protected:
	DWORD m_pmesh;
    InlineHook DrawSubset_hook;
	InlineHook OptimizeInplace_hook;
	InlineHook Optimize_hook;
	////////////////////////////////////////////////////////////////////////////
	InlineHook D3DXLoadMeshFromXA_hook;//32,
	InlineHook D3DXLoadMeshFromXInMemory_hook;//36,
	InlineHook D3DXLoadMeshFromXResource_hook;//40,
	InlineHook D3DXLoadMeshFromXW_hook;//32,
	InlineHook D3DXLoadMeshFromXof_hook;//32,
	InlineHook D3DXLoadPatchMeshFromXof_hook;//28,
	InlineHook D3DXLoadSkinMeshFromXof_hook;//36,
	
};
//Mesh->OptimizeInplace(D3DXMESHOPT_VERTEXCACHE, pAdj,NULL, NULL, NULL);
//////////////////////////////////////////////////////////////////////////
class CFontFunHook
{
public:
	void UnHook();
	void SetAllHook();
	DWORD GetFontFunAddr(DWORD off);
	void Init(DWORD pFont);
	CFontFunHook();
	virtual ~CFontFunHook();
	static void naked_DrawText();
protected:
	DWORD m_pFont;
	InlineHook DrawText_hook;	
};
//////////////////////////////////////////////////////////////////////////
class CD3DXSpriteHook
{
public:
	CD3DXSpriteHook();
	virtual ~CD3DXSpriteHook();
	static void naked_Draw();
	static void naked_SetWorldViewRH();
    static void naked_SetWorldViewRL();
	void Init(DWORD pSprite);
	void SetAllHook();
	void UnHook();
	DWORD GetSpriteFunAddr(DWORD off);

protected:
	DWORD m_pSprite;
	InlineHook Draw_hook;
    InlineHook SetWorldViewRH_hook;
    InlineHook SetWorldViewLH_hook;

};
//////////////////////////////////////////////////////////////////////////
class CD3dTextureHook
{
public:
	static void nakde_D3DXCreateTextureFromResourceA( 
		LPDIRECT3DDEVICE9         pDevice,
        HMODULE                   hSrcModule,
        LPCSTR                    pSrcResource,
        LPDIRECT3DTEXTURE9*       ppTexture);
	//////////////////////////////////////////////////////////////////////////
	static void naked_D3DXCreateTextureFromFileInMemory(
		LPDIRECT3DDEVICE9    pDevice,
		LPCVOID   pSrcData,
		UINT    SrcDataSize,
		LPDIRECT3DTEXTURE9* ppTexture);
	//////////////////////////////////////////////////////////////////////////
	static  void naked_D3DXCreateTextureFromFile(
		LPDIRECT3DDEVICE9 pDevice,
		LPCSTR    pSrcFile,
		LPDIRECT3DTEXTURE9*    ppTexture);
	//////////////////////////////////////////////////////////////////////////
	void UnHook();
	void Init();
	void SetAllHook();
    CD3dTextureHook();
	virtual ~CD3dTextureHook();
protected:
	HMODULE hD3d;
	InlineHook D3DXCreateTextureFromFile_hook;

	InlineHook D3DXCreateTextureFromFileInMemory_hook;
	InlineHook D3DXCreateTextureFromResourceA_hook;
	
};
class CGameMeshFileEx
{
public:
	static BOOL IsMeshFile(void* lpReadFileBuf);
	static void OldReadFile(
		HANDLE hFile, 
		LPVOID lpBuffer, 
		DWORD nNumberOfBytesToRead,
		LPDWORD lpNumberOfBytesRead,
		LPOVERLAPPED lpOverlapped);
	void UnHook();
	void SetAllHook();
	void Init();
	static void naked_ReadFile( 
		HANDLE hFile, 
		LPVOID lpBuffer, 
		DWORD nNumberOfBytesToRead,
		LPDWORD lpNumberOfBytesRead,
		LPOVERLAPPED lpOverlapped );
	CGameMeshFileEx()
	{
		
	}
	virtual ~CGameMeshFileEx()
	{

	}
protected:
	InlineHook ReadMeshFile_hook;
};
//////////////////////////////////////////////////////////////////////////
#endif // 
