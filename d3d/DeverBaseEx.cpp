// DeverBase.cpp: implementation of the CDeverBase class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "../function/function.h"
#include "DeverBaseEx.h"
#include <TLHELP32.H>
#ifdef UNICODE
#undef UNICODE
#endif
//////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
static DWORD  SetRenderState_Jmp=0;
static DWORD  SetRenderState_disable=0;
static DWORD  EndScene_jmp=0;
static DWORD  DrawText_jmp=0;
static int	  aa=0;
static  D3DGAMEBASE g_3DGameData;
static DWORD  ReadFile_jmp=0;
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//BeginScene地址=[[创建的D3D设备对象]+0XAC]
//EndScene地址=[[创建的D3D设备对象]+0XA8]
//极它的依次类推
//////////////////////////////////////////////////////////////////////
static D3DDISPLAYMODE         d3ddm; //D3D显示模式
static D3DPRESENT_PARAMETERS  d3dpp; //D3D图像显示方法
static LPDIRECT3D9EX            g_pD3D=NULL; //D3D对象

///////////////////////////////////////////////////////////////////
static LPDIRECT3DDEVICE9EX      g_pDev=NULL;      //D3D设备
static LPDIRECT3DDEVICE9EX      g_Game_pDev=NULL; //游戏的的D3D设备
static LPD3DXMESH             g_pMesh=NULL;     //模型函数指针
static LPDIRECT3DTEXTURE9     g_pTexture=NULL;  //纹理指针
static LPD3DXFONT			 g_pfont = 0;       //字体指针
static LPD3DXSPRITE          g_d3dSprite=NULL;
static HMODULE               g_hD3dDll=NULL;
//////////////////////////////////////////////////////////////////////////
static CDeverBaseHook		Basehook;
static CMeshFunHook			MeshHook;
static CDeverBase			base;
static CFontFunHook			FontHook;
static CD3DXSpriteHook		SpriteHook;
static CD3dTextureHook		TextureHook;
//////////////////////////////////////////////////////////////////////////
void WINAPI InitD3dData()
{
	memset((void*)&g_3DGameData,0,sizeof(g_3DGameData));
	g_3DGameData.DrawRectPatch_off=0x1cc ;
	g_3DGameData.DrawPrimitive_off= 0x144;
	g_3DGameData.DrawIndexedPrimitive_off=0x148 ;
	g_3DGameData.DrawPrimitiveUP_off= 0x14c;
	g_3DGameData.DrawIndexedPrimitiveUP_off=0x150 ;
	g_3DGameData.DrawTriPatch_off=0x1d0 ;
	g_3DGameData.Present_off=0x44 ;
    g_3DGameData.EndScene_off=0xa8 ;
    g_3DGameData.DrawSubset_off=0x0c;
	g_3DGameData.GameRun_off=0x004087A9;

}
//////////////////////////////////////////////////////////////////////////
bool WINAPI Start3DBlack(HWND hWnd)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	//return false;
	//return false;
	if (FALSE == IsWindow(hWnd))
	{
		printDebugMessage("错误 试图HOOK一个无效的窗口");
		return false;
	}
	CStringA dllPath=CDeverBase::GetGameD3dDllName();
	printDebugMessage("InitD3dData");
    InitD3dData();
	if(!dllPath.IsEmpty())
	{
		printDebugMessage("GetModuleHandleA");
		g_hD3dDll=GetModuleHandleA(dllPath);
	}
	if (hWnd==NULL)
	{

		OutputDebugStringA("msk_ffo: d3d error 句柄不能为空");
		return false;
	}
	if (g_pDev==NULL)
	{ 
		if (!base.Init3DDever(hWnd))
		{
			OutputDebugStringA("msk_ffo: d3d error Init3DDever error");
			return false;
		}
	}
	printDebugMessage("Init");
	Basehook.Init((DWORD)g_pDev);
	printDebugMessage("SetAllHook");
	Basehook.SetAllHook();
	printDebugMessage("InitMesh");
	base.InitMesh();
	if (g_pMesh)
	{
		MeshHook.Init((DWORD)g_pMesh);
		MeshHook.SetAllHook();
	}
	base.InitTexture();
	if (g_pTexture)
	{
		TextureHook.Init();
		TextureHook.SetAllHook();
	}
	base.InitFont();
	if (g_pfont)
	{
		FontHook.Init((DWORD)g_pfont);
		FontHook.SetAllHook();
	}
	base.InitD3DXSprite();
	if(g_d3dSprite)
	{

		SpriteHook.Init((DWORD)g_d3dSprite);
		SpriteHook.SetAllHook();
	}
	return true;
}
void WINAPI End3DBlack()
{
	//return;
	Basehook.UnHook();
	MeshHook.UnHook();
	FontHook.UnHook();
	SpriteHook.UnHook();
	TextureHook.UnHook();
}
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
CDeverBase::CDeverBase()
{
}

CDeverBase::~CDeverBase()
{
	
}

BOOL CDeverBase::Init3DDever(HWND hWnd)
{
	HRESULT hr = E_FAIL;
	IDirect3D9Ex * pD3D = NULL;
	IDirect3DDevice9Ex * pDevice = NULL;
	// Create the D3D object, which is needed to create the D3DDevice.
	if(FAILED(hr = Direct3DCreate9Ex( D3D_SDK_VERSION, &pD3D )))
	{
		g_pD3D = NULL;
		return FALSE;
	}

	g_pD3D = pD3D;
	// Set up the structure used to create the D3DDevice. 
	D3DPRESENT_PARAMETERS d3dpp; 
	ZeroMemory( &d3dpp, sizeof(d3dpp) );
	d3dpp.Windowed = TRUE;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;

	// Create the Direct3D device. 
	if( FAILED( hr = pD3D->CreateDeviceEx( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING,
		&d3dpp, NULL, &pDevice ) ) )

	{
		g_pDev = NULL;
		return hr;
	}

	// Device state would normally be set here
	g_pDev = pDevice;
	return (BOOL)g_pDev;//g_pD3D
}
BOOL CDeverBase::InitMesh()
{
	D3DXCreateTeapot(g_pDev, &g_pMesh, 0);
	return (BOOL)g_pMesh;
}

BOOL CDeverBase::InitTexture()
{
// 操控资源
// ________________________________________
// 为了渲染场景，应用程序需要操控资源。首先，应用程序用以下方法创建纹理资源。
// "	IDirect3DDevice9::CreateCubeTexture 
// "	IDirect3DDevice9::CreateTexture 
// "	IDirect3DDevice9::CreateVolumeTexture 
// 由纹理创建方法返回的纹理对象是表面或立体纹理的容器，这些容器一般被称为缓存
// 。资源拥有的缓存在继承资源的用途、格式和池的同时也有自己的类型。更多信息请参阅资源属性。
// 为了载入图片，应用程序应该调用以下方法，得到对所包含的表面的访问权。更多细节请参阅锁定资源。
// "	IDirect3DCubeTexture9::LockRect 
// "	IDirect3DTexture9::LockRect 
// "	IDirect3DVolumeTexture9::LockBox 
// lock方法接收的参数指出了所包含的表面--例如，纹理的mipmap sub-level或立方体
// 纹理的表面--并返回指向像素的指针。一般的应用程序从不直接使用表面对象。
// 另外，应用程序用以下方法创建面向几何体的资源。
// "	IDirect3DDevice9::CreateIndexBuffer 
// "	IDirect3DDevice9::CreateVertexBuffer 
// 应用程序通过调用以下方法锁定并填充缓存资源。
// "	IDirect3DIndexBuffer9::Lock 
// "	IDirect3DVertexBuffer9::Lock 
// 如果应用程序允许Microsoft? Direct3D?运行库管理这些资源，那么资源创建过程就到此为止。
// 否则，应用程序还要通过调用IDirect3DDevice9::UpdateTexture方法，负责把系统资源提升
// 为设备可访问的资源，这样硬件加速器就可以使用它们。
// 要呈现(present)从资源渲染得到的图像，应用程序还需要颜色和深度/模板缓存。
// 对于一般的应用程序，颜色缓存属于设备的交换链，交换链是一个后缓存表面的集合，由设备隐式地 

	D3DXCreateTexture(g_pDev,30,30,1,1,D3DFMT_L8,D3DPOOL_DEFAULT,&g_pTexture);
	return (BOOL)g_pTexture;
	
}
BOOL CDeverBase::InitFont()
{
	LOGFONTA lf;
	ZeroMemory(&lf, sizeof(LOGFONTA));
	lf.lfHeight = 25; // in logical units
	lf.lfWidth = 12; // in logical units
	lf.lfWeight = 500; // boldness, range 0(light) - 1000(bold)
	lf.lfItalic = false;
	lf.lfUnderline = false;
	lf.lfStrikeOut = false;
	lf.lfCharSet = DEFAULT_CHARSET;
	strcpy(lf.lfFaceName, "Times New Roman"); // font style
	D3DXCreateFontIndirect(g_pDev,(CONST D3DXFONT_DESC*)&lf, &g_pfont);
	return(BOOL)g_pfont;
}



BOOL CDeverBase::InitLight()
{
	
	return true;
}
//////////////////////////////////////////////////////////////////////////
DWORD CDeverBase::GetOutFunAddres(CStringA D3dFunName)
{
	DWORD dwRet=0;
	if (g_hD3dDll)
	{
		dwRet=(DWORD)GetProcAddress(g_hD3dDll,D3dFunName);
	}
	return dwRet;
}
CStringA CDeverBase::GetGameD3dDllName()
{
	HANDLE hSnap = INVALID_HANDLE_VALUE; 
	CStringA msg,strRet="",tmp;
	MODULEENTRY32 te32; 
	hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE,::GetCurrentProcessId()); 
	if( hSnap == INVALID_HANDLE_VALUE ) 
		return strRet; 
	te32.dwSize = sizeof(MODULEENTRY32 ); 
	if( !Module32First( hSnap, &te32 ) ) 
	{
		CloseHandle( hSnap ); 
		return strRet;
	}
	do 
	{ 
		msg = CStringA(te32.szModule);
		tmp=msg.Left(5);
		if (msg.GetLength()>8&&(tmp=="d3dx9"||tmp=="d3dx8"||tmp=="d3dx7")&&msg.Right(3)=="dll")
		{
			strRet.Format("%s",te32.szExePath);
			break;
		}
	} while(Module32Next(hSnap, &te32 )); 
	CloseHandle(hSnap);
	return strRet;
}
BOOL CDeverBase::InitD3DXSprite()
{
	D3DXCreateSprite(g_pDev,&g_d3dSprite);
	return(BOOL)g_d3dSprite;
}
//////////////////////////////////////////////////////////////////////////
//
//
//
//////////////////////////////////////////////////////////////////////////
CDeverBaseHook::CDeverBaseHook()
{
	m_Dev=0;
	memset((void*)&DrawRectPatch_hook,0,sizeof(InlineHook));
	memset((void*)&DrawPrimitive_hook,0,sizeof(InlineHook));
	memset((void*)&DrawIndexedPrimitive_hook,0,sizeof(InlineHook));
	memset((void*)&DrawPrimitiveUP_hook,0,sizeof(InlineHook));
	memset((void*)&DrawIndexedPrimitiveUP_hook,0,sizeof(InlineHook));
	memset((void*)&DrawTriPatch_hook,0,sizeof(InlineHook));
	memset((void*)&Present_hook,0,sizeof(InlineHook));
	memset((void*)&SetRenderState_hook,0,sizeof(InlineHook));
	
	memset((void*)&SetLight_hook,0,sizeof(InlineHook));
	memset((void*)&LightEnable_hook,0,sizeof(InlineHook));
	memset((void*)&SetTexture_hook,0,sizeof(InlineHook));
	memset((void*)&SetTransform_hook,0,sizeof(InlineHook));
	memset((void*)&Rese_hook,0,sizeof(InlineHook));
	memset((void*)&SetSamplerState_hook,0,sizeof(InlineHook));
	memset((void*)&SetStreamSource_hook,0,sizeof(InlineHook));
	
	
	
}
CDeverBaseHook::~CDeverBaseHook()
{
	UnHook();
}
//////////////////////////////////////////////////////////////////////////
NAKED void CDeverBaseHook::nakde_SetTransform()
{
	ASM
	{
		xor eax,eax;
		ret 0x0c;
	}
}
NAKED void CDeverBaseHook::naked_SetSamplerState()
{
	ASM
	{
		xor eax,eax;
		ret 16;
	}
}
NAKED void CDeverBaseHook::naked_Rese()
{
	ASM
	{
		xor eax,eax;
		ret 0x08;
	}
}

NAKED void CDeverBaseHook::naked_Present()
{

	ASM
	{
		xor eax,eax;
		ret 0x14;
	}
}
//////////////////////////////////////////////////////////////////////////
NAKED void CDeverBaseHook::MySetRenderState(DWORD lpDeve,DWORD log,DWORD val)
{
	ASM
	{
		mov edi,edi;
		push ebp;
		mov ebp,esp;
		mov eax,SetRenderState_Jmp;
		add eax,5;
		jmp eax;
	}
}
void CDeverBaseHook::disableAll(DWORD lpDeve)
{
	CDeverBaseHook::MySetRenderState(lpDeve,D3DRS_SPECULARENABLE,0);
	CDeverBaseHook::MySetRenderState(lpDeve,D3DRS_LIGHTING,0);      //禁用灯光
}
NAKED void CDeverBaseHook::Naked_SetRenderState(LPDIRECT3DDEVICE9 lpDeve,D3DRENDERSTATETYPE log,DWORD val)
{
	ASM
	{
		xor eax,eax;
		ret  0x0c;
		mov edi,edi;
		push ebp;
		mov ebp,esp;
		pushad;
	}
	if (SetRenderState_Jmp==0)		goto PassRet;
	if(SetRenderState_disable==0&&lpDeve!=NULL&&lpDeve!=g_pDev)
	{
		SetRenderState_disable=1;
        disableAll((DWORD)lpDeve);
	}
	//if (log==true)					goto PassRet;
	if (log==D3DRS_SPECULARENABLE && val==true)  goto PassRet;
	if( log==D3DRS_LIGHTING  && val==true)      goto PassRet;
	ASM
	{
		popad;
		pop ebp;
		mov eax,SetRenderState_Jmp;
		add eax,5;
		jmp eax;
	}
	//////////////////////////////////////////////////////////////////////////
PassRet:
	ASM
	{
		xor eax,eax;
		ret 0xc;
	}
}
NAKED void CDeverBaseHook::naked_DrawRectPatch()
{
	ASM
	{
		xor eax,eax;
		ret 0x10;
	}
}
NAKED void CDeverBaseHook::naked_LightEnable()
{
	ASM
	{
		xor eax,eax;
		ret 0x0c;
	}
}
NAKED void CDeverBaseHook::naked_SetLight()
{
	ASM
	{
		xor eax,eax;
		ret 0x0c;
	}
	
}
NAKED void CDeverBaseHook::naked_SetTexture()
{
	ASM
	{
		xor eax,eax;
		ret 0x0c;
	}
}
NAKED void CDeverBaseHook::naked_DrawPrimitive()
{
	ASM
	{
		mov edi,edi;
		push ebp;
		mov ebp,esp;
	}
	if (aa>10 && g_dwSleepTime)
	{
		aa=0;
		Sleep(8);
	}
	aa++;
	ASM
	{
		mov esp,ebp;
		pop ebp;
		xor eax,eax;
		ret 0x10;
	}
}
NAKED void CDeverBaseHook::naked_DrawIndexedPrimitive()
{
	
	ASM
	{
		xor eax,eax;
		ret 0x1c;
	}
}
NAKED void CDeverBaseHook::naked_DrawPrimitiveUP()
{
	ASM
	{
		xor eax,eax;
		ret 0x14;
	}
}
NAKED void CDeverBaseHook::naked_DrawIndexedPrimitiveUP()
{
	ASM
	{
		xor eax,eax;
		ret 0x24;
	}
}
NAKED void CDeverBaseHook::naked_DrawTriPatch()
{
	ASM
	{
		xor eax,eax;
		ret 0x10;
	}
}
NAKED void CDeverBaseHook::naked_EndScene(LPDIRECT3DDEVICE9 pDev)
{
	ASM
	{
		mov edi,edi;
		push ebp;
		mov ebp,esp;
		pushad;
	}
	
	if (pDev!=g_pDev)
	{
		if (g_Game_pDev==NULL)
		{
			g_Game_pDev=(LPDIRECT3DDEVICE9EX)pDev;
		}
		//pDev->Clear(0, 0, D3DCLEAR_ZBUFFER|D3DCLEAR_TARGET|D3DCLEAR_STENCIL, 0, 0.8f, 0);
	}
	ASM{
		popad;
		mov eax,EndScene_jmp;
		Add  eax,5;
		jmp eax;
	}
}
NAKED void CDeverBaseHook::naked_SetStreamSource()
{
	ASM
	{
		xor eax,eax;
		ret 20;
	}
}
//////////////////////////////////////////////////////////////////////////
void CDeverBaseHook::SetAllHook()
{
	
	CInlineHook::SetInlineHook(&SetTexture_hook);
	CInlineHook::SetInlineHook(&DrawRectPatch_hook);
	CInlineHook::SetInlineHook(&DrawPrimitive_hook);
	CInlineHook::SetInlineHook(&DrawIndexedPrimitive_hook);
	CInlineHook::SetInlineHook(&DrawPrimitiveUP_hook);
	CInlineHook::SetInlineHook(&DrawIndexedPrimitiveUP_hook);
	CInlineHook::SetInlineHook(&DrawTriPatch_hook);
	CInlineHook::SetInlineHook(&Present_hook);
	CInlineHook::SetInlineHook(&EndScene_hook);
	CInlineHook::SetInlineHook(&SetLight_hook);
	CInlineHook::SetInlineHook(&LightEnable_hook);
	CInlineHook::SetInlineHook(&SetRenderState_hook);
	CInlineHook::SetInlineHook(&SetTransform_hook);
	CInlineHook::SetInlineHook(&Rese_hook);
	CInlineHook::SetInlineHook(&SetSamplerState_hook);
	//CInlineHook::SetInlineHook(&SetStreamSource_hook);

		g_pDev->SetStreamSource( 0, NULL, 0,10 );
	  //g_pDev->SetTransform(D3DTS_VIEW,&V);
 	g_pDev->Present(0,0,0,0);
 	g_pDev->DrawRectPatch(NULL,NULL,NULL);
 	g_pDev->DrawPrimitive(D3DPT_POINTLIST,0,0);
 	g_pDev->DrawIndexedPrimitive(D3DPT_POINTLIST,0,0,0,0,0);
	g_pDev->DrawPrimitiveUP(D3DPT_POINTLIST,0,0,0);
 	g_pDev->DrawIndexedPrimitiveUP(D3DPT_POINTLIST,0,0,0,0,D3DFMT_L16,0,0);
 	g_pDev->DrawRectPatch(0,0,0);
	 	g_pDev->DrawTriPatch(0,0,0);
	g_pDev->EndScene();
}
void CDeverBaseHook::UnHook()
{ 
	CInlineHook::TestHookVlaue(&SetTexture_hook);
	CInlineHook::TestHookVlaue(&DrawRectPatch_hook);
	CInlineHook::TestHookVlaue(&DrawPrimitive_hook);
	CInlineHook::TestHookVlaue(&DrawIndexedPrimitive_hook);
	CInlineHook::TestHookVlaue(&DrawPrimitiveUP_hook);
    CInlineHook::TestHookVlaue(&DrawIndexedPrimitiveUP_hook);
	CInlineHook::TestHookVlaue(&DrawTriPatch_hook);
	CInlineHook::TestHookVlaue(&Present_hook);  
	CInlineHook::TestHookVlaue(&SetRenderState_hook);
	CInlineHook::TestHookVlaue(&EndScene_hook);
	CInlineHook::TestHookVlaue(&SetLight_hook);
	CInlineHook::TestHookVlaue(&LightEnable_hook);
	CInlineHook::TestHookVlaue(&SetTransform_hook);
	CInlineHook::TestHookVlaue(&Rese_hook);
	
	
	CInlineHook::TestHookVlaue(&SetStreamSource_hook);
	CInlineHook::UnInlineHook(&SetStreamSource_hook);
	
	CInlineHook::TestHookVlaue(&SetSamplerState_hook);
	CInlineHook::UnInlineHook(&SetSamplerState_hook);
	
	CInlineHook::UnInlineHook(&Rese_hook);
	CInlineHook::UnInlineHook(&SetTransform_hook);
	CInlineHook::UnInlineHook(&SetTexture_hook);
	CInlineHook::UnInlineHook(&DrawRectPatch_hook);
	CInlineHook::UnInlineHook(&DrawPrimitive_hook);
	CInlineHook::UnInlineHook(&DrawIndexedPrimitive_hook);
	CInlineHook::UnInlineHook(&DrawPrimitiveUP_hook);
	CInlineHook::UnInlineHook(&DrawIndexedPrimitiveUP_hook);
	CInlineHook::UnInlineHook(&DrawTriPatch_hook);
	CInlineHook::UnInlineHook(&Present_hook);
	CInlineHook::UnInlineHook(&SetRenderState_hook);
	CInlineHook::UnInlineHook(&EndScene_hook);
	CInlineHook::UnInlineHook(&SetLight_hook);
	CInlineHook::UnInlineHook(&LightEnable_hook);
	
}
void CDeverBaseHook::Init(DWORD pDev)
{
	if (pDev==0)
		return;
	//////////////////////////////////////////////////////////////////////////
	m_Dev=pDev;
	//////////////////////////////////////////////////////////////////////////
	//DrawRectPatch_hook.OldAddres=GetBaseFunAddr(0x1cc);
	DrawRectPatch_hook.OldAddres=GetBaseFunAddr(g_3DGameData.DrawRectPatch_off);
	DrawRectPatch_hook.NewAddres=(DWORD)naked_DrawRectPatch;
	//////////////////////////////////////////////////////////////////////////
	//DrawPrimitive_hook.OldAddres=GetBaseFunAddr(0x144);
	DrawPrimitive_hook.OldAddres=GetBaseFunAddr(g_3DGameData.DrawPrimitive_off);
	DrawPrimitive_hook.NewAddres=(DWORD)naked_DrawPrimitive;

	//////////////////////////////////////////////////////////////////////////
	//DrawIndexedPrimitive_hook.OldAddres=GetBaseFunAddr(0x148);
	DrawIndexedPrimitive_hook.OldAddres=GetBaseFunAddr(g_3DGameData.DrawIndexedPrimitive_off);
	DrawIndexedPrimitive_hook.NewAddres=(DWORD)naked_DrawIndexedPrimitive;
	//////////////////////////////////////////////////////////////////////////
	//DrawPrimitiveUP_hook.OldAddres=GetBaseFunAddr(0x14c);
	DrawPrimitiveUP_hook.OldAddres=GetBaseFunAddr(g_3DGameData.DrawPrimitiveUP_off);
	DrawPrimitiveUP_hook.NewAddres=(DWORD)naked_DrawPrimitiveUP;
	//////////////////////////////////////////////////////////////////////////
	//DrawIndexedPrimitiveUP_hook.OldAddres=GetBaseFunAddr(0x150);
	DrawIndexedPrimitiveUP_hook.OldAddres=GetBaseFunAddr(g_3DGameData.DrawIndexedPrimitiveUP_off);
	DrawIndexedPrimitiveUP_hook.NewAddres=(DWORD)naked_DrawIndexedPrimitiveUP;
	//////////////////////////////////////////////////////////////////////////
	//DrawTriPatch_hook.OldAddres=GetBaseFunAddr(0x1d0);
	DrawTriPatch_hook.OldAddres=GetBaseFunAddr(g_3DGameData.DrawTriPatch_off);
	DrawTriPatch_hook.NewAddres=(DWORD)naked_DrawTriPatch;
	//////////////////////////////////////////////////////////////////////////
    //Present_hook.OldAddres=GetBaseFunAddr(0x44);
	Present_hook.OldAddres=GetBaseFunAddr(g_3DGameData.Present_off);
	Present_hook.NewAddres=(DWORD)naked_Present;
	//////////////////////////////////////////////////////////////////////////
	SetRenderState_hook.OldAddres=GetBaseFunAddr(0xe4);
	SetRenderState_hook.NewAddres=(DWORD)Naked_SetRenderState;
	SetRenderState_Jmp=SetRenderState_hook.OldAddres;
	//////////////////////////////////////////////////////////////////////////
	//EndScene_hook.OldAddres=GetBaseFunAddr(0xa8);
	EndScene_hook.OldAddres=GetBaseFunAddr(g_3DGameData.EndScene_off);
	EndScene_hook.NewAddres=(DWORD)naked_EndScene;
	EndScene_jmp=EndScene_hook.OldAddres;
	//////////////////////////////////////////////////////////////////////////
	SetLight_hook.OldAddres=GetBaseFunAddr(0xcc);
	SetLight_hook.NewAddres=(DWORD)naked_SetLight;
	//////////////////////////////////////////////////////////////////////////
	LightEnable_hook.OldAddres=GetBaseFunAddr(0xd4);
	LightEnable_hook.NewAddres=(DWORD)naked_LightEnable;
	//////////////////////////////////////////////////////////////////////////
	SetTexture_hook.OldAddres=GetBaseFunAddr(0x104);
	SetTexture_hook.NewAddres=(DWORD)naked_SetTexture;

	//////////////////////////////////////////////////////////////////////////
	   
	SetTransform_hook.OldAddres=GetBaseFunAddr(0xb0);
	SetTransform_hook.NewAddres=(DWORD)nakde_SetTransform;
	//////////////////////////////////////////////////////////////////////////
	
	Rese_hook.NewAddres=(DWORD)naked_Rese;
	Rese_hook.OldAddres=GetBaseFunAddr(0x40);
	//////////////////////////////////////////////////////////////////////////
	SetSamplerState_hook.OldAddres=GetBaseFunAddr(0x114);
	SetSamplerState_hook.NewAddres=(DWORD)naked_SetSamplerState;
	//////////////////////////////////////////////////////////////////////////
	
	SetStreamSource_hook.OldAddres=GetBaseFunAddr(0x190);
	SetStreamSource_hook.NewAddres=(DWORD)naked_SetStreamSource;
	//////////////////////////////////////////////////////////////////////////
}
DWORD CDeverBaseHook::GetBaseFunAddr(DWORD Dev_off)
{
	DWORD plist=0,dwRet=0;
	if (m_Dev==NULL)
		return 0;
	plist=CInlineHook::GetMemValue((DWORD)m_Dev);
	dwRet=CInlineHook::GetMemValue(plist+Dev_off);
	return dwRet;
	
}
///////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////
CMeshFunHook::CMeshFunHook()
{
	m_pmesh=0;
	memset((void*)&DrawSubset_hook,0,sizeof(InlineHook));
	memset((void*)&OptimizeInplace_hook,0,sizeof(InlineHook));
	memset((void*)&Optimize_hook,0,sizeof(InlineHook));
	//////////////////////////////////////////////////////////////////////////
	memset((void*)&D3DXLoadMeshFromXA_hook,0,sizeof(InlineHook));//32,
	memset((void*)&D3DXLoadMeshFromXInMemory_hook,0,sizeof(InlineHook));//36,
	memset((void*)&D3DXLoadMeshFromXResource_hook,0,sizeof(InlineHook));//40,
	memset((void*)&D3DXLoadMeshFromXW_hook,0,sizeof(InlineHook));//32,
	memset((void*)&D3DXLoadMeshFromXof_hook,0,sizeof(InlineHook));//32,
	memset((void*)&D3DXLoadPatchMeshFromXof_hook,0,sizeof(InlineHook));//28,
	memset((void*)&D3DXLoadSkinMeshFromXof_hook,0,sizeof(InlineHook));//36,
	// 	memset((void*)&D3DXLoadSurfaceFromFileA_hook,0,sizeof(InlineHook));//32,
	// 	memset((void*)&D3DXLoadSurfaceFromFileInMemory_hook,0,sizeof(InlineHook));//36,
	// 	memset((void*)&D3DXLoadSurfaceFromFileW_hook,0,sizeof(InlineHook));//32,
	// 	memset((void*)&D3DXLoadSurfaceFromMemory_hook,0,sizeof(InlineHook));//40,
	// 	memset((void*)&D3DXLoadSurfaceFromResourceA_hook,0,sizeof(InlineHook));//36,
	// 	memset((void*)&D3DXLoadSurfaceFromResourceW_hook,0,sizeof(InlineHook));//36,
	// 	memset((void*)&D3DXLoadSurfaceFromSurface_hook,0,sizeof(InlineHook));//32,
}
CMeshFunHook::~CMeshFunHook()
{
	UnHook();
}
//////////////////////////////////////////////////////////////////////////
NAKED void	CMeshFunHook::naked_DrawSubset()
{
	ASM
	{
		xor eax,eax;
		ret 8;
	}
}
NAKED void CMeshFunHook::naked_OptimizeInplace()
{
	ASM
	{
		xor eax,eax;
		ret 24;
	}
}
NAKED void CMeshFunHook::naked_Optimize()
{
	ASM
	{

		xor eax,eax;
		ret 28;
	}

}
//////////////////////////////////////////////////////////////////////////
NAKED void CMeshFunHook::naked_D3DXLoadMeshFromXA(LPCSTR pFilename, 
												  DWORD Options, 
												  LPDIRECT3DDEVICE9 pD3DDevice, 
												  LPD3DXBUFFER *ppAdjacency,
												  LPD3DXBUFFER *ppMaterials, 
												  LPD3DXBUFFER *ppEffectInstances, 
												  DWORD *pNumMaterials,
												  LPD3DXMESH *ppMesh)
{
	ASM
	{
		mov edi,edi;
		push ebp;
		mov ebp,esp;
	}
	if (g_Game_pDev)
	{
		D3DXCreateTeapot(g_Game_pDev, ppMesh,ppAdjacency );
       ppMaterials=ppEffectInstances=ppAdjacency;
        *pNumMaterials=1;
	}
	ASM
	{
		mov esp,ebp;
		pop ebp;
		xor eax,eax;
		ret 32;
	}
}//32,
//////////////////////////////////////////////////////////////////////////
NAKED void CMeshFunHook::naked_D3DXLoadMeshFromXInMemory(     
														 LPCVOID Memory,
														 DWORD SizeOfMemory,
														 DWORD Options, 
														 LPDIRECT3DDEVICE9 pD3DDevice, 
														 LPD3DXBUFFER *ppAdjacency,
														 LPD3DXBUFFER *ppMaterials, 
														 LPD3DXBUFFER *ppEffectInstances, 
														 DWORD *pNumMaterials,
														 LPD3DXMESH *ppMesh)
{

	ASM
	{
		mov edi,edi;
		push ebp;
		mov ebp,esp;
	}
	if (g_Game_pDev)
	{
		D3DXCreateTeapot(g_Game_pDev, ppMesh,ppAdjacency );
       ppMaterials=ppEffectInstances=ppAdjacency;
        *pNumMaterials=1;
	}
	ASM
	{
		mov esp,ebp;
		pop ebp;
		xor eax,eax;
		ret 36;
	}
}//36,
//////////////////////////////////////////////////////////////////////////
NAKED void CMeshFunHook::naked_D3DXLoadMeshFromXResource(
														 HMODULE Module,
														 LPCSTR Name,
														 LPCSTR Type,
														 DWORD Options, 
														 LPDIRECT3DDEVICE9 pD3DDevice, 
														 LPD3DXBUFFER *ppAdjacency,
														 LPD3DXBUFFER *ppMaterials, 
														 LPD3DXBUFFER *ppEffectInstances, 
														 DWORD *pNumMaterials,
														 LPD3DXMESH *ppMesh)
{
	ASM
	{
		mov edi,edi;
		push ebp;
		mov ebp,esp;
	}
	if (g_Game_pDev)
	{
		D3DXCreateTeapot(g_Game_pDev, ppMesh,ppAdjacency );
       ppMaterials=ppEffectInstances=ppAdjacency;
        *pNumMaterials=1;
	}
	ASM
	{
		mov esp,ebp;
		pop ebp;
		xor eax,eax;
		ret 40;
	}
}//40,
//////////////////////////////////////////////////////////////////////////
NAKED void CMeshFunHook::naked_D3DXLoadMeshFromXW(  
												  LPCWSTR pFilename, 
												  DWORD Options, 
												  LPDIRECT3DDEVICE9 pD3DDevice, 
												  LPD3DXBUFFER *ppAdjacency,
												  LPD3DXBUFFER *ppMaterials, 
												  LPD3DXBUFFER *ppEffectInstances, 
												  DWORD *pNumMaterials,
												  LPD3DXMESH *ppMesh)
{
	ASM
	{
		mov edi,edi;
		push ebp;
		mov ebp,esp;
	}
	if (g_Game_pDev)
	{
		//AfxMessageBox("naked_D3DXLoadMeshFromXW");
		D3DXCreateTeapot(g_Game_pDev, ppMesh,ppAdjacency );
		ppMaterials=ppEffectInstances=ppAdjacency;
        *pNumMaterials=1;
	}
	ASM
	{
		mov esp,ebp;
		pop ebp;
		xor eax,eax;
		ret 32;
	}
}//32,
//////////////////////////////////////////////////////////////////////////
NAKED void CMeshFunHook::naked_D3DXLoadMeshFromXof(  
												   LPD3DXFILEDATA pxofMesh, 
												   DWORD Options, 
												   LPDIRECT3DDEVICE9 pD3DDevice, 
												   LPD3DXBUFFER *ppAdjacency,
												   LPD3DXBUFFER *ppMaterials, 
												   LPD3DXBUFFER *ppEffectInstances, 
												   DWORD *pNumMaterials,
												   LPD3DXMESH *ppMesh)
{
	ASM
	{
		mov edi,edi;
		push ebp;
		mov ebp,esp;
	}
	if (g_Game_pDev)
	{
		//AfxMessageBox("naked_D3DXLoadMeshFromXof");
		D3DXCreateTeapot(g_Game_pDev, ppMesh,ppAdjacency );
		ppMaterials=ppEffectInstances=ppAdjacency;
        *pNumMaterials=1;
	}
	ASM
	{
		mov esp,ebp;
		pop ebp;
		xor eax,eax;
		ret 32;
	}
}//32,

NAKED void CMeshFunHook::naked_D3DXLoadPatchMeshFromXof(   
														LPD3DXFILEDATA pXofObjMesh,
														DWORD Options,
														LPDIRECT3DDEVICE9 pD3DDevice,
														LPD3DXBUFFER *ppMaterials,
														LPD3DXBUFFER *ppEffectInstances, 
														PDWORD pNumMaterials,
														LPD3DXPATCHMESH *ppMesh)
{
	ASM
	{
		mov edi,edi;
		push ebp;
		mov ebp,esp;
	}
	if (g_Game_pDev)
	{
		//AfxMessageBox("naked_D3DXLoadPatchMeshFromXof");
		D3DXCreateTeapot(g_Game_pDev, &g_pMesh,ppEffectInstances );
		ppMaterials=ppEffectInstances;
        *pNumMaterials=1;
	}
	ASM
	{
		mov esp,ebp;
		pop ebp;
		xor eax,eax;
		ret 28;
	}
}//28,
NAKED void CMeshFunHook::naked_D3DXLoadSkinMeshFromXof(   
													   LPD3DXFILEDATA pxofMesh, 
													   DWORD Options,
													   LPDIRECT3DDEVICE9 pD3DDevice,
													   LPD3DXBUFFER* ppAdjacency,
													   LPD3DXBUFFER* ppMaterials,
													   LPD3DXBUFFER *ppEffectInstances, 
													   DWORD *pMatOut,
													   LPD3DXSKININFO* ppSkinInfo,
													   LPD3DXMESH* ppMesh)
{
	ASM
	{
		mov edi,edi;
		push ebp;
		mov ebp,esp;
	}
	if (g_Game_pDev)
	{
		//AfxMessageBox("naked_D3DXLoadSkinMeshFromXof");
	
		D3DXCreateTeapot(g_Game_pDev, ppMesh,ppAdjacency );
		ppMaterials=ppEffectInstances=ppAdjacency;
	}
	ASM
	{
		mov esp,ebp;
		pop ebp;
		xor eax,eax;
		ret 36;
	}
}//36,
//////////////////////////////////////////////////////////////////////////
DWORD CMeshFunHook::GetMeshFunAddr(DWORD off)
{
	DWORD plist=0,dwRet=0;
	if (m_pmesh==NULL)
		return 0;
	plist=CInlineHook::GetMemValue((DWORD)m_pmesh);
	dwRet=CInlineHook::GetMemValue(plist+off);
	return dwRet;
}
void CMeshFunHook::Init(DWORD pMesh)
{
	if (pMesh==0)
		return;
	m_pmesh=pMesh;
	//////////////////////////////////////////////////////////////////////////
	//DrawSubset_hook.OldAddres=GetMeshFunAddr(0x0c);
	DrawSubset_hook.OldAddres=GetMeshFunAddr(g_3DGameData.DrawSubset_off);
	DrawSubset_hook.NewAddres=(DWORD)naked_DrawSubset;
	//////////////////////////////////////////////////////////////////////////
	Optimize_hook.NewAddres=(DWORD)naked_Optimize;
	Optimize_hook.OldAddres=GetMeshFunAddr(0x68);
	//////////////////////////////////////////////////////////////////////////
	
	OptimizeInplace_hook.OldAddres=GetMeshFunAddr(0x6c);
	OptimizeInplace_hook.NewAddres=(DWORD)naked_OptimizeInplace;
	//////////////////////////////////////////////////////////////////////////
	D3DXLoadMeshFromXA_hook.NewAddres=(DWORD)naked_D3DXLoadMeshFromXA;
	D3DXLoadMeshFromXA_hook.OldAddres=CDeverBase::GetOutFunAddres("D3DXLoadMeshFromXA");
	//////////////////////////////////////////////////////////////////////////
	D3DXLoadMeshFromXInMemory_hook.NewAddres=(DWORD)naked_D3DXLoadMeshFromXInMemory;
	D3DXLoadMeshFromXInMemory_hook.OldAddres=CDeverBase::GetOutFunAddres("D3DXLoadMeshFromXInMemory");
	//////////////////////////////////////////////////////////////////////////
	D3DXLoadMeshFromXResource_hook.NewAddres=(DWORD)naked_D3DXLoadMeshFromXResource;
	D3DXLoadMeshFromXResource_hook.OldAddres=CDeverBase::GetOutFunAddres("D3DXLoadMeshFromXResource");
	//////////////////////////////////////////////////////////////////////////
	D3DXLoadMeshFromXW_hook.NewAddres=(DWORD)naked_D3DXLoadMeshFromXW;
	D3DXLoadMeshFromXW_hook.OldAddres=CDeverBase::GetOutFunAddres("D3DXLoadMeshFromXW");
	//////////////////////////////////////////////////////////////////////////
	D3DXLoadMeshFromXof_hook.NewAddres=(DWORD)naked_D3DXLoadMeshFromXof;
	D3DXLoadMeshFromXof_hook.OldAddres=CDeverBase::GetOutFunAddres("D3DXLoadMeshFromXof");
	//////////////////////////////////////////////////////////////////////////
	D3DXLoadPatchMeshFromXof_hook.NewAddres=(DWORD)naked_D3DXLoadPatchMeshFromXof;
	D3DXLoadPatchMeshFromXof_hook.OldAddres=CDeverBase::GetOutFunAddres("D3DXLoadPatchMeshFromXof");
	//////////////////////////////////////////////////////////////////////////
	D3DXLoadSkinMeshFromXof_hook.NewAddres=(DWORD)naked_D3DXLoadSkinMeshFromXof;
	D3DXLoadSkinMeshFromXof_hook.OldAddres=CDeverBase::GetOutFunAddres("D3DXLoadSkinMeshFromXof");
	//////////////////////////////////////////////////////////////////////////
	
}

void CMeshFunHook::SetAllHook()
{
	CInlineHook::SetInlineHook(&DrawSubset_hook);
	CInlineHook::SetInlineHook(&OptimizeInplace_hook);
	CInlineHook::SetInlineHook(&Optimize_hook);
	//////////////////////////////////////////////////////////////////////////
	CInlineHook::SetInlineHook(&D3DXLoadMeshFromXA_hook);//32,
	CInlineHook::SetInlineHook(&D3DXLoadMeshFromXInMemory_hook);//36,
	CInlineHook::SetInlineHook(&D3DXLoadMeshFromXResource_hook);//40,
	CInlineHook::SetInlineHook(&D3DXLoadMeshFromXW_hook);//32,
	CInlineHook::SetInlineHook(&D3DXLoadMeshFromXof_hook);//32,
	CInlineHook::SetInlineHook(&D3DXLoadPatchMeshFromXof_hook);//28,
	CInlineHook::SetInlineHook(&D3DXLoadSkinMeshFromXof_hook);//36,
		
	// 	CInlineHook::SetInlineHook(&D3DXLoadSurfaceFromFileA_hook);//32,
	// 	CInlineHook::SetInlineHook(&D3DXLoadSurfaceFromFileInMemory_hook);//36,
	// 	CInlineHook::SetInlineHook(&D3DXLoadSurfaceFromFileW_hook);//32,
	// 	CInlineHook::SetInlineHook(&D3DXLoadSurfaceFromMemory_hook);//40,
	// 	CInlineHook::SetInlineHook(&D3DXLoadSurfaceFromResourceA_hook);//36,
	// 	CInlineHook::SetInlineHook(&D3DXLoadSurfaceFromResourceW_hook);//36,
	// 	CInlineHook::SetInlineHook(&D3DXLoadSurfaceFromSurface_hook);//32,
}

void CMeshFunHook::UnHook()
{
	CInlineHook::TestHookVlaue(&DrawSubset_hook);
	CInlineHook::TestHookVlaue(&OptimizeInplace_hook);
	CInlineHook::UnInlineHook(&DrawSubset_hook);
	CInlineHook::UnInlineHook(&OptimizeInplace_hook);
	CInlineHook::TestHookVlaue(&Optimize_hook);
	CInlineHook::UnInlineHook(&Optimize_hook);
	//////////////////////////////////////////////////////////////////////////
	CInlineHook::TestHookVlaue(&D3DXLoadMeshFromXA_hook);//32,
	CInlineHook::TestHookVlaue(&D3DXLoadMeshFromXInMemory_hook);//36,
	CInlineHook::TestHookVlaue(&D3DXLoadMeshFromXResource_hook);//40,
	CInlineHook::TestHookVlaue(&D3DXLoadMeshFromXW_hook);//32,
	CInlineHook::TestHookVlaue(&D3DXLoadMeshFromXof_hook);//32,
	CInlineHook::TestHookVlaue(&D3DXLoadPatchMeshFromXof_hook);//28,
	CInlineHook::TestHookVlaue(&D3DXLoadSkinMeshFromXof_hook);//36,
	// 	CInlineHook::TestHookVlaue(&D3DXLoadSurfaceFromFileA_hook);//32,
	// 	CInlineHook::TestHookVlaue(&D3DXLoadSurfaceFromFileInMemory_hook);//36,
	// 	CInlineHook::TestHookVlaue(&D3DXLoadSurfaceFromFileW_hook);//32,
	// 	CInlineHook::TestHookVlaue(&D3DXLoadSurfaceFromMemory_hook);//40,
	// 	CInlineHook::TestHookVlaue(&D3DXLoadSurfaceFromResourceA_hook);//36,
	// 	CInlineHook::TestHookVlaue(&D3DXLoadSurfaceFromResourceW_hook);//36,
	// 	CInlineHook::TestHookVlaue(&D3DXLoadSurfaceFromSurface_hook);//32,
	//////////////////////////////////////////////////////////////////////////
	CInlineHook::UnInlineHook(&D3DXLoadMeshFromXA_hook);//32,
	CInlineHook::UnInlineHook(&D3DXLoadMeshFromXInMemory_hook);//36,
	CInlineHook::UnInlineHook(&D3DXLoadMeshFromXResource_hook);//40,
	CInlineHook::UnInlineHook(&D3DXLoadMeshFromXW_hook);//32,
	CInlineHook::UnInlineHook(&D3DXLoadMeshFromXof_hook);//32,
	CInlineHook::UnInlineHook(&D3DXLoadPatchMeshFromXof_hook);//28,
	CInlineHook::UnInlineHook(&D3DXLoadSkinMeshFromXof_hook);//36,
	// 	CInlineHook::UnInlineHook(&D3DXLoadSurfaceFromFileA_hook);//32,
	// 	CInlineHook::UnInlineHook(&D3DXLoadSurfaceFromFileInMemory_hook);//36,
	// 	CInlineHook::UnInlineHook(&D3DXLoadSurfaceFromFileW_hook);//32,
	// 	CInlineHook::UnInlineHook(&D3DXLoadSurfaceFromMemory_hook);//40,
	// 	CInlineHook::UnInlineHook(&D3DXLoadSurfaceFromResourceA_hook);//36,
	// 	CInlineHook::UnInlineHook(&D3DXLoadSurfaceFromResourceW_hook);//36,
	// 	CInlineHook::UnInlineHook(&D3DXLoadSurfaceFromSurface_hook);//32,
}
//////////////////////////////////////////////////////////////////////////
//
//
//
//////////////////////////////////////////////////////////////////////////
CFontFunHook::CFontFunHook()
{
	m_pFont=0;
	memset((void*)&DrawText_hook,0,sizeof(InlineHook));
}
CFontFunHook::~CFontFunHook()
{
	UnHook();
}
//////////////////////////////////////////////////////////////////////////
NAKED void CFontFunHook::naked_DrawText()
{
	ASM
	{
		xor eax,eax;
		ret  0x1c;
	}
}
//////////////////////////////////////////////////////////////////////////
void CFontFunHook::Init(DWORD pFont)
{
	if (pFont==0)
		return;
	m_pFont=pFont;
	//////////////////////////////////////////////////////////////////////////
	DrawText_hook.OldAddres=GetFontFunAddr(0x38);
	DrawText_hook.NewAddres=(DWORD)naked_DrawText;
	DrawText_jmp=DrawText_hook.OldAddres;
	//////////////////////////////////////////////////////////////////////////
	
}

DWORD CFontFunHook::GetFontFunAddr(DWORD off)
{
	DWORD plist=0,dwRet=0;
	if (m_pFont==NULL)
		return 0;
	plist=CInlineHook::GetMemValue((DWORD)m_pFont);
	dwRet=CInlineHook::GetMemValue(plist+off);
	return dwRet;
}

void CFontFunHook::SetAllHook()
{
	CInlineHook::SetInlineHook(&DrawText_hook);
}

void CFontFunHook::UnHook()
{
	CInlineHook::TestHookVlaue(&DrawText_hook);
	CInlineHook::UnInlineHook(&DrawText_hook);
	
}
//////////////////////////////////////////////////////////////////////////
//
//
//
//////////////////////////////////////////////////////////////////////////
CD3DXSpriteHook::CD3DXSpriteHook()
{
	m_pSprite=0;
	memset((void*)&Draw_hook,0,sizeof(InlineHook));
	memset((void*)&SetWorldViewLH_hook,0,sizeof(InlineHook));
	memset((void*)&SetWorldViewRH_hook,0,sizeof(InlineHook));
}
CD3DXSpriteHook::~CD3DXSpriteHook()
{
	UnHook();
}
NAKED void CD3DXSpriteHook::naked_Draw()
{
	ASM
	{
		xor eax,eax;
		ret 0x18;
	}
}
NAKED void CD3DXSpriteHook::naked_SetWorldViewRH()
{
	ASM
	{
		xor eax,eax;
		ret 12;
	}
}
NAKED void CD3DXSpriteHook::naked_SetWorldViewRL()
{
	ASM
	{
		xor eax,eax;
		ret 12;
	}
}

void CD3DXSpriteHook::Init(DWORD pSprite)
{
	if (pSprite==NULL)
		return;
	m_pSprite=pSprite;
	//////////////////////////////////////////////////////////////////////////
	Draw_hook.OldAddres=GetSpriteFunAddr(0x24);
	Draw_hook.NewAddres=(DWORD)naked_Draw;
	//////////////////////////////////////////////////////////////////////////
	
	SetWorldViewLH_hook.OldAddres=GetSpriteFunAddr(0x1c);
	SetWorldViewLH_hook.NewAddres=(DWORD)naked_SetWorldViewRL;
	//////////////////////////////////////////////////////////////////////////
	SetWorldViewRH_hook.NewAddres=(DWORD)naked_SetWorldViewRH;
	SetWorldViewRH_hook.OldAddres=GetSpriteFunAddr(0x18);
	//////////////////////////////////////////////////////////////////////////
	
	
}
void CD3DXSpriteHook::SetAllHook()
{
	CInlineHook::SetInlineHook(&Draw_hook);
	CInlineHook::SetInlineHook(&SetWorldViewRH_hook);
	CInlineHook::SetInlineHook(&SetWorldViewLH_hook);
}
void CD3DXSpriteHook::UnHook()
{
	CInlineHook::TestHookVlaue(&SetWorldViewRH_hook);
	CInlineHook::UnInlineHook(&SetWorldViewRH_hook);
	
	CInlineHook::TestHookVlaue(&SetWorldViewLH_hook);
	CInlineHook::UnInlineHook(&SetWorldViewLH_hook);
	
	CInlineHook::TestHookVlaue(&Draw_hook);
	CInlineHook::UnInlineHook(&Draw_hook);
}
DWORD CD3DXSpriteHook::GetSpriteFunAddr(DWORD off)
{
	DWORD plist=0,dwRet=0;
	if (m_pSprite==NULL)
		return 0;
	plist=CInlineHook::GetMemValue((DWORD)m_pSprite);
	dwRet=CInlineHook::GetMemValue(plist+off);
	return dwRet;
	
}
//////////////////////////////////////////////////////////////////////////
//
//
//
//////////////////////////////////////////////////////////////////////////
CD3dTextureHook::CD3dTextureHook()
{
	memset((void*)&D3DXCreateTextureFromFile_hook,0,sizeof(InlineHook));
	memset((void*)&D3DXCreateTextureFromFileInMemory_hook,0,sizeof(InlineHook));
	memset((void*)&D3DXCreateTextureFromResourceA_hook,0,sizeof(InlineHook));
}
CD3dTextureHook:: ~CD3dTextureHook()
{
	
}
void CD3dTextureHook::SetAllHook()
{
	CInlineHook::SetInlineHook(&D3DXCreateTextureFromFile_hook);
	CInlineHook::SetInlineHook(&D3DXCreateTextureFromFileInMemory_hook);
	CInlineHook::SetInlineHook(&D3DXCreateTextureFromResourceA_hook);
}

void CD3dTextureHook::Init()
{
	D3DXCreateTextureFromFile_hook.NewAddres=(DWORD)naked_D3DXCreateTextureFromFile;
	D3DXCreateTextureFromFile_hook.OldAddres=CDeverBase::GetOutFunAddres("D3DXCreateTextureFromFileA");
	//////////////////////////////////////////////////////////////////////////
	D3DXCreateTextureFromFileInMemory_hook.NewAddres=(DWORD)naked_D3DXCreateTextureFromFileInMemory;
	D3DXCreateTextureFromFileInMemory_hook.OldAddres=CDeverBase::GetOutFunAddres("D3DXCreateTextureFromFileInMemory");
	//////////////////////////////////////////////////////////////////////////
	D3DXCreateTextureFromResourceA_hook.NewAddres=(DWORD)nakde_D3DXCreateTextureFromResourceA;
	D3DXCreateTextureFromResourceA_hook.OldAddres=CDeverBase::GetOutFunAddres("D3DXCreateTextureFromResourceA");
}

void CD3dTextureHook::UnHook()
{
	CInlineHook::TestHookVlaue(&D3DXCreateTextureFromFile_hook);
	CInlineHook::TestHookVlaue(&D3DXCreateTextureFromFileInMemory_hook);
	CInlineHook::TestHookVlaue(&D3DXCreateTextureFromResourceA_hook);
	CInlineHook::UnInlineHook(&D3DXCreateTextureFromFile_hook);
	CInlineHook::UnInlineHook(&D3DXCreateTextureFromFileInMemory_hook);
	CInlineHook::UnInlineHook(&D3DXCreateTextureFromResourceA_hook);
}

NAKED void CD3dTextureHook::naked_D3DXCreateTextureFromFile(LPDIRECT3DDEVICE9 pDevice,
															LPCSTR    pSrcFile,
															LPDIRECT3DTEXTURE9*    ppTexture)
{
	ASM
	{
		mov edi,edi;
		push ebp;
		mov ebp,esp;
	}
	D3DXCreateTexture(pDevice,30,30,1,1,D3DFMT_L8,D3DPOOL_DEFAULT,ppTexture);
	ASM
	{
		mov esp,ebp;
		pop ebp;
		ret 12;
	}
}

NAKED void CD3dTextureHook::naked_D3DXCreateTextureFromFileInMemory(LPDIRECT3DDEVICE9     pDevice,
																	LPCVOID                   pSrcData,
																	UINT                      SrcDataSize,
																	LPDIRECT3DTEXTURE9*       ppTexture)
{
	ASM
	{
		mov edi,edi;
		push ebp;
		mov ebp,esp;
	}
	D3DXCreateTexture(pDevice,30,30,1,1,D3DFMT_L8,D3DPOOL_DEFAULT,ppTexture);
	ASM
	{
		mov esp,ebp;
		pop ebp;
		ret 16;
	}
}

NAKED void CD3dTextureHook::nakde_D3DXCreateTextureFromResourceA(LPDIRECT3DDEVICE9   pDevice,
																 HMODULE   hSrcModule,
																 LPCSTR     pSrcResource,
																 LPDIRECT3DTEXTURE9*       ppTexture)
{
	ASM
	{
		mov edi,edi;
		push ebp;
		mov ebp,esp;
	}
	D3DXCreateTexture(pDevice,30,30,1,1,D3DFMT_L8,D3DPOOL_DEFAULT,ppTexture);
	ASM
	{
		mov esp,ebp;
		pop ebp;
		ret 16;
	}
}
//////////////////////////////////////////////////////////////////////////
//
//
//
//////////////////////////////////////////////////////////////////////////
void  NAKED CGameMeshFileEx::naked_ReadFile( 
		HANDLE hFile, 
		LPVOID lpBuffer, 
		DWORD nNumberOfBytesToRead,
		LPDWORD lpNumberOfBytesRead,
		LPOVERLAPPED lpOverlapped )
{
	ASM
	{
		mov edi,edi;
		push ebp;
		mov ebp,esp;
	}
	//////////////////////////////////////////////////////////////////////////
	CGameMeshFileEx::OldReadFile(hFile,lpBuffer,nNumberOfBytesToRead,lpNumberOfBytesRead,lpOverlapped);
    if (CGameMeshFileEx::IsMeshFile(lpBuffer))
    {
		memset(lpBuffer,0,nNumberOfBytesToRead);
		*lpNumberOfBytesRead=0;
    }
	//////////////////////////////////////////////////////////////////////////
	ASM
	{
	 mov esp,ebp;
	  pop ebp;
	  ret 20;
	}
}

void CGameMeshFileEx::Init()
{
	DWORD oldProctect=0,oldProtect1=0,add=(DWORD)CGameMeshFileEx::OldReadFile;
	HMODULE hmod=GetModuleHandleA("kernel32.dll");
	memset((void*)&ReadMeshFile_hook,0,sizeof(ReadMeshFile_hook));
	if (hmod)
	{
		ReadMeshFile_hook.OldAddres=(DWORD)GetProcAddress(hmod,"ReadFile");
		ReadFile_jmp=ReadMeshFile_hook.OldAddres;
		ReadMeshFile_hook.NewAddres=(DWORD)CGameMeshFileEx::naked_ReadFile;
		ReadMeshFile_hook.HookCodeLen=7;
	}
	if (ReadFile_jmp!=0)
	{
		VirtualProtect((void*)ReadFile_jmp,7,PAGE_EXECUTE_READWRITE,&oldProctect);
		VirtualProtect((void*)add,7,PAGE_EXECUTE_READWRITE,&oldProtect1);
		//////////////////////////////////////////////////////////////////////////
		memcpy((void*)add,(void*)ReadFile_jmp,7);
		//////////////////////////////////////////////////////////////////////////
		VirtualProtect((void*)ReadFile_jmp,7,oldProctect,&oldProctect);
		VirtualProtect((void*)add,7,oldProtect1,&oldProtect1);
	}
}
void CGameMeshFileEx::SetAllHook()
{
	CInlineHook::SetInlineHook(&ReadMeshFile_hook);
}
void CGameMeshFileEx::UnHook()
{
	CInlineHook::TestHookVlaue(&ReadMeshFile_hook);
	CInlineHook::UnInlineHook(&ReadMeshFile_hook);
}
void NAKED CGameMeshFileEx::OldReadFile(
										HANDLE hFile, 
										LPVOID lpBuffer, 
										DWORD nNumberOfBytesToRead,
										LPDWORD lpNumberOfBytesRead,
										LPOVERLAPPED lpOverlapped )
{
	ASM
	{
		push 0x20;
		push 0x7C809C38;
		mov eax,ReadFile_jmp;
		add eax,7;
		jmp eax;
	}	
}

BOOL CGameMeshFileEx::IsMeshFile(void *lpReadFileBuf)
{
	BOOL bRet=false;
    CStringA FileBuf=(char*)lpReadFileBuf;
	if (FileBuf.IsEmpty()||FileBuf.GetLength()<20)
		return bRet;
	CStringA tmp=FileBuf.Left(20);
    if (tmp.Find("Mesh",0)!=-1||
		tmp.Find("NG",0)!=-1||
		tmp.Find("JFIF",0)!=-1||
		tmp.Find("DDS",0)!=-1)
    {
		bRet=true;
    }
	return bRet;
}
