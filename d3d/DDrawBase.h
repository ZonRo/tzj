// DDrawBase.h: interface for the CDDrawBase class.
//
//////////////////////////////////////////////////////////////////////
#ifndef DDRAWBASE_H
#define DDRAWBASE_H
//////////////////////////////////////////////////////////////////////////
#include "InlineHook.h"
//////////////////////////////////////////////////////////////////////////
#include <afxtempl.h>
#include <ddraw.h>
#pragma comment (lib,"ddraw.lib")
//////////////////////////////////////////////////////////////////////////
typedef struct _D2DGAMEBASE
{
	
	DWORD	DeleteAttachedSurface_off;
	DWORD	Blt_off;
	DWORD	BltBatch_off;
	DWORD	BltFast_off;
	DWORD	SetColorKey_off;
	DWORD	Flip_off;
}D2DGAMEBASE,*LPD2DGAMEBASE;
//////////////////////////////////////////////////////////////////////////
class CDDrawBase  
{
public:

	BOOL InitDDrawBase();
	CDDrawBase();
	virtual ~CDDrawBase();

};
class CDDrawSurfaceHook
{
public:
	void UnHook();
	void SetAllHook();
	static void naked_IsLost();
	static void naked_Restore();
	static void naked_DeleteAttachedSurface();
	static void naked_Blt();
	static void naked_BltBatch();
	static void naked_BltFast();
	static void naked_SetColorKey();
	static void naked_ReleaseDC();
	static void naked_Flip();
	//////////////////////////////////////////////////////////////////////////
public:
	DWORD GetFunOff(DWORD off);
	void Init(DWORD Base);
	CDDrawSurfaceHook();
	virtual ~CDDrawSurfaceHook();
protected:
	DWORD      m_Base;
	InlineHook IsLost_hook;
	InlineHook Restore_hook;
	InlineHook DeleteAttachedSurface_hook;
	InlineHook Blt_hook;
	InlineHook BltBatch_hook;
	InlineHook BltFast_hook;
	InlineHook SetColorKey_hook;
	InlineHook ReleaseDC_hook;
	InlineHook Flip_hook;  
};
//////////////////////////////////////////////////////////////////////////
void WINAPI Start2DBlack();
void WINAPI End2DBlack();
void WINAPI InitD2dData();
//////////////////////////////////////////////////////////////////////////
#endif // !defined(AFX_DDRAWBASE_H__7725EFA1_A6A6_4110_B959_490CE7E19198__INCLUDED_)
