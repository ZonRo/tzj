local sleep = mskco.Sleep
local g_player = g_player
local g_global = g_globalt
local coroutine = coroutine
local maxlv = 36
local minlv = 0
local taskoption = {}
local curtaskoption
local curstatustimeout = 120--当前状态超时时间
local curstatustime = 0
local canaccpettask
local canaccpettasktype
local canaccpettaskkind
local taskstatus = "ready"
CTaskObj = CTaskObj or {}
local curthread
local tmpsubtask
local taskopdeal
local lasttasklv
local animationwnd
local curruntask
local curruntasklist

--local g_global.runListTask = {}
--指定任务顺序
local curtasklist = {
	
}
--可以做的支线任务
local speciatask = {
	[388] = true,--斩灭心魔
	[7000] = true,--新手押镖
	[6007] = true,--装备锻造
	[6019] = true,--融魂
	[6032] = true,--了解庄园
}
--快速接取任务
local quickaccepttask = {
	[6001] = true,--同舟共济 加入帮派任务
	[6005] = true,
	[6007] = true,--装备锻造
	[6014] = true,--了解聚灵
	[6012] = true,--珍珠园·鲛族深渊
	[6019] = true,--融魂
	[6020] = true,--融魂
	[6021] = true,--融魂
	[6022] = true,--融魂
	[6023] = true,--融魂
	[6024] = true,--志同道合 好友任务
	[6025] = true,--风雨同舟 好友任务
	[6026] = true,--肝胆相照 好友任务
	[8100] = true,--击败仙草精
}
--引导任务
--AcceptQuest  -1  6019  4  0--融魂
--AcceptQuest  -1  6007  4  0--装备锻造
function AddTaskOption(tm,call,...)
	local t = {}
	t.call = call
	t.tm = tm
	t.args = {...}
	taskoption[#taskoption+1] = t
end

function AddTaskSub(tm,call,...)
	local t = {}
	t.call = call
	t.tm = tm
	t.args = {...}
	curtaskoption.next = t
end

function CreateTaskObj(id,kind,o)
    o = o or {}
	o.id = id
	o.kind = kind
    setmetatable(o,CTaskObj)
    CTaskObj.__index = CTaskObj
    return o
end

function ExecuteOption(t)
	if t.tm > 0 then t.tm = t.tm-1 return false end--时机未到
	local root = t
	local lastt
	while t.next do
		lastt = t
		t = t.next 
	end
	curtaskoption = t
	local ret,err = pcall(t.call,unpack(t.args))
	if ret and err then
		if lastt then 
			lastt.next = nil
			return ExecuteOption(root) 
		end
		return true
	end
	return false
end

function TransTaskOption()
	local t,root
	for i=1,#taskoption do
		--数组 一个个的执行 前面的失败了 后面的不执行
		if not ExecuteOption(taskoption[i]) then 
			return 
		end
	end
end

local QuestKindDesc = {
  [0] = "全部任务",
  [1] = "主线",
  [2] = "支线",
  [3] = "副本",
  [4] = "职业",
  [5] = "活动",
  [6] = "引导",
  [7] = "日常",
  [8] = "跑环",
  [9] = "帮会",
  [10] = "押镖"
}

local hasLogedCanAccpetQuest = false
g_global.taskupdate = false
local lastCanAcceptQuest = {}
function FilterQuestEvent(event)
	--print("FilterQuestEvent:"..event)
	if event == "QUEST_ADD_CAN_ACCEPT" then
		--print("FilterQuestEvent:"..arg1)
		if hasLogedCanAccpetQuest then
			lastCanAcceptQuest = {}
			hasLogedCanAccpetQuest = false
		end
		local nQuestID = tonumber(arg2)
		print("get a nQuestID:",nQuestID)
		lastCanAcceptQuest[nQuestID] = true
	elseif event == "QUEST_CAN_ACCEPT_END" then
		print("task filter over")
		hasLogedCanAccpetQuest = true
		g_global.taskupdate = true
		mskco.HandleEvent('utask')
		--[[
		if waitresume == true and nil ~= g_global.runthread and g_global.online  then
			coroutine.resume(g_global.runthread,0,"utask")
		end
		--]]
	end
end

function CanQuestAccepted(id)
	if g_global.taskupdate == false then
		UpdateCanAcceptTaskList(false)
	end
	if id == 6032 or id == 6033 then
		local list = GetNpcQuestList(0, 4, 39)
		for i, value in pairs(list) do
			if value == id then return true end
		end
		return false
	elseif id == 6012 or id == 6013 then
		local list = GetNpcQuestList(0, 4, 32)
		for i, value in pairs(list) do
			if value == id then return true end
		end
		return false
	elseif id == 6034 or id == 8129 then--庄园管家
		local list = GetNpcQuestList(0, 4, 41)
		for i, value in pairs(list) do
			if value == id then return true end
		end
		return false
	elseif id == 6007 or id == 6008 or (id >= 6019 and id <= 6023) then
		local list = GetNpcQuestList(0, 4, 1)
		for i, value in pairs(list) do
			if value == id then return true end
		end
		return false
	elseif id == 6001 then
		local list = GetNpcQuestList(0, 4, 5)
		for i, value in pairs(list) do
			if value == id then return true end
		end
		return false
	elseif id == 6005 then
		local list = GetNpcQuestList(0, 4, 515)
		for i, value in pairs(list) do
			if value == id then return true end
		end
		return false
	elseif id == 6014 then
		local list = GetNpcQuestList(0, 4, 17)
		for i, value in pairs(list) do
			if value == id then return true end
		end
		return false
	elseif id == 8100 then
		local list = GetNpcQuestList(0, 4, 41)
		for i, value in pairs(list) do
			if value == id then return true end
		end
		return false
	elseif (id >= 6024 and id <= 6026) then
		local list = GetNpcQuestList(0, 4, 2)
		for i, value in pairs(list) do
			if value == id then return true end
		end
		return false
	end
	if lastCanAcceptQuest[id] then return true end
	return false
end

function FilterCanAcceptTask(strTitle, nQuestID)
	if canaccpettask then return end
	canaccpettask = nQuestID
	do return end
	print("filter:"..nQuestID)
	--Tasktest
	--if g_Task[nQuestID] then
		for i=1,#canaccpettask do
			if canaccpettask[i] == nQuestID then
				return true
			end
		end
		canaccpettask[#canaccpettask+1] = nQuestID
	--end
	return true
end

function FilterTrackAddItem(Title, IsComplete, IsFailed, Desc, ReqLevel, MyLevel, QuestId)
	--msco.HandleEvent('ttask')
	if curruntask and QuestId == curruntask.id then
		print("update task:",Desc)
		curruntask.Desc = Desc
	end
end

function RollBackOption(opt)
	for _,op in pairs(opt) do
		if op.type == "functionhook" then
			mc_tools.UnHookFunction(op.value)
		elseif op.type == "timer" then
			mc_tools.RemoveTimer(op.value)
		end
	end
end
--血量检测
local ywtm = 0
local xdtm = 0
function CheckHp()
	--local --pl = g_player.chara
	if gUI_CurrentStage ~= 3 then return end
	--print("checkhp")
	if g_player.hp == 0 and not g_global.nowrelive then
		mc_tools.SetListCallBack(nil,"QuickRevive",trueRelive)
	elseif g_player.hp < 80 and not g_global.isbusy then
		local curtm = os.time()
		if g_player.menpai == 3 and g_player.lv >= 23 and not g_global.isfight and (curtm -g_global.kmtm) > 15.5 then
			g_global.kmtm = curtm
			WorldStage:SetSkillTargetToSelf(true)
			CastSkill(3915)
			WorldStage:SetSkillTargetToSelf(false)
		elseif not g_global.isfighting and g_player.hp < 30 and (curtm - xdtm) > 31 then
			local slot,bag = GetItemByName("气血仙丹")
			if slot then 
				xdtm = curtm
				UseItem(bag,slot) 
			end
		else
		--elseif (curtm-ywtm) > 31 then
			local slot,bag = GetItemByName("止血散",true)
			if slot then 
				ywtm = curtm
				UseItem(bag,slot) 
			end
		end
	end
end

function TeamTaskControl()
	local cmd,data,t,taskid
	repeat
		cmd,data = coroutine.yield("msk")
		print("TeamTaskControl get cmd:",cmd,data)
		if data then
			taskid = tonumber(data)
			if cmd == "at" then
				t = CreateTaskObj(taskid)
				if t:Accept(true) then
					SendChatMessage(g_CHAT_TYPE.TELL, "mskcmd#rsptok",g_global.teamleader)
				end
			elseif cmd == "st" then
				t = CreateTaskObj(taskid)
				if t:Submit(true) then
					SendChatMessage(g_CHAT_TYPE.TELL, "mskcmd#rsptok",g_global.teamleader)
				end
			elseif cmd == "rt" then
				t = CreateTaskObj(taskid)
				if t:Run(true) then
					SendChatMessage(g_CHAT_TYPE.TELL, "mskcmd#rsptok",g_global.teamleader)
				end
			elseif cmd == "rst" then
				t = CreateTaskObj(taskid)
				if t:Restart(true) then
					SendChatMessage(g_CHAT_TYPE.TELL, "mskcmd#rsptok",g_global.teamleader)
				end
			elseif cmd == "fm" then
				t = loadstring("tmpt = {"..data.."}")
				if t then
					t()
					FightMonster(tmpt[1],tmpt[2],tmpt[3],tmpt[4],tmpt[5],tmpt[6])
				end
			end
		end
	until cmd == "Exit"
end

function TestXcj()
	if g_global.xcjtest then return end
	g_global.xcjtest = true
	g_global.specia = "xcj"
	msk.send("xcjteam#2")
	while g_global.specia == "xcj" do
		ShowTaskState("等待组队完成仙草精任务")
		sleep(5)
	end
	do return end
	local lv = 99
	GetAllFriendList()
	for k,v in pairs(g_global.friends) do
		if k ~= g_player.guid then
			MskAskHomeInfo(0,k)
			sleep(1)
		end
	end
	local num = 5
	local taskid = 8100
	local status = GetQuestState(taskid)
	local t8100
	if status == QUEST_STATUS_AVAILABLE and CanQuestAccepted(taskid) == false then
		return mc_tools.CharExit("失败:无法升到指定等级-"..lv)
	end
	g_global.selfcanwater = nil
	DealSelfCasher()
	g_global.selfcanwater = true
	while g_global.selfcanwater == nil do
		MskAskHomeInfo(0,-1)
		sleep(2)
	end
	if g_global.selfcanwater == false then 
		return mc_tools.CharExit("失败:无法升到指定等级-"..lv)
	end
	local bInit = false
	while status ~= QUEST_STATUS_AVAILABLE or CanQuestAccepted(taskid) do
		t8100 = t8100 or CreateTaskObj(taskid)
		if status == QUEST_STATUS_AVAILABLE then
			t8100:Accept()
		elseif status == QUEST_STATUS_FAILED then
			t8100:Restart()
		elseif status == QUEST_STATUS_INCOMPLETE then
			if bInit == false then
				g_global.specia = "xcj"
				msk.send("xcjteam#2")
			end
			while g_global.specia == "xcj" do
				ShowTaskState("等待组队完成仙草精任务")
				sleep(5)
			end
		elseif status == QUEST_STATUS_COMPLETE then
			t8100:Submit()
		else
			print("unknow status",status)
		end
		if g_global.taskupdate == false then
			UpdateCanAcceptTaskList(false)
			sleep(1)
		end
		status = GetQuestState(taskid)
	end
end

local function _ChekRunList()
	CreateTaskObj(6024):Accept()
	CreateTaskObj(6025):Accept()
	CreateTaskObj(6026):Accept()
end

function AddFriendUpLevel(lv)
	if g_player.lv >= lv then
		print("角色等级足够",g_player.lv,lv)
		return true
	end
	print("SpeLevelCheck",g_player.lv,lv)
	_ChekRunList()
	ShowTaskState("给好友浇水")
	if false and lv < 31 then--活跃25就可以了
		g_global.waterlv = lv--waterlv 预留一个给自己 做活跃？
	end
	UI_HomeLandCash_ShowMain()
	for i=1,20 do--十分钟后 不管是否成功 都停止等待
		GetAllFriendList()
		for k,v in pairs(g_global.friends) do
			if k ~= g_player.guid then
				MskAskHomeInfo(0,k)
				sleep(1)
			end
		end
		if g_player.lv >= lv then 
			g_global.waterlv = nil
			break
		end
		_ChekRunList()
		msk.send("nfriend#"..g_player.guid)
		sleep(30)
	end
	UI_HomeLandCash_CloseHomeLandCash()
	g_global.waterlv = nil
	if g_player.lv >= lv then
		print("角色等级足够",g_player.lv,lv)
		return true
	end
	return false
end

--任务控制
function TaskControl(teammode)
	if not curruntasklist then
		return mc_tools.CharExit("失败:没有找到指定的任务脚本")
	end
	if g_global.teammode and not g_global.isteamleader then 
		--return TeamTaskControl() 
	end
	--TestXcj()
	g_global.mainthread = coroutine.running()
	print("TaskControl")
	local bgidx = 1
	local curthread,taskid,status,t
	local hasWater = false
	local hasCollect = false
	local firstLand = true
	local bWaterPrepare = false
	local bFirstPrepare = true
	local bContinue
	local taskcount
	local lastidx
	local logger = g_global.logger
	while true do
		--local --pl = g_player.chara\
		bContinue = false
		if g_global.specia ~= "normal" then--临时组队状态
			bContinue = true
			sleep(3)
		end
		if bContinue == false  then
			for k,v in pairs(g_global.runListTask) do
				if v then
					status = GetQuestState(k.id)
					if status == QUEST_STATUS_INCOMPLETE then
						k:Run()
					elseif status == QUEST_STATUS_COMPLETE then
						k:Submit()
					else
						g_global.runListTask[k] = false
					end
				end
			end
			curthread = nil
		end
		if bContinue == false and g_global.taskupdate then
			print("g_global.taskupdate ok")
			g_global.taskupdate = false
			local bBreak = false
			for i=bgidx,#curruntasklist do
				taskid = curruntasklist[i]
				if taskid == 7000 and g_player.lv > 27 then
					bgidx = i+1
					bBreak = true
					break
				end
				--if taskid == 601 then return mc_tools.CharExit("成功:准备测试组队") end
				if taskid == 601 and g_player.lv == 29 then
					AddFriendUpLevel()
					bBreak = true
					break
				end
				status = GetQuestState(taskid)
				print("TaskControl check:",taskid,status)
				if status ~= QUEST_STATUS_AVAILABLE or CanQuestAccepted(taskid) then
					print("get a task:",taskid,status)
					bgidx = i
					if lastidx == bgidx then
						taskcount = taskcount + 1
					else
						local tname = GetQuestInfoLua(taskid,0)
						msk.send("extern#task="..(tname or taskid))
						taskcount = 0
						lastidx = bgidx
					end
					if taskcount > 10 then
						taskcount = 0
						MoveToTargetLocation(g_player.x+math.random(-3,3), g_player.y+math.random(-3,3), GetMPlayerMapId())
						sleep(1)
					end
					t = CreateTaskObj(taskid)
					--g_global.numDead = 0
					if status == QUEST_STATUS_AVAILABLE then
						t:Accept()
					elseif status == QUEST_STATUS_FAILED then
						t:Restart()
						--msk.pcall(CTaskObj.Restart,t)
					elseif status == QUEST_STATUS_INCOMPLETE then
						t:Run()
						--msk.pcall(CTaskObj.Run,t)
					elseif status == QUEST_STATUS_COMPLETE then
						t:Submit()
						--msk.pcall(CTaskObj.Submit,t)
					else
						print("unknow status",status)
					end
					--if g_global.numDead > 0 then
						--local titleTask = GetCanAcceptQuestInfo(id)
						--logger:info("%s 死亡次数:%d",titleTask,g_global.numDead)
					--end
					if g_global.runListTask[t] then bgidx = bgidx + 1 end
					bBreak = true
					break
				--[[
				elseif taskid == 6034 then--某个任务代表着可以处理花园了
					bWaterPrepare = true
					--条件满足后去浇水吧
					if not hasWater and g_player.lv >= 30 and bFirstPrepare then
						bFirstPrepare = false
						bBreak = true
						break
					end
				--]]
				end
			end
			if not bBreak then
				local at = g_global.accountdata
				if at.script == "1_35" and g_player.lv < 35 then
					return mc_tools.CharExit("失败:无法升到指定等级-35")
				else--没有满级 重来
					return mc_tools.CharExit("成功")
					--bgidx = 1
				end
			end
		elseif bContinue == false then
			print("g_global.taskupdate not set")
			UpdateCanAcceptTaskList(false)
			sleep(1)
		end
	end
end

function InitTaskBase()
	--动画窗口
	--animationwnd = WindowSys_Instance:GetWindow("MissionPlot")
	--定义处理函数
	taskopdeal = {
		["移动"] = CTaskObj.MoveToCoord,
		["不处理"] = function () return true end,
		["等待"] = CTaskObj.Wait,
		["采集"] = CTaskObj.CollectObj,
		["采集1"] = CTaskObj.CollectObj1,--不需要移动到坐标 直接采集,一般用于传送到副本采集的任务,这些任务并没有采集物的坐标
		["SPE装备门派武器"] = CTaskObj.SpeEquipProWeapon,
		["SPE捕捉酥皮蟾蜍"] = CTaskObj.SpeCaptureToad,
		["SPE购买忏悔石"] = CTaskObj.SpeBuyChanHuiShi,
		["SPE解救中毒村民"] = CTaskObj.SpeRescueVillagers,
		["SPE初次锻造"] = CTaskObj.SpeForgeEquip,
		["SPE好友任务"] = CTaskObj.SpeAddFriend,
		["SPE仙草精"] = CTaskObj.SpeHomeBron,
		["SPE试炼"] = CTaskObj.SpeTrial,
		["SPE聚灵"] = CTaskObj.SpeAmass,
		["SPE等级判断"] = CTaskObj.SpeLevelCheck,
		["战斗"] = CTaskObj.FightMonster,
		["使用物品"] = CTaskObj.UseItem,
		["护送"] = CTaskObj.Escort,
		["对话"] = CTaskObj.TalkToNpc,
		["对话1"] = CTaskObj.TalkToNpc1,--对话完成后 任务提示没变的 其他都是根据任务提示判断是否完成,这个根据对话框是否消失判断
		["SPE击败自身心魔"] = CTaskObj.SpeSelfDayBreak,
		["飞行战斗"] = CTaskObj.VehicleFight,
	}
	--mc_tools.HookFunction("_OnQB_AddCanAcceptQuest",FilterCanAcceptTask)
	--加载任务信息
	msk.require("alltask3")
	msk.require("task_1_35")
	tmpsubtask = g_TaskSubInfo
	--mc_tools.SetLoop("CheckHp",CheckHp,5)
	--去掉泡泡
	Lua_Chat_ApplyPaoPao = function() end
end

function BeginTaskLoop(outcontrol,teammode)
	--加入任务控制
	if not outcontrol then
		--msk.require("task_1_35")
		curruntasklist = task_1_35
		mc_tools.SetCallBack(nil,"TaskControl",TaskControl,teammode)
	end
	UpdateCanAcceptTaskList(false)
	g_player:init()
	g_player.updatechara("BeginTaskLoop")
end

function AcceptTask(npcid,taskid)
    AcceptQuest(npcid, taskid, DIALOG_STATUS_AVAILABLE, 0) --DIALOG_STATUS_REWARD,-1 为完成任务
end

function SubmitTask(npcid,taskid)
	print("SubmitTask",npcid,taskid)
    AcceptQuest(npcid, taskid, DIALOG_STATUS_REWARD, -1) --DIALOG_STATUS_REWARD,-1 为完成任务
end

function CTaskObj:BeforeAccept()
	if self.beforeaccept then return true end
	self.beforeaccept  = true
	return true
end

function CTaskObj:Restart()
	if GetQuestState(self.id) ~= QUEST_STATUS_FAILED then 
		return false 
	end
	--animationwnd:SetVisible(false)
	local strTitle, mapname, npcname = GetCanAcceptQuestInfo(self.id)
	msk.assert(npcname,"找不到接任务的NPC:"..self.id)
	local x, y, mapid = string.match(npcname, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
    x = tonumber(x)
    y = tonumber(y)
    mapid = tonumber(mapid)
	if not PathTo(x, y, mapid) then return false end
    if mapid == -1 then
        ReAccectQuest(GetNPCObjId(x),self.id)
       sleep(2,"utask")
    end
    if GetQuestState(self.id) ~= QUEST_STATUS_FAILED then--accept suc
        return self:Run()
    end
    return false
end

function CTaskObj:Accept()
	if GetQuestState(self.id) == QUEST_STATUS_FAILED then 
		return self:Restart()
	end
	if GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE then 
		return self:Run()
	end
	if GetQuestState(self.id) == QUEST_STATUS_COMPLETE then 
		return self:Submit() 
	end
	if GetQuestState(self.id) ~= QUEST_STATUS_AVAILABLE or false == CanQuestAccepted(self.id) then 
		return false 
	end
	print("CTaskObj:Accept")
	local tmout
	--animationwnd:SetVisible(false)
    self:BeforeAccept()
	if quickaccepttask[self.id] then
		AcceptQuest(-1,  self.id,  4,  0)
		sleep(2,"utask")
	else
		local strTitle, mapname, npcname = GetCanAcceptQuestInfo(self.id)
		self.name = strTitle
		g_global.curTask = self
		local x, y, mapid
		if self.id == 401 or self.id == 8129 then
			x, y, mapid = 41,140,-1
		else
			x, y, mapid = string.match(npcname, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
			x = tonumber(x)
			y = tonumber(y)
			mapid = tonumber(mapid)
		end
		if not x then
			if npcname == "北落师门" then
				x, y, mapid = 544,50,-1
			elseif npcname == "青帝" then
				x, y, mapid = 443,50,-1
			else
				--msk.assert(x,"找不到接任务的NPC:"..self.id,npcname)
				sleep(1)
				return false
			end
		end
		if not PathTo(x, y, mapid,tmout,2) then return false end
		if mapid == -1 then
			if 7000 == self.id then
				SelectGossipOption(GetNPCObjId(x), 0, 0)
				sleep(1)
				SelectGossipOption(GetNPCObjId(x), 0, 0)
				sleep(1)
			else
				AcceptTask(GetNPCObjId(x),self.id)
				sleep(2,"utask")
			end
		end
		UI_Gossip_CloseBtnClick()
	end
    if GetQuestState(self.id) ~= QUEST_STATUS_AVAILABLE then--accept suc
        return self:AfterAccept() and self:Run()
    end
    return false
end

function CTaskObj:AfterAccept()
	if self.afteraccept then return true end
	self.afteraccept  = true
	if self.id == 401 or self.id == 138 then
		sleep(8)
	end
	return true
end

function CTaskObj:IsObjectiveOk(strObjective)
	print("check obj:",strObjective)
	if self.sub and #self.sub == 1 then
		if GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE then 
			return false
		else
			return true
		end
	end
	if self.subnum == 1 and  GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE then
		if GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE then 
			return false
		else
			return true
		end
	end
	strObjective = mc_tools.decodestr(strObjective)
	if not self.Desc then
		curruntask = self
		TrackQuest(self.id,true)
		UpdateTrackQuestList()
	end
	if not self.Desc then
		msk.warn("error UpdateTrackQuestList failed")
		return true
	end
	if string.find(self.Desc,strObjective) == nil then
		print("ObjectiveOk")
		return true
	end
	return false
end

function CTaskObj:Failed()
	curtasklist[self.id] = nil
end

function CTaskObj:BeforeRun()
	if self.beforerun then return true end
	if self.id == 303 then
		LearProSkill()
	end
	self.beforerun  = true
	return true
end

function CTaskObj:Wait(strObjective)
	print("Wait:")
	if self:IsObjectiveOk(strObjective) then return true end
	--msk.send("extern#state=等待任务完成")
	local wtm = 0
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE do
		if self:IsObjectiveOk(strObjective) then return true end
		if self.id == 314  and wtm > 40 then
			AbandonQuest(self.id)
			sleep(1)
			return false
		end
		sleep(3)
		if not isAutoVehicleMoving() then
			wtm = wtm + 3
		end
	end
	return true
end
local replaceStr = {
	["溢出的魔气"] = "魔气",
	["真正的阴谋者"] = "青女",
	["孤独心魔"] = "心魔",
	["流言心魔"] = "心魔",
	["争斗心魔"] = "心魔",
}

function CTaskObj:FightMonster(strObjective)
	print("FightMonster:")
	if self:IsObjectiveOk(strObjective) then return true end
	local tarname,x, y, mapid = string.match(strObjective, "{PaTh%^%[(%X+)%]%^[%xx]+%^([+-]?%d+)%^([+-]?%d+)%^([+-]?%d+)}")
	x, y, mapid = tonumber(x), tonumber(y), tonumber(mapid)
	if self.id == 641 then
		x, y, mapid  = 178,119,5
	elseif self.id == 5117 then
		x, y, mapid  = 32,334,2
		tarname = "灵熊"
	end
	if self.id == 805 then
		tarname = "幻狐巡逻者"
	else
		tarname = replaceStr[tarname] or tarname
	end
	num = num or 1
	local fit = false
	if self.id == 674 or self.id == 697 then
		fit = true
	end
	--msk.send("extern#state=打怪:"..(tarname or ""))
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE do
		if self:IsObjectiveOk(strObjective) then return true end
		FightMonster(tarname,x, y, mapid,120,fit)
		if 303 == self.id then
			MoveToTargetLocation(g_player.x+math.random(-3,3), g_player.y+math.random(-3,3), GetMPlayerMapId())
			sleep(1)
		end
	end
	return true
end

function CTaskObj:VehicleFight(strObjective)
	print("FightMonster:")
	if self:IsObjectiveOk(strObjective) then return true end
	local monster,skillid,skilltimer,skilldis
	if self.id == 520 then
		monster = "妖魔探军"
		skillid = 4109
		skilltimer = 3
		skilldis = 40
	else
		msk.warn("不支持的飞行战斗任务")
		return false
	end
	--msk.send("extern#state=飞行打怪:"..(monster or ""))
	local ch
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE do
		ch = GetCharByName(monster)
		if not ch or ch.dis >= skilldis then
			sleep(1)
		else
			CastAreaSkill(skillid,ch.x,ch.y)
			sleep(skilltimer+0.5)
		end
		if self:IsObjectiveOk(strObjective) then return true end
	end
	return true
end

function CTaskObj:SpeAmass(strObjective)
	--需要检测 新手武器锻造石
	local slot = GetItemByName("聚灵丹")
	if not slot then
		self:Failed()
		return false
	end
	--msk.send("extern#state=聚灵")
	_OnForge_ShowRootWin()
	sleep(1)
	Forge_Quick_Palce(0)
	sleep(1)
	UI_Forge_OkBtnClick()
	sleep(1)
	_Forge_CloseWindow()
	return GetQuestState(self.id) == QUEST_STATUS_COMPLETE
end

function CTaskObj:SpeBuyChanHuiShi(strObjective)
	local slot,bag,num = GetItemByName('加入帮会奖励礼包')
	if slot then
		UseItem(bag,slot)
		sleep(4,"utask")
		sleep(0.3)
	end
	return GetQuestState(self.id) == QUEST_STATUS_COMPLETE
end

function CTaskObj:SpeForgeEquip(strObjective)
	--需要检测 新手武器锻造石
	if false and GetRemainProtectTime() > 0 then
		g_global.runListTask[self] = true
		return true
	end
	local slot = GetItemByName("新手武器锻造石")
	if not slot then
		self:Failed()
		return false
	end
	--msk.send("extern#state=武器锻造")
	_OnForge_ShowRootWin()
	sleep(1)
	Forge_Quick_Palce(0)
	sleep(1)
	UI_Forge_OkBtnClick()
	sleep(1)
	UI_Forge_OkBtnClick()
	sleep(1)
	_Forge_CloseWindow()
	return GetQuestState(self.id) == QUEST_STATUS_COMPLETE
end

function CTaskObj:SpeAddFriend(strObjective)
	sleep(0.1)
	g_global.runListTask[self] = true
	return true
end

function CTaskObj:SpeLevelCheck(strObjective)
	local lv = strObjective:match("(%d+)级")
	lv = tonumber(lv) or 99
	do return AddFriendUpLevel(lv) or mc_tools.CharExit("失败:无法升到指定等级-"..lv) end
	print("等级限制",g_player.lv,lv)
	if g_player.lv < 30 then
		ShowTaskState("给好友浇水")
		for i=1,20 do--十分钟后 不管是否成功 都停止等待
			GetAllFriendList()
			for k,v in pairs(g_global.friends) do
				if k ~= g_player.guid then
					MskAskHomeInfo(0,k)
					sleep(1)
				end
			end
			if g_player.lv >= 30 then return true end
			msk.send("nfriend#"..g_player.guid)
			sleep(30)
		end
		return mc_tools.CharExit("失败:无法升到30级")
	elseif g_player.lv < lv then
		local t = CreateTaskObj(4502)--尝试异境试炼场
		t:Accept()
		LearProSkill()
		if g_player.lv >= lv then
			print("角色等级足够",g_player.lv,lv)
			return true
		end
		GetAllFriendList()
		for k,v in pairs(g_global.friends) do
			if k ~= g_player.guid then
				MskAskHomeInfo(0,k)
				sleep(1)
			end
		end
		sleep(3)
		DoXcjTask()
		sleep(2)
	end
	if g_player.lv >= lv then
		print("角色等级足够",g_player.lv,lv)
		return true
	end
	mc_tools.CharExit("失败:无法升到指定等级-"..lv)
end

function CTaskObj:SpeHomeBron(strObjective)
	DoXcjTask()
	g_global.runListTask[self] = true
	return GetQuestState(self.id) == QUEST_STATUS_COMPLETE 
end

function CTaskObj:UseItem(strObjective)
	print("UseItem:")
	--检查是否使用物品
	if self:IsObjectiveOk(strObjective) then return true end
	local gid = strObjective:match("GoOdSbOx^(%d+)}")
	gid = tonumber(gid)
	local num = 0
	local pl
	local bmove = false
	while gid do
		if num > 6 then return false end
		local x, y, mapid 
		if self.id == 6034 then
			x, y, mapid  = 176,109,140
		else
			x, y, mapid  = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
			x = tonumber(x)
			y = tonumber(y)
			mapid = tonumber(mapid)
		end
		if not PathTo(x, y, mapid) then return false end
		if num > 3 and not bmove then
			bmove = true
			--pl = g_player.chara
			MoveToTargetLocation(g_player.x+math.random(-3,3), g_player.y+math.random(-3,3), GetMPlayerMapId())
			sleep(1)
		end
		if mapid == -1 then
			local npcid = GetNPCObjId(x)
			if GetCurTargetObjId() ~= npcid then
				SelectCharById(npcid)
			end
		elseif self.id == 802 then
			local ch = GetCharByName("幻狐士兵")
			if not ch then return false end
			if GetCurTargetObjId() ~= ch.id then
				SelectCharById(ch.id)
			end
		elseif self.id == 916 then
			local ch = GetCharByName("魑魅")
			if not ch then return false end
			if GetCurTargetObjId() ~= ch.id then
				SelectCharById(ch.id)
			end
		elseif self.id == 854 then
			local ch = GetCharByName("灯蝶")
			if not ch then return false end
			if GetCurTargetObjId() ~= ch.id then
				SelectCharById(ch.id)
			end
		end
		local bag,slot = GetItemInBagIndex(gid)
		if not bag then return true end
		print("GetItemInBagIndex:",bag,slot)
		if self.id == 6034 then
			for i=1,4 do
				UseItem(bag,slot)
				sleep(4)
				bag,slot = GetItemInBagIndex(gid)
				if not bag then return true end
			end
		else
			UseItem(bag,slot)
			sleep(4,"utask")
			sleep(0.3)
		end
		if self:IsObjectiveOk(strObjective) then return true end
		num = num + 1
	end
	return true
end

function CTaskObj:SpeEquipProWeapon(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	print("SpeEquipProWeapon")
	local slot,bag = GetItemByType(2,1)
	if slot then 
		UseItem(bag,slot) 
		sleep(1) 
	end
	if not self:IsObjectiveOk(strObjective) then
		--local --pl = g_player.chara
		MoveToTargetLocation(g_player.x+math.random(-3,3), g_player.y+math.random(-3,3), GetMPlayerMapId())
		return false
	end
	return true
end

function CTaskObj:SpeSelfDayBreak(strObjective)
	print("SpeSelfDayBreak:")
	if self:IsObjectiveOk(strObjective) then return true end
	local tarname = g_player.menpainame.."心魔"
	FightMonster(tarname,x, y, mapid,120)
	return self:IsObjectiveOk(strObjective)
end

function CTaskObj:SpeTrial(strObjective)
	g_global.runListTask[self] = true
	local trial = mc_tools.GetData'trial'
	if trial then return false end
	local cmap = GetMPlayerMapId()
	if cmap ~= 108 and cmap ~= 111 and cmap ~= 112 and not EnterInstance(2075,50,-1,0,1) and not EnterInstance(2075,50,-1,0,1) then
		return false
	end
	_OnForge_ShowRootWin()
	sleep(1)
	Forge_Quick_Palce(0)
	sleep(1)
	UI_Forge_OkBtnClick()
	sleep(1)
	UI_Forge_OkBtnClick()
	sleep(1)
	UI_Forge_OkBtnClick()
	sleep(1)
	UI_Forge_OkBtnClick()
	sleep(1)
	_Forge_CloseWindow()
	g_global.reliveid = 6
	sleep(2)
	local kBtn = WindowSys_Instance:GetWindow("Reward")
	local old = g_global.areafight
	while GetMPlayerMapId() ~= 50 do
		--print('curmap:',GetMPlayerMapId())
		if g_global.triallv == 15 then
			break
		end
		if g_global.triallv == 9 then
			g_global.areafight = false
			g_global.ksidx = 1
		elseif g_global.triallv == 10 or g_global.triallv == 15 then
			g_global.areafight = false
			g_global.ksidx = nil
		else
			g_global.areafight = true
			g_global.ksidx = nil
		end
		if kBtn:IsVisible() then
			UI_CarnivalUI_GoOn()
		else
			local ch = GetEnemy()
			while ch do
				FightChar(ch)
				ch = GetEnemy()
			end
		end
		sleep(1)
	end
	g_global.areafight = old
	g_global.reliveid = nil
	mc_tools.SetData('trial',true)
	return self:IsObjectiveOk(strObjective)
end


function CTaskObj:SpeCaptureToad(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	while not GetMyCharByName("酥皮蟾蜍") do
		if GetQuestState(self.id) ~= QUEST_STATUS_INCOMPLETE then return true end
		local gid = strObjective:match("GoOdSbOx^(%d+)}")
		gid = tonumber(gid)
		if gid then
			local x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
			x = tonumber(x)
			y = tonumber(y)
			mapid = tonumber(mapid)
			local ch = GetCharByName("酥皮蟾蜍")
			if not ch and not PathTo(x, y, mapid) then return false end
			if not ch then ch = GetCharByName("酥皮蟾蜍") end
			if not ch then return false end
			ch:Select()
			MoveToTargetLocation(ch.x, ch.y, GetMPlayerMapId())
			sleep(1)
			--[[
			local hp, _, _, _, _, tname = GetTargetEaseChangeInfo(1,ch.id)
			while hp and hp > 0 and tname ~= g_player.name do
				local stm = AttackOne(ch)
				sleep(stm)
				hp, _, _, _, _, tname = GetTargetEaseChangeInfo(1,ch.id)
			end
			--]]
			if true or hp then
				local bag,slot = GetItemInBagIndex(gid)
				print("GetItemInBagIndex:",bag,slot)
				UseItem(bag,slot)
				sleep(4,"spell")
				return self:IsObjectiveOk(strObjective)
			end
		end
	end
	return true
end

function CTaskObj:SpeRescueVillagers(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE  do
		local gid = strObjective:match("GoOdSbOx^(%d+)}")
		gid = tonumber(gid)
		if gid then
			local x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
			x = tonumber(x)
			y = tonumber(y)
			mapid = tonumber(mapid)
			if not PathTo(x, y, mapid) then return false end
			local ch = GetCharByName("中毒村民")
			if ch then
				ch:Select()
				local bag,slot = GetItemInBagIndex(gid)
				print("GetItemInBagIndex:",bag,slot)
				UseItem(bag,slot)
				sleep(4,"spell")
			else
				sleep(1)
			end
		end
	end
	return true
end

function CTaskObj:Escort(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	print("CTaskObj Escort:")
	local x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
	local num = 0
	--local --pl = g_player.chara
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE do
		if self.id == 7000 and g_player:Dis(300,73) > 125 then
			Escort(nil,398,159,4)
		end
		Escort(nil,x,y,mapid)
		num = num + 1
		if num > 1 then
			sleep(2)
			if num > 10 then 
				msk.warn("护送失败次数过多,放弃任务")
				AbandonQuest(self.id)
				sleep(1)
				return false
			end
		end
	end
	return self:IsObjectiveOk(strObjective)
end

function CTaskObj:MoveToCoord(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	print("MoveToCoord:")
	local x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
	if not PathTo(x, y, mapid) then 
		--local --pl = g_player.chara
		MoveToTargetLocation(g_player.x+math.random(-3,3), g_player.y+math.random(-3,3), GetMPlayerMapId())
		sleep(1)
		return false 
	end
	return self:IsObjectiveOk(strObjective)
end

function CTaskObj:CollectObj(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	print("CollectObj:")
	local x, y, mapid
	if self.id == 715 then
		x, y, mapid = 487,493,5
	else
		x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
		x, y, mapid = tonumber(x), tonumber(y), tonumber(mapid)
	end
	--if not PathTo(x, y, mapid) then return false end
	local rcount = 0
	local clf = CollectObj
	if self.id == 340 then
		clf = CollectNearlistObj
	end
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE do
		if self:IsObjectiveOk(strObjective) then return true end
		if not clf(nil,x, y, mapid,120) then sleep(1) end
		rcount = rcount + 1
		if rcount > 8 then
			rcount = 0
			MoveToTargetLocation(g_player.x+math.random(-3,3), g_player.y+math.random(-3,3), GetMPlayerMapId())
			sleep(1)
		end
	end
	return true
end

function CTaskObj:CollectObj1(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	print("CollectObj:")
	if not CollectObj(nil,nil, nil, nil,120) then sleep(1) end
	return true
end

function CTaskObj:TalkToNpc(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	print("TalkToNpc:")
	UI_Gossip_CloseBtnClick()--必须关闭原来的对话框 否则对话无法继续
	local x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
	x, y, mapid = tonumber(x),tonumber(y),tonumber(mapid)
	--msk.assert(mapid == -1,"子任务对话信息有误,地图不是-1 :"..self.id)
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE do
		if not PathTo(x, y, mapid) then return false end
		local npcId = gUI.Gossip.CurNpcId
		if mapid ~= -1 then
			local tarname = string.match(strObjective, "{PaTh%^%[(.+)%]%^.+%^[+-]?%d+%^[+-]?%d+%^[+-]?%d+}")
			local ch = GetCharByName(tarname)
			if ch then 
				npcId = ch.id
				if not PathTo(ch.x, ch.y, GetMPlayerMapId(),nil,1) then return false end
				ch:Open()
			end
		end
		if npcId then
			for j=1,8 do
				SelectGossipOption(npcId,0,0)
				sleep(1)
				if self:IsObjectiveOk(strObjective) then return true end
			end
		else
			msk.warn("无法获取对话NPCID:"..self.id)
			break
		end
	end
	return self:IsObjectiveOk(strObjective)
end

function CTaskObj:TalkToNpc1(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	print("TalkToNpc1:")
	UI_Gossip_CloseBtnClick()--必须关闭原来的对话框 否则对话无法继续
	local x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
	x, y, mapid = tonumber(x),tonumber(y),tonumber(mapid)
	--msk.assert(mapid == -1,"子任务对话信息有误,地图不是-1 :"..self.id)
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE do
		if not PathTo(x, y, mapid) then return false end
		local npcId = gUI.Gossip.CurNpcId
		if mapid ~= -1 then
			local tarname = string.match(strObjective, "{PaTh%^%[(.+)%]%^.+%^[+-]?%d+%^[+-]?%d+%^[+-]?%d+}")
			local ch = GetCharByName(tarname)
			if ch then 
				npcId = ch.id
				if not PathTo(ch.x, ch.y, GetMPlayerMapId(),nil,1) then return false end
				ch:Open()
			end
		end
		if npcId then
			for j=1,8 do
				SelectGossipOption(npcId,0,0)
				sleep(1)
				if self:IsObjectiveOk(strObjective) then return true end
				--if gUI.Gossip.Root:GetProperty("Visible") == "false" then return true end
			end
			break
		else
			msk.warn("无法获取对话NPCID:"..self.id)
			break
		end
	end
	return true
	--return self:IsObjectiveOk(strObjective)
end

function CTaskObj:Run()
	if GetQuestState(self.id) == QUEST_STATUS_COMPLETE then 
		return self:Submit() 
	end
	if GetQuestState(self.id) ~= QUEST_STATUS_INCOMPLETE then 
		print("Run err task complete:"..self.id)
		return false 
	end
	print("CTaskObj:Run")
	self:BeforeRun()
	curruntask = self
	--TrackQuest(self.id,true)
	--animationwnd:SetVisible(false)
	local st = tmpsubtask[self.id]
	local runOk = true
	local dealfun
	if st then
		for i=#st,1,-1 do
			if st[i][2] == "不处理" then
				table.remove(st,i)
			end
		end
		self.sub = st
		self.subnum = #st
		for i=1,#st do
			print("新的子任务",st[i][2],st[i][1])
			if string.find(st[i][1],"QuTrAnS") then
				print("准备传送:"..self.id)
				AcceptQuestTrans(self.id)
				sleep(1)
			end
			dealfun = taskopdeal[st[i][2]]
			if not dealfun then msk.warn("无法处理的任务类新:"..(st[i][2] or nil)) break end
			if not dealfun(self,st[i][1]) then msk.warn("任务失败:"..(st[i][2] or nil)) break end
			self.subnum = self.subnum - 1
		end
	else
		msk.warn("there is no SubInfo:",self.id)
	end
	print("CTaskObj:Run Over")
	return self:AfterRun() and self:Submit()
end

function CTaskObj:AfterRun()
	if self.afterrun then return true end
	self.afterrun  = true
	return true
end


function CTaskObj:BeforeSubmit()
	if self.beforesubmit then return true end
	self.beforesubmit  = true
	if self.id == 303 then
		sleep(3)
	end
	if self.id == 401 and GetMPlayerMapId() == 140 then
		sleep(8)
	end
	return true
end

function CTaskObj:Submit()
	if GetQuestState(self.id) ~= QUEST_STATUS_COMPLETE then 
		return false 
	end
	--animationwnd:SetVisible(false)
	print("CTaskObj:Submit")
	self:BeforeSubmit()
	local x, y, mapid
	if  self.id == 6033 then
		x,y,mapid = 41,140,-1
	else
		if self.Desc then
			if string.find(self.Desc,"QuTrAnS") then
				print("准备传送:"..self.id)
				AcceptQuestTrans(self.id)
				sleep(1)
			end
			x, y, mapid = string.match(self.Desc, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
		end
		if not x then
			local _, strObjective, _, _, _, _, _, _, NpcName = GetQuestInfoLua(self.id,0)
			if string.find(strObjective,"QuTrAnS") then
				print("准备传送:"..self.id)
				AcceptQuestTrans(self.id)
				sleep(1)
			end
			msk.assert(NpcName or strObjective,"无法获取任务完成NPC:"..self.id)
			x, y, mapid = string.match(NpcName, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
			if not x then
				x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
			end
		end
		x = tonumber(x)
		y = tonumber(y)
		mapid = tonumber(mapid)
	end
	msk.assert(x,"找不到完成任务NPC:",GetQuestInfoLua(self.id,0))
	if not PathTo(x, y, mapid) then return false end
    if mapid == -1 then
        SubmitTask(GetNPCObjId(x),self.id)
		sleep(2,"utask")
    end
    if GetQuestState(self.id) == 4 then--accept suc
        self:AfterSubmit()
		g_global.curTask = nil
        return true
    end
    return false
end

function CTaskObj:AfterSubmit()
	if g_global.taskupdate == false then
		UpdateCanAcceptTaskList(false)
	end
	if self.id == 546 then
		AddFriendUpLevel(30)
		sleep(2)
	end
	if self.id == 150 then
		sleep(5)
	end
	if self.id == 5117 then
		DealBag()
	end
	if self.id == 4502 then
		DealBag()
	end
	if self.id == 8100 then
		AcceptQuest(-1,  self.id,  4,  0)
		sleep(2,"utask")
		if GetQuestState(8100) == 4 then
			local t = CreateTaskObj(8100)
			g_globalt.runListTask[t] = true
		end
	elseif self.id == 6024 then
		AcceptQuest(-1,  6025,  4,  0)
		sleep(2,"utask")
		if GetQuestState(6025) == 4 then
			local t = CreateTaskObj(6025)
			g_globalt.runListTask[t] = true
		end
	elseif self.id == 6025 then
		AcceptQuest(-1,  6026,  4,  0)
		sleep(2,"utask")
		if GetQuestState(6026) == 4 then
			local t = CreateTaskObj(6026)
			g_globalt.runListTask[t] = true
		end
	end
	if g_player.lv > 26 and g_global.fightback then
		FightPursuers()
	end
	if self.aftersubmit then return true end
	self.aftersubmit  = true
	return true
end
InitTaskBase()
return CTaskObj
