msk.dofile("global")
msk.require("toolsapi")
msk.require("player")
msk.require("task")

local cfg = cfg
local math = math
local string = string
local os = os
local msk = msk
local sig = sig
local g_player = g_player
local g_global = g_globalt
local sleep = mskco.Sleep
local Names = Names
local curNearChar = 0

local base = msk.getmodulebase("user32.dll")
msk.initdelaydata("TGClientWnd",base+0x57e21)
--角色管理器
local call_CsCharacterManager_GetSingleton = msk.getcalladdress("TG_Engine.dll","?getSingletonPtr@CsCharacterManager@CrossEngine@@SAPAV12@XZ")
local g_CharMn = msk.qcall(call_CsCharacterManager_GetSingleton,0,2)
print("g_CharMn:",g_CharMn)
--拾取物品管理器
local call_DynamicObjectManager_GetSingleton = msk.getcalladdress("TG_Engine.dll","?getSingletonPtr@DynamicObjectManager@CrossEngine@@SAPAV12@XZ")
local g_DmObjMn = msk.qcall(call_DynamicObjectManager_GetSingleton,0,2)
print("g_DmObjMn:",g_DmObjMn)
--取角色坐标
local call_CsCharacter_getPosition = msk.getcalladdress("TG_Engine.dll","?getPosition@CsCharacter@CrossEngine@@QBEABVVector3@Ogre@@XZ")
print("call_CsCharacter_getPosition:",call_CsCharacter_getPosition)

local call_findPath = msk.getcalladdress("TG_Engine.dll","?findPath@TerrainPathManager@CrossEngine@@UAE_NABVVector3@Ogre@@0H_N@Z")
print("call_findPath:",call_findPath)
--随机种子
math.randomseed(os.time())

function GetCharsBase()
	local esi = msk.dword(sig.charsbase)
	return msk.dword(esi+sig.charsoff)
end

function CastAreaSkill(id,x,y)
	local edi = GetCharsBase()
	mskg.castareaskill(sig.castareaskillcall,edi,id,x,y)
end

function GetCharPos(data)
	if not data or data == 0 then return 0,0,0 end
	local paddr = msk.qcall(call_CsCharacter_getPosition,data,2)
	return msk.float(paddr),msk.float(paddr+4),msk.float(paddr+8)
end

local CharMt = {}

local selectData = sig.selectBase

function SelectCharById(id)
	local edi = msk.dword(selectData)
	edi = msk.dword(edi+0x24)
	edi = msk.dword(edi+0xc)
	mskg.selectchar(sig.selectcharcall,edi,id)
end

function SelectCharById2(id)
	local root = msk.dword(selectData)
	root = msk.dword(root+0x24)
	root = msk.dword(root+0xc)
	msk.sdword(root+sig.selectId,id)
	_OnPlays_TargetChanged(id)
	_CombatStat_TargetChange(id)
end

function OpenCharById(id)
	local esi = GetCharsBase()
	mskg.openchar(sig.opencharcall,esi,id)
end

function CharMt:Open()
	OpenCharById(self.id)
end

function CharMt:Select()
	SelectCharById(self.id)
end

function CharMt:Dis(x,y)
	local mx,my = x-self.x,y-self.y
	return math.sqrt(mx*mx+my*my)
end

function GetSelectId()
	local root = msk.dword(selectData)
	root = msk.dword(root+0x24)
	root = msk.dword(root+0xc)
	local id = msk.dword(root+sig.selectId)
	if id ~= -1 then return id end
end

CharMt.__index = function (t,key)
	if CharMt[key] then return CharMt[key] end
	if key == "x" then
		return msk.float(t.posaddr)
	elseif key == "y" then
		return msk.float(t.posaddr+8)
	elseif key == "z" then
		return msk.float(t.posaddr+4)
	elseif key == "hasteam" then
		local _, _, _, _, target_hasTeam = GetTargetEaseChangeInfo(0,t.id)
		return target_hasTeam
	elseif key == "hp" then
		return GetTargetEaseChangeInfo(0,t.id) or 0
	elseif key == "mp" then
		local _,mp = GetTargetEaseChangeInfo(0,t.id)
		return mp
	elseif key == "lv" then
		local _, _, _, level = GetTargetEaseChangeInfo(0,t.id)
		return level
	elseif key == "dis" then
		return GetCharacterDistance(t.id) or 999
		--[[
		local player = g_player.chara
		local x,y = t.x-player.x,t.y-player.y
		return math.sqrt(x*x+y*y)
		--]]
	end
	print("try a char cunsupport key;"..key)
end

function Create(class,data,o)
	if not data or data == 0 then msk.warn("try Crate a unvilid data class:"..class) return end
	o = o or {}
	if class == "char" then
		o.data = data
		o.id = msk.dword(data+0xc)
		o.posaddr = msk.qcall(call_CsCharacter_getPosition,data,2)
		setmetatable(o,CharMt)
		o.char_type, _, o.bHideName, o.name, o.menpai, o.gender, o.head_id, o.hair_model, o.hair_color, o.guildName = GetTargetBaseInfo(o.id)
		return o
	end
	return o
end

function GetCharByType(tartp,notnear,name)
	local root = g_CharMn
	local head = msk.dword(root+0x48)
	local data = msk.dword(head)
	local id,chara,lastdis,lastchara,lasttype
	local o = {}
	local to,tp
	while data ~= 0 and data ~= head do
		id = msk.dword(data+8)
		chara = msk.dword(data+0xc)
		tp = GetTargetBaseInfo(id)
		if tp == tartp then
			to = Create("char",chara,o)
			to.type = tp
			if notnear and (not name or name == to.name) then return to end 
			if to and to.hp > 0 and  (not lastdis or lastdis > to.dis) then
				lastchara = chara
				lastdis = to.dis
				lasttype = char_type
			end
		end
		data = msk.dword(data)
	end
	if lastchara then 
		o = Create("char",lastchara,o) 
		o.type = lasttype
		return o
	end
end

function GetMyCharByName(tname,notnear,fit)
	fit = fit or false
	--玩家 是个链表
	local root = g_CharMn
	local head = msk.dword(root+0x48)
	--print("head:"..string.format("%x",head))
	local data = msk.dword(head)
	local id,chara,lastdis,lastchara,lasttype
	local o = {}
	local to
	while data ~= 0 and data ~= head do
		id = msk.dword(data+8)
		chara = msk.dword(data+0xc)
		local char_type, _, _, name, _, _, _, _, name_owner = GetTargetBaseInfo(id)
		if (char_type == 2 or char_type == 5) and name_owner == g_player.name and name then
			if not tname or (fit and name == tname) or (not fit and string.find(name,tname)) then
				--print("get a fit:"..name)
				to = Create("char",chara,o)
				to.type = char_type
				if notnear then return to end 
				if to and to.hp > 0 and  (not lastdis or lastdis > to.dis) then
					lastchara = chara
					lastdis = to.dis
					lasttype = char_type
				end
			end
		end
		data = msk.dword(data)
	end
	if lastchara then 
		o = Create("char",lastchara,o) 
		o.type = lasttype
		return o
	end
end

function GetCharById(tarid)
	if not tarid or tarid == 0 then return end
	local root = g_CharMn
	local head = msk.dword(root+0x48)
	local data = msk.dword(head)
	local id,chara
	local o = {}
	local to
	while data ~= 0 and data ~= head do
		id = msk.dword(data+8)
		if id == tarid then
			chara = msk.dword(data+0xc)
			to = Create("char",chara,o)
			to.type = char_type
			return to
		end
		data = msk.dword(data)
	end
end

function IsTargerMe(id,sel)
	local _, _, _, _, _, tname = GetTargetEaseChangeInfo(1,id)
	if sel or not g_global.teammode then return tname == g_player.name end
	local mn = g_global.teammemernames
	for i=1,#mn do
		if tname == mn[i] then return true end
	end
	return false
end


function GetPursuerNum()
	local root = g_CharMn
	local head = msk.dword(root+0x48)
	--print("head:"..string.format("%x",head))
	local data = msk.dword(head)
	local id,chara,lastdis,lastchara,lasttype
	local o = {}
	local to
	local char_type,name,name_owner,dis
	local nlist = 0
	while data ~= 0 and data ~= head do
		id = msk.dword(data+8)
		chara = msk.dword(data+0xc)
		--local char_type, _, _, name = GetTargetBaseInfo(id)
		dis = GetCharacterDistance(id) or 999
		if IsCharEnemy(id) and dis < 15 and IsTargerMe(id) then
			nlist = nlist + 1
		end
		data = msk.dword(data)
	end
	return nlist
end

function GetPursuers()
	local root = g_CharMn
	local head = msk.dword(root+0x48)
	--print("head:"..string.format("%x",head))
	local data = msk.dword(head)
	local id,chara,lastdis,lastchara,lasttype
	local o = {}
	local to
	local char_type,name,name_owner,dis
	local list = {}
	while data ~= 0 and data ~= head do
		id = msk.dword(data+8)
		chara = msk.dword(data+0xc)
		--local char_type, _, _, name = GetTargetBaseInfo(id)
		dis = GetCharacterDistance(id) or 999
		if IsCharEnemy(id) and dis < 15 and IsTargerMe(id) then
			char_type, _, _, name, _, _, _, _, name_owner = GetTargetBaseInfo(id)
			to = Create("char",chara,o)
			to.type = char_type
			list[#list+1] = to
		end
		data = msk.dword(data)
	end
	return list
end

function FightPursuers()
	local list = GetPursuers()
	for i=1,#list do
		if list[i].dis < 10 and list[i].hp > 0 then
			FightChar(list[i])
		end
	end
end

function GetEnemy()
	--玩家 是个链表
	curNearChar = 0
	local root = g_CharMn
	local head = msk.dword(root+0x48)
	--print("head:"..string.format("%x",head))
	local data = msk.dword(head)
	local id,chara,lastdis,lastchara,lasttype
	local o = {}
	local to
	local char_type,name,name_owner
	while data ~= 0 and data ~= head do
		id = msk.dword(data+8)
		chara = msk.dword(data+0xc)
		--local char_type, _, _, name = GetTargetBaseInfo(id)
		if IsCharEnemy(id) then
			char_type, _, _, name, _, _, _, _, name_owner = GetTargetBaseInfo(id)
			to = Create("char",chara,o)
			to.type = char_type
			if name_owner == g_player.name then return to end 
			if to.dis < s_config.qundis then curNearChar = curNearChar+1 end
			if to and to.hp > 0 and  (not lastdis or lastdis > to.dis) then
				lastchara = chara
				lastdis = to.dis
				lasttype = char_type
			end
		end
		data = msk.dword(data)
	end
	if lastchara then 
		o = Create("char",lastchara,o) 
		o.type = lasttype
		return o
	end
end

function GetCharByName(tname,notnear,fit,fight)
	fit = fit or false
	fight = fight or false
	--玩家 是个链表
	curNearChar = 0
	local root = g_CharMn
	local head = msk.dword(root+0x48)
	--print("head:"..string.format("%x",head))
	local data = msk.dword(head)
	local id,chara,lastdis,lastchara,lasttype
	local o = {}
	local to
	while data ~= 0 and data ~= head do
		id = msk.dword(data+8)
		chara = msk.dword(data+0xc)
		local char_type, _, _, name, _, _, _, _, name_owner = GetTargetBaseInfo(id)
		--local char_type, _, _, name = GetTargetBaseInfo(id)
		if name and (not fight or IsCharEnemy(id)) and (name_owner == nil or name_owner == "" or name_owner == g_player.name) then
			if (fit and name == tname) or (not fit and string.find(name,tname)) then
				--print("get a fit:"..name)
				to = Create("char",chara,o)
				to.type = char_type
				if notnear or name_owner == g_player.name then return to end 
				if to.dis < s_config.qundis then curNearChar = curNearChar+1 end
				if to and to.hp > 0 and  (not lastdis or lastdis > to.dis) then
					lastchara = chara
					lastdis = to.dis
					lasttype = char_type
				end
			end
		end
		data = msk.dword(data)
	end
	if lastchara then 
		o = Create("char",lastchara,o) 
		o.type = lasttype
		return o
	end
end

function GetCharByTask(taskid,notnear,tasktp)
	--玩家 是个链表
	local root = g_CharMn
	local head = msk.dword(root+0x48)
	tasktp = tasktp or 3--正在做的任务
	--print("head:"..string.format("%x",head))
	local data = msk.dword(head)
	local id,chara,lastdis,lastchara
	local o = {}
	local to
	local list,bfind
	while data ~= 0 and data ~= head do
		id = msk.dword(data+8)
		chara = msk.dword(data+0xc)
		local char_type, _, _, _ = GetTargetBaseInfo(id)
		list = GetNpcQuestList(objId, tasktp)
		bfind = false
		--GetQuestMonsterInfo ?
		if list[0] ~= nil then
			for i = 0, table.maxn(list) do
				if list[i]==taskid then
					bfind = true
					break
				end
			end
		end
		if bfind then
			to = Create("char",chara,o)
			to.type = char_type
			if notnear then return to end 
			if to and to.hp > 0 and  (not lastdis or lastdis > to.dis) then
				lastchara = chara
				lastdis = to.dis
			end
		end
		data = msk.dword(data)
	end
	if lastchara then 
		o = Create("char",lastchara,o) 
		o.type = lasttype
		return o
	end
end

function GetNpcByTemplateId(npcid,map)
	local TargetList = GetTargetList(map)
	for _, item in spairs(TargetList) do
		if item.TemplateID == npcid then return item end
	end
end

function PrintAllChar()
	--玩家 是个链表
	local root = g_CharMn
	local head = msk.dword(root+0x48)
	print("head:"..string.format("%x",head))
	local num = msk.dword(root+0x4c)
	print("周围怪物数量:"..num)
	local data = msk.dword(head)
	--local --pl = g_player.chara
	local id,chara,name,tname,tid
	while data ~= 0 and data ~= head do
		id = msk.dword(data+8)
		chara = msk.dword(data+0xc)
		_,_,_,name = GetTargetBaseInfo(id)
		_, _, _, _, _, tname, _, _, _, _, _, _, tid = GetTargetEaseChangeInfo(1,id)
		print(string.format("%x",id),IsCharEnemy(id) and "敌人" or "友军",GetCharacterDistance(id),name,'--->',tname,tid)
		data = msk.dword(data)
	end
end

function GetDynamicObjPosPtr(data)
	data = msk.dword(data+0x24)
	data = msk.dword(data+0x28)
	return data+0x50
end

function CanDynamicObjPick(obj)
	--local id = msk.dword(obj+0xc)
	--if msk.isobjlock(id) then return false end
	if msk.byte(obj+sig.dynamicObjCanPick) == 0 then return false end
	return true
end

--动态物品 树
function PrintDynamicObj(data)
	if not data then 
		data = g_DmObjMn
		data = msk.dword(data+0x10)
		data = msk.dword(data+4)
	end
	if data == 0 or msk.byte(data+sig.dynamicObjFlag) == 1 then
		return 
	end
	PrintDynamicObj(msk.dword(data))
	local obj = data+0x10
	local bcanpick = msk.byte(obj+0x148)
	local strcanpick = "可拾取"
	if bcanpick == 0 then
		strcanpick = "不可拾取"
	end
	--local --pl = g_player.chara
	local posptr = GetDynamicObjPosPtr(obj)
	local x,y = msk.float(posptr),msk.float(posptr+8)
	local id = msk.dword(data+0xc)
	local name = ""
	local namelen = msk.dword(data+0x30)
	local nameaddr = data+0x1c
	if namelen > 0xf then
		nameaddr = msk.dword(nameaddr)
	end
	name=msk.str(nameaddr)
	print("dynamicobj:"..string.format("%x %d,%d dis:%d",id,x,y,g_player:Dis(x,y)).." "..name.." "..strcanpick)
	PrintDynamicObj(msk.dword(data+8))
end

--动态物品 树
function PrintDynamicObj2(data)
	if not data then 
		data = g_DmObjMn
		data = msk.dword(data+4)
		data = msk.dword(data+4)
	end
	if data == 0 or msk.byte(data+sig.dynamicObjFlag2) == 1 then
		return 
	end
	PrintDynamicObj(msk.dword(data))
	local id = msk.dword(data+0xc)
	local name = ""
	local namelen = msk.dword(data+0x2c)
	local nameaddr = data+0x18
	if namelen > 0xf then
		nameaddr = msk.dword(nameaddr)
	end
	name=msk.str(nameaddr)
	print("dynamicobj:"..string.format("%x",id).." "..name)
	PrintDynamicObj(msk.dword(data+8))
end

local neadlistdmobj,neadlistdmname,neadlistdmobj2,neadlistdmname2
local tmpx,tmpy,tmpx2,tmpy2
local nearlistobjs = {}
local nearlistdiss = {}
local nearlistobjnames = {}
local nearlistnum = 0
local bigdisidx = 0
function _FindByName(data,tname,fit,pick,nearlist)
	if data == 0 or msk.byte(data+sig.dynamicObjFlag) == 1 then
		return 
	end
	local name = ""
	local namelen = msk.dword(data+0x30)
	local nameaddr = data+0x1c
	if namelen > 0xf then
		nameaddr = msk.dword(nameaddr)
	end
	name=msk.str(nameaddr)
	--print("check:"..name)
	--local --pl = g_player.chara
	local posptr,x,y,id,dis
	local pickcheck = false
	if not pick or CanDynamicObjPick(data+0x10) then pickcheck = true end
	if pickcheck and ( not tname or (fit and name == tname) or (not fit and string.find(name,tname))) then
		posptr = GetDynamicObjPosPtr(data+0x10)
		x,y = msk.float(posptr),msk.float(posptr+8)
		id = msk.dword(data+0xc)
		dis = g_player:Dis(x,y)
		if not nearlist then
			if nearlistnum == 0 then
				nearlistnum = nearlistnum + 1
				nearlistobjs[nearlistnum] = data+0x10
				nearlistobjnames[nearlistnum] = name
				nearlistdiss[nearlistnum] = dis
			elseif nearlistnum < 10 then
				nearlistnum = nearlistnum + 1
				nearlistdiss[nearlistnum] = 99999
				for i=1,nearlistnum do
					if dis < nearlistdiss[i] then
						--print('insert',i,name,dis,nearlistdiss[i])
						table.insert(nearlistobjs,i,data+0x10)
						table.insert(nearlistobjnames,i,name)
						table.insert(nearlistdiss,i,dis)
						break
					end
				end
			else
				return
			end
		end
		if not neadlistdmobj or g_player:Dis(tmpx,tmpy) > dis then
			neadlistdmobj = data+0x10
			neadlistdmname = name
			if msk.isobjlock(id) == false then
				neadlistdmobj2 = neadlistdmobj
				neadlistdmname2 = neadlistdmname
				tmpx2,tmpy2 = x,y
			end
			tmpx,tmpy = x,y
		elseif msk.isobjlock(id) == false and (not neadlistdmobj2 or g_player:Dis(tmpx2,tmpy2) > dis) then
			neadlistdmobj2 = data+0x10
			neadlistdmname2 = name
			tmpx2,tmpy2 = x,y
		end
	end
	return _FindByName(msk.dword(data),tname,fit,pick,nearlist) or _FindByName(msk.dword(data+8),tname,fit,pick,nearlist)
end

function GetGameObjectByName(name,fit,pick,nearlist)
	print("GetGameObjectByName:"..(name or "nil"))
	nearlistnum = 0
	if pick == nil then pick = true end
	fit= fit or false
	neadlistdmobj,neadlistdmname,neadlistdmobj2,neadlistdmname2 = nil
	local data = g_DmObjMn
	data = msk.dword(data+0x10)
	data = msk.dword(data+4)
	_FindByName(data,name,fit,pick,nearlist)
	--随机处理
	if not nearlist and nearlistnum > 0 then
		local objidx = math.random(1,nearlistnum)
		local obj,name =  nearlistobjs[objidx],nearlistobjnames[objidx]
		if not obj then return end
		local id = msk.dword(obj)
		local posptr = GetDynamicObjPosPtr(obj)
		local x,y = msk.float(posptr),msk.float(posptr+8)
		return id,x,y,name
	else
		local obj,name =  neadlistdmobj2 or neadlistdmobj,neadlistdmname2 or neadlistdmname
		if not obj then return end
		local id = msk.dword(obj)
		local posptr = GetDynamicObjPosPtr(obj)
		local x,y = msk.float(posptr),msk.float(posptr+8)
		return id,x,y,name
	end
end

function PairsTree(data,tname,fit)
	if data == 0 or msk.byte(data+sig.dynamicObjFlag) == 1 then
		return false
	end
	local id,name,namelen,nameaddr
	id = msk.dword(data+0xc)
	name = ""
	namelen = msk.dword(data+0x30)
	nameaddr = data+0x1c
	if namelen > 0xf then
		nameaddr = msk.dword(nameaddr)
	end
	name=msk.str(nameaddr)
	if fit and name == tname then
		return data + 0x10
	elseif not fit and string.find(name,tname) then
		return data + 0x10
	end
	return PairsTree(msk.dword(data),tname,fit) or PairsTree(msk.dword(data+8),tname,fit)
end

function GetDynamicObjByName(tname,fit)
	local root  = g_DmObjMn
	root = msk.dword(root+0x10)
	root = msk.dword(root+4)
	return PairsTree(root,tname,fit)
end

function ShowTaskState(state)
	local button = WindowSys_Instance:GetWindow("RouterMaker_btn")
	if state == nil then
		return button:SetVisible(false)
	end
	button:SetVisible(true)
    button:SetProperty("Text", tostring(state))
end

function Escort(tarname,x,y,mapid,movstep)
	local ch = GetMyCharByName(tarname)
	if not ch then msk.warn("无法找到护送对象:"..(tarname or "nil")) return false end
	movstep = movstep or 1
	x, y, mapid = tonumber(x), tonumber(y), tonumber(mapid)
	print("Escort:",x,y,mapid)
    local tarx, tary = x,y
	--local --pl = g_player.chara
	local limitdis = s_config.npctalkdis
    if mapid == -1 then
		local npc = GetNpcByTemplateId(x,y)
        tarx,tary = npc.X,npc.Y
		local CurrentMapID = GetMPlayerMapId()
		if CurrentMapID == y and g_player:Dis(tarx,tary) < limitdis and gUI.Gossip.Root:IsVisible() then
			msk.warn("PathTo ok get tar1")
			return true
		end
	else
		limitdis = s_config.coorddis
		local CurrentMapID = GetMPlayerMapId()
		if CurrentMapID == mapid and g_player:Dis(tarx,tary) < limitdis then
			msk.warn("PathTo ok get tar1")
			return true
		end
    end
    local lastx,lasty  = 0,0
    local curx,cury = g_player.x,g_player.y
    local mx,my
    local tardis
    local movlen = 0
	local lastdis = ch.dis
	local miscount = 0
    repeat
		if miscount > 5 then
			PathTo(ch.x,ch.y,GetMPlayerMapId(),5)
		end
		if miscount > 30 then
			return false
		end
		for i=1,5 do
			if ch.dis == 999 then
				if i == 5 then
					msk.warn("无法找到护送对象:"..(tarname or "nil")) 
					return false
				else
					sleep(2)
				end
			else
				break
			end
		end
		print("Escort dis:",ch.dis,ch:Dis(tarx,tary),g_player:Dis(tarx,tary))
		if ch.dis < s_config.escortdis or ch:Dis(tarx,tary) < g_player:Dis(tarx,tary) then
			if not g_global.ismove or movlen <= s_config.movmindis then
				print("start move")
				if mapid == -1 then
					TalkToNpc(x, y)
				else
					--msk.error("未知的接任务地点! unknow accept npc")
					MoveToTargetLocation(x, y, mapid)
				end
				g_global.ismove = true
			end
		elseif g_global.ismove then
			MoveToTargetLocation(g_player.x, g_player.y, GetMPlayerMapId())
			g_global.ismove = false
		end
		if lastdis == ch.dis then
			miscount = miscount + 1
		else
			miscount = 0
		end
		lastdis = ch.dis
        lastx,lasty = curx,cury
        sleep(movstep)
        curx,cury = g_player.x,g_player.y
        mx,my = curx-tarx,cury-tary
        tardis = math.sqrt(mx*mx+my*my)
        mx,my = lastx-curx,lasty-cury
        movlen = math.sqrt(mx*mx+my*my)
        --print("tardis:"..tardis)
        --print("movlen:"..movlen)
        ----pl = g_player.chara
    until tardis <= limitdis
	print("path to ok")
	return true
end

local allgamemaps
function GetMapById(id)
	allgamemaps = allgamemaps or GetMapList()
	for _,t in pairs(allgamemaps) do
		if t.MapID == id then return t end
	end
	return {}
end

function GetMapByName(name)
	allgamemaps = allgamemaps or GetMapList()
	for _,t in pairs(allgamemaps) do
		if t.MapName == name then return t end
	end
	return {}
end

function PickItemByName(name,wnd,skip,fit)
	wnd = wnd or WindowSys_Instance:GetWindow('NameBoardItemBox')
	skip = skip or {}
	local childIndex = 0
	local childWnd
	local group_id
	local item_index
	local itemname
	while true do
		childWnd = wnd:GetChildByIndex(childIndex)
		if not childWnd then return end
		itemname = childWnd:GetProperty('Text')
		group_id = childWnd:GetCustomUserData(0)
		item_index = childWnd:GetCustomUserData(1)
		if childWnd:IsVisible() and (name == nil or (fit and name == itemname ) or (not fit and itemname:find(name))) and (not skip[group_id] or not skip[group_id][item_index]) then
			print('找到物品 尝试拾取:',name)
			skip[group_id] = skip[group_id] or {}
			skip[group_id][item_index] = true
			PickUpItem(group_id, item_index)
			sleep(5,'item_picked')
			return PickItemByName(name,wnd,skip,fit)
		else
			if childWnd:IsVisible() == false then
				print('不可见 跳过拾取',itemname)
			elseif name ~= nil and ((fit and name ~= itemname ) or (not fit and itemname:find(name) == nil)) then
				print('名字不匹配 跳过拾取',itemname,name,fit)
			elseif skip[group_id] and skip[group_id][item_index] then
				print('已排除 跳过拾取',itemname)
			else
				print('未知原因 跳过拾取',itemname,name,fit)
			end
		end
		--print('childWnd:IsVisible()',childWnd:IsVisible())
		childIndex = childIndex + 1
	end
	print('拾取物品结束:',name)
end

--39^50^-1 庄园管理员
function CheckInstance(tarmap,tmout)
	local CurrentMapID = GetMPlayerMapId()
	local mp = GetMapById(CurrentMapID)
	if CurrentMapID ~= tarmap and CurrentMapID == 99 and tarmap ~= 2  then
		print('尝试退出帮派')
		if not EnterInstance(1913,99,-1,0,1,50) and not EnterInstance(1913,99,-1,0,1,50) then
			return -1
		end
		return tmout
	end
	--print("地图资源",mp.MapID,mp.IsInstance)
	while CurrentMapID ~= tarmap and mp.IsInstance do
		print("副本内")
		if tmout < 0 then return -1 end
		InstanceExit()
		sleep(1)
		tmout = tmout - 1
		CurrentMapID = GetMPlayerMapId()
		mp = GetMapById(CurrentMapID)
	end
	while tarmap == 101 and CurrentMapID ~= tarmap do
		print("试图进入 白羽之巅 刷宝模式")
		if tmout < 0 then return -1 end
		if PathTo(515,5,-1) == false then return -1 end
		local npcid = GetNPCObjId(515)
		SelectGossipOption(npcid,1,1)
		sleep(2)
		sleep(3)
		tmout = tmout-5
		CurrentMapID = GetMPlayerMapId()
		print("CurrentMapID",CurrentMapID)
	end
	return tmout
end

function CheckHome(tarmap,tmout)
	if tarmap ~= 140 then return tmout end
	local CurrentMapID = GetMPlayerMapId()
	for i=1,2 do
		if CurrentMapID == 140 then
			return tmout
		end
		print("试图进入庄园")
		if tmout < 0 then return -1 end
		EnterFriendFarm(g_global.farmguid or g_player.guid)
		sleep(3)
		tmout = tmout - 3
		CurrentMapID = GetMPlayerMapId()
	end
	return -1
end

function CheckCity(tarmap,tmout)
	local CurrentMapID = GetMPlayerMapId()
	if CurrentMapID ~= tarmap and tarmap == 50 then
		print("试图进入城市")
		local bag, slot = GetItemInBagIndex(370057)
		if bag then
			UseItem(bag,slot)
			sleep(5)
		end
	end
	return tmout
end

function CheckGuild(tarmap,tmout)
	if GetMPlayerMapId() == tarmap then return tmout end
	if tarmap == 2 then
		tmout = CheckGuild(99,tmout)
		if tarmap ~= -1 then
			if not EnterInstance(1913,99,-1,1,1,2) and not EnterInstance(1913,99,-1,1,1,2) then
				return -1
			end
		end
		return tmout
	end
	if tarmap ~= 99 then return tmout end
	if GetMPlayerMapId() == 2 and not EnterInstance(1920,2,-1,0,0,99) and not EnterInstance(1920,2,-1,0,0,99) then
		return -1
	elseif not EnterInstance(5,50,-1,2,0,99) and not EnterInstance(5,50,-1,2,0,99) then
		return -1
	end
	return tmout
end

function TryMoveing(x,y,mapid,tarx, tary, tarmap,tmout,movstep,limitdis)
	if mapid == -1 then
		UI_Gossip_CloseBtnClick()
	end
	EnableFindPath(tarmap)
    local lastx,lasty  = 0,0
    local curx,cury = g_player.x,g_player.y
    local mx,my
    local tardis
    local movlen = 0
	local mismove = 0
    repeat
		while isAutoVehicleMoving() do
			sleep(2)
			--msk.warn("AutoVehicleMoving")
			tmout = -1
		end
		if tmout < 0 then return false end
        if not g_global.ismove or movlen <= s_config.movmindis then
            print("start move")
            if mapid == -1 then
                msk.pcall(TalkToNpc,x, y)
            else
                --msk.error("未知的接任务地点! unknow accept npc")
                msk.pcall(MoveToTargetLocation,x, y, mapid)
            end
			g_global.ismove = true
			--sleep(1)
			--tmout = tmout -1
        end
        lastx,lasty = curx,cury
        sleep(movstep)	
		tmout =tmout-movstep
        curx,cury = g_player.x,g_player.y
        mx,my = curx-tarx,cury-tary
        tardis = math.sqrt(mx*mx+my*my)
        mx,my = lastx-curx,lasty-cury
        movlen = math.sqrt(mx*mx+my*my)
		if movlen == 0 then
			mismove = mismove+1
		else
			mismove = 0
		end
		if mismove > 5 then--卡住在一个地方了
			print('卡住了')
			MoveToTargetLocation(g_player.x+math.random(-3,3), g_player.y+math.random(-3,3), GetMPlayerMapId())
			sleep(1)
			tmout = tmout -3
			curx,cury = g_player.x,g_player.y
			mx,my = curx-tarx,cury-tary
			tardis = math.sqrt(mx*mx+my*my)
			mx,my = lastx-curx,lasty-cury
			movlen = math.sqrt(mx*mx+my*my)
		end
        print("tardis:"..tardis)
        print("movlen:"..movlen)
       -- --pl = g_player.chara
    until tardis <= limitdis or (mapid == -1 and gUI.Gossip.Root:IsVisible())
	print("path to ok")
	if mapid == -1 then MoveToTargetLocation(tarx, tary, tarmap) end
	return true
end

function PathTo(x,y,mapid,tmout,movstep)
	movstep = movstep or 1
	tmout = tmout or 180--默认超时3M
	x, y, mapid = tonumber(x), tonumber(y), tonumber(mapid)
	print("PathTo:",x,y,mapid)
    local tarx, tary,tarmap = x,y,mapid
	if not tarx then return msk.warn("PathTo error tax nil1") or true end
	--local --pl = g_player.chara
	local limitdis = s_config.npctalkdis
    if mapid == -1 then
		local npc = GetNpcByTemplateId(x,y)
        tarx,tary = npc.X,npc.Y
		if not tarx then return msk.warn("PathTo error tax nil2") or true end
		tarmap = y
		local CurrentMapID = GetMPlayerMapId()
		if CurrentMapID == tarmap and g_player:Dis(tarx,tary) < limitdis and gUI.Gossip.Root:IsVisible() then
			msk.warn("PathTo ok get tar1")
			MoveToTargetLocation(tarx, tary, tarmap)
			return true
		end
		tmout = CheckInstance(tarmap,tmout)--副本
		tmout = CheckHome(tarmap,tmout)--庄园
		tmout = CheckCity(tarmap,tmout)--庄园
		tmout = CheckGuild(tarmap,tmout)--帮派
	else
		limitdis = s_config.coorddis
		local CurrentMapID = GetMPlayerMapId()
		if CurrentMapID == tarmap and g_player:Dis(tarx,tary) < limitdis then
			msk.warn("PathTo ok get tar2")
			return true
		end
		tmout = CheckInstance(tarmap,tmout)--副本
		tmout = CheckHome(tarmap,tmout)--庄园
		tmout = CheckCity(tarmap,tmout)--庄园
		tmout = CheckGuild(tarmap,tmout)--帮派
    end
	return TryMoveing(x,y,mapid,tarx, tary, tarmap,tmout,movstep,limitdis)
end

function InistanceMove(x,y)
	local slot,bag,num = GetItemByName('小浣熊',true,1)
	if slot then 
		MoveItem(bag,slot,5,-1)
		sleep(1)
	end 
	PathTo(x,y,GetMPlayerMapId())
	slot,bag,num = GetItemByName('小浣熊',true,5)
	if slot then 
		MoveItem(bag,slot,1,-1)
		sleep(1)
	end 
	
end

function EnableFindPath(id,no)
	local addr = msk.call(sig.canpathcall,0,2,id)
	if addr then
		msk.sbyte(addr+0x24,no and 0 or 1)
	end
end

function TalkAndSelectOption(tarname,x, y, mapid,timeout)
	local ch = GetCharByName(tarname)
	if not ch then 
		if not PathTo(x,y,mapid,30) then return false end
		ch = GetCharByName(tarname)
	end
	if not ch then 
		return msk.warn("TalkAndSelectOption cant get tar:"..tarname) 
	end
	if not PathTo(ch.x,ch.y,GetMPlayerMapId(),10) then return false end
	ch:Select()
	for i=1,10 do
		SelectGossipOption(ch.id, 0, 0)
		sleep(1)
	end
	return true
end


function CollectGameObj(id,dx,dy)
	msk.lockobj(id)
	print(string.format("get a obj: %x",id),dx,dy)
	if not PathTo(dx,dy,GetMPlayerMapId(),10) then 
		--msk.unlockobj(id)
		return 
	end
	collect(id)
	sleep(4,'spell')
	sleep(0.3)
	if GetPursuerNum() > 3 and g_player.hp < 30 then
		FightPursuers()
	end
	--msk.unlockobj(id)
	return true
end

function CollectNearlistObj(tarname,x, y, mapid,timeout)
	local id,dx,dy = GetGameObjectByName(tarname,nil,nil,true)
	if not id and x then 
		print("cant get obj,move")
		if not  PathTo(x,y,mapid,30) then return false end
		id,dx,dy = GetGameObjectByName(tarname,nil,nil,true)
	end
	if not id then 
		return msk.warn("CollectObj cant get tar:"..(tarname or "nil")) 
	end
	return CollectGameObj(id,dx,dy)
end


function CollectObj(tarname,x, y, mapid,timeout)
	local id,dx,dy = GetGameObjectByName(tarname)
	if not id and x then 
		print("cant get obj,move")
		if not  PathTo(x,y,mapid,30) then return false end
		id,dx,dy = GetGameObjectByName(tarname)
	end
	if not id then 
		return msk.warn("CollectObj cant get tar:"..(tarname or "nil")) 
	end
	return CollectGameObj(id,dx,dy)
end
--gUI_GOODSBOX_BAG = {
--template = -1,
--backpack0 = 1,
function GetItemByName(tname,notfit,bag)
	bag = bag or 1
	local bagnum = s_bagnum[bag] or 0
	if bag == 1 then bagnum = bagnum + gUI.Bag.BagExtNumCur*10 end
	local name,num
	for i=0,bagnum-1 do
		num, _, _, name = GetItemBaseInfoBySlot(bag, i)
		if name and ((not notfit and name == tname) or (notfit and string.find(name,tname))) then
			return i,bag,num
		end
	end
end

function GetItemByType(titemType,tequipKind,bag)
	bag = bag or 1
	local bagnum = s_bagnum[bag] or 0
	if bag == 1 then bagnum = bagnum + gUI.Bag.BagExtNumCur*10 end
	for i=0,bagnum-1 do
		local _, _, _, _, _, _, _, _, _,_, _, _, _, itemType, _, _, _, _, _, _, equipKind= GetItemInfoBySlot(bag, i)
		if titemType == itemType and equipKind == tequipKind then
			return i,bag
		end
	end
end

function PrintAllItem(bag)
	bag = bag or 1
	local bagnum = s_bagnum[bag] or 0
	if bag == 1 then bagnum = bagnum + gUI.Bag.BagExtNumCur*10 end
	print("PrintAllItem:"..bag.."-"..bagnum)
	local name
	for i=0,bagnum-1 do
		--local name, _, _, minLevel, job, sex, _, _, canSell, _, _, _, _, itemType, _, _, _, _, sellPrice, _, equipKind, _, _, _= GetItemInfoBySlot(bag, i,0)
		--print(i,name,minLevel, job, sex,canSell, itemType, sellPrice,equipKind)
		print(i,GetItemBaseInfoBySlot(bag, i))
	end
end

function MoveAndSelect(x,y,mapid,idx1,idx2)
	if false == PathTo(x,y,mapid) then
		return false
	end
	local npcid = GetNPCObjId(x)
	if false == gUI.Gossip.Root:IsVisible() then
		local ch = GetCharById(npcid)
		ch:Open()
		sleep(1)
	end
	SelectGossipOption(npcid,idx1,idx2)
	sleep(2)
	sleep(3)
	return true
end
			
function EnterInstance(x,y,mapid,idx1,idx2,tarmap)
	local oldMapId = GetMPlayerMapId()
	if tarmap == oldMapId then return true end
	if not MoveAndSelect(x,y,mapid,idx1,idx2) then return false end
	if oldMapId ~= GetMPlayerMapId() then
		return true
	end
	return false
end


--c[1], a[1], b[1], c[2], a[2], b[2], c[3], a[3], b[3], skillType, chargeTime, channelTime, maxRange, cooldown, desc, desc2, _, _ = GetSkillExInfo(skillId, 1)
local g_allSkillCd = {}
function SkillCoolDown(index, duration, rest)
	--print("SkillCoolDown",index, duration, rest)
end

function TrySkill(id,hp,mp,pl)
	hp = hp or 0
	if g_player.hp < hp or g_player.mp < mp then return end--血蓝要求
end

local curdelayskillid
function collect(id)
	if not id or id == -1 then
		return
	end
	msk.call(sig.collectcall,0,0,id)
	--curdelayskillid = 0x12345
	--skillsleep(4)
	--curdelayskillid = 0
end

local sjlastbuftime = 0
local cxlastbuftime = 0
local xfhlastbuftime = 0
local lxhlastbuftime = 0
local MenPaiBuff = {
	[0] = function()
		--local --pl = g_player.chara
		local curtm = os.time()
		if g_player.lv >= 15 and (curtm - sjlastbuftime) > 60*60 then
			CastSkill(211)
			sjlastbuftime = curtm
			sleep(1)
		end
	end,
	[2] = function ()
		local curtm = os.time()
		if g_player.lv >= 15 and (curtm - lxhlastbuftime) > 60*60 then
			CastSkill(610)
			lxhlastbuftime = curtm
			sleep(1)
		end
	end,
	[3] = function()
		--local --pl = g_player.chara
		local curtm = os.time()
		if g_player.lv >= 15 and (curtm - cxlastbuftime) > 60*60 then
			CastSkill(3910)
			cxlastbuftime = curtm
			sleep(1)
		end
	end,
	[4] = function()
		--local --pl = g_player.chara
		local curtm = os.time()
		if g_player.lv >= 15 and (curtm - xfhlastbuftime) > 4*60*60 then
			CastSkill(4416)
			xfhlastbuftime = curtm
			sleep(4,"spell")
		end
	end,
}
--神将
local txtm = 0
local sftm = 0
local sftmm = 0
local xxtm = 0
local jgtm = 0
local pgtm = 0
--沉香
local ydtm = 0
local sxtm = 0
local cxskilid1 = 3906
local MenPaiFight = {
	[0] = function(dis,x,y)
		local curtm = os.time() 
		--local --pl = g_player.chara
		local lv = g_player.lv
		local mp = g_player.mp
		local hp = g_player.hp
		if g_global.isbusy then
			if dis > 5 then MoveToTargetLocation(x,y,GetMPlayerMapId()) end
		elseif lv >= 5 and dis < 15 and (curtm -txtm) > 20.5 then--突袭
			CastSkill(208)
			txtm = curtm
		elseif lv >= 26 and curNearChar > 1 and dis < 15 and dis > 6 and (curtm-pgtm) > 20.5 then--破阵曲
			CastAreaSkill(213,x,y)
			pgtm = curtm
		elseif mp >= 40 and lv >= 23 and hp < 90 and (curtm -xxtm) > 5.5 then--吸血 仇饮血
			xxtm = curtm
			CastSkill(251)
		elseif lv >= 20 and dis < 5 and (curtm -sftm) > 30.5 and (curtm -sftmm) > 30.5 and (curtm-pgtm) > 2 then--十方俱灭
			CastSkill(212)
			curdelayskillid = 212
			sftmm = curtm-28
			return 1
		elseif curNearChar > 1 and lv >= 10 and mp >= 30 and dis < 5 then--横扫千魔
			CastSkill(210)
		elseif mp >= 40 and (curtm -jgtm) > 3 then--撩金戈
				CastSkill(207)
				jgtm = curtm
		else
			CastSkill(cxskilid1)--206
		end
		return 0.5
	end,
	[2] = function(dis,x,y)
		if not g_global.isbusy then
			CastSkill(cxskilid1)
		end
		return 0.5
	end,
	[3] = function(dis,x,y)
		if g_global.isbusy then
			return 0.5
		end
		local curtm = os.time() 
		--local --pl = g_player.chara
		local lv = g_player.lv
		local mp = g_player.mp
		local maxhp, thp, maxmp, tmp = GetSelfEaseChangeInfo()
		local hp = g_player.hp
		if lv >= 23 and tmp >= 80 and hp < 90 and (curtm -g_global.kmtm) > 15.5 then--枯木回春
			if isdelay then--那个技能结束后 需要过一会才能放其他技能
				sleep(1)
			end
			g_global.kmtm = curtm
			WorldStage:SetSkillTargetToSelf(true)
			CastSkill(3915)
			WorldStage:SetSkillTargetToSelf(false)
		elseif lv >= 5 and tmp >= 60 and ((hp < 30 and curNearChar < 3) or hp < 20) and hp > 5 then--芳华绽放
			WorldStage:SetSkillTargetToSelf(true)
			CastSkill(3908)
			WorldStage:SetSkillTargetToSelf(false)
		elseif mp < 50 and (curtm -sxtm) > 185 then
			sxtm = curtm
			WorldStage:SetSkillTargetToSelf(true)
			CastSkill(3917)
			WorldStage:SetSkillTargetToSelf(false)
		elseif lv >= 20 and (hp < 40 or (curNearChar >= 3 and curNearChar < 5))and dis < 6 and tmp > 200 and (curtm -ydtm) > 30.5 then--月渡天河
			ydtm = curtm
			CastSkill(3911)
		elseif true and (hp < 40 or (curNearChar >= 3 and curNearChar < 5)) and lv >=10 and dis < 6 and tmp >= 50 then--吸血
			CastSkill(3909)
		elseif (cxskilid1 == 3919 or tmp >= 80) and dis < 24 then
			CastSkill(cxskilid1)
			sleep(4,"spell")
			return 0
			--[[
			curdelayskillid = cxskilid1
			return 4
			--]]
		elseif dis > 24 then
			MoveToTargetLocation(x,y,GetMPlayerMapId())
		else
			CastSkill(3901)
		end
		isdelay = false
		return 1
	end,
	[4] = function(dis,x,y)
	end,
}

function UseBreakSkill(ch)
	--return false
	local curtm = os.time()
	local mp = g_player.menpai
	if mp == 0 and g_player.lv >= 5 and ch.dis < 15 and (curtm -txtm) > 20.5 then--突袭
		txtm = curtm
		CastSkill(208)
		return true
	end
end

local slptm=0
local lastsklltm = 0

function MySkillDelay(isleadtype, flag)
	print("MySkillDelay",isleadtype, flag)
	if flag ~= 0 or isleadtype == -1 then return end
	mskco.HandleEvent('spell')
end

local _lastSkillDec
local _kspos = {
	{68,57},
	{57,57},
	{57,68},
	{68,68},
}

function _SkipKengSha()
	_lastSkillDec = nil
	local pos = _kspos[g_global.ksidx]
	g_global.ksidx = g_global.ksidx == #_kspos and 1 or g_global.ksidx + 1
	for i=1,4 do
		if math.abs(g_player.x-pos[1]) < 2 and math.abs(g_player.y-pos[2]) < 2 then
			break
		end
		MoveToTargetLocation(pos[1], pos[2], GetMPlayerMapId())
		sleep(0.5)
	end
end

function MySkillOver(isleadtype, flag)
	print("MySkillOver",isleadtype, flag,_lastSkillDec,g_global.ksidx)
	if flag == 1 and g_global.ksidx ~= nil and _lastSkillDec == '坑杀' then
		local f = coroutine.create(_SkipKengSha)
		coroutine.resume(f)
	end
	if flag ~= 0 or isleadtype == -1 then return end
	g_global.isbusy = false
	mskco.HandleEvent('spell')
end


function Msk_OnPlays_SpellOver(isleadtype, flag)
	print("Msk_OnPlays_SpellOver",isleadtype, flag)
	--do return end
	if flag == 1 and g_global.ksidx ~= nil and _lastSkillDec == '坑杀' then
		_lastSkillDec = nil
		local pos = _kspos[g_global.ksidx]
		g_global.ksidx = g_global.ksidx == #_kspos and 1 or g_global.ksidx + 1
		for i=1,4 do
			if math.abs(g_player.x-pos[1]) < 2 and math.abs(g_player.y-pos[2]) < 2 then
				break
			end
			MoveToTargetLocation(pos[1], pos[2], GetMPlayerMapId())
			sleep(0.5)
		end
	end
end

function Msk_OnPlays_SpellStart(dura, stype, other, desc, flag)
	print("Msk_OnPlays_SpellStart",dura, stype, other, desc, flag)
	_lastSkillDec = desc
	if flag ~= 2 then return end
	local ch = GetCharById(GetCurTargetObjId())
	if not ch then return end
	if (desc == "千针" or desc == "狂煞" ) then--千针 正面技能 需要绕背 处理:2秒移动到怪物背后
		if UseBreakSkill(ch) then return end
		if ch.dis > 15 then return end
		--local --pl = g_player.chara
		local tarx,tary = 2*ch.x-g_player.x,2*ch.y-g_player.y
		print("移动到怪物背面:",tarx,tary,g_player.x,g_player.y)
		MoveToTargetLocation(tarx,tary,GetMPlayerMapId())
		sleep(2)
	elseif desc == "巨力回转" or desc == "魔威" then--desc == '恐怖降临'
		if UseBreakSkill(ch) then return end
		local dis = ch.dis 
		if dis > 10 then return end
		--三角形相似
		--local --pl = g_player.chara
		local scale = 10/dis
		local xdif = g_player.x-ch.x
		xdif = xdif*scale
		local ydif = g_player.y-ch.y
		ydif = ydif*scale
		local tarx,tary = ch.x+xdif,ch.y+ydif
		print("移动到10米外:",tarx,tary,g_player.x,g_player.y)
		MoveToTargetLocation(tarx,tary,GetMPlayerMapId())
		sleep(2)
	end
	--print("尝试躲避技能:"..desc)
end

function AttackOne(ch)
	local curmenpai = g_player.menpai
	return MenPaiFight[curmenpai](ch.dis,ch.x,ch.y)
end

function FightChar(ch,timeout)
	timeout = timeout or 120
	--curNearChar = 1
	ch:Select()
	--local --pl = g_player.chara
	local tarid = ch.id
	if g_player.lv >= 15 and Lua_Pet_GetCurrentPetIndex() == -1 then
		AskCallupFaerie()
	end
	g_global.isfight = true
	if g_global.areafight then 
		curNearChar = 5 
	end
	local curmenpai = g_player.menpai
	MenPaiBuff[curmenpai]()
	local tm = 0
	local attackcount = 0
	local lasthp = ch.hp
	local misscount = 0
	local sleeptm
	slptm=0
	_, cxskilid1 = GetCharSpellInfo(1, 0)
	print("cxskilid1",cxskilid1)
	--UIConfig:setAutoDisMount(false, true)
	while tm < timeout do
		if tarid ~= GetCurTargetObjId() or ch.hp == 0 then 
			--UIConfig:setAutoDisMount(true, true)
			g_global.isfight = false
			return true 
		end
		if misscount > 5 and misscount%5 then
			if not PathTo(ch.x,ch.y,GetMPlayerMapId(),10,1) then break end
		elseif misscount > 5 and misscount%3 then
			MoveToTargetLocation(g_player.x+math.random(-3,3), g_player.y+math.random(-3,3), GetMPlayerMapId())
			sleep(1)
			slptm = slptm -1
			tm = tm + 1
		end
		if slptm > 0 then
			slptm = 0
			sleep(slptm)
		end
		sleep(MenPaiFight[curmenpai](ch.dis,ch.x,ch.y))
		lastsklltm = slptm
		if lasthp == ch.hp then
			misscount = misscount + 1
		else
			lasthp = ch.hp
			misscount = 0
		end
		tm = tm + 1
		attackcount = attackcount + 1
	end
	--UIConfig:setAutoDisMount(true, true)
	g_global.isfight = false
	return false
end

function FightMonster(tarname,x, y, mapid,timeout,fit)
	local ch
	if type(tarname) == "number" then
		ch = GetCharById(tarname)
	else
		ch = GetCharByName(tarname,false,fit,true)
	end
	if not ch and x then 
		if not PathTo(x,y,mapid,30)  then return false end
		if type(tarname) == "number" then
			ch = GetCharById(tarname)
		else
			ch = GetCharByName(tarname,false,fit,true)
		end
	end
	if not ch then 
		sleep(1)
		return msk.warn("FightMonster cant get tar:"..(tarname or "??")) 
	end
	return FightChar(ch,timeout)
end
--需要处理的信息框

local checkcount = 0
function SignInCheck()
	print("SignInCheck")
	local root = WindowSys_Instance:GetWindow("LoginGroup.Picture5_bg")
	local loginBtn = WindowToButton(root:GetGrandChild("Accept_btn"))
	if loginBtn:IsEnable() and loginBtn:IsVisible() then
		if checkcount < 2 then 
			checkcount = checkcount + 1
		end
		checkcount = 0
		SignIn()
	end
	print("SignInCheck over")
end

--登录 accs:帐号 pwds:密码 例:SignIn("xxoo","ooxx")
function SignIn()
  print("SignIn")
  local at = g_global.accountdata
  local root = WindowSys_Instance:GetWindow("LoginGroup.Picture5_bg")
  local acc = WindowToEditBox(root:GetGrandChild("Account_bg.Edit_ebox"))
  acc:SetProperty("Text",at.user)
  local pw = WindowToEditBox(root:GetGrandChild("Password_bg.Edit_ebox"))
  pw:SetProperty("Text",at.pwd)
  --print("SaveLoginServerInfo",at.gname,at.sname,at.server,at.server,at.area)
  SaveLoginServerInfo(at.gname,at.sname,at.server,at.server,at.area)
  UI_LoginGrp_Clicked()
end

function SelectServerFlag()
	if g_global.updatedata then 
		print('开始更新数据...')
		local ret,err = pcall(dofile,[[G:\bitbuket\tzj\aide\SigSearch.lua]])
		print('结果',ret,err)
		return
	end
	g_global.selectserverok = true
	print("selectserverok")
	if g_global.pause == true then return end
	if g_global.accountdata.user ~= nil then
		AddTimerEvent(1,"SelectServer",SelectServer)
	end
end

--选区 gname:大区名 sname:服务器名 sid:服务器位置 gid:大区位置 例:SelectServer("世外桃园 ", "轩辕", 1, 0)
function SelectServer()
	print("try SelectServer",g_global.pause,g_global.selectserverok)
	if g_global.pause == true then return end
	if not g_global.selectserverok then return end
	g_global.selectserverok = false
	DelTimerEvent(1,"SelectServer")
	
	--mc_tools.UnHookFunction("_OnLogin_InitLastSelection")
	local at = g_global.accountdata
	local wnd = WindowSys_Instance:GetWindow("Server.Picture"..(at.server+1)..".Server_btn")
	at.sname = wnd and wnd:GetProperty("Text") or "一服"
	wnd = WindowSys_Instance:GetWindow("Server.CurrentGroup_dlab")
	at.gname = wnd and wnd:GetProperty("Text") or "一区"
	StopDownLoadServerInfo()
	Lua_LoginGrp_ShowServerUI(false)
	local wnd = WindowSys_Instance:GetWindow("LoginGroup.Picture5_bg")
	wnd:SetVisible(true)
	wnd = WindowToButton(WindowSys_Instance:GetWindow("Server.Close_btn"))
	wnd:SetEnable(true)
	wnd = WindowSys_Instance:GetWindow("LoginGroup.BtnLeft_bg.Area_dlab")
	if wnd then
	  wnd:SetProperty("Text", at.gname .. "   " .. at.sname)
	  SaveServerInfo(at.sname)
	end
	local ret,err = pcall(SignIn)
	if not ret then print("SignIn error",err) end
	AddTimerEvent(1,"SignInCheck",SignInCheck)
end

-->  0成功 4重名 3角色已满 7:符号不对
local CreateCharResult
function HookedNewCharResult(result)
	CreateCharResult = tonumber(result)
end

function CheckCreateChar()
	if CreateCharResult ~= 0 and CreateCharResult ~= 3 then
		CreateChar()
	else
		mc_tools.RemoveTimer("CheckCreateChar")
		EnterGame()
	end
end


function _GetBlendItem(bagType, itemSlot)
	local rateList = GetBlendAttr(itemSlot, bagType, 0)
	local name, icon, quality, minLevel, job, sex, isUnique, canBank, canSell, canDiscard, bindinfo, count, maxCount, itemType, desc1, desc2, desc3, leftEffectTime, sellPrice, _, equipKind, offlineTime, isTimeLimit, EquipPoint = GetItemInfoBySlot(bagType, itemSlot, 0)
	if itemType ~= 2 then return end
	local _, coefMain, kindMain, _, _ = GetBlendEquipInfo(itemSlot)
	local index = 0
	for i = 0, table.maxn(rateList) do
		if rateList[0] == nil then
		  return
		end
		local GuidList = GetBlendCommonInfo(rateList[i].typeId, rateList[i].value, EquipPoint, minLevel, coefMain)
		local count = 0
		local BlendName, newObj, Gb1, Gb2, Gb3
		if GuidList ~= nil then
			for j = 0, table.maxn(GuidList) do
				local CREATEFLAG = false
				local itemGuid = GuidList[j].ItemGuid
				local bagType1, itemSlot1 = GetItemPosByGuid(itemGuid)
				if itemSlot1 ~= -1 then
					return itemSlot1
				end
			end
		end
	end
end

function GetBlendItem()
	local bagnum = s_bagnum[1] or 0
	bagnum = bagnum + gUI.Bag.BagExtNumCur*10
	local sslot
	for i=0,bagnum-1 do
		sslot = _GetBlendItem(1,i)
		if sslot then return i,sslot end
	end
	bagnum = s_bagnum[1] or 0
	for i=0,bagnum-1 do
		sslot = _GetBlendItem(4,i)
		if sslot then return i,sslot end
	end
end

function ShowBlendWindow()
  local wnd = WindowSys_Instance:GetWindow("Blend")
  if wnd:IsVisible() then
    return
  end
  _OnBlend_OnPanelOpen()
end

function CloseBlendWindow()
	local wnd = WindowSys_Instance:GetWindow("Blend")
	if wnd:IsVisible() then
		_OnBlend_CloseWindw()
	end
end

function BlendAnyOne()
	if not GetItemByName'融魂石' then return end
	local mmslot,sslot = GetBlendItem()
	if not mmslot then return end
	ShowBlendWindow()
	SetBlendEquip(mmslot, OperateMode.Add, BLEND_SLOT_COLLATE.MAINEQUIP)
	sleep(1.5)
	SetBlendEquip(sslot, OperateMode.Add, BLEND_SLOT_COLLATE.SUBEQUIP)
	sleep(1.5)
	UI_Blend_OkBtnClick()
	sleep(1.5)
	CloseBlendWindow()
end

function CreateChar()
	--do return end
	if g_global.testlv < 2 then return end
	CreateCharResult = nil
	local xstr = math.random(1,#Names.LAST_NAME)
	local FirstNameT
	local sex = g_global.accountdata.sex or s_config.defaultsex
	local pro = g_global.script_config['职业']
	pro = pro and gUI_MenPaiId[pro] or s_config.defaultpro
	if pro == 0 or pro == 1 then
		sex = 1
	elseif pro == 2 or pro == 3 then
		sex = 0
	end
	if sex == 0 then
		FirstNameT = Names.FEMALE_FIRST_NAME
	else
		FirstNameT = Names.MALE_FIRST_NAME
	end
	local mstr = math.random(1,#FirstNameT)
	local name
	name = Names.LAST_NAME[xstr]..FirstNameT[mstr]
	print("CreateChar",name, pro, sex, 1, 1, 1, 1)
	CreateNewChar(name, pro, sex, 1, 1, 1, 1)
end

function GetGoalProcess()
	local list, active, allact = GetFormulaList()
	if list[0] ~= nil then
		for i = 0, table.maxn(list) do
			if list[i].Cost <= active and list[i].OnlineTime < 0 and list[i].State == 2 then
				 ExchangeRecommend(list[i].FormulaID, i)
				 sleep(1)
			end
		end
	end
	--ItemDeal()
	DealBag()
end

function SkipChar()
	--if msk.ver_test then return end
	do return end
	local at = g_global.accountdata
	while at.charidx < 3 do
		local _, name,_, level = GetAccountCharInfo(at.charidx)
		if at.script == "1_35" and tonumber(level) >= (cfg.splv[at.script] or 99) then
			msk.send("extern#name="..name..",lv="..level)
			msk.send("result#成功")
			at.charidx = at.charidx + 1
			msk.send("extern#charidx="..at.charidx)
		else
			break
		end
	end
	if at.charidx > 2 then
		msk.send("extern#status=已结束")
		Exit()
		return
	end
end

local etCheckTime
function CheckEnterGame(tm)
	etCheckTime = etCheckTime+tm:GetRealInterval()
	if etCheckTime > 10 then
		DelTimerEvent(1,"CheckEnterGame")
		BackToLogin()
	end
end

function EnterGame()
	if g_global.pause == true then return end
	print("EnterGame")
	--mc_tools.UnHookFunction("_OnCharList_OnOpen")
	DelTimerEvent(1,"SignInCheck")
	if g_global.testlv < 2 then return end
	--_OnCharList_EnterToGame()
	--do return end
	if not GetAccountCharInfo(0) or not GetAccountCharInfo(1) or not GetAccountCharInfo(2) then 
		mc_tools.HookFunction('_OnCharConfigure_CreateResult',HookedNewCharResult)
		mc_tools.SetTimer(3,"CheckCreateChar",CheckCreateChar)
		return CreateChar() 
	end 
	mc_tools.RemoveTimer("CheckCreateChar")
	if g_global.test_wait_trade then
		EnterIntoGameWorld(2)
	else
		EnterIntoGameWorld(g_global.accountdata.charidx)
	end
	SkipChar()
	--EnterIntoGameWorld(1)
	_CharList_SetEnterDisable()
	etCheckTime = 0
	AddTimerEvent(1,"CheckEnterGame",CheckEnterGame)
end

--性能剖析
local Counters = {}
local Names = {}

function ProHook()
	local f = debug.getinfo(2,"f").func
	if Counters[f] == nil then
		Counters[f] = 1
		Names[f] = debug.getinfo(2,"Sn")
	else
		Counters[f] = Counters[f]+1
	end
end


function getname(func)
	local n = Names[func]
	if n.what == "C" then
		return n.name
	end
	local lc = string.format("[%s]:%s",n.short_src,n.linedefined)
	if n.namewhat ~= "" then
		return string.format("%s (%s)",lc,n.name)
	else
		return lc
	end
end

function printCurProNum()
	debug.sethook()
	local tt ={}
	for func,count in pairs(Counters) do
		tt[#tt+1] = {getname(func),count}
	end
	table.sort(tt,function (v1,v2) if v1[2] < v2[2] then return true end end)
	for i=1,#tt do
		print(tt[1],tt[2])
	end
	debug.sethook(ProHook,"c")
end

local ferrMsg = {
	"登录失败，你输入的帐号或密码错误",
	"无效帐号…",
}

local skipMsg = {
	--"有非法字符不能创建成功",
	"创建角色失败:角色已经满了...",
	"账号名含有非法字符",
	"角色名长度必须为4~12个字符",
	"输入的角色名称已被其他玩家使用，请重新输入角色名",
}

local errorMsg = {
	'永久封闭',
	'账号或密码错误',
	'版本错误',
}


--local oldMessagebox_Show = Messagebox_Show
function MyMessagebox_Show(msg_box_type, arg1,...)
	print("Messagebox_Show:",msg_box_type, arg1,...)
	if msg_box_type == "NOTIFY_MSG" and arg1 then
		for i=1,#ferrMsg do
			if ferrMsg[i] == arg1 then
				msk.error(arg1)
				return true
			end
		end
		for i=1,#skipMsg do
			if skipMsg[i] == arg1 then
				msk.warn(arg1)
				return true
			end
		end
		for i=1,#errorMsg do
			if arg1:find(errorMsg[i]) then
				local logger = g_global.logger
				logger:error(errorMsg[i])
				mc_tools.AccountExit(errorMsg[i])
				return true
			end
		end
		if false and arg1 == "有非法字符不能创建成功" then
			local errordata = debug.traceback()
			for line in errordata:gmatch("(.-)\n") do
				print("非法字符-->",line)
			end
		end
	elseif msg_box_type == "NOTIFY_MSG_RELOGIN" then
		BackToLogin()
		return true
	elseif msg_box_type == "CONNECT_SERVER_FAIL2" then--直接小退
		BackToLogin()
		return true
	elseif msg_box_type == "SUMMON_REQUEST_SCENE" then--任务召唤
		RetSceneTranse(true, arg1,arg2,arg3)
		return true
	elseif msg_box_type == "CONFIRM_OTHER_REVIVE" then
		AcceptReviveByOther(true)
		return true
	end
	--return true
	--return oldMessagebox_Show(msg_box_type, arg1,...)
end

function RouterStateChange(isshow)
	g_global.ismove = isshow
	return false
end

function MapChange()
	g_global.isfighting = false
	mc_tools.SetListCallBack1(nil,"MapChange",PlayerInit,"MapChange")
end

function Msk_OnChat_RecvChat(chat_type, strMsg, name, exInfo, title, guid, menpaiId, GMLev, errorColor)
	g_global.lastchat = os.time()
  if arg1 == g_CHAT_TYPE.TELL then
	return mc_tools.ParseCmd(strMsg, name, exInfo, title, guid, menpaiId, GMLev, errorColor)
  end
end

local lhtm = 0
local curSavedPeople = {}
function SavePeople(x,y,mapid,name)
	print("SavePeople now")
	curSavedPeople[name] = nil
	local curtm = os.time()
	if curtm-lhtm < 31 then
		return
	end
	lhtm = curtm
	if not g_global.isfighting then
		PathTo(x,y,mapid)
	else
		return
	end
	if not g_global.isfighting then
		local ch = GetCharByType(gUI_CharType.CHAR_PLAYER,true,name)
		if ch then
			ch:Select()
			CastSkill(3913)
			sleep(5,"spell")
		end
	end
end


--[[
	--if event == "TEAM_MEMBER_INFO_UPDATE" then
	--_OnParty_MemberInfoUpdate(arg1, arg2)
	--BUFF更新
	--elseif event == "TEAM_MEMBER_AURA_UPDATE" then
		--_OnParty_MemberAuraUpdate(arg1, arg2)
	--拾取方式
	--elseif event == "DISTRIBUTE_MODE_CHANGED" then
	--拾取品质
	--elseif event == "DISTRIBUTE_LEVEL_CHANGED" then
	--入队申请
	--elseif event == "TEAM_BE_REQUESTED" then
	--组队邀请
	--elseif event == "TEAM_BE_INVITED" then
	--??
	--elseif event == "TEAM_ASKLEADER_INVITE" then
		--_OnTeam_AskLeaderInvite(arg1, arg2, arg3, arg4, arg5)
	--踢出副本
	--elseif event == "TEAM_INSTANCE_KICK" then
		--_OnTeam_InstanceKick(arg1, arg2)
	--更新所有数据
	--elseif event == "TEAM_UPDATE_ALLINFO" then
		_--OnParty_UpdateAll()
	--好友关系更新
	--elseif event == "COMMUNITY_UPDATE_RELATION" then
		--_OnParty_UpdateFriendBtn(arg1)
	--elseif event == "COMMUNITY_DEL_RELATION" then
		--_OnParty_UpdateFriendBtn(arg1)
--]]

local _Party_DefaultBackImage = "UiIamge012:Image_Leave"
local _Party_DeadBackImage = "UiIamge012:Dead_Party"
local _Party_STR_YOU = "您"
local _Party_Index_Max = 3
local USER_GUIDE_TEAM_ID = 87
local USER_GUIDE_TEAMINVITE_ID = 88
local CHANGE_TYPE = {
  leaveTeam = 0,
  change = 1,
  offline = 2
}

function Msk_OnParty_MemberInfoUpdate(member_name, index)
	if g_global.testmode then return end
	local isself, name, level, max_hp, hp, max_mp, mp, is_leader, head, hair_model, hair_color, offline, menpai, objid, gender, IsDead, guildName, mSenceId, is_same_scene,guid, _, mSenceKey, _, _, _, lineId = GetTeamMemberInfo(index, false)
	if g_global.teammode then
		local t = g_global.teammemers[name] or {}
		t.level = level
		t.max_hp = max_hp
		t.hp = hp
		t.max_mp = max_mp
		t.mp = mp
		t.is_leader = is_leader
		t.head = head
		t.hair_model = hair_model
		t.hair_color = hair_color
		t.offline = offline
		t.menpai = menpai
		t.objid = objid
		t.gender = gender
		t.IsDead = IsDead
		t.is_same_scene = is_same_scene
		t.guildName = guildName
		t.mSenceId = mSenceId
		t.lineId = lineId
		t.guid = guid
		g_global.teammemers[name] = t
	end
	local player, player2
	for i = 0, 3 do
		player = gUI.Party.BgRoot:GetGrandChild("player" .. i)
		player2 = gUI.Party.BgRoot2:GetGrandChild("player" .. i)
		if player:GetChildByName("name"):GetProperty("Text") == member_name then
			break
		end
	end
	local progress = not offline and max_hp ~= 0 and hp / max_hp or 1
	local hp_bar = player:GetChildByName("hp")
	local hp_bar2 = player2:GetChildByName("hp")
	if IsDead == 0 then
		hp_bar:SetProperty("Progress", tostring(progress))
		hp_bar2:SetProperty("Progress", tostring(progress))
	else
		hp_bar:SetProperty("Progress", tostring(progress))
		hp_bar2:SetProperty("Progress", tostring(progress))
	end
	if hp < max_hp * 0.4 then
		hp_bar:SetProperty("Blink", "true")
		hp_bar2:SetProperty("Blink", "true")
	else
		hp_bar:SetProperty("Blink", "false")
		hp_bar2:SetProperty("Blink", "false")
	end
	if max_hp == 0 then
		hp_bar:GetChildByName("hpLab"):SetVisible(false)
		hp_bar2:GetChildByName("hpLab"):SetVisible(false)
	else
		hp_bar:GetChildByName("hpLab"):SetVisible(true)
		hp_bar2:GetChildByName("hpLab"):SetVisible(true)
	end
	hp_bar:GetChildByName("hpLab"):SetProperty("Text", tostring(hp) .. "/" .. tostring(max_hp))
	hp_bar2:GetChildByName("hpLab"):SetProperty("Text", tostring(hp) .. "/" .. tostring(max_hp))
	progress = not offline and max_mp ~= 0 and mp / max_mp or 1
	local mp_bar = player:GetChildByName("mp")
	local ep_bar = player:GetChildByName("ep")
	local bar
	if menpai == 4 then
		mp_bar:SetProperty("Visible", "false")
		ep_bar:SetProperty("Visible", "true")
		bar = ep_bar
	else
		ep_bar:SetProperty("Visible", "false")
		mp_bar:SetProperty("Visible", "true")
		bar = mp_bar
		if menpai == 1 then
			mp_bar:SetProperty("FrontImage", "UiIamge012:Image_TeamEP")
		elseif menpai == 0 then
			mp_bar:SetProperty("FrontImage", "UiIamge012:Image_TeamSP")
		else
			mp_bar:SetProperty("FrontImage", "UiIamge012:Image_TeamMP")
		end
		if max_mp == 0 then
			bar:GetChildByName("Bar_Lab"):SetVisible(false)
		else
			bar:GetChildByName("Bar_Lab"):SetVisible(true)
		end
		bar:GetChildByName("Bar_Lab"):SetProperty("Text", tostring(mp) .. "/" .. tostring(max_mp))
	end
	if bar then
		bar:SetProperty("Progress", tostring(progress))
	end
	local portrait = player:GetChildByName("Portrait")
	if IsDead == 0 then
		portrait:SetProperty("BackImage", _Party_DeadBackImage)
	else
		if head < 9 then
			portrait:SetProperty("GrayImage", "0")
			local isInStall, modelID = GetPlayerInfo(false, objid)
			if isInStall and modelID >= -7 and modelID <= -2 then
				if offline ~= nil and not offline and is_same_scene then
				else
					portrait:SetProperty("GrayImage", "1")
				end
				portrait:SetProperty("BackImage", "")
			else
			if offline ~= nil and not offline and is_same_scene then
			else
				portrait:SetProperty("GrayImage", "1")
			end
			portrait:SetProperty("BackImage", "")
		end
	end
		if offline ~= nil and not offline then
		else
			portrait:SetProperty("GrayImage", "0")
			portrait:SetProperty("BackImage", _Party_DefaultBackImage)
		end
	end
	if IsDead == 0 then
		local pic2 = player2:GetChildByName("profession_pic")
		pic2:SetProperty("BackImage", _Party_DefaultBackImage)
	elseif offline then
		local pic2 = player2:GetChildByName("profession_pic")
		pic2:SetProperty("BackImage", _Party_DefaultBackImage)
	end
	player:GetChildByName("level"):SetProperty("Text", tostring(level))
	player2:GetChildByName("level"):SetProperty("Text", tostring(level))
	if is_same_scene ~= nil and not is_same_scene then
		_OnParty_MemberAuraUpdate(name, -1)
	end
	_OnParty_UpdateFriendBtn()
	return true
end

function MskAppointTeamLeader()
	if g_global.teammode ~= true then return end
	local t = g_global.teammemers and g_global.teammemers[arg1]
	for i=1,5 do
		if g_global.curteamleader == g_player.name and g_global.curteamleader ~= g_global.teamleader and t and not t.offline then
			AppointTeamLeader(g_global.teamleader)
			sleep(3)
		else
			break
		end
	end
end

function Msk_Script_Party_OnEvent(event)
	if event == "TEAM_MEMBER_JOIN" then
		g_global.teammemers[arg1] = g_global.teammemers[arg1] or {}
		g_global.curteamnum = GetTeamInfo()
		print(arg1,"join team num =",g_global.curteamnum)
	elseif event == "TEAM_MEMBER_LEAVE" then
		g_global.teammemers[arg1] = nil
		g_global.curteamnum = GetTeamInfo()
		if g_global.curteamnum == 1 then 
			g_global.curteamnum = 0
			g_global.teammemers = {}
		end
		print(arg1,"leave team num =",g_global.curteamnum)
	elseif event == "TEAM_MEMBER_ONLINE" then
		g_global.teammemers[arg1].offline = not arg2--掉线
		if arg2 and g_global.teamleader == arg1 and g_global.curteamleader == g_player.name then
			AppointTeamLeader(g_global.teamleader)
		end
		print(arg1,"line change",arg2 and "在线" or "离线")
	elseif event == "TEAM_LEADER_CHANGED" then
		g_global.curteamleader = arg1
		mc_tools.SetListCallBack(nil,"ATL",MskAppointTeamLeader)
		print("leader change",arg1,arg2,arg3)
	end
end

function IsPlayerInTeam(guid,tname)
	local mate_index = 0
    while true do
      local isself, name = GetTeamMemberInfo(mate_index, false)
	  if isself == nil then break end
	  if tname == name then return true end
      mate_index = mate_index + 1
    end
	return false
end

function GetTeamInfo()
	local mate_index = 0
	local leadername
	local isself,name,mem_is_leader
    while true do
      isself, name, _, _, _, _, _, mem_is_leader, _, _, _, _, _, _ = GetTeamMemberInfo(mate_index, false)
      if isself == nil then
        break
      end
      if mem_is_leader then 
	    if isself then
		  leadername = g_player.name
		else
		  leadername = name 
		end
	  end
      mate_index = mate_index + 1
    end
	if mate_index == 1 then mate_index = 0 end
	return mate_index,leadername
end

function MskGetTeamMemberInfo(index)
	local isself, name, level, max_hp, hp, max_mp, mp, is_leader, head, hair_model, hair_color, offline, menpai, objid, gender, IsDead, guildName, mSenceId, is_same_scene,guid, _, mSenceKey, _, _, _, lineId = GetTeamMemberInfo(index, false)
	if g_global.teammode then
		local t = g_global.teammemers[name] or {}
		t.level = level
		t.max_hp = max_hp
		t.hp = hp
		t.max_mp = max_mp
		t.mp = mp
		t.is_leader = is_leader
		t.head = head
		t.hair_model = hair_model
		t.hair_color = hair_color
		t.offline = offline
		t.menpai = menpai
		t.objid = objid
		t.gender = gender
		t.IsDead = IsDead
		t.is_same_scene = is_same_scene
		t.guildName = guildName
		t.mSenceId = mSenceId
		t.lineId = lineId
		t.guid = guid
		g_global.teammemers[name] = t
		msk.warn('取组队信息成功',index,guid)
	else
		msk.warn('非组队模式试图获取组队信息?',index)
	end
end

function ParseTeam(target,nteam)
	print("开始组队")
	local at = g_global.accountdata
	target = target or 'tsk'
	nteam = nteam or at.teamnun or tonumber(g_global.script_config['组队人数']) or 1
	--if nteam <= 1 then return nteam end
	at.teamnun = nteam
	mc_tools.SendAndWait('nteam',pickle{tar = target ,num =nteam})
	g_global.teammemernames = {}
	for i=0,nteam-1 do
		local _, name = GetTeamMemberInfo(i,false)
		g_global.teammemernames[i+1] = name
	end
	if nteam < 2 then
		g_global.teammemernames[1] = g_player.name
	end
	return nteam
end

--Water BeWater Name Guid
function Msk_HomeLandCash_Update()
  local t = g_global.curcasher
  if nil == t then return end
  local t2 = g_global.curfriendrain
  t.bSelf, t.state, t.maxCount, t.curCount, _,t.Ramain, t.cashModle = GetGardenInfo()
  if t.bSelf then g_global.selfcasher = t end
  local name = t2 and t2.Name or "??"
  if t.Ramain > 0 and t.state ~= 4 then
	if not t.bSelf and (nil == g_global.waterlv or g_global.waterlv > g_player.lv) then
		print("花园可以浇水",name)
		FarmAddRain()
		if t2 then t2.Water = true end
	elseif t.Ramain > 0 then
		print("自己的花园 不自动浇水")
		g_global.selfcanwater = true
	end
  else
	if t.bSelf then
		g_global.selfcanwater = false
	end
	print("花园不浇水",name)
  end
  t.update = true
  g_global.curcasher = nil
  g_global.curfriendrain = nil
  return true
end

function MskAskHomeInfo(type,guid)
	if type == 0 then
		local tguid = guid
		if guid== -1 then tguid = g_player.guid end
		g_global.cashers[tguid] = g_global.cashers[tguid] or {}
		g_global.curcasher = g_global.cashers[tguid]
		g_global.curfriendrain = g_global.friendrains[tguid]
		g_global.curcasher.update = false
	end
	AskHomeInfo(type,guid)
end

function UpdateAllGarden(all)
	if all then
		g_global.cashers = {}
	end
	local bAll = true
	for k,v in pairs(g_global.friends) do
		if v.region ~= "" and (all or not g_global.cashers[k] or g_global.cashers[k].Water == false) then
			MskAskHomeInfo(0,k)
			sleep(0.8)
			bAll = false
		end
	end
	if not bAll then 
		return UpdateAllGarden(false) 
	end
end

function ResetAllTeamReady()
	for k,v in pairs(g_global.teammemers) do
		v.ready = false
	end
end

function IsAllTeamReay()
	for k,v in pairs(g_global.teammemers) do
		if v.guid == g_player.guid and v.ready == false then
			return false
		end
	end
	return true
end

function DealSelfCasher(farmself,buyItem)
	print("开始处理自己的浇水")
	buyItem = buyItem or farmself
	local firstLand = true
	local bContinue = true
	local nCollect = 0
	local bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
	while bContinue do
		bContinue = false
		local bSelf, state, maxCount, curCount,_, Ramain, cashModle = GetGardenInfo()
		if nil == bSelf or firstLand then
			print("更新花园数据")
			firstLand = false
			UI_HomeLandCash_ShowMain()
			sleep(2)
			bSelf, state, maxCount, curCount,_, Ramain, cashModle = GetGardenInfo()
			UI_HomeLandCash_CloseHomeLandCash()
		end
		if nil ~= bSelf then
			if maxCount > 0 and maxCount == curCount and nCollect < 2 then
				print("采摘花朵")
				CollectObj(nil,176, 109, 140,120)
				nCollect = nCollect + 1
				bContinue =true
			elseif maxCount == 0 then
				local slot,bag,num = GetItemByName("花种·",true)
				if not slot then
					--[[
					while buyItem and GetRemainProtectTime() > 0 do
						ShowTaskState("等待保护时间过后买花种")
						sleep(5)
					end
					--]]
					print("购买花种")
					if PathTo(41,140,-1) then
						--pl = g_player.chara
						local ch = GetCharByName("庄园管家")
						if ch then
							SelectGossipOption(ch.id, 3, 0)
							sleep(1)
							if g_player.lv >= 40 and bindMoney >= 10 then
								BuyItem(0, 2, 1)
							elseif g_player.lv >= 30 and bindMoney>=4 then
								BuyItem(0, 1, 1)
							elseif g_player.lv >= 20 and bindMoney >=2 then
								BuyItem(0, 0, 1)
							else
								return
							end
							sleep(1)
							slot,bag,num = GetItemByName("花种·",true)
						end
					end
				end
				if slot then
					print("种花")
					local oldnum = num
					for i=1,3 do
						PathTo(176, 109, 140)
						--pl = g_player.chara
						UseItem(bag, slot)
						sleep(4)
						slot,bag,num = GetItemByName("花种·",true)
						num = num or 0
						if oldnum ~= num then 
							bContinue =true
							break 
						end
					end
				end
			elseif bSelf and farmself and Ramain > 0 then
				FarmAddRain()
				sleep(1)
			end
		end
	end
end

function trypick(name,pnum,fit)
	print('trypick 准备拾取:',name)
	local slot,bag,num = GetItemByName(name)
	local oldnum = num or 0
	for i=1,pnum do
		sleep(1)
		slot,bag,num = GetItemByName(name,not fit)
		num = num or 0
		if num == oldnum then
			PickItemByName(name)
		else
			print('trypick 拾取物品成功!!!',name)
			return true
		end
	end
	print('trypick 拾取物品失败了!!!',name)
	return false
end

function FightFengYe(guid)
	if true == mc_tools.GetData('fengye') or not GetGameObjectByName('神灵草') then return end
	local id,dx,dy,ch
	lastx,lasty = 174, 136
	for i=1,60 do
		ch = GetCharByName("枫叶精",false,false,true)
		if ch then
			lastx,lasty = ch.x ~= 0 and ch.x or lastx, ch.y ~= 0 and ch.y or lasty
			FightChar(ch)
		else
			id,dx,dy = GetGameObjectByName('神灵草')
			if not id then
				PathTo(174, 136, 140)
				id,dx,dy = GetGameObjectByName('神灵草')
			end
			if not id and not GetCharByName("枫叶精",false,false,true) then
				break
			end
			if id and guid == g_player.guid then
				CollectGameObj(id,dx,dy)
			end
		end
		sleep(2)
	end
	local logger = g_global.logger
	if false == trypick('枫叶精内丹',20) then
		logger:error('枫叶精内丹拾取失败!')
	else
		logger:info('枫叶精内丹拾取成功!')
	end
	mc_tools.SetData('fengye',true)
end

function AddGuild()
	g_global.script_config = mc_tools.GetData('script_config')
	local cfg = g_global.script_config
	local tguildName = cfg['加入帮派']
	if not tguildName or tguildName == '不加入' or #tguildName < 2 then return end
	--[[准备加帮]]
	if gUI_INVALIDE == GetGuildBaseInfo() then
		print('尝试加入帮派:',tguildName)
		local tguild = nil
		mskco.SetFunctionHandler("_OnGuildC_GuildListPage",
				function (pageNum, memNum, guildID, guildName, leaderName, level, population, isRequested, maxPop) 
					--print('_OnGuildC_GuildListPage->',pageNum, memNum, guildID, guildName, leaderName, level, population, isRequested, maxPop)
					if guildName == tguildName  then
						tguild = guildID
						mskco.HandleEvent('guild_get')
					end
				end)
		for i=1,3 do
			GuildSearchRequestGuild(tguildName)
			sleep(3,'guild_get')
			if tguild then break end
		end
		if tguild == nil then
			print('获取帮派失败:',tguildName)
			local logger = g_global.logger
			logger:error('加入帮派:%s 失败，无法获取帮派ID',tguildName)
			return
		end
		mskco.SetEventProducer('_OnGuildC_JoinSucc','guild_join',nil,true)
		for i=1,3 do
			ApplyJoinGuild(tguild)
			sleep(3,'guild_join')
			if  gUI_INVALIDE ~= GetGuildBaseInfo() then break end
		end
		sleep(2)
		if gUI_INVALIDE == GetGuildBaseInfo() then
			print('加入帮派失败 申请超时:',tguildName)
			local logger = g_global.logger
			logger:error('加入帮派:%s 申请超时',tguildName)
			return
		end
		DealBag()
	end
end

function DoXcjTask()
	AddGuild()
	if g_global.xcjtaskstart then return end--防止重入
	g_global.xcjtaskstart = true
	local selfwater = mc_tools.GetData('selfwater')
	if selfwater == nil then
		g_global.selfcanwater = nil
		DealSelfCasher(false,true)
		while g_global.selfcanwater == nil do
			MskAskHomeInfo(0,-1)
			sleep(2)
		end
		selfwater = g_global.selfcanwater
		mc_tools.SetData('selfwater',selfwater)
	end
	local guid,name
	local idx,oldidx,oldfguid = 0,0,g_global.farmguid
	repeat
		CreateTaskObj(8100):Accept()
		idx =  mc_tools.SendAndWait('tstep','xcj_'..oldidx)
		idx = tonumber(idx)+1
		oldidx = idx
		local tinfo = mc_tools.GetData('xcjat')
		if tinfo == 'end' then break end
		if idx%4 == 0 then
			idx = 4
		elseif idx > 4 then
			idx = oldidx%4 
		end
		guid,name = tinfo.guid,tinfo.name
		print('仙草精目标人物',guid,name)
		guid = tonumber(guid)
		g_global.farmguid = guid
		if idx == 1 then
			print("仙草精第1步1 加好友")
			local bfind = false
			while guid ~= g_player.guid  do
				GetAllFriendList()
				--[[
				if g_global.friends[guid] then
					MskAskHomeInfo(0,guid)
					break
				end
				--]]
				for fguid,ft in pairs(g_global.friends) do
					if ft.name == name or fguid == guid or GetFriendPointDetail(guid) then
						MskAskHomeInfo(0,guid)
						bfind = true
						break
					end
				end
				if bfind then break end
				AddToFriend(name)
				sleep(2)
			end
			print("仙草精第1步2 好友等级升级到2")
			local tpl,bag,sloat,friLevel
			while guid ~= g_player.guid do
				friLevel = GetFriendPointDetail(guid)
				if friLevel >=2 then
					break
				else
					SendCmd("jh","370,250,50",name)
					PathTo(370,250,50)
					sleep(2)
				end
				tpl = GetCharByType(gUI_CharType.CHAR_PLAYER,true,name)
				if tpl then
					print("已经找到",tpl.name)
					tpl:Select()
					slot,bag = GetItemByName("极品仙桃")
					if slot then
						UseItem(bag,slot)
					end
				end
				sleep(2)
			end
		elseif idx == 2 then
			--防止进错庄园
			if GetMPlayerMapId() == 140 then
				InstanceExit()
				msk.sleep(2)
				msk.sleep(5)
			end
			print("仙草精第2步 移动到指定庄园位置")
			if guid ~= g_player.guid then
				mc_tools.SendCmd("eh",nil,name)
			end
			sleep(3)
			local enum = 0
			while GetMPlayerMapId() ~= 140 do
				if enum == 20 then BackToLogin() end
				EnterFriendFarm(guid)
				sleep(3)
				if guid ~= g_player.guid then
					mc_tools.SendCmd("eh",nil,name)
				end
				enum = enum + 1
			end
			enum = 0
			while not PathTo(175,120,140) do
				if enum == 10 then BackToLogin() end
				sleep(2)
				enum = enum + 1
			end
		elseif idx == 3 then
			if guid == g_player.guid then
				for i=1,15 do
					if GetCharByName("仙草精",false,false,true) or GetGameObjectByName('仙灵芝') then
						break
					end
					DealSelfCasher(true)
					sleep(2)
				end
			end
		elseif idx == 4 then
			print("仙草精第4步 准备战斗")
			local rnum = 0
			local id,dx,dy,ch
			local lastx,lasty = 175, 120
			while true do
				if rnum >= 40 then--超时了
					break
				end
				PathTo(175, 120, 140)
				id,dx,dy = GetGameObjectByName('仙灵芝')
				ch = GetCharByName("仙草精",false,false,true)
				if not id and not ch then
					sleep(2)
					ch = GetCharByName("仙草精",false,false,true)
					if not id and not ch then
						sleep(2)
						break
					end
				end
				if id and guid == g_player.guid then
					CollectGameObj(id,dx,dy)
				end
				if ch then 
					lastx,lasty = ch.x ~= 0 and ch.x or lastx, ch.y ~= 0 and ch.y or lasty
					FightChar(ch)
				end
				sleep(3)
				rnum = rnum + 1
			end
			if guid == g_player.guid then
				g_global.homebron = false
				mc_tools.SetData('selfwater',false)
			end
			FightFengYe(guid)
			PathTo(lastx, lasty, 140)
			trypick('庄园徽章',20)
		end
	until false
	g_global.farmguid = oldfguid
	g_global.xcjtaskstart = false
	local t,slot,bag,num
	repeat 
		sleep(1)
		t = CreateTaskObj(8129):Accept()
		slot,bag,num = GetItemByName('庄园徽章')
		num = num or 0
	until GetQuestState(8129) == QUEST_STATUS_AVAILABLE or num < 3
	DealBag()
end

local oldfriendlist
function Msk_OnFri_AddRelation(relation_type, guid, level, name, region, job, index)
	if relation_type ~= 1 then return end
	local t = oldfriendlist and oldfriendlist[guid] or {}
	t.level = level
	t.name = name
	t.region = region--地图
	t.job = job
	t.index = index
	t.update = true
	g_global.friends[guid] = t
end


function GetAllFriendList()
	g_global.stophookfriend = true
	g_global.friendnum =  SortRelationList()
	oldfriendlist = g_global.friends
	g_global.friends = {}
	local old = _OnFri_AddRelation
	_OnFri_AddRelation = Msk_OnFri_AddRelation
	GetFriendGroupList(0, 15)
	_OnFri_AddRelation = old
	--mc_tools.SetListCallBack(nil,nil,UpdateAllGarden,false)
	g_global.stophookfriend = false
end

function PlayerInit(rs)
	print("player init ")
	g_player.chara = nil
	while not g_player.updatechara(rs) do
		sleep(2)
	end
	g_player:init()
	print("player init ok",g_player.name)
end

function init_login()
	
end

local loschattm = 0
function CheckOffline()
	--print('CheckOffline')
	do return end
	if gUI_CurrentStage ~= 3 then
		--print('非游戏状态')
		loschattm = 0
		return
	end
	if loschattm > 1 then
		print("超时重上..............")
		BackToLogin()
		loschattm = 0
		return
	end
	local ctm = os.time()
	if g_global.lastchat and ctm - g_global.lastchat > 40 then
		--print("超时喊话...")
		SendChatMessage(5,os.date(),"")
		loschattm = loschattm + 1
	else
		loschattm = 0
		--print("没有超时...")
	end
end

function WorldEnterInit()
	--先等待角色初始化
	PlayerInit("WorldEnterInit")
	--g_global.online = true
	if g_global.state == "login" then
		if false and g_player.lv >= (cfg.splv[at.script] or 99) then
			return mc_tools.CharExit("成功")
		end
		--init_login()
		local at = g_global.accountdata
		logger = g_global.logger
		logger:info("角色登入:%s lv:%d",g_player.name,g_player.lv)
		g_global.numDead = mc_tools.GetData('numDead') or 0
		mskg.unhookbuff()
		g_global.farmguid = g_player.guid
		if math.random(1,2) == 1 then
			UIConfig:setHideFashion(false)
		end
		local bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
		msk.send("extern#name="..g_player.name..",guid="..g_player.guid..",lv="..g_player.lv..",status=运行中"..",money="..nonBindemoney..",bindmoney="..bindMoney)
		msk.send('pid#'..msk.pid())
		g_global.state = "run"
		mc_tools.SetLoop("CheckHp",CheckHp,3)
		mc_tools.SetLoop("CheckOffline",CheckOffline,20)
		msk.send("nfriend#"..g_player.guid)
		print("GetAllFriendList ")
		GetAllFriendList()
		print("GetAllFriendList ok")
		if g_player.hp == 0 then
			sleep(1)
			trueRelive()
			sleep(2)
		end
		g_global.acceptfrient = true
		if g_global.testlv < 3 then return end
		--if msk.ver_test  then return true end
		if g_global.test_wait_trade then
			return Wait_Trade() 
		end
		print("开始组队")
		at.teamnun = at.teamnun or tonumber(g_global.script_config['组队人数'] or 1)
		mc_tools.SendAndWait('nteam',pickle{tar = 'tsk' ,num =at.teamnun})
		g_global.teammemernames = {}
		for i=0,at.teamnun-1 do
			local _, name = GetTeamMemberInfo(i,false)
			g_global.teammemernames[i+1] = name
		end
		if at.teamnun < 2 then
			g_global.teammemernames[1] = g_player.name
		end
		local f = loadfile(g_global.appdir..'脚本/'..at.script)
		f = coroutine.create(f)
		coroutine.resume(f)
		do return end
		if at.script == "1_35" then
			--g_global.mainfun = BeginTaskLoop
			RunTask1_35()
		elseif at.script == "test" then
			RunScript_Test()
		end
		if at.script == "1_35" then
			g_global.mainfun = BeginTaskLoop
			if at.teamnun > 1 then
				RunStepFun = RunTaskStep
				BeginTaskLoop(true)
				msk.send("tskteam#"..at.teamnun)
			else
				BeginTaskLoop()
			end
		end
	end
end

function WorldEnter()
	print("<<<<<<<<<<<<<<<<<<<<<<<<<-WorldEnter->>>>>>>>>>>>>>>>>>>>>>>>")
	for k,v in pairs(g_global.timers) do
		print("reset timer:",v[1], k, v[2])
		AddTimerEvent(v[1], k, v[2])
	end
	collectgarbage("collect")--垃圾收集
	g_global.online = true
	mc_tools.SetListCallBack1(nil,"WorldEnterInit",WorldEnterInit)--延时
end


logTask = logTask or {}
function ObAEvent(event)
	print("Script_QB_OnEvent:"..event,arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8,arg9,arg10)
	if false and event == "PLAYER_ACCEPT_NEW_QUEST" then
		if logTask[arg1] then return end
		logTask[arg1] = true
		print("now log")
		local g_TaskFile = io.open("G:/bitbuket/tzj/script/alltask.lua","a")
		g_TaskFile:write("--"..arg4.."\n")
		g_TaskFile:write("["..arg1.."] = {\n")
		local _, strObjective = GetQuestInfoLua(arg1,2)
		local bg = strObjective:find([[\n]])
		local first,last
		while bg do
			first = strObjective:sub(1,bg-1)
			print("save:"..first)
			g_TaskFile:write("    {\"",first,"\",\"??\"},\n")
			strObjective = strObjective:sub(bg+2,-1)
			bg = strObjective:find([[\n]])
		end
		g_TaskFile:write("    {\"",strObjective,"\",\"??\"},\n")
		print("save:"..strObjective)
		g_TaskFile:write("},\n")
		g_TaskFile:close()
		print("log over")
	end
end

local equipindex = {
	1,3,4,5,6,8,11,9,12,10,21
}

local menpaieuipjob = {
	[0] = 1,
	[1] = 2,
	[2] = 4,
	[3] = 8,
	[4] = 16,
}

function GetEquipTypeIndex(tp)
	for i=1,#equipindex do
		if equipindex[i] == tp then return i-1 end
	end
end


function Wait_Trade()
	g_global.notmove = true--不能移动
	local cfg = g_global.script_config
	local tradecoord = cfg['交易地点'] or '五方城|433|170'
	local map,x,y = tradecoord:match('(.+)|(%d+)|(%d+)')
	x,y = tonumber(x),tonumber(y)
	map = GetMapByName(map)
	map = map and map.MapID
	map = tonumber(map)
	PathTo(x,y,map)
	local tar,tarname
	local stm = 2
	mskco.SetEventProducer('_OnExchange_Show','trade_open',nil,true)
	mskco.SetEventProducer('_Exchange_CloseTrade','trade_close')
	mskco.SetFunctionHandler("_OnExchange_Lock",
			function (bMyLocked, bOtherLocked) 
				if bOtherLocked then
					mskco.HandleEvent('trade_lock')
				end
			end)
	print('等待保护时间',GetRemainProtectTime())
	while GetRemainProtectTime() > 0 do
		ShowTaskState('等待保护时间')
		sleep(5)
	end
	ShowTaskState('等待交易')
	repeat
		_Exchange_ClosePanel()--关掉以前的交易框
		print('开始等待交易')
		tarname = mskco.Wait('trade')--等待交易申请
		print('有人申请交易:',tarname)
		_Exchange_ClosePanel()--关掉以前的交易框
		tar = GetCharByType(gUI_CharType.CHAR_PLAYER,true,tarname)--获取交易对象
		if tar and tar.dis < 10 then--判交易对象距离是否准确
			SwapRequestTrade(tar.id)--邀请交易
			sleep(stm,'trade_open')--等待交易开始
			if gUI.Exchange.Root:IsVisible() then--交易框打开了!
				SwapSetTradeGold(0)--锁定交易 不给钱!
				SwapLockTrade()
				sleep(stm,'trade_lock')--等待对方锁定
				SwapCommitTrade()
				sleep(stm,'trade_close')
			end
		else
			msk.warn('找不到',tarname,'距离',tar and tar.dis)
		end
	until false
end

local jptm = 0
function ItemDeal(num)
	num = num or 0--可以再次放技能了
	if num > 5 then 
		TidyBag(1)
		TidyBag(2)
		sleep(2)
		return 
	end
	local bag = 1
	local bagnum = 40+gUI.Bag.BagExtNumCur*10
	local psex = gUI_MainPlayerAttr.Sex
	local name,minLevel, job, sex,canSell,itemType,sellPrice,equipKind, canDiscard,curscore,tarscore,eidx,inum
	--local --pl = g_player.chara
	local bnotstop = true
	local bequip = false
	local pljob = gUI_MainPlayerAttr.MenPai
	--local pljob = menpaieuipjob[g_player.menpai] or -1
	local curtm = os.time()
	for i=0,bagnum-1 do
		name, _, _, minLevel, job, sex, _, _, canSell,canDiscard, _, _, _, itemType, _, _, _, _, sellPrice, _, equipKind= GetItemInfoBySlot(bag, i)
		--print(name,minLevel, job, sex,canSell,itemType,sellPrice,equipKind)
		if g_global.isfighting then
			g_global.itemupdate = true
		end
		if false ==  g_global.isfighting and bnotstop and itemType == 2 and (job == -1 or checkJobKindIsCanUse(pljob, job)) and (sex == -1 or sex == psex ) then--装备
			if minLevel <= g_player.lv then
				curscore = GetEquipScore(bag, i)
				eidx = GetEquipTypeIndex(equipKind)
				tarscore = GetEquipScore(gUI_GOODSBOX_BAG.myequipbox,eidx)
				tarscore = tarscore or 0
				if curscore > tarscore then--装备分数更高的装备
					print("equip:",name,eidx,curscore,tarscore)
					MoveItem(bag, i, gUI_GOODSBOX_BAG.myequipbox, eidx)
					sleep(0.3)
					bequip = true
				elseif false and canDiscard == 1 then--或者丢弃
					print("drop:"..name)
					DropItem(bag,i)
					sleep(0.3)
				else
					print("skip equip",itemType,equipKind,name,minLevel, job, sex,canSell,sellPrice, canDiscard,curscore,tarscore)
				end
			else
				print("skil equip because lv",name,minLevel)
			end
		elseif bnotstop and  itemType == 6 and minLevel <= g_player.lv then--使用礼包
			if string.find(name,"奖励礼包") or string.find(name,"仙草精的恩赐") then
				print("use:"..name)
				bequip = true
				inum = GetItemBaseInfoBySlot(bag, i) or 0
				for j=1,inum do
					UseItem(bag,i)
					sleep(0.3) 
				end
			elseif string.find(name,"永久背包")  then
				print("use:"..name)
				inum = GetItemBaseInfoBySlot(bag, i) or 0
				for j=1,inum do
					UseItem(bag,i)
					sleep(0.3) 
				end
			elseif g_player.lv >= 25 and (string.find(name,"精魄丹（中）") or string.find(name,"精魄丹（小）")) then
				print("use:"..name)
				inum = GetItemBaseInfoBySlot(bag, i) or 0
				for j=1,inum do
					UseItem(bag,i)
					sleep(0.3) 
				end
			elseif g_player.lv >= 30 and (string.find(name,"经验丹") or string.find(name,"每日礼包"))  then
				print("use:"..name)
				inum = GetItemBaseInfoBySlot(bag, i) or 0
				for j=1,inum do
					UseItem(bag,i)
					sleep(0.3) 
				end
			elseif g_player.lv >= 35 and string.find(name,"金袋子") then
				print("use:"..name)
				inum = GetItemBaseInfoBySlot(bag, i) or 0
				for j=1,inum do
					UseItem(bag,i)
					sleep(0.3) 
				end
			elseif g_player.lv >= 30 and string.find(name,"枫叶精内丹") then
				print("use:"..name)
				UseItem(bag,i)
				sleep(0.3) 
			elseif string.find(name,"精魄丹（大）") and (curtm - jptm > 18*60*60+2) then
				jptm = curtm
				print("use:"..name)
				UseItem(bag,i)
				sleep(0.3) 
				print("skip",itemType,equipKind,name,minLevel, job, sex,canSell,sellPrice, canDiscard)
			else
				print("skill use:"..name)
			end
		elseif name then
			print("skip",name,g_global.isfighting,bnotstop, itemType,job ,minLevel)
		end
	end
	if bequip then return ItemDeal(num+1) end
	--TidyBag(1)
	--TidyBag(2)
	--sleep(2)
end
local needlearnskil3 = {
	10,6,9,0,2,4,5,3,7,8,11,1
}
local needlearnskil0 = {
	5,3,1,6,0,4,2,7,8,9,10,11
}
local needlearnskil4 = {
	11,10,0,4,1,2,3,5,6,7,8,9
}
local needlearnskil2 = {
	0,4,3,5,6,7,8,9,10,11,2,1
}
function LearProSkill()
	--沉香技能学习
	local bStudy = true
	local skill_id
	local curlearnskilllist
	if g_player.menpai == 3 then
		curlearnskilllist = needlearnskil3
	elseif g_player.menpai == 0 then
		curlearnskilllist = needlearnskil0
	elseif g_player.menpai == 2 then
		curlearnskilllist = needlearnskil2
	elseif g_player.menpai == 4 then
		curlearnskilllist = needlearnskil4
	else
		curlearnskilllist = {11,10,9,8,7,6,5,4,3,2,1,0}--倒着学
	end
	for i=1,#curlearnskilllist do
		_, skill_id, bStudy = GetCharSpellInfo(1, curlearnskilllist[i])
		for j=1,5 do--每个技能有限学习5次
			if bStudy then
				LearnSkill(skill_id)
				sleep(1)
				_, _, bStudy = GetCharSpellInfo(1, curlearnskilllist[i])
			else
				break
			end
		end
	end
end
--[[
[6184] msk_tg:1  {defy=true,daily=false,name=绑定手机,complated=false,id=900,submited=false,tracking=false,revactive=0}   
[6184] msk_tg:2  {defy=true,daily=false,name=绑定手机安全令牌,complated=false,id=901,submited=false,tracking=false,revactive=0}   
[6184] msk_tg:3  {defy=false,daily=false,name=进行5次装备融魂,complated=false,id=28,submited=false,tracking=true,revactive=0}   
[6184] msk_tg:4  {defy=false,daily=false,name=加入帮会,complated=false,id=30,submited=false,tracking=false,revactive=0}   
[6184] msk_tg:5  {defy=false,daily=false,name=收获一次花朵,complated=false,id=42,submited=false,tracking=false,revactive=0}   
[6184] msk_tg:6  {defy=false,daily=false,name=与1个好友的好友度达到5级,complated=false,id=44,submited=false,tracking=false,revactive=0}   
[6184] msk_tg:7  {defy=false,daily=false,name=通关白羽之巅,complated=false,id=49,submited=false,tracking=false,revactive=0}   
[6184] msk_tg:8  {defy=false,daily=false,name=角色等级达到35级,complated=false,id=6,submited=false,tracking=true,revactive=0}   
[6184] msk_tg:9  {defy=false,daily=true,name=为花朵浇水一次,complated=true,id=1511,submited=false,tracking=false,revactive=10}   
[6184] msk_tg:10  {defy=false,daily=true,name=白羽之巅（刷宝）,complated=false,id=1504,submited=false,tracking=false,revactive=10}   
[6184] msk_tg:11  {defy=false,daily=true,name=挑战帮会试炼,complated=false,id=1513,submited=false,tracking=false,revactive=10}   
[6184] msk_tg:12  {defy=false,daily=true,name=破阵,complated=false,id=1502,submited=false,tracking=false,revactive=20}   
[6184] msk_tg:13  {defy=false,daily=true,name=融魂1次,complated=false,id=1512,submited=false,tracking=false,revactive=10}   
[6184] msk_tg:14  {defy=false,daily=true,name=帮会狩猎任务,complated=false,id=1507,submited=false,tracking=false,revactive=10}   
[6184] msk_tg:15  {defy=false,daily=true,name=聚灵20次,complated=false,id=1514,submited=false,tracking=false,revactive=10}   
[6184] msk_tg:16  {defy=false,daily=true,name=异境试炼,complated=false,id=1501,submited=false,tracking=false,revactive=20}   
[6184] msk_tg:为花朵浇水一次  给自己或好友的花朵浇水一次  false  true  true  1  1  0  0  0  0  10  20000  0  UiIamge013:CarnivalLevel_n  false   
]]
--8100,--击败仙草精
--4502,--养魔境
function DaysTask()
	--日常
	CreateTaskObj(7000):Accept()
	CreateTaskObj(8100):Accept()
	CreateTaskObj(4502):Accept()
	--活跃
	local _, _, _, _, _complate = GetGoalInfo(1511)
	if _complate == false then
		GetAllFriendList()
		for k,v in pairs(g_global.friends) do
			if k ~= g_player.guid then
				MskAskHomeInfo(0,k)
				sleep(1)
			end
		end
	end
	_, _, _, _, _complate = GetGoalInfo(1512)
	if _complate == false then
		BlendAnyOne()
	end
end

function DealBag(num,lv)
	print("bg")
	num = num or 0
	if num > 2 then return end
	lv = lv or g_player.lv
	if lv == 30 then
		if g_player.menpai == 0 then
			ActiveRune(59)
			InsertRune(0, 59)
			sleep(1)
		elseif g_player.menpai == 3 then	
			print("符文")
			ActiveRune(32)
			InsertRune(0, 32)
			sleep(1)
		end
	end
	print("1")
	local slot,bag = GetItemByName("新手时装")
	if slot then UseItem(bag,slot) sleep(1) end
	if lv == 11 then
		slot,bag = GetItemByName("小浣熊")
		if slot then UseItem(bag,slot) sleep(1) end
	elseif lv >= 16 then--十六级以上 处理宠物
		slot,bag = GetItemByName(g_player.menpainame.."仙灵")
		if slot then UseItem(bag,slot) sleep(1) end
		local FaerieId, nLv, train, trainMax, str, dex, int, con, modelId, name, nextLv, hasNext, score = GetFaerieBase()
		local _, _, _, _, level = GetPlaySelfProp(4)
		if FaerieId ~= -1 and  train < trainMax then
			if nLv == 1 then
				for i=1,5 do
					slot,bag = GetItemByName("炼气散")
					if slot then 
						UseItem(bag,slot) 
						sleep(0.5) 
					else
						break
					end
				end
			elseif nLv == 2 then
				for i=1,5 do
					slot,bag = GetItemByName("筑基散")
					if slot then 
						UseItem(bag,slot) 
						sleep(0.5) 
					else
						break
					end
				end
			elseif nLv == 3 then
				for i=1,5 do
					slot,bag = GetItemByName("结丹散")
					if slot then 
						UseItem(bag,slot) 
						sleep(0.5) 
					else
						break
					end
				end
			elseif nLv == 4 then
				for i=1,5 do
					slot,bag = GetItemByName("元婴散")
					if slot then 
						UseItem(bag,slot) 
						sleep(0.5) 
					else
						break
					end
				end
			end
			FaerieId, nLv, train, trainMax, str, dex, int, con, modelId, name, nextLv, hasNext, score = GetFaerieBase()
		end
		if FaerieId ~= -1 and nextLv <= level and train==trainMax then
			AskFaerieLvup()
			sleep(1)
		end
		--处理技能书
		slot,bag = GetItemByName("自愈")
		if slot then UseItem(bag,slot) sleep(1) end
		
	end
	print("2")
	local itemID = 361643
	--先处理背包扩展
	for i=1,8 do
		bag, slot = GetItemInBagIndex(itemID)
		if bag == gUI_GOODSBOX_BAG.backpack0 and slot ~= -1 then
			print("扩展背包!!!")
			UseItem(bag, slot)
			sleep(0.5)
		else
			break
		end
	end
	local goalList = GetGoalList()
	local _item
	for i = 1, table.maxn(goalList) do
		_item = goalList[i]
		if _item.complated then
			local _name = GetGoalInfo(_item.id)
			print("完成目标:",_name)
			GoalSubmit(_item.id)
			sleep(0.5)
		end
	end
	local bnext = false
	bag = 1
	local bagnum = 40+gUI.Bag.BagExtNumCur*10
	local tname = "奖励礼包"
	local name
	for i=0,bagnum-1 do
		_, _, _, name = GetItemBaseInfoBySlot(bag, i)
		if name and string.find(name,tname) then
			print("使用礼包:",name)
			UseItem(bag,i)
			sleep(0.5)
			bnext = true
		end
	end
	if bnext and num < 2 then
		return DealBag(num+1,lv)
	else--每级清理一下装备
		ItemDeal()
	end
	LearProSkill()
end

function trueLevelUpCallBack(lv)
	lv = lv or g_player.lv or 0
	if g_global.lastcallbacklv == lv then return end
	g_global.lastcallbacklv = lv
	DealBag(0,lv)
	--浇水
	if lv == 29 then
		GetAllFriendList()
		for k,v in pairs(g_global.friends) do
			if  v.region ~= "" and  k ~= g_player.guid then
				MskAskHomeInfo(0,k)
				sleep(1)
			end
		end
	end
	local bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
	msk.send("extern#lv="..g_player.lv..",money="..nonBindemoney..",bindmoney="..bindMoney)
end

function LevelUpCallBack(lv)
	--if g_global.state == "login" then return end
	mc_tools.SetListCallBack(nil,"LUC",trueLevelUpCallBack,tonumber(lv))
end

function AutoEquip(bag, slot, index)
	if bag == gUI_GOODSBOX_BAG.backpack0 and index ~= -1 then
		MoveItem(bag, slot, gUI_GOODSBOX_BAG.myequipbox, index)
	end
	return true
end

function SendCmd(cmd,data,name)
	data = data or "ov"
	SendChatMessage(g_CHAT_TYPE.TELL, "mskcm#"..cmd.."#"..data,name)
end

local _relivev1,_relivev2
function _trueRelive(tm, btncount, v1, v2, v3, bShow, mapId)
	v1 = v1 or _relivev1 or 0
	v2 = v2 or _relivev2 or 1
	_relivev1,_relivev2 = v1,v2
	local CurrentMapID = GetMPlayerMapId()
	local mp = GetMapById(CurrentMapID)
	if g_global.reliveid then
		v1, v2 = g_global.reliveid,g_global.reliveid
	elseif CurrentMapID ~= 10 and not mp.IsInstance and GetItemByName'还魂丹' then
		v2 = v1
	elseif g_player.lv < 26 then
		v2 = v1
	end
	sleep(2)
	for i=1,3 do
		Revive(v2)
		sleep(1)
		if g_player.hp ~= 0 then return end
	end
	for i=1,3 do
		Revive(v2)
		sleep(5)
		if g_player.hp ~= 0 then return end
	end
	for i=1,2 do
		Revive(v1)
		sleep(10)
		if g_player.hp ~= 0 then return end
	end
	if g_player.hp ~= 0 then return end
	BackToLogin()
	for i=1,5 do
		sleep(1)
		Revive(v2)
	end
	--g_global.nowrelive = false
	if g_player.hp == 0 then 
		logger:warn("尝试复活失败 退出")
		Exit() 
	end
end

function trueRelive(tm, btncount, v1, v2, v3, bShow, mapId)
	--local --pl = g_player.chara
	--g_global.nowrelive = tm and true
	_trueRelive(tm, btncount, v1, v2, v3, bShow, mapId)
	--g_global.nowrelive = false
end

function QuickRevive(time, btncount, v1, v2, v3, bShow, mapId)
	g_global.numDead = g_global.numDead+1
	if g_global.ksidx ~= nil then g_global.ksidx = 1 end
	mc_tools.SetListCallBack(nil,"QuickRevive",trueRelive,time, btncount, v1, v2, v3, bShow, mapId)
end

function MyQuestSubmit(strTitle, QuestId)
	print("MyQuestSubmit",QuestId, MapId, ChapterPictureStr, QuestTitle)
	--do return end
	do return mskco.HandleEvent('stask') end
	if nil ~= g_global.runthread and g_global.online  then
		--print("MyQuestSubmit resume g_global.runthread")
		local ret,err = coroutine.resume(g_global.runthread,0,"spell")
		if not ret then  msk.warn("MyQuestSubmit resume error",err) end
	else
		--print("MyQuestSubmit no runthread")
	end
end

function MyQuestAccepted(QuestId, MapId, ChapterPictureStr, QuestTitle)
	print("MyQuestAccepted",QuestId, MapId, ChapterPictureStr, QuestTitle)
	--do return end
	do return mskco.HandleEvent('atask') end
	if nil ~= g_global.runthread and g_global.online  then
		--print("MyQuestAccepted resume g_global.runthread")
		local ret,err = coroutine.resume(g_global.runthread,0,"utask")
		if not ret then  msk.warn("MyQuestAccepted resume error",err) end
	else
		--print("MyQuestAccepted no runthread")
	end
end

MessageboxTypes.MskTask = {
  title = "小黑手",
  
  has_button1 = true,
  has_button2 = true,
  button1_text = "确定",
  button2_text = "取消",
  Create = function(self)
	if g_global.pause == false then
		self.text = "小黑手:是否暂停脚本?"
	else
	    self.text = "小黑手:是否运行脚本?"
	end
    return self
  end,
  OnAccept = function(self)
	if gUI_CurrentStage ~= 3 then return end
	g_global.pause = not g_global.pause
	do return end
    if g_RunTask then
		g_RunTask = false
		mc_tools.CharExit("stop","ov")
	else
		--msk.dofile"gamestart"
		--msk.require"task"
		mc_tools.SetTimer(0.2,"GameControl",mc_tools.GameControl,2)
		g_player:init()
		g_player.updatechara()
		local ret,err = pcall(BeginTaskLoop)
		if not ret then 
			print("BeginTask err:"..err)
		else
			g_RunTask = true
		end
	end
  end
}

function MyKeyDown(msg)
  if msg:get_wparam() == 199 then
	--g_player.init()
	--g_player.updatechara("MyKeyDown")
	Messagebox_Show("MskTask")
  end
end

function DealFriendApply(guid)
	local name = GetPlayerInfoByGUID(guid)
	if g_global.acceptfrient or g_global.allowfriends[guid] or g_global.teammemers[name] then
		AcceptOrRefuseApply(true, guid)
	else
		AcceptOrRefuseApply(false, guid)
	end
	DeleteHint(89)
	return true
end

function Msk_OnCarnivalUI_ShowPoint(show, current_lv, max_lv, obtain_Exp, obtain_Vig, gain_Exp, gain_Vig)
	if show then
		if current_lv == max_lv then
			ChooseFloor(1)
			return true
		end
	end
end

function MyEnterCombat(into,ext)
	if into then
		print("已经进入战斗状态")
		g_global.isfighting = true
	else
		g_global.isfighting = false
		g_global.beattacked = false
		if g_global.itemupdate == true then
			g_global.itemupdate = false
			mc_tools.SetListCallBack(nil,"IDL",ItemDeal)
			mc_tools.SetListCallBack(nil,"LPS",LearProSkill)
		end
		print("已经脱离战斗状态")
	end
	return true
end

function MySkillLernOk(skillId, lv)
    if lv == 1 then
		local wnd = WindowToContainer(WindowSys_Instance:GetWindow("Skill.Skill_bg.Center_bg.Skill_cnt"))
		wnd:SizeSelf()
		local item = wnd:GetChildByData0(skillId)
		if item then
			local goodsBoxNew = WindowToGoodsBox(item:GetChildByName("Skill_gbox"))
			local speeds = 0.01
			local pic = goodsBoxNew:GetItemIcon(0)
			local effect = "efxc_ui_hint02"
			local sourceX = goodsBoxNew:GetItemRect(0):get_left()
			local sourceY = goodsBoxNew:GetItemRect(0):get_top()
			ActBar_SetSourceData(sourceX, sourceY, speeds, pic, effect, skillId)
		end
    end
	return true
end

function Msk_Lua_Guide_HintOpen(msg)
end

function MskTriggerUserGuide(tp, a1, a2,guid,...)
	if tp == 59 and a1 == 1 and a2 == -1 then
		print("仙草精出发!!!!!!")
		g_global.homebron = true
		g_global.homebrontm = os.time()
		if GetQuestState(8100) == QUEST_STATUS_AVAILABLE and CanQuestAccepted(8100) then
			AcceptQuest(-1, 8100, 4,  0)
			local t = CreateTaskObj(8100)
			g_globalt.runListTask[t] = true
		end
		if g_global.teammode and not g_global.isteamleader then
			--SendCmd("xlz",g_player.guid,g_global.teamleader)
		end
		return true
	elseif tp == 57 and a1 == -1 and a2 == -1 and guid then
		print("被对方浇水了 ,还回去",a1,a2,guid)
		MskAskHomeInfo(0,tonumber(guid))
	else
		print("MskTriggerUserGuide Skip",tp, a1, a2,guid,...)
	end
end

function Msk_OnWorldGroup_SpellStart(dura, stype, other, desc, flag)
	print('Msk_OnWorldGroup_SpellStart',dura, stype, other, desc, flag)
	if flag == 2 then
		_lastSkillDec = desc
	end
	if other == 212 then
		print('reset sftm')
		sftm = os.time()
	end
	if flag == 0 then
		g_global.isbusy = true
	end
end

function Msk_OnParty_ReceiveApply(count, max_count)
	if g_global.testmode then return end
	_Party_Apply_Reject()
	return true
end

function Msk_OnParty_ReceiveInvite(count, max_count)
	if g_global.testmode then return end
	if count > 0 and g_global.teammode then
		local name, level, menpai, member_guid = GetTeamInvitationInfo(0)
		if g_global.teamleader == name then
			AcceptTeamInvitation(member_guid)
			msk.send("teamok#"..member_guid)
		else
			print("未知的组队邀请,拒绝",name)
			_Party_Invite_Reject()
			return true
		end
    elseif count > 0 then
		print("非组队模式 拒绝 组队")
		_Party_Invite_Reject()
		return true
	end
	DeleteHint(87)
	return true
end

function MskScript_Fri_OnEvent(event)
	if event == "COMMUNITY_ADD_RELATION" or event == "COMMUNITY_DEL_RELATION" then
		if arg1 == 1 and not g_global.stophookfriend then
			GetAllFriendList()
			mc_tools.GameEvent("friend_change")
		end
	end
end

function RunTaskAlone1_35()
	local idx,tid = 0,0
	local tnum = #task_1_35
	local rnum,t
	repeat
		idx = idx+1
		tid = task_1_35[idx]
		rnum = 0
		while true do
			rnum = rnum + 1
			if g_global.taskupdate == false or rnum > 10 then
				rnum = 0
				g_global.taskupdate = false
				UpdateCanAcceptTaskList(false)
				sleep(1)
			else
				t = CreateTaskObj(tid)
				t:Accept()
				if g_global.runListTask[t] or (false == CanQuestAccepted(tid) and GetQuestState(tid) == QUEST_STATUS_AVAILABLE) then
					break
				else
					sleep(1)
				end
			end
		end
	until idx >= tnum
	mc_tools.CharExit("成功")
end


function Trade()
	--获取交易对象 交易后需要剩下的金钱等信息 然后判断是否可以交易
	DealBag()
	local cfg = g_global.script_config--获取脚本配置
	print('--获取脚本配置',cfg)
	if tonumber(cfg['本机交易']) == 1 then
		g_global.script_config = mc_tools.GetData('script_config')
	end
	local trade_people = cfg['交易对象']
	local keep_money = tonumber(cfg['保留金钱']) or 0
	local bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
	local trade_money = nonBindemoney - keep_money
	local tradecoord = cfg['交易地点'] or '五方城|433|170'
	local map,x,y = tradecoord:match('(.+)|(%d+)|(%d+)')
	x,y = tonumber(x),tonumber(y)
	map = GetMapByName(map)
	map = map and map.MapID
	map = tonumber(map)
	if not trade_people or trade_people == '不交易' or #trade_people < 2 or trade_money <= 0 or not map or not PathTo(x,y,map) then 
		msk.warn('交易条件不具备 放弃',trade_people,trade_money,map)
		return false
	end
	g_global.notmove = true--不能移动

	--Hook相应函数准备接受交易
	mskco.SetFunctionHandler("_OnExchange_AcceptTrade",
			function (name) 
				if name == trade_people then
					SwapAcceptTradeRequest()
				else
					SwapRefuseTradeRequest()
				end
				return true
			end)
			
	--对方锁定后立刻触发通知
	mskco.SetFunctionHandler("_OnExchange_Lock",
			function (bMyLocked, bOtherLocked) 
				if bOtherLocked then
					mskco.HandleEvent('trade_lock')
				end
			end)
	mskco.SetEventProducer('_Exchange_CloseTrade','trade_close')
	
	--等待保护时间过去
	print('等待保护时间',GetRemainProtectTime())
	while GetRemainProtectTime() > 0 do
		ShowTaskState('等待保护时间')
		sleep(5)
	end
	
	--死循环 直到交易成功
	print('尝试 等待交易')
	g_global.trade_people = trade_people
	repeat
		while nil == GetCharByType(gUI_CharType.CHAR_PLAYER,true,trade_people) do
			ShowTaskState('找不到交易对象:'..trade_people)
			sleep(5)
		end
		ShowTaskState('交易对象:'..trade_people)
		--重新计算金钱 看是否成功交易
		bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
		trade_money = nonBindemoney - keep_money
		if trade_money <= 0 then break end
		_Exchange_CloseTrade()
		mc_tools.SendCmd('trade',nil,trade_people)
		sleep(5,'trade_lock')
		if gUI.Exchange.Root:IsVisible() then
			SwapSetTradeGold(trade_money)
			SwapLockTrade()
			sleep(1)
			SwapCommitTrade()
			sleep(3,'trade_close')
		end
	until false
	g_global.notmove = false--现在可以移动了
	QuitGuildOrganization(false)
	print 'trade over'
end

function RunScript_Test()
	CreateTaskObj(4502):Accept()
	DoXcjTask()
	return mc_tools.CharExit("成功")
end

function Run_LevelUp(lv,nteam,trade,target)
	if g_player.lv >= lv then 
		if trade then 
			GetGoalProcess()
			Trade()
			return mc_tools.CharExit("成功") 
		end
		return
	end
	local at = g_global.accountdata
	target = target or 'tsk'
	nteam = ParseTeam(target,nteam)
	local idx,tid = 0,0
	local tnum = #task_1_35
	local rnum,t
	repeat
		if g_player.lv >= lv then
			mc_tools.Send('tstep','tm35task_9999')
			break
		end
		idx = mc_tools.SendAndWait('tstep','tm35task_'..idx)
		idx = tonumber(idx)+1
		tid = task_1_35[idx]
		rnum = 0
		while true do
			rnum = rnum + 1
			if g_global.taskupdate == false or rnum > 10 then
				rnum = 0
				g_global.taskupdate = false
				UpdateCanAcceptTaskList(false)
				sleep(1)
			else
				t = CreateTaskObj(tid)
				t:Accept()
				if g_global.runListTask[t] or (false == CanQuestAccepted(tid) and GetQuestState(tid) == QUEST_STATUS_AVAILABLE) then
					break
				else
					sleep(1)
				end
			end
		end
	until idx >= tnum
	if g_player.lv < lv then
		DaysTask()
	end
	if trade then 
		GetGoalProcess()
		Trade()
		return mc_tools.CharExit("成功")
	end
	return
end

function RunTask1_35()
	--msk.require("task_1_35")
	if g_player.lv >= 35 then
		return mc_tools.CharExit("成功")
	end
	local idx,tid = 0,0
	local tnum = #task_1_35
	local rnum,t
	local at = g_global.accountdata
	repeat
		if g_player.lv >= 35 then
			mc_tools.Send('tstep','tm35task_9999')
			break
		end
		idx = mc_tools.SendAndWait('tstep','tm35task_'..idx)
		idx = tonumber(idx)+1
		tid = task_1_35[idx]
		rnum = 0
		while true do
			rnum = rnum + 1
			if g_global.taskupdate == false or rnum > 10 then
				rnum = 0
				g_global.taskupdate = false
				UpdateCanAcceptTaskList(false)
				sleep(1)
			else
				t = CreateTaskObj(tid)
				t:Accept()
				if g_global.runListTask[t] or (false == CanQuestAccepted(tid) and GetQuestState(tid) == QUEST_STATUS_AVAILABLE) then
					break
				else
					sleep(1)
				end
			end
		end
	until idx >= tnum
	if g_player.lv < 35 then
		DaysTask()
	end
	mc_tools.CharExit("成功")
end

function RunTaskStep(taskid)
	if g_global.taskupdate == false then
		print('try updata task')
		UpdateCanAcceptTaskList(false)
		sleep(1)
		return RunTaskStep(taskid)
	end
	local status
	for k,v in pairs(g_global.runListTask) do
		if v then
			status = GetQuestState(k.id)
			if status == QUEST_STATUS_INCOMPLETE then
				k:Run()
			elseif status == QUEST_STATUS_COMPLETE then
				k:Submit()
			else
				g_global.runListTask[k] = nil
			end
		end
	end
	taskid = tonumber(taskid)
	if taskid == -1 then
		mc_tools.CharExit("成功")
	end
	status = GetQuestState(taskid)
	local t = CreateTaskObj(taskid,nil)
	while status ~= QUEST_STATUS_AVAILABLE or CanQuestAccepted(taskid) do
		if status == QUEST_STATUS_AVAILABLE then
			if not t:Accept() then sleep(1) end
		elseif status == QUEST_STATUS_FAILED then
			if not t:Restart()  then sleep(1) end
			--msk.pcall(CTaskObj.Restart,t)
		elseif status == QUEST_STATUS_INCOMPLETE then
			if not not t:Run()  then sleep(1) end
			--msk.pcall(CTaskObj.Run,t)
		elseif status == QUEST_STATUS_COMPLETE then
			if not t:Submit() then sleep(1) end
			--msk.pcall(CTaskObj.Submit,t)
		else
			print("未知的任务状态",taskid,status)
			sleep(1)
		end
		if g_global.runListTask[t] then
			break
		end
		status = GetQuestState(taskid)
	end
	msk.send("stepok#"..taskid)
end

function AddFriends(t)
	print("AddFriends")
	for i=1,10 do
		if #t < 1 then break end
		GetAllFriendList()
		--再根据名字排除
		for i=#t,1,-1 do
			for fguid,ft in pairs(g_global.friends) do
				if t[i][1] == g_player.guid or t[i][2] == g_player.name then--排除自己
					table.remove(t,i)
					break
				end
				if fguid == t[i][1] then--排除ID
					table.remove(t,i)
					break
				end
				if ft.name == t[i][2] then--排除名字
					table.remove(t,i)
					break
				end
				if GetFriendPointDetail(t[i][1]) then
					table.remove(t,i)
					break
				end
				print('好友不匹配:',ft.name,t[i][2],#ft.name,#t[i][2])
			end
		end
		for i=1,#t do
			print("trye add",t[i][2])
			AddToFriend(t[i][2])
			sleep(0.5)
		end
		UI_Fri_ShowMainUI()
		sleep(2)
	end
	msk.send("friendok#firend")
end

function InvitePlayerTeam(data)
	g_global.teammode = true
	local tmnum,leadername = GetTeamInfo()
	if g_player.hasteam and not g_player.isleader then
		print("不是自己的队伍")
		QuitTeam()--非指定队伍 退出
		sleep(1)
	end
	local name
	data,name = data:match("(.+),(.+)")
	local tguid = tonumber(data)
	if IsPlayerInTeam(tguid,name) == false then
		InviteJoinTeamByGUID("??",tguid)
	else
		msk.send("teamok#ov")
		--msk.send("inteam#"..data)
	end
end

function TeamLeaderGet(data)
	g_global.teammode = true
	g_global.teamleader = data
	local tmnum,leadername = GetTeamInfo()
	if tmnum > 0 and leadername ~= g_global.teamleader then
		print("不是指定的队伍",leadername,g_player.name)
		QuitTeam()--非指定队伍 退出
		sleep(1)
	end
	msk.send("leaderok#"..data)
end
--[[
				1751 错误
				采集没有等别人
				任务完成后没有再接
--]]
function RunXcjStep(data)
	local idx,guid,name = data:match("(%d+)|(.-)|(.+)")
	idx = tonumber(idx)
	guid = tonumber(guid)
	g_global.farmguid = guid
	print("RunXcjStep",idx,guid,name)
	if GetQuestState(8100) == QUEST_STATUS_AVAILABLE then
		if g_global.taskupdate == false then
			UpdateCanAcceptTaskList(false)
		end
		if CanQuestAccepted(8100) then
			AcceptQuest(-1,  8100,  4,  0)
			sleep(1)
		end
	end
	if idx == 1 then
		print("仙草精第1步1 加好友")
		while guid ~= g_player.guid  do
			GetAllFriendList()
			if g_global.friends[guid] then
				MskAskHomeInfo(0,guid)
				break
			end
			AddToFriend(name)
			sleep(2)
		end
		print("仙草精第1步2 好友等级升级到2")
		local tpl,bag,sloat,friLevel
		while guid ~= g_player.guid do
			friLevel = GetFriendPointDetail(guid)
			if friLevel >=2 then
				break
			else
				SendCmd("jh","370,250,50",name)
				PathTo(370,250,50)
				sleep(2)
			end
			tpl = GetCharByType(gUI_CharType.CHAR_PLAYER,true,name)
			if tpl then
				print("已经找到",tpl.name)
				tpl:Select()
				slot,bag = GetItemByName("极品仙桃")
				if slot then
					UseItem(bag,slot)
				end
			end
			sleep(2)
		end
	elseif idx == 2 then
		if GetMPlayerMapId() == 140 then
			InstanceExit()
			msk.sleep(2)
			msk.sleep(2)
		end
		print("仙草精第2步 移动到指定庄园位置")
		if guid ~= g_player.guid then
			mc_tools.SendCmd("eh",nil,name)
			sleep(3)
		end
		while not PathTo(175,120,140) do
			if guid ~= g_player.guid then
				mc_tools.SendCmd("eh",nil,name)
			end
			sleep(3)
		end
	elseif idx == 3 then
		if guid == g_player.guid then
			local ch
			for i=1,10 do
				if g_global.homebron == true and (os.time()-g_global.homebrontm) < (2*60*60-120) then--1小时58分钟
					break
				end
				ch = GetCharByName("仙草精",false,false,true)
				if ch then
					print("get the char !!",ch.name)
					break
				end
				DealSelfCasher(true)
			end
		end
	elseif idx == 4 then
		print("仙草精第4步 准备战斗")
		local t = CreateTaskObj(8100)
		local rnum = 0
		local tstatus = GetQuestState(8100)
		while true do
			CollectObj(nil,175, 120, 140,120)
			FightMonster("仙草精",175, 120, 140,120)
			--FightMonster("枫叶精",75, 156, 140,120)
			if GetQuestState(8100) == QUEST_STATUS_COMPLETE then
				g_global.taskupdate = false
				t:Submit()
				break
			elseif GetQuestState(8100) == QUEST_STATUS_AVAILABLE then
				if g_global.taskupdate == false then
					UpdateCanAcceptTaskList(false)
				end
				if CanQuestAccepted(8100) then
					t:Accept()
				else
					break
				end
			end
			if rnum >= 40 then--2分钟
				break
			end
			sleep(3)
			rnum = rnum + 1
		end
		if guid == g_player.guid then
			g_global.homebron = false
		end
	elseif idx == 5 then
		print("仙草精第5步 操作结束")
		g_global.farmguid = g_player.guid
		g_global.specia = g_global.savedspecia or "normal"
		local at = g_global.accountdata
		g_global.teammode = at.teamnun > 1
		return
	end
	if idx == 4 then
		InstanceExit()
		msk.sleep(5)
	end
	print("RunXcjStep over",idx)
	msk.send("stepok#"..idx)
end

--可以副本复活
--白羽之巅
function RunByStep(data)
	local idx = tonumber(data)
	if idx == 0 then
		print("白羽之巅第一步 接任务")
		while not PathTo(515,5,-1) do
			sleep(3)
		end
		local npcid = GetNPCObjId(515)
		for taskid= 3001,3014 do
			if CanQuestAccepted(tid) then
				AcceptQuest(npcid, taskid, DIALOG_STATUS_AVAILABLE, 0)
				sleep(2,"utask")
			end
		end
	elseif idx == 1 then
		if GetMPlayerMapId() ~= 101 then
			
		end
	else
		msk.warn("未知的操作步骤 白羽之巅")
	end
	print("RunByStep over",idx)
	msk.send("stepok#"..idx)
end

function DoGameTest(data)
	repeat
		local f,err = loadfile("G:/bitbuket/tzj/script_tg/"..data..".lua")
		if not f then 
			print("dofile err:"..err) 
		else
			f()
		end
		data = mskco.Wait('file')
	until false
end

function Handler_AccountData(data)
	if g_global.testlv < 1 then return end
	if gUI_CurrentStage ~= 1 then return end
	local at =  g_globalt.accountdata
	at.user,at.pwd,at.area,at.server,at.charidx,g_global.specia,at.script = data:match("(%w+),(%w+),(%d+),(%d+),(%d+),(%w+),(.+)")
	at.area = tonumber(at.area)
	at.server = tonumber(at.server)
	at.charidx = tonumber(at.charidx)
	print("get accountdata",at.user,at.pwd,at.area,at.server)
	local logger = logging.file(g_global.appdir..'日志/'..at.user.."_%s.txt", "%Y-%m-%d")
	logger:setLevel (logging.INFO)
	g_global.logger = logger
	g_global.script_config = mc_tools.GetData('script_config')
	print('get ok -> now add timer')
	SelectServer()
	--AddTimerEvent(1,"SelectServer",SelectServer)
	print('add ok ->')
end

function ConsoleData(data)
	--do return end
	if #data < 2 then return end
	local bg,ed = data:find("&")
	if bg then
		local first,second = data:sub(1,bg-1),data:sub(ed+1,-1)
		ConsoleData(first)
		return ConsoleData(second)
	end
	print("ConsoleData",data)
	local cmd,data = data:match("(%w+)#(.+)")
	if mskco.HandleEvent(cmd,data) then return end
	if cmd == 'ttstep' then
		cmd = g_global.tttar
	end
	if cmd == "at" then
	elseif cmd == 'printlv' then
		pt_lv_cur = tonumber(data)
	elseif cmd == "exit" then
		g_global.logger:warn("控制台要求结束!!")
		Exit()
	--开始组队信息
	elseif cmd == "rstep" then
		mc_tools.SetListCallBack(nil,cmd,RunStepFun,data)
	elseif cmd == "rbystep" then
		mc_tools.SetListCallBack(nil,cmd,RunByStep,data)	
	elseif cmd == "ifriend" then
		local needaddfirend = {}
		local ntguid,name
		for tguid,name in data:gmatch("(.-)|(.-),") do
			ntguid = tonumber(tguid)
			if ntguid ~= g_player.guid and name ~= g_player.name then
				print("add friend",tguid,name)
				needaddfirend[#needaddfirend+1] = {ntguid,name}
			else
				print("skip friend",tguid,name)
			end
		end
		if #needaddfirend > 0 then
			mc_tools.SetListCallBack(nil,cmd,AddFriends,needaddfirend)
		else
			msk.send("friendok#firend")
		end
	elseif cmd == "ifriend2" then
		local ntguid,name
		GetAllFriendList()
		for tguid,name in data:gmatch("(.+)|(.+)") do
			ntguid = tonumber(tguid)
			if ntguid ~= g_player.guid and name ~= g_player.name then
				print("add ifriend2",tguid,name)
				if g_global.friends[ntguid] == nil then
					AddToFriend(name)
				end
			else
				print("skip ifriend2",tguid,name)
			end
		end
	elseif cmd == "tleader" then
		mc_tools.SetListCallBack(nil,cmd,TeamLeaderGet,data)
	elseif cmd == "iteam" then
		mc_tools.SetListCallBack(nil,cmd,InvitePlayerTeam,data)
	elseif cmd == "teamdata" then
		g_global.teammaxnum,g_global.teamleader = data:match("(%d+),(.+)")
		g_global.curteamleader = g_global.teamleader
		g_global.teammaxnum = tonumber(g_global.teammaxnum)
		if nil ~= g_global.runthread and g_global.online  then
			local ret,err = coroutine.resume(g_global.runthread,0,"team")
			if not ret then 
				msk.warn("teamdata resume error",err) 
				g_global.runthread = nil
			end
		end
	elseif cmd == "file" then
		local f,err = loadfile("G:/bitbuket/tzj/script_tg/"..data..".lua")
		if not f then 
			print("dofile err:"..err) 
		else
			f()
		end
	elseif cmd == "string" then
	  local f,err = loadstring(data)
	  if not f then 
		print("dostring err:"..err) 
	  else
		f()
	  end
	elseif cmd == "rxcjstep" then
		mc_tools.SetListCallBack(nil,cmd,RunXcjStep,data)
	else
		print("unknow console data",cmd,data)
	end
end

function WorldGroup_OnPlayHitSoundWithFileName(str, weaponType, armorType)
  if str == "PLAYER_ON_ATTACKING" or str == "PLAYER_BE_ATTACKED" then
    local strSound = gUI_WEAPON_ARMOR_SOUND_MAP[weaponType][armorType]
    WorldStage:playSoundByMainPlayer(strSound)
  end
end

local upLastBag,upLastSlot = -1,-1
function Msk_OnAutoChangeEquip_ItemDelete(bag, slot, isAdd)
	if isAdd == nil then return end
	if upLastBag == bag and upLastSlot == slot then 
		upLastBag,upLastSlot = -1,-1
		return 
	end
	upLastBag,upLastSlot = bag,slot
	print("Msk_OnAutoChangeEquip_ItemDelete")
	local psex = gUI_MainPlayerAttr.Sex
	local pljob = gUI_MainPlayerAttr.MenPai
	local name, _, _, minLevel, job, sex, _, _, canSell,canDiscard, _, _, _, itemType, _, _, _, _, sellPrice, _, equipKind= GetItemInfoBySlot(bag, slot)
	--print(name,minLevel, job, sex,canSell,itemType,sellPrice,equipKind)
	if false ==  g_global.isfighting and itemType == 2 and (job == -1 or checkJobKindIsCanUse(pljob, job)) and (sex == -1 or sex == psex ) then--装备
		if minLevel <= g_player.lv then
			local curscore = GetEquipScore(bag, slot)
			local eidx = GetEquipTypeIndex(equipKind)
			local tarscore = GetEquipScore(gUI_GOODSBOX_BAG.myequipbox,eidx)
			tarscore = tarscore or 0
			if curscore > tarscore then--装备分数更高的装备
				print("equip:",name,eidx,curscore,tarscore)
				MoveItem(bag, slot, gUI_GOODSBOX_BAG.myequipbox, eidx)
			elseif false and canDiscard == 1 then--或者丢弃
				print("drop:"..name)
				DropItem(bag,slot)
			else
				print("skip equip",itemType,equipKind,name,minLevel, job, sex,canSell,sellPrice, canDiscard,curscore,tarscore)
			end
		else
			print("skil equip because lv",name,minLevel)
		end
	end
	return true
end

function Msk_OnChat_AddFightLog(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
	if arg2 and arg1 == "COMBAT_BE_KILL" then
		local mp = GetMapById(GetMPlayerMapId())
		g_global.logger:warn('死亡 攻击者:%s 地点:%s %.2f,%.2f',arg2,mp.MapName,g_player.x,g_player.y)
		g_global.numDead = g_global.numDead + 1
		mc_tools.SetData('numDead',g_global.numDead)
	end
end

function CompleteGoal(goalID)
	local goalList = GetGoalList()
	local _item
	for i = 1, table.maxn(goalList) do
		_item = goalList[i]
		if _item.complated then
			local _name = GetGoalInfo(goalID)
			print("完成目标:",_name)
			GoalSubmit(_item.id)
			sleep(0.5)
		end
	end
end

function Msk_OnGoalM_ComplateGoal(goalID)
	local _name = GetGoalInfo(goalID)
	print("完成目标:",_name)
	GoalSubmit(goalID)
	return true
end

local g_lastfname = ""
function FileLoadCallBack(fname)
	--print("load file:"..fname)
	if fname == 'reload' or g_lastfname == "CharList.lua" then
		mc_tools.HookFunction("_OnCharList_OnOpen", EnterGame)
	elseif fname == 'reload' or g_lastfname == "GoalMain.lua" then
		mc_tools.HookFunction("_OnGoalM_ComplateGoal",Msk_OnGoalM_ComplateGoal,true)
	elseif fname == 'reload' or g_lastfname == "PartyStats.lua" then
		mc_tools.HookFunction("_OnParty_ReceiveInvite",Msk_OnParty_ReceiveInvite,true)
		mc_tools.HookFunction("_OnParty_ReceiveApply",Msk_OnParty_ReceiveApply)
		mc_tools.HookFunction("Script_Party_OnEvent",Msk_Script_Party_OnEvent,true)
		mc_tools.HookFunction("_OnParty_MemberInfoUpdate",Msk_OnParty_MemberInfoUpdate,true)
	elseif fname == 'reload' or g_lastfname == "HomeLand_Cash.lua" then
		mc_tools.HookFunction("_HomeLandCash_Update", Msk_HomeLandCash_Update,true)
	elseif fname == 'reload' or g_lastfname == "Guide.lua" then
		--mc_tools.HookFunction("Lua_Guide_HintOpen", Msk_Lua_Guide_HintOpen)
	elseif fname == 'reload' or g_lastfname == "PlayerStats.lua" then
		mc_tools.HookFunction("_OnPlays_RouterStateChange", RouterStateChange)
		mc_tools.HookFunction("_Plays_LevelUp",LevelUpCallBack)
		mskco.SetFunctionHandler("_OnPlays_SpellStart",Msk_OnPlays_SpellStart)
		mskco.SetFunctionHandler("_OnPlays_SpellOver",Msk_OnPlays_SpellOver)
		--mc_tools.HookFunction("_OnPlays_SpellStart",Msk_OnPlays_SpellStart)
		--mc_tools.HookFunction("_OnPlays_SpellOver",Msk_OnPlays_SpellOver)
	elseif fname == 'reload' or g_lastfname == "SafeSetup.lua" then
		mc_tools.HookFunction("_OnSafeSetup_ProtectStart",MapChange)
	elseif fname == 'reload' or g_lastfname == "WorldGroup.lua" then
		mc_tools.HookFunction("_OnWorldGroup_StartTimer",WorldEnter)
		mc_tools.HookFunction("WorldGroup_KeyDown",MyKeyDown)
		mc_tools.HookFunction("_OnWorldGroup_SpellOver",MySkillOver)
		mc_tools.HookFunction("_OnWorldGroup_SpellStart",Msk_OnWorldGroup_SpellStart)
		WorldGroup_OnPlayHitSoundWithFileName = function (str) 
			if str == "PLAYER_BE_ATTACKED" then
				g_global.beattacked = true
			end
		end
		--mc_tools.HookFunction("_OnWorldGroup_SpellDelay",MySkillDelay)
	elseif fname == 'reload' or g_lastfname == "Quest.lua" then
		mc_tools.HookFunction("_QB_DoneMsg",MyQuestSubmit)
		mc_tools.HookFunction("_OnQB_PlayerAcceptNewQuest",MyQuestAccepted)
		--拦截任务进度
		mc_tools.HookFunction("_OnQB_TrackAddItem",FilterTrackAddItem)
		--拦截可接任务
		mc_tools.HookFunction("Script_QB_OnEvent",FilterQuestEvent)
		--mc_tools.HookFunction("Script_QB_OnEvent",ObAEvent)
	elseif fname == 'reload' or g_lastfname == "ReliveFrame.lua" then
		mc_tools.HookFunction("_OnRelive_Show",QuickRevive)
	elseif fname == 'reload' or g_lastfname == "AutoChangeEquip.lua" then
		mc_tools.HookFunction("UI_AutoChangeAutoOpen",AutoEquip,true)
		--mc_tools.HookFunction("_OnAutoChangeEquip_ItemDelete",Msk_OnAutoChangeEquip_ItemDelete,true)
	elseif fname == 'reload' or g_lastfname == "ActionBar.lua" then
		mc_tools.HookFunction("_OnActBr_CoolDownNormal",SkillCoolDown)
		mc_tools.HookFunction("_OnActBr_CoolDownVehicle",SkillCoolDown)
	elseif fname == 'reload' or g_lastfname == "Friends.lua" then
		mc_tools.HookFunction("_OnFri_AskAcceptApply",DealFriendApply,true)
		mc_tools.HookFunction("Script_Fri_OnEvent",MskScript_Fri_OnEvent)
		mskg.AskHomeInfo = AskHomeInfo
		--AskHomeInfo = MskAskHomeInfo
	elseif fname == 'reload' or g_lastfname == "CarnivalUI.lua" then
		mc_tools.HookFunction("_OnCarnivalUI_ShowPoint",Msk_OnCarnivalUI_ShowPoint,true)	
		mc_tools.HookFunction("_OnCarnivalUI_ShowOsd",function(lv) g_global.triallv = lv end)	
	elseif fname == 'reload' or g_lastfname == "ChatDialog.lua" then
		mc_tools.HookFunction("_OnChat_EnterCombat",MyEnterCombat,true)
		mc_tools.HookFunction("_OnChat_RecvChat",Msk_OnChat_RecvChat,true)
		mc_tools.HookFunction("_OnChat_AddFightLog",Msk_OnChat_AddFightLog)
	elseif fname == 'reload' or g_lastfname == "SkillGift.lua" then
		mc_tools.HookFunction("_OnGift_SpellEnhanceResult",MySkillLernOk,true)
	elseif fname == 'reload' or g_lastfname == "Exchange.lua" then
	elseif fname == 'reload' or g_lastfname == "PickUp.lua" then
		mskco.SetEventProducer('Lua_Pick_OnGetNewItem','item_picked')
	end
	g_lastfname = fname
end

local msk_event = {
	['您没有拾取这个道具的权限'] = 'item_picked',
	['离掉落包裹距离过远'] = 'item_picked',
	['你退出帮会不足24小时，无法申请加入帮会'] = 'guild_join',
}
function ErrorMsgHandler(msg)
	local event = msk_event[msg]
	return event and mskco.HandleEvent(event) and false
end

function TestTimer()
	print("我是测试时钟")
end
--curthread = coroutine.create(printsid)
--AddTimerEvent(1,"TestSleep",TestSleep)

if false and msk.ver_test and gUI_CurrentStage == 3 then--测试用
	--mc_tools.UnHookFunction()
	--mc_tools.HookFunction("Script_QB_OnEvent",ObAEvent)
	mc_tools.HookFunction("_OnPlays_RouterStateChange", RouterStateChange,true)
	mc_tools.HookFunction("_Plays_LevelUp",LevelUpCallBack)
	mc_tools.HookFunction("_OnSafeSetup_ProtectStart",MapChange)
	mc_tools.HookFunction("_OnWorldGroup_StartTimer",WorldEnter)
	mc_tools.HookFunction("_OnRelive_Show",QuickRevive)
	mc_tools.HookFunction("UI_AutoChangeAutoOpen",AutoEquip,true)
	mc_tools.HookFunction("_OnActBr_CoolDownNormal",SkillCoolDown)
	mc_tools.HookFunction("_OnActBr_CoolDownVehicle",SkillCoolDown)
	mc_tools.HookFunction("_OnActBr_CoolDownVehicle",_OnActBr_CoolDown)
end

GetMousePos = function () return 0.5,0.5 end
mc_tools.HookFunction("Messagebox_Show",MyMessagebox_Show,true)
mc_tools.HookFunction("TriggerUserGuide",MskTriggerUserGuide,true)
mc_tools.HookFunction("_OnLogin_InitLastSelection",SelectServerFlag)
mc_tools.SetTimer(0.2,"GameControl",mc_tools.GameControl,2)
mskco.SetEventHandler('file',DoGameTest)
mskco.SetEventHandler('at',Handler_AccountData)
mskco.SetFunctionHandler('ShowErrorMessage',ErrorMsgHandler)



--debug.sethook(ProHook,"c")
