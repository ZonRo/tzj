
// server_tgDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "server_tg.h"
#include "server_tgDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Cserver_tgDlg 对话框



Cserver_tgDlg::Cserver_tgDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Cserver_tgDlg::IDD, pParent)
	, m_iPrepareCardNum(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Cserver_tgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PREPARECARDNNUM, m_iPrepareCardNum);
	DDV_MinMaxInt(pDX, m_iPrepareCardNum, 0, 10000);
	DDX_Control(pDX, IDC_LIST_LOG, m_lstLog);
	DDX_Control(pDX, IDC_PROGRESS1, m_process);
}

BEGIN_MESSAGE_MAP(Cserver_tgDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_PREPARECARD, &Cserver_tgDlg::OnBnClickedPreparecard)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR, &Cserver_tgDlg::OnBnClickedButtonClear)
	ON_BN_CLICKED(IDC_BUTTON_COPY, &Cserver_tgDlg::OnBnClickedButtonCopy)
END_MESSAGE_MAP()


// Cserver_tgDlg 消息处理程序

BOOL Cserver_tgDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO:  在此添加额外的初始化代码
	m_lstLog.InsertColumn(0, _T("卡号"),0,300);
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void Cserver_tgDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void Cserver_tgDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR Cserver_tgDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


#include <sstream>
void Cserver_tgDlg::OnBnClickedPreparecard()
{
	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);
	SQLiteWrapper::ResultTable res;
	theApp.m_sqlite.SelectStmt("select max(id) from cards", res);
	stringstream ss;
	ss << res.next()->fields_[0];
	int indx = 100;
	ss >> indx;
	if (m_iPrepareCardNum < 1) return;
	m_process.SetRange(0, m_iPrepareCardNum);
	m_process.SetStep(1);
	m_process.ShowWindow(TRUE);
	USES_CONVERSION;
	SQLiteStatement* stmt = theApp.m_sqlite.Statement(R"(insert into cards(number,createtime) values(?,datetime('now','+8 hour')))");
	for (int i = indx + 1; i < indx + 1 + m_iPrepareCardNum; ++i)
	{
		stringstream ss1;
		ss1 << "tg_" << i << theTools.GetRandomString(15);
		stmt->Bind(0, ss1.str());
		theTools.Assert(stmt->Execute(), "create card failed");
		m_process.StepIt();
		int itm = m_lstLog.InsertItem(m_lstLog.GetItemCount(), _T(""));
		m_lstLog.SetItemText(itm,0, A2W(ss1.str().c_str()));
	}
	m_process.ShowWindow(FALSE);
	
}


void Cserver_tgDlg::OnBnClickedButtonClear()
{
	// TODO:  在此添加控件通知处理程序代码
	m_lstLog.DeleteAllItems();
}


void Cserver_tgDlg::OnBnClickedButtonCopy()
{
	// TODO:  在此添加控件通知处理程序代码
	if (m_lstLog.GetSelectedCount() < 1) return;
	auto pos = m_lstLog.GetFirstSelectedItemPosition();
	int selit = m_lstLog.GetNextSelectedItem(pos);
	CString source;
	while (selit != -1){
		source += m_lstLog.GetItemText(selit, 0);
		source += _T("\n");
		selit = m_lstLog.GetNextSelectedItem(pos);
	}
	CStringA sourceA(source);
	if (OpenClipboard())
	{
		HGLOBAL   clipbuffer;
		char   *   buffer;
		EmptyClipboard();
		clipbuffer = GlobalAlloc(GMEM_MOVEABLE, sourceA.GetLength() + 1);
		buffer = (char*)GlobalLock(clipbuffer);
		strcpy(buffer, LPCSTR(sourceA));
		GlobalUnlock(clipbuffer);
		SetClipboardData(CF_TEXT, clipbuffer);
		CloseClipboard();
	}
}


BOOL Cserver_tgDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO:  在此添加专用代码和/或调用基类
	if (pMsg->hwnd == m_lstLog.GetSafeHwnd() && pMsg->message == WM_KEYDOWN && GetAsyncKeyState(VK_CONTROL) && pMsg->wParam == 0x43){
		OnBnClickedButtonCopy();
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}
