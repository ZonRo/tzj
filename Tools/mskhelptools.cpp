#include "stdafx.h"
#include "MskHelpTools.h"
#include <Nb30.h>
#pragma comment(lib, "Iphlpapi.lib")
#include <random>
#include <fstream>
#include <time.h>

DWORD _CreateFileW = (DWORD)GetProcAddress(GetModuleHandleA("Kernel32.dll"), "CreateFileW") + 5;
HANDLE
WINAPI
MyCreateFileW(LPCWSTR lpFileName, DWORD dwDesiredAccess, DWORD dwShareMode, LPSECURITY_ATTRIBUTES lpSecurityAttributes, DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes, HANDLE hTemplateFile)
{
	__asm jmp _CreateFileW
}
CTools::CTools() :m_strDataFile("mskdata:")
{
}


CTools::~CTools()
{
}


_ptr CTools::GetData(string strFileName)
{
	strFileName = m_strDataFile + strFileName;
	USES_CONVERSION;
	HANDLE hOpenFile = fCreateFileW(A2W(strFileName.c_str()), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == hOpenFile) {
		DWORD lerr = GetLastError();
		return nullptr;
	}
	DWORD RSize;
	DWORD fileSize = 0;
	fileSize = fGetFileSize(hOpenFile, NULL);
	byte* pBuffer = new byte[fileSize];
	fReadFile(hOpenFile, pBuffer, fileSize, &RSize, NULL);
	fCloseHandle(hOpenFile);
	return _ptr(pBuffer, RSize,true);
}

void CTools::SetData(string strFileName, _ptr pt)
{
	strFileName = m_strDataFile + strFileName;
	USES_CONVERSION;
	HANDLE hOpenFile = fCreateFileW(A2W(strFileName.c_str()), GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD dWrite;
	fWriteFile(hOpenFile, pt.GetPtr(), pt.GetSize(), &dWrite, NULL);
	fCloseHandle(hOpenFile);
	return;
}


bool CTools::Inject(unsigned long pid, string dllname, int noelevateprivs, int freedll, int sysdir)
{
	unsigned long len;
	void *remotelib;
	HANDLE hToken, hProcess, hThread;
	HMODULE kernel32lib;
	LPTHREAD_START_ROUTINE procaddr;
	LUID luid;
	TOKEN_PRIVILEGES tp, oldtp;
	if (!noelevateprivs) {
		if (!OpenProcessToken(GetCurrentProcess(),
			TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)){
			PRT("OpenProcessToken ERROR");
			return FALSE;
		}
		PRT("process token opened, handle 0x%x", hToken);

		if (!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luid)){
			PRT("LookupPrivilegeValue ERROR");
			return FALSE;
		}
		PRT("debug luid: %08x:%08x", luid.HighPart, luid.LowPart);

		len = sizeof(TOKEN_PRIVILEGES);

		tp.PrivilegeCount = 1;
		tp.Privileges->Luid = luid;
		tp.Privileges->Attributes = 0;
		if (!AdjustTokenPrivileges(hToken, 0, &tp,
			sizeof(TOKEN_PRIVILEGES), &oldtp, &len)){
			PRT("AdjustTokenPrivileges ERROR");
			return FALSE;
		}

		oldtp.PrivilegeCount = 1;
		oldtp.Privileges->Luid = luid;
		oldtp.Privileges->Attributes |= SE_PRIVILEGE_ENABLED;
		if (!AdjustTokenPrivileges(hToken, 0, &oldtp,
			sizeof(TOKEN_PRIVILEGES), NULL, NULL)){
			PRT("re AdjustTokenPrivileges ERROR");
			return FALSE;
		}
		PRT("elevated process privileges sucessfully!");
	}
	hProcess = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION |
		PROCESS_VM_OPERATION | PROCESS_VM_WRITE |
		PROCESS_VM_READ, 0, pid);
	if (!hProcess){
		PRT("OpenProcess EROOR");
		return FALSE;
	}

	remotelib = VirtualAllocEx(hProcess, NULL, len, MEM_COMMIT, PAGE_READWRITE);
	if (!remotelib){
		PRT("VirtualAllocEx EROOR");
		return FALSE;
	}
	WriteProcessMemory(hProcess, remotelib, dllname.c_str(), len, NULL);

	kernel32lib = GetModuleHandleA("kernel32");

	procaddr = (LPTHREAD_START_ROUTINE)GetProcAddress(kernel32lib,
		freedll ? "FreeLibraryA" : "LoadLibraryA");

	hThread = CreateRemoteThread(hProcess, NULL, 0,
		procaddr, remotelib, 0, NULL);
	if (hThread){
		WaitForSingleObject(hThread, INFINITE);
	}
	else{
		PRT("CreateRemoteThread Failed");
	}
	VirtualFreeEx(hProcess, remotelib, len, MEM_DECOMMIT);
	if (hThread)
		fCloseHandle(hThread);
	if (hProcess)
		fCloseHandle(hProcess);
	PRT("successfully %sjected %s into %d, thread handle 0x%08x.\n",
		freedll ? "de" : "in", dllname, pid, hThread);
	return true;
}

#define NET_CARD_KEY R"(SYSTEM\CurrentControlSet\Control\Class\{4D36E972-E325-11CE-BFC1-08002BE10318})"
#define  NCF_PHYSICAL 4
#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))
BOOL GetLocalAdapter(char *szDataBuff, DWORD dwDataLen = MAX_PATH)
{
	DWORD dwType = REG_SZ;
	HKEY hNetKey = NULL;
	HKEY hLocalNet = NULL;
	char keyBuff[10];
	if (ERROR_SUCCESS != RegOpenKeyExA(HKEY_LOCAL_MACHINE, NET_CARD_KEY, 0, KEY_READ, &hNetKey))
		return FALSE;
	SCOPE_EXIT(RegCloseKey(hNetKey););
	int idx = 0;
	sprintf(keyBuff, R"(%04d)", idx);
	DWORD dwValue; 
	while (ERROR_SUCCESS == RegOpenKeyExA(hNetKey, keyBuff, 0, KEY_READ, &hLocalNet)){
		if (ERROR_SUCCESS == RegQueryValueExA(hLocalNet, "Characteristics", 0, &dwType, (BYTE *)&dwValue, &dwDataLen)){
			SCOPE_EXIT(RegCloseKey(hLocalNet););
			if ((dwValue & NCF_PHYSICAL) == NCF_PHYSICAL){
				dwDataLen = MAX_PATH;
				dwType = REG_SZ;
				auto ret = RegQueryValueExA(hLocalNet, "NetCfgInstanceId", 0, &dwType, (BYTE *)szDataBuff, &dwDataLen);
				if (ERROR_SUCCESS == ret)
					return TRUE;
			}
		}
		idx++;
		sprintf(keyBuff, R"(%04d)", idx);
	}
	return FALSE;
}

BOOL GetMac1(char *mac)
{
	/* Declare and initialize variables */
	char szAdName[MAX_PATH + 1] = { 0 };
	if (GetLocalAdapter(szAdName) == FALSE)
		return FALSE;
	DWORD dwSize = 0;
	DWORD dwRetVal = 0;

	int i = 0;

	// Set the flags to pass to GetAdaptersAddresses
	ULONG flags = GAA_FLAG_INCLUDE_PREFIX;

	// default to unspecified address family (both)
	ULONG family = AF_UNSPEC;

	LPVOID lpMsgBuf = NULL;

	PIP_ADAPTER_ADDRESSES pAddresses = NULL;
	ULONG outBufLen = 0;

	PIP_ADAPTER_ADDRESSES pCurrAddresses = NULL;
	PIP_ADAPTER_ADDRESSES pGetAddresses = NULL;
	PIP_ADAPTER_UNICAST_ADDRESS pUnicast = NULL;
	PIP_ADAPTER_ANYCAST_ADDRESS pAnycast = NULL;
	PIP_ADAPTER_MULTICAST_ADDRESS pMulticast = NULL;
	IP_ADAPTER_DNS_SERVER_ADDRESS *pDnServer = NULL;
	IP_ADAPTER_PREFIX *pPrefix = NULL;

	family = AF_INET;

	outBufLen = sizeof (IP_ADAPTER_ADDRESSES);
	pAddresses = (IP_ADAPTER_ADDRESSES *)MALLOC(outBufLen);

	// Make an initial call to GetAdaptersAddresses to get the 
	// size needed into the outBufLen variable
	if (GetAdaptersAddresses(family, flags, NULL, pAddresses, &outBufLen)== ERROR_BUFFER_OVERFLOW) {
		FREE(pAddresses);
		pAddresses = (IP_ADAPTER_ADDRESSES *)MALLOC(outBufLen);
	}

	if (pAddresses == NULL) return FALSE;
	SCOPE_EXIT(FREE(pAddresses););
	dwRetVal = GetAdaptersAddresses(family, flags, NULL, pAddresses, &outBufLen);;
	if (dwRetVal == NO_ERROR) {
		// If successful, output some information from the data we received
		pCurrAddresses = pAddresses;
		while (pCurrAddresses) {
			if (strcmp(szAdName, pCurrAddresses->AdapterName) == 0){
				pGetAddresses = pCurrAddresses;
				break;
			}
			pCurrAddresses = pCurrAddresses->Next;
		}
	}
	if (NULL == pGetAddresses) return FALSE;
	sprintf(mac, "%02X-%02X-%02X-%02X-%02X-%02X",
		pGetAddresses->PhysicalAddress[0],
		pGetAddresses->PhysicalAddress[1],
		pGetAddresses->PhysicalAddress[2],
		pGetAddresses->PhysicalAddress[3],
		pGetAddresses->PhysicalAddress[4],
		pGetAddresses->PhysicalAddress[5]);
	return TRUE;
}

int CTools::GetMac(char * mac)
{
	return GetMac1(mac);
	/*
	NCB ncb;
	typedef struct _ASTAT_
	{
		ADAPTER_STATUS   adapt;
		NAME_BUFFER   NameBuff[30];
	}ASTAT, *PASTAT;
	ASTAT Adapter;
	typedef struct _LANA_ENUM
	{
		UCHAR   length;
		UCHAR   lana[MAX_LANA];
	}LANA_ENUM;
	LANA_ENUM lana_enum;
	UCHAR uRetCode;
	memset(&ncb, 0, sizeof(ncb));
	memset(&lana_enum, 0, sizeof(lana_enum));
	ncb.ncb_command = NCBENUM;
	ncb.ncb_buffer = (unsigned char *)&lana_enum;
	ncb.ncb_length = sizeof(LANA_ENUM);
	uRetCode = Netbios(&ncb);
	if (uRetCode != NRC_GOODRET)
		return uRetCode;
	for (int lana = 0; lana < lana_enum.length; lana++)
	{
		ncb.ncb_command = NCBRESET;
		ncb.ncb_lana_num = lana_enum.lana[lana];
		uRetCode = Netbios(&ncb);
		if (uRetCode == NRC_GOODRET)
			break;
	}
	if (uRetCode != NRC_GOODRET)
		return uRetCode;
	memset(&ncb, 0, sizeof(ncb));
	ncb.ncb_command = NCBASTAT;
	ncb.ncb_lana_num = lana_enum.lana[0];
	strcpy((char*)ncb.ncb_callname, "*");
	ncb.ncb_buffer = (unsigned char *)&Adapter;
	ncb.ncb_length = sizeof(Adapter);
	uRetCode = Netbios(&ncb);
	if (uRetCode != NRC_GOODRET)
		return uRetCode;
	sprintf(mac, "%02X-%02X-%02X-%02X-%02X-%02X",
		Adapter.adapt.adapter_address[0],
		Adapter.adapt.adapter_address[1],
		Adapter.adapt.adapter_address[2],
		Adapter.adapt.adapter_address[3],
		Adapter.adapt.adapter_address[4],
		Adapter.adapt.adapter_address[5]);
	return 0;
	*/
}

int CTools::GetRandomNum(int iMin, int iMax)
{
	static std::random_device rd;
	static std::mt19937 mt(rd());
	std::uniform_int_distribution<> dis(iMin, iMax);
	return dis(mt);
}

string CTools::GetRandomString(int num)
{
	if (num > 4095) num = 4095;
	char buff[4096];
	for (int i = 0; i < num; ++i){
		buff[i] = GetRandomNum('A', 'z');
		if (buff[i] > 90 && buff[i] < 97){
			buff[i] = '_';
		}
	}
	buff[num] = 0;
	return buff;
}

string CTools::GetTimeString()
{
	time_t t = time(0);
	char tmp[64];
	strftime(tmp, sizeof(tmp), "%Y/%m/%d %H:%M:%S", localtime(&t));
	return tmp;
}

void CTools::Log(const char *msg, const char *fname)
{
	ofstream ofs(fname);
	if (!ofs.is_open()) return;
	ofs << GetTimeString() << " " << msg << endl;
	ofs.close();
}

void CTools::Assert(bool ret, const char *errormsg)
{
	if (false == ret){
		Log(errormsg);
	}
}


CTools theTools;