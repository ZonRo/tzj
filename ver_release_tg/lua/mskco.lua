local setmetatable = setmetatable
local print = print
local warn = print
local cocreate = coroutine.create
local coresume = coroutine.resume
local coyield = coroutine.yield
local corunning = coroutine.running
local costatus = coroutine.status
local oldpcall, oldxpcall = pcall, xpcall
local dbgtraceback = debug.traceback
local unpack = unpack
local _G = _G
local type = type
module('mskco')

function NewStack()
	local _top = 1
	local _cos = {}
	local _times = {}
   setmetatable(_cos, { __index = {
		push = function (_cos,co ,tm)
			_cos[_top] = co
			_times[_top] = tm
			_top = _top+1
			return _top-1
		end,
		
		get = function(_cos,idx)
			if idx < 1 or idx >= _top then return end
			return _cos[idx],_times[idx]
		end,
		
		set = function(_cos,idx,co,tm)
			if idx < 1 or idx >= _top then return end
			_cos[idx] = co
			_times[idx] = tm
		end,
		
		erase = function(_cos,co)
			local idx = 0
			for i=1,_top-1 do
				if _cos[i] == co then
					idx = i
					break
				end
			end
			if idx < 1 or idx >= _top then return end
			if idx == _top -1 then
				_top = _top -1
			else
				_top = _top -1
				_cos[idx] = _cos[_top]
				_times[idx] = _times[_top]
			end
		end,
		
		size = function(_cos)
			return _top-1
		end,
		
		pop = function (_cos, key)
			if _top < 2 then return end
			_top = _top - 1
			return _cos[_top],_times[_top]
		end,
   }})
   return _cos
end


function NewSet(set)
   local reverse = {}
   set = set or {}
   local q = {}
   setmetatable(set, { __index = {
       insert = function(set, value)
           if not reverse[value] then
               set[#set + 1] = value
               reverse[value] = #set
           end
       end,

       remove = function(set, value)
           local index = reverse[value]
           if index then
               reverse[value] = nil
               local top = set[#set]
               set[#set] = nil
               if top ~= value then
                   reverse[top] = index
                   set[index] = top
               end
           end
       end,

       push = function (set, key, itm)
               local qKey = q[key]
               if qKey == nil then
                       q[key] = {itm}
               else
                       qKey[#qKey + 1] = itm
               end
       end,

       pop = function (set, key)
         local t = q[key]
         if t ~= nil then
           local ret = table.remove (t, 1)
           if t[1] == nil then
             q[key] = nil
           end
           return ret
         end
       end
   }})
   return set
end

function NewMap()
	local _set = {}
	 setmetatable(_set, { __index = {
		push = function (_set,v,co ,tm)
			local t = _set[v]
			if nil == t then
				t = NewStack()
				_set[v] = t
			end
			return t:push(co,tm)
		end,
		
		erase = function(_set,v,co)
			local t = _set[v]
			if t then t:erase(co) end
		end,
		
		pop = function (_set, k)
			local t = _set[k]
			if t then return t:pop() end
		end
   }})
	return _set
end

local _errfuns = {}
local _threadstack = NewStack()--sleep 状态的线程
local _eventsets = NewMap()--事件回调->线程
local _eventfuns = {}--事件回调->函数
local _eventerrfs = {}
local _tmoutthreads = {}
local _evproducers = {}
local _funevents = {}

local function _Resume(co,...)
	--print("_Resume",co)
	local ok,err,ret1,ret2 = coresume(co,...)
	if not ok then 
		local errf = _errfuns[co] or print
		errf("_Resume error",co,err)
	end
	_errfuns[co] = nil
	return ok,err,ret1,ret2
end

local function _Create(f,...)
end

function Clear(notall)
	_errfuns = {}
	_threadstack = NewStack()--sleep 状态的线程
	_eventsets = NewMap()--事件回调->线程
	_tmoutthreads = {}
	if not notall then
		_eventfuns = {}--事件回调->函数
		_eventerrfs = {}
	end
end

function SetEventHandler(ev,handler,errf)
	errf = errf or function(...) warn('event handle failed:',ev,...) end
	_eventfuns[ev] = handler
	_eventerrfs[ev] = errf
end

function SetEventProducer(fname,event,t,after)
	t = t or _G
	local f = t[fname]
	if type(f) ~= 'function' then
		return warn('SetEventProducer failed ',fname,'not a function')
	end
	if _evproducers[f] then
		f = _evproducers[f]
	end
	local newf = not after and function(...)
								local ok,ret = HandleEvent(event,...)
								return ok and ret or f(...)
							end 
							or function(...)
								f(...)
								HandleEvent(event,...)
							end
	_evproducers[newf] = f
	t[fname] = newf
end

function SetFunctionHandler(fname,handle,t)
	t = t or _G
	local f = t[fname]
	if type(f) ~= 'function' then
		return warn('SetEventProducer failed ',fname,'not a function')
	end
	if _funevents[f] then
		f = _funevents[f]
	end
	local newf = function(...)
		local ok,ret = HandleEvent(f,...)
		return ok and ret or f(...)
	end
	_funevents[newf] = f
	t[fname] = newf
	SetEventHandler(f,handle)
end

--唤醒等待的线程或者启动新的线程
function HandleEvent(event,...)
	local co = _eventsets:pop(event)
	if co then 
		print("HandleEvent thread",event,co)
		if _tmoutthreads[co] then
			_threadstack:erase(co)
			_tmoutthreads[co] = nil
		end
		return _Resume(co,...)
	end
	local f = _eventfuns[event]
	if f then
		print("HandleEvent function",event,f)
		co = cocreate(f)
		_errfuns[co] = _eventerrfs[event]
		return _Resume(co,...)
	end
	print("HandleEvent nothing",event)
end

--等待事件触发
function Wait(ev)
	print('Wait Event',ev)
	local co = corunning()
	local idx = _eventsets:push(ev,co)
	return coyield()
end


--延时
function Sleep(tm,ev)
	local co = corunning()
	local idx = _threadstack:push(co,tm)
	if ev then
		_tmoutthreads[co] = ev
		_eventsets:push(ev,co)
	end
	return coyield()
end

--处理sleep状态的线程 
function Step(tm)
	local co,stm = _threadstack:pop()
	if co then 
		stm = stm-tm
		if stm > 0 then
			_threadstack:push(co,stm)
		else
			if _tmoutthreads[co] then
				_eventsets:erase(_tmoutthreads[co],co)
				_tmoutthreads[co] = nil
			end
			return _Resume(co) 
		end
	end
end


-------------------------------------------------------------------------------
-- Coroutine safe xpcall and pcall versions
--
-- Encapsulates the protected calls with a coroutine based loop, so errors can
-- be dealed without the usual Lua 5.x pcall/xpcall issues with coroutines
-- yielding inside the call to pcall or xpcall.
--
-- Authors: Roberto Ierusalimschy and Andre Carregal 
-- Contributors: Thomas Harning Jr., Ignacio Burgue駉, F醔io Mascarenhas
--
-- Copyright 2005 - Kepler Project (www.keplerproject.org)
--
-- $Id: coxpcall.lua,v 1.13 2008/05/19 19:20:02 mascarenhas Exp $
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Implements xpcall with coroutines
-------------------------------------------------------------------------------
local performResume, handleReturnValue

function handleReturnValue(err, co, status, ...)
    if not status then
        return false, err(dbgtraceback(co, (...)), ...)
    end
    if costatus(co) == 'suspended' then
        return performResume(err, co, coyield(...))
    else
        return true, ...
    end
end

function performResume(err, co, ...)
    return handleReturnValue(err, co, coresume(co, ...))
end    

function xpcall(f, err, ...)
    local res, co = oldpcall(cocreate, f)
    if not res then
        local params = {...}
        local newf = function() return f(unpack(params)) end
        co = cocreate(newf)
    end
    return performResume(err, co, ...)
end

-------------------------------------------------------------------------------
-- Implements pcall with coroutines
-------------------------------------------------------------------------------

local function id(trace, ...)
  return ...
end

function pcall(f, ...)
    return xpcall(f, id, ...)
end
