package.path = "../lua/?.lua"
local socket = require("luasocket.socket")
local math = math
local sleep = socket.sleep
host = host or "127.0.0.1"
port = port or 1117

function threadfun()
  local cln = assert(socket.udp())
  assert(cln:setpeername("127.0.0.1",1117))
  math.randomseed(os.time())
  assert(udp:settimeout(2))
  local dgram, err
  while true do
    cln:send(math.random(1,100)) 
    dgram, err = cln:receive()
    if dgram then 
      print("recv from server:"..dgram)
    else
      print("recv error:"..err)
    end
    sleep(2)
  end
end


print("Binding to host '" ..host.. "' and port " ..port.. "...")
udp = assert(socket.udp())
assert(udp:setsockname(host, port))
--assert(udp:settimeout(10))
ip, port = udp:getsockname()
assert(ip, port)

--运行客户线程
local clnthread = newthread()
runthread(clnthread,threadfun)
local dgram, ip, port
while 1 do
	dgram, ip, port = udp:receivefrom()
	if dgram then
		print("Echoing '" .. dgram .. "' to " .. ip .. ":" .. port)
		udp:sendto(dgram, ip, port)
  end
end
