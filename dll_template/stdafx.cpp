// stdafx.cpp : 只包括标准包含文件的源文件
// dll_template.pch 将作为预编译头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"

// TODO: 在 STDAFX.H 中
// 引用任何所需的附加头文件，而不是在此文件中引用
HANDLE g_hMutex = NULL;
char dllModuleName[MAX_PATH] = {0};
char exeModuleName[MAX_PATH] = {0};
//char gAppPath[MAX_PATH] = { 0 };
ScopeGuard g_sgCallwapper;
lua_State *g_luaState = NULL;
std::string g_strSigData;
CRITICAL_SECTION g_csSafeCall;
CRITICAL_SECTION g_csGlobal;
HWND g_hGame = NULL;
DWORD g_dwGameWndThread = 0;
set<HANDLE> g_arrHandle;
DWORD g_dwSafeCallJmp;
lua_State *g_luaGame = NULL;
bool g_bWaitCall = false;
HANDLE g_hCall = NULL;
hash_map<string, string> g_mapGlobal;
SOCKET g_sockClient;
int g_iSocketPort = 6119;
//需要Hook的lua函数
_lua_newstate g_lua_newstate = NULL;
_luaL_register g_luaL_register = NULL;
_luaL_loadfile g_luaL_loadfile = NULL;
_lua_pcall g_lua_pcall = NULL;
_luaL_loadbuffer g_luaL_loadbuffer = NULL;
_lua_tolstring g_lua_tolstring = NULL;
_lua_settop g_lua_settop = NULL;
_lua_getfield g_lua_getfield;


void printDebugMessage(const char *lpszFormat)
{
	if (lpszFormat == 0 || strlen(lpszFormat) == 0)
		return;
	string strMsg = "msk_tg:";
	strMsg += lpszFormat;
	//va_list vl;
	//va_start(vl, lpszFormat);
	//CStringA s;
	//s.FormatV(lpszFormat, vl);
	//strMsg = strMsg + s.GetBuffer(s.GetLength());
	//s.ReleaseBuffer();
	OutputDebugStringA(strMsg.c_str());
}


void printDebugMessage(const wchar_t * msg)
{
	if (msg == 0 || wcslen(msg) == 0)
		return;
	wstring strMsg = msg;
	strMsg = L"msk_tg:" + strMsg;
	OutputDebugStringW(strMsg.c_str());
}

DWORD _CreateFileA = (DWORD)GetProcAddress(GetModuleHandleA("Kernel32.dll"), "CreateFileW") + 5;
HANDLE
WINAPI
MyCreateFileW(LPCWSTR lpFileName, DWORD dwDesiredAccess, DWORD dwShareMode, LPSECURITY_ATTRIBUTES lpSecurityAttributes, DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes, HANDLE hTemplateFile)
{
	__asm jmp _CreateFileA
}