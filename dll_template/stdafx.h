// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             //  从 Windows 头文件中排除极少使用的信息
// Windows 头文件:
#include <windows.h>
#include <WinSock2.h>
#define DllImport   __declspec( dllimport )
#define DllExport   extern "C" __declspec( dllexport )

// TODO: 在此处引用程序需要的其他头文件
#include "../lua514/lua.hpp"
#include "../luatinker/lua_tinker.h"
#include <process.h>
extern "C"{;
#include "../luasocket/luasocket.h"
#include "../luasocket/mime.h"
#include "../luasql/luasql.h"
#include "../lmd5/ldes56.h"
#include "../lpack/lpack.h"
}
#include "../macro/macro.h"
#include "../msktools/msktools.h"
using namespace macro;
#include <string>
#include <exception>
#include <stdexcept>
#include <queue>
#include <hash_map>
#include <vector>
#include <set>
using namespace std;
//定义一些 打码API
#define  CRACKCAPTCHA_API WINAPI
typedef int (WINAPI *_D2WndHandle)(
	const char *pszSoftwareId,
	const char *pszUserName,
	const char *pszUserPassword,
	HWND hHandle, LPCRECT lpRect,
	unsigned short usTimeout,
	unsigned long ulVCodeTypeID,
	char *pszVCodeText);
typedef int (CRACKCAPTCHA_API* _DaMaInit)(const char *pszSoftwareName, const char *pszSoftwareID);
typedef int (CRACKCAPTCHA_API* _DamaLogin2)(const char *pszUserName, const char *pszUserPassword);
typedef int (CRACKCAPTCHA_API* _DecodeWndHandle)(
	HWND hWnd, LPCRECT lpRect,
	unsigned char ucVerificationCodeLen, unsigned short usTimeout,
	unsigned long ulVCodeTypeID,
	unsigned long *pulRequestID);
typedef int (CRACKCAPTCHA_API* _GetResult)(unsigned long ulRequestID,
	unsigned long ulTimeout, char *pszVCode, unsigned long ulVCodeBufLen,
	unsigned long *pulVCodeID, char *pszReturnCookie, unsigned long ulCookieBufferLen);

typedef int (CRACKCAPTCHA_API* _D2File)(
	const char *pszSoftwareId,
	const char *pszUserName,
	const char *pszUserPassword,
	const char *pszFilePath,
	unsigned short usTimeout,
	unsigned long ulVCodeTypeID,
	char *pszVCodeText);
typedef bool(__stdcall *sendDataFun)(const char *);
typedef const char*(__stdcall *DecryptFun)(const char *, const char*);

//定义一些 lua API
typedef void(*_luaL_register)(lua_State *L, const char *libname,
	const luaL_Reg *l);
typedef int(*_luaL_loadbuffer)(lua_State *L, const char *buff, size_t sz,
	const char *name);
typedef int(*_luaL_loadfile)(lua_State *L, const char *filename);
typedef int(*_lua_pcall)(lua_State *L, int nargs, int nresults, int errfunc);
typedef lua_State *(*_lua_newstate)(lua_Alloc f, void *ud);
typedef const char     *( * _lua_tolstring)(lua_State *L, int idx, size_t *len);
typedef void(*_lua_settop)(lua_State *L, int idx);
typedef void(*_lua_getfield)(lua_State *L, int idx, const char *k);
//异常
#define  _TEST
#define EXCEPTION_NAME(file,line,expname) file##line##expname
#define ASSERT_DOMAIN(exp) if (!(exp)) throw domain_error("domain_error:"EXCEPTION_NAME(_FILE_,__LINE__,#exp))
#define ASSERT_ARG(exp) if (!(exp)) throw invalid_argument("invalid_argument:"EXCEPTION_NAME(_FILE_,__LINE__,#exp))
#define ASSERT_LENGTH(exp) if (!(exp)) throw length_error("length_error:"EXCEPTION_NAME(_FILE_,__LINE__,#exp))
#define ASSERT_RANGE(exp) if (!(exp)) throw out_of_range("length_error:"EXCEPTION_NAME(_FILE_,__LINE__,#exp))


//安全CALL
#define SAFECALL2(x) __asm push x \
	__asm mov eax, g_dwSafeCallJmp \
	__asm call eax
#define SAFECALL(x) __asm push x \
	__asm mov ebx, g_dwSafeCallJmp \
	__asm call ebx
/*
#define MSKCALL(fun) CAutoSection asTmp(g_csSafeCall);\
	g_sgCallwapper.Replace([&](){\
	fun; \
	}); \
	SendMessage(g_hGame, WM_WGCALL, 123, 0)
*/
#define MSKCALL(fun) CAutoSection asTmp(g_csSafeCall);\
	g_sgCallwapper.Replace([&](){\
	fun; \
}); \
	g_bWaitCall = true;\
	WaitForSingleObject(g_hCall,-1)

//LUA C 交互 类型
#define E_V_BYTE 0
#define E_V_WORD 1
#define E_V_DWORD 2
#define E_V_FLOAT 3
#define E_V_ASCII 4
#define E_V_UNICODE 5

//
#define	WM_WGCALL WM_ACTIVATEAPP

#define  NO_MODULE_MARK 0x2012aaae
#define fCreateFileW  MyCreateFileW
#define fReadFile  ReadFile
#define fWriteFile  WriteFile
#define fCloseHandle  CloseHandle
#define fGetFileSize  GetFileSize

extern HANDLE g_hMutex;
extern char dllModuleName[MAX_PATH];
extern char exeModuleName[MAX_PATH];
//extern char gAppPath[MAX_PATH];
extern ScopeGuard g_sgCallwapper;
extern lua_State *g_luaState;
extern CRITICAL_SECTION g_csSafeCall;
extern CRITICAL_SECTION g_csGlobal;
extern HWND g_hGame;
extern DWORD g_dwGameWndThread;
extern set<HANDLE> g_arrHandle;
extern DWORD g_dwSafeCallJmp;
extern lua_State *g_luaGame;
extern bool g_bWaitCall;
extern HANDLE g_hCall;
extern hash_map<string, string> g_mapGlobal;
extern SOCKET g_sockClient;
extern string g_strSigData;
extern int g_iSocketPort;
//需要Hook的lua函数
extern _lua_newstate g_lua_newstate;
extern _luaL_register g_luaL_register;
extern _luaL_loadfile g_luaL_loadfile;
extern _lua_pcall g_lua_pcall;
extern _luaL_loadbuffer g_luaL_loadbuffer;
extern _lua_tolstring g_lua_tolstring;
extern _lua_settop g_lua_settop;
extern _lua_getfield g_lua_getfield;
//extern hash_map<string, string> g_mapGlobal;

void printDebugMessage(const char *lpszFormat);
void printDebugMessage(const wchar_t * msg);
string getglobal(string gname);
void setglobal(string gname, string gvalue);
HANDLE WINAPI MyCreateFileW(LPCWSTR lpFileName, DWORD dwDesiredAccess, DWORD dwShareMode, LPSECURITY_ATTRIBUTES lpSecurityAttributes, DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes, HANDLE hTemplateFile);